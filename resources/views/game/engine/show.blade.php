@extends("game.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/train.png') }}" alt="">
            </div>
            <x-base.underline
                title="Détail d'un engin"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.engine.partials.menu")
    </div>
    <div class="card shadow-lg bg-gray-400">
        <div class="card-body p-5">
            <div class="d-flex flex-row justify-content-end align-items-center nav mb-5">
                <button class="btn btn-flush active me-3" data-bs-toggle="tab" data-bs-target="#detail">
                    <img src="{{ asset('/storage/icons/train.png') }}" class="w-50px h-50px" alt="" data-bs-toggle="tooltip" title="Détail d'un engin">
                </button>
                <button class="btn btn-flush me-3" data-bs-toggle="tab" data-bs-target="#hub">
                    <img src="{{ asset('/storage/icons/hub.png') }}" class="w-70px h-70px" alt="" data-bs-toggle="tooltip" title="Assignée à un hub">
                </button>
                <button class="btn btn-flush me-3" data-bs-toggle="tab" data-bs-target="#sell">
                    <img src="{{ asset('/storage/icons/train_sell.png') }}" class="w-50px h-50px" alt="" data-bs-toggle="tooltip" title="Vente d'un engin">
                </button>
                <button class="btn btn-flush me-3" data-bs-toggle="tab" data-bs-target="#planning">
                    <img src="{{ asset('/storage/icons/planning.png') }}" class="w-50px h-50px" alt="" data-bs-toggle="tooltip" title="Planifier les trajets">
                </button>
                <button class="btn btn-flush me-3" data-bs-toggle="tab" data-bs-target="#travels">
                    <img src="{{ asset('/storage/icons/ligne.png') }}" class="w-50px h-50px" alt="" data-bs-toggle="tooltip" title="Détail des trajets">
                </button>
                <button class="btn btn-flush me-3" data-bs-toggle="tab" data-bs-target="#incidents">
                    <img src="{{ asset('/storage/icons/train_incident.png') }}" class="w-50px h-50px" alt="" data-bs-toggle="tooltip" title="Historique des incidents">
                </button>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="detail" role="tabpanel">
                    @include("game.engine.partials.show.detail", ["engine" => $engine])
                </div>
                <div class="tab-pane fade" id="hub" role="tabpanel">
                    @include("game.engine.partials.show.hub", ["engine" => $engine])
                </div>
                <div class="tab-pane fade" id="sell" role="tabpanel">
                    @include("game.engine.partials.show.sell", ["engine" => $engine])
                </div>
                <div class="tab-pane fade" id="planning" role="tabpanel">
                    @include("game.engine.partials.show.planning", ["engine" => $engine])
                </div>
                <div class="tab-pane fade" id="travels" role="tabpanel">
                    @include("game.engine.partials.show.trajet", ["engine" => $engine])
                </div>
                <div class="tab-pane fade" id="incidents" role="tabpanel">
                    @include("game.engine.partials.show.incident", ["engine" => $engine])
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.9/index.global.min.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/@fullcalendar/core@6.1.9/locales-all.global.min.js'></script>

    <script data-js="variable" type="text/javascript">
        let manualCalendar = document.querySelector('#manualCalendar');
        let todayDate = moment().locale('fr').startOf("day");
        let YM = todayDate.format("YYYY-MM");
        let YESTERDAY = todayDate.clone().subtract(1, "day").format("YYYY-MM-DD");
        let TODAY = todayDate.format("YYYY-MM-DD");
        let TOMORROW = todayDate.clone().add(1, "day").format("YYYY-MM-DD");
        let NOW = moment().locale('fr').format("YYYY-MM-DDTHH:mm:ss");
    </script>
    <script data-js="tablePlanning" type="text/javascript">
        let table = $('#planningStack').DataTable({
            info: !1,
            order: [],
            pageLength: 10,
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
        });
    </script>
    <script data-js="action" type="text/javascript">
        document.querySelectorAll('[data-action="maintenance"]').forEach(btn => {
            btn.addEventListener('click', e => {
                e.preventDefault()
                $.ajax({
                    url: '{{ route('api.game.maintenance.store') }}',
                    method: "POST",
                    data: {
                        user_id: '{{ auth()->user()->id }}',
                        engine_id: '{{ $engine->id }}',
                        type: btn.dataset.type,
                    },
                    success: data => {
                        toastr.success("Maintenance programmée avec succès.", "Maintenance matériel roulant")
                    }
                })
            })
        })

        document.querySelectorAll('[data-action="TransferToHub"]').forEach(btn => {
            btn.addEventListener('click', e => {
                e.preventDefault()
                $.ajax({
                    url: '{{ route('api.game.maintenance.engine.transfer') }}',
                    method: "GET",
                    data: {
                        user_id: '{{ auth()->user()->id }}',
                        engine_id: '{{ $engine->id }}',
                        to_engine_id: btn.dataset.to,
                    },
                    success: () => {
                        toastr.success("Transféré avec succès.", "Matériel Roulant")
                        setTimeout(() => {
                            window.location.reload()
                        }, 1200)
                    },
                    error: (jqxhr, data) => {
                        console.log(jqxhr)
                        console.log(data)
                    }
                })
            })
        })

        document.querySelector('[data-action="selling"]').addEventListener('click', e => {
            e.preventDefault()
            $.ajax({
                url: '{{ route('api.game.maintenance.engine.sell') }}',
                method: "DELETE",
                data: {
                    user_id: '{{ auth()->user()->id }}',
                    engine_id: '{{ $engine->id }}',
                },
                success: () => {
                    toastr.success("Vente effectué avec succès", "Matériel Roulant")
                    setTimeout(() => {
                        window.location.href = '{{ route('engine.index') }}'
                    }, 1200)
                },
                error: () => {
                    toastr.error("Une erreur à eu lieu, nous avons été prévenue", "Erreur Système X0032")
                }
            })
        })

    </script>
    <script data-js="calendarPlanning" type="text/javascript">
        $.ajax({
            url: '/api/account/{{ auth()->user()->id }}/planning/engine/{{ $engine->id }}',
            success: data => {
                let calendar = new FullCalendar.Calendar(manualCalendar, {
                    locale: 'fr',
                    headerToolbar: {
                        left: "prev,next today",
                        center: "title",
                    },

                    height: 800,
                    contentHeight: 780,
                    aspectRatio: 3,
                    buttonIcons: false, // show the prev/next text
                    selectable: true,
                    selectMirror: true,

                    nowIndicator: true,
                    now: NOW,

                    views: {
                        dayGridMonth: {buttonText: "month"},
                        timeGridWeek: {buttonText: "week"},
                        timeGridDay: {buttonText: "day"}
                    },

                    initialView: "timeGridDay",
                    initialDate: TODAY,

                    editable: true,
                    dayMaxEvents: true, // allow "more" link when too many events
                    navLinks: true,
                    events: data.plannings,
                    select: (args) => {
                        $.ajax({
                            url: '/api/account/{{ auth()->user()->id }}/planning/engine/{{ $engine->id }}/check',
                            method: 'GET',
                            data: {'start': args.startStr, 'time': data.ligne_time},
                            success: data => {
                                if (data.status === 'success') {
                                    Swal.fire({
                                        icon: 'question',
                                        title: data.message + ', êtes-vous sûr ?',
                                        showCancelButton: true,
                                        buttonsStyling: false,
                                        confirmButtonText: "Oui, utiliser ce créneaux",
                                        cancelButtonText: "Non",
                                        customClass: {
                                            confirmButton: "btn btn-primary",
                                            cancelButton: "btn btn-active-light"
                                        }
                                    }).then((result) => {
                                        if (result.isConfirmed) {
                                            $.ajax({
                                                url: '/api/account/{{ auth()->user()->id }}/planning/engine/{{ $engine->id }}/add',
                                                method: 'POST',
                                                data: {'start': args.startStr},
                                                success: data => {
                                                    if (data.status === 'success') {
                                                        toastr.success(data.message, null, {
                                                            positionClass: 'toastr-bottom-center',
                                                        });
                                                        calendar.addEvent({
                                                            title: data.engine,
                                                            start: data.start,
                                                            end: data.end,
                                                            allDay: data.allDay
                                                        })
                                                    } else {
                                                        toastr.error(data.message, null, {
                                                            positionClass: 'toastr-bottom-center',
                                                        });
                                                    }
                                                }
                                            })
                                        } else {
                                            calendar.unselect()
                                        }
                                    })
                                } else {
                                    toastr.error(data.message, null, {
                                        positionClass: 'toastr-bottom-center',
                                    });
                                }
                            }
                        })
                    },
                    eventClick: (arg) => {
                        let planning_id = arg.event._def.extendedProps.planning_id;
                        let engine_id = arg.event._def.extendedProps.engine_id;
                        let start_at = arg.event.startStr
                        console.log(arg.event.startStr)
                        Swal.fire({
                            icon: 'question',
                            title: 'Voulez-vous editer ou supprimer ce créneaux ?',
                            showCancelButton: true,
                            buttonsStyling: false,
                            confirmButtonText: "Editer",
                            cancelButtonText: "Supprimer",
                            customClass: {
                                confirmButton: "btn btn-primary",
                                cancelButton: "btn btn-active-light"
                            }
                        }).then((result) => {
                            console.log(start_at)
                            let htmlEditForm = `
                            <div class="mb-10 fw-bold">Edition de la mission</div>
                            <input type="hidden" name="planning_id" value="${planning_id}">
                            <input type="hidden" name="engine_id" value="${engine_id}">
                            <input type="hidden" name="start_at" value="${start_at}">
                            <div class="mb-5">
                                <label for="date_depart" class="form-label">Heure de départ</label>
                                <input type="text" name="heure_depart" class="form-control" value="${moment(arg.event.start).locale('fr').format('HH:mm')}" >
                            </div>
                            <div class="mb-5">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="repeat_mission" value="1" id="flexCheckDefault" />
                                    <label class="form-check-label" for="flexCheckDefault">
                                        Répétition de la mission
                                    </label>
                                </div>
                            </div>
                            <div class="mb-5">
                                <label for="date_depart" class="form-label">Nombre de fois</label>
                                <input type="text" name="repeat_mission_number" class="form-control" >
                            </div>
                            `
                            if (result.isConfirmed) {
                                Swal.fire({
                                    title: 'Edition de la mission',
                                    html: htmlEditForm,
                                    icon: 'question',
                                    confirmButtonText: "Editer la mission",
                                    cancelButtonText: "Annuler",
                                    showCancelButton: true,
                                    buttonsStyling: false,
                                    customClass: {
                                        confirmButton: "btn btn-primary",
                                        cancelButton: "btn btn-danger"
                                    }
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        let heure_depart = document.querySelector('[name="heure_depart"]').value;
                                        let repeat_mission = document.querySelector('[name="repeat_mission"]').checked;
                                        let repeat_mission_number = document.querySelector('[name="repeat_mission_number"]').value;
                                        let planning_id = document.querySelector('[name="planning_id"]').value;

                                        $.ajax({
                                            url: '/api/account/{{ auth()->user()->id }}/planning/engine/{{ $engine->id }}/edit',
                                            method: 'POST',
                                            data: {
                                                'heure_depart': heure_depart,
                                                'repeat_mission': repeat_mission,
                                                'repeat_mission_number': repeat_mission_number,
                                                'planning_id': planning_id,
                                                'start_at': start_at
                                            },
                                            success: data => {
                                                toastr.success(data.message, null, {
                                                    positionClass: 'toastr-bottom-center',
                                                })
                                                calendar.refetchEvents();
                                            }
                                        })
                                    }
                                })
                            }
                            if (result.isDismissed) {
                                Swal.fire({
                                    title: 'Suppression de la mission',
                                    text: "Êtes-vous sûr de vouloir supprimer cette mission ?",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonText: "Supprimer la mission",
                                    cancelButtonText: "Annuler",
                                    buttonsStyling: false,
                                    customClass: {
                                        confirmButton: "btn btn-danger",
                                        cancelButton: "btn btn-active-light"
                                    }
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        $.ajax({
                                            url: '/api/account/{{ auth()->user()->id}}/planning/engine/{{ $engine->id }}/delete',
                                            method: 'POST',
                                            data: {'planning_id': planning_id},
                                            success: data => {
                                                toastr.success(data.message, null, {
                                                    positionClass: 'toastr-bottom-center',
                                                })
                                                calendar.refetchEvents();
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
                calendar.render();
            }
        })
    </script>
    <script data-js="tableTrajet" type="text/javascript">
        let loadTable = document.querySelector('#loadTable')
        document.querySelectorAll('.page-item a').forEach(item => {
            item.addEventListener('click', event => {
                event.preventDefault()
                let table = loadTable.querySelector('#liste_travel')
                let date = item.dataset.date
                loadTable.querySelector('.table-responsive').classList.add('table-loading')
                table.querySelector('tbody').innerHTML = ''

                $.ajax({
                    url: '/api/game/maintenance/engine/travels',
                    method: 'GET',
                    data: {date: date, engine_id: {{ $engine->id }}},
                    success: function (data) {
                        table.querySelector('tbody').innerHTML = data.content
                        loadTable.querySelector('.table-responsive').classList.remove('table-loading')
                        new bootstrap.Tooltip(document.querySelector('[data-bs-toggle="tooltip"]'));
                    }
                })
            })
        })

    </script>
    <script data-js="modalIncident" type="text/javascript">
        document.querySelectorAll('.showIncident').forEach(btn => {
            btn.addEventListener('click', (e) => {
                const incidentId = btn.getAttribute('data-incident-id');
                let modal = document.querySelector('#incidentModal');

                $.ajax({
                    url: '/api/game/maintenance/incident/'+incidentId,
                    data: {user_id: {{ auth()->user()->id }}},
                    success: data => {
                        modal.querySelector('.modal-title').querySelector('[data-content="warning"]').setAttribute('src', '/storage/icons/warning_'+data.incident.niveau_indicator+'.png');
                        modal.querySelector('.modal-title').querySelector('[data-content="title"]').innerHTML = data.incident.designation
                        modal.querySelector('.modal-body').querySelector('[data-content="status"]').innerHTML = data.incident.status_label
                        modal.querySelector('.modal-body').querySelector('[data-content="type_incident"]').innerHTML = data.incident.type.designation
                        modal.querySelector('.modal-body').querySelector('[data-content="image_origin"]').setAttribute('src', data.incident.type.image_origin_url)
                        modal.querySelector('.modal-body').querySelector('[data-content="probleme"]').innerHTML = data.incident.designation
                        modal.querySelector('.modal-body').querySelector('[data-content="amount"]').innerHTML = data.incident.amount_reparation_format
                        modal.querySelector('.modal-body').querySelector('[data-content="traffic"]').innerHTML = data.incident.repercute_traffic_format
                        new bootstrap.Modal(modal).show();
                    }
                })
            })
        })

    </script>
@endsection
