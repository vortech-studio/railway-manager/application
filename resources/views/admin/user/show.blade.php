@extends("admin.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Joueur: " . $user->name ." (". $user->email .")",
        'action' => true,
        'data' => [
            [
                'url' => back()->getTargetUrl(),
                'title' => "<i class='fa-solid fa-arrow-left me-2'></i> Retour",
                'class' => "primary"
            ],
        ]
    ])
@endsection

@section("content")
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="card shadow-lg">
                <div class="card-body">
                    <div class="d-flex flex-center flex-column py-5">
                        <!--begin::Avatar-->
                        <div class="symbol symbol-100px symbol-circle mb-7">
                            <img src="/storage/avatar/users/{{ $user->avatar }}" alt="image">
                            @if($user->premium)
                                <span class="symbol-badge badge badge-circle bg-body top-100 start-100" data-bs-toggle="tooltip" title="Premium">
                                    <img src="/storage/icons/premium.png" alt="" class="w-15px">
                                </span>
                            @endif
                        </div>
                        <!--end::Avatar-->

                        <!--begin::Name-->
                        <a href="#" class="fs-3 text-gray-800 text-hover-primary fw-bold mb-3">{{ $user->name }}</a>
                        <!--end::Name-->

                        @if($user->admin)
                        <!--begin::Position-->
                        <div class="mb-9">
                            <!--begin::Badge-->
                            <div class="badge badge-lg badge-light-primary d-inline">Administrateur</div>
                            <!--begin::Badge-->
                        </div>
                        <!--end::Position-->
                        @else
                            <!--begin::Position-->
                            <div class="mb-9">
                                <!--begin::Badge-->
                                <div class="badge badge-lg badge-light-danger d-inline">Joueur</div>
                                <!--begin::Badge-->
                            </div>
                            <!--end::Position-->
                        @endif
                        <div class="d-flex flex-row mt-5 justify-content-around align-items-center">
                            <div class="d-flex flex-row mt-5 align-items-center me-10">
                                <div class="symbol symbol-50px me-5">
                                    <img src="/storage/avatar/company/{{ $user->logo_company }}" alt="">
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fw-bold">Company</div>
                                    <div class="text-gray-600">{{ $user->name_company }}</div>
                                </div>
                            </div>
                            <div class="d-flex flex-row mt-5 align-items-center">
                                <div class="symbol symbol-50px me-5">
                                    <img src="/storage/avatar/secretary/{{ $user->avatar_secretary }}" alt="">
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fw-bold">Secrétariat</div>
                                    <div class="text-gray-600">{{ $user->name_secretary }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-stack fs-4 py-3">
                        <div class="fw-bold rotate collapsible" data-bs-toggle="collapse" href="#kt_user_view_details" role="button" aria-expanded="false" aria-controls="kt_user_view_details">
                            Details
                            <span class="ms-2 rotate-180">
                                <i class="ki-duotone ki-down fs-3"></i>
                            </span>
                        </div>

                        <span data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-original-title="Edit customer details" data-kt-initialized="1">
                            <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-sm btn-light-primary">
                                Editer
                            </a>
                        </span>
                    </div>
                    <div class="separator"></div>
                    <div id="kt_user_view_details" class="collapse show">
                        <div class="d-flex flex-row mt-5 justify-content-between align-items-center">
                            <div class="fw-bold">Compte Vérifier</div>
                            {!! $user->email_verified_at ? "<i class='fa-solid fa-check text-success fs-3'></i>" : "<i class='fa-solid fa-time text-danger fs-3'></i>" !!}
                        </div>
                        <div class="d-flex flex-row mt-5 justify-content-between align-items-center">
                            <div class="fw-bold">Email</div>
                            <div class="text-gray-600">{{ $user->email }}</div>
                        </div>

                        <div class="d-flex flex-row mt-5 justify-content-between align-items-center">
                            <div class="fw-bold">Niveau Actuel</div>
                            <span class="badge badge-circle badge-primary">{{ $user->getLevel() }}</span>
                        </div>
                        <div class="d-flex flex-row mt-5 justify-content-between align-items-center">
                            <div class="fw-bold">Expériences</div>
                            <div>
                                <div class="fw-bolder">{{ $user->getPoints() }} Points</div>
                                Prochain niveau dans <div class="fw-bolder">{{ $user->nextLevelAt() }} Points</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mt-5 justify-content-between align-items-center">
                            <div class="fw-bold">Argent</div>
                            <div class="text-gray-600">{{ eur($user->argent) }}</div>
                        </div>
                        <div class="d-flex flex-row mt-5 justify-content-between align-items-center">
                            <div class="fw-bold">T Point</div>
                            <div class="text-gray-600">{{ $user->tpoint }}</div>
                        </div>
                        <div class="d-flex flex-row mt-5 justify-content-between align-items-center">
                            <div class="fw-bold">Allocation de recherche</div>
                            <div class="text-gray-600">{{ eur($user->research) }}</div>
                        </div>
                        <div class="d-flex flex-row mt-5 justify-content-between align-items-center">
                            <div class="fw-bold">Dernière connexion</div>
                            <div class="text-gray-600">
                                @isset($user->last_login)
                                    @if($user->last_login <= now()->startOfDay())
                                        {{ $user->last_login->format('d/m/Y à H:i') }}
                                    @else
                                        {{ $user->last_login->diffForHumans() }}
                                    @endif
                                @endisset
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12">
            <div class="rounded-3 shadow-lg p-10 bg-white">
                <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#compta">Comptabilités & Statistiques</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#hubs">Hubs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#lines">Lignes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#engines">Matériels Roulants</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#achievements">Achievements</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#settings">Configuration</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="compta" role="tabpanel">
                        @include('admin.user.show.compta', ['user' => $user])
                    </div>
                    <div class="tab-pane fade" id="hubs" role="tabpanel">
                        @include('admin.user.show.hubs', ['user' => $user])
                    </div>
                    <div class="tab-pane fade" id="lines" role="tabpanel">
                        @include('admin.user.show.ligne', ['user' => $user])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>

    <script type="text/javascript">
        let table = $('#liste_mouvements').DataTable({
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.user.show', $user->id) }}",
            columns: [
                {data: 'type_mvm', name: 'type_mvm'},
                {data: 'title', name: 'libelle'},
                {data: 'created_at', name: 'created_at'},
                {data: 'amount', name: 'montant', class: 'text-end'},
            ],
            columnDefs: [
                {
                    targets: 0,
                    render: function (data, type, row) {
                        return `<img src='/storage/icons/mouvement/${row.type_mvm}.png' alt='' class='w-35px me-3' data-bs-toggle='tooltip' title='` + row.type_mvm + "'>";
                    }
                },
            ]
        });
        let tableHub = $('#liste_hub').DataTable({
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
            processing: true,
            serverSide: true,
            ajax: "{{ route('api.core.users.hubs', $user->id) }}",
            columns: [
                {data: 'gare', name: 'gare'},
                {data: 'date_achat', name: 'date_achat'},
                {data: 'km_ligne', name: 'km_ligne'},
                {data: 'ca_hub', name: 'ca_hub'},
            ],
            columnDefs: [
                {
                    targets: 2,
                    render: function (data, type, row) {
                        return data+" km";
                    }
                },
            ]
        });


        document.querySelector('[data-hub-filter="search"]').addEventListener('keyup', function (e) {
            tableHub.search(e.target.value).draw();
        });

        $("#formAddHub").on("submit", function (e) {
            e.preventDefault()
            $.ajax({
                url: "{{ route('api.core.users.addHub', $user->id) }}",
                method: "POST",
                data: $(this).serialize(),
                success: function (data) {
                    tableHub.ajax.reload()
                    $("#formAddHub")[0].reset()
                    $("#addHub").modal('hide')
                    toastr.success(data.message)
                },
                error: function (data) {
                    toastr.error(data.responseJSON.message)
                }
            })
        })

        let tableLigne = $('#liste_ligne').DataTable({
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
            processing: true,
            serverSide: true,
            ajax: "{{ route('api.core.users.lignes', $user->id) }}",
            columns: [
                {data: 'hub', name: 'hub'},
                {data: 'line', name: 'line'},
                {data: 'date_achat', name: 'date_achat'},
                {data: 'nb_depart', name: 'nb_depart'},
                {data: 'composition', name: 'composition'},
            ],
            columnDefs: [
                {
                    targets: 4,
                    render: function (data, type, row) {

                        let html = ""
                        html += data+'<br/>'
                        row.images.forEach(function (item) {
                            console.log(item)
                            html += `<img src='${item}' alt='' class='w-35px me-3' data-bs-toggle='tooltip' title='` + item + "'>"
                        })
                        return html;
                    }
                },
                {
                    targets: 0,
                    render: function (data, type, row) {
                        let html = "";
                        html += `<div class="d-flex flex-row justify-content-between align-items-center">`
                        html += `<span class="fw-bolder">${row.hub}</span>`
                        html += `<div class="d-flex flex-center justify-content-center align-items-center p-5 bg-info border-3 border-inoui-first rounded-2">${row.quai}</div`
                        html += `</div>`

                        return html;
                    }
                },
            ]
        });

        $("#formAddLigne").on("submit", function (e) {
            e.preventDefault()
            $.ajax({
                url: "{{ route('api.core.users.addLigne', $user->id) }}",
                method: "POST",
                data: $(this).serialize(),
                success: function (data) {
                    tableHub.ajax.reload()
                    $("#formAddLigne")[0].reset()
                    $("#addLigne").modal('hide')
                    toastr.success(data.message)
                },
                error: function (data) {
                    toastr.error(data.responseJSON.message)
                }
            })
        })
    </script>
@endsection
