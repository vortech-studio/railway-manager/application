@extends("game.template")

@section("css")
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/hq.png') }}" alt="">
            </div>
            <x-base.underline
                title="Ma Compagnie"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.compagnie.partials.menu")
    </div>
    <div class="card shadow-lg mb-5">
        <div class="card-header">
            <div class="card-title">
                <img src="{{ asset('storage/icons/hq.png') }}" alt="" class="w-30px me-3">
                <span class="fs-3 fw-semibold">Progression des hubs</span>
            </div>
        </div>
        <div class="card-body">
            @foreach(auth()->user()->hubs()->get() as $hub)
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <span>{{ $hub->hub->gare->name }}</span>
                    <span>{!! $hub->calcRatioPerformance() !!}</span>
                </div>
            @endforeach
        </div>
    </div>
    <div class="card shadow-lg mb-5">
        <div class="card-header">
            <div class="card-title">
                <img src="{{ asset('storage/icons/hq.png') }}" alt="" class="w-30px me-3">
                <span class="fs-3 fw-semibold">Votre compagnie</span>
            </div>
        </div>
        <div class="card-body">
            <div class="w-50">
                <div class="d-flex flex-row justify-content-between align-items-center bg-gray-300 rounded p-2 mb-2">
                    <span>Etoile</span>
                    <span></span>
                </div>
                <div class="d-flex flex-row justify-content-between align-items-center bg-gray-300 rounded p-2 mb-2">
                    <span>CA des 7 derniers jours</span>
                    <span>{{ eur($infoCompagnie['CASeven']) }}</span>
                </div>
                <div class="d-flex flex-row justify-content-between align-items-center bg-gray-300 rounded p-2 mb-2">
                    <span>Nombre total de passagers <small>des 7 derniers jours</small></span>
                    <span>{{ $infoCompagnie['nb_passenger'] }}</span>
                </div>
                <div class="d-flex flex-row justify-content-between align-items-center bg-gray-300 rounded p-2 mb-2">
                    <span>Nombre de matériels roulants</span>
                    <span>{{ auth()->user()->engines()->count() }}</span>
                </div>
                <div class="d-flex flex-row justify-content-between align-items-center bg-gray-300 rounded p-2 mb-2">
                    <span>Kilométrage de ligne total</span>
                    <span>{{ $infoCompagnie['kilometrage'] }} Km</span>
                </div>
            </div>
        </div>
    </div>
    <div class="card shadow-lg mb-5">
        <div class="card-body">
            <div class="w-100">
                <div class="d-flex flex-wrap justify-content-between">
                    <div class="d-flex flex-column w-40 me-5">
                        <x-base.underline
                            title="Passagers"
                            size-text="fs-1"
                            size="3" class="w-100 mb-5 fs-uppercase"/>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                            <span>Distraction</span>
                            <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->distraction }}" data-bs-content="Les distractions sont très appréciés durant les longs trajets et en gares">
                                <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->distraction) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                            <span>Tarifications</span>
                            <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->tarification }}" data-bs-content="Les clients qui effectue un cours trajets recherchent avant tous des prix attractifs.">
                                <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->tarification) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                            <span>Ponctualité</span>
                            <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->ponctualite }}" data-bs-content="Les clients de classe unique ou seconde classe apprécient la ponctualité">
                                <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->ponctualite) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                            <span>Sécurité</span>
                            <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->secutite }}" data-bs-content="Les clients apprécients d'être en sécurité lors des trajets.">
                                <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->secutite) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                            <span>Confort</span>
                            <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->confort }}" data-bs-content="Les clients débourseront plus si le confort est augmenté.">
                                <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->confort) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                            <span>Revenues Auxiliaires</span>
                            <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->rent_aux }}" data-bs-content="Augmenté les revenus auxiliaires vous permette d'avoir plus de ventes à bord voir même des subventions.">
                                <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->rent_aux) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                            <span>Frais</span>
                            <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->frais }}" data-bs-content="Réduire les frais permet de diminuer les charges sur les trajets, les augmentés revoir vos prix à la hausse.">
                                <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->frais) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                            <span>Livraison</span>
                            <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->livraison }}" data-bs-content="Plus le niveau est augmenté et plus le service livraison exécutera les tâches rapidement.">
                                <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->livraison) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">

    </script>
@endsection
