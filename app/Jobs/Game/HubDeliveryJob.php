<?php

namespace App\Jobs\Game;

use App\Models\User\UserDelivery;
use App\Models\User\UserHub;
use App\Notifications\Game\HubDeliveryNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class HubDeliveryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserDelivery $delivery, public UserHub $hub)
    {
    }

    public function handle(): void
    {
        $this->hub->active = true;
        $this->hub->save();

        $this->hub->user->notify(new HubDeliveryNotification($this->hub));

        $this->delivery->delete();
    }
}
