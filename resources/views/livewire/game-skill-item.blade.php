<div wire:click="research" class="bg-gray-800 border border-2 border-warning rounded-3 p-10 me-5 mb-5 cursor-pointer">
    <div class="d-flex flex-column w-150px h-150px"
        data-bs-toggle="popover"
         data-bs-trigger="hover"
         title="{{ $skill->name }}"
         data-bs-html="true"
         data-bs-custom-class="popover-inverse"
        data-bs-content="{{ $contentPopover }}"
    >
        <img src="{{ asset('/storage/icons/research/'.$skill->image) }}" alt="" class="w-150px h-150px">
    </div>
</div>
