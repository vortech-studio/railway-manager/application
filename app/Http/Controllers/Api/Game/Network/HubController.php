<?php

namespace App\Http\Controllers\Api\Game\Network;

use App\Http\Controllers\Controller;
use App\Models\Core\Mouvement;
use App\Models\User;
use App\Models\User\UserHub;
use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Http\Request;

class HubController extends Controller
{
    public function list(Request $request)
    {
        $user = User::find($request->get('user_id'));
        $hubs = $user->hubs()->with('lignes', 'hub', 'mouvements')->get();

        return response()->json([
            "hubs" => $hubs
        ]);
    }

    public function info(Request $request, $hub_id)
    {
        $user = User::find($request->get('user_id'));
        $hub = $user->hubs()->with('lignes', 'hub', 'mouvements')->find($hub_id);

        return response()->json([
            "hub" => $hub
        ]);
    }

    public function lines(Request $request, $hub_id)
    {
        $user = User::find($request->get('user_id'));
        $lignes = User\UserLigne::where('user_hub_id', $hub_id)->get();

        $data = [];

        foreach ($lignes as $ligne) {
            $stations = [];
            $coordinates = [];

            foreach ($ligne->ligne->stations()->where('gares_id', '!=', $ligne->ligne->station_start_id)->where('gares_id', '!=', $ligne->ligne->station_end_id)->get() as $station) {
                $stations[] = [
                    "name" => $station->gare->name,
                    "latitude" => $station->gare->latitude,
                    "longitude" => $station->gare->longitude,
                ];
            }

            foreach ($ligne->ligne->stations as $stationc) {
                $coordinates[] = [
                    $stationc->gare->latitude,
                    $stationc->gare->longitude,
                ];
            }

            $data[] = [
                "id" => $ligne->id,
                "date_achat" => $ligne->date_achat_format,
                "quai" => $ligne->quai,
                "distance" => $ligne->ligne->distance_format,
                "type" => $ligne->ligne->type_ligne,
                "type_ligne_format" => [
                    "name" => $ligne->ligne->type_ligne,
                    "logo" => $ligne->ligne->type_ligne_logo
                ],
                "timestamp" => [
                    "time" => $ligne->ligne->time_min,
                    "format" => $ligne->ligne->time_format
                ],
                "arrival" => [
                    "name" => $ligne->ligne->stationEnd->name,
                    "latitude" => $ligne->ligne->stationEnd->latitude,
                    "longitude" => $ligne->ligne->stationEnd->longitude,
                    "ligne" => $ligne->ligne
                ],
                "composition" => [
                    "name" => $ligne->user_engine->engine->name,
                    "slug" => $ligne->user_engine->engine->slug,
                    "nb_wagon" => $ligne->user_engine->engine->technical->nb_wagon,
                    "nb_pasenger" => $ligne->user_engine->engine->nb_passager
                ],
                "hub" => [
                    "name" => $ligne->userHub->hub->gare->name,
                    "latitude" => $ligne->userHub->hub->gare->latitude,
                    "longitude" => $ligne->userHub->hub->gare->longitude,
                    "nb_slot_commerce" => $ligne->userHub->hub->nb_slot_commerce,
                    "nb_slot_pub" => $ligne->userHub->hub->nb_slot_pub,
                    "nb_slot_parking" => $ligne->userHub->hub->nb_slot_parking,
                    "freq_actual" => $ligne->userHub->hub->calcFreqActual(),
                    "status" => $ligne->userHub->hub->getStatus()
                ],
                "tarifs" => $ligne->tarifs,
                "stations" => $stations,
                "coordinates" => $coordinates,
                "next_departure" => $ligne->plannings()->where('date_depart', '>=', Carbon::now())->orderBy('date_depart', 'asc')->count() > 0 ? $ligne->plannings()->where('date_depart', '>=', Carbon::now())->orderBy('date_depart', 'asc')->first()->date_depart->format('d/m/Y à H:i') : "Non Planifier",
            ];
        }

        return response()->json([
            "lignes" => $data
        ]);
    }

    public function line(Request $request, $hub_id, $line_id)
    {
        $user = User::find($request->get('user_id'));
        $ligne = $user->lignes()->with('ligne')->find($line_id);

        return response()->json([
            "ligne" => $ligne
        ]);
    }

    public function lineSell(Request $request, $hub_id, $line_id)
    {
        $user = User::find($request->get('user_id'));
        $ligne = $user->lignes()->with('ligne')->find($line_id);

        Mouvement::adding(
            'revenue',
            'Vente de la ligne: '.$ligne->ligne->name,
            'vente_ligne',
            $request->get('amount'),
            $user->company->id,
            $ligne->user_hub_id,
        );

        $faker = \Faker\Factory::create('fr_FR');
        \Log::info("Vente de la ligne: ".$ligne->ligne->name." par ".$user->name." pour ".$request->get('amount')."€");
        $user->messages()->create([
            "from" => User\Mailbox::generateFrom('vente_ligne', $ligne),
            "subject" => "Confirmation de vente - Ligne ".\Str::upper($ligne->ligne->type_ligne)." ".$ligne->ligne->name,
            "message" => view('game.includes.mailbox.selling_ligne', ['ligne' => $ligne, 'name_responsable' => $faker->name, 'amount' => $request->get('amount')]),
            "important" => true,
            "user_id" => $user->id,
        ]);

        $ligne->delete();

        return response()->json([
            "success" => true
        ]);
    }

    public function hubSell(Request $request, $user_hub_id)
    {
        $user = User::find($request->get('user_id'));
        $hub = $user->hubs()->with('hub')->find($user_hub_id);

        Mouvement::adding(
            'revenue',
            'Vente du hub: '.$hub->hub->gare->name,
            'vente_hub',
            $request->get('amount'),
            $user->company->id,
        );

        foreach ($hub->lignes as $ligne) {
            Mouvement::adding(
                'revenue',
                'Vente de la ligne: '.$ligne->ligne->name,
                'vente_ligne',
                $request->get('amount'),
                $user->company->id,
                $ligne->user_hub_id,
            );

            \Log::info("Vente de la ligne: ".$ligne->ligne->name." par ".$user->name." pour ".$request->get('amount')."€");

            $ligne->delete();
        }

        $faker = \Faker\Factory::create('fr_FR');
        \Log::info("Vente du hub: ".$hub->hub->gare->name." par ".$user->name." pour ".$request->get('amount')."€");
        $user->messages()->create([
            "from" => User\Mailbox::generateFrom('vente_hub', $hub),
            "subject" => "Confirmation de vente - Hub ".$hub->hub->gare->name,
            "message" => view('game.includes.mailbox.selling_hub', ['hub' => $hub, 'name_responsable' => $faker->name, 'amount' => $request->get('amount')]),
            "important" => true,
            "user_id" => $user->id,
        ]);

        $hub->delete();

        return response()->json([
            "success" => true
        ]);
    }

    public function plannings(Request $request, $user_hub_id)
    {
        $user_hub = UserHub::find($user_hub_id);
        if($request->has('date')) {
            $date = Carbon::createFromTimestamp(strtotime($request->get('date')));
            $diffInDay = $date->startOfDay()->diffInDays(now()->startOfDay());
            $plannings = $user_hub
                ->plannings()
                ->whereBetween('date_depart', [now()->addDays($diffInDay), $date->endOfDay()])
                ->get();
        } else {
            $plannings = User\UserPlanning::where('user_hub_id', $user_hub_id)->get();
        }

        return response()->json([
            "plannings" => $plannings
        ]);
    }
}
