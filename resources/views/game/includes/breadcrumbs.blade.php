@unless($breadcrumbs->isEmpty())
    <!--begin::Breadcrumb-->
    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
        @foreach($breadcrumbs as $breadcrumb)
            @if(!is_null($breadcrumb->url) && !$loop->last)
                <li class="breadcrumb-item">
                    <a href="{{ $breadcrumb->url }}" class="text-white text-hover-primary">{{ $breadcrumb->title }}</a>
                </li>
                <!--begin::Item-->
                <li class="breadcrumb-item">
                    <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
                </li>
                <!--end::Item-->
            @else
                <li class="breadcrumb-item text-white fw-bold lh-1">Dashboards</li>
            @endif
        @endforeach
    </ul>
    <!--end::Breadcrumb-->
@endunless
