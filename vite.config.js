import { defineConfig,splitVendorChunkPlugin  } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/js/app.js',
                'resources/js/sensor.js',
                'resources/sass/custom.scss',
            ],
            refresh: true,
        }),
        splitVendorChunkPlugin()
    ],

});
