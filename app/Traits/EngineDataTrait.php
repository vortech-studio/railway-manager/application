<?php

namespace App\Traits;

trait EngineDataTrait
{
    public static function getDataTypeTrain()
    {
        $type = [];
        $type[] = ["id" => "ter", "value" => "Train TER"];
        $type[] = ["id" => "tgv", "value" => "TGV INOUI"];
        $type[] = ["id" => "ic", "value" => "Train TER Intercités"];
        $type[] = ["id" => "other", "value" => "Autre"];

        return $type;
    }

    public static function getDataTypeEngine()
    {
        $type = [];
        $type[] = ["id" => "motrice", "value" => "Motrice"];
        $type[] = ["id" => "remorque", "value" => "Remorque / Voiture"];
        $type[] = ["id" => "automotrice", "value" => "Automotrice"];

        return $type;
    }

    public static function getDataTypeEnergy()
    {
        $type = [];
        $type[] = ["id" => "diesel", "value" => "Diesel"];
        $type[] = ["id" => "electrique", "value" => "Electrique"];
        $type[] = ["id" => "hybride", "value" => "Hybride"];
        $type[] = ["id" => "vapeur", "value" => "Vapeur"];

        return $type;
    }

    public static function getDataTypeEssieu()
    {
        $type = [];
        $type[] = ["id" => "BB", "value" => "BB"];
        $type[] = ["id" => "BOBO", "value" => "BOBO"];
        $type[] = ["id" => "CC", "value" => "CC"];
        $type[] = ["id" => "COCO", "value" => "COCO"];

        return $type;
    }

    public static function getDataTypeMotor()
    {
        $type = [];
        $type[] = ["id" => "diesel", "value" => "Diesel"];
        $type[] = ["id" => "electrique 1500V", "value" => "Electrique 1500V"];
        $type[] = ["id" => "electrique 25000V", "value" => "Electrique 25000V"];
        $type[] = ["id" => "electrique 1500V/25000V", "value" => "Electrique 1500V/25000V"];
        $type[] = ["id" => "vapeur", "value" => "Vapeur"];
        $type[] = ["id" => "hybride", "value" => "Hybride"];
        $type[] = ["id" => "autre", "value" => "Autre"];

        return $type;
    }

    public static function getDataTypeMarchandise()
    {
        $type = [];
        $type[] = ["id" => "none", "value" => "Aucun"];
        $type[] = ["id" => "passagers", "value" => "Passagers"];
        $type[] = ["id" => "marchandises", "value" => "Marchandises"];

        return $type;
    }
}
