<?php

namespace App\Traits;

use App\Models\Core\TypeIncident;
use App\Models\User\UserEngine;
use App\Models\User\UserIncident;
use App\Models\User\UserLigne;
use function PHPUnit\Framework\matches;

trait UserIncidentTrait
{
    public function startMajorEventJob(UserIncident $incident)
    {
        $min = rand(2, 15);

    }

    public static function getDesignationFromTypeIncident(string $type): string
    {
        return match ($type) {
            "Collision contre un animal" => "Un animal à traversée la voie",
            "Collision contre un obstacle" => "Un obstacle à géner le passage d'un train sur la ligne",
            "Dépassement de vitesse autorisée" => "Un train à dépassé la vitesse autorisée",
            "Franchissement d'un signal carré" => "Un train à franchi un signal carré sur la ligne",
            "Déraillement" => "Un train à dérailler sur la ligne",
            "Pantographes arrachés" => "Un train à arraché un pantographe",
            "Pantographe sortie de caténaire" => "Le pantographe d'un train est sortie de la caténaire provoquant sont arret sur la ligne",
            "Problème avec les feux de position", "Attelage inopérant ou défectueux", "Bloc de freinage défectueux", "Bloc commun défectueux", "Bogie moteur défectueux", "Bogie secondaire en défaillance", "Compresseur défectueux", "Alimentation des lumières en voiture défectueuse", "Ventilation en voiture défectueuse", "Chauffage en voiture défectueuse" => "Problème matériel lors de la préparation du train",
            "Incendie à bord du train" => "Un incendie c'est déclarée dans un train naviguant sur la ligne",
            "Déformation de la voie" => "Une déformation de la voie à été aperçu sur la ligne",
            "Défaillance d'un aiguillage" => "Un aiguillage defectueux à été déclarer sur la ligne",
            "Défaillance d'un signal carré" => "Un signal carré défectueux à été déclaré sur la ligne",
            "Panne générale de courant" => "Une panne de courant sur le secteur à été déclarer.",
            "Caténaire arrachée, défectueuse ou sortie de son logement" => "Des techniciens du service réseau ont reprérer un caténaire défectueux sur le tronçon de la ligne",
            "Défaillance d'un passage à niveau" => "Un passage à niveau défectueux à été déclaré sur la ligne",
            "Défaillance d'un système de sécurité (ex: TIV, KVB, etc...)" => "Un système de sécurité défectueux à été déclaré sur la ligne",
            "Défaillance d'un système de signalisation (ex: BAL, BAPR, etc...)" => "Un système de signalisation défectueux à été déclaré sur la ligne",
            "Défaillance d'un système de contrôle de vitesse (ex: TVM, ETCS, etc...)" => "Un système de contrôle de vitesse défectueux à été déclaré sur la ligne",
            "Accident de personne" => "Un accident impliquant une ou plusieurs personne impact le traffic de la ligne",
            "Agression à bord du train" => "Une aggression à été déclarer à bord d'un train naviguant sur la ligne",
            "Activation abusive du signal d'alarme" => "Un signal d'alarme à bord d'un train à été déclanché abusivement impactant le traffic de la ligne",
            "Oubli d'ouverture / fermeture des portes" => "Un opérateur à oublie d'ouverture ou de fermeture de portes sur la ligne",
            default => "Incident inconnu"
        };
    }

    public static function getAmountReparationFromTypeIncident(UserLigne $ligne, TypeIncident $type)
    {
        return match ($type->severity) {
            1 => ($ligne->ligne->distance * $type->severity) / rand(2, 3),
            2 => ($ligne->ligne->distance * $type->severity) / rand(3, 6),
            3 => ($ligne->ligne->distance * $type->severity) / rand(6, 10)
        };

    }

    public static function getCompositionFromTypeIncident(UserEngine $engine, TypeIncident $type)
    {
        if($type->origine == 'materials') {
            return match ($type->designation) {
                "Collision contre un animal", "Collision contre un obstacle", "Déraillement" => $engine->engine->compositions()->where('name', "LIKE", "%Infrastructure de caisse%")->orWhere('name', "LIKE", "%Cabine%")->first()->id ?? null,
                "Pantographes arrachés" => $engine->engine->compositions()
                    ->where('name', "LIKE", "%Ligne de toiture%")
                    ->orWhere('name', "LIKE", "%Pantographe%")
                    ->orWhere('name', "LIKE", "%Liaison haute tension%")
                    ->first()->id ?? null,
                "Pantographe sortie de caténaire" => $engine->engine->compositions()
                    ->where('name', "LIKE", "%Pantographe%")
                    ->first()->id ?? null,
                "Problème avec les feux de position" => $engine->engine->compositions()
                    ->where('name', "LIKE", "%Eclairage%")
                    ->orWhere('name', "LIKE", "%Système de signalisation%")
                    ->first()->id ?? null,
                "Attelage inopérant ou défectueux" => $engine->engine->compositions()
                    ->where('name', "LIKE", "%Attelage%")
                    ->first()->id ?? null,
                "Bloc de freinage défectueux" => $engine->engine->compositions()
                    ->where('name', "LIKE", "%Système de freinage%")
                    ->first()->id ?? null,
                "Bloc commun défectueux" => $engine->engine->compositions()
                    ->where('name', "LIKE", "%Bloc électrique centrale%")
                    ->first()->id ?? null,
                "Bogie moteur défectueux" => $engine->engine->compositions()
                    ->where('name', "LIKE", "%Moteur%")
                    ->first()->id ?? null,
                "Bogie secondaire en défaillance" => $engine->engine->compositions()
                    ->where('name', "LIKE", "%Bloc auxiliaire%")
                    ->first()->id ?? null,
                "Compresseur défectueux" => $engine->engine->compositions()
                    ->where('name', "LIKE", "%Convertisseur%")
                    ->first()->id ?? null,
                "Alimentation des lumières en voiture défectueuse" => $engine->engine->compositions()
                    ->where('name', "LIKE", "%Luminaire%")
                    ->first()->id ?? null,
                "Ventilation en voiture défectueuse" => $engine->engine->compositions()
                ->where('name', "LIKE", "%Système de ventilation%")
                ->first()->id ?? null,
                "Chauffage en voiture défectueuse" => $engine->engine->compositions()
                ->where('name', "LIKE", "%Chauffage%")
                ->first()->id ?? null,
                default => null
            };
        }
    }
}
