<!--begin::Toolbar-->
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
    <!--begin::Toolbar container-->
    <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                {!! $title !!}</h1>
            <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1"></ul>
        </div>
        <!--end::Page title-->
        <!--begin::Actions-->
        @if(isset($action) && $action)
        <div class="d-flex align-items-center gap-2 gap-lg-3">
            <!--begin::Secondary button-->
            @foreach($data as $item)
                <a href="{{ $item['url'] }}" class="btn btn-sm fw-bold btn-light-{{ $item['class'] }}">{!! $item['title'] !!}</a>
            @endforeach
        </div>
        <!--end::Actions-->
        @endif
    </div>
    <!--end::Toolbar container-->
</div>
<!--end::Toolbar-->
