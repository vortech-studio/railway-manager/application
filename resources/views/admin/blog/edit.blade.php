@extends("admin.template")

@section("css")

@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Edition de l'article: ".$article->titre,
        'action' => true,
        'data' => [
            [
                'url' => back()->getTargetUrl(),
                'title' => "<i class='fa-solid fa-arrow-left me-2'></i> Retour",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <form action="{{ route('admin.blog.update', $article->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method("PUT")
        <div class="card card-flush shadow-lg">
            <div class="card-body">
                <x-form.input
                    name="title"
                    label="Titre de l'annonce"
                    required="true"
                    value="{{ $article->titre }}"/>
                <x-form.select
                    name="type_article"
                    label="Type d'annonce"
                    :datas="\App\Models\Core\Blog\Articles::getDataTypeAnnonce()"
                    value="{{ $article->type_article }}" />

                <x-form.textarea
                    name="content_annonce"
                    label="Description de l'annonce"
                    required="true"
                    value="{!! $article->content !!}" />
            </div>
            <div class="card-footer d-flex flex-end">
                <x-form.button />
            </div>
        </div>
    </form>
@endsection

@section("js")
    <script type="text/javascript">

    </script>
@endsection
