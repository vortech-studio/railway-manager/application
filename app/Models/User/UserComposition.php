<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserComposition extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function hub()
    {
        return $this->belongsTo(UserHub::class);
    }

    public function ligne()
    {
        return $this->belongsTo(UserLigne::class);
    }

    public function tracks()
    {
        return $this->hasMany(UserCompositionTrack::class);
    }

    public function livraisons(): HasMany
    {
        return $this->hasMany(UserDelivery::class);
    }
}
