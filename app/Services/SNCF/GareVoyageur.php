<?php

namespace App\Services\SNCF;

class GareVoyageur
{
    public function allGares()
    {
        $file = \Storage::disk('public')->get('referentiel-gares-voyageurs.json');

        return collect(json_decode($file))->where('dtfinval', null);
    }

    public function searchGare(string $nameGare)
    {
        return $this->allGares()->where('gare_alias_libelle_noncontraint', $nameGare)->first();
    }

    public function countGares()
    {
        return count($this->allGares()->toArray());
    }

    public function frequentationByGare(object $gare): string
    {
        try {
            $file = \Storage::disk('public')->get('frequentation-gares.json');
            $frequentation = collect(json_decode($file))->where('nom_gare', $gare->gare_alias_libelle_noncontraint)->first();

            return $frequentation->total_voyageurs_2021 ?? match ($gare->segmentdrg_libelle) {
                default => rand(150, 1500),
                'b' => rand(1500, 50000),
                'a' => rand(50000, 5000000)
            };
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function setFrequentation(string $type_gare)
    {
        return match($type_gare) {
            'a' => rand(50000, 5000000),
            'b' => rand(1500, 50000),
            default => rand(150, 1500)
        };
    }
}
