<p>Cher PDG,</p>
<p>
    Un nouveau porte-carte est disponible à la récupération.<br>
    Ce porte-carte de type <strong>économique</strong> vous octroie les avantages suivants :
</p>
<div class="m-10 rounded-2 bg-gray-500 p-5">
    <div class="row">
        @foreach($card->cards()->get() as $card)
            <div class="col">
                <div class="d-flex flex-column justify-content-center align-items-center bg-gray-600 border-2 border-black rounded-2 p-10 w-250px h-auto">
                    <img src="/storage/icons/{{ $card->cardHolder->type_avantage }}.png" alt="" class="w-50px">
                    <p class="text-center">{{ $card->cardHolder->name }}</p>
                </div>
            </div>
        @endforeach
    </div>
</div>
