<!--begin::Header wrapper-->
<div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1" id="kt_app_header_wrapper">
    <!--begin::Menu wrapper-->
    <div class="app-header-menu app-header-mobile-drawer align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="app-header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="250px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_header_menu_toggle" data-kt-swapper="true" data-kt-swapper-mode="{default: 'append', lg: 'prepend'}" data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}" data-intro="Voici le menu principal du jeux, il vous permettera de naviguer entre les différentes interfaces afin de gérer votre entreprise.">
        <!--begin::Menu-->
        <div class="menu menu-rounded menu-active-bg menu-state-primary menu-column menu-lg-row menu-title-gray-700 menu-icon-gray-500 menu-arrow-gray-500 menu-bullet-gray-500 my-5 my-lg-0 align-items-stretch fw-semibold px-2 px-lg-0" id="kt_app_header_menu" data-kt-menu="true">
            <!--begin:Menu item-->
            <div class="menu-item me-10">
                <a href="{{ route('home') }}" class="menu-link {{ Route::is(["home"]) ? 'active' : '' }}" data-bs-toggle="tooltip" title="Acceuil">
                    <span class="menu-icon">
						<img src="{{ asset('/storage/icons/home.png') }}" alt="Acceuil" class="w-35px h-35px"/>
					</span>
                </a>
            </div>
            <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start" class="menu-item {{ route_is(['home']) ? 'here show menu-here-bg' : '' }} menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2">
                <span class="menu-link" data-bs-toggle="tooltip" title="Gestion du réseau">
                    <span class="menu-icon">
						<img src="{{ asset('/storage/icons/network.png') }}" alt="Réseau" class="w-35px h-35px"/>
					</span>
				</span>
                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-300px">
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('planning.editing') }}" class="menu-link {{ route_is(['planning.editing.*']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/journey.png') }}" alt="Planification Trajet" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Planifier les trajets</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('planning.index') }}" class="menu-link {{ route_is(['planning.*']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/planning.png') }}" alt="Planification Général" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Planning Général</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('hub.checkout') }}" class="menu-link {{ route_is(['hub.checkout']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/hub_checkout.png') }}" alt="Achat Hub" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Acheter un hub</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('ligne.checkout') }}" class="menu-link {{ route_is(['ligne.checkout']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/mouvement/achat_ligne.png') }}" alt="Achat Ligne" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Acheter une ligne</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('network') }}" class="menu-link {{ route_is(['network.*']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/network.png') }}" alt="Réseau" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Mon réseau</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start" class="menu-item {{ route_is(['home']) ? 'here show menu-here-bg' : '' }} menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2">
                <span class="menu-link" data-bs-toggle="tooltip" title="Gestion de la flotte">
                    <span class="menu-icon">
						<img src="{{ asset('/storage/icons/train.png') }}" alt="Flotte" class="w-35px h-35px"/>
					</span>
				</span>
                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-300px">
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('engine.buy.index') }}" class="menu-link {{ route_is(['engine.buy.*']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/mouvement/achat_materiel.png') }}" alt="Achat Engin" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Acheter un matériels</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('engine.maintenance.book') }}" class="menu-link {{ route_is(['home']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/technician.png') }}" alt="Entretien" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Carnet d'entretien</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('engine.index') }}" class="menu-link {{ route_is(['engine.index', 'engine.show']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/train.png') }}" alt="Flotte" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Ma Flotte</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start" class="menu-item {{ route_is(['home']) ? 'here show menu-here-bg' : '' }} menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2">
                <span class="menu-link">
                    <span class="menu-icon" data-bs-toggle="tooltip" title="Gestion de la companie">
						<img src="{{ asset('/storage/icons/hq.png') }}" alt="Gestion companie" class="w-35px h-35px"/>
					</span>
				</span>
                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-300px">
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('compagnie.customise.index') }}" class="menu-link {{ request()->routeIs('compagnie.customise.index') ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/adjust.png') }}" alt="Personnalisation" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Personnalisation</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('compagnie.profil.index') }}" class="menu-link {{ request()->routeIs('compagnie.profil.index') ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/identity.png') }}" alt="Profil" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Profil</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('compagnie.index') }}" class="menu-link {{ request()->routeIs('compagnie.index') ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/hq.png') }}" alt="Ma Companie" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Ma Companie</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="{{ route('compagnie.bonus.index') }}" class="menu-link {{ request()->routeIs('compagnie.bonus.index') ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/bonus.png') }}" alt="" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Connexion quotidienne</span>
                            </span>
                        </a>
                    </div>
                    <!--<div class="menu-item p-0 m-0">
                        <a href="#" class="menu-link {{ route_is(['home']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/ranking.png') }}" alt="Classement" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Classement Général</span>
                            </span>
                        </a>
                    </div>-->
                </div>
            </div>
            <div class="menu-item me-10">
                <a href="{{ route('research.index') }}" class="menu-link {{ request()->routeIs('research.index') ? 'active' : '' }}" data-bs-toggle="tooltip" title="R&D">
                    <span class="menu-icon">
						<img src="{{ asset('/storage/icons/research.png') }}" alt="Recherche" class="w-35px h-35px"/>
					</span>
                </a>
            </div>
            <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start" class="menu-item {{ route_is(['home']) ? 'here show menu-here-bg' : '' }} menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2">
                <span class="menu-link" data-bs-toggle="tooltip" title="Finances">
                    <span class="menu-icon">
						<img src="{{ asset('/storage/icons/financial.png') }}" alt="Finance" class="w-35px h-35px"/>
					</span>
				</span>
                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-300px">
                    <div class="menu-item p-0 m-0">
                        <a href="#" class="menu-link {{ route_is(['home']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/bank.png') }}" alt="banque" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Banques</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="#" class="menu-link {{ route_is(['home']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/accounting.png') }}" alt="Comptabilité" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Comptabilité Générale</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="#" class="menu-link {{ route_is(['home']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/financial.png') }}" alt="Finance" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Mes Finances</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start" class="menu-item {{ route_is(['home']) ? 'here show menu-here-bg' : '' }} menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2">
                <span class="menu-link" data-bs-toggle="tooltip" title="Marketing">
                    <span class="menu-icon">
						<img src="{{ asset('/storage/icons/marketing.png') }}" alt="Marketing" class="w-35px h-35px"/>
					</span>
				</span>
                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-300px">
                    <div class="menu-item p-0 m-0">
                        <a href="#" class="menu-link {{ route_is(['home']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/audit.png') }}" alt="Audit" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Audit Interne</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="#" class="menu-link {{ route_is(['home']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/accounting.png') }}" alt="Tarifs" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Gestion des tarifs</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="#" class="menu-link {{ route_is(['home']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/marketing.png') }}" alt="marketing" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Mon Marketing</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start" class="menu-item {{ route_is(['home']) ? 'here show menu-here-bg' : '' }} menu-lg-down-accordion menu-sub-lg-down-indention me-0 me-lg-2">
                <span class="menu-link" data-bs-toggle="tooltip" title="Gestion de Masse">
                    <span class="menu-icon">
						<img src="{{ asset('/storage/icons/masstool.png') }}" alt="Mass Tool" class="w-35px h-35px"/>
					</span>
				</span>
                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-300px">
                    <div class="menu-item p-0 m-0">
                        <a href="#" class="menu-link {{ route_is(['home']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/accounting.png') }}" alt="Tarification" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Tarification</span>
                            </span>
                        </a>
                    </div>
                    <div class="menu-item p-0 m-0">
                        <a href="#" class="menu-link {{ route_is(['home']) ? 'active' : '' }}">
						<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
							<img src="{{ asset('/storage/icons/masstool.png') }}" alt="Structure" class="w-40px h-40px"/>
						</span>
                            <span class="d-flex flex-column">
                                <span class="fs-6 fw-bold text-gray-800">Restructuration</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Menu-->
    </div>
    <!--end::Menu wrapper-->
    <!--begin::Navbar-->
    <div class="app-navbar flex-shrink-0">
        <!--begin::Notifications-->
        <div class="app-navbar-item ms-1 ms-lg-5" data-intro="Cette icone rassemble les notifications que vous recevrez pendant votre session, n'hésitez pas à cliquer dessus lorsque l'icone clignote, une notification est ajouter !">
            <!--begin::Menu- wrapper-->
            <div class="btn btn-icon btn-custom btn-color-gray-600 btn-active-light btn-active-color-primary w-35px h-35px w-md-40px h-md-40px" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-attach="parent" data-kt-menu-placement="bottom">
                <i class="fa-regular fa-bell fs-1"></i>
            </div>
            <!--begin::Menu-->
            <div class="menu menu-sub menu-sub-dropdown menu-column w-350px w-lg-375px" data-kt-menu="true" id="kt_menu_notifications">
                <!--begin::Heading-->
                <div class="d-flex flex-column bgi-no-repeat rounded-top" style="background-image:url('/assets/media/misc/menu-header-bg.jpg')">
                    <!--begin::Title-->
                    <h3 class="text-white fw-semibold px-9 mt-10 mb-6">Notifications
                        <span class="fs-8 opacity-75 ps-3">{{ auth()->user()->unreadNotifications()->count() }} {{ Str::plural('nouvelle', auth()->user()->unreadNotifications()->count()) }} {{ Str::plural('notification', auth()->user()->unreadNotifications()->count()) }}</span></h3>
                    <!--end::Title-->
                </div>
                <!--end::Heading-->
                <!--begin::Items-->
                <div class="scroll-y mh-325px my-5 px-8">
                    <!--begin::Item-->
                    @if(auth()->user()->unreadNotifications()->count() == 0)
                        <div class="d-flex flex-row justify-content-center py-4">
                            <i class="fa-solid fa-triangle-exclamation fs-2 text-warning me-4"></i>
                            <span class="fs-6 fw-bold text-gray-800">Aucune notification</span>
                        </div>
                    @else
                        @foreach(auth()->user()->unreadNotifications as $notification)
                            <div class="d-flex flex-stack py-4">
                                <!--begin::Section-->
                                <div class="d-flex align-items-center">
                                    <!--begin::Symbol-->
                                    <div class="symbol symbol-35px me-4">
									    <img src="{{ $notification->data['icon'] }}" alt="notifImage" class="w-35px" />
                                    </div>
                                    <!--end::Symbol-->
                                    <!--begin::Title-->
                                    <div class="mb-0 me-2">
                                        <a href="#" class="fs-6 text-gray-800 text-hover-primary fw-bold">{{ $notification->data['title'] }}</a>
                                        <div class="text-gray-400 fs-7">{{ $notification->data['description'] }}</div>
                                    </div>
                                    <!--end::Title-->
                                </div>
                                <!--end::Section-->
                                <!--begin::Label-->
                                <span class="badge badge-light fs-8" data-ago="{{ strtotime($notification->data['time']) }}">-</span>
                                <!--end::Label-->
                            </div>
                        @endforeach
                    @endif

                    <!--end::Item-->
                </div>
                <!--end::Items-->
                <!--begin::View more-->
                <div class="py-3 text-center border-top">
                    <a href="{{ route('account.notifications') }}" class="btn btn-color-gray-600 btn-active-color-primary">Voir les notifications
                        <i class="ki-outline ki-arrow-right fs-5"></i></a>
                </div>
                <!--end::View more-->
            </div>
            <!--end::Menu-->
            <!--end::Menu wrapper-->
        </div>
        @if(auth()->user()->livraisons()->where('end_at', '>=', now())->count() != 0)
        <div class="app-navbar-item ms-1 ms-lg-5">
            <!--begin::Menu- wrapper-->
            <div class="btn btn-icon btn-custom btn-color-gray-600 btn-active-light btn-active-color-primary w-35px h-35px w-md-40px h-md-40px" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-attach="parent" data-kt-menu-placement="bottom">
                <i class="fa-solid fa-truck fs-1"></i>
            </div>
            <!--begin::Menu-->
            <div class="menu menu-sub menu-sub-dropdown menu-column w-350px w-lg-375px" data-kt-menu="true" id="kt_menu_notifications">
                <!--begin::Heading-->
                <div class="d-flex flex-column bgi-no-repeat rounded-top" style="background-image:url('/assets/media/misc/menu-header-bg.jpg')">
                    <!--begin::Title-->
                    <h3 class="text-white fw-semibold px-9 mt-10 mb-6">Livraison
                        <span class="fs-8 opacity-75 ps-3">{{ auth()->user()->livraisons()->where('end_at', '>=', now())->count() }} {{ Str::plural('livraison', auth()->user()->livraisons()->where('end_at', '>=', now())->count()) }} en cours</span></h3>
                    <!--end::Title-->
                </div>
                <!--end::Heading-->
                <!--begin::Items-->
                <div class="scroll-y mh-325px my-5 px-8">
                    <!--begin::Item-->
                    @foreach(auth()->user()->livraisons()->where('end_at', '>=', now())->get() as $livraison)
                        <div class="d-flex flex-row justify-content-between align-items-center py-4">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-45px me-4">
                                    <i class="fa-solid fa-truck fs-1 text-primary"></i>
                                    <span class="symbol-badge badge badge-circle bg-light-warning top-100 start-100">
                                        <i class="fa-solid fa-clock text-warning fs-9"></i>
                                    </span>
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fs-6 text-black">{{ $livraison->designation }}</div>
                                    <div class="d-flex align-items-center flex-column mt-3 w-100">
                                        <div class="d-flex justify-content-between fw-bold fs-6 text-black opacity-75 w-100 mt-auto mb-2">
                                            <span>{{ $livraison->end_at->format("H:i") }}</span>
                                            <span data-ago="{{ $livraison->end_at->timestamp }}"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <!--end::Item-->
                </div>
                <!--end::Items-->
            </div>
            <!--end::Menu-->
            <!--end::Menu wrapper-->
        </div>
        @endif
        <!--end::Notifications-->
        <div class="app-navbar-item ms-1 ms-lg-5" data-intro="Si vous avez des questions ou des idées d'améliorations sur le jeux, n'hésitez pas à cliquer sur ce bouton, une aide précieuse est toujours la bienvenue !">
            <a href="https://vortechstudio.atlassian.net/servicedesk/customer/portal/3" target="_blank" class="btn btn-icon btn-custom btn-color-gray-600 btn-active-light btn-active-color-primary w-35px h-35px w-md-40px h-md-40px" data-bs-toggle="tooltip" title="Support">
                <i class="fa-regular fa-circle-question fs-1"></i>
            </a>
        </div>
        <div class="app-navbar-item ms-1 ms-lg-5" data-intro="Vous avez des amies, n'hésitez pas également à cliquer sur ce bouton afin de les parrainer !">
            <a href="#" class="btn btn-icon btn-custom btn-color-gray-600 btn-active-light btn-active-color-primary w-35px h-35px w-md-40px h-md-40px" data-bs-toggle="tooltip" title="Parrainage">
                <i class="fa-solid fa-hands-holding-circle fs-1"></i>
            </a>
        </div>
        <div class="app-navbar-item ms-1 ms-lg-5" data-intro="Même si le jeux est déjà très complet tel qu'il est notre équipe de développeur n'à pas hésitez à pousser très loin la gestion d'une firme ferroviaire, si vous êtes comme eux, passer PREMIUM !">
            <a href="{{ route('account.premium') }}" class="btn btn-icon btn-custom btn-color-gray-600 btn-active-light btn-active-color-primary w-35px h-35px w-md-40px h-md-40px" data-bs-toggle="tooltip" title="Premium">
                <img src="{{ asset('/storage/icons/premium.png') }}" alt="Premium" class="w-30px h-30px" />
            </a>
        </div>
        <div class="app-navbar-item ms-1 ms-lg-5" data-intro="Le Porte carte vous permettra d'avoir des avantages en jeux !">
            <a href="{{ route('shop.index') }}" class="btn btn-icon btn-custom btn-color-gray-600 btn-active-light btn-active-color-primary w-35px h-35px w-md-40px h-md-40px" data-bs-toggle="tooltip" title="Porte Carte & Avantages">
                <img src="{{ asset('/storage/icons/gatcha.png') }}" alt="Porte Carte" class="w-30px h-30px" />
            </a>
        </div>
        <!--begin::User menu-->
        <div class="app-navbar-item ms-3 ms-lg-5" id="kt_header_user_menu_toggle">
            <!--begin::Menu wrapper-->
            <div class="cursor-pointer symbol symbol-35px symbol-md-45px" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                <img class="symbol symbol-circle symbol-35px symbol-md-45px" src="{{ auth()->user()->avatar_src }}" alt="user" />
                @if(auth()->user()->messages()->where('read_at', null)->count() != 0)
                    <span class="symbol-badge badge badge-circle bg-success top-100 start-100">{{ auth()->user()->messages()->where('read_at', null)->count() }}</span>
                @endif
            </div>
            <!--begin::User account menu-->
            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px" data-kt-menu="true">
                <!--begin::Menu item-->
                <div class="menu-item px-3">
                    <div class="menu-content d-flex align-items-center px-3">
                        <!--begin::Avatar-->
                        <div class="symbol symbol-50px me-5">
                            <img alt="Logo" src="{{ auth()->user()->avatar_src }}" />
                            @if(auth()->user()->premium)
                                <span class="symbol-badge badge badge-circle bg-danger start-100">
                                    <img src="{{ asset('/storage/icons/premium.png') }}" alt="premium" class="w-20px h-20px" />
                                </span>
                            @endif
                            <span class="symbol-badge badge badge-circle bg-success top-100 start-100">{{ auth()->user()->getLevel() }}</span>
                        </div>
                        <!--end::Avatar-->
                        <!--begin::Username-->
                        <div class="d-flex flex-column">
                            <div class="fw-bold d-flex align-items-center fs-5">{{ auth()->user()->name }}
                                <span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2">{{ auth()->user()->name_company }}</span></div>
                            <a href="#" class="fw-semibold text-muted text-hover-primary fs-7">{{ auth()->user()->email }}</a>
                        </div>
                        <!--end::Username-->
                    </div>
                </div>
                <!--end::Menu item-->
                <!--begin::Menu separator-->
                <div class="separator my-2"></div>
                <!--end::Menu separator-->
                <!--begin::Menu item-->
                <div class="menu-item px-5" data-intro="Gérez votre compte à partir de cette page">
                    <a href="{{ route('account.index') }}" class="menu-link px-5">
                        <span class="menu-icon">
                            <i class="fa-regular fa-user-circle fs-2"></i>
                        </span>
                        Mon comptes
                    </a>
                </div>
                <div class="menu-item px-5" data-intro="La messagerie interne de Railway Manager, vos conseillez, fournisseurs et technicien, vous contacterons certainement par cette boite">
                    <a href="{{ route('mailbox.index') }}" class="menu-link px-5">
                        <span class="menu-icon">
                            <i class="fa-solid fa-envelope fs-2"></i>
                        </span>
                        Ma messagerie
                        @if(auth()->user()->messages()->where('read_at', null)->count() != 0)
                            <span class="menu-badge">
                                <span class="badge badge-light-danger badge-circle fw-bold fs-7">{{ auth()->user()->messages()->where('read_at', null)->count() }}</span>
                            </span>
                        @endif
                    </a>
                </div>
                <div class="menu-item px-5" data-intro="Notre équipe de développeur à également incorporer certains engins roulants qui ont demander beaucoup de travail et de recherche, n'hésitez pas à faire le tour de la boutique et moyennant quelque TPOINT ces engins seront à vous !">
                    <a href="{{ route('shop.index') }}" class="menu-link px-5">
                        <span class="menu-icon">
                            <i class="fa-solid fa-shopping-cart fs-2"></i>
                        </span>
                        Boutique
                    </a>
                </div>
                @if(auth()->user()->admin)
                    <div class="menu-item px-5">
                        <a href="{{ route('admin.dashboard') }}" class="menu-link px-5">
                            <span class="menu-icon">
                                <i class="fa-solid fa-crown fs-2"></i>
                            </span>
                            Administration
                        </a>
                    </div>
                @endif
                <!--end::Menu item-->
                <div class="separator border-gray-500 my-5"></div>
                <!--begin::Menu item-->
                <div class="menu-item px-5">
                    <a href="/logout" class="menu-link px-5">
                        <span class="menu-icon">
                            <i class="fa-solid fa-sign-out fs-2"></i>
                        </span>
                        Deconnexion
                    </a>
                </div>
                <!--end::Menu item-->
            </div>
            <!--end::User account menu-->
            <!--end::Menu wrapper-->
        </div>
        <!--end::User menu-->
        <!--begin::Header menu toggle-->
        <div class="app-navbar-item ms-3 ms-lg-5" data-intro="L'heure c'est très important !">
            <span id="clock-hours" class="fs-1"></span>
            <span class="animation-blink">
                            <span class="animation-blink-clock fs-1">:</span>
                        </span>
            <span id="clock-minutes" class="fs-1"></span>
            <small id="clock-seconds" class="fs-3 ms-1 text-orange-600"></small>
        </div>
        <!--end::Header menu toggle-->
    </div>
    <!--end::Navbar-->
</div>
<!--end::Header wrapper-->
