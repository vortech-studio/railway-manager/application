@extends("game.template")

@section("css")
    <link src="{{ asset('/plugins/leaflet/leaflet.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
    <input type="hidden" name="user_hub_id" value="{{ $planning->user_hub_id }}">
    <input type="hidden" name="planning_id" value="{{ $planning->id }}">
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-5">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3" data-bs-toggle="tooltip" title="{{ $planning->status_format_text }}">
                {!! $planning->status_format_icon !!}
            </div>
            <x-base.underline
                title="Détail du trajet"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.network.partials.menu")
    </div>
    <div class="btn-group w-100 w-lg-50" data-kt-buttons="true" data-kt-buttons-target="[data-kt-button]">
        <!--begin::Radio-->
        <label class="btn btn-outline btn-color-muted btn-active-success active" data-kt-button="true">
            <!--begin::Input-->
            <input class="btn-check" type="radio" name="method" checked="checked" value="ecran"/>
            <!--end::Input-->
            Ecran de contrôle
        </label>
        <!--end::Radio-->

        <!--begin::Radio-->
        <label class="btn btn-outline btn-color-muted btn-active-success" data-kt-button="true">
            <!--begin::Input-->
            <input class="btn-check" type="radio" name="method" value="tableau" />
            <!--end::Input-->
            Tableau Interactif
        </label>
        <!--end::Radio-->
    </div>
    <!--end::Radio group-->
    <div id="methodContainer" class="mb-5">
        <div id="ecranMethod">
            <div class="card card-xl-stretch mb-5 shadow-md">
                @if($planning->status == 'initialized' || $planning->status == 'departure' || $planning->status == 'retarded' || $planning->status == 'canceled')
                    @if($planning->date_depart <= now()->addMinute())
                        @include('game.includes.ecran.eva.ecran_departure_alerte')
                    @else
                        @include('game.includes.ecran.eva.ecran_departure_single')
                    @endif
                @elseif($planning->status == 'travel' || $planning->status == 'in_station')
                    @include('game.includes.ecran.sive.ecran_sive_travel')
                @else
                    @include("game.includes.ecran.sive.ecran_sive_arrival")
                @endif
            </div>
        </div>
        <div id="tableMethod" class="mt-5 d-none">
            <table class="table-responsive table table-striped table-hover table-bordered bg-color-eva text-white table-dark table-rounded rounded-3 fs-1">
                <thead>
                    <tr>
                        <th>Arret</th>
                        <th>Arrivée</th>
                        <th>Etat</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($planning->planning_stations as $station)
                    <tr>
                        <td>{{ $station->ligneStation->gare->name }}</td>
                        <td>{{ $station->arrival_at->format("H:i") }}</td>
                        <td>
                            {!! $station->status_badge_format !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot class="bg-warning">
                    <tr class="bg-warning">
                        <td colspan="3" class="bg-amber-600 fs-italic fs-4">Essai</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="card shadow-lg">
        <div class="card-header">
            <div class="card-title">Rapport sur le trajet {{ $planning->engine->engine->type_train_format }} N° {{ $planning->number_travel }} à destination de {{ $planning->ligne->ligne->stationEnd->name }}</div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="d-flex flex-column mb-5">
                        <x-base.underline
                            title="Détail du trajet"
                            size="3"
                            size-text="fs-1" />
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                            <span>Gare de départ</span>
                            <span>{{ $planning->ligne->ligne->stationStart->name }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                            <span>Heure de départ</span>
                            <span>{{ $planning->date_depart->format("H:i") }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                            <span>Gare d'arrivée</span>
                            <span>{{ $planning->ligne->ligne->stationEnd->name }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                            <span>Heure d'arrivée</span>
                            <span>{{ $planning->date_depart->addMinutes($planning->ligne->ligne->time_min)->format("H:i") }}</span>
                        </div>
                        @if($planning->status == 'travel' && $planning->planning_stations()->where('status', 'init')->count() > 0)
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                <span>Prochaine Gare</span>
                                <span>{{ $planning->planning_stations()->where('status', 'init')->first()->name }}</span>
                            </div>
                        @endif
                        <x-base.underline
                            title="Composition"
                            size="3"
                            size-text="fs-1" />

                        <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                            <span>Nombre de voyageurs</span>
                            <span>{{ $planning->passengers()->sum('nb_passengers') }}</span>
                        </div>

                        @if($planning->status == 'arrival')
                            <x-base.underline
                                title="Résulat Financier"
                                size="3"
                                size-text="fs-1" />
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                <span>Chiffre d'affaire</span>
                                <span>{{ eur($planning->travel->calcCA()) }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 fs-italic ms-10">
                                <span>dont CA Billetterie</span>
                                <span>{{ eur($planning->travel->ca_billetterie) }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 fs-italic ms-10 mb-10">
                                <span>dont revenues auxiliaires</span>
                                <span>{{ eur($planning->travel->ca_other) }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                <span>Coût du trajet</span>
                                <span class="text-red-600">- {{ eur($planning->travel->calcCoast()) }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 fs-italic ms-10">
                                <span>dont Frais de Gasoil</span>
                                <span class="text-red-600">- {{ eur($planning->travel->frais_gasoil) }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center fs-italic ms-10">
                                <span>dont Frais d'electricité</span>
                                <span class="text-red-600">{{ eur($planning->travel->frais_electricite) }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center fs-italic ms-10 mb-20">
                                <span>dont autres charges</span>
                                <span class="text-red-600">{{ eur($planning->travel->frais_autre) }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                <span>Résultat du trajet</span>
                                <span class="fs-3 fw-bold">{{ eur($planning->travel->calcResultat()) }}</span>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col">
                    <x-base.underline
                        title="Message de la ligne"
                        size="3"
                        size-text="fs-1" />
                    <table class="table">
                        <tbody>
                            @foreach($planning->logs as $log)
                            <tr>
                                <td>{{ $log->created_at->format('d/m/Y H:i') }}</td>
                                <td>{{ $log->message }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <x-base.underline
                        title="Incident sur le trajet"
                        size="3"
                        size-text="fs-1" />
                    @if($planning->incidents()->count() == 0)
                    <x-base.alert
                        type="solid"
                        color="success"
                        icon="check-circle"
                        title="Aucun incident"
                        content="Aucun incident n'a été signalé sur ce trajet"
                         />
                    @else
                        @include("game.engine.partials.incident", ["style" => "advanced", "incidents" => $planning->incidents()])
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="https://unpkg.com/@turf/turf@6/turf.min.js"></script>
    <script src="{{ asset('/plugins/leaflet/leaflet.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/AnimatedMarker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/map.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/game/network/travel.js') }}" type="text/javascript"></script>
    <script type="text/javascript">

    </script>
@endsection
