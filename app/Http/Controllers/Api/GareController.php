<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\Gares;
use App\Services\Mapping\Mapbox;
use App\Services\Mapping\OpenRouteService;
use App\Services\SNCF\GareVoyageur;
use Illuminate\Http\Request;

class GareController extends Controller
{
    public function index()
    {
        return response()->json([
            'message' => 'Hello World',
        ]);
    }

    public function search(Request $request)
    {
        return match ($request->get('type')) {
            "coordonnees" => $this->searchCoordoonees($request),
            "infos" => $this->searchInfos($request),
        };
    }

    public function show($gare_id)
    {
        return response()->json([
            'url' => route('admin.gare.show', $gare_id),
        ]);
    }

    public function is_hub($gares_id)
    {
        if(Gares::find($gares_id)->hub()->count() > 0) {
            return response()->json([
                'is_hub' => true,
            ]);
        } else {
            return response()->json();
        }
    }

    public function destroy($gare_id)
    {
        $gare = Gares::find($gare_id);
        $gare->delete();

        return response()->json([
            'message' => 'Gare deleted',
        ]);
    }

    private function searchCoordoonees(Request $request)
    {
        $openRouteService = new OpenRouteService();
        $geocode = $openRouteService->geocodeSearch(null, 'Gare '.formatTextForSlugify($request->get('query')));
        $info = $openRouteService->geocodeMatchInfo(null, 'Gare '.formatTextForSlugify($request->get('query')));
        if(isset($geocode) && isset($info)) {
            return response()->json([
                "status" => "success",
                "data" => [
                    "latitude" => $geocode['lat'],
                    "longitude" => $geocode['lng'],
                    "region" => $info['region'],
                    "pays" => $info['pays'],
                ]
            ]);
        } else {
            $mapbox = new Mapbox();
            $geocode = $mapbox->geocodeSearch(null, formatTextForSlugify($request->get('query')));
            $info = $mapbox->geocodeMatchInfo(null, formatTextForSlugify($request->get('query')));

            return response()->json([
                "status" => "success",
                "data" => [
                    "latitude" => $geocode['lat'],
                    "longitude" => $geocode['lng'],
                    "region" => $info['region'],
                    "pays" => $info['pays'],
                ]
            ]);
        }
    }

    private function searchInfos(Request $request)
    {
        $gareVoyageur = new GareVoyageur();
        $gare = $gareVoyageur->searchGare($request->get('name'));

        return response()->json($gare);
    }
}
