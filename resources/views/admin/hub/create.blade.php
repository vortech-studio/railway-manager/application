@extends("admin.template")

@section("css")

@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Nouveau HUB",
        'action' => false,
        'data' => [
            [
                'url' => route('admin.hub.create'),
                'title' => "Ajouter un nouveau HUB",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="card shadow-lg">
        <form action="{{ route('admin.hub.store') }}" method="post">
            <div class="card-body">

                @csrf
                <label for="gares_id" class="form-label">Gare affilié</label>
                <select name="gares_id" id="gares_id" class="form-control selectpicker" data-live-search="true">
                    <option value="">-- Choisir une Gare --</option>
                    @foreach($gares as $id=>$name)
                        <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
                <span class="text-danger info d-none"><i class="fa-solid fa-exclamation-triangle text-danger me-2"></i> La gare est déjà indiquer comme un hub</span>

            </div>
            <div class="card-footer d-flex flex-end">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>
        </form>
    </div>
@endsection

@section("js")

    <script type="text/javascript">
        document.querySelector('#gares_id').addEventListener('change', e => {
            console.log(e.target.value)
            e.preventDefault()
            fetch(`/api/core/gare/${e.target.value}/is-hub`)
                .then(response => response.json())
                .then(data => {
                    if (data.is_hub) {
                        document.querySelector('.info').classList.remove('d-none')
                    } else {
                        document.querySelector('.info').classList.add('d-none')
                    }
                })
        })
    </script>
@endsection
