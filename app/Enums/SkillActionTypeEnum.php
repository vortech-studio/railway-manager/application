<?php

namespace App\Enums;

enum SkillActionTypeEnum : string
{
    case PERCENT = "percent";
    case UNIT = "unit";
}
