<?php

namespace App\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class ImageInput extends Component
{
    public $name;
    /**
     * @var null
     */
    public $accept;

    /**
     * Create a new component instance.
     */
    public function __construct($name, $accept = null)
    {
        //
        $this->name = $name;
        $this->accept = $accept;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.form.image-input');
    }
}
