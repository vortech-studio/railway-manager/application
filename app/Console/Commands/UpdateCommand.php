<?php

namespace App\Console\Commands;

use App\Models\Core\Engine;
use App\Models\Core\EngineComposition;
use App\Models\Core\EngineTechnical;
use App\Models\Core\GareEquipement;
use App\Models\Core\Gares;
use App\Models\Core\GareTransportation;
use App\Models\Core\Hub;
use App\Models\Core\Ligne;
use App\Models\Core\LigneRequirement;
use App\Models\Core\LigneStation;
use App\Notifications\Admin\SystemUpdateNotification;
use App\Notifications\SlackNotifiable;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Process;
use Illuminate\Support\Facades\Storage;
use function Deployer\error;
use function Laravel\Prompts\info;
use function Laravel\Prompts\intro;
use function Laravel\Prompts\outro;
use function Laravel\Prompts\spin;

class UpdateCommand extends Command
{
    protected $signature = 'update {env?}';

    protected $description = 'Mise à jour des systèmes';

    public function handle(): void
    {
        if (!$this->argument('env')) {
            $option = $this->choice("Système à mettre à jour", ["testing", "production"]);
        } else {
            $option = $this->argument('env');
        }

        (new SlackNotifiable())->notify(new SystemUpdateNotification("Mise à jour du système de $option"));

        if ($option == 'testing') {
            intro("Installation des informations de test");
            info('Installation de la base de donnée général');
            $this->call('migrate:fresh', [
                '--force' => true,
            ]);
            info('Installation de la base pour 2023.1');
            $this->call('migrate', ['--path' => '/database/migrations/2023_1/']);
            info('Initialisation de la base de donnée');
            $this->call('db:seed', [
                "--class" => "DatabaseSeeder",
                "--force" => true,
            ]);

            info('Installation des informations de test');
            $this->call('db:seed', [
                "--class" => "TestSeeder",
                "--force" => true,
            ]);

            spin(function () {
                $this->importGare();
            }, "Importation des Gares");
            spin(function () {
                $this->importHubs();
            }, "Importation des Hubs");
            spin(function () {
                $this->importLignes();
            }, "Importation des Lignes");
            spin(function () {
                $this->importEngines();
            }, "Importation des matériels roulants");

            spin(function () {
                $this->call('log-viewer:clear');
            }, "Processus suplémentaire");

        } else {
            info("Mise à jour du système de production");
            $this->call('migrate');
            info('Installation de la base pour 2023.1');
            $this->call('migrate', ['--path' => '/database/migrations/2023_1/']);
            spin(function () {
                $this->importGare();
            }, "Importation des Gares");
            spin(function () {
                $this->importHubs();
            }, "Importation des Hubs");
            spin(function () {
                $this->importLignes();
            }, "Importation des Lignes");
            spin(function () {
                $this->importEngines();
            }, "Importation des matériels roulants");
        }

        info('Mise à jours des informations dans la base de donnée principal');
        $this->call("db:seed", [
            "--class" => "UpdateSeeder",
            "--force" => true,
        ]);

        outro("Mise à jour termine");
        (new SlackNotifiable())->notify(new SystemUpdateNotification("Mise à jour du système de $option terminé", "success"));
    }

    private function importGare(): void
    {
        $file = fopen(public_path('/storage/gares.json'), 'r');
        $gares = json_decode(fread($file, filesize(public_path('/storage/gares.json'))), true);
        fclose($file);

        foreach ($gares as $gare) {
            if (Gares::find($gare['id'])) {
                Gares::find($gare['id'])->update([
                    "name" => $gare['name'],
                    "code_uic" => $gare['code_uic'],
                    "type_gare" => $gare['type_gare'],
                    "latitude" => $gare['latitude'],
                    "longitude" => $gare['longitude'],
                    "region" => $gare['region'],
                    "pays" => $gare['pays'],
                    "superficie_infra" => $gare['superficie_infra'],
                    "superficie_quai" => $gare['superficie_quai'],
                    "long_quai" => $gare['long_quai'],
                    "nb_quai" => $gare['nb_quai'],
                    "commerce" => $gare['commerce'],
                    "pub" => $gare['pub'],
                    "parking" => $gare['parking'],
                    "frequentation" => $gare['frequentation'],
                    "nb_habitant_city" => $gare['nb_habitant_city'],
                    "time_day_work" => $gare['time_day_work'],
                    "created_at" => $gare['created_at'],
                    "updated_at" => $gare['updated_at'],
                ]);
            } else {
                Gares::create([
                    "id" => $gare['id'],
                    "name" => $gare['name'],
                    "code_uic" => $gare['code_uic'],
                    "type_gare" => $gare['type_gare'],
                    "latitude" => $gare['latitude'],
                    "longitude" => $gare['longitude'],
                    "region" => $gare['region'],
                    "pays" => $gare['pays'],
                    "superficie_infra" => $gare['superficie_infra'],
                    "superficie_quai" => $gare['superficie_quai'],
                    "long_quai" => $gare['long_quai'],
                    "nb_quai" => $gare['nb_quai'],
                    "commerce" => $gare['commerce'],
                    "pub" => $gare['pub'],
                    "parking" => $gare['parking'],
                    "frequentation" => $gare['frequentation'],
                    "nb_habitant_city" => $gare['nb_habitant_city'],
                    "time_day_work" => $gare['time_day_work'],
                    "created_at" => $gare['created_at'],
                    "updated_at" => $gare['updated_at'],
                ]);
            }
        }

        //----------------------------------------------//
        $file = fopen(public_path('/storage/gares_transportation.json'), 'r');
        $transportations = json_decode(fread($file, filesize(public_path('/storage/gares_transportation.json'))), true);
        fclose($file);

        foreach ($transportations as $transportation) {
            if (GareTransportation::find($transportation['id'])) {
                GareTransportation::find($transportation['id'])->update([
                    "type" => $transportation['type'],
                    "gares_id" => $transportation['gares_id'],
                    "created_at" => $transportation['created_at'],
                    "updated_at" => $transportation['updated_at'],
                ]);
            } else {
                GareTransportation::create([
                    "id" => $transportation['id'],
                    "type" => $transportation['type'],
                    "gares_id" => $transportation['gares_id'],
                    "created_at" => $transportation['created_at'],
                    "updated_at" => $transportation['updated_at'],
                ]);
            }
        }

        //---------------------------------------------//
        $file = fopen(public_path('/storage/gares_equipements.json'), 'r');
        $equipements = json_decode(fread($file, filesize(public_path('/storage/gares_equipements.json'))), true);
        fclose($file);

        foreach ($equipements as $equipement) {
            if (GareEquipement::find($equipement['id'])) {
                GareEquipement::find($equipement['id'])->update([
                    "code_uic" => $equipement['code_uic'],
                    "type_equipement" => $equipement['type_equipement'],
                    "gares_id" => $equipement['gares_id'],
                ]);
            } else {
                GareEquipement::create([
                    "id" => $equipement['id'],
                    "code_uic" => $equipement['code_uic'],
                    "type_equipement" => $equipement['type_equipement'],
                    "gares_id" => $equipement['gares_id'],
                ]);
            }
        }
    }

    private function importHubs()
    {
        $file = fopen(public_path('/storage/hubs.json'), 'r');
        $hubs = json_decode(fread($file, filesize(public_path('/storage/hubs.json'))), true);
        fclose($file);

        foreach ($hubs as $hub) {
            if (Hub::find($hub['id'])) {
                Hub::find($hub['id'])->update([
                    "price" => $hub['price'],
                    "taxe_hub" => $hub['taxe_hub'],
                    "passenger_first" => $hub['passenger_first'],
                    "passenger_second" => $hub['passenger_second'],
                    "nb_slot_commerce" => $hub['nb_slot_commerce'],
                    "nb_slot_pub" => $hub['nb_slot_pub'],
                    "nb_slot_parking" => $hub['nb_slot_parking'],
                    "active" => $hub['active'],
                    "created_at" => $hub['created_at'],
                    "updated_at" => $hub['updated_at'],
                    "gares_id" => $hub['gares_id'],
                ]);
            } else {
                Hub::create([
                    "id" => $hub['id'],
                    "price" => $hub['price'],
                    "taxe_hub" => $hub['taxe_hub'],
                    "passenger_first" => $hub['passenger_first'],
                    "passenger_second" => $hub['passenger_second'],
                    "nb_slot_commerce" => $hub['nb_slot_commerce'],
                    "nb_slot_pub" => $hub['nb_slot_pub'],
                    "nb_slot_parking" => $hub['nb_slot_parking'],
                    "active" => $hub['active'],
                    "created_at" => $hub['created_at'],
                    "updated_at" => $hub['updated_at'],
                    "gares_id" => $hub['gares_id'],
                ]);
            }
        }
    }

    private function importLignes()
    {
        $file = fopen(public_path('/storage/lignes.json'), 'r');
        $lignes = json_decode(fread($file, filesize(public_path('/storage/lignes.json'))), true);
        fclose($file);

        foreach ($lignes as $ligne) {
            if (Ligne::find($ligne['id'])) {
                Ligne::find($ligne['id'])->update([
                    "nb_station" => $ligne['nb_station'],
                    "price" => $ligne['price'],
                    "distance" => $ligne['distance'],
                    "time_min" => $ligne['time_min'],
                    "station_start_id" => $ligne['station_start_id'],
                    "station_end_id" => $ligne['station_end_id'],
                    "active" => $ligne['active'],
                    "hub_id" => $ligne['hub_id'],
                    "type_ligne" => $ligne['type_ligne'],
                ]);
            } else {
                Ligne::create([
                    "id" => $ligne['id'],
                    "nb_station" => $ligne['nb_station'],
                    "price" => $ligne['price'],
                    "distance" => $ligne['distance'],
                    "time_min" => $ligne['time_min'],
                    "station_start_id" => $ligne['station_start_id'],
                    "station_end_id" => $ligne['station_end_id'],
                    "active" => $ligne['active'],
                    "hub_id" => $ligne['hub_id'],
                    "type_ligne" => $ligne['type_ligne'],
                ]);
            }
        }

        //---------------------------------------------------------//
        $file = fopen(public_path('/storage/lignes_requirements.json'), 'r');
        $requirements = json_decode(fread($file, filesize(public_path('/storage/lignes_requirements.json'))), true);
        fclose($file);

        foreach ($requirements as $requirement) {
            if (LigneRequirement::find($requirement['id'])) {
                LigneRequirement::find($requirement['id'])->update([
                    "ligne_id" => $requirement['ligne_id'],
                    "hub_id" => $requirement['hub_id'],
                ]);
            } else {
                LigneRequirement::create([
                    "id" => $requirement['id'],
                    "ligne_id" => $requirement['ligne_id'],
                    "hub_id" => $requirement['hub_id'],
                ]);
            }
        }

        //---------------------------------------------------------//
        $file = fopen(public_path('/storage/lignes_stations.json'), 'r');
        $stations = json_decode(fread($file, filesize(public_path('/storage/lignes_stations.json'))), true);
        fclose($file);

        foreach ($stations as $station) {
            if (LigneStation::find($station['id'])) {
                LigneStation::find($station['id'])->update([
                    "time" => $station['time'],
                    "gares_id" => $station['gares_id'],
                    "ligne_id" => $station['ligne_id'],
                ]);
            } else {
                LigneStation::create([
                    "id" => $station['id'],
                    "time" => $station['time'],
                    "gares_id" => $station['gares_id'],
                    "ligne_id" => $station['ligne_id'],
                ]);
            }
        }
    }

    private function importEngines()
    {
        $file = fopen(public_path('/storage/engines.json'), 'r');
        $engines = json_decode(fread($file, filesize(public_path('/storage/engines.json'))), true);
        fclose($file);

        foreach ($engines as $engine) {
            if (Engine::find($engine['id'])) {
                Engine::find($engine['id'])->update([
                    "name" => $engine['name'],
                    "slug" => $engine['slug'],
                    "type_train" => $engine['type_train'],
                    "type_engine" => $engine['type_engine'],
                    "type_energy" => $engine['type_energy'],
                    "velocity" => $engine['velocity'],
                    "nb_passager" => $engine['nb_passager'],
                    "price_achat" => $engine['price_achat'],
                    "price_maintenance" => $engine['price_maintenance'],
                    "duration_maintenance" => $engine['duration_maintenance'],
                    "price_location" => $engine['price_location'],
                    "image" => $engine['image'],
                ]);
            } else {
                Engine::create([
                    "id" => $engine['id'],
                    "name" => $engine['name'],
                    "slug" => $engine['slug'],
                    "type_train" => $engine['type_train'],
                    "type_engine" => $engine['type_engine'],
                    "type_energy" => $engine['type_energy'],
                    "velocity" => $engine['velocity'],
                    "nb_passager" => $engine['nb_passager'],
                    "price_achat" => $engine['price_achat'],
                    "price_maintenance" => $engine['price_maintenance'],
                    "duration_maintenance" => $engine['duration_maintenance'],
                    "price_location" => $engine['price_location'],
                    "image" => $engine['image'],
                ]);
            }
        }

        //-------------------------------------------------//
        $file = fopen(public_path('/storage/engines_technical.json'), 'r');
        $technical = json_decode(fread($file, filesize(public_path('/storage/engines_technical.json'))), true);
        fclose($file);

        foreach ($technical as $engine) {
            if (EngineTechnical::find($engine['id'])) {
                EngineTechnical::find($engine['id'])->update([
                    "longueur" => $engine['longueur'],
                    "largeur" => $engine['largeur'],
                    "hauteur" => $engine['hauteur'],
                    "essieux" => $engine['essieux'],
                    "max_speed" => $engine['max_speed'],
                    "poids" => $engine['poids'],
                    "traction_force" => $engine['traction_force'],
                    "type_motor" => $engine['type_motor'],
                    "power_motor" => $engine['power_motor'],
                    "start_exploi" => $engine['start_exploi'],
                    "end_exploi" => $engine['end_exploi'],
                    "gasoil" => $engine['gasoil'],
                    "type_marchandise" => $engine['type_marchandise'],
                    "nb_type_marchandise" => $engine['nb_type_marchandise'],
                    "engine_id" => $engine['engine_id'],
                    "nb_wagon" => $engine['nb_wagon'],
                ]);
            } else {
                EngineTechnical::create([
                    "id" => $engine['id'],
                    "longueur" => $engine['longueur'],
                    "largeur" => $engine['largeur'],
                    "hauteur" => $engine['hauteur'],
                    "essieux" => $engine['essieux'],
                    "max_speed" => $engine['max_speed'],
                    "poids" => $engine['poids'],
                    "traction_force" => $engine['traction_force'],
                    "type_motor" => $engine['type_motor'],
                    "power_motor" => $engine['power_motor'],
                    "start_exploi" => $engine['start_exploi'],
                    "end_exploi" => $engine['end_exploi'],
                    "gasoil" => $engine['gasoil'],
                    "type_marchandise" => $engine['type_marchandise'],
                    "nb_type_marchandise" => $engine['nb_type_marchandise'],
                    "engine_id" => $engine['engine_id'],
                    "nb_wagon" => $engine['nb_wagon'],
                ]);
            }
        }

        //--------------------------------------------------//
        $file = fopen(public_path('/storage/engines_composition.json'), 'r');
        $engine_composition = json_decode(fread($file, filesize(public_path('/storage/engines_composition.json'))), true);
        fclose($file);

        foreach ($engine_composition as $engine) {
            if (EngineComposition::find($engine['id'])) {
                EngineComposition::find($engine['id'])->update([
                    "name" => $engine['name'],
                    "importance" => $engine['importance'],
                    "maintenance_check_time" => $engine['maintenance_check_time'],
                    "maintenance_repar_time" => $engine['maintenance_repar_time'],
                    "engine_id" => $engine['engine_id'],
                ]);
            } else {
                EngineComposition::create([
                    "id" => $engine['id'],
                    "name" => $engine['name'],
                    "importance" => $engine['importance'],
                    "maintenance_check_time" => $engine['maintenance_check_time'],
                    "maintenance_repar_time" => $engine['maintenance_repar_time'],
                    "engine_id" => $engine['engine_id'],
                ]);
            }
        }
    }
}
