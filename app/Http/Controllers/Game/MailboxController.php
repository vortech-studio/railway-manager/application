<?php

namespace App\Http\Controllers\Game;

use App\Http\Controllers\Controller;

class MailboxController extends Controller
{
    public function index()
    {
        return view('game.account.mailbox.index', [
            "messages" => auth()->user()->messages()->orderBy('created_at', 'desc')->get()
        ]);
    }

    public function show($message_id)
    {
        $message = auth()->user()->messages()->findOrFail($message_id);

        $message->read_at = now();
        $message->save();

        return view('game.account.mailbox.show', [
            "message" => $message
        ]);
    }
}
