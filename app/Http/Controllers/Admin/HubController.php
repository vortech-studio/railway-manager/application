<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Core\Gares;
use App\Models\Core\Hub;

class HubController extends Controller
{
    public function index()
    {
        return view('admin.hub.index', [
            "hubs" => Hub::join('gares', 'hubs.gares_id', '=', 'gares.id')
                ->select('hubs.*', 'gares.name as gare_name')
                ->orderBy('gares.name', 'asc')
                ->get()
        ]);
    }

    public function create()
    {
        //dd(Gares::where('type_gare', 'a')->pluck('name', 'id'));
        return view('admin.hub.create', [
            "gares" => Gares::where('type_gare', 'a')
                ->pluck('name', 'id')
        ]);
    }

    public function store()
    {
        $g = new Gares();
        $gare = Gares::find(request('gares_id'));
        $hub = new Hub();
        $hub->price = $g->calcPrice($gare);
        $hub->taxe_hub = $g->calcTaxeHub($gare, $gare);
        $hub->passenger_first = $g->calcPassengerFirst($gare);
        $hub->passenger_second = $g->calcPassengerSecond($gare);
        $hub->nb_slot_commerce = $g->calcNbSlotCommerce($gare);
        $hub->nb_slot_pub = $g->calcNbSlotPub($gare);
        $hub->nb_slot_parking = $g->calcNbSlotParking($gare);
        $hub->active = true;
        $hub->gares_id = request('gares_id');
        $hub->save();

        alert()->success('Gestion des hubs','Le hub à été ajouter avec succès');
        return redirect()->route('admin.hub.index');
    }

    public function edit($id)
    {
        return view('admin.hub.edit', [
            "hub" => Hub::find($id),
            "gares" => Gares::where('type_gare', 'a')
                ->pluck('name', 'id')
        ]);
    }

    public function update($id)
    {
        $g = new Gares();
        $hub = Hub::find($id);
        $gare = Gares::find(request('gares_id'));
        $hub->price = $g->calcPrice($gare);
        $hub->taxe_hub = $g->calcTaxeHub($gare, $gare);
        $hub->passenger_first = $g->calcPassengerFirst($gare);
        $hub->passenger_second = $g->calcPassengerSecond($gare);
        $hub->nb_slot_commerce = $g->calcNbSlotCommerce($gare);
        $hub->nb_slot_pub = $g->calcNbSlotPub($gare);
        $hub->nb_slot_parking = $g->calcNbSlotParking($gare);
        $hub->active = true;
        $hub->gares_id = request('gares_id');
        $hub->save();

        alert()->success('Gestion des hubs','Le hub à été modifier avec succès');
        return redirect()->route('admin.hub.index');
    }
}
