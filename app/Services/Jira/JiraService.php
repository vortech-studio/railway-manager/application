<?php

namespace App\Services\Jira;

use Jira\Laravel\Facades\Jira;

class JiraService
{
    public static function getAllNewTickets()
    {
        $new_tickets = Jira::issues()->search(['jql' => 'type = Support OR type = Bug AND status = Open AND reporter != 712020:325159f7-73bc-4060-b8e8-90d88c06d6c9']);

        return $new_tickets;
    }

    public static function createCustomer($fullname, $email)
    {
        return Jira::customers()->create(
            body: [
                'fullName' => $fullname,
                'email' => $email,
            ]
        );
    }

    public static function createTicket(string $subject, string $description)
    {
        return Jira::issues()->create(
            body: [
                'fields' => [
                    'project' => [
                        'key' => 'RS',
                    ],
                    'summary' => $subject,
                    'description' => $description,
                    'issuetype' => [
                        'name' => 'Bug',
                    ],
                    'reporter' => [
                        'name' => 'Vortech Studio',
                    ],
                    'components' => [
                        [
                            'name' => 'Core',
                        ],
                    ],
                    'versions' => [
                        [
                            'name' => config('global.version'),
                        ],
                    ],
                    'priority' => [
                        'name' => 'High',
                    ],
                    'fixVersions' => [
                        [
                            'name' => self::getNextPatchVersion(),
                        ],
                    ],
                ],
            ]
        );
    }

    public static function getNextPatchVersion()
    {
        $version = explode('.', config('global.version'));
        $version[2] = intval($version[2]) + 1;
        return implode('.', $version);
    }

    public static function getNextMinorVersion()
    {
        $version = explode('.', config('global.version'));
        $version[1] = intval($version[1]) + 1;
        return implode('.', $version);
    }

    public static function getNextMajorVersion()
    {
        $version = explode('.', config('global.version'));
        $version[0] = intval($version[0]) + 1;
        return implode('.', $version);
    }
}
