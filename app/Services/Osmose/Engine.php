<?php

namespace App\Services\Osmose;

use App\Models\User\UserEngine;
use App\Services\Osmose\Osmose;

class Engine extends Osmose
{
    public function storing(UserEngine $engine)
    {

        try {
            $request = \Http::withoutVerifying()->post($this->endpoint.'/engine', [
                "engine_id" => $engine->id,
                "type_materiel" => $engine->engine->type_engine,
                "type_moteur" => $engine->engine->type_energy,
            ]);

            if(request()->ajax()) {
                \Log::debug("Storing {$engine->engine->name} on Osmose");
                return response()->json();
            } else {
                \Log::debug("Storing {$engine->engine->name} on Osmose");
                return [
                    "status" => $request->status(),
                    "body" => $request->body(),
                    "object" => $request->object(),
                ];
            }
        }catch (\Exception $exception) {
            \Log::critical("OSMOSE ERROR: {$exception->getMessage()}", ["context" => $exception->getTraceAsString()]);
            if(request()->ajax()) {
                return response()->json([
                    "message" => $exception->getMessage(),
                ], 500);
            } else {
                abort(500, $exception->getMessage());
            }
        }
    }

    public function retrieve(int $user_engine_id)
    {
        if(request()->ajax()) {
            try {
                $request = \Http::withoutVerifying()->get($this->endpoint . '/engine/' . $user_engine_id);
                return $request->object();
            }catch (\Exception $exception) {
                \Log::critical("OSMOSE ERROR: {$exception->getMessage()}", ["context" => $exception->getTraceAsString()]);
                return $exception->getMessage();
            }
        } else {
            try {
                $request = \Http::withoutVerifying()->get($this->endpoint . '/engine/' . $user_engine_id);
                return $request->object();
            }catch (\Exception $exception) {
                \Log::critical("OSMOSE ERROR: {$exception->getMessage()}", ["context" => $exception->getTraceAsString()]);
                abort(500, $exception->getMessage());
            }
        }
    }
}
