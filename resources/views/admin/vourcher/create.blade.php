@extends("admin.template")

@section("css")

@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Nouveau code d'échange",
        'action' => true,
        'data' => [
            [
                'url' => back()->getTargetUrl(),
                'title' => "<i class='fa-solid fa-arrow-left me-2'></i> Retour",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <form action="{{ route('admin.vourcher.store') }}" method="post">
        @csrf
        <div class="card shadow-lg">
            <div class="card-body">
                <div class="row mb-10">
                    <div class="col-md-10">
                        <label for="model_type" class="control-label required">Secteur</label>
                        <select name="model_type" id="model_type" class="form-control selectpicker" data-live-search="true" required title="---- Selectionner un secteur ----">
                            <option value="Engine">Matériels Roulants</option>
                            <option value="Hub">Hubs</option>
                            <option value="Ligne">Lignes</option>
                            <option value="Shop">Boutique</option>
                        </select>
                        <div class="mb-10 me-3">
                            <label for="model_id" class="control-label required">Produit</label>
                            <select name="model_id" id="model_id" class="form-control selectpicker" data-live-search="true" required title="---- Selectionner un Produit ----">

                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <x-form.input
                            name="nb_vourcher"
                            required="true"
                            value="1"
                            label="Nombre de code" />
                    </div>
                </div>

                <x-form.input
                    name="designation"
                    label="Titre" />
                <x-form.input-date
                    name="date_expiration"
                    label="Date d'expiration" />
                <div class="mb-10 me-3">
                    <label for="action_type" class="control-label required">Type d'action</label>
                    <select name="action_type" id="action_type" class="form-control selectpicker" data-live-search="true" required title="---- Selectionner un type d'action ----">
                        <option value="gift">Cadeaux</option>
                        <option value="reduce">Réduction</option>
                    </select>
                </div>
                <x-form.input
                    name="action_value"
                    label="Valeur de l'action" />
            </div>
            <div class="card-footer d-flex flex-end">
                <x-form.button class="btn-primary"/>
            </div>
        </div>
    </form>
@endsection

@section("js")
    <script src="https://npmcdn.com/flatpickr/dist/l10n/fr.js"></script>
    <script type="text/javascript">

        $("#date_expiration").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d 00:00:00",
            locale: "fr",
            monthSelectorType: 'dropdown'
        })

        document.querySelector("#model_type").addEventListener("change", function(e) {
            document.querySelector("#model_id").innerHTML = ""
            $.ajax({
                url: '/api/core/voucher/liste-product',
                data: {
                    model_type: e.target.value
                },
                success: function (data) {
                    let html = ""
                    data.forEach(function (item) {
                        html += "<option value='" + item.id + "'>" + item.name + "</option>"
                    })
                    document.querySelector("#model_id").innerHTML = html
                    $('#model_id').selectpicker('refresh');
                }
            })
        })
        document.querySelector("#action_type").addEventListener("change", e => {
            if(e.target.value === "gift") {
                document.querySelector("#action_value").setAttribute("type", "number")
                document.querySelector("#action_value").setAttribute("min", "1")
                document.querySelector("#action_value").setAttribute("max", "10000")
                document.querySelector("#action_value").setAttribute("step", "1")
                document.querySelector("#action_value").setAttribute("value", "1")
            } else {
                document.querySelector("#action_value").setAttribute("type", "number")
                document.querySelector("#action_value").setAttribute("min", "1")
                document.querySelector("#action_value").setAttribute("max", "100")
                document.querySelector("#action_value").setAttribute("step", "1")
            }
        })
    </script>
@endsection
