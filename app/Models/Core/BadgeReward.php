<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class BadgeReward extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $appends = ['type_icon', 'type_text'];

    public function badge()
    {
        return $this->belongsTo(Badge::class, 'badge_id');
    }

    public function getTypeIconAttribute()
    {
        return match ($this->type) {
            "argent" => asset('/storage/icons/euro.png'),
            "tpoint" => asset('/storage/icons/tpoint.png'),
            "research" => asset('/storage/icons/research.png'),
            "experience" => asset('/storage/icons/level-up.png'),
            "engine" => asset('/storage/icons/train.png'),
            "hub" => asset('/storage/icons/hub.png'),
            "ligne" => asset('/storage/icons/ligne.png'),
        };
    }

    public function getTypeTextAttribute()
    {
        return match ($this->type) {
            "argent", "research" => eur($this->value),
            "tpoint" => $this->value . "TP",
            "experience" => $this->value . "XP",
            "engine" => Engine::find($this->value)->name,
            "hub" => Hub::find($this->value)->gare->name,
            "ligne" => Ligne::find($this->value)->name,
        };
    }
}
