<?php

namespace App\Models\Core;

use App\Traits\HasCheckout;
use BeyondCode\Vouchers\Traits\HasVouchers;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Hub extends Model
{
    use HasFactory, HasVouchers, HasCheckout;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $appends = ["format_price", "format_taxe", "freq_actual", "status", "lignes"];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'price' => 'decimal:2',
        'taxe_hub' => 'decimal:2',
    ];

    public function gare(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Core\Gares::class, 'gares_id');
    }

    public function lignes()
    {
        return $this->hasMany(\App\Models\Core\Ligne::class);
    }

    public function requirements()
    {
        return $this->hasMany(\App\Models\Core\LigneRequirement::class);
    }

    public function userHubs()
    {
        return $this->hasMany(\App\Models\User\UserHub::class);
    }

    public function getFormatPriceAttribute()
    {
        return eur($this->price);
    }

    public function getFormatTaxeAttribute()
    {
        return eur($this->taxe_hub);
    }

    public function getFreqActualAttribute()
    {
        return $this->calcFreqActual();
    }

    public function getStatusAttribute()
    {
        return $this->getStatus();
    }

    public function getLignesAttribute()
    {
        return $this->lignes()->where('active', true)->get();
    }

    public function calcFreqActual(): int
    {
        $freq_horaire = $this->gare->time_day_work;
        $start_day = Carbon::createFromTime(06,00, 00);
        $end_day = Carbon::createFromTime(06,00, 00)->addHours($freq_horaire);
        $now_hour = Carbon::now();

        if ($now_hour >= $start_day && $now_hour <= $end_day) {
            return intval($this->passenger_first + $this->passenger_second * rand(50, 100) / 100);
        } else {
            return 0;
        }
    }

    public function getStatus(): bool
    {
        $freq_horaire = $this->gare->time_day_work;
        $start_day = Carbon::createFromTime(06,00, 00);
        $end_day = Carbon::createFromTime(06,00, 00)->addHours($freq_horaire);
        $now_hour = Carbon::now();

        if ($now_hour >= $start_day && $now_hour <= $end_day) {
            return true;
        } else {
            return false;
        }
    }
}
