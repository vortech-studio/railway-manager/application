@extends("game.template")

@section("css")
    <link src="{{ asset('/plugins/leaflet/leaflet.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/hub_checkout.png') }}" alt="">
            </div>
            <x-base.underline
                title="Achat d'un hub"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.network.partials.menu")
    </div>
    <form action="{{ route('hub.checkout.confirm') }}" method="post" id="formHubCheckout">
        @csrf
        <div class="row">
            <div class="col-8">
                <div class="card card-custom gutter-b shadow-lg">
                    <div class="card-body">
                        <div class="mb-10">
                            <label for="hub_id" class="form-label required">Hub Disponible</label>
                            <select id="hub_id" class="form-control selectpicker" name="hub_id" data-live-search="true">
                                <option value="">-- Choisir un hub --</option>
                                @foreach($hubs as $hub)
                                    <option value="{{ $hub->id }}" {{ auth()->user()->hubs()->where('hub_id', $hub->id)->get()->count() != 0 ? 'disabled' : '' }}>{{ $hub->gare->name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="amount" value="0">
                        </div>
                        <div class="d-flex flex-row w-100 d-none" id="loadInfo">
                            <div class="w-550px me-2">
                                <div id="mapHub" style="height: 250px;"></div>
                            </div>
                            <div class="w-50">
                                <div class="d-flex flex-column p-5">
                                    <div class="fw-bold">Nom du Hub</div>
                                    <div data-ajax-load="nameHub">-</div>
                                </div>
                                <div class="separator border-1 border-gray-400 py-3"></div>
                                <div class="d-flex flex-column p-5">
                                    <div class="fw-bold">Taxe de passage de ligne</div>
                                    <div data-ajax-load="taxe">-</div>
                                </div>
                                <div class="separator border-1 border-gray-400 py-3"></div>
                                <div class="d-flex flex-row justify-content-around border border-1 border-gray-500">
                                    <div class="d-flex flex-column justify-content-center align-items-center m-5">
                                        <div class="fs-6 text-center">Nb Voyageur First Classe</div>
                                        <div class="fs-4 fw-bolder" data-ajax-load="passengerFirst">-</div>
                                    </div>
                                    <div class="d-flex flex-column justify-content-center align-items-center m-5">
                                        <div class="fs-6 text-center">Nb Voyageur Second Classe</div>
                                        <div class="fs-4 fw-bolder" data-ajax-load="passengerSecond">-</div>
                                    </div>
                                </div>
                                <div class="separator border-1 border-gray-400 py-3"></div>
                                <div class="d-flex flex-row justify-content-around border border-1 border-gray-500">
                                    <div class="d-flex flex-column justify-content-center align-items-center m-5">
                                        <div class="fs-6 text-center">Commerce</div>
                                        <div class="fs-4 fw-bolder" data-ajax-load="nbCommerce">-</div>
                                    </div>
                                    <div class="d-flex flex-column justify-content-center align-items-center m-5">
                                        <div class="fs-6 text-center">Publicité</div>
                                        <div class="fs-4 fw-bolder" data-ajax-load="nbPublicite">-</div>
                                    </div>
                                    <div class="d-flex flex-column justify-content-center align-items-center m-5">
                                        <div class="fs-6 text-center">Parking</div>
                                        <div class="fs-4 fw-bolder" data-ajax-load="nbParking">-</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card card-flush shadow-lg d-none" id="loadRecap">
                    <div class="card-header">
                        <h3 class="card-title">Récapitulatif de l'achat</h3>
                    </div>
                    <div class="card-body">
                        <div class="d-flex flex-column">
                            <div class="d-flex flex-row justify-content-between align-items-center px-3">
                                <div class="">Prix du hub</div>
                                <div class="fw-bolder" data-ajax-load="price">-</div>
                            </div>
                            <div class="separator border-1 border-gray-400 py-3"></div>
                            <div class="d-flex flex-row justify-content-between align-items-center px-3">
                                <div class="">Subvention (<span data-ajax-load="subvention_percent">-</span>)</div>
                                <div class="fw-bolder" data-ajax-load="subvention_amount">-</div>
                            </div>
                            <div class="separator border-1 border-gray-400 py-5"></div>
                            <div class="d-flex flex-row justify-content-between align-items-center text-danger fw-bolder fs-3 px-3">
                                <div class="">Prix à payer</div>
                                <div class="fw-bolder" data-ajax-load="total">-</div>
                            </div>
                            <div class="d-flex flex-center w-100 mt-5">
                                <button type="submit" class="btn btn-success"><i class="fa-solid fa-euro-sign"></i> Acheter ce hub</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>



@endsection

@section("script")
    <script src="{{ asset('/plugins/leaflet/leaflet.js') }}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinysort/2.3.6/tinysort.js"></script>
    <script src="{{ asset('/assets/js/game/network/hub_checkout.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        let subvention_percent = 0;
        $.ajax({
            url: '/api/account/'+{{ auth()->user()->id }}+'/compta/subvention',
            success: data => {
                console.log(data)
                subvention_percent = data.subvention
            }
        })

        document.querySelector('[name="hub_id"]').addEventListener('change', e => {
            $.ajax({
                url: '/api/core/hub/'+e.target.value,
                success: data => {
                    console.log(data)
                    document.querySelector('#loadInfo').classList.remove('d-none')
                    document.querySelector('#loadRecap').classList.remove('d-none')
                    document.querySelector('[data-ajax-load="nameHub"]').innerHTML = data.hub.gare.name
                    document.querySelector('[data-ajax-load="taxe"]').innerHTML = data.hub.format_taxe + ' / Voyage'
                    document.querySelector('[data-ajax-load="passengerFirst"]').innerHTML = data.hub.passenger_first
                    document.querySelector('[data-ajax-load="passengerSecond"]').innerHTML = data.hub.passenger_second
                    document.querySelector('[data-ajax-load="nbCommerce"]').innerHTML = new Intl.NumberFormat('fr-FR').format(data.hub.nb_slot_commerce)
                    document.querySelector('[data-ajax-load="nbPublicite"]').innerHTML = new Intl.NumberFormat('fr-FR').format(data.hub.nb_slot_pub)
                    document.querySelector('[data-ajax-load="nbParking"]').innerHTML = new Intl.NumberFormat('fr-FR').format(data.hub.nb_slot_parking)
                    document.querySelector('[data-ajax-load="price"]').innerHTML = data.hub.format_price
                    document.querySelector('[data-ajax-load="subvention_percent"]').innerHTML = subvention_percent + '%'
                    document.querySelector('[data-ajax-load="subvention_amount"]').innerHTML = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(data.hub.price * subvention_percent / 100)
                    document.querySelector('[data-ajax-load="total"]').innerHTML = calcTotal(data.hub.price, subvention_percent)
                    document.querySelector('[name="amount"]').value = data.hub.price - (data.hub.price * subvention_percent / 100)

                    MapInitHub(data.hub, 'mapHub')
                }
            })
        })

        function calcTotal(price, subvention) {
            return new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(price - (price * subvention / 100))
        }
    </script>
@endsection
