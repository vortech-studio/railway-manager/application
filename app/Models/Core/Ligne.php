<?php

namespace App\Models\Core;

use App\Traits\LigneTrait;
use BeyondCode\Vouchers\Traits\HasVouchers;
use Illuminate\Database\Eloquent\Model;

class Ligne extends Model
{
    use LigneTrait, HasVouchers;
    public $timestamps = false;

    protected $guarded = [];

    protected $appends = ['name', 'time_format', 'type_ligne_logo', 'distance_format', 'type_ligne_name', 'type_ligne_color'];

    public function hub()
    {
        return $this->belongsTo(Hub::class);
    }

    public function stationStart()
    {
        return $this->belongsTo(Gares::class, 'station_start_id', 'id');
    }

    public function stationEnd()
    {
        return $this->belongsTo(Gares::class, 'station_end_id', 'id');
    }

    public function stations()
    {
        return $this->hasMany(LigneStation::class);
    }

    public function requirements()
    {
        return $this->hasMany(LigneRequirement::class);
    }

    public function getNameAttribute(): string
    {
        return $this->stationStart->name." - ".$this->stationEnd->name;
    }

    public function getTimeFormatAttribute()
    {
        return convertMinuteToHours($this->time_min);
    }

    public function getTypeLigneNameAttribute()
    {
        return match($this->type_ligne) {
            'ter' => 'TER',
            'tgv' => 'TGV Inoui',
            'ic' => 'Intercité',
            'transilien' => 'Transilien',
        };
    }

    public function getTypeLigneLogoAttribute()
    {
        return match($this->type_ligne) {
            'ter' => '/storage/logos/logo_ter.svg',
            'tgv' => '/storage/logos/logo_tgv.svg',
            'ic' => '/storage/logos/logo_intercite.svg',
            'transilien' => '/storage/logos/logo_transilien.svg',
        };
    }
    public function getTypeLigneColorAttribute()
    {
        return match($this->type_ligne) {
            'ter' => 'primary',
            'tgv' => 'danger',
            'ic' => 'info',
            'transilien' => 'success',
        };
    }

    public function getDistanceFormatAttribute()
    {
        return $this->distance." Km";
    }

    public function calculVitesseMoyenne()
    {
        $stations = $this->stations()->get();

        $vitesseMoyenne = $stations->map(function ($station) {
            if($station->time == 0) {
                return $station->distance / 1;
            } else {
                return $station->distance / $station->time;
            }

        });

        $vitesseMoyenneGlobale = floatval($vitesseMoyenne->avg());

        return $vitesseMoyenneGlobale;
    }
}
