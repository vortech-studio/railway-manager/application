<div class="mb-10">
    <label class="form-label {{ $required = true ? 'required' : '' }}">{{ $label }}</label>
    <div id="{{ $id }}"></div>
</div>
