<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCardBonusesTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_card_bonuses', function (Blueprint $table) {
            $table->id();
            $table->integer('sim_offer');
            $table->integer('audit_ext_offer');
            $table->integer('audit_int_offer');

            $table->foreignId('user_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_card_bonuses');
    }
}
