<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class LigneRequirement extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public function ligne()
    {
        return $this->belongsTo(Ligne::class);
    }

    public function hub()
    {
        return $this->belongsTo(Hub::class);
    }
}
