<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserPlanningPassenger extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function planning()
    {
        return $this->belongsTo(UserPlanning::class, 'user_planning_id');
    }

    public function passengersOnBoard(UserPlanningStation $station)
    {
        if($this->planning->engine->engine->type_train == 'ter') {
            $this->planning->passengers()->create([
                "type" => "second",
                "nb_passengers" => rand(0, $station->ligneStation->gare->passenger_unique),
                "user_planning_id" => $this->planning->id,
            ]);
        } else {
            $this->planning->passengers()->create([
                "type" => "first",
                "nb_passengers" => rand(0, $station->ligneStation->gare->passenger_first),
                "user_planning_id" => $this->planning->id,
            ]);

            $this->planning->passengers()->create([
                "type" => "second",
                "nb_passengers" => rand(0, $station->ligneStation->gare->passenger_second),
                "user_planning_id" => $this->planning->id,
            ]);
        }
    }


}
