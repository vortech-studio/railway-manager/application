<?php

namespace App\Models\User;

use App\Models\Core\EngineComposition;
use App\Models\Core\TypeIncident;
use App\Models\User;
use App\Traits\UserIncidentTrait;
use Illuminate\Database\Eloquent\Model;

class UserIncident extends Model
{
    use UserIncidentTrait;

    protected $guarded = [];
    protected $appends = [
        'niveau_indicator',
        'status_label',
        'amount_reparation_format',
        'date_format',
        "repercute_traffic_format",
        'niveau_image',
        'niveau_color'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function planning()
    {
        return $this->belongsTo(UserPlanning::class, 'user_planning_id');
    }

    public function type()
    {
        return $this->belongsTo(TypeIncident::class, 'type_incident_id');
    }

    public function material()
    {
        return $this->belongsTo(EngineComposition::class, 'engine_composition_id');
    }

    public function user_engine()
    {
        return $this->belongsTo(UserEngine::class, 'user_engine_id');
    }

    public function hub()
    {
        return $this->belongsTo(UserHub::class, 'user_hub_id');
    }

    public function getNiveauIndicatorAttribute()
    {
        return match ($this->niveau) {
            1 => "low",
            2 => "middle",
            3 => "high"
        };
    }

    public function getNiveauImageAttribute()
    {
        return asset('/storage/icons/warning_'.$this->niveau_indicator.'.png');
    }

    public function getNiveauLabelAttribute()
    {
        return match ($this->niveau) {
            1 => "Panne Mineur",
            2 => "Panne Majeur",
            3 => "Grave Incident"
        };
    }

    public function getNiveauColorAttribute()
    {
        return match ($this->niveau) {
            1 => "primary",
            2 => "warning",
            3 => "danger"
        };
    }

    public function getStatusLabelAttribute()
    {
        return match ($this->status) {
            "open" => '<span class="badge badge-light-danger fw-bold fs-8 px-2 py-1 ms-2">Ouvert</span>',
            "closed" => '<span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2">Fermer</span>',
        };
    }

    public function getAmountReparationFormatAttribute()
    {
        return eur($this->amount_reparation);
    }

    public function getDateFormatAttribute()
    {
        return $this->created_at->format('d/m/Y à H:i');
    }

    public function getRepercuteTrafficFormatAttribute()
    {
        if($this->niveau == 2) {
            return $this->planning->retarded_time. " ". \Str::plural('minute', $this->planning->retarded_time)." de retard sur le trajet";
        } else if($this->niveau == 3) {
            return "Trajet supprimé";
        } else {
            return "Aucune incidence sur le traffic";
        }
    }
}
