<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Core\Gares;
use App\Models\Core\Ligne;
use App\Models\Core\LigneRequirement;
use App\Models\Core\LigneStation;
use App\Services\Mapping\Mapbox;
use App\Services\Mapping\OpenRouteService;
use Illuminate\Http\Request;

class LigneStationController extends Controller
{

    public function __construct(public Mapbox $mapbox)
    {
    }

    public function store($ligne_id, Request $request)
    {
        $ligne = Ligne::findOrFail($ligne_id);
        $latestStation = $ligne->stations()->latest('id')->first();
        $gare = Gares::find($request->get('gares_id'));

        if($latestStation) {
            $distance = $latestStation->calculerDistanceVincenty(
                $latestStation->gare->latitude,
                $latestStation->gare->longitude,
                $gare->latitude,
                $gare->longitude
            );

            if ($ligne->type_ligne == 'ter' || $ligne->type_ligne == 'ic') {
                $vitesse = $this->convertVitesse(160);
            } else if ($latestStation->ligne->type_ligne == 'tgv') {
                $vitesse = $this->convertVitesse(320);
            } else {
                $vitesse = $this->convertVitesse(80);
            }

            $station = LigneStation::create([
                "distance" => $distance,
                "time" => $latestStation->time + $latestStation->calculerTemps($distance, $vitesse),
                "gares_id" => $gare->id,
                "ligne_id" => $ligne->id,
            ]);
        } else {
            $station = LigneStation::create([
                "distance" => 0,
                "time" => 0,
                "gares_id" => $gare->id,
                "ligne_id" => $ligne->id,
            ]);
        }

        $ligne->time_min = $station->time;
        $ligne->save();

        toastr()->success("La station a bien été ajoutée à la ligne {$ligne->name}", "Success");
        return redirect()->route('admin.ligne.show', $ligne->id);
    }

    public function storeRequirement($ligne_id, Request $request)
    {
        $ligne = Ligne::findOrFail($ligne_id);

        LigneRequirement::create([
            'hub_id' => $request->hub_id,
            'ligne_id' => $ligne->id,
        ]);

        toast("Le hub a bien été ajouté à la ligne {$ligne->name}", "success")->autoClose(3000)->position('top-end');
        return redirect()->route('admin.ligne.show', $ligne->id);
    }

    private function calculerDistance(LigneStation $latestStation, $time)
    {
        if ($latestStation->ligne->type_ligne == 'ter' || $latestStation->ligne->type_ligne == 'ic') {
            $vitesse = $this->convertVitesse(140);
        } else if ($latestStation->ligne->type_ligne == 'tgv') {
            $vitesse = $this->convertVitesse(300);
        } else {
            $vitesse = $this->convertVitesse(80);
        }
        $diffTime = ($time - $latestStation->time);
        $tempsEnSecond = $diffTime * 60;

        $distanceEnMetre = $vitesse * $tempsEnSecond;
        return $distanceEnMetre / 1000;
    }

    private function convertVitesse($vitesse)
    {
        return $vitesse / 3.6;
    }

    private function calculTime($distance, $vitesse)
    {
        $tempsEnSecond = $distance / $vitesse;
        return $tempsEnSecond / 60;
    }
}
