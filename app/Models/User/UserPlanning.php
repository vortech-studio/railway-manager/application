<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserPlanning extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $casts = [
        'date_depart' => 'datetime',
        'date_arrived' => 'datetime',
    ];

    protected $appends = [
        'status_format',
        'status_format_icon',
        'status_format_text',
        'status_format_color_text',
    ];


    public function ligne()
    {
        return $this->belongsTo(UserLigne::class, 'user_ligne_id');
    }

    public function engine()
    {
        return $this->belongsTo(UserEngine::class, 'user_engine_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function hub()
    {
        return $this->belongsTo(UserHub::class, 'user_hub_id');
    }

    public function passengers()
    {
        return $this->hasMany(UserPlanningPassenger::class, 'user_planning_id');
    }

    public function logs()
    {
        return $this->hasMany(UserPlanningLog::class, 'user_planning_id');
    }

    public function travel()
    {
        return $this->hasOne(UserPlanningTravel::class, 'user_planning_id');
    }

    public function planning_stations()
    {
        return $this->hasMany(UserPlanningStation::class, 'user_planning_id');
    }

    public function incidents()
    {
        return $this->hasMany(UserIncident::class);
    }

    public function generateNumberTravel()
    {
        return match ($this->ligne->ligne->type_ligne) {
            "ter" => rand(800000,899999),
            "tgv" => rand(8000,8999),
            "ic" => rand(16000,16999),
            "transilien" => rand(100000,199999),
        };
    }

    public function getStatusFormatAttribute()
    {
        return match($this->status) {
            "travel" => "<i class='fa-solid fa-arrow-right-arrow-left text-success me-2'></i> En Transit",
            "in_station" => '<iconify-icon icon="fluent-emoji-high-contrast:station" class="text-warning me-2"></iconify-icon> A Quai',
            "departure" => '<iconify-icon icon="tabler:traffic-lights" class="text-success me-2"></iconify-icon> Départ imminent',
            "arrival" => '<iconify-icon icon="tabler:traffic-lights" class="text-danger me-2"></iconify-icon> Arrivée',
            "initialized" => "<i class='fa-solid fa-clock text-white fs-2 me-2'></i> A L'heure",
            "retarded" => "<i class='fa-solid fa-clock text-warning fs-2 me-2'></i> Retardée",
            "canceled" => "<i class='fa-solid fa-times-circle text-danger fs-2 me-2'></i> Train Supprimé",
        };
    }

    public function getStatusFormatTextAttribute()
    {
        return match($this->status) {
            "travel" => "En Transit",
            "in_station" => 'A Quai',
            "departure", "initialized" => "A L'heure",
            "arrival" => 'Arrivée',
            "retarded" => "Retardé",
            "canceled" => "Train Supprimé",
        };
    }

    public function getStatusFormatColorTextAttribute()
    {
        return match($this->status) {
            "travel", "initialized", "departure", "arrival" => "success",
            "in_station" => 'info',
            "retarded" => "warning",
            "canceled" => "danger",
        };
    }

    public function getStatusFormatIconAttribute()
    {
        return match($this->status) {
            "travel" => "<i class='fa-solid fa-arrow-right-arrow-left text-success me-2'></i>",
            "in_station" => '<iconify-icon icon="fluent-emoji-high-contrast:station" class="text-info me-2"></iconify-icon>',
            "departure", "arrival" => '<iconify-icon icon="tabler:traffic-lights" class="text-success me-2"></iconify-icon>',
            "initialized" => "<i class='fa-solid fa-clock text-success fs-2 me-2'></i>",
            "retarded" => "<i class='fa-solid fa-clock text-warning fs-2 me-2'></i>",
            "canceled" => "<i class='fa-solid fa-clock text-danger fs-2 me-2'></i>",
        };
    }

}
