<?php

namespace App\Livewire;

use App\Jobs\Game\HubSkillJob;
use App\Models\Core\Mouvement;
use App\Models\User\UserDelivery;
use App\Traits\ResearchTrait;
use Carbon\Carbon;
use Livewire\Component;

class GameSkillItem extends Component
{
    public $skill;
    public $contentPopover = '';

    public function mount()
    {
        $this->contentPopover = $this->setContentPopover();
    }

    public function research()
    {
        if(UserDelivery::where('type', 'research')->count() > 0) {
            toastr()->error("Vous avez déja une recherche en cours");
        } else {
            if(ResearchTrait::amountIsValid($this->skill->calcCoastActualLevel())) {
                $calcEnd = $this->skill->calcTimeActualLevel(false);
                // Création de la livraison
                $delivery = UserDelivery::create([
                    "type" => "research",
                    "designation" => "Recherche Active: " . $this->skill->name,
                    "start_at" => now(),
                    "end_at" => now()->addMinutes($calcEnd),
                    "user_id" => auth()->user()->id,
                    "user_hub_id" => $this->skill->user_hub_id
                ]);

                ResearchTrait::checkoutResearch($this->skill->calcCoastActualLevel());

                dispatch(new HubSkillJob($this->skill, $delivery))->delay(now()->addMinutes($calcEnd))->onQueue('delivery');
                toastr()->success("Votre demande de recherche à été programmer", "Recherche en cours...");
            } else {
                toastr()->error("Vous n'avez pas les fonds de recherche necessaire pour effectuer cette recherche", "Fond Insuffisant");
            }

        }
        $this->redirect(route('hub.show', $this->skill->user_hub_id));
    }

    public function render()
    {
        return view('livewire.game-skill-item');
    }

    public function setContentPopover()
    {
        ob_start();
        ?>
        <div class="d-flex flex-column">
            <?= $this->skill->description ?>
            <div class="separator border border-2 border-grey-800 my-5"></div>
            <?php if($this->skill->level): ?>
                <ul class="d-flex flex-column">
                    <?php for($l=1; $l <= $this->skill->level_max; $l++): ?>
                        <?php if($l <= $this->skill->level_actual): ?>
                            <li class="d-flex align-items-center py-1 text-white fs-9 fst-italic">
                                <span class="bullet bullet-dot bg-green-800 me-5"></span> <?= $this->skill->description; ?>: <?= $this->skill->calcActionValueFromLevel($l) ?>
                            </li>
                        <?php else: ?>
                            <li class="d-flex align-items-center py-1 text-muted fs-9 fst-italic">
                                <span class="bullet bullet-dot bg-white me-5"></span> <?= $this->skill->description; ?>: <?= $this->skill->calcActionValueFromLevel($l) ?>
                            </li>
                        <?php endif; ?>
                    <?php endfor; ?>
                </ul>
                <div class="separator border border-2 border-grey-800 my-5"></div>
            <?php endif; ?>
            <div class="d-flex flex-row justify-content-between align-items-center">
                <div class="d-flex flex-row align-items-center">
                    <img src="<?= asset('/storage/icons/euro.png') ?>" alt="" class="w-30px h-30px me-3">
                    <?= $this->skill->actual_coast ?>
                </div>
                <div class="d-flex flex-row align-items-center">
                    <i class="fa-solid fa-clock fs-2 me-3 text-white"></i>
                    <?= $this->skill->actual_time ?>
                </div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
}
