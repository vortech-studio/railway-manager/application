<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserHubsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_hubs', function (Blueprint $table) {
            $table->id();
            $table->integer('nb_salarie_iv');
            $table->integer('nb_salarie_com');
            $table->integer('nb_salarie_ir');
            $table->integer('nb_salarie_technicentre');
            $table->timestamp('date_achat');
            $table->integer('km_ligne');
            $table->boolean('active')->default(false);

            $table->foreignId('user_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('hub_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_hubs');
    }
}
