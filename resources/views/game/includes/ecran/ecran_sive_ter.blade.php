@if($planning->status == 'departure')
    <div id="kt_carousel_1_carousel" class="carousel carousel-custom slide" data-bs-ride="carousel" data-bs-interval="10000">
        <div class="carousel-inner pt-8">
            <div class="carousel-item active">
                <div class="bg-gray-300 w-100 h-100">
                    <div class="d-flex flex-row justify-content-around align-items-center bg-gray-800 h-150px">
                        <div class="fs-4x text-white fw-bolder">TER 830690</div>
                        <div class="">
                            <span id="clock-hours" class="fs-4x text-blue-400 fw-semibold">23</span>
                            <span class="animation-blink">
                                <span class="animation-blink-clock fs-4x text-blue-400 fw-semibold">:</span>
                            </span>
                            <span id="clock-minutes" class="fs-4x text-blue-400 fw-semibold">32</span>
                            <small id="clock-seconds" class="fs-3 ms-1 text-color-eva-arrival d-none">37</small>
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-center align-items-center">
                        <div class="fs-3hx text-gray-800 mb-5 text-center w-50">
                            La Région {{ $planning->ligne->ligne->stationStart->region }} et {{ $planning->user->name_company }} vous souhaitent la bienvenue à bord de ce train.
                        </div>
                        <div class="fs-2x text-gray-800 mb-2">Destination: </div>
                        <div class="fs-3x fw-bolder text-pink-800">{{ $planning->ligne->ligne->stationEnd->name }}</div>
                    </div>
                    <div class="position-relative">
                        <div class="position-absolute bottom-0 start-0 m-5">
                            <img src="{{ asset('/storage/avatar/company/'.$planning->user->logo_company) }}" alt="{{ $planning->user->name_company }}" class="w-100px">
                        </div>
                        <div class="position-absolute bottom-0 end-0 m-5">
                            <img src="{{ asset('/storage/logos/logo_'.$planning->ligne->ligne->type_ligne.'.svg') }}" alt="{{ $planning->ligne->ligne->type_ligne }}" class="w-100px">
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="bg-gray-300 w-100 h-100">
                    <div style="overflow: hidden" class="d-flex flex-row justify-content-around align-items-center bg-gray-800 h-150px">
                        <div class="fs-4x text-white fw-bolder">TER 830690</div>
                        <div class="">
                            <span id="clock-hours" class="fs-4x text-blue-400 fw-semibold">23</span>
                            <span class="animation-blink">
                                <span class="animation-blink-clock fs-4x text-blue-400 fw-semibold">:</span>
                            </span>
                            <span id="clock-minutes" class="fs-4x text-blue-400 fw-semibold">32</span>
                            <small id="clock-seconds" class="fs-3 ms-1 text-color-eva-arrival d-none">37</small>
                        </div>
                    </div>
                    <div class="d-flex flex-column align-items-center mx-5 h-500px">
                        <table class="table table-flush">
                            <thead>
                            <tr>
                                <th>
                                    <span class="fs-2x text-pink-800">Gares desservies:</span>
                                </th>
                                <th class="text-end">
                                    <span class="fs-2x text-pink-800">Arrivée Prévue:</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="scroll-y" style="animation: 10s linear(1.4 -17.06%, -0.82 115.29%) 0s infinite normal none running scrollY;">
                                @foreach($planning->planning_stations as $station)
                                    @if($station->ligneStation->gare->name == $planning->ligne->ligne->stationStart->name)
                                        <tr class="fs-2tx text-gray-600 pt-5">
                                            <td>{{ $station->ligneStation->gare->name }}</td>
                                            <td class="text-end">{{ $station->arrival_at->format("H:i") }}</td>
                                        </tr>
                                        <tr class="">
                                            <td><div class="separator border-gray-600 py-5"></div></td>
                                            <td><div class="separator border-gray-600 py-5"></div></td>
                                        </tr>
                                    @elseif($station->ligneStation->gare->name == $planning->ligne->ligne->stationEnd->name)
                                        <tr class="fs-2tx text-gray-600 pt-5">
                                            <td>{{ $station->ligneStation->gare->name }}</td>
                                            <td class="text-end">{{ $station->arrival_at->format("H:i") }}</td>
                                        </tr>
                                    @else
                                        <tr class="fs-2tx text-gray-600">
                                            <td>{{ $station->ligneStation->gare->name }}</td>
                                            <td class="text-end">{{ $station->arrival_at->format("H:i") }}</td>
                                        </tr>
                                        <tr class="">
                                            <td><div class="separator border-gray-600 py-5"></div></td>
                                            <td><div class="separator border-gray-600 py-5"></div></td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--<div class="position-relative">
                        <div class="position-absolute bottom-0 start-0 m-5">
                            <img src="{{ asset('/storage/avatar/company/'.$planning->user->logo_company) }}" alt="{{ $planning->user->name_company }}" class="w-100px">
                        </div>
                        <div class="position-absolute bottom-0 end-0 m-5">
                            <img src="{{ asset('/storage/logos/logo_'.$planning->ligne->ligne->type_ligne.'.svg') }}" alt="{{ $planning->ligne->ligne->type_ligne }}" class="w-100px">
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
@elseif($planning->status == 'in_station')
    <div id="kt_carousel_1_carousel" class="carousel carousel-custom slide" data-bs-ride="carousel" data-bs-interval="5000">
        <div class="carousel-inner pt-8">
            <div class="carousel-item active">
                <div class="bg-gray-300 w-100 h-100">
                    <div class="d-flex flex-row justify-content-around align-items-center bg-gray-800 h-150px">
                        <div class="fs-4x text-white fw-bolder">TER 830690</div>
                        <div class="">
                            <span id="clock-hours" class="fs-4x text-blue-400 fw-semibold">23</span>
                            <span class="animation-blink">
                                <span class="animation-blink-clock fs-4x text-blue-400 fw-semibold">:</span>
                            </span>
                            <span id="clock-minutes" class="fs-4x text-blue-400 fw-semibold">32</span>
                            <small id="clock-seconds" class="fs-3 ms-1 text-color-eva-arrival d-none">37</small>
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-center h-500px mx-10">
                        <div class="d-flex flex-column mb-10">
                            <div class="fs-2x text-gray-800 mb-2">Destination :</div>
                            <div class="fs-4x fw-bolder text-gray-800">{{ $planning->ligne->ligne->stationEnd->name }}</div>
                        </div>
                        <div class="d-flex flex-column mb-10">
                            <div class="fs-2x text-gray-800 mb-2">Train à l'arret en gare de  :</div>
                            <div class="fs-4x fw-bolder text-pink-800">{{ $planning->planning_stations()->where('status', 'arrival')->orderBy('arrival_at', 'desc')->first()->ligneStation->gare->name }}</div>
                        </div>
                    </div>
                    <div class="position-relative">
                        <div class="position-absolute bottom-0 start-0 m-5">
                            <img src="{{ asset('/storage/avatar/company/'.$planning->user->logo_company) }}" alt="{{ $planning->user->name_company }}" class="w-100px">
                        </div>
                        <div class="position-absolute bottom-0 end-0 m-5">
                            <img src="{{ asset('/storage/logos/logo_'.$planning->ligne->ligne->type_ligne.'.svg') }}" alt="{{ $planning->ligne->ligne->type_ligne }}" class="w-100px">
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="bg-gray-300 w-100 h-100">
                    <div class="d-flex flex-row justify-content-around align-items-center bg-gray-800 h-150px">
                        <div class="fs-4x text-white fw-bolder">TER 830690</div>
                        <div class="">
                            <span id="clock-hours" class="fs-4x text-blue-400 fw-semibold">23</span>
                            <span class="animation-blink">
                                <span class="animation-blink-clock fs-4x text-blue-400 fw-semibold">:</span>
                            </span>
                            <span id="clock-minutes" class="fs-4x text-blue-400 fw-semibold">32</span>
                            <small id="clock-seconds" class="fs-3 ms-1 text-color-eva-arrival d-none">37</small>
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-center align-items-center">
                        <div class="fs-3hx text-gray-800 mb-5 text-center w-50">
                            La Région {{ $planning->ligne->ligne->stationStart->region }} et {{ $planning->user->name_company }} vous souhaitent la bienvenue à bord de ce train.
                        </div>
                        <div class="fs-2x text-gray-800 mb-2">Destination: </div>
                        <div class="fs-3x fw-bolder text-pink-800">{{ $planning->ligne->ligne->stationEnd->name }}</div>
                    </div>
                    <div class="position-relative">
                        <div class="position-absolute bottom-0 start-0 m-5">
                            <img src="{{ asset('/storage/avatar/company/'.$planning->user->logo_company) }}" alt="{{ $planning->user->name_company }}" class="w-100px">
                        </div>
                        <div class="position-absolute bottom-0 end-0 m-5">
                            <img src="{{ asset('/storage/logos/logo_'.$planning->ligne->ligne->type_ligne.'.svg') }}" alt="{{ $planning->ligne->ligne->type_ligne }}" class="w-100px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($planning->status == 'travel')
    <div id="kt_carousel_1_carousel" class="carousel carousel-custom slide" data-bs-ride="carousel" data-bs-interval="5000">
        <div class="carousel-inner pt-8">
            <div class="carousel-item active">
                <div class="bg-gray-300 w-100 h-100">
                    <div class="d-flex flex-row justify-content-around align-items-end bg-gray-800 h-150px">
                        <div class="fs-4x text-white fw-bolder">TER 830690</div>
                        <div class="fs-4x text-white" data-html="speed">
                            275 Km/h
                        </div>
                    </div>
                    <div class="d-flex flex-row h-500px w-100">
                        <div class="d-flex flex-column justify-content-center bg-white w-25">
                            <div class="d-flex flex-column mb-5 mx-10">
                                <div class="fs-1">Destination :</div>
                                <div class="fs-2x fw-bolder text-pink-800">{{ $planning->ligne->ligne->stationEnd->name }}</div>
                            </div>
                            <div class="d-flex flex-column mb-5 mx-10">
                                <div class="fs-1">Prochaine Arret :</div>
                                <div class="fs-2x fw-bolder text-pink-800" data-html="next-station">{{ $planning->planning_stations()->where('status', '!=', 'done')->first()->name }}</div>
                            </div>
                            <div class="d-flex flex-column mb-5 mx-10">
                                <div class="fs-1">Arrivée Prévue :</div>
                                <div class="fs-3x fw-bolder text-gray-800" data-html="arrival-station">{{ $planning->planning_stations()->where('status', '!=', 'done')->first()->arrival_at->format("H:i") }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row-fluid align-items-center justify-content-center mx-5">
                            <div class="">
                                <div class="w-100px h-100px rounded-circle border border-2 border-primary d-flex justify-content-center align-items-center">
                                    <i class="fa-solid fa-train fs-4x text-primary"></i>
                                </div>
                            </div>
                            <div class="h-20px mx-3 w-100 bg-white bg-opacity-50 rounded">
                                <div class="bg-primary rounded h-20px progress-bar-striped progress-bar-animated" role="progressbar" style="width: 72%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="pulse pulse-primary">
                                <div class="w-100px h-100px rounded-circle border border-2 border-dotted border-primary d-flex justify-content-center align-items-center">
                                    <i class="fa-solid fa-train fs-4x text-primary"></i>
                                    <span class="pulse-ring border-5"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="position-relative">
                        <div class="position-absolute bottom-0 start-50 translate-middle-x">
                            <span id="clock-hours" class="fs-2x text-blue-400 fw-semibold">23</span>
                            <span class="animation-blink">
                                <span class="animation-blink-clock fs-2x text-blue-400 fw-semibold">:</span>
                            </span>
                            <span id="clock-minutes" class="fs-2x text-blue-400 fw-semibold">32</span>
                            <small id="clock-seconds" class="fs-3 ms-1 text-color-eva-arrival d-none">37</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($planning->status == 'arrival')
    <div id="kt_carousel_1_carousel" class="carousel carousel-custom slide" data-bs-ride="carousel" data-bs-interval="5000">
        <div class="carousel-inner pt-8">
            <div class="carousel-item active">
                <div class="bg-gray-300 w-100 h-100">
                    <div class="d-flex flex-row justify-content-around align-items-end bg-gray-800 h-150px">
                        <div class="fs-4x text-white fw-bolder">TER 830690</div>
                        <div class="d-flex flex-row align-items-center text-white">
                            <div class="symbol symbol-50px me-3">
                                <img src="{{ $planning->ligne->ligne->stationEnd->weather->icon_url ?? null }}" alt="" class="w-80px h-80px">
                            </div>
                            <div class="d-flex flex-column">
                                <span class="fs-2x">Température extérieur:</span>
                                <div class="fs-4x">{{ $planning->ligne->ligne->stationEnd->weather->temperature ?? "NaN" }} °C</div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-row h-500px w-100">
                        <div class="d-flex flex-column justify-content-center px-10">
                            <div class="fw-bolder text-pink-800 fs-5tx">{{ $planning->ligne->ligne->stationEnd->name }}</div>
                            <div class="fs-4x text-gray-400">Terminus</div>
                        </div>
                    </div>
                    <div class="position-relative">
                        <div class="position-absolute bottom-0 start-50 translate-middle-x">
                            <span id="clock-hours" class="fs-2x text-blue-400 fw-semibold">23</span>
                            <span class="animation-blink">
                                <span class="animation-blink-clock fs-2x text-blue-400 fw-semibold">:</span>
                            </span>
                            <span id="clock-minutes" class="fs-2x text-blue-400 fw-semibold">32</span>
                            <small id="clock-seconds" class="fs-3 ms-1 text-color-eva-arrival d-none">37</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($planning->status == 'retarded')

@else

@endif
