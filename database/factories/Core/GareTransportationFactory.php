<?php

namespace Database\Factories\Core;

use App\Models\Core\Gare;
use App\Models\Core\GareTransportation;
use Illuminate\Database\Eloquent\Factories\Factory;

class GareTransportationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GareTransportation::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'type' => $this->faker->randomElement(['ter', 'tgv', 'intercite', 'transilien']),
            'gare_id' => Gare::factory(),
        ];
    }
}
