<?php

namespace App\Models\User;

use App\Models\Core\Hub;
use App\Models\Core\Ligne;
use App\Models\Core\Mouvement;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserLigne extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    protected $casts = [
        'date_achat' => 'datetime',
    ];

    protected $appends = ['date_achat_format'];

    public static function calcNbDepartJour(UserLigne $ligne, Hub $hub): int
    {
        $nb_hour_to_min = convertHoursToMinutes($hub->gare->time_day_work);
        $temps_trajet = $ligne->ligne->time_min;

        return intval($nb_hour_to_min / $temps_trajet);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function userHub()
    {
        return $this->belongsTo(UserHub::class, 'user_hub_id');
    }

    public function ligne()
    {
        return $this->belongsTo(Ligne::class, 'ligne_id');
    }

    public function tarifs()
    {
        return $this->hasMany(UserLigneTarif::class);
    }

    public function composition()
    {
        return $this->hasOne(UserCompositionTrack::class, 'user_composition_id');
    }

    public function mouvements()
    {
        return $this->hasMany(Mouvement::class);
    }

    public function plannings()
    {
        return $this->hasMany(UserPlanning::class);
    }

    public function user_engine()
    {
        return $this->belongsTo(UserEngine::class, 'user_engine_id');
    }

    public function livraisons(): HasMany
    {
        return $this->hasMany(UserDelivery::class);
    }

    public function skills()
    {
        return $this->hasMany(UserLigneSkill::class);
    }

    public function getDateAchatFormatAttribute()
    {
        return $this->date_achat->format('d/m/Y à H:i');
    }

    public function calcCA($nb_sub_day = null)
    {
        if ($nb_sub_day) {
            if($nb_sub_day != 0) {
                return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'revenue')
                    ->whereBetween('created_at', [now()->subDays($nb_sub_day)->startOfDay(), now()->subDays($nb_sub_day)->endOfDay()])
                    ->sum('amount');
            } else {
                return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'revenue')
                    ->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
                    ->sum('amount');
            }
        } else {
            return $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_account', 'revenue')
                ->sum('amount');
        }
    }

    public function calcCABilletterie($nb_sub_day = null)
    {
        if ($nb_sub_day) {
            if($nb_sub_day != 0) {
                return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'revenue')
                    ->where('type_mvm', 'billetterie')
                    ->whereBetween('created_at', [now()->subDays($nb_sub_day)->startOfDay(), now()->subDays($nb_sub_day)->endOfDay()])
                    ->sum('amount');
            } else {
                return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'revenue')
                    ->where('type_mvm', 'billetterie')
                    ->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
                    ->sum('amount');
            }
        } else {
            return $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_account', 'revenue')
                ->where('type_mvm', 'billetterie')
                ->sum('amount');
        }
    }

    public function calcCout($nb_sub_day = null)
    {
        if ($nb_sub_day) {
            if($nb_sub_day != 0) {
                $gasoil = $this->mouvements()
                        ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'charge')
                    ->where('type_mvm', 'gasoil')
                    ->whereBetween('created_at', [now()->subDays($nb_sub_day)->startOfDay(), now()->subDays($nb_sub_day)->endOfDay()])
                    ->sum('amount');

                $electricite = $this->mouvements()
                        ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'charge')
                    ->where('type_mvm', 'electricite')
                    ->whereBetween('created_at', [now()->subDays($nb_sub_day)->startOfDay(), now()->subDays($nb_sub_day)->endOfDay()])
                    ->sum('amount');

                $taxe = $this->mouvements()
                        ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'charge')
                    ->where('type_mvm', 'taxe')
                    ->whereBetween('created_at', [now()->subDays($nb_sub_day)->startOfDay(), now()->subDays($nb_sub_day)->endOfDay()])
                    ->sum('amount');
            } else {
                $gasoil = $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'charge')
                    ->where('type_mvm', 'gasoil')
                    ->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
                    ->sum('amount');

                $electricite = $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'charge')
                    ->where('type_mvm', 'electricite')
                    ->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
                    ->sum('amount');

                $taxe = $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'charge')
                    ->where('type_mvm', 'taxe')
                    ->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
                    ->sum('amount');
            }

        } else {
            $gasoil = $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_account', 'charge')
                ->where('type_mvm', 'gasoil')
                ->sum('amount');

            $electricite = $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_account', 'charge')
                ->where('type_mvm', 'electricite')
                ->sum('amount');

            $taxe = $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_account', 'charge')
                ->where('type_mvm', 'taxe')
                ->sum('amount');
        }

        return $gasoil + $electricite + $taxe;
    }

    public function calcFrais(string $type_frais, $nb_sub_day = null)
    {
        if ($nb_sub_day) {
            if($nb_sub_day != 0) {
                return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'charge')
                    ->where('type_mvm', $type_frais)
                    ->whereBetween('created_at', [now()->subDays($nb_sub_day)->startOfDay(), now()->subDays($nb_sub_day)->endOfDay()])
                    ->sum('amount');
            } else {
                return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'charge')
                    ->where('type_mvm', $type_frais)
                    ->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
                    ->sum('amount');
            }
        } else {
            return $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_account', 'charge')
                ->where('type_mvm', $type_frais)
                ->sum('amount');
        }
    }

    public function calcCoutIncident($nb_sub_day = null)
    {
        if ($nb_sub_day) {
            if($nb_sub_day != 0) {
                return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'charge')
                    ->where('type_mvm', 'maintenance_vehicule')
                    ->whereBetween('created_at', [now()->subDays($nb_sub_day)->startOfDay(), now()->subDays($nb_sub_day)->endOfDay()])
                    ->sum('amount');
            } else {
                return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'charge')
                    ->where('type_mvm', 'maintenance_vehicule')
                    ->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
                    ->sum('amount');
            }
        } else {
            return $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_account', 'charge')
                ->where('type_mvm', 'maintenance_vehicule')
                ->sum('amount');
        }
    }

    public function calcRentTrajetAux($nb_sub_day = null)
    {
        if ($nb_sub_day) {
            if($nb_sub_day != 0) {
                return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'revenue')
                    ->where('type_mvm', 'rent_trajet_aux')
                    ->whereBetween('created_at', [now()->subDays($nb_sub_day)->startOfDay(), now()->subDays($nb_sub_day)->endOfDay()])
                    ->sum('amount');
            } else {
                return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_account', 'revenue')
                    ->where('type_mvm', 'rent_trajet_aux')
                    ->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
                    ->sum('amount');
            }
        } else {
            return $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_account', 'revenue')
                ->where('type_mvm', 'rent_trajet_aux')
                ->sum('amount');
        }
    }

    public function calcBenefice($nb_sub_day = null)
    {
        if ($nb_sub_day) {
            if($nb_sub_day != 0) {
                $billetterie = $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_mvm', 'billetterie')
                    ->whereBetween('created_at', [now()->subDays($nb_sub_day)->startOfDay(), now()->subDays($nb_sub_day)->endOfDay()])
                    ->sum('amount');

                $cout = $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_mvm', 'cout_trajet')
                    ->whereBetween('created_at', [now()->subDays($nb_sub_day)->startOfDay(), now()->subDays($nb_sub_day)->endOfDay()])
                    ->sum('amount');
            } else {
                $billetterie = $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_mvm', 'billetterie')
                    ->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
                    ->sum('amount');

                $cout = $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                    ->where('type_mvm', 'cout_trajet')
                    ->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
                    ->sum('amount');
            }

        } else {
            $billetterie = $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_mvm', 'billetterie')
                ->sum('amount');

            $cout = $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_mvm', 'cout_trajet')
                ->sum('amount');

        }
        return $billetterie - $cout;
    }

    public function CAseven($type = null)
    {
        if ($type) {
            return $this->mouvements()
                    ->where('user_ligne_id', $this->id)
                ->where('type_mvm', $type)
                ->whereBetween('created_at', [now()->subDays(7)->startOfDay(), now()->endOfDay()])
                ->sum('amount');
        } else {
            return $this->mouvements()
                ->where('user_ligne_id', $this->id)
                ->where('type_account', 'revenue')
                ->whereBetween('created_at', [now()->subDays(7)->startOfDay(), now()->endOfDay()])
                ->sum('amount');
        }
    }

    public function calcResultat($nb_sub_day = null)
    {
        if (isset($nb_sub_day)) {
            return $this->calcCA($nb_sub_day) - $this->calcCout($nb_sub_day);
        } else {
            return $this->calcCA() - $this->calcCout();
        }
    }

    public function calcResultatSeven()
    {
        $ca = $this->CAseven();
        $cout = $this->CAseven('electricite') + $this->CAseven('gasoil') + $this->CAseven('taxe') + $this->CAseven('maintenance_vehicule') + $this->CAseven('cout_trajet');

        return $ca - $cout;
    }

    public function simulateSelling()
    {
        return ($this->ligne->price / 2) + $this->calcResultatSeven();
    }

    public function calcOffreRestante(Carbon $from = null, Carbon $to = null)
    {
        $sum = 0;
        if ($from && $to) {
            $sum += $this->tarifs()
                ->whereBetween('date_tarif', [$to->endOfDay(), $from->startOfDay()])
                ->sum('offre');
        } else {
            $sum += $this->tarifs()->sum('offre');
        }

        return $sum;
    }

    public function calcDemande(Carbon $from = null, Carbon $to = null)
    {
        $sum = 0;
        if ($from && $to) {
            $sum += $this->tarifs()
                ->whereBetween('date_tarif', [$to->endOfDay(), $from->startOfDay()])
                ->sum('demande');
        } else {
            $sum += $this->tarifs()->sum('demande');
        }

        return $sum;
    }

    public function sumPassengersFirst()
    {
        $sum = 0;

        foreach ($this->plannings as $planning) {
            $sum += $planning->passengers()->where('type', 'first')->sum('nb_passengers');
        }

        return $sum;
    }

    public function sumPassengersSecond()
    {
        $sum = 0;

        foreach ($this->plannings as $planning) {
            $sum += $planning->passengers()->where('type', 'second')->sum('nb_passengers');
        }

        return $sum;
    }

    public function calcAvgPrice($nb_day = null)
    {
        if (isset($nb_day)) {
            return $this->tarifs()
                ->where('date_tarif', now()->subDays($nb_day)->startOfDay())
                ->avg('price');
        } else {
            return $this->tarifs()->where('date_tarif', now()->startOfDay())->avg('price');
        }
    }

    public function calcAvgDemande($nb_day = null)
    {
        if (isset($nb_day)) {
            return $this->tarifs()
                ->where('date_tarif', now()->subDays($nb_day)->startOfDay())
                ->avg('demande');
        } else {
            return $this->tarifs()->where('date_tarif', now()->startOfDay())->avg('demande');
        }
    }

    public function calcPrevCA($nb_day = null)
    {
        if (isset($nb_day)) {
            $avg_demande = $this->calcAvgDemande($nb_day);
            $avg_price = $this->calcAvgPrice($nb_day);

            return $avg_demande * $avg_price;
        } else {
            $avg_demande = $this->calcAvgDemande();
            $avg_price = $this->calcAvgPrice();

            return $avg_demande * $avg_price;
        }
    }

    public function calcPerformance()
    {
        $yest_result = $this->calcBenefice(1);
        $ref_result = $this->calcBenefice(2);

        if($yest_result > $ref_result) {
            return "<i class='fa-solid fa-arrow-up text-success fs-2'></i>";
        } elseif($yest_result == $ref_result) {
            return "<i class='fa-solid fa-arrow-right text-warning fs-2'></i>";
        } else {
            return "<i class='fa-solid fa-arrow-down text-danger fs-2'></i>";
        }
    }
}
