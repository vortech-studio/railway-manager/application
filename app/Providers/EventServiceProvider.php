<?php

namespace App\Providers;

use App\Listeners\AchievementAwardedListener;
use App\Listeners\JobPushedListener;
use App\Models\User;
use App\Models\User\UserEngine;
use App\Models\User\UserHub;
use App\Models\User\UserTechnicentre;
use App\Observer\UserEngineObserver;
use App\Observer\UserLigneObserver;
use App\Observer\UserObserver;
use App\Observer\UserTechnicentreObserver;
use App\Services\BadgeSubscriber;
use App\UserHubObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use Laravel\Horizon\Events\JobPushed;
use LevelUp\Experience\Events\AchievementAwarded;
use SocialiteProviders\Discord\DiscordExtendSocialite;
use SocialiteProviders\Facebook\FacebookExtendSocialite;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            // add your listeners (aka providers) here
            \SocialiteProviders\Google\GoogleExtendSocialite::class . '@handle',
            FacebookExtendSocialite::class . '@handle',
            DiscordExtendSocialite::class . '@handle',
        ],
        AchievementAwarded::class => [
            AchievementAwardedListener::class,
        ],
        JobPushed::class => [
            JobPushedListener::class,
        ],
    ];

    protected $subscribe = [
        BadgeSubscriber::class,
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        User::observe(UserObserver::class);
        UserHub::observe(\App\Observer\UserHubObserver::class);
        UserTechnicentre::observe(UserTechnicentreObserver::class);
        UserEngine::observe(UserEngineObserver::class);
        //User\UserLigne::observe(UserLigneObserver::class);
        User\UserLigne::observe(UserLigneObserver::class);
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
