<?php

namespace App\Models\User;

use App\Models\Core\Mouvement;
use App\Models\User;
use App\Traits\SubventionTrait;
use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $appends = ['valorisation'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function situation()
    {
        return $this->hasMany(UserCompanySituation::class, 'user_company_id');
    }

    public function mouvement()
    {
        return $this->hasMany(Mouvement::class, 'user_company_id');
    }

    public static function automatedCreate(User $user)
    {
        return self::create([
            'user_id' => $user->id,
            "subvention" => SubventionTrait::getSubvention(0),
        ]);
    }

    public function getValorisationAttribute()
    {
        $sum_hub = 0;
        $sum_ligne = 0;
        $sum_engine = 0;

        foreach ($this->user->hubs()->get() as $hub) {
            $sum_hub += $hub->simulateSelling();
        }

        foreach ($this->user->lignes()->get() as $ligne) {
            $sum_ligne += $ligne->simulateSelling();
        }

        foreach ($this->user->engines()->get() as $engine) {
            $sum_engine += $engine->calcSell();
        }

        return $sum_hub + $sum_ligne + $sum_engine + $this->user->argent;
    }

    public function getValueToPercent($value)
    {
        return $value * 10;
    }
}
