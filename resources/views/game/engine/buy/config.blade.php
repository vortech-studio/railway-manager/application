@extends("game.template")

@section("css")
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/mouvement/achat_materiel.png') }}" alt="">
            </div>
            <x-base.underline
                title="Achat d'un matériel roulant"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.engine.partials.menu")
    </div>
    <form action="{{ route('engine.buy.checkout.confirm') }}" method="post">
        @csrf
        <div class="card shadow-lg bg-gray-300 mb-10">
            <div class="card-body">
                <input type="hidden" name="engine_id" value="{{ $engine->id }}">
                <input type="hidden" name="qte" value="{{ $qte }}">
                <div class="d-flex flex-column bg-gray-500 rounded-3 p-5 mb-5">
                    <div class="d-flex flex-row border-bottom border-bottom-gray-900 justify-content-between align-items-center pb-5">
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-30px me-3">
                                <img alt="Logo" src="{{ asset('/storage/logos/logo_'.$engine->type_train.'.svg') }}"/>
                            </div>
                            <div class="fw-semibold fs-3 text-white name me-10">{{ $engine->name }}</div>
                            <div class="d-flex flex-row align-items-center me-3">
                                <label for="qte" class="form-label fs-6 fw-bold text-white me-1">Quantité:</label>
                                <input type="text" class="form-control form-control-sm form-control-solid" id="qte" name="qte" value="{{ $qte }}">
                            </div>
                            <div class="d-flex flex-row align-items-center me-3">
                                <label for="user_hub_id" class="form-label fs-6 fw-bold text-white me-1 required">Hub de livraison:</label>
                                <select class="form-control form-control-sm selectpicker" name="user_hub_id" data-placeholder="--- Selectionner un hub ---" required>
                                    <option value=""></option>
                                    @foreach(\App\Models\User\UserHub::where('active', true)->get() as $hub)
                                        <option value="{{ $hub->id }}">{{ $hub->hub->gare->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="d-flex flex-row align-items-center me-3 text-white">
                                <span class="me-2">Prix public: <span class="fw-bold">{{ eur($engine->price_achat) }}</span></span>
                            </div>
                        </div>
                        <a href="{{ route('engine.buy.checkout') }}" data-action="trash" class="btn btn-flush"><i class="fa-solid fa-trash fs-1 text-danger"></i> </a>
                    </div>
                    <div class="d-flex flex-row align-items-center p-10 text-white">
                        <div class="me-4">
                            <img alt="Logo" src="{{ $engine->image_url }}" class="w-120px"/>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center w-75">
                            <div class="d-flex flex-column">
                                <div class="fw-bolder fs-2">Informations</div>
                                <div class="d-flex flex-row mb-1">
                                    <span class="me-2">Mise en service: <span class="fw-bold">{{ $engine->technical->start_exploi->year }}</span></span>
                                    <span class="me-2">Place: <span class="fw-bold">{{ $engine->nb_passager ?? "Aucun" }}</span></span>
                                </div>
                                <div class="d-flex flex-row mb-1">
                                    <span class="me-2">Rayon d'action: <span class="fw-bold">{{ round($engine->calcMaxRuntimeEngine(), 2) }} Km</span></span>
                                    <span class="me-2">Vitesse: <span class="fw-bold">{{ $engine->velocity }} Km/h</span></span>
                                </div>
                                <div class="d-flex flex-row mb-1">
                                    <span class="me-2">Vitesse d'usure: <span class="fw-bold">{{ $engine->calcVitesseUsure() }} % / 100h</span></span>
                                </div>
                            </div>
                            <div class="d-flex flex-column">
                                @if($engine->type_train == 'ter')
                                    <div class="d-flex flex-row mb-1">
                                        <span class="me-2">Classe Unique: <span class="fw-bold">{{ $engine->nb_passager }}</span></span>
                                    </div>
                                @elseif($engine->type_train == 'other')
                                    <div class="d-flex flex-row mb-1">
                                        <span class="me-2">Classe Unique: <span class="fw-bold">{{ $engine->nb_passager }}</span></span>
                                    </div>
                                @else
                                    <div class="d-flex flex-row mb-1">
                                        <span class="me-2">1ere Classe: <span class="fw-bold">{{ intval($engine->nb_passager * 20 /100) }}</span></span>
                                    </div>
                                    <div class="d-flex flex-row mb-1">
                                        <span class="me-2">2eme Classe: <span class="fw-bold">{{ intval($engine->nb_passager * 80 /100) }}</span></span>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if($engine->type_engine == 'automotrice')
                    <div class="d-flex flex-row justify-content-center align-items-end h-auto py-3 bg-gray-500 rounded-3 mb-5">
                        @for($w = 0; $w <= $engine->technical->nb_wagon; $w++)
                            <img src="{{ asset('/storage/engines/automotrice/'.$engine->slug.'-'.$w.'.gif') }}" alt="">
                        @endfor
                    </div>
                @endif
                <!-- TODO: Bloc choix de livrée -->
                <div class="d-flex flex-column align-items-center h-auto py-3 bg-gray-500 rounded-3 mb-5">
                    <div class="fw-bolder fs-1 fs-underline mb-3">Récapitulatif de votre achat</div>
                    <div class="d-flex flex-column w-450px lozad" id="cart">
                        <div id="product">
                            <div class="d-flex flex-row justify-content-between align-items-center mb-1">
                                <span class="fs-3">{{ $engine->name }} <small>X {{ $qte }}</small></span>
                                <span class="fw-semibold">{{ eur($engine->price_achat * $qte) }}</span>
                            </div>
                        </div>
                        <div id="livre" class="d-none">
                            <div class="d-flex flex-row justify-content-between align-items-center mb-1">
                                <span class="fs-3">Livré TER <small>X 1</small></span>
                                <span class="fw-semibold">0 €</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-1">
                                <span class="fs-3">Livré TER Pays de la loire <small>X 1</small></span>
                                <span class="fw-semibold">0 €</span>
                            </div>
                        </div>

                        <div class="separator border-gray-800 my-10"></div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-1">
                            <span class="fs-3">Sous Total</span>
                            <span class="fw-semibold" data-content="subtotal">{{ eur($engine->calcCart($qte, 'subtotal')) }}</span>
                        </div>
                        @if($engine->in_reduction)
                        <div class="d-flex flex-row justify-content-between align-items-center mb-1">
                            <span class="fs-3">Réduction ({{ $engine->percent_reduction }}):</span>
                            <span class="fw-semibold" data-content="reduc">{{ eur($engine->calcCart($qte, 'reduction')) }}</span>
                        </div>
                        @endif
                        <div class="d-flex flex-row justify-content-between align-items-center mb-1">
                            <span class="fs-3">Subvention (<span data-content="subvention_percent">{{ auth()->user()->company->subvention }} %</span>):</span>
                            <span class="fw-semibold" data-content="subvention">{{ eur($engine->calcCart($qte, 'subvention')) }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                            <span class="fs-1 fw-bold">Total à payer</span>
                            <span class="fw-bold fs-1 text-danger" data-content="total_price">{{ eur($engine->calcCart($qte, 'total')) }}</span>
                        </div>
                        <input type="hidden" name="engine_id" value="{{ $engine->id }}">
                        <!-- TODO: Input de livrée choisie -->
                        <button type="submit" class="btn btn-primary">Valider l'achat</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script type="text/javascript">
        @if($engine->in_reduction)
        document.querySelector('#qte').addEventListener('change', e => {
            e.preventDefault()
            let qte = e.target.value
            let subtotal = {{ $engine->price_achat }} * qte;
            let reduc = subtotal - {{ $engine->percent_reduction }} / 100;
            let subvention = reduc * {{ auth()->user()->company->subvention }} / 100;
            let total = reduc - subvention;
            document.querySelector('[data-content="subtotal"]').innerHTML = new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'eur'}).format(subtotal)
            document.querySelector('[data-content="reduc"]').innerHTML = new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'eur'}).format(reduc)
            document.querySelector('[data-content="subvention"]').innerHTML = new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'eur'}).format(subvention)
            document.querySelector('[data-content="total_price"]').innerHTML = new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'eur'}).format(total)
        })
        @else
        document.querySelector('#qte').addEventListener('change', e => {
            e.preventDefault()
            let qte = e.target.value
            let subtotal = {{ $engine->price_achat }} * qte;
            let subvention = subtotal * {{ auth()->user()->company->subvention }} / 100;
            let total = subtotal - subvention;
            document.querySelector('[data-content="subtotal"]').innerHTML = new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'eur'}).format(subtotal)
            document.querySelector('[data-content="subvention"]').innerHTML = new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'eur'}).format(subvention)
            document.querySelector('[data-content="total_price"]').innerHTML = new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'eur'}).format(total)
        })
        @endif
    </script>
@endsection
