<?php

namespace App\Imports;

use App\Models\Core\GareEquipement;
use Maatwebsite\Excel\Concerns\ToModel;

class GareEquipementImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new GareEquipement([
            "id" => $row["id"],
            "code_uic" => $row["code_uic"],
            "type_equipement" => $row["type_equipement"],
            "gare_id" => $row["gare_id"],
        ]);
    }
}
