<?php

namespace App\Models\User;

use App\Jobs\Game\Maintenance\EndMaintenanceJob;
use App\Models\Core\Engine;
use App\Models\Core\EngineComposition;
use App\Models\User;
use App\Notifications\Game\SendMessageNotification;
use App\Traits\UserEngineTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class UserEngine extends Model
{
    use UserEngineTrait;
    public $timestamps = false;
    protected $guarded = [];

    protected $casts = [
        'date_achat' => 'datetime',
    ];

    protected $appends = [
        "rentability",
        "nombre_trajet",
        'heure_trajet',
        'status_text',
        'status_icon',
        'status_color',
        'status_format',
        'date_achat_format',
        'other_info',
        'total_distance'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function engine()
    {
        return $this->belongsTo(Engine::class, 'engine_id');
    }

    public function user_hub()
    {
        return $this->belongsTo(UserHub::class, 'user_hub_id');
    }

    public function plannings()
    {
        return $this->hasMany(UserPlanning::class, 'user_engine_id');
    }

    public function planning_constructors(): HasMany
    {
        return $this->hasMany(UserPlanningConstructor::class);
    }

    public function user_ligne()
    {
        return $this->hasOne(UserLigne::class);
    }

    public function user_rental()
    {
        return $this->hasOne(UserRental::class);
    }

    public function incidents()
    {
        return $this->hasMany(UserIncident::class, 'user_engine_id');
    }

    public function technicentre()
    {
        return $this->hasOne(UserTechnicentreEngine::class);
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(UserTechnicentreTask::class);
    }

    public function livraisons(): HasMany
    {
        return $this->hasMany(UserDelivery::class);
    }

    public function skills()
    {
        return $this->hasMany(UserEngineSkill::class);
    }

    public function calcUsureEngine()
    {
        return UserEngineTrait::calculerUsureTotale(
            $this->engine->calcVitesseUsure(),
            $this->plannings()->where('status', 'arrival')->sum('kilometer'),
            $this->max_runtime_engine
        );
    }

    public function calcUsureEngineColor()
    {
        $value = $this->calcUsureEngine();

        if($value >= 0 && $value <= 40) {
            return 'danger';
        } elseif($value > 40 && $value <= 68) {
            return 'warning';
        } else {
            return 'success';
        }
    }

    public function calcUtilisationEngine()
    {
        if($this->user_ligne()->count() != 0) {
            if ($this->user_ligne->nb_depart_jour != 0) {
                $nb_max_planning = $this->user_ligne->nb_depart_jour;
                $nb_planning = $this->plannings()->where('status', 'initialized')->count();

                return intval($nb_planning / $nb_max_planning * 100);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function calcAncienneteEngine()
    {
        if($this->technicentre->technicentre->tasks()->where('type', 'engine_cur')->exists()) {
            return UserEngineTrait::calculerIndiceAnciennete($this->date_achat, $this->technicentre->technicentre->tasks()->where('type', 'engine_cur')->orderBy('end_at', 'desc')->first()->end_at);
        } else {
            return UserEngineTrait::calculerIndiceAnciennete($this->date_achat);
        }
    }

    public function calcAmountMaintenancePrev()
    {
        return floatval($this->engine->price_maintenance);
    }

    public function calcAmountMaintenanceCur()
    {
        return floatval($this->engine->price_maintenance * ($this->engine->compositions()->sum('maintenance_repar_time') / 60 / 10));
    }

    public function calcResultat()
    {
        $sum = 0;
        foreach ($this->plannings()->where('status', 'arrival')->get() as $item) {
            $sum += $item->travel->calcResultat();
        }

        return $sum;
    }

    public function calcRentability()
    {
        if ($this->calcResultat() == 0) {
            return 0;
        } else {
            return $this->engine->price_achat / $this->calcResultat() * 100;
        }
    }

    public function calcTravelKilometer()
    {
        return $this->plannings()->where('status', 'arrival')->sum('kilometer');
    }

    public function calcLatestMaintenancePreventive()
    {
        return $this->technicentre->technicentre->tasks()->where('type', 'engine_prev')->orderBy('end_at', 'desc')->first();
    }

    public function calcLatestMaintenanceCurative()
    {
        return $this->technicentre->technicentre->tasks()->where('type', 'engine_cur')->orderBy('end_at', 'desc')->first();
    }

    public function getRentabilityAttribute()
    {
        return $this->calcRentability();
    }

    public function getNombreTrajetAttribute()
    {
        return $this->plannings()->where('status', 'arrival')->count();
    }

    public function getHeureTrajetAttribute()
    {
        $totalTime = 0;
        $arrivalPlannings = $this->plannings()->where('status', 'arrival')->get();

        if ($arrivalPlannings->isEmpty()) {
            return "00:00";
        }

        foreach ($arrivalPlannings as $planning) {
            $totalTime += $planning->ligne->ligne->time_min;
        }

        return Carbon::createFromTimestamp(0)->addMinutes($totalTime)->format('H:i');
    }

    public function getTotalDistanceAttribute()
    {
        return $this->plannings()->where('status', 'arrival')->sum('kilometer');
    }

    public function getStatusTextAttribute()
    {
        return match($this->status) {
            "free" => "Disponible",
            "in_maintenance" => "En maintenance",
            "travel" => "En trajet",
            "default" => "En panne",
            default => "Indisponible",
        };
    }

    public function getStatusColorAttribute()
    {
        return match($this->status) {
            "free" => "success",
            "in_maintenance" => "warning",
            "travel" => "info",
            "default" => "danger",
            default => "secondary",
        };
    }

    public function getStatusIconAttribute()
    {
        return match($this->status) {
            "free" => "check",
            "in_maintenance" => "cogs",
            "travel" => "route",
            "default" => "xmark",
            default => "question",
        };
    }

    public function getStatusFormatAttribute($labeled = true)
    {
        if($labeled) {
            return '<span class="badge badge-light-'.$this->getStatusColorAttribute().' fw-bold fs-8 px-2 py-1 ms-2"><i class="fa-solid fa-'.$this->getStatusIconAttribute().' fs-8 me-3"></i>'.$this->getStatusTextAttribute().'</span>';
        } else {
            return '<i class="fa-solid fa-'.$this->getStatusIconAttribute().' fs-3 text-'.$this->getStatusColorAttribute().' me-3"></i> '.$this->getStatusTextAttribute();
        }
    }

    public function getDateAchatFormatAttribute()
    {
        return Carbon::parse($this->date_achat)->format('d/m/Y');
    }

    public function getOtherInfoAttribute()
    {
        return [
            "latestMaintenancePrev" => $this->calcLatestMaintenancePreventive(),
            "latestMaintenanceCur" => $this->calcLatestMaintenanceCurative(),
            "compositions" => $this->engine->getComposition(),
            "resulat" => eur($this->calcResultat()),
            "latestPosition" => $this->getLatestPosition(),
            "maxCharged" => $this->getMaxCharged(),
            "speedAverage" => $this->user_ligne ?$this->user_ligne->ligne->calculVitesseMoyenne() : null,
            "usedPlanning" => $this->user_ligne ? ($this->user_ligne->nb_depart_jour * 7)*$this->plannings()->whereBetween('date_depart', [now()->startOfWeek()->startOfDay(), now()->endOfWeek()->endOfDay()])->count()/100: null,
            "usure" => [
                "color" => $this->calcUsureEngineColor(),
                "value" => $this->calcUsureEngine()
            ]
        ];
    }

    public static function defineNumberEngine()
    {
        return \Str::upper(\Str::random(1)."-".\Str::random(3)."-".\Str::random(2));
    }

    /**
     * Creates a maintenance for the given type of maintenance.
     *
     * @param string $type_maintenance The type of maintenance to create.
     * @return UserTechnicentreTask The created task for the maintenance.
     */
    public function createMaintenance(string $typeMaintenance)
    {
        $technicentre = $this->technicentre;

        $this->update([
            "available" => false,
            "in_maintenance" => true,
            "status" => 'in_maintenance',
        ]);

        $sumTime = $this->defineCheckTime($typeMaintenance);
        $type = $typeMaintenance == 'preventif' ? 'engine_prev' : 'engine_cur';

        $task = UserTechnicentreTask::create([
            "type" => $type,
            "status" => "plannified",
            "start_at" => now()->addMinute()->startOfMinute(),
            "end_at" => now()->addMinutes($sumTime)->startOfMinute(),
            "user_technicentre_id" => $technicentre->user_technicentre_id,
            "user_technicentre_staf_id" => UserTechnicentreStaf::where('user_technicentre_id', $technicentre->user_technicentre_id)->where('job', 'LIKE', '%Opérateur Maintenance%')->where('status', 'inactive')->get()->random()->id,
            "user_engine_id" => $this->id,
        ]);

        foreach ($this->engine->compositions as $composition) {
            if (!UserTechnicentreTasking::where('user_technicentre_task_id', $task->id)->exists()) {
                UserTechnicentreTasking::create([
                    "subject" => EngineComposition::generateTaskPreventif($composition->name),
                    "status" => "planified",
                    "start_at" => $task->start_at->addMinute()->startOfMinute(),
                    "end_at" => $task->start_at->addMinutes($composition->maintenance_repar_time / $this->user_hub->technicentre->level)->startOfMinute(),
                    "user_technicentre_task_id" => $task->id,
                ]);
            } else {
                $tasking = $task->tasks()->orderBy('start_at', 'desc')->first();
                UserTechnicentreTasking::create([
                    "subject" => EngineComposition::generateTaskPreventif($composition->name),
                    "status" => "planified",
                    "start_at" => $tasking->end_at->startOfMinute(),
                    "end_at" => $tasking->end_at->addMinutes($composition->maintenance_repar_time / $this->user_hub->technicentre->level)->startOfMinute(),
                    "user_technicentre_task_id" => $task->id,
                ]);
            }
        }

        $task->engine->user->notify(new SendMessageNotification(
            'Maintenance en cours',
            "Une maintenance est actuellement en cours dans le technicentre de " . $this->user_hub->hub->gare->name,
            "warning",
            asset('storage/icons/maintenance_target.png'),
            null,
            ["engine", "vigimat"]
        ));

        dispatch(new EndMaintenanceJob($task))->delay(now()->addMinutes($sumTime))->onQueue('maintenance');

        return $task;
    }

    /**
     * Crée une maintenance à partir d'une date donnée.
     *
     * @param string $typeMaintenance Le type de maintenance.
     * @param mixed $start_at La date de début de la maintenance.
     * @return UserTechnicentreTask La tâche de maintenance créée.
     */
    public function createMaintenanceFromDate(string $typeMaintenance, $start_at)
    {
        $technicentre = $this->technicentre;

        $sumTime = $this->defineCheckTime($typeMaintenance);
        $type = $typeMaintenance == 'preventif' ? 'engine_prev' : 'engine_cur';

        $task = UserTechnicentreTask::create([
            "type" => $type,
            "status" => "plannified",
            "start_at" => Carbon::createFromTimestamp(strtotime($start_at))->startOfDay(),
            "end_at" => Carbon::createFromTimestamp(strtotime($start_at))->addMinutes($sumTime)->startOfMinute(),
            "user_technicentre_id" => $technicentre->user_technicentre_id,
            "user_technicentre_staf_id" => UserTechnicentreStaf::where('user_technicentre_id', $technicentre->user_technicentre_id)->where('job', 'LIKE', '%Opérateur Maintenance%')->where('status', 'inactive')->get()->random()->id,
            "user_engine_id" => $this->id,
        ]);

        foreach ($this->engine->compositions as $composition) {
            if (!UserTechnicentreTasking::where('user_technicentre_task_id', $task->id)->exists()) {
                UserTechnicentreTasking::create([
                    "subject" => EngineComposition::generateTaskPreventif($composition->name),
                    "status" => "planified",
                    "start_at" => $task->start_at->addMinute()->startOfMinute(),
                    "end_at" => $task->start_at->addMinutes($composition->maintenance_repar_time / $this->user_hub->technicentre->level)->startOfMinute(),
                    "user_technicentre_task_id" => $task->id,
                ]);
            } else {
                $tasking = $task->tasks()->orderBy('start_at', 'desc')->first();
                UserTechnicentreTasking::create([
                    "subject" => EngineComposition::generateTaskPreventif($composition->name),
                    "status" => "planified",
                    "start_at" => $tasking->end_at->startOfMinute(),
                    "end_at" => $tasking->end_at->addMinutes($composition->maintenance_repar_time / $this->user_hub->technicentre->level)->startOfMinute(),
                    "user_technicentre_task_id" => $task->id,
                ]);
            }
        }

        $task->engine->user->notify(new SendMessageNotification(
            'Maintenance prévue',
            "Une maintenance à été enregistré dans le technicentre " . $this->user_hub->hub->gare->name,
            "info",
            asset('storage/icons/maintenance_target.png'),
            null,
            ["engine", "vigimat"]
        ));

        return $task;
    }

    public function transferToHub(int $toHub)
    {
        $this->update([
            "user_hub_id" => $toHub,
        ]);
    }

    public function calcSell()
    {
        return floatval(($this->engine->price_achat / 2) + ($this->calcResultat() * 0.3));
    }

    private function defineCheckTime(string $type_maintenance): float|int
    {
        if ($type_maintenance == 'preventif') {
            return intval($this->engine->compositions()->sum('maintenance_check_time')) / $this->user_hub->technicentre->level;
        } else {
            return intval($this->engine->compositions()->sum('maintenance_repar_time')) / $this->user_hub->technicentre->level;
        }
    }

    private function getLatestPosition()
    {
        if($this->status == 'free' || $this->status == 'in_maintenance') {
            return [$this->user_hub->hub->gare->latitude, $this->user_hub->hub->gare->longitude];
        } elseif ($this->status == 'travel') {
            $latestPlanning = $this->plannings()
                ->where('status', 'travel')
                ->first();
            $station = $latestPlanning->planning_stations()
                ->where('status', 'done')
                ->orderBy('arrival_at', 'desc')
                ->first();

            return [$station->ligneStation->gare->latitude, $station->ligneStation->gare->longitude];
        } else {
            return [];
        }
    }

    private function getMaxCharged()
    {
        $plannings = $this->plannings()
            ->where('status', 'arrival')
            ->get();

        $sum = 0;

        foreach ($plannings as $planning) {
            $sum = $planning->passengers()->max('nb_passengers');
        }

        return $sum;
    }

}
