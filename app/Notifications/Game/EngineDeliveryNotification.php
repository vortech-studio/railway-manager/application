<?php

namespace App\Notifications\Game;

use App\Models\User\UserEngine;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;

class EngineDeliveryNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public function __construct(public UserEngine $engine)
    {
    }

    public function via($notifiable): array
    {
        return ['database', 'broadcast', WebPushChannel::class];
    }

    public function toDatabase($notifiable): array
    {
        return [
            'type' => 'success',
            'icon' => 'ki-delivery-time',
            'title' => 'Nouvelle livraison',
            'description' => "Le matériel roulant ".$this->engine->engine->name." est maintenant disponible dans votre hub:" . $this->engine->user_hub->hub->gare->name,
            'time' => now(),
            'sector' => 'delivery'
        ];
    }

    public function toArray($notifiable): array
    {
        return [
            'type' => 'success',
            'icon' => 'ki-delivery-time',
            'title' => 'Nouvelle livraison',
            'description' => "Le matériel roulant ".$this->engine->engine->name." est maintenant disponible dans votre hub:" . $this->engine->user_hub->hub->gare->name,
            'time' => now(),
            'sector' => 'delivery'
        ];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage([
            'type' => 'success',
            'icon' => 'ki-delivery-time',
            'title' => 'Nouvelle livraison',
            'description' => "Le matériel roulant ".$this->engine->engine->name." est maintenant disponible dans votre hub:" . $this->engine->user_hub->hub->gare->name,
            'time' => now(),
            "user_id" => $this->engine->user_id,
            'sector' => 'delivery'
        ]);
    }

    public function broadcastOn()
    {
        return ['delivery-channel'];
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new \NotificationChannels\WebPush\WebPushMessage)
            ->title('Nouvelle livraison')
            ->icon(asset('/storage/icons/fast-delivery.png'))
            ->body("Le matériel roulant ".$this->engine->engine->name." est maintenant disponible dans votre hub:" . $this->engine->user_hub->hub->gare->name)
            ->action('Voir', 'notification_action');
    }
}
