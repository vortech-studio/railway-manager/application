window.mapboxgl.accessToken = 'pk.eyJ1Ijoidm9ydGVjaHN0dWRpbyIsImEiOiJjbG9icDIwOGswNXZnMnduMXg2NmZwMmc4In0.P0Vz5GqpKLfGPBTm1eD04w'
window.initMap = function initMap(center = [2.716370, 47.315787], zoom = 5) {
    return new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/vortechstudio/clokmdndz006v01plcq6d31pr',
        center: center,
        zoom: zoom
    })
}

window.formatForHub = function formatForHub(map, hub) {
    let el = document.createElement('div')
    el.className = 'marker'
    el.style.backgroundImage = `url('/storage/icons/station_hub.png')`;
    el.style.backgroundSize = '100%'
    el.style.width = '60px'
    el.style.height = '60px'


    let popup = new mapboxgl.Popup({closeOnClick: true, offset: 25})
        .setHTML(`
        <h3>${hub.name}</h3>
        <table class="table table-row-bordered table-row-gray-800 border border-gray-800 table-striped gy-2 gx-2 gs-2 mb-2">
            <thead>
                <tr>
                    <th>Commerce</th>
                    <th>Publicité</th>
                    <th>Parking</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>${hub.nb_slot_commerce}</td>
                    <td>${hub.nb_slot_pub}</td>
                    <td>${hub.nb_slot_parking}</td>
                </tr>
            </tbody>
        </table>
        <strong class="mb-1">Fréquence Actuel:</strong> ${hub.freq_actual} / <small>par heure</small><br>
        <strong class="me-1">Etat de la Gare: </strong> <span class="badge bg-${hub.status === true ? 'success' : 'danger'}">${hub.status === true ? 'Ouvert' : 'Fermer'}</span> <br>
        `)

    new mapboxgl.Marker(el)
        .setLngLat([hub.latitude, hub.longitude])
        .setPopup(popup)
        .addTo(map)

}

window.formatForLigne = function formatForLigne(map, ligne) {
    let sources = [];
    let typeLine = {
        "ter": {color: '#1257ce'},
        "tgv": {color: '#d83131'},
        "ic": {color: '#ad0082'},
        "othe": {color: '#000'},
    }


    ligne.forEach(l => {


        sources.push({
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'LineString',
                'coordinates': l.coordinates
            }
        })
    })

    map.addSource('route', {
        'type': 'geojson',
        'data': {
            'type': 'FeatureCollection',
            'features': sources
        }
    });

    map.addLayer({
        'id': 'route',
        'type': 'line',
        'source': 'route',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': '#1257ce',
            'line-width': 6
        }
    })
}

window.markerForStationEnd = function markerForStationEnd(map, ligne) {
    let el = document.createElement('div')
    el.className = 'marker'
    el.style.backgroundImage = `url('/storage/icons/station_end.png')`;
    el.style.backgroundSize = '100%'
    el.style.width = '40px'
    el.style.height = '40px'

    const popup = new mapboxgl.Popup({closeOnClick: true, offset: 25})
        .setHTML(`
        <h3>${ligne.name}</h3>
        <div class="d-flex flex-column">
            <div class="d-flex flex-row justify-content-between align-items-center p-2">
                <div class="fw-bolder">Départ:</div> ${ligne.station_start.name}
            </div>
            <div class="separator border-gray-700 my-1"></div>
            <div class="d-flex flex-row justify-content-between align-items-center p-2">
                <div class="fw-bolder">Arrivée:</div> ${ligne.station_end.name}
            </div>
            <div class="separator border-gray-700 my-1"></div>
            <div class="d-flex flex-row justify-content-between align-items-center p-2">
                <div class="fw-bolder"><i class="fa-solid fa-clock me-1"></i> Prochain départ:</div> ${ligne.next_departure}
            </div>
        </div>
        `)

    new mapboxgl.Marker(el)
        .setLngLat([ligne.station_end.latitude, ligne.station_end.longitude])
        .setPopup(popup)
        .addTo(map)
}
window.markerForStation = function markerForStation(map, station) {
    let el = document.createElement('div')
    el.className = 'marker'
    el.style.backgroundImage = `url('/storage/icons/station.png')`;
    el.style.backgroundSize = '100%'
    el.style.width = '30px'
    el.style.height = '30px'

    const popup = new mapboxgl.Popup({closeOnClick: true, offset: 25})
        .setHTML(`
        <h3>${station.name}</h3>
        <div>
            <span>Voy. Potentiel: </span> ${station.nb_voyageur} / <small>par heure</small>
        </div>
        `)

    new mapboxgl.Marker(el)
        .setLngLat([station.latitude, station.longitude])
        .setPopup(popup)
        .addTo(map)
}
window.markerForTrain = function markerForTrain(map, station) {
    let el = document.createElement('div')
    el.className = 'marker'
    el.style.backgroundImage = `url('/storage/icons/train_point.png')`;
    el.style.backgroundSize = '100%'
    el.style.width = '40px'
    el.style.height = '40px'

    new mapboxgl.Marker(el)
        .setOffset([0, -45])
        .setLngLat([station.latitude, station.longitude])
        .addTo(map)
}

window.zoomInLine = function zoomInLine(map, ligne) {
    const coordinates = ligne.coordinates

    const bounds = new mapboxgl.LngLatBounds(
        coordinates
    )

    for (const coord of coordinates) {
        bounds.extend(coord);
    }

    map.fitBounds(bounds, {
        padding: 50
    });
}




