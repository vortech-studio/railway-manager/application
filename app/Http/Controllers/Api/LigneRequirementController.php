<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\LigneRequirement;

class LigneRequirementController extends Controller
{
    public function destroy($ligne_id, $requirement_id)
    {
        $requirement = LigneRequirement::findOrFail($requirement_id);
        $requirement->delete();

        return response()->json([
            'message' => "Le hub a bien été supprimé de la ligne {$requirement->ligne->name}"
        ]);
    }
}
