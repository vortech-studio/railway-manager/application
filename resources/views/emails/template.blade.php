<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic
Product Version: 8.1.8
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->
<head>
    <base href="{{ route('home') }}"/>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="{{ asset('/assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="app-blank">
<!--begin::Theme mode setup on page load-->
<!--end::Theme mode setup on page load-->
<!--begin::Root-->
<div class="d-flex flex-column flex-root" id="kt_app_root">
    <!--begin::Wrapper-->
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Body-->
        <div class="scroll-y flex-column-fluid px-10 py-10" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_app_header_nav" data-kt-scroll-offset="5px" data-kt-scroll-save-state="true" style="background-color:#D5D9E2; --kt-scrollbar-color: #d9d0cc; --kt-scrollbar-hover-color: #d9d0cc">
            <!--begin::Email template-->
            <style>html, body {
                    padding: 0;
                    margin: 0;
                    font-family: Inter, Helvetica, "sans-serif";
                }

                a:hover {
                    color: #009ef7;
                }</style>
            <div id="#kt_app_body_content" style="background-image: url('{{ asset('/storage/other/wall-rm.png') }}') no-repeat center center; font-family:Arial,Helvetica,sans-serif; line-height: 1.5; min-height: 100%; font-weight: normal; font-size: 15px; color: #2F3044; margin:0; padding:0; width:100%;">
                <div style="background-color:#ffffff; padding: 45px 0 34px 0; border-radius: 24px; margin:40px auto; max-width: 600px;">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="auto" style="border-collapse:collapse">
                        <tbody>
                        <tr>
                            <td align="center" valign="center" style="text-align:center; padding-bottom: 10px">
                                <!--begin:Email content-->
                                <div style="text-align:center; margin:0 15px 34px 15px">
                                    <!--begin:Logo-->
                                    <div style="margin-bottom: 10px">
                                        <a href="https://railway-manager.ovh" rel="noopener" target="_blank">
                                            <img alt="Logo" src="{{ asset('/storage/logos/logo-long-color.png') }}" style="height: 35px" />
                                        </a>
                                    </div>
                                    <!--end:Logo-->
                                    <!--begin:Media-->
                                    <div style="margin-bottom: 15px">
                                        @yield("media_card")
                                    </div>
                                    <!--end:Media-->
                                    @yield("synopsis")
                                </div>
                                <!--end:Email content-->
                            </td>
                        </tr>
                        <tr style="display: flex; justify-content: center; margin:0 60px 35px 60px">
                            @yield("content")
                        </tr>
                        <tr style="background: #18083e;">
                            <td align="center" valign="center" style="text-align:center; padding-bottom: 20px;">
                                <a href="#" style="margin-right:10px">
                                    <img alt="Logo" src="{{ asset('/assets/media/icons/duotune/abstract/abs004.svg') }}" class="text-blue-600"/>
                                </a>
                                <a href="#" style="margin-right:10px">
                                    <img alt="Logo" src="{{ asset('/assets/media/icons/duotune/abstract/abs006.svg') }}" class="text-blue-500"/>
                                </a>
                                <a href="#" style="margin-right:10px">
                                    <img alt="Logo" src="{{ asset('/assets/media/icons/duotune/abstract/abs007.svg') }}" class="text-red-700"/>
                                </a>
                            </td>
                        </tr>
                        <tr></tr>
                        <tr>
                            <td align="center" valign="center" style="font-size: 13px; padding:0 15px; text-align:center; font-weight: 500; color: #A1A5B7;font-family:Arial,Helvetica,sans-serif">
                                <p>&copy; Copyright Vortechstudio 2023.
                                    <a href="https://vortechstudio.fr" rel="noopener" target="_blank" style="font-weight: 600;font-family:Arial,Helvetica,sans-serif">Unsubscribe</a>&nbsp; from newsletter.</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--end::Email template-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Wrapper-->
</div>
<!--end::Root-->
<!--begin::Javascript-->
<script>var hostUrl = "assets/";</script>
<!--end::Javascript-->
</body>
<!--end::Body-->
</html>
