<?php

namespace App\Http\Controllers\Game\Network;

use App\Http\Controllers\Controller;

class NetworkController extends Controller
{
    public function __invoke()
    {
        seo()->title(config('app.name') . ' - Réseau');
        return view('game.network.index');
    }
}
