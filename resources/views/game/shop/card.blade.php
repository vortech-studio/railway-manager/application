@extends("game.template")

@section("css")

@endsection

@section("info_bar")
@endsection

@section("content")
    @if($card->check_advantage)
        <div class="alert alert-dismissible bg-light-info d-flex flex-center flex-column py-10 px-10 px-lg-20 mb-10">
            <!--begin::Close-->
            <button type="button" class="position-absolute top-0 end-0 m-2 btn btn-icon btn-icon-danger" data-bs-dismiss="alert">
                <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
            </button>
            <!--end::Close-->

            <!--begin::Icon-->
            <i class="ki-duotone ki-information-5 fs-5tx text-info mb-5"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
            <!--end::Icon-->

            <!--begin::Wrapper-->
            <div class="text-center">
                <!--begin::Title-->
                <h1 class="fw-bold mb-5">Porte carte indisponible</h1>
                <!--end::Title-->

                <!--begin::Separator-->
                <div class="separator separator-dashed border-danger opacity-25 mb-5"></div>
                <!--end::Separator-->

                <!--begin::Content-->
                <div class="mb-9 text-dark">
                    Ce porte-carte a déjà été réclamé, vous ne pouvez pas le réclamer une seconde fois.
                </div>
                <!--end::Content-->

                <!--begin::Buttons-->
                <div class="d-flex flex-center flex-wrap">
                    <a href="{{ route('home') }}" class="btn btn-danger m-2">Retour au tableau de bord</a>
                </div>
                <!--end::Buttons-->
            </div>
            <!--end::Wrapper-->
        </div>
    @else
        <div class="d-flex flex-row justify-content-around align-items-center h-250px shadow-lg" style="background: url('{{ asset('/storage/other/wall-rm.png') }}')">
            <div>
                <div class="right-arrow"></div>
                <div class="shop-description">
                    <h3>Nouveau porte carte disponible !</h3>
                    <p>Le porte carte {{ $card->type_card }} est disponible.<br>Pour profiter de ces avantages, veuillez cliquer sur le bouton <strong>"Recevoir mes avantages"</strong></p>
                </div>
            </div>
            <div>
                <div class="symbol symbol-circle symbol-200px">
                    <img src="{{ asset('/storage/avatar/secretary/2.jpg') }}" alt="">
                </div>
            </div>
        </div>
        <div class="rounded-2 bg-gray-800 p-5 mb-10">
            <div class="d-flex flex-row justify-content-center align-items-center">
                @foreach($card->cards()->get() as $item)
                    <div class="d-flex flex-column justify-content-center rounded-2 bg-gray-400 border border-gray-700 p-5 m-5 w-auto h-200px">
                        <img src="{{ asset('/storage/icons/'.$item->cardHolder->type_avantage.'.png') }}" class="w-80px" alt="">
                        <div class="d-flex flex-column justify-content-end align-items-center bg-gray-800 text-white p-5 rounded-4">
                            {{ $item->cardHolder->name }}
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="d-flex flex-wrap justify-content-center align-items-center">
                <button class="btn btn-lg btn-rounded btn-info w-100" id="btnClaimCardHolder">Réclamer mes avantages</button>
            </div>
        </div>
    @endif
@endsection

@section("script")
    <script type="text/javascript">
        document.querySelector('#btnClaimCardHolder').addEventListener('click', e => {
            e.preventDefault()
            $.ajax({
                url: '{{ route('card.receive', $card->uuid) }}',
                method: 'POST',
                success: data => {
                    console.log(data)
                    Swal.fire({
                        title: 'Félicitation !',
                        text: 'Vous avez reçu vos avantages !',
                        icon: 'success',
                        confirmButtonText: 'Ok'
                    }).then(() => {
                        window.location.href = '{{ route('home') }}'
                    })
                }
            })
        })
    </script>
@endsection
