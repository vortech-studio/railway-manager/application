@extends("game.template")

@section("css")
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/maintenance.png') }}" alt="">
            </div>
            <x-base.underline
                title="Maintenance"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.engine.partials.menu")
    </div>
    <form action="{{ route('engine.maintenance.group.confirmMaintenance') }}" method="post">
        @csrf
        <div class="card shadow-lg mb-10">
            <div class="card-body">
                <div class="row align-items-center mb-10">
                    <div class="col-lg-6 col-sm-12">
                        <div class="text-center fs-2 fw-bolder">Usure de l'engin</div>
                        <div class="rounded pt-15 px-5 pb-5 shadow-box bg-gray-800 border border-3 border-gray-600 text-white mb-5">
                            <div class="mb-0">
                                <div id="slide_used"></div>
                                <input type="hidden" name="wear" value="0">

                                <div class="d-flex flex-row justify-content-between pt-15">
                                    <div class="fw-semibold mb-2 fs-2">Très Bon Etat</div>
                                    <div class="mb-2 fs-6 w-25">Sélectionne tous les appareils dont le taux d'usure est entre <span class="text-blue-400 fw-bolder" id="used_slide">20</span> % et 100 %</div>
                                    <div class="fw-semibold mb-2 fs-2">Très Mauvais Etat</div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center fs-2 fw-bolder">Note d'ancienneté</div>
                        <div class="rounded pt-15 px-5 pb-5 shadow-box bg-gray-800 border border-3 border-gray-600 text-white mb-5">
                            <div class="mb-0">
                                <div id="slide_ancien"></div>
                                <input type="hidden" name="mark" value="0">

                                <div class="d-flex flex-row justify-content-between pt-15">
                                    <div class="fw-semibold mb-2 fs-2">Neuf</div>
                                    <div class="mb-2 fs-6 w-250px">Sélectionne uniquement les appareils ayant une note d'ancienneté entre <span class="text-green-400 fw-bolder" id="mark_slide">1</span> et 5</div>
                                    <div class="fw-semibold mb-2 fs-2">Très Vieux</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="rounded-3 shadow-box bg-gray-800 border border-3 border-gray-300 text-white mb-5 py-10 px-5">
                            <div class="d-flex flex-row align-items-center mb-3">
                                <div class="symbol symbol-50 me-3">
                                    <img src="{{ asset('/storage/icons/train.png') }}" alt="">
                                </div>
                                <div class="fs-3 me-2">Nombre de matériels roulants:</div>
                                <div class="fw-bolder fs-3">0</div>
                            </div>
                            <div class="d-flex flex-row align-items-center mb-3">
                                <div class="symbol symbol-50 me-3">
                                    <img src="{{ asset('/storage/icons/maintenance.png') }}" alt="">
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="d-flex flex-row justify-content-between align-items-center w-100 mb-1">
                                        <div class="fs-3 me-2">Prix Check Prév:</div>
                                        <div class="fw-bolder fs-3" data-content="coast_maint_prev">0 €</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between align-items-center w-100 mb-1">
                                        <div class="fs-3 me-2">Prix Check Cura:</div>
                                        <div class="fw-bolder fs-3" data-content="coast_maint_cur">0 €</div>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex flex-row justify-content-around align-items-center">
                                <label for="selected_maintenance">Type de maintenance choisie</label>
                                <select name="type_maintenance" id="selected_maintenance" class="form-control form-control-sm selectpicker" data-placeholder="--- Choisir ---">
                                    <option value="preventif">Maintenance Préventive</option>
                                    <option value="curatif">Maintenance Curative</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-row-bordered rounded-3 gx-5 gy-5 gs-5">
                    <thead>
                    <tr class="bg-gray-900 text-white fs-semibold">
                        <th class="min-w-100px">
                            <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                <input class="form-check-input me-3" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_datatable_example_1 .form-check-input" value="1"/>
                                Engins
                            </div>
                        </th>
                        <th>Usure Moyenne</th>
                        <th>Ancienneté Moyenne</th>
                        <th>Incidents Récents</th>
                        <th>Coût Incident Récents</th>
                    </tr>
                    </thead>
                    <tbody id="engines">

                    </tbody>
                </table>
                <div class="d-flex flex-center">
                    <button type="submit" class="btn btn-primary w-75">Valider</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script type="text/javascript">
        let sliderUsure = document.querySelector("#slide_used");
        let sliderAncien = document.querySelector("#slide_ancien");

        noUiSlider.create(sliderUsure, {
            start: [0],
            step: 1,
            connect: true,
            tooltips: [wNumb({ decimals: 0 })],
            range: {
                "min": 0,
                "max": 100
            },
            format: wNumb({
                decimals: 0,
            }),
            pips: {
                mode: 'range',
                density: 3
            }
        });

        noUiSlider.create(sliderAncien, {
            start: [0],
            step: 1,
            connect: true,
            tooltips: [wNumb({ decimals: 0 })],
            range: {
                "min": 0,
                "max": 5
            },
            format: wNumb({
                decimals: 0,
            }),
            pips: {
                mode: 'range',
                density: 22
            }
        });

        let inputwear = document.querySelector('[name="wear"]')
        let inputmark = document.querySelector('[name="mark"]')

        sliderUsure.noUiSlider.on('update', (value) => {
            inputwear.value = value[0];
            document.querySelector('#used_slide').innerHTML = value[0];
            updateTable()
        })

        sliderAncien.noUiSlider.on('update', (value) => {
            inputmark.value = value[0];
            document.querySelector('#mark_slide').innerHTML = value[0];
            updateTable()
        })

        function updateTable() {
            $.ajax({
                url: '{{ route('api.game.maintenance.group.wear') }}',
                method: "POST",
                data: {
                    user_id: {{ auth()->user()->id }},
                    wear: inputwear.value,
                    mark: inputmark.value
                },
                success: data => {
                    let enginesDiv = document.querySelector('#engines')
                    enginesDiv.innerHTML = ''
                    if(data.count === 0) {
                        enginesDiv.innerHTML = `<tr><td colspan="6" class="text-center fs-3">Aucune Donnée</td></tr>`
                    } else {
                        data.engines.forEach(engine => {
                            console.log(engine)
                            enginesDiv.innerHTML += `
                        <tr>
                            <td>
                                <div class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input me-3" name="engines_id[]" type="checkbox" value="${engine.engine.id}" />
                                    ${engine.engine.engine.name} - ${engine.engine.number}
                                </div>
                            </td>
                            <td class="text-center">
                                ${engine.usure}
                            </td>
                            <td class="text-center">
                                ${engine.mark}
                            </td>
                            <td class="text-center">
                                ${engine.nb_rec_incident}
                            </td>
                            <td class="text-center">
                                ${engine.coast_incident}
                            </td>
                        </tr>
                        `
                        })
                    }
                }
            })
        }
    </script>
@endsection
