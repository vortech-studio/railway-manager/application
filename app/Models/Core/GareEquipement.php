<?php

namespace App\Models\Core;

use App\Models\User\UserTechnicentreTask;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class GareEquipement extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    protected $appends = ["type_equipement_icon"];

    public function gare()
    {
        return $this->belongsTo(Gares::class, 'gares_id');
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(UserTechnicentreTask::class);
    }

    public function getTypeEquipementIconAttribute()
    {
        return match ($this->type_equipement) {
            "Toilette" => "fa-restroom",
            "Information Sonore" => "fa-volume-up",
            "Information Visuelle" => "fa-eye",
            "Ascenceurs" => "fa-elevator",
            "Escalators" => "fa-stairs",
            "Guichets" => "fa-ticket",
            "Boutiques" => "fa-shop",
            "Restaurants" => "fa-utensils",
            "Accès PMR" => "fa-accessible-icon",
        };
    }

    public static function dispatching(Gares $gares)
    {
        match ($gares->type_gare) {
            "a" => self::insertGrandeGare($gares),
            "b" => self::insertMoyenneGare($gares),
            "c" => self::insertPetiteGare($gares),
            "d" => self::insertHalteGare($gares),
        };
    }

    protected static function insertGrandeGare(Gares $gares)
    {
        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Toilette",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Information Sonore",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Information Visuelle",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Ascenceurs",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Escalators",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Guichets",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Boutiques",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Restaurants",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Accès PMR",
            "gares_id" => $gares->id,
        ]);
    }

    protected static function insertMoyenneGare(Gares $gares)
    {
        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Toilette",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Information Sonore",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Information Visuelle",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Ascenceurs",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Escalators",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Guichets",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Boutiques",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Accès PMR",
            "gares_id" => $gares->id,
        ]);
    }

    protected static function insertPetiteGare(Gares $gares)
    {
        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Toilette",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Information Sonore",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Information Visuelle",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Ascenceurs",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Guichets",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Accès PMR",
            "gares_id" => $gares->id,
        ]);
    }

    protected static function insertHalteGare(Gares $gares)
    {
        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Information Visuelle",
            "gares_id" => $gares->id,
        ]);

        self::create([
            "code_uic" => $gares->code_uic,
            "type_equipement" => "Accès PMR",
            "gares_id" => $gares->id,
        ]);
    }

    public function getAmountMaintenance()
    {
        return match ($this->type_equipement) {
            "Toilette" => 0.4 * 15,
            "Information Sonore", "Information Visuelle" => 0.7 * 30,
            "Ascenceurs", "Escalators" => 1.2 * 120,
            "Guichets", "Accès PMR" => 1.6 * 45,
            "Boutiques", "Restaurants" => 2 * 75
        };
    }
}
