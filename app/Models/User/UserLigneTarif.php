<?php

namespace App\Models\User;

use App\Models\Core\Engine;
use App\Models\Core\Hub;
use Illuminate\Database\Eloquent\Model;

class UserLigneTarif extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    protected $casts = [
        'date_tarif' => 'datetime',
    ];

    public function ligne()
    {
        return $this->belongsTo(UserLigne::class, 'user_ligne_id');
    }

    public static function calcDemande(Hub $hub)
    {
        $calc = ($hub->gare->frequentation / 365) / $hub->gare->time_day_work;
        return intval($calc);
    }

    public static function calcOffre(Engine $engine)
    {
        return $engine->nb_passager;
    }

    public static function createTarif(UserLigne $userLigne, Engine $engine, Hub $hub)
    {
        match ($engine->type_train) {
            "ter" => self::generateTarifTer($userLigne, $engine, $hub),
            "tgv" => self::generateTarifTgv($userLigne, $engine, $hub),
            "ic" => self::generateTarifIc($userLigne, $engine, $hub),
            "other" => self::generateTarifOther($userLigne, $engine, $hub),
        };
    }

    public static function calcTarifFirst(UserLigne $ligne)
    {
        $calc = (config('global.price_kilometer') * $ligne->ligne->distance)*($ligne->ligne->distance * (config('global.price_electricity')/3))/100;
        return round(floatval($calc * 60 / 100), 2);
    }

    public static function calcTarifSecond(UserLigne $ligne)
    {
        $calc = (config('global.price_kilometer') * $ligne->ligne->distance)*($ligne->ligne->distance * (config('global.price_electricity')/3))/100;
        return round(floatval($calc * 40 / 100), 2);
    }

    protected static function generateTarifTer(UserLigne $userLigne, Engine $engine, Hub $hub)
    {
        $userLigne->tarifs()->create([
            "date_tarif" => now()->startOfDay(),
            "type" => "unique",
            "demande" => self::calcDemande($hub),
            "offre" => self::calcOffre($engine),
            "price" => self::calcTarifSecond($userLigne),
            "user_ligne_id" => $userLigne->id,
        ]);
    }

    protected static function generateTarifTgv(UserLigne $userLigne, Engine $engine, Hub $hub)
    {
        $userLigne->tarifs()->create([
            "date_tarif" => now()->startOfDay(),
            "type" => "first",
            "demande" => self::calcDemande($hub),
            "offre" => self::calcOffre($engine),
            "price" => self::calcTarifFirst($userLigne),
            "user_ligne_id" => $userLigne->id,
        ]);
        $userLigne->tarifs()->create([
            "date_tarif" => now()->startOfDay(),
            "type" => "second",
            "demande" => self::calcDemande($hub),
            "offre" => self::calcOffre($engine),
            "price" => self::calcTarifSecond($userLigne),
            "user_ligne_id" => $userLigne->id,
        ]);
    }

    protected static function generateTarifIc(UserLigne $userLigne, Engine $engine, Hub $hub)
    {
        $userLigne->tarifs()->create([
            "date_tarif" => now()->startOfDay(),
            "type" => "first",
            "demande" => self::calcDemande($hub),
            "offre" => self::calcOffre($engine),
            "price" => self::calcTarifFirst($userLigne),
            "user_ligne_id" => $userLigne->id,
        ]);
        $userLigne->tarifs()->create([
            "date_tarif" => now()->startOfDay(),
            "type" => "first",
            "demande" => self::calcDemande($hub),
            "offre" => self::calcOffre($engine),
            "price" => self::calcTarifSecond($userLigne),
            "user_ligne_id" => $userLigne->id,
        ]);
    }

    protected static function generateTarifOther(UserLigne $userLigne, Engine $engine, Hub $hub)
    {
        $userLigne->tarifs()->create([
            "date_tarif" => now()->startOfDay(),
            "type" => "unique",
            "demande" => self::calcDemande($hub),
            "offre" => self::calcOffre($engine),
            "price" => self::calcTarifSecond($userLigne),
            "user_ligne_id" => $userLigne->id,
        ]);
    }
}
