@extends("admin.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Gestion des Articles",
        'action' => true,
        'data' => [
            [
                'url' => route('admin.blog.create'),
                'title' => "Ajouter un nouvelle article",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="card card-flush">
        <!--begin::Card header-->
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                    <input type="text" data-blog-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Rechercher un article" />
                </div>
                <!--end::Search-->
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body pt-0">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_article">
                <thead>
                <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                    <th class="w-10px pe-2">
                        <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                            <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_ecommerce_products_table .form-check-input" value="1" />
                        </div>
                    </th>
                    <th class="min-w-200px">Titre</th>
                    <th class="text-center min-w-100px">Etat</th>
                    <th class="text-end min-w-70px">Actions</th>
                </tr>
                </thead>
                <tbody class="fw-semibold text-gray-600">
                @foreach($articles as $article)
                <tr>
                    <td>
                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                            <input class="form-check-input" type="checkbox" value="1" />
                        </div>
                    </td>
                    <td>
                        <div class="d-flex flex-column">
                            <div class="fs-2 fw-bold">{{ $article->titre }}</div>
                            <div class="fs-italic">{{ $article->type_article }}</div>
                        </div>
                    </td>
                    <td class="text-center pe-0">
                        {!! $article->published !!}
                    </td>
                    <td class="text-end">
                        <a href="#" class="btn btn-sm btn-light btn-flex btn-center btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <i class="ki-duotone ki-down fs-5 ms-1"></i></a>
                        <!--begin::Menu-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                            @if($article->published == 1)
                                <div class="menu-item px-3">
                                    <span class="menu-link text-danger px-3 dispublish" data-id="{{ $article->id }}">Dépublier</span>
                                </div>
                            @else
                                <div class="menu-item px-3">
                                    <span class="menu-link text-success px-3 publish" data-id="{{ $article->id }}">Publier</span>
                                </div>
                            @endif
                            <div class="menu-item px-3">
                                <a href="{{ route('admin.blog.show', $article->id) }}" class="menu-link px-3">Voir la fiche</a>
                            </div>
                            <div class="menu-item px-3">
                                <a href="{{ route('admin.blog.edit', $article->id) }}" class="menu-link px-3">Editer la fiche</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3" data-action="delete" data-id="{{ $article->id }}">Supprimer</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu-->
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let table = $('#liste_article').DataTable({
            info: !1,
            order: [],
            pageLength: 10,
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
        });

        document.querySelector('[data-blog-filter="search"]').addEventListener('keyup', function (e) {
            table.search(e.target.value).draw();
        });

        document.querySelectorAll('.dispublish').forEach(element => {
            element.addEventListener('click', e => {
                e.preventDefault();
                let id = element.getAttribute('data-id');
                axios.put('/api/core/blog/' + id + '/dispublish')
                    .then(function (response) {
                        Swal.fire({
                            title: "Succès !",
                            text: "L'article a été dépublié avec succès !",
                            icon: "success",
                            confirmButtonText: "Fermer",
                        }).then(function (result) {
                            if (result.value) {
                                window.location.reload();
                            }
                        });
                    })
                    .catch(function (error) {
                        Swal.fire({
                            title: "Erreur !",
                            text: "L'article n'a pas été dépublié !",
                            icon: "error",
                            confirmButtonText: "Fermer",
                        });
                    });
            })
        })

        document.querySelectorAll('.publish').forEach(element => {
            element.addEventListener('click', e => {
                e.preventDefault();
                let id = element.getAttribute('data-id');
                axios.put('/api/core/blog/' + id + '/publish')
                    .then(function (response) {
                        Swal.fire({
                            title: "Succès !",
                            text: "L'article a été publié avec succès !",
                            icon: "success",
                            confirmButtonText: "Fermer",
                        }).then(function (result) {
                            if (result.value) {
                                window.location.reload();
                            }
                        });
                    })
                    .catch(function (error) {
                        Swal.fire({
                            title: "Erreur !",
                            text: "L'article n'a pas été publié !",
                            icon: "error",
                            confirmButtonText: "Fermer",
                        });
                    });
            })
        })

        document.querySelectorAll('[data-action="delete"]').forEach(function (element) {
            element.addEventListener('click', function (e) {
                e.preventDefault();
                let id = element.getAttribute('data-id');
                Swal.fire({
                    title: "Êtes-vous sûr ?",
                    text: "Vous ne pourrez pas revenir en arrière !",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Oui, supprimer !",
                    cancelButtonText: "Non, annuler !",
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        axios.delete('/admin/blogs/' + id)
                            .then(function (response) {
                                Swal.fire({
                                    title: "Suppression !",
                                    text: "L'article a été supprimé avec succès !",
                                    icon: "success",
                                    confirmButtonText: "Fermer",
                                }).then(function (result) {
                                    if (result.value) {
                                        element.closest('tr').remove();
                                    }
                                });
                            })
                            .catch(function (error) {
                                Swal.fire({
                                    title: "Erreur !",
                                    text: "L'article n'a pas été supprimé !",
                                    icon: "error",
                                    confirmButtonText: "Fermer",
                                });
                            });
                    } else if (result.dismiss === "cancel") {
                        Swal.fire({
                            title: "Annulation",
                            text: "L'article n'a pas été supprimé !",
                            icon: "info",
                            confirmButtonText: "Fermer",
                        });
                    }
                });
            });
        });


    </script>
@endsection
