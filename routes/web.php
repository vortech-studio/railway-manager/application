<?php

use App\Http\Controllers\ProfileController;
use App\Models\Core\Achievement;
use App\Models\User\UserTechnicentreTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/test', function () {
    $open = new \App\Services\OpenAi();
    $request = $open->dalle(1, "Background abstrait avec des rails ferroviaire. Couleur Foncée", "1024x1024");
    dd($request->data[0]->url);

});

Route::get('/offline', function () {
    return view('offline');
});

Route::post('/push', function (Request $request) {

    $request->validate([
        'endpoint' => 'required',
        'key.auth' => 'required',
        'keys.p256dh' => 'required',
    ]);

    $endpoint = $request->get('endpoint');
    $token = $request->keys['auth'];
    $key = $request->keys['p256dh'];

    $user = auth()->user();
    $user->updatePushSubscription($endpoint, $key, $token);

    return response()->json(['success' => true],200);

})->middleware(['auth']);

include ("auth.php");
include ("admin.php");
include ("games.php");
