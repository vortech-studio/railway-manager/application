<?php
use Illuminate\Support\Facades\Route;

Route::prefix('vigimat')->middleware(['premium'])->group(function () {
    Route::get('/', [\App\Http\Controllers\Game\Vigimat\DashboardController::class, 'index'])->name('vigimat.dashboard');
    Route::get('quit', [\App\Http\Controllers\Game\Vigimat\DashboardController::class, 'quit'])->name('vigimat.quit');

    Route::prefix('engine')->group(function () {
        Route::get('/', [\App\Http\Controllers\Game\Vigimat\EngineController::class, 'index'])->name('vigimat.engine');
        Route::get('{engine_id}', [\App\Http\Controllers\Game\Vigimat\EngineController::class, 'show'])->name('vigimat.engine.show');
    });
});
