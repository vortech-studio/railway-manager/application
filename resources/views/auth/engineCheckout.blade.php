<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic
Product Version: 8.1.8
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="{{ config('app.locale') }}">
<!--begin::Head-->
<head><base href="../../../"/>
    {{ seo()->render() }}
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    @vite(["resources/css/app.css"])
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="app-blank bgi-size-cover bgi-attachment-fixed bgi-position-center">
<!--begin::Theme mode setup on page load-->
<script>var defaultThemeMode = "light"; var themeMode; if ( document.documentElement ) { if ( document.documentElement.hasAttribute("data-bs-theme-mode")) { themeMode = document.documentElement.getAttribute("data-bs-theme-mode"); } else { if ( localStorage.getItem("data-bs-theme") !== null ) { themeMode = localStorage.getItem("data-bs-theme"); } else { themeMode = defaultThemeMode; } } if (themeMode === "system") { themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light"; } document.documentElement.setAttribute("data-bs-theme", themeMode); }</script>
<!--end::Theme mode setup on page load-->
<!--begin::Root-->
<div class="d-flex flex-column flex-root" id="kt_app_root">
    <!--begin::Page bg image-->
    <style>body { background-image: url('/storage/other/wall-rm.png'); } [data-bs-theme="dark"] body { background-image: url('/storage/other/wall-rm.png'); }</style>
    <!--end::Page bg image-->
    <!--begin::Authentication - Sign-in -->
    <div class="d-flex flex-column flex-lg-row flex-column-fluid">
        <!--begin::Aside-->
        <div class="d-flex flex-lg-row-fluid w-50">
            <!--begin::Content-->
            <div class="d-flex flex-column flex-center pb-0 pb-lg-10 p-10 w-100 bg-white bg-opacity-75">
                <div class="d-flex flex-row p-5 mb-10 shadow-lg">
                    <div class="symbol symbol-55px symbol-circle me-5">
                        <img src="/assets/media/avatars/300-1.jpg" alt="">
                    </div>
                    <div class="d-flex flex-column">
                        <p>Vous avez acheter votre HUB ainsi que votre première ligne, maintenant afin de demarrer votre activité, il vous faut un engin roulant.</p>
                        <p>
                            Il existe différents type de matériels roulants que vous pourrez acquérir ultérieurement.<br>
                            Pour en savoir plus sur les différents type de matériels roulants, veuillez consultez le <a href="">WIKI sur les matériels roulants</a>.
                        </p>
                        <p>Pour commencer, veuillez selectionner une <strong>automotrice</strong> capable de circuler sur votre ligne.</p>
                    </div>
                </div>
            </div>
            <!--end::Content-->
        </div>
        <!--begin::Aside-->
        <!--begin::Body-->
        <div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12 bg-white bg-opacity-50 w-75">
            <!--begin::Wrapper-->
            <div class="bg-body d-flex flex-column flex-center rounded-4 w-md-1000px p-10">
                <!--begin::Content-->
                <div class="d-flex flex-center flex-column align-items-stretch h-lg-100 w-md-800px">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-center flex-column flex-column-fluid pb-15 pb-lg-20">
                        <!--begin::Form-->
                        <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="{{ route('oauth.configStore', ["step" => "4"]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-10">
                                @include("admin.includes.alerts")
                                <label for="engine_id" class="form-label required">Matériels Roulants</label>
                                <select id="engine_id" class="form-control selectpicker" name="engine_id" data-live-search="true">
                                    <option value="">-- Choisir un matériel roulant --</option>
                                    @foreach($engines as $engine)
                                        <option value="{{ $engine->id }}">{{ $engine->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row g-3 mb-9" data-show="info">
                                <div class="col-5">
                                    <div class="card card-flush shadow-lg bg-inverse p-5">
                                        <img src="/storage/engines/automotrice/Z-6400.gif" class="w-100" alt="" data-ajax-load="imgEngine">
                                        <div class="card-body">
                                            <div class="d-flex flex-column">
                                                <div class="d-flex flex-row justify-content-between align-items-center">
                                                    <span>Désignation</span>
                                                    <span class="text-end" data-ajax-load="nameEngine">Z 6400</span>
                                                </div>
                                                <div class="separator border-gray-500 my-5"></div>
                                                <div class="d-flex flex-row justify-content-between align-items-center">
                                                    <span>Type de Train</span>
                                                    <span class="text-end" data-ajax-load="typeTrain">Autre</span>
                                                </div>
                                                <div class="separator border-gray-500 my-5"></div>
                                                <div class="d-flex flex-row justify-content-between align-items-center">
                                                    <span>Type de Système</span>
                                                    <span class="text-end" data-ajax-load="typeEngine">Automotrice</span>
                                                </div>
                                                <div class="separator border-gray-500 my-5"></div>
                                                <div class="d-flex flex-row justify-content-between align-items-center">
                                                    <span>Type de Motorisation</span>
                                                    <span class="text-end p-1 border border-gray-500">
                                                        <img src="/storage/icons/type_energy/electrique.png" data-ajax-load="typeEnergy" class="w-20px" alt="" data-bs-toggle="tooltip" title="Electrique">
                                                    </span>
                                                </div>
                                                <div class="separator border-gray-500 my-5"></div>
                                                <div class="d-flex flex-row justify-content-between align-items-center">
                                                    <div class="d-flex flex-row align-items-center">
                                                        <div class="symbol symbol-20px me-3">
                                                            <img src="/storage/icons/train_engine.png" alt="">
                                                        </div>
                                                        <span>Vitesse Maximal</span>
                                                    </div>
                                                    <span class="text-end" data-ajax-load="velocity">120 Km/h</span>
                                                </div>
                                                <div class="separator border-gray-500 my-5"></div>
                                                <div class="d-flex flex-row justify-content-between align-items-center">
                                                    <div class="d-flex flex-row align-items-center">
                                                        <div class="symbol symbol-20px me-3">
                                                            <i class="fa-solid fa-users fs-6 text-black"></i>
                                                        </div>
                                                        <span>Nb Passagers Maximal</span>
                                                    </div>
                                                    <span class="text-end" data-ajax-load="nbPassager">330</span>
                                                </div>
                                                <div class="separator border-gray-500 my-5"></div>
                                                <div class="d-flex flex-row justify-content-between align-items-center">
                                                    <div class="d-flex flex-row align-items-center">
                                                        <div class="symbol symbol-20px me-3">
                                                            <img src="/storage/icons/technician.png" alt="">
                                                        </div>
                                                        <span>Durée de la maintenance</span>
                                                    </div>
                                                    <span class="text-end" data-ajax-load="durationMaintenance">03:55:00</span>
                                                </div>
                                                <div class="separator border-gray-500 my-5"></div>
                                                <div class="d-flex flex-row justify-content-center align-items-center">
                                                    <a href="" class="btn btn-outline btn-outline-info">En savoir plus</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-7">
                                    <div class="d-flex flex-column justify-content-center shadow-lg p-5 bg-inverse">
                                        <div class="d-flex flex-column p-5 rounded-2 border border-2 border-primary bg-light-primary mb-2">
                                            <span>Prix à l'achat</span>
                                            <span class="fs-2 fw-bolder" data-ajax-load="priceAchat">30 554,03 €</span>
                                        </div>
                                        <div class="d-flex flex-column p-5 rounded-2 border border-2 border-danger bg-light-danger mb-2">
                                            <span>Prix à la location</span>
                                            <span class="fs-2 fw-bolder" data-ajax-load="priceLoan">848,72 € / par trajet</span>
                                        </div>
                                        <div class="d-flex flex-column p-5 rounded-2 border border-2 border-warning bg-light-warning mb-2">
                                            <span>Prix de la maintenance</span>
                                            <span class="fs-2 fw-bolder" data-ajax-load="priceMaintenance">1 147,95 € / par jour de maintenance</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-end">
                                <x-form.button />
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Wrapper-->

                </div>
                <!--end::Content-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Authentication - Sign-in-->
</div>
<!--end::Root-->
<!--begin::Javascript-->
<script>var hostUrl = "assets/";</script>
<!--begin::Global Javascript Bundle(mandatory for all pages)-->
<script src="assets/plugins/global/plugins.bundle.js"></script>
<script src="assets/js/scripts.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta3/dist/js/bootstrap-select.min.js"></script>
<script src="https://unpkg.com/react@17.0.2/umd/react.production.min.js"></script>
<script src="https://unpkg.com/react-dom@17.0.2/umd/react-dom.production.min.js"></script>
@vite(["resources/js/app.js"])
<!--end::Global Javascript Bundle-->
<script type="text/javascript">
    $(document).on('change', '#engine_id', function() {
        let engine_id = $(this).val();
        $.ajax({
            url: "/api/core/engine/"+engine_id,
            type: "GET",
            success: function(data) {
                console.log(data);
                $('[data-ajax-load="imgEngine"]').attr('src', '/storage/engines/' + data.engine.type_engine + '/' + data.engine.slug+'-0.gif');
                $('[data-ajax-load="nameEngine"]').html(data.engine.name);
                $('[data-ajax-load="typeTrain"]').html(data.engine.type_train);
                $('[data-ajax-load="typeEngine"]').html(data.engine.type_engine);
                $('[data-ajax-load="typeEnergy"]').attr('src', '/storage/icons/type_energy/' + data.engine.type_energy + '.png');
                $('[data-ajax-load="velocity"]').html(data.engine.velocity + ' Km/h');
                $('[data-ajax-load="nbPassager"]').html(data.engine.nb_passager);
                $('[data-ajax-load="durationMaintenance"]').html(data.engine.duration_maintenance);
                $('[data-ajax-load="priceAchat"]').html(data.engine.format_price_checkout);
                $('[data-ajax-load="priceLoan"]').html(data.engine.format_price_loan + ' / par trajet');
                $('[data-ajax-load="priceMaintenance"]').html(data.engine.format_price_maintenance + ' / par jour de maintenance');
            }
        });
    });
</script>
<!--end::Javascript-->
</body>
<!--end::Body-->
</html>
