<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCompositionTracksTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_composition_tracks', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_composition_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_engine_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });

        Schema::table('user_lignes', function (Blueprint $table) {
            $table->foreignId('user_composition_track_id')
                ->nullable()
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_composition_tracks');
    }
}
