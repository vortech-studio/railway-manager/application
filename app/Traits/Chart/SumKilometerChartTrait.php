<?php

namespace App\Traits\Chart;

use App\Models\User\UserTechnicentre;

trait SumKilometerChartTrait
{
    public static function get($engine_id)
    {
        $technicentre = UserTechnicentre::find(\Session::get('technicentre')[0]['id']);
        $engine = $technicentre->engines()->find($engine_id)->engine;
        $dataset = [];

        for ($i = 12; $i >= 6; $i--) {
            $dataset[] = [
                "mois" => now()->locale('fr_FR')->subMonths($i)->format('F'),
                "kilometers" => $engine->plannings()
                    ->where('status', 'arrival')
                    ->whereBetween('date_arrived', [now()->subMonths($i)->startOfMonth(), now()->subMonths($i)->endOfMonth()])
                    ->sum('kilometer'),
            ];
        }

        return response()->json($dataset);
    }
}
