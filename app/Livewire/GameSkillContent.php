<?php

namespace App\Livewire;

use App\Models\User\UserHubSkill;
use Livewire\Component;

class GameSkillContent extends Component
{
    public $skills;
    public function render()
    {
        return view('livewire.game-skill-content');
    }
}
