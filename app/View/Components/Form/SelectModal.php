<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class SelectModal extends Component
{
    public $name;

    public mixed $datas;

    public $label;

    /**
     * @var mixed|null
     */
    public mixed $placeholder;

    public bool $required;

    public string|int|null $value;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $datas, $label, $placeholder = null, bool $required = false, string|int $value = null)
    {
        //
        $this->name = $name;
        $this->datas = $datas;
        $this->label = $label;
        $this->placeholder = $placeholder;
        $this->required = $required;
        $this->value = $value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.select-modal');
    }
}
