<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hubs', function (Blueprint $table) {
            $table->id();
            $table->decimal('price', 16, 2);
            $table->decimal('taxe_hub', 16, 2);
            $table->integer('passenger_first')->default(0);
            $table->integer('passenger_second')->default(0);
            $table->integer('nb_slot_commerce')->default(0);
            $table->integer('nb_slot_pub')->default(0);
            $table->integer('nb_slot_parking')->default(0);
            $table->boolean('active')->default(false);
            $table->timestamps();
            $table->foreignId('gares_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hubs');
    }
};
