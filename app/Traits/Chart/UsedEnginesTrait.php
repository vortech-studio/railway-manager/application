<?php

namespace App\Traits\Chart;

use App\Models\User\UserIncident;
use App\Models\User\UserTechnicentre;

trait UsedEnginesTrait
{
    public static function get()
    {
        $technicentre = UserTechnicentre::find(\Session::get('technicentre')[0]['id']);

        $labels = [];

        for($i=0; $i < 15; $i++) {
            $date = \Carbon\Carbon::now()->subDays($i)->format('d/m');
            $labels[] = $date;
        }

        $datas = [];
        for ($i=0; $i < 15; $i++) {
            $datas[] = $technicentre->hub->plannings()
                ->where('date_depart', now()->subDays($i)->startOfDay())
                ->where('status', 'arrived')
                ->count();
        }

        $datas = collect($datas);

        return response()->json([
            "labels" => $labels,
            "content" => $datas->toArray()
        ]);

    }
}
