<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCardContentsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_card_contents', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_card_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('card_holder_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_card_contents');
    }
}
