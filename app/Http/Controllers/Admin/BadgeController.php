<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Core\Achievement;
use App\Models\Core\Reward;
use LevelUp\Experience\Models\Level;

class BadgeController extends Controller
{
    public function index()
    {
        return view('admin.badges.index', [
            "achievements" => Achievement::all(),
            "levels" => Level::all(),
            "rewards" => Reward::all()
        ]);
    }
}
