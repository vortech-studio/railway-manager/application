<?php

namespace App\Models\User;

use App\Models\Core\LigneStation;
use Illuminate\Database\Eloquent\Model;

class UserPlanningStation extends Model
{
    public $timestamps = false;

    protected $casts = [
        'departure_at' => 'datetime',
        'arrival_at' => 'datetime',
    ];
    protected $appends = ['status_badge_format'];

    protected $guarded = [];

    public function userPlanning()
    {
        return $this->belongsTo(UserPlanning::class);
    }

    public function ligneStation()
    {
        return $this->belongsTo(LigneStation::class, 'ligne_station_id');
    }

    public function getStatusBadgeFormatAttribute()
    {
        return match ($this->status) {
            "init" => '<span class="badge badge-secondary"><i class="fa-solid fa-clock me-2"></i> A L\'heure</span>',
            "arrival" => '<span class="badge badge-danger"><i class="fa-solid fa-train text-white me-2"></i> Train à l\'approche</span>',
            "departure" => '<span class="badge badge-success"><i class="fa-solid fa-train text-white me-2"></i> Train au départ</span>',
            "done" => '<span class="badge badge-success"><i class="fa-solid fa-check-circle text-white me-2"></i> Arret effectuer</span>',
        };
    }
}
