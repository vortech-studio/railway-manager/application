<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCompaniesTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_companies', function (Blueprint $table) {
            $table->id();
            $table->integer('distraction')->default(0)->comment("Niveau de distraction de l'entreprise");
            $table->integer('tarification')->default(0)->comment("Niveau de tarification de l'entreprise");
            $table->integer('ponctualite')->default(0)->comment("Niveau de ponctualité de l'entreprise");
            $table->integer('secutite')->default(0)->comment("Niveau de sécurité de l'entreprise");
            $table->integer('confort')->default(0)->comment("Niveau de confort de l'entreprise");
            $table->integer('rent_aux')->default(0)->comment("Niveau de rentabilité auxiliaire de l'entreprise");
            $table->integer('frais')->default(0)->comment("Niveau de frais de l'entreprise");
            $table->integer('livraison')->default(1)->comment("Niveau de vitesse de livraison des infrastructures de l'entreprise");
            $table->decimal('valorisation')->default(0)->comment("Valorisation de l'entreprise");
            $table->decimal('benefice_hebdo_travel')->default(0)->comment("Bénéfice hebdomadaire de l'entreprise");
            $table->decimal('mass_salarial')->default(0)->comment("Masse salariale de l'entreprise");
            $table->decimal('treso_structurel')->default(0)->comment("Trésorerie structurelle de l'entreprise");
            $table->decimal('last_impot')->default(0)->comment("Dernier impôt payé de l'entreprise");
            $table->decimal('location_mensual_fee')->default(0)->comment("Frais de location mensuel de l'entreprise");
            $table->decimal('remb_mensual')->default(0)->comment("Remboursement de pret mensuel de l'entreprise");
            $table->integer('subvention')->default(0)->comment("Subvention de l'entreprise");

            $table->foreignId('user_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_companies');
    }
}
