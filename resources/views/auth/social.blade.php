<!--begin::Col-->
<div class="col-md-12">
    <!--begin::Google link=-->
    <a href="{{ route('oauth.redirect', 'google') }}" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
        <img alt="Logo" src="/assets/media/svg/brand-logos/google-icon.svg" class="h-15px me-3" />Connexion avec google</a>
    <!--end::Google link=-->
</div>
<!--end::Col-->
<!--begin::Col-->
<div class="col-md-12">
    <!--begin::Google link=-->
    <a href="{{ route('oauth.redirect', 'facebook') }}" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
        <img src="/assets/media/svg/brand-logos/facebook-3.svg" class="h-15px me-3" alt="facebook" /> Connexion avec facebook
    </a>
</div>
<div class="col-md-12">
    <!--begin::Google link=-->
    <a href="{{ route('oauth.redirect', 'microsoft') }}" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100 disabled">
        <img src="/assets/media/svg/brand-logos/microsoft-5.svg" class="h-15px me-3" alt="microsoft" /> Connexion avec microsoft
    </a>
</div>
<div class="col-md-12">
    <!--begin::Google link=-->
    <a href="{{ route('oauth.redirect', 'discord') }}" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
        <img src="/assets/media/svg/brand-logos/discord.svg" class="h-15px me-3" alt="discord" /> Connexion avec discord
    </a>
</div>
<div class="col-md-12">
    <!--begin::Google link=-->
    <a href="{{ route('oauth.redirect', 'steam') }}" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100 disabled" data-bs-toggle="tooltip" title="Bientôt disponible">
        <img src="/assets/media/svg/brand-logos/steam.svg" class="h-15px me-3" alt="steam" /> Connexion avec steam
    </a>
</div>
<div class="col-md-12">
    <!--begin::Google link=-->
    <a href="{{ route('oauth.redirect', 'twitch') }}" class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100 disabled" data-bs-toggle="tooltip" title="Bientôt disponible">
        <img src="/assets/media/svg/brand-logos/twitch.svg" class="h-15px me-3" alt="twitch" /> Connexion avec twitch
    </a>
</div>
<!--end::Col-->
