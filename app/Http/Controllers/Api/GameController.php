<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\Bonus;
use App\Models\Core\CardHolder;
use App\Models\Core\CardHolderCategory;
use App\Models\Core\Mouvement;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Irfa\Gatcha\Roll;
use Irfa\Gatcha\Roulette\Roulette;

class GameController extends Controller
{
    public function gatcha(Request $request)
    {
        $user = User::find($request->get('user_id'));
        $card_communes = CardHolder::where('card_holder_category_id', 1);
        $card_remarquables = CardHolder::where('card_holder_category_id', 2);
        $card_prestiges = CardHolder::where('card_holder_category_id', 3);
        $card_legends = CardHolder::where('card_holder_category_id', 4);

        $card_eco = [];
        $card_finance = [];
        $card_platinum = [];
        $card_epique = [];

        $matching = match($request->get('action')) {
            "card-eco" => $this->gatchaCardEco($card_communes),
            "card-finance" => $this->gatchaCardFinance($card_communes, $card_remarquables),
        };

        return response()->json([
            "gatcha" => $matching
        ]);
    }

    private function gatchaCardEco(Builder $cardCommune)
    {
        $com = $cardCommune->get()->random(1);

        $gatch = Roll::put([
            $com->first()->name => 100,
        ])->spin();

        return $gatch;
    }

    private function gatchaCardFinance(Builder $card_communes, Builder $card_remarquables)
    {
        $com = $card_communes->get()->random(3);
        $rem = $card_remarquables->get()->random(1);


        foreach ($com as $key => $value) {
            $gatcha = Roll::put([
                $value->name => rand(10,100),
            ]);
        }

        foreach ($rem as $key => $value) {
            $gatcha = Roll::put([
                $value->name => rand(10,100),
            ]);
        }

        return $gatcha->spin();
    }

    public function claimBonus(Request $request)
    {
        $user = User::find($request->get('user_id'));
        $bonus = Bonus::find($request->get('bonus_id'));

        if($user->bonuses()->where('bonus_id', $bonus->id)->exists()) {
            return response()->json([
                "result" => false,
                "message" => "Vous avez déjà reçu ce bonus aujourd'hui"
            ]);
        } else {
            if($user->dailys()->whereDate('date', now()->toDateString())->where('claim_daily', true)->exists()) {
                return response()->json([
                    "result" => false,
                    "message" => "Vous avez déjà reçu ce bonus aujourd'hui"
                ]);
            } else {
                $user->bonuses()->attach($bonus->id);
                $this->claimer($user, $bonus);
                $user->dailys()->whereDate('date', now()->toDateString())->first()->update([
                    "claim_daily" => true
                ]);

                return response()->json([
                    'result' => true,
                    "message" => "Vous avez reçu le bonus",
                    "bonus" => $bonus
                ]);
            }
        }
    }

    public function verify(Request $request)
    {
        return match ($request->get('type')) {
            "notification" => $this->verifyNotification($request),
            "bonus" => $this->verifyBonus($request),
        };
    }

    public function accelerate(Request $request)
    {
        dd($request->all());
    }

    private function claimer(User $user, Bonus $bonus)
    {
        if($bonus->type == 'argent') {
            $user->argent += $bonus->qte;
            $user->save();
        } elseif ($bonus->type == 'tpoint') {
            $user->tpoint += $bonus->qte;
            $user->save();
        } elseif($bonus->type == 'research') {
            $user->research += $bonus->qte;
            $user->save();
        } elseif($bonus->type == 'simulation') {
            $user->card_bonus->sim_offer += $bonus->qte;
            $user->save();
        } elseif($bonus->type == 'audit_int') {
            $user->card_bonus->audit_int_offer += $bonus->qte;
            $user->save();
        } elseif($bonus->type == 'audit_ext') {
            $user->card_bonus->audit_ext_offer += $bonus->qte;
            $user->save();
        }
    }

    private function verifyNotification(Request $request)
    {
        $user = User::find($request->get('user_id'));
        $count_notif = $user->unreadNotifications()->count();

        if($count_notif > 0) {
            return response()->json([
                "result" => true,
                "count" => $count_notif
            ]);
        } else {
            return response()->json([
                "result" => false
            ]);
        }
    }

    private function verifyBonus(Request $request)
    {
        $user = User::find($request->get('user_id'));
        $count_bonus = $user->dailys()->whereDate('date', now()->toDateString())->where('claim_daily', 0)->count();
        if($count_bonus == 0) {
            return response()->json([
                "result" => true
            ]);
        } else {
            return response()->json([
                "result" => false
            ]);
        }

    }
}
