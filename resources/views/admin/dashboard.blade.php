@extends("admin.template")

@section("css")

@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Tableau de Bord",
        'action' => false,
        'data' => [
            [
                'url' => route('admin.dashboard'),
                'title' => "Tableau de Bord",
                'class' => "primary"
            ],
            [
                'url' => route('admin.dashboard'),
                'title' => "Tableau de Bord",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
        <div class="col-xl-3">
            <div class="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-xl-100" style="background-color: #F1416C;background-image:url('/metronic8/demo1/assets/media/svg/shapes/wave-bg-red.svg')">
                <!--begin::Header-->
                <div class="card-header pt-5 mb-3">
                    <!--begin::Icon-->
                    <div class="d-flex flex-center rounded-circle h-80px w-80px" style="border: 1px dashed rgba(255, 255, 255, 0.4);background-color: #F1416C">
                        <i class="ki-duotone ki-user text-white fs-2qx lh-0"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></i>
                    </div>
                    <!--end::Icon-->
                </div>
                <!--end::Header-->

                <!--begin::Card body-->
                <div class="card-body d-flex align-items-end mb-3">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center">
                        <span class="fs-4hx text-white fw-bold me-6">{{ \App\Models\User::all()->count() }}</span>

                        <div class="fw-bold fs-6 text-white">
                            <span class="d-block">Joueurs</span>
                            <span class="">Enregistrées</span>
                        </div>
                    </div>
                    <!--end::Info-->
                </div>
                <!--end::Card body-->

                <!--begin::Card footer-->
                <div class="card-footer" style="border-top: 1px solid rgba(255, 255, 255, 0.3);background: rgba(0, 0, 0, 0.15);">
                    <!--begin::Progress-->
                    <div class="fw-bold text-white py-2">
                        <span class="fs-1 d-block">{{ \App\Models\User::where('status', 'active')->get()->count() }}</span>
                        <span class="opacity-50">Connectés Actuellements</span>
                    </div>
                    <!--end::Progress-->
                </div>
                <!--end::Card footer-->
            </div>
        </div>
        <div class="col-xl-3">
            <div class="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-xl-100" style="background-color: #146aef;background-image:url('/assets/media/svg/shapes/wave-bg-red.svg')">
                <!--begin::Header-->
                <div class="card-header pt-5 mb-3">
                    <!--begin::Icon-->
                    <div class="d-flex flex-center rounded-circle h-80px w-80px" style="border: 1px dashed rgba(255, 255, 255, 0.4);background-color: #146aef">
                        <img src="/storage/icons/hub.png" class="w-60px" alt="">
                    </div>
                    <!--end::Icon-->
                </div>
                <!--end::Header-->

                <!--begin::Card body-->
                <div class="card-body d-flex align-items-end mb-3">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center">
                        <span class="fs-4hx text-white fw-bold me-6">{{ \App\Models\Core\Hub::all()->count() }}</span>

                        <div class="fw-bold fs-6 text-white">
                            <span class="d-block">Hubs</span>
                            <span class="">Enregistrées</span>
                        </div>
                    </div>
                    <!--end::Info-->
                </div>
                <!--end::Card body-->

                <!--begin::Card footer-->
                <div class="card-footer" style="border-top: 1px solid rgba(255, 255, 255, 0.3);background: rgba(0, 0, 0, 0.15);">
                    <!--begin::Progress-->
                    <div class="fw-bold text-white py-2">
                        <span class="fs-1 d-block">{{ \App\Models\Core\Hub::where('active', true)->count() }}</span>
                        <span class="opacity-50">Activés</span>
                    </div>
                    <!--end::Progress-->
                </div>
                <!--end::Card footer-->
            </div>
        </div>
        <div class="col-xl-3">
            <div class="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-xl-100" style="background-color: #3cef14;background-image:url('/assets/media/svg/shapes/wave-bg-red.svg')">
                <!--begin::Header-->
                <div class="card-header pt-5 mb-3">
                    <!--begin::Icon-->
                    <div class="d-flex flex-center rounded-circle h-80px w-80px" style="border: 1px dashed rgba(255, 255, 255, 0.4);background-color: #3cef14">
                        <img src="/storage/icons/tracks.png" class="w-60px" alt="">
                    </div>
                    <!--end::Icon-->
                </div>
                <!--end::Header-->

                <!--begin::Card body-->
                <div class="card-body d-flex align-items-end mb-3">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center">
                        <span class="fs-4hx text-white fw-bold me-6">{{ \App\Models\Core\Ligne::count() }}</span>

                        <div class="fw-bold fs-6 text-white">
                            <span class="d-block">Lignes</span>
                            <span class="">Enregistrées</span>
                        </div>
                    </div>
                    <!--end::Info-->
                </div>
                <!--end::Card body-->

                <!--begin::Card footer-->
                <div class="card-footer" style="border-top: 1px solid rgba(255, 255, 255, 0.3);background: rgba(0, 0, 0, 0.15);">
                    <!--begin::Progress-->
                    <div class="fw-bold text-white py-2">
                        <span class="fs-1 d-block">{{ \App\Models\Core\Ligne::where('active', true)->count() }}</span>
                        <span class="opacity-50">Activés</span>
                    </div>
                    <!--end::Progress-->
                </div>
                <!--end::Card footer-->
            </div>
        </div>
        <div class="col-xl-3">
            <div class="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-xl-100" style="background-color: #5d14ef;background-image:url('/assets/media/svg/shapes/wave-bg-red.svg')">
                <!--begin::Header-->
                <div class="card-header pt-5 mb-3">
                    <!--begin::Icon-->
                    <div class="d-flex flex-center rounded-circle h-80px w-80px" style="border: 1px dashed rgba(255, 255, 255, 0.4);background-color: #5d14ef">
                        <img src="/storage/icons/train.png" class="w-60px" alt="">
                    </div>
                    <!--end::Icon-->
                </div>
                <!--end::Header-->

                <!--begin::Card body-->
                <div class="card-body d-flex align-items-end mb-3">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center">
                        <span class="fs-4hx text-white fw-bold me-6">{{ \App\Models\Core\Engine::all()->count() }}</span>

                        <div class="fw-bold fs-6 text-white">
                            <span class="d-block">Matériels Roulants</span>
                            <span class="">Enregistrées</span>
                        </div>
                    </div>
                    <!--end::Info-->
                </div>
                <!--end::Card body-->
            </div>
        </div>
    </div>

    <div class="row gy-5 g-xl-10">
        <div class="col-xl-6">
            <div class="card card-flush h-lg-100">
                <div class="card-header py-7">
                    <div class="fs-4 fw-bold border-bottom border-3 border-dark">
                        <i class="fa-solid fa-newspaper fs-4 text-dark me-3"></i>
                        Dernières News
                    </div>
                </div>
                <div class="card-body pt-1">
                    <table class="table table-bordered table-striped gy-4 no-footer align-middle">
                        <tbody>
                        @foreach(\App\Models\Core\Blog\Articles::where('published', true)->orderBy('updated_at', 'desc')->limit(5)->get() as $article)
                            <tr>
                                <td class="min-w-175px">
                                    <div class="position-relative ps-6 pe-3 py-2">
                                        <div class="position-absolute start-0 top-0 w-4px h-100 rounded-2 bg-{{ random_color() }}"></div>
                                        <div class="d-flex flex-row align-items-center">
                                            <div class="symbol symbol-30px me-3">
                                                <i class="fa-solid {{ $article->type_article_icon }} fs-3 text-dark" data-bs-toggle="tooltip" title="{{ $article->type_article }}"></i>
                                            </div>
                                            <div class="d-flex flex-column align-items-center">
                                                <a href="{{ route('admin.blog.show', $article->id) }}" class="mb-1 text-dark text-hover-primary fw-bold">{{ $article->titre }}</a>
                                                <div class="fs-7 text-muted fw-bold">{{ $article->updated_at->format("d/m/Y à H:i") }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-end">
                                    <a href="{{ route('admin.blog.show', $article->id) }}" class="btn btn-icon btn-sm btn-light btn-active-primary w-25px h-25px">
                                        <i class="ki-duotone ki-black-right fs-2 text-muted"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card card-flush h-lg-100">
                <div class="card-header py-7">
                    <div class="fs-4 fw-bold border-bottom border-3 border-dark">
                        <i class="fa-solid fa-user-plus fs-4 text-dark me-3"></i>
                        Derniers Joueurs Inscrit
                    </div>
                </div>
                <div class="card-body pt-1">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped gy-4 no-footer align-middle">
                            <tbody>
                            @foreach(\App\Models\User::orderBy('created_at', 'desc')->limit(5)->get() as $user)
                                <tr>
                                    <td class="min-w-175px">
                                        <div class="d-flex flex-row align-items-center">
                                            <div class="symbol symbol-50px me-3">
                                                <img src="/storage/avatar/users/{{ $user->avatar }}" alt="">
                                            </div>
                                            <div class="d-flex flex-column align-items-start">
                                                <a href="" class="mb-1 text-dark text-hover-primary fw-bold">{{ $user->name }}</a>
                                                <div class="fs-7 text-muted fw-bold">{{ $user->created_at->format("d/m/Y à H:i") }}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-row align-items-center">
                                            <img src="/storage/icons/euro.png" class="w-15px me-2" alt="" />
                                            <span class="fw-bold fs-9">Argent:</span> &nbsp;
                                            <div class="text-success fs-9">{{ eur($user->argent) }}</div>
                                        </div>
                                        <div class="d-flex flex-row align-items-center">
                                            <img src="/storage/icons/tpoint.png" class="w-15px me-2" alt="" />
                                            <span class="fw-bold fs-9">T Point:</span> &nbsp;
                                            <div class="text-success fs-9">{{ intval($user->tpoint) }}</div>
                                        </div>
                                        <div class="d-flex flex-row align-items-center">
                                            <img src="/storage/icons/research.png" class="w-15px me-2" alt="" />
                                            <span class="fw-bold fs-9">Recherche:</span> &nbsp;
                                            <div class="text-success fs-9">{{ eur($user->research) }}</div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-row align-items-center mb-2">
                                            <img src="/storage/icons/hub.png" class="w-15px me-2" alt="" />
                                            <span class="fw-bold fs-9">Hubs:</span> &nbsp;
                                            <div class="badge badge-info ">{{ $user->hubs()->count() }}</div>
                                        </div>
                                        <div class="d-flex flex-row align-items-center">
                                            <img src="/storage/icons/train.png" class="w-15px me-2" alt="" />
                                            <span class="fw-bold fs-9">Matériels Roulants:</span> &nbsp;
                                            <div class="badge badge-info">{{ $user->engines()->count() }}</div>
                                        </div>
                                    </td>
                                    <td class="text-end">
                                        <a href="" class="btn btn-icon btn-sm btn-light btn-active-primary w-25px h-25px">
                                            <i class="ki-duotone ki-black-right fs-2 text-muted"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")

@endsection
