<div class="mb-10">
    <div class="image-input image-input-circle" data-kt-image-input="true" style="background-image: url(/assets/media/svg/avatars/blank.svg)">
        <!--begin::Image preview wrapper-->
        <div class="image-input-wrapper w-125px h-125px" style="background-image: url(/storage/avatar/default.png)"></div>
        <!--end::Image preview wrapper-->

        <!--begin::Edit button-->
        <label class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
               data-kt-image-input-action="change"
               data-bs-toggle="tooltip"
               data-bs-dismiss="click"
               title="Changer l'image">
            <i class="ki-duotone ki-pencil fs-6"><span class="path1"></span><span class="path2"></span></i>

            <!--begin::Inputs-->
            <input type="file" name="{{ $name }}" accept="{{ $accept ?? '.png, .jpg, .jpeg' }}" />
            <input type="hidden" name="{{ $name }}_remove" />
            <!--end::Inputs-->
        </label>
        <!--end::Edit button-->

        <!--begin::Cancel button-->
        <span class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
              data-kt-image-input-action="cancel"
              data-bs-toggle="tooltip"
              data-bs-dismiss="click"
              title="Annuler l'image">
        <i class="ki-outline ki-cross fs-3"></i>
    </span>
        <!--end::Cancel button-->

        <!--begin::Remove button-->
        <span class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
              data-kt-image-input-action="remove"
              data-bs-toggle="tooltip"
              data-bs-dismiss="click"
              title="Supprimer l'image">
        <i class="ki-outline ki-cross fs-3"></i>
    </span>
        <!--end::Remove button-->
    </div>
</div>
