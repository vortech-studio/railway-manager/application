@extends("game.template")

@section("css")

@endsection

@section("info_bar")

@endsection

@section("content")
    <div class="flex-lg-row-fluid ms-lg-7 ms-xl-10">
        <!--begin::Card-->
        <div class="card">
            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                <!--begin::Actions-->
                <div class="d-flex flex-wrap gap-2">

                    <!--begin::Reload-->
                    <a href="#" class="btn btn-sm btn-icon btn-light btn-active-light-primary" data-bs-toggle="tooltip" data-bs-dismiss="click" data-bs-placement="top" title="Reload">
                        <i class="ki-duotone ki-arrows-circle fs-2">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </a>
                    <!--end::Reload-->
                </div>
                <!--end::Actions-->
                <!--begin::Actions-->
                <div class="d-flex align-items-center flex-wrap gap-2">
                    <!--begin::Search-->
                    <div class="d-flex align-items-center position-relative">
                        <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                        <input type="text" data-mailbox-filter="search" class="form-control form-control-sm form-control-solid mw-100 min-w-125px min-w-lg-150px min-w-xxl-200px ps-11" placeholder="Rechercher un message" />
                    </div>
                    <!--end::Search-->
                </div>
                <!--end::Actions-->
            </div>
            <div class="card-body p-0">
                <!--begin::Table-->
                <table class="table table-hover table-row-dashed fs-6 gy-5 my-0" id="kt_inbox_listing">
                    <thead class="d-none">
                    <tr>
                        <th>Actions</th>
                        <th>Author</th>
                        <th>Title</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $message)
                        <tr>
                            <td class="min-w-80px">
                                <!--begin::Star-->
                                <a href="#" class="btn btn-icon btn-color-gray-400 btn-active-color-primary w-35px h-35px" data-bs-toggle="tooltip" data-bs-placement="right" title="{{ $message->important ? 'Important' : '' }}">
                                    <i class="ki-duotone ki-star {{ $message->important ? 'text-warning' : '' }} fs-3"></i>
                                </a>
                                <!--end::Star-->
                            </td>
                            <td class="w-150px w-md-175px">
                                <a href="{{ route('mailbox.show', $message->id) }}" class="d-flex align-items-center text-dark">
                                    <!--begin::Avatar-->
                                    <div class="symbol symbol-35px me-3">
                                        <div class="symbol-label bg-light-{{ random_color() }}">
                                            <span class="text-{{ random_color() }}">{{ Str::limit($message->from, 1, '') }}</span>
                                        </div>
                                    </div>
                                    <!--end::Avatar-->
                                    <!--begin::Name-->
                                    <span class="{{ $message->read_at ?? 'fw-semibold' }}">{{ $message->from }}</span>
                                    <!--end::Name-->
                                </a>
                            </td>
                            <td>
                                <div class="text-dark gap-1 pt-2">
                                    <!--begin::Heading-->
                                    <a href="{{ route('mailbox.show', $message->id) }}" class="text-dark">
                                        <span class="{{ $message->read_at ?? 'fw-semibold' }}">{{ $message->subject }}</span>
                                    </a>
                                    <!--end::Heading-->
                                </div>
                                <!--begin::Badges-->
                                <!--<div class="badge badge-light-primary">inbox</div>
                                <div class="badge badge-light-warning">task</div>-->
                                <!--end::Badges-->
                            </td>
                            <td class="w-100px text-end fs-7 pe-9">
                                <span class="{{ $message->read_at ?? 'fw-semibold' }}">
                                    @if($message->created_at->isToday())
                                        {{ $message->created_at->format('H:i') }}
                                    @else
                                        {{ $message->created_at->format('d/m/Y') }}
                                    @endif
                                </span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <!--end::Table-->
            </div>
        </div>
        <!--end::Card-->
    </div>
@endsection

@section("script")
    <script type="text/javascript">

    </script>
@endsection
