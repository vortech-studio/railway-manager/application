<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddingFieldToUserEngines extends Migration
{
    public function up(): void
    {
        Schema::table('user_engines', function (Blueprint $table) {
            $table->integer('use_percent')->default(0);
            $table->integer('ancien')->default(0);
            $table->string('number')->nullable();
            $table->enum('status', ["free", "in_maintenance", "travel", "default", "nofree"])->default('free');
        });
    }

    public function down(): void
    {
        Schema::table('user_engines', function (Blueprint $table) {
            $table->dropColumn('use_percent');
            $table->dropColumn('ancien');
            $table->dropColumn('number');
            $table->dropColumn('status');
        });
    }
}
