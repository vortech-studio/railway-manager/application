<?php

namespace App\Models\User;

use App\Enums\SkillActionTypeEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserHubSkill extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $casts = [
        "action_type" => SkillActionTypeEnum::class
    ];
    protected $appends = ['actual_coast', 'actual_time'];

    public function user_hub()
    {
        return $this->belongsTo(UserHub::class);
    }

    public function calcActionValueFromLevel($level_niv)
    {
        return round($this->action_base_value * ($this->level_multiplicator * $level_niv), 0);
    }

    public function calcCoastActualLevel()
    {
        if($this->level_actual == 0) {
            return $this->coast_base;
        } else {
            return $this->coast_base * ($this->level_actual * $this->level_multiplicator);
        }
    }

    public function getActualCoastAttribute()
    {
        return eur($this->calcCoastActualLevel());
    }

    public function calcTimeActualLevel($format = true)
    {
        if($format) {
            $time = Carbon::parse(now()->startOfDay()->addMinutes($this->time_base));
            if($this->level_actual == 0) {
                return $time;
            } else {
                return $time->addMinutes($this->time_base * ($this->level_actual * $this->level_multiplicator));
            }
        } else {
            if($this->level_actual == 0) {
                return $this->time_base;
            } else {
                return $this->time_base * ($this->level_actual * $this->level_multiplicator);
            }
        }
    }

    public function getActualTimeAttribute()
    {
        return $this->calcTimeActualLevel()->format('H:i:s');
    }
}
