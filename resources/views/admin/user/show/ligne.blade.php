<div class="d-flex flex-row justify-content-between mb-10">
    <div class="d-flex align-items-center position-relative my-1">
        <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
            <span class="path1"></span>
            <span class="path2"></span>
        </i>
        <input type="text" data-ligne-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Rechercher une ligne" />
    </div>
    <button class="btn btn-sm btn-outline btn-outline-info" data-bs-toggle="modal" data-bs-target="#addLigne"><i class="fa-solid fa-plus me-3"></i> Ajouter une ligne au joueur</button>
</div>

<table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_ligne">
    <thead>
        <tr>
            <th class="min-w-150px">Hub</th>
            <th>Ligne</th>
            <th>Date Achat</th>
            <th>Nombre de départ par jour</th>
            <th class="min-w-150px">Composition</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<div class="modal fade" tabindex="-1" id="addLigne">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Ajout d'une ligne au joueur</h3>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                </div>
                <!--end::Close-->
            </div>
            <form id="formAddLigne" action="" method="post">
                @csrf
                <div class="modal-body">
                    <div class="mb-10">
                        <label for="ligne_id" class="control-label required">Ligne</label>
                        <select name="ligne_id" id="ligne_id" class="form-control selectpicker" data-live-search="true" required title="---- Selectionner une ligne ----">
                            @foreach($user->hubs()->get() as $hub)
                                <optgroup label="{{ $hub->hub->gare->name }}">
                                    @foreach(\App\Models\Core\Hub::find($hub->hub_id)->lignes as $ligne)
                                        <option value="{{ $ligne->id }}">{{ $ligne->name }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
