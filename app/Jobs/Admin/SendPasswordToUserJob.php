<?php

namespace App\Jobs\Admin;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendPasswordToUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public readonly User $user, public readonly string $password)
    {
    }

    public function handle(): void
    {
        $this->user->notify(new \App\Notifications\Admin\SendPasswordToUserNotification($this->user, $this->password));
    }
}
