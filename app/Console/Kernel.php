<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('horizon:snapshot')->everyFiveMinutes();
        // $schedule->command('inspire')->hourly();
        $schedule->command('system sitemap')->daily();
        $schedule->command('system planning_today')->daily();
        $schedule->command('system weather')->everySixHours();
        $schedule->command('system tarif')->daily();
        $schedule->command("system purge_notification")->daily();
        $schedule->command('system update_search')->daily();

        $schedule->command('travel prepare')->everyMinute();
        $schedule->command('travel departure')->everyMinute();
        $schedule->command('travel transit')->everyMinute();
        $schedule->command('travel in_station_arrival')->everyMinute();
        $schedule->command('travel in_station')->everyMinute();
        $schedule->command('travel in_station_departure')->everyMinute();
        $schedule->command('travel arrival')->everyMinute();
        $schedule->command('planning incident_before_travel')->everyTenMinutes();
        $schedule->command('planning incident_travel')->everyFifteenMinutes();

        $schedule->command('maintenance verifTask')->everyThirtySeconds();
        $schedule->command('maintenance verifTasking')->everyThirtySeconds();
        $schedule->command('maintenance autoMaintenance')->hourly();

        $schedule->command('generate:bonus')->monthlyOn(1, '00:00:00');
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
