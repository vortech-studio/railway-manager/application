<?php

namespace App\Models\User;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserDelivery extends Model
{
    public $timestamps = false;
    protected $casts = [
        'start_at' => "datetime",
        "end_at" => "datetime"
    ];
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function user_hub(): BelongsTo
    {
        return $this->belongsTo(UserHub::class);
    }

    public function user_ligne(): BelongsTo
    {
        return $this->belongsTo(UserLigne::class);
    }

    public function user_engine(): BelongsTo
    {
        return $this->belongsTo(UserEngine::class);
    }

    public function user_composition(): BelongsTo
    {
        return $this->belongsTo(UserComposition::class);
    }

    public function getRateFromDelivery(): float|int
    {
        return match ($this->type) {
            "hub" => 15 / $this->user->company->livraison,
            "ligne" => 30 / $this->user->company->livraison,
            "engine" => 85 / $this->user->company->livraison,
            "composition" => 45 / $this->user->company->livraison,
        };
    }

    public function timerRestant(string $format = 'percent')
    {
        if($format == 'percent') {
            $heure_actuel = Carbon::now()->timestamp;

            $heure_debut = $this->start_at->timestamp;
            $heure_fin = $this->end_at->timestamp;

            $diff = $heure_actuel - $heure_debut;

            $pourcentage_rest = ($diff / ($heure_fin - $heure_debut)) * 100;

            return round($pourcentage_rest);
        } else {
            return $this->end_at->diffInMinutes(now())->diffForHumans();
        }
    }
}
