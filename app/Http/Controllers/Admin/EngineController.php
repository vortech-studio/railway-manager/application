<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\Admin\TransferImageJob;
use App\Models\Core\Engine;
use App\Models\Core\EngineTechnical;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EngineController extends Controller
{
    public function index()
    {
        return view('admin.engine.index', [
            "engines" => Engine::all()
        ]);
    }

    public function create()
    {
        return view('admin.engine.create');
    }

    public function store(Request $request)
    {
        //dd($request->all(), $request->nb_type_marchandise);
        $engine = Engine::create([
            "name" => $request->input('name'),
            "slug" => \Str::slug($request->input('name')),
            "type_train" => $request->input('type_train'),
            "type_engine" => $request->input('type_engine'),
            "type_energy" => $request->input('type_energy'),
            "velocity" => $request->input('max_speed'),
            "nb_passager" => $request->input('type_marchandise') == 'passagers' ? $request->input('nb_type_marchandise') : 0,
            "price_achat" => Engine::calcPriceCheckout(
                $request->longueur,
                $request->largeur,
                $request->hauteur,
                $request->poids,
                $request->essieux,
                $request->type_motor,
                $request->type_marchandise,
                $request->type_engine,
                $request->puissance
            ),
            "price_maintenance" => Engine::calcPriceMaintenance(
                $request->get('type_energy'),
                Engine::calcPriceCheckout(
                    $request->longueur,
                    $request->largeur,
                    $request->hauteur,
                    $request->poids,
                    $request->essieux,
                    $request->type_motor,
                    $request->type_marchandise,
                    $request->type_engine,
                    $request->puissance
                ),
                Engine::calcDureeMaintenance(
                    $request->date_mise_hors_service,
                    Engine::calcSurface($request->longueur, $request->largeur, $request->hauteur),
                    $request->date_mise_en_service
                ),
                $request->gasoil,
            ),
            "duration_maintenance" => Engine::calcDureeMaintenance(
                $request->date_mise_hors_service,
                Engine::calcSurface($request->longueur, $request->largeur, $request->hauteur),
                $request->date_mise_en_service
            ),
            "price_location" => Engine::calcPriceLocation(
                Engine::calcPriceCheckout(
                    $request->longueur,
                    $request->largeur,
                    $request->hauteur,
                    $request->poids,
                    $request->essieux,
                    $request->type_motor,
                    $request->type_marchandise,
                    $request->type_engine,
                    $request->puissance
                )
            ),
            'in_game' => $request->has('in_game'),
            'in_shop' => $request->has('in_shop'),
        ]);
        $technical = EngineTechnical::create([
            "engine_id" => $engine->id,
            "longueur" => $request->input('longueur'),
            "largeur" => $request->input('largeur'),
            "hauteur" => $request->input('hauteur'),
            "essieux" => $request->input('essieux'),
            "max_speed" => $request->input('max_speed'),
            "poids" => $request->input('poids'),
            "traction_force" => Engine::calcTractionForce($request->input('poids')),
            "type_motor" => $request->input('type_motor'),
            "power_motor" => $request->input('puissance'),
            "start_exploi" => Carbon::createFromDate($request->input('date_mise_service'), 1, 1),
            "end_exploi" => Carbon::createFromDate($request->input('date_mise_hors_service'), now()->month, now()->day),
            "gasoil" => $request->input('gasoil') ?? null,
            "type_marchandise" => $request->input('type_marchandise'),
            "nb_type_marchandise" => $request->input('nb_type_marchandise') ?? null,
        ]);

        if($request->hasFile('image')) {
            \Storage::disk('engines')->put($engine->type_engine.'/'.$engine->slug.'.'.$request->file('image')->getClientOriginalExtension(), $request->file('image')->get());
            $engine->image = $engine->slug.'.'.$request->file('image')->getClientOriginalExtension();
            $engine->save();
            toast()->info('Image en cours de traitement', 'Merci de patienter quelques secondes');
        }

        alert()->success('Matériel Roulant ajouté avec succès', 'Succès');
        return redirect()->route('admin.engine.index');

    }

    public function show($id)
    {
        return view('admin.engine.show', [
            "engine" => Engine::findOrFail($id)
        ]);
    }

    public function edit($id)
    {
        return view('admin.engine.edit', [
            "engine" => Engine::findOrFail($id)
        ]);
    }

    public function delete($id)
    {
        try {
            $engine = Engine::findOrFail($id);
            Storage::disk('engines')->delete($engine->type_engine.'/'.$engine->image);
            Engine::findOrFail($id)->delete();
            return response()->json();
        } catch (\Exception $e) {
            return response()->json([
                "error" => $e->getMessage()
            ], 500);
        }

    }
}
