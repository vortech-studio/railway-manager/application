<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\GareEquipement;
use App\Models\Core\Gares;
use App\Models\Core\GareTransportation;
use Maatwebsite\Excel\Facades\Excel;

class ExportGareController extends Controller
{
    public function __invoke()
    {
        $gares = Gares::select([
            "id",
            "name",
            "code_uic",
            "type_gare",
            "latitude",
            "longitude",
            "region",
            "pays",
            "superficie_infra",
            "superficie_quai",
            "long_quai",
            "nb_quai",
            "commerce",
            "pub",
            "parking",
            "frequentation",
            "nb_habitant_city",
            "time_day_work",
            "created_at",
            "updated_at",
        ])->get();


        $file = fopen(public_path('/storage/gares.json'), 'w');

        fwrite($file, json_encode($gares));

        fclose($file);
        $this->exportTransportation();
        $this->exportEquipements();
        return response()->json();
    }

    private function exportTransportation()
    {
        $transportations = GareTransportation::all();

        $file = fopen(public_path('/storage/gares_transportation.json'), 'w');

        fwrite($file, json_encode($transportations));

        fclose($file);
        return;
    }

    private function exportEquipements()
    {
        $equipements = GareEquipement::all();
        $file = fopen(public_path('/storage/gares_equipements.json'), 'w');

        fwrite($file, json_encode($equipements));

        fclose($file);

        return;
    }
}
