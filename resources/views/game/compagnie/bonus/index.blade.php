@extends("game.template")

@section("css")
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/bonus.png') }}" alt="">
            </div>
            <x-base.underline
                title="Connexion Quotidienne"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.compagnie.partials.menu")
    </div>
    <div class="d-flex flex-row justify-content-between align-items-center p-5 mb-10">
        <span class="fs-2x fw-bolder"><span class="text-yellow-600">{{ auth()->user()->bonuses()->count() }}</span> Jours de connexion ce mois-ci</span>
        <div>
            <button data-bs-toggle="modal" data-bs-target="#modalShowBonus" class="btn btn-icon btn-secondary btn-lg me-3"><i class="fa-solid fa-gift fs-1 text-black"></i> </button>
            <button class="btn btn-icon btn-secondary btn-lg"><i class="fa-solid fa-question fs-1 text-black"></i> </button>
        </div>
    </div>
    <div class="d-flex flex-wrap align-items-center justify-content-center">
        @foreach(\App\Models\Core\Bonus::all() as $bonus)
        <button class="btn btn-flex w-120px h-170px mb-5 bg-gray-400 p-5 rounded-3 @if(auth()->user()->bonuses()->where('bonus_id', $bonus->id)->exists()) opacity-50 @endif me-10 btnBonus" data-id="{{ $bonus->id }}">
            <div class="d-flex flex-column justify-content-center align-items-center h-100">
                <img src="{{ asset('/storage/icons/'.$bonus->type.'.png') }}" alt="" class="w-80px h-80px" />
                <span class="fs-4">X {{ $bonus->qte }}</span>
                <span class="fs-2 fw-bold text-center">Jour {{ $bonus->number_day }}</span>
                @if(auth()->user()->bonuses()->where('bonus_id', $bonus->id)->exists())
                <div class="position-relative">
                    <img src="{{ asset('/storage/icons/checked_yellow.png') }}" alt="" class="w-70px h-70px position-absolute" style="bottom: 48px; right: -31px;">
                </div>
                @endif
            </div>
        </button>
        @endforeach
    </div>
    <div class="modal fade" tabindex="-1" id="modalShowBonus">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Historique des récompense</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <div class="h-300 scroll">
                        <div class="d-flex flex-column">
                            @foreach(auth()->user()->bonuses()->get() as $bonus)
                            <div class="d-flex align-items-center bg-gray-200 shadow mb-3 p-5">
                                <img src="{{ asset('/storage/icons/'.$bonus->type.'.png') }}" class="w-70px h-70px" alt="">
                                <div class="d-flex flex-column">
                                    <div class="fs-2">{{ $bonus->designation }}</div>
                                    <div class="fs-4">{{ $bonus->pivot->created_at->format("d.m.Y H:i") }}</div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        document.querySelectorAll('.btnBonus').forEach(btn => {
            btn.addEventListener('click', e => {
                e.preventDefault()
                let id = btn.getAttribute('data-id')
                $.ajax({
                    url: '{{ route('api.game.claimBonus') }}',
                    data: {'user_id': '{{ auth()->user()->id }}', 'bonus_id': id},
                    success: data => {
                        if(data.result === false) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: data.message,
                            })
                        } else {
                            btn.classList.add('opacity-50')
                            Swal.fire({
                                icon: 'success',
                                title: 'Bravo !',
                                text: data.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    }
                })
            })
        })
    </script>
@endsection
