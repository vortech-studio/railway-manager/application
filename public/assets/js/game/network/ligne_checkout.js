let typeIcon = {
    'hub': {url: '/storage/icons/hub.png', talle: [45, 48]},
    'train': {url: '/storage/icons/train_map.png', talle: [32, 37]},
    'station': {url: '/storage/icons/train-station.png', talle: [32, 37]}
}

function addMarker(options, map, type = 'hub') {
    const icon = L.icon({
        iconUrl: typeIcon[type].url,
        iconSize: typeIcon[type].talle, // size of the icon
        iconAnchor: [16, 18], // point of the icon which will correspond to marker's location
        popupAnchor: [1, -9]
    })
    const marker = L.marker([options.lat, options.lng], {
        title: options.title,
        draggable: options.draggable,
        icon: icon
    });
    marker.addTo(map);

    marker.on('dragend', function (event) {
        coords = event.target._latlng;
        showNewCoords(event.target._latlng, event.target);
        if (followMarker) {
            map.setView(event.target._latlng);
        }
    });

    return marker;
}

function addPopup(marker, popupContent) {
    marker.bindPopup('', {'className': 'custom'})
    let popup = marker.getPopup()
    popup.setContent(popupContent)
    marker.openPopup()
}


function showNewCoords(coords, marker) {
    marker
        .bindPopup(`lat: ${coords.lat} - lng: ${coords.lng}`)
        .openPopup();
}

function MapInitLignes(ligne, stations, cos, containerDiv) {
    let followMarker = false;
    let coords = {};
    const zoomLevel = 7

    let container = L.DomUtil.get(containerDiv);
    if (container != null) {
        container._leaflet_id = null;
    }
    let map = null;

    const hubsLatLng = [];

    map = L.map(containerDiv).setView([ligne.station_start.latitude, ligne.station_start.longitude], zoomLevel);
    const secondLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    secondLayer.addTo(map);

    const hubOptionsStart = {
        lat: ligne.station_start.latitude,
        lng: ligne.station_start.longitude,
        title: ligne.station_start.name,
        draggable: false,
    };

    const hubOptionsEnd = {
        lat: ligne.station_end.latitude,
        lng: ligne.station_end.longitude,
        title: ligne.station_end.name,
        draggable: false,
    };

    let hubMarkerStart = addMarker(hubOptionsStart, map, 'hub');
    hubMarkerStart.on('click', function (event) {
        addPopup(hubMarkerStart, `
                <h3>${ligne.station_start.name}</h3>
            `);
    });

    let hubMarkerEnd = addMarker(hubOptionsEnd, map, 'hub');
    hubMarkerEnd.on('click', function (event) {
        addPopup(hubMarkerEnd, `
                <h3>${ligne.station_end.name}</h3>
            `);
    });

    stations.forEach(station => {
        const stationOptions = {
            lat: station.gare.latitude,
            lng: station.gare.longitude,
            title: station.gare.name,
            draggable: false,
        };
        let stationMarker = addMarker(stationOptions, map, 'station');
        stationMarker.on('click', function (event) {
            addPopup(stationMarker, `
                <h3>${station.gare.name}</h3>
            `);
        });

    });

    let typeLine = {
        "ter": {color: 'blue'},
        "tgv": {color: 'red'},
        "ic": {color: 'purple'},
        "othe": {color: 'black'},
    }
    console.log(cos)
    let polyline = L.polyline(cos, {color: typeLine[ligne.type_ligne].color}).addTo(map)

    var bounds = L.latLngBounds(cos);
    map.fitBounds(bounds);


}
