<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardHoldersTable extends Migration
{
    public function up(): void
    {
        Schema::create('card_holders', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum("type_avantage", ["argent", "research", "rate_research", "credit_impot", "engin", "audit_int", "audit_ext", "simulation"]);
            $table->float('value', 16);

            $table->foreignId('card_holder_category_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('card_holders');
    }
}
