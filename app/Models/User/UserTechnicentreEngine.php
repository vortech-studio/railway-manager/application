<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserTechnicentreEngine extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'user_technicentre_engine';

    public function technicentre()
    {
        return $this->belongsTo(UserTechnicentre::class, 'user_technicentre_id');
    }

    public function engine()
    {
        return $this->belongsTo(UserEngine::class, 'user_engine_id');
    }
}
