<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMouvementsTable extends Migration
{
    public function up(): void
    {
        Schema::create('mouvements', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->decimal('amount', 16, 2);
            $table->enum('type_account', ['charge', 'revenue']);
            $table->enum('type_mvm', [
                "electricite",
                "gasoil",
                "salaire_technicien",
                "salaire_commercial",
                "salaire_administratif",
                "salaire_reseau",
                "pret",
                "interet",
                "taxe",
                "maintenance_vehicule",
                "maintenance_technicentre",
                "billetterie",
                "rent_trajet_aux",
                "commerce",
                "publicite",
                "parking",
                "impot",
                "subvention",
                "achat_materiel",
                "achat_ligne",
                "location_materiel",
                "cout_trajet",
                "divers",
                'vente_hub',
                'vente_ligne',
                'vente_engins',
                'achat_hub'
            ]);
            $table->timestamps();

            $table->foreignId('user_company_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_hub_id')
               ->nullable()
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_ligne_id')
               ->nullable()
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('mouvements');
    }
}
