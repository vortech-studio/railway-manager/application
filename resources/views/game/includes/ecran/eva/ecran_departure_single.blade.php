@mobile
    <div class="loadMobile">
        <div class="d-flex flex-column p-2">
            <div class="d-flex flex-row justify-content-between align-items-center p-5">
                <div class="d-flex flex-row">
                    <img src="{{ asset('/storage/logos/logo_ter.svg') }}" class="w-60px me-2" alt="">
                    <div class="fs-1 fw-bolder text-color-eva">TER 856 630</div>
                </div>
                <span class="badge badge-square bg-info">En transit</span>
            </div>
            <div class="d-flex align-items-end bg-gray-400 p-5 scroll scroll-x w-100">
                <img src="{{ asset('/storage/engines/automotrice/B-84500-0.gif') }}" class="" alt="" />
                <img src="{{ asset('/storage/engines/automotrice/B-84500-1.gif') }}" class="" alt="" />
                <img src="{{ asset('/storage/engines/automotrice/B-84500-2.gif') }}" class="" alt="" />
                <img src="{{ asset('/storage/engines/automotrice/B-84500-3.gif') }}" class="" alt="" />
            </div>
            <div class="d-flex flex-column align-items-start p-2">
                <small class="mb-1">Train "Regiolis" - 4 Voitures - 211 Places</small>
                <div class="fs-3">Destination Nantes</div>
            </div>
            <a href="" class="btn btn-link d-flex align-items-center p-3 bg-gray-300 rounded-3">
                <div class="symbol symbol-15px symbol-circle me-3">
                    <i class="fa-solid fa-info-circle fs-2x text-danger"></i>
                    <span class="symbol-badge badge badge-sm badge-circle bg-dark start-100 text-white">1</span>
                </div>
                <span>Perturbation en cours sur le trajet...</span>
                <div class="flex-shrink">
                    <i class="fa-solid fa-angle-right text-muted"></i>
                </div>
            </a>
            <div class="d-flex align-items-center p-5">
                <span class="badge badge-lg badge-primary me-5">05:44</span>
                <div class="w-25 text-center">
                    <span class="bullet bullet-dot w-20px h-20px bg-white border border-3 border-primary me-5"></span>
                </div>
                <div class="fw-bolder fs-2">Nantes</div>
            </div>
            <div class="d-flex align-items-center p-5">
                <span class="badge badge-lg badge-primary me-5">05:44</span>
                <div class="w-25 text-center">
                    <span class="bullet bullet-dot w-15px h-15px bg-white border border-3 border-primary me-5"></span>
                </div>
                <div class="d-flex flex-column">
                    <div class="fw-semibold fs-3">Nantes</div>
                    <small class="text-gray-500">1 min d'arret</small>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', () => {
            let contentLoadMobile = document.querySelector('.loadMobile')
            let blockContentLoadMobile = new KTBlockUI(contentLoadMobile, {
                overlayClass: 'bg-gray-500 bg-opacity-50'
            })

            blockContentLoadMobile.block()

            $.ajax({
                url: '/api/game/ecran/departure/{{ $planning->id }}',
                data: {"user_id": {{ auth()->user()->id }}},
                success: function (data) {
                    blockContentLoadMobile.release()
                    contentLoadMobile.innerHTML = data.content_mobile
                }
            })

            setInterval(() => {
                $.ajax({
                    url: '/api/game/ecran/departure/{{ $planning->id }}',
                    data: {"user_id": {{ auth()->user()->id }}},
                    success: function (data) {
                        blockContentLoadMobile.release()
                        contentLoadMobile.innerHTML = data.content_mobile
                    }
                })
            }, 10000)
        })
    </script>
@else
    <div class="d-flex flex-column bg-color-eva border border-5 border-primary rounded-3 shadow-lg ">
        <div class="position-relative">
            <div class="position-absolute top-0 end-0">
                <div class="d-flex flex-row justify-content-between align-items-center h-75px p-0 m-0 bg-gray-300">
                    <div class=""></div>
                    <div class="d-flex flex-row bg-color-eva text-white border border-2 border-indigo-900 border-radius-2 p-5 justify-content-center align-items-end rounded-3">
                        <span id="clock-hours" class="fs-1">23</span>
                        <span class="animation-blink">
                            <span class="animation-blink-clock fs-1">:</span>
                        </span>
                        <span id="clock-minutes" class="fs-1">32</span>
                        <small id="clock-seconds" class="fs-3 ms-1 text-orange-600">37</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column loadDesktop">

        </div>
    </div>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', () => {
            let contentLoadMobile = document.querySelector('.loadDesktop')
            let blockContentLoadMobile = new KTBlockUI(contentLoadMobile, {
                overlayClass: 'bg-gray-500 bg-opacity-50'
            })

            blockContentLoadMobile.block()

            $.ajax({
                url: '/api/game/ecran/departure/{{ $planning->id }}',
                data: {"user_id": {{ auth()->user()->id }}},
                success: function (data) {
                    blockContentLoadMobile.release()
                    contentLoadMobile.innerHTML = data.content_desktop
                }
            })

            setInterval(() => {
                $.ajax({
                    url: '/api/game/ecran/departure/{{ $planning->id }}',
                    data: {"user_id": {{ auth()->user()->id }}},
                    success: function (data) {
                        blockContentLoadMobile.release()
                        contentLoadMobile.innerHTML = data.content_desktop
                    }
                })
            }, 10000)
        })
    </script>
@endmobile
