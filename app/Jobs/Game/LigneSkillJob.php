<?php

namespace App\Jobs\Game;

use App\Models\User\UserDelivery;
use App\Models\User\UserHubSkill;
use App\Models\User\UserLigneSkill;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LigneSkillJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserLigneSkill $userHubSkill, public UserDelivery $userDelivery)
    {
    }

    public function handle(): void
    {
        $jobID = $this->job->getJobId();
        $startTime = now()->timestamp;
        $totalTime = $this->userDelivery->end_at->diffInSeconds(now());
        app('redis')->hMSet("job:$jobID", [
            "start_time" => $startTime,
            "end_time" => $startTime + $totalTime,
            "accelerated" => false
        ]);

        $this->executeAction();
    }

    private function executeAction()
    {
        match($this->userHubSkill->action) {
            "improveDispatch" => $this->improveDispatch(),
            "openCommerce" => $this->openCommerce(),
            "commercialLocation" => $this->commercialLocation(),
            "openPublicity" => $this->openPublicity(),
            "publicityLocation" => $this->publicityLocation(),
            "openParking" => $this->openParking(),
            "parkingLocation" => $this->parkingLocation(),
        };

        $this->userHubSkill->level_actual += 1;
        $this->userHubSkill->save();

        $this->userHubSkill->user_hub->user->addPoints(100);

        $this->userDelivery->delete();
    }

    private function setActionValueFromCompetence()
    {
        return $this->userHubSkill->calcActionValueFromLevel($this->userHubSkill->level_actual + 1);
    }
    private function improveDispatch()
    {
        $this->userHubSkill->user_hub->limit_ligne = $this->setActionValueFromCompetence();
        $this->userHubSkill->user_hub->save();
    }

    private function openCommerce()
    {
        $this->userHubSkill->user_hub->commerce = true;
        $this->userHubSkill->user_hub->save();
    }

    private function commercialLocation()
    {
        $this->userHubSkill->user_hub->nb_commerce_limit = $this->setActionValueFromCompetence();
        $this->userHubSkill->user_hub->save();
    }

    private function openPublicity()
    {
        $this->userHubSkill->user_hub->publicity = true;
        $this->userHubSkill->user_hub->save();
    }

    private function publicityLocation()
    {
        $this->userHubSkill->user_hub->nb_publicity_limit = $this->setActionValueFromCompetence();
        $this->userHubSkill->user_hub->save();
    }

    private function openParking()
    {
        $this->userHubSkill->user_hub->parking = true;
        $this->userHubSkill->user_hub->save();
    }

    private function parkingLocation()
    {
        $this->userHubSkill->user_hub->nb_parking_limit = $this->setActionValueFromCompetence();
        $this->userHubSkill->user_hub->save();
    }
}
