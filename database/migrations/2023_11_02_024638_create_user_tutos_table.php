<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTutosTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_tutos', function (Blueprint $table) {
            $table->id();
            $table->boolean('home')->default(0);
            $table->boolean('network')->default(0);
            $table->boolean('hub_checkout')->default(0);
            $table->boolean('ligne_checkout')->default(0);
            $table->boolean('planning_auto')->default(0);
            $table->boolean('planning_edit')->default(0);
            $table->boolean('engine_checkout')->default(0);
            $table->boolean('engine_rent')->default(0);
            $table->boolean('carnet')->default(0);
            $table->string('incident')->default(0);
            $table->string('maint_group')->default(0);

            $table->foreignId('user_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_tutos');
    }
}
