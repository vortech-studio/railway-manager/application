Cher PDG,<br>
<br>
<p>
    Je tiens à vous informer que la vente du hub {{ Str::upper($hub->hub->gare->name) }} pour un montant de {{ eur($amount) }} a été confirmée.
    Je vous remercie pour votre vente et je tiens à vous confirmer que le paiement a déjà été effectué sur votre compte avec succès.
</p>
<p>
    Nous tenons également à vous informer que le hub a été vendu avec les lignes suivantes :
</p>
<ul>
    @foreach($hub->lignes as $ligne)
        <li>{{ $ligne->ligne->name }} - {{ eur($ligne->simulateSelling()) }}</li>
    @endforeach
</ul>
<p>
    En tant que responsable du service des ventes des services ferroviaires {{ \App\Models\User\Mailbox::generateFrom('vente_ligne', $ligne) }}, je m'engage à fournir un service de qualité et à veiller à votre entière satisfaction tout au long de votre expérience lors de la vente de votre hub.<br>
    Je vous remercie pour votre confiance et je reste à votre disposition pour toute information complémentaire.
</p>
<p>
    Cordialement,<br>
    <strong>{{ $name_responsable }}</strong><br>
    <i>Responsable des ventes des lignes ferroviaires</i><br>
    <b>{{ \App\Models\User\Mailbox::generateFrom('vente_ligne', $ligne) }}</b>
</p>
