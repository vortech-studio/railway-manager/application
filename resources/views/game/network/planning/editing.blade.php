@extends("game.template")

@section("css")
    <link href="/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <input type="hidden" name="user_id" id="user_id" value="{{ auth()->user()->id }}">
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/journey.png') }}" alt="">
            </div>
            <x-base.underline
                title="Planifications des trajets"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.network.partials.menu")
    </div>
    <div class="card card-custom card-stretch shadow-lg">
        <div class="card-header">
            <h3 class="card-title">Planification</h3>
            <div class="card-toolbar">
                <x-form.switches
                    name="automated_planning"
                    value="1"
                    label="Planification Automatique"
                    :check="auth()->user()->automated_planning ? 'checked' : ''" />
            </div>
        </div>
        <div class="card-body">
            <div id="automated_planning" class="d-none">
                <x-base.alert
                    type="solid"
                    color="primary"
                    icon="question-circle"
                    title="Planification Automatique"
                    content="La planification automatique permet de planifier automatiquement les trajets de vos lignes.<br>Vous ne pouvez donc pas modifier les trajets de vos lignes.<br>Si vous désactivez la planification automatique, vous pourrez modifier les trajets de vos lignes." />
                <div class="card">
                    <div class="card-header d-flex flex-row justify-content-center">
                        <div class="card-title dateToday" data-date="{{ now() }}"></div>
                    </div>
                    <div class="card-body" id="timelinePlanning" data-id="{{ auth()->user()->id }}"></div>
                </div>
            </div>
            <div id="manual_planning" class="d-none">
                <div class="position-fixed bottom-0 end-0 p-3 z-index-3">
                    <div id="kt_docs_toast_toggle" class="toast show bg-light-primary" role="alert" aria-live="assertive" aria-atomic="true" data-bs-autohide="false">
                        <div class="toast-header bg-primary">
                            <i class="ki-duotone ki-question fs-2 text-white me-3"><span class="path1"></span><span class="path2"></span></i>
                            <strong class="me-auto">Aide disponible</strong>
                            <small>&nbsp;</small>
                            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                        </div>
                        <div class="toast-body">
                            <p>Un tutoriel est disponible pour cette page</p>
                            <a href="" class="btn btn-sm btn-secondary">Cliquez ici</a>
                        </div>
                    </div>
                </div>
                <div class="card shadow-lg" id="loadManualCalendar">
                    <div class="card-header">
                        <div class="card-title">&nbsp;</div>
                        <div class="card-toolbar">
                            <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newPlanning">Nouvelle planification</button>
                        </div>
                    </div>
                    <div class="card-body" id="manualCalendar">
                        <div class="rounded-2 table-responsive">
                            <table class="table table-striped table-bordered table-dark g-7 align-middle">
                                <thead>
                                <tr>
                                    <th>Matériel</th>
                                    @for($i=0; $i <= 6; $i++)
                                        <th>{{ now()->locale('fr_FR')->startOfWeek(\Carbon\Carbon::MONDAY)->addDays($i)->dayName }}</th>
                                    @endfor
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($engines as $engine)
                                        @if($engine->user_ligne()->exists())
                                            <tr>
                                                <td>{{ $engine->engine->name }}</td>
                                                @for($i=0; $i <= 6; $i++)
                                                    <td>
                                                        @isset($engine->planning_constructors)
                                                            @foreach($engine->planning_constructors as $planning)
                                                                @if(in_array($i, json_decode($planning->dayOfWeek)))
                                                                    <span class="badge badge-sm bg-color-{{ $engine->engine->type_train }} text-white">{{ $planning->start_at->format("H:i") }} - {{ $planning->end_at->format("H:i") }} | {{ $engine->user_ligne->ligne->name }}</span>
                                                                @endif
                                                            @endforeach
                                                        @endisset
                                                    </td>
                                                @endfor
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal bg-body fade" tabindex="-1" id="newPlanning">
        <div class="modal-dialog modal-lg  modal-dialog-centered">
            <div class="modal-content shadow-none">
                <div class="modal-header">
                    <h5 class="modal-title">Nouvelle planification</h5>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-2x"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form action="{{ route('planning.store') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="mb-10">
                            <label for="engines_id" class="form-label required">Matériel roulant</label>
                            <select name="engines_id" id="engines_id" class="form-control selectpicker" required data-live-search="true" data-placeholder="--Sélectionner une machine--">
                                @foreach($engines as $engine)
                                    @if($engine->user_ligne()->exists())
                                        <option value="{{ $engine->id }}"
                                                data-content="
                                            <div class='d-flex flex-row align-items-center'>
                                                <div class='symbol symbol-15px bg-color-{{ $engine->engine->type_train }} me-5'>&nbsp;</div>
                                                <div class='d-flex flex-column'>
                                                    <div class='fw-bold'>{{ $engine->engine->name }} | {{ $engine->number }}</div>
                                                    <small class='text-color-{{ $engine->engine->type_train }}'>{{ $engine->user_ligne->ligne->name }}</small>
                                                </div>
                                            </div
                                            "></option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-10">
                            <label for="date_depart" class="form-label required">Heure de départ</label>
                            <input type="text" id="date_depart" name="date_depart" class="form-control" required>
                        </div>
                        <div class="d-flex flex-row justify-content-around align-items-center mb-10">
                            <label for="" class="form-label">Jour du planning</label>
                            <div class="form-check">
                                <input class="form-check-input" name="day[]" type="checkbox" value="0" id="lundi">
                                <label class="form-check-label" for="lundi">
                                    Lundi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="day[]" type="checkbox" value="1" id="mardi">
                                <label class="form-check-label" for="mardi">
                                    Mardi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="day[]" type="checkbox" value="2" id="mercredi">
                                <label class="form-check-label" for="mercredi">
                                    Mercredi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="day[]" type="checkbox" value="3" id="jeudi">
                                <label class="form-check-label" for="jeudi">
                                    Jeudi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="day[]" type="checkbox" value="4" id="vendredi">
                                <label class="form-check-label" for="vendredi">
                                    Vendredi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="day[]" type="checkbox" value="5" id="samedi">
                                <label class="form-check-label" for="samedi">
                                    Samedi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="day[]" type="checkbox" value="6" id="dimanche">
                                <label class="form-check-label" for="dimanche">
                                    Dimanche
                                </label>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-around align-items-center mb-10">
                            <div class="d-flex flex-column justify-content-center">
                                <label for="" class="form-label">Répétition</label>
                                <div class="form-floating mb-7">
                                    <input type="number" class="form-control" name="number_repeat" id="number_repeat" value="1"/>
                                    <label for="floatingInput">Nombre de répétition</label>
                                </div>
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" name="repeat" type="radio" value="hebdo" id="hebdo">
                                <label class="form-check-label" for="hebdo">
                                    Hebdomadaire
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="repeat" type="radio" value="mensuel" id="mensuel">
                                <label class="form-check-label" for="mensuel">
                                    Mensuel
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="repeat" type="radio" value="trim" id="trim">
                                <label class="form-check-label" for="trim">
                                    Trimestriel
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="repeat" type="radio" value="sem" id="sem">
                                <label class="form-check-label" for="sem">
                                    Semestriel
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="repeat" type="radio" value="annual" id="annual">
                                <label class="form-check-label" for="annual">
                                    Annuel
                                </label>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")

    <script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.9/index.global.min.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/@fullcalendar/core@6.1.9/locales-all.global.min.js'></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/game/network/planning_editing.js') }}"></script>
    <script type="text/javascript">
        let checked = document.querySelector('[name="automated_planning"]').checked;
        document.querySelector('[name="automated_planning"]').addEventListener('change', e => {
            let value = e.target.checked ? 1 : 0;
            $.ajax({
                url: '/api/account/'+{{ auth()->user()->id }}+'/automated-planning',
                method: 'POST',
                data: {
                    automated_planning: value
                },
                success: () => {
                    toastr.success("Modification effectuée avec succès !", "Planning Automatique")
                    checkedAutoPlanning();
                }
            })
        })

        function checkedAutoPlanning() {
            if(checked) {
                document.querySelector('#automated_planning').classList.remove('d-none');
                document.querySelector('#manual_planning').classList.add('d-none');
            } else {
                document.querySelector('#automated_planning').classList.add('d-none');
                document.querySelector('#manual_planning').classList.remove('d-none');
            }
        }
        checkedAutoPlanning();

        Inputmask({
            "mask": "99:99",
        }).mask('[name="date_depart"]')

    </script>

@endsection
