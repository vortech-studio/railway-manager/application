<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\Admin\TransferImageJob;
use App\Models\Core\Achievement;
use Illuminate\Http\Request;

class BadgeAchievementController extends Controller
{
    /**
     * Enregistre un badge.
     *
     * @param Request $request Les données de la requête
     * @return \Illuminate\Http\RedirectResponse La redirection vers la page d'index des badges
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'string',
            'image' => 'file|mimes:png,jpg,jpeg',
        ]);

        $achievement = Achievement::create([
            'name' => $request->name,
            'description' => $request->description_achievement,
            'is_secret' => $request->has('is_secret') ?? false,
        ]);

        if($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->extension();
            $path = 'public/badges/' . $achievement->id . '.' . $extension;
            $file->storePubliclyAs($path);
            $achievement->image = $achievement->id . '.' . $extension;
            $achievement->save();
        }

        toast()->success('Badge créé avec succès !');
        return redirect()->route('admin.badge.index');
    }

    /**
     * Met à jour une réalisation.
     *
     * @param int $id L'identifiant de la réalisation à mettre à jour.
     * @param Request $request La requête contenant les données de mise à jour.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     * @throws \Illuminate\Validation\ValidationException
     *
     * @return \Illuminate\Http\RedirectResponse Redirige vers la liste des badges.
     */
    public function update($id, Request $request)
    {
        // Valide les données de la requête
        $request->validate([
            'name' => 'required|string',
            'description' => 'string',
            'image' => 'file|mimes:png,jpg,jpeg',
        ]);

        // Récupère la réalisation à mettre à jour
        $achievement = Achievement::findOrFail($id);

        // Met à jour les champs de la réalisation
        $achievement->update([
            'name' => $request->name,
            'description' => $request->description_achievement,
            'is_secret' => $request->has('is_secret') ?? false,
        ]);

        // Vérifie si une nouvelle image a été uploadée
        if($request->hasFile('image')) {
            // Stocke l'image dans le dossier "public/badges"
            $request->file('image')->storePubliclyAs('public/badges', $achievement->id . '.' . $request->file('image')->extension());

            // Met à jour le chemin de l'image dans la réalisation
            $achievement->image = $achievement->id . '.' . $request->file('image')->extension();
            $achievement->save();
        }

        // Affiche un message de succès
        toast()->success('Badge modifié avec succès !');

        // Redirige vers la liste des badges
        return redirect()->route('admin.badge.index');
    }
}
