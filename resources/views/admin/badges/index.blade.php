@extends("admin.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Badges & Récompenses",
        'action' => false,
        'data' => [

        ]
    ])
@endsection

@section("content")
    <div class="card">
        <div class="card-header card-header-stretch">
            <ul class="nav nav-tabs nav-line-tabs nav-stretch nav-justified w-100 fs-4 border-0">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#achievements">Missions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#levels">Niveaux</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#rewards">Récompenses</a>
                </li>
            </ul>
        </div>
        <div class="card-body tab-content">
            <div class="tab-pane fade show active" id="achievements" role="tabpanel">
                <div class="d-flex flex-row justify-content-between align-items-center mb-10">
                    <div class="d-flex align-items-center position-relative my-1">
                        <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                        <input type="text" data-achievement-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Rechercher une mission" />
                    </div>
                    <button class="btn btn-outline btn-outline-primary" data-bs-toggle="modal" data-bs-target="#addAchievement"><i class="fa-solid fa-plus me-2"></i> Nouvelle mission</button>
                </div>
                <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_achievement">
                    <thead>
                        <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                            <th>Désignation</th>
                            <th>Description</th>
                            <th class="min-w-70px text-center">Secret</th>
                            <th class="text-end">Actions</th>
                        </tr>
                    </thead>
                    <tbody class="fw-semibold text-gray-600">
                        @foreach($achievements as $achievement)
                        <tr>
                            <td>
                                <div class="d-flex flex-row align-items-center">
                                    <div class="symbol symbol-50px me-5">
                                        <img src="/storage/badges/{{ $achievement->image }}" alt="image" />
                                    </div>
                                    {{ $achievement->name }}
                                </div>
                            </td>
                            <td>{!! $achievement->description !!}</td>
                            <td class="text-center">{!! $achievement->is_secret_icon !!}</td>
                            <td class="text-end">
                                <button class="btn btn-icon btn-sm btn-outline btn-outline-info" data-action="edit" data-id="{{ $achievement->id }}" data-bs-toggle="tooltip" title="Editer la mission"><i class="fa-solid fa-edit"></i> </button>
                                <button class="btn btn-icon btn-sm btn-outline btn-outline-danger" data-action="delete" data-id="{{ $achievement->id }}" data-bs-toggle="tooltip" title="Supprimer la mission"><i class="fa-solid fa-trash"></i> </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="levels" role="tabpanel">
                <div class="d-flex flex-row justify-content-end align-items-center mb-10">
                    <button class="btn btn-outline btn-outline-primary" data-bs-toggle="modal" data-bs-target="#addLevel"><i class="fa-solid fa-plus me-2"></i> Nouveau niveau</button>
                </div>
                <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_level">
                    <thead>
                    <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                        <th>Niveau</th>
                        <th class="text-end">Actions</th>
                    </tr>
                    </thead>
                    <tbody class="fw-semibold text-gray-600">
                    @foreach($levels as $level)
                        <tr>
                            <td>
                                <div class="d-flex flex-column">
                                    <div class="fw-bolder fs-3">Niveau {{ $level->level }}</div>
                                    <div class="fs-italic fs-5">Niveau Suivant après: <strong>{{ $level->next_level_experience }}</strong> points d'expériences</div>
                                </div>
                            </td>
                            <td class="text-end">
                                <button class="btn btn-icon btn-sm btn-outline btn-outline-info" data-action="edit" data-id="{{ $level->id }}" data-bs-toggle="tooltip" title="Editer le niveau"><i class="fa-solid fa-edit"></i> </button>
                                <button class="btn btn-icon btn-sm btn-outline btn-outline-danger" data-action="delete" data-id="{{ $level->id }}" data-bs-toggle="tooltip" title="Supprimer le niveau"><i class="fa-solid fa-trash"></i> </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="rewards" role="tabpanel"></div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" id="addAchievement">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Nouvelle Mission</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form id="formAddAchievement" action="{{ route('admin.badge.achievement.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <x-form.input
                            name="name"
                            label="Désignation"
                            required="true" />

                        <div class="row d-flex align-items-center">
                            <div class="col-md-10">
                                <x-form.input-file
                                    name="image"
                                    label="Image de la mission" />
                            </div>
                            <div class="col-md-2">
                                <x-form.switch
                                    name="is_secret"
                                    label="Secret"
                                    value="1"
                                    check="false" />
                            </div>
                        </div>

                        <x-form.textarea
                            name="description_achievement"
                            label="Description"
                            class="textEditor"/>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <x-form.button />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="editAchievement">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Editer la mission</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form id="formEditAchievement" action="" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <x-form.input
                            name="name"
                            label="Désignation"
                            required="true" />

                        <div class="row d-flex align-items-center">
                            <div class="col-md-10">
                                <x-form.input-file
                                    name="image"
                                    label="Image de la mission" />
                            </div>
                            <div class="col-md-2">
                                <x-form.switch
                                    name="is_secret"
                                    label="Secret"
                                    value="1"
                                    check="false" />
                            </div>
                        </div>

                        <x-form.textarea
                            name="description_achievement"
                            label="Description" />

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <x-form.button />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="addLevel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Nouveau niveau</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form id="formAddLevel" action="{{ route('admin.badge.level.store') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <x-form.input
                            name="level"
                            label="Niveau"
                            required="true" />

                        <x-form.input
                            name="next_level_experience"
                            label="Expérience pour le prochain niveau"
                            required="true" />
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <x-form.button />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="editLevel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Editer le niveau</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form id="formEditLevel" action="" method="post">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <x-form.input
                            name="level"
                            label="Niveau"
                            required="true" />

                        <x-form.input
                            name="next_level_experience"
                            label="Expérience pour le prochain niveau"
                            required="true" />

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <x-form.button />
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let options = {
            info: !1,
            order: [],
            pageLength: 10,
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
        }

        let table_achievements = $("#liste_achievement").DataTable(options)
        let table_levels = $("#liste_level").DataTable(options)

        $('[data-achievement-filter="search"]').on('keyup', function() {
            table_achievements.search($(this).val()).draw();
        })

        $('[data-action="edit"]').on('click', function() {
            let id = $(this).data('id')
            $.ajax({
                url: "/api/core/badges/achievements/"+id,
                type: "GET",
                success: function(data) {
                    $("#editAchievement .modal-body").find('input[name="name"]').val(data.achievement.name)
                    $("#editAchievement .modal-body").find('input[name="description"]').val(data.achievement.description)
                    $("#formEditAchievement").attr('action', '/admin/badges/achievements/'+data.achievement.id)
                    if(data.achievement.is_secret) {
                        $("#editAchievement .modal-body").find('input[name="is_secret"]').attr('checked', true)
                    } else {
                        $("#editAchievement .modal-body").find('input[name="is_secret"]').attr('checked', false)
                    }
                    $("#editAchievement").modal('show')

                }
            })
        })

        $('[data-action="delete"]').on('click', function() {
            let id = $(this).data('id')
            $.ajax({
                url: "/api/core/badges/achievements/"+id,
                type: "DELETE",
                success: function(data) {
                    toastr.success(data.message)
                    setTimeout(() => {
                        window.location.reload()
                    }, 1000)
                }
            })
        })

        document.querySelector('#liste_level').querySelectorAll('[data-action="edit"]').forEach(btn => {
            btn.addEventListener('click', function() {
                let id = this.dataset.id
                $.ajax({
                    url: "/api/core/badges/levels/"+id,
                    type: "GET",
                    success: function(data) {
                        $("#editLevel .modal-body").find('input[name="level"]').val(data.level.level)
                        $("#editLevel .modal-body").find('input[name="next_level_experience"]').val(data.level.next_level_experience)
                        $("#formEditLevel").attr('action', '/admin/badges/levels/'+data.level.id)
                        $("#editLevel").modal('show')

                    }
                })
            })
        })

        document.querySelector('#liste_level').querySelectorAll('[data-action="delete"]').forEach(btn => {
            btn.addEventListener('click', function() {
                let id = this.dataset.id
                $.ajax({
                    url: "/api/core/badges/levels/"+id,
                    type: "DELETE",
                    success: function(data) {
                        toastr.success(data.message)
                        setTimeout(() => {
                            window.location.reload()
                        }, 1000)
                    }
                })
            })
        })

        ClassicEditor
            .create( document.querySelector( '#description_achievement' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
@endsection
