<?php

namespace App\Models\Core;

use App\Traits\HasCheckout;
use Illuminate\Database\Eloquent\Model;

class CardHolder extends Model
{
    use HasCheckout;
    public $timestamps = false;
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(CardHolderCategory::class);
    }
}
