<?php

namespace App\Http\Controllers\Game\Compagnie;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomiseController extends Controller
{
    public function index()
    {
        return view('game.compagnie.customise.index');
    }

    public function update(Request $request)
    {
        return match ($request->get('action')) {
            "direction" => $this->updateDirection($request),
            "secretary" => $this->updateSecretary($request),
            "company" => $this->updateCompany($request),
        };
    }

    private function updateDirection(Request $request)
    {
        $user = auth()->user();
        $user->name = $request->get('name');
        $user->tpoint -= 5;
        $user->save();

        toastr()->success("Information mise à jour");
        return redirect()->back();
    }

    private function updateSecretary(Request $request)
    {
        $user = auth()->user();
        $user->name_secretary = $request->get('name_secretary');
        $user->tpoint -= 5;
        $user->save();

        toastr()->success("Information mise à jour");
        return redirect()->back();
    }

    private function updateCompany(Request $request)
    {
        $user = auth()->user();
        $user->name_company = $request->get('name_company');
        $user->desc_company = $request->get('desc_company');
        $user->save();

        if($request->hasFile('logo_company')) {
            $request->file('logo_company')->storeAs('public/avatar/company', $user->id.'.png');
            $user->logo_company = $user->id.'.png';
            $user->save();
        }

        toastr()->success("Information mise à jour");
        return redirect()->back();
    }
}
