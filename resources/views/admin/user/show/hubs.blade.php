<div class="d-flex flex-row justify-content-between mb-10">
    <div class="d-flex align-items-center position-relative my-1">
        <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
            <span class="path1"></span>
            <span class="path2"></span>
        </i>
        <input type="text" data-hub-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Rechercher un hub" />
    </div>
    <button class="btn btn-sm btn-outline btn-outline-info" data-bs-toggle="modal" data-bs-target="#addHub"><i class="fa-solid fa-plus me-3"></i> Ajouter un hub au joueur</button>
</div>

<table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_hub">
    <thead>
        <tr>
            <th>Gare</th>
            <th>Date Achat</th>
            <th>Km Ligne</th>
            <th>CA du HUB</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<div class="modal fade" tabindex="-1" id="addHub">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Ajout d'un hub au joueur</h3>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                </div>
                <!--end::Close-->
            </div>
            <form id="formAddHub" action="" method="post">
                @csrf
                <div class="modal-body">
                    <div class="mb-10">
                        <label for="hub_id" class="control-label required">Hub</label>
                        <select name="hub_id" id="hub_id" class="form-control selectpicker" data-live-search="true" required title="---- Selectionner un Hub ----">
                            @foreach(\App\Models\Core\Hub::where('active', true)->get() as $hub)
                                <option value="{{ $hub->id }}">{{ $hub->gare->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
