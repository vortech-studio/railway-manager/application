<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\Blog\Articles;
use App\Notifications\Admin\PostArticleToSocialProviderNotification;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function list(Request $request)
    {
        $articles = Articles::with('author')
            ->where('published', true)
            ->where('type_article', 'news')
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json($articles);
    }
    public function dispublish($id)
    {
        $article = Articles::find($id);
        $article->published = false;
        $article->save();
        return response()->json();
    }

    public function publish($id)
    {
        $article = Articles::find($id);
        $article->published = true;
        $article->save();
        if(!$article->publish_social) {
            $article->author->notify(new PostArticleToSocialProviderNotification($article));
            $article->publish_social = true;
            $article->save();
        }
        return response()->json();
    }
}
