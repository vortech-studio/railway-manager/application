<?php

namespace App\Console\Commands;

use App\Jobs\Game\Planning\InStationTravelJob;
use App\Jobs\Game\Planning\PrepareTravelJob;
use App\Models\Core\LigneStation;
use App\Models\Core\Mouvement;
use App\Models\Core\TypeIncident;
use App\Models\User\UserIncident;
use App\Models\User\UserLigne;
use App\Models\User\UserLigneTarif;
use App\Models\User\UserPlanning;
use App\Models\User\UserPlanningStation;
use App\Notifications\Game\SendMessageNotification;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;

class PlanningCommand extends Command
{
    protected $signature = 'planning {action}';

    protected $description = 'Gestion du planning de trajet';

    public function handle(): void
    {
        match ($this->argument('action')) {
            "create" => $this->createPlanning(),
            "incident_before_travel" => $this->generateIncidentBeforeTravel(),
            "incident_travel" => $this->generateIncidentTravel(),
        };
    }

    /**
     * Crée une planification.
     *
     * @throws Exception En cas d'erreur.
     * @return void
     */
    private function createPlanning(): void
    {
        $userLignes = UserLigne::with('ligne')->get();
        $lignes = [];
        foreach ($userLignes as $userLigne) {
            $lignes[$userLigne->id] = $userLigne->id;
        }

        $chosenLigne = $this->choice('Choisissez une ligne', $lignes);
        $departureDate = $this->ask('Date de départ (format: 2021-01-01 00:00:00)');

        $dataLigne = UserLigne::find($chosenLigne);

        $planning = UserPlanning::create([
            "number_travel" => null,
            "date_depart" => $departureDate,
            "date_arrived" => Carbon::createFromTimestamp(strtotime($departureDate))->addMinutes($userLigne->ligne->time_min),
            "user_hub_id" => $dataLigne->user_hub_id,
            "user_ligne_id" => $dataLigne->id,
            "user_engine_id" => $dataLigne->user_engine_id,
            "user_id" => $dataLigne->user_id,
            "kilometer" => $dataLigne->ligne->distance
        ]);

        $planning->travel()->create([
            "ca_billetterie" => 0,
            "ca_other" => 0,
            "frais_electrique" => 0,
            "frais_gasoil" => 0,
            "frais_autre" => 0,
            "user_planning_id" => $planning->id,
        ]);

        foreach ($dataLigne->ligne->stations as $station) {
            if ($station->id == $dataLigne->ligne->stationStart->id) {
                UserPlanningStation::create([
                    "type_infra" => "station",
                    "name" => $station->gare->name,
                    "departure_at" => $planning->date_depart,
                    "arrival_at" => $planning->date_depart,
                    "user_planning_id" => $planning->id,
                    "ligne_station_id" => $station->id,
                ]);
                continue;
            } elseif ($station->id == $dataLigne->ligne->stationEnd->id) {
                UserPlanningStation::create([
                    "type_infra" => "station",
                    "name" => $station->gare->name,
                    "departure_at" => $planning->date_depart->addMinutes($dataLigne->ligne->time_min),
                    "arrival_at" => $planning->date_depart->addMinutes($dataLigne->ligne->time_min),
                    "user_planning_id" => $planning->id,
                    "ligne_station_id" => $station->id,
                ]);
                continue;
            } else {
                UserPlanningStation::create([
                    "type_infra" => "station",
                    "name" => $station->gare->name,
                    "departure_at" => $planning->date_depart->addMinutes($station->time),
                    "arrival_at" => $planning->date_depart->addMinutes($station->time + LigneStation::time_stop_station_config($station)),
                    "user_planning_id" => $planning->id,
                    "ligne_station_id" => $station->id,
                ]);
            }

        }

    }

    /**
     * Génère un incident avant le départ.
     */
    private function generateIncidentBeforeTravel()
    {
        $faker = \Faker\Factory::create("fr_FR");

        // Récupère les plannings des utilisateurs dont la date de départ est dans les 10 prochaines minutes
        $plannings = UserPlanning::whereBetween('date_depart', [now()->startOfMinute(), now()->addMinutes(10)->endOfMinute()])
            ->where('status', '!=', 'canceled')
            ->get();

        // Vérifie s'il existe au moins un planning
        try {
            if ($plannings->count() > 0) {
                foreach ($plannings as $planning) {
                    // Génère un incident avec une certaine probabilité
                    if ($faker->boolean(25)) {
                        \Log::debug("PlanningCommand::generateIncidentBeforeTravel");
                        $type_incidents = TypeIncident::all()->random();

                        if($type_incidents->severity == 2) {
                            $attemps = rand(15, 60);
                            $incident = $planning->incidents()->create([
                                "niveau" => $type_incidents->severity,
                                "status" => "open",
                                "designation" => UserIncident::getDesignationFromTypeIncident($type_incidents->designation),
                                "amount_reparation" => UserIncident::getAmountReparationFromTypeIncident($planning->ligne, $type_incidents),
                                "user_id" => $planning->user_id,
                                "user_planning_id" => $planning->id,
                                "type_incident_id" => $type_incidents->id,
                                "engine_composition_id" => UserIncident::getCompositionFromTypeIncident($planning->engine, $type_incidents),
                                "user_engine_id" => $planning->user_engine_id,
                                "user_hub_id" => $planning->user_hub_id,
                            ]);

                            // Met à jour le retard du planning avec le nombre de tentatives
                            $planning->retarded_time += $attemps;
                            $planning->status = 'retarded';
                            $planning->save();

                            $planning->date_depart = $planning->date_depart->addMinutes($attemps);
                            $planning->date_arrived = $planning->date_arrived->addMinutes($attemps);
                            $planning->save();

                            $planning->engine->use_percent += $planning->engine->engine->calcVitesseUsure() / 2;
                            $planning->engine->save();

                            // Met à jour les horaires de toutes les stations du planning
                            foreach ($planning->planning_stations()->where('status', '!=', 'done')->get() as $station) {
                                $station->update([
                                    "arrival_at" => $station->arrival_at->addMinutes($attemps),
                                    "departure_at" => $station->departure_at->addMinutes($attemps),
                                ]);
                            }

                            // Envoie une notification à l'utilisateur concerné
                            $planning->user->notify(new SendMessageNotification(
                                'Incident',
                                "Incident lors de la préparation du train N°" . $planning->number_travel."<br>Le train à été retardé de ".$attemps." minutes",
                                $incident->niveau == 1 ? "info" : ($incident->niveau == 2 ? "warning" : "danger"),
                                $incident->niveau == 1 ? asset('/storage/icons/warning_low.png') : ($incident->niveau == 2 ? asset('/storage/icons/warning_middle.png') : asset('/storage/icons/warning_high.png')),
                                route('engine.maintenance.history'),
                                "incident"
                            ));

                            return null;
                        } elseif ($type_incidents->severity == 3) {
                            $incident = $planning->incidents()->create([
                                "niveau" => $type_incidents->severity,
                                "status" => "open",
                                "designation" => UserIncident::getDesignationFromTypeIncident($type_incidents->designation),
                                "amount_reparation" => UserIncident::getAmountReparationFromTypeIncident($planning->ligne, $type_incidents),
                                "user_id" => $planning->user_id,
                                "user_planning_id" => $planning->id,
                                "type_incident_id" => $type_incidents->id,
                                "engine_composition_id" => UserIncident::getCompositionFromTypeIncident($planning->engine, $type_incidents),
                                "user_engine_id" => $planning->user_engine_id,
                                "user_hub_id" => $planning->user_hub_id,
                            ]);

                            // Met à jour le retard du planning avec le nombre de tentatives
                            $planning->status = 'canceled';
                            $planning->save();

                            $planning->engine->use_percent += $planning->engine->engine->calcVitesseUsure() * 2;
                            $planning->engine->save();

                            // Met à jour les horaires de toutes les stations du planning
                            foreach ($planning->planning_stations()->where('status', '!=', 'done')->get() as $station) {
                                $station->update([
                                    "status" => "done"
                                ]);
                            }

                            // Envoie une notification à l'utilisateur concerné
                            $planning->user->notify(new SendMessageNotification(
                                'Incident',
                                "Incident Grave lors de la préparation du train N°" . $planning->number_travel."<br>Le train à été annulée",
                                $incident->niveau == 1 ? "info" : ($incident->niveau == 2 ? "warning" : "danger"),
                                $incident->niveau == 1 ? asset('/storage/icons/warning_low.png') : ($incident->niveau == 2 ? asset('/storage/icons/warning_middle.png') : asset('/storage/icons/warning_high.png')),                                route('engine.maintenance.history'),
                                "incident"
                            ));

                            return null;
                        } else {
                            $attemps = rand(2, 15);
                            $incident = $planning->incidents()->create([
                                "niveau" => $type_incidents->severity,
                                "status" => "open",
                                "designation" => UserIncident::getDesignationFromTypeIncident($type_incidents->designation),
                                "amount_reparation" => UserIncident::getAmountReparationFromTypeIncident($planning->ligne, $type_incidents),
                                "user_id" => $planning->user_id,
                                "user_planning_id" => $planning->id,
                                "type_incident_id" => $type_incidents->id,
                                "engine_composition_id" => UserIncident::getCompositionFromTypeIncident($planning->engine, $type_incidents),
                                "user_engine_id" => $planning->user_engine_id,
                                "user_hub_id" => $planning->user_hub_id,
                            ]);

                            // Met à jour le retard du planning avec le nombre de tentatives
                            $planning->retarded_time += $attemps;
                            $planning->status = 'retarded';
                            $planning->save();

                            $planning->date_depart = $planning->date_depart->addMinutes($attemps);
                            $planning->date_arrived = $planning->date_arrived->addMinutes($attemps);
                            $planning->save();

                            // Met à jour les horaires de toutes les stations du planning
                            foreach ($planning->planning_stations()->where('status', '!=', 'done')->get() as $station) {
                                $station->update([
                                    "arrival_at" => $station->arrival_at->addMinutes($attemps),
                                    "departure_at" => $station->departure_at->addMinutes($attemps),
                                ]);
                            }

                            // Envoie une notification à l'utilisateur concerné
                            $planning->user->notify(new SendMessageNotification(
                                'Incident',
                                "Incident lors de la préparation du train N°" . $planning->number_travel."<br>Le train à été retardé de ".$attemps." minutes",
                                $incident->niveau == 1 ? "info" : ($incident->niveau == 2 ? "warning" : "danger"),
                                $incident->niveau == 1 ? asset('/storage/icons/warning_low.png') : ($incident->niveau == 2 ? asset('/storage/icons/warning_middle.png') : asset('/storage/icons/warning_high.png')),                                route('engine.maintenance.history'),
                                "incident"
                            ));

                            return null;
                        }
                    }
                }
            }
        }catch (Exception $exception) {
            \Log::error($exception);
        }
    }

    /**
     * Generates an incident during travel.
     *
     * @throws Exception description of exception
     * @return Void
     */
    private function generateIncidentTravel()
    {
        $faker = \Faker\Factory::create("fr_FR");

        $plannings = UserPlanning::where('status', 'travel')
            ->orWhere('status', "in_station")
            ->get();

        try {
            if ($plannings->count() > 0) {
                if($faker->boolean(25)) {
                    foreach ($plannings as $planning) {
                        \Log::debug("PlanningCommand::generateIncidentTravel");
                        if ($faker->boolean(25)) {
                            $type_incidents = TypeIncident::where('severity', 3)->get()->random();
                            $attemps = rand(15,60);

                            $incident = $planning->incidents()->create([
                                "niveau" => $type_incidents->severity,
                                "status" => "open",
                                "designation" => UserIncident::getDesignationFromTypeIncident($type_incidents->designation),
                                "amount_reparation" => UserIncident::getAmountReparationFromTypeIncident($planning->ligne, $type_incidents),
                                "user_id" => $planning->user_id,
                                "user_planning_id" => $planning->id,
                                "type_incident_id" => $type_incidents->id,
                                "engine_composition_id" => UserIncident::getCompositionFromTypeIncident($planning->engine, $type_incidents),
                                "user_hub_id" => $planning->user_hub_id,
                            ]);

                            $planning->retarded_time += $attemps;
                            $planning->save();

                            $planning->date_arrived = $planning->date_arrived->addMinutes($attemps);
                            $planning->save();

                            $planning->engine->use_percent += $planning->engine->engine->calcVitesseUsure() * 2;
                            $planning->engine->save();

                            foreach ($planning->planning_stations()->where('status', '!=', 'done')->get() as $station) {
                                $station->update([
                                    "arrival_at" => $station->arrival_at->addMinutes($attemps),
                                    "departure_at" => $station->departure_at->addMinutes($attemps),
                                ]);
                            }

                            $planning->user->notify(new SendMessageNotification(
                                'Incident',
                                "Incident déclarer sur lors du trajet N°".$planning->number_travel,
                                $incident->niveau == 1 ? "info" : ($incident->niveau == 2 ? "warning" : "danger"),
                                $incident->niveau == 1 ? asset('/storage/icons/warning_low.png') : ($incident->niveau == 2 ? asset('/storage/icons/warning_middle.png') : asset('/storage/icons/warning_high.png')),                                route('travel.show', $planning->id),
                                "incident"
                            ));

                            return null;
                        }
                        if ($faker->boolean(60)) {
                            $type_incidents = TypeIncident::where('severity', 2)->get()->random();
                            $attemps = rand(2,15);
                            $amount = UserIncident::getAmountReparationFromTypeIncident($planning->ligne, $type_incidents);

                            $incident = $planning->incidents()->create([
                                "niveau" => $type_incidents->severity,
                                "status" => "open",
                                "designation" => UserIncident::getDesignationFromTypeIncident($type_incidents->designation),
                                "amount_reparation" => $amount,
                                "user_id" => $planning->user_id,
                                "user_planning_id" => $planning->id,
                                "type_incident_id" => $type_incidents->id,
                                "engine_composition_id" => UserIncident::getCompositionFromTypeIncident($planning->engine, $type_incidents),
                                "user_engine_id" => $planning->user_engine_id,
                                "user_hub_id" => $planning->user_hub_id,
                            ]);

                            $planning->retarded_time += $attemps;
                            $planning->save();

                            $planning->date_arrived = $planning->date_arrived->addMinutes($attemps);
                            $planning->save();

                            $planning->engine->use_percent += $planning->engine->engine->calcVitesseUsure() / 2;
                            $planning->engine->save();

                            foreach ($planning->planning_stations()->where('status', '!=', 'done')->get() as $station) {
                                $station->update([
                                    "arrival_at" => $station->arrival_at->addMinutes($attemps),
                                    "departure_at" => $station->departure_at->addMinutes($attemps),
                                ]);
                            }

                            Mouvement::adding(
                                'charge',
                                $incident->niveau_label." sur le trajet N°".$planning->number_travel,
                                "cout_trajet",
                                $amount,
                                $planning->user->company->id,
                                $planning->hub->id,
                                $planning->ligne->id
                            );

                            $planning->user->notify(new SendMessageNotification(
                                'Incident',
                                "Incident déclarer sur lors du trajet N°".$planning->number_travel,
                                $incident->niveau == 1 ? "info" : ($incident->niveau == 2 ? "warning" : "danger"),
                                $incident->niveau == 1 ? asset('/storage/icons/warning_low.png') : ($incident->niveau == 2 ? asset('/storage/icons/warning_middle.png') : asset('/storage/icons/warning_high.png')),                                route('travel.show', $planning->id),
                                "incident"
                            ));

                            return null;
                        }
                        if ($faker->boolean(80)) {
                            $type_incidents = TypeIncident::where('severity', 1)->get()->random();

                            $incident = $planning->incidents()->create([
                                "niveau" => $type_incidents->severity,
                                "status" => "open",
                                "designation" => UserIncident::getDesignationFromTypeIncident($type_incidents->designation),
                                "amount_reparation" => UserIncident::getAmountReparationFromTypeIncident($planning->ligne, $type_incidents),
                                "user_id" => $planning->user_id,
                                "user_planning_id" => $planning->id,
                                "type_incident_id" => $type_incidents->id,
                                "engine_composition_id" => UserIncident::getCompositionFromTypeIncident($planning->engine, $type_incidents),
                                "user_engine_id" => $planning->user_engine_id,
                                "user_hub_id" => $planning->user_hub_id,
                            ]);

                            $planning->user->notify(new SendMessageNotification(
                                'Incident',
                                "Incident déclarer sur lors du trajet N°".$planning->number_travel,
                                $incident->niveau == 1 ? "info" : ($incident->niveau == 2 ? "warning" : "danger"),
                                $incident->niveau == 1 ? asset('/storage/icons/warning_low.png') : ($incident->niveau == 2 ? asset('/storage/icons/warning_middle.png') : asset('/storage/icons/warning_high.png')),                                route('travel.show', $planning->id),
                                "incident"
                            ));

                            return null;
                        }
                    }
                }
            }
        }catch (Exception $exception) {
            \Log::error($exception);
        }
    }

}
