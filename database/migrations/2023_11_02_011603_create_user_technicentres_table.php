<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTechnicentresTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_technicentres', function (Blueprint $table) {
            $table->id();
            $table->integer('level')->default(1);
            $table->integer('limit_engine')->default(2);
            $table->integer('limit_perso')->default(6);

            $table->foreignId('user_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_hub_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_technicentres');
    }
}
