<?php

namespace App\Http\Controllers\Api\Game\Maintenance;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\User\UserEngine;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function wear(Request $request)
    {
        $user = User::find($request->get('user_id'));

        $querys = $user->engines()
            ->with('incidents')
            ->whereBetween('use_percent', [$request->get('wear'), 100])
            ->whereBetween('ancien', [$request->get('mark'), 5])
            ->get();

        $engines = [];

        foreach ($querys as $item) {
            $engines[] = [
                "engine" => $item,
                "usure" => $item->use_percent ."%",
                "mark" => $item->ancien." / 5",
                "nb_rec_incident" => $item->incidents()->whereBetween('created_at',[now()->startOfDay(),now()->subDays(7)->endOfDay()])->get()->count(),
                "coast_incident" => eur($item->incidents()->whereBetween('created_at',[now()->startOfDay(), now()->subDays(7)->endOfDay()])->sum('amount_reparation')),
                "coast_maint_prev" => floatval($item->engine->price_maintenance),
                "coast_maint_cur" => $item->engine->price_maintenance * ($item->engine->compositions()->sum('maintenance_repar_time') / 60 / 10),
            ];
        }

        return response()->json([
            "count" => count($engines),
            "engines" => $engines,
        ]);
    }

    public function store(Request $request)
    {
        $engine = UserEngine::findOrFail($request->get('engine_id'));
        if($engine->in_maintenance) {
            return response()->json(["message" => "Matériel déjà en maintenance"], 500);
        }

        try {
            $engine->createMaintenance($request->get('type'));
            return response()->json(null, 200);
        }catch (\Exception $exception) {
            \Log::error($exception->getMessage(), [$exception]);
            return response()->json(["message" => "Erreur lors de la création de la maintenance"], 500);
        }
    }
}
