<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic
Product Version: 8.2.0
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="{{ config('app.locale') }}">
<!--begin::Head-->
<head><base href=""/>
    <title>VIGIMAT - {{ config('app.name') }}</title>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="/assets/media/logos/favicon.ico" />
    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <!--end::Fonts-->
    @yield("css")
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="https://api.mapbox.com/mapbox-gl-js/v2.14.1/mapbox-gl.css" rel="stylesheet">
    @vite(["resources/css/app.css", "resources/sass/custom.scss"])
    <!--end::Global Stylesheets Bundle-->
    <script>// Frame-busting to prevent site from being loaded within a frame without permission (click-jacking) if (window.top != window.self) { window.top.location.replace(window.self.location.href); }</script>
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" data-kt-app-header-stacked="true" data-kt-app-header-primary-enabled="true" data-kt-app-header-secondary-enabled="false" class="app-default">
<!--begin::Theme mode setup on page load-->
<script>var defaultThemeMode = "light";
    var themeMode;
    if (document.documentElement) {
        if (document.documentElement.hasAttribute("data-bs-theme-mode")) {
            themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
        } else {
            if (localStorage.getItem("data-bs-theme") !== null) {
                themeMode = localStorage.getItem("data-bs-theme");
            } else {
                themeMode = defaultThemeMode;
            }
        }
        if (themeMode === "system") {
            themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
        }
        document.documentElement.setAttribute("data-bs-theme", themeMode);
    }</script>
<!--end::Theme mode setup on page load-->
<!--begin::App-->
<div class="d-flex flex-column flex-root app-root" id="kt_app_root">
    <!--begin::Page-->
    <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
        <!--begin::Header-->
        <div id="kt_app_header" class="app-header">
            <!--begin::Header primary-->
            <div class="app-header-primary" data-kt-sticky="true" data-kt-sticky-name="app-header-primary-sticky" data-kt-sticky-offset="{default: 'false', lg: '300px'}">
                <!--begin::Header primary container-->
                <div class="app-container container-xxl d-flex align-items-stretch justify-content-between">
                    <!--begin::Logo and search-->
                    <div class="d-flex flex-grow-1 flex-lg-grow-0">
                        <!--begin::Logo wrapper-->
                        <div class="d-flex align-items-center me-7" id="kt_app_header_logo_wrapper">
                            <!--begin::Header toggle-->
                            <button class="d-lg-none btn btn-icon btn-flex btn-color-gray-600 btn-active-color-primary w-35px h-35px ms-n2 me-2" id="kt_app_header_menu_toggle">
                                <i class="ki-outline ki-abstract-14 fs-2"></i>
                            </button>
                            <!--end::Header toggle-->
                            <!--begin::Logo-->
                            <a href="{{ route('vigimat.dashboard') }}" class="d-flex align-items-center me-lg-20 me-5">
                                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}" class="h-20px d-sm-none d-inline" />
                                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}" class="h-20px h-lg-25px theme-light-show d-none d-sm-inline" />
                                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}" class="h-20px h-lg-25px theme-dark-show d-none d-sm-inline" />
                                <span class="ms-3 fw-bolder fs-3 text-color-ic">VIGIMAT</span>
                            </a>
                            <!--end::Logo-->
                        </div>
                        <!--end::Logo wrapper-->
                        <!--begin::Menu wrapper-->
                        <div class="app-header-menu app-header-mobile-drawer align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="app-header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="250px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_header_menu_toggle" data-kt-swapper="true" data-kt-swapper-mode="{default: 'append', lg: 'prepend'}" data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}">
                            <!--begin::Menu-->
                            <div class="menu menu-rounded menu-active-bg menu-state-primary menu-column menu-lg-row menu-title-gray-700 menu-icon-gray-500 menu-arrow-gray-500 menu-bullet-gray-500 my-5 my-lg-0 align-items-stretch fw-semibold px-2 px-lg-0" id="kt_app_header_menu" data-kt-menu="true">
                                <div class="menu-item me-10">
                                    <a href="{{ route('vigimat.dashboard') }}" class="menu-link {{ request()->routeIs('vigimat.dashboard') ? 'active' : '' }}">
                                        <span class="menu-link py-3">
											<span class="menu-title">Acceuil</span>
											<span class="menu-arrow d-lg-none"></span>
										</span>
                                    </a>
                                </div>
                                <div class="menu-item me-10">
                                    <a href="{{ route('vigimat.engine') }}" class="menu-link {{ request()->routeIs('vigimat.engine*') ? 'active' : '' }}">
                                        <span class="menu-link py-3">
											<span class="menu-title">Matériels Roulants</span>
											<span class="menu-arrow d-lg-none"></span>
										</span>
                                    </a>
                                </div>
                                <div class="menu-item me-10">
                                    <a href="{{ route('vigimat.dashboard') }}" class="menu-link {{ Route::is(["vigimat.dashboard"]) ? 'active' : '' }}">
                                        <span class="menu-link py-3">
											<span class="menu-title">Personnels</span>
											<span class="menu-arrow d-lg-none"></span>
										</span>
                                    </a>
                                </div>
                                <div class="menu-item me-10">
                                    <a href="{{ route('vigimat.dashboard') }}" class="menu-link {{ Route::is(["vigimat.dashboard"]) ? 'active' : '' }}">
                                        <span class="menu-link py-3">
											<span class="menu-title">Cartographie</span>
											<span class="menu-arrow d-lg-none"></span>
										</span>
                                    </a>
                                </div>
                            </div>
                            <!--end::Menu-->
                        </div>
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Logo and search-->

                    <!--begin::Navbar-->
                    <div class="app-navbar flex-shrink-0">
                        <div class="vr me-3"></div>
                        <div class="app-navbar-item me-3">
                            <span class="fw-bold fs-2 me-3">Technicentre:</span>
                            <span class="fs-2" data-vue="technicentre_name">Aucun</span>
                        </div>
                        <div class="vr"></div>
                        <!--begin::Notifications-->
                        <div class="app-navbar-item">
                            <!--begin::Menu- wrapper-->
                            <div class="btn btn-icon btn-icon-gray-600 btn-active-color-primary" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-attach="parent" data-kt-menu-placement="bottom">
                                <i class="fa-solid fa-circle-exclamation fs-1"></i>
                            </div>
                            <!--begin::Menu-->
                            <div class="menu menu-sub menu-sub-dropdown menu-column w-350px w-lg-375px" data-kt-menu="true" id="kt_menu_notifications">
                                <!--begin::Heading-->
                                <div class="d-flex flex-column bgi-no-repeat rounded-top" style="background-image:url('/assets/media/misc/menu-header-bg.jpg')">
                                    <!--begin::Title-->
                                    <h3 class="text-white fw-semibold px-9 mt-10 mb-6">Notifications
                                        <span class="fs-8 opacity-75 ps-3">{{ auth()->user()->unreadNotifications()->count() }} {{ Str::plural('nouvelle', auth()->user()->unreadNotifications()->count()) }} {{ Str::plural('notification', auth()->user()->unreadNotifications()->count()) }}</span></h3>
                                    <!--end::Title-->
                                </div>
                                <!--end::Heading-->
                                <!--begin::Items-->
                                <div class="scroll-y mh-325px my-5 px-8">
                                    <!--begin::Item-->
                                    @if(auth()->user()->unreadNotifications()->count() == 0)
                                        <div class="d-flex flex-row justify-content-center py-4">
                                            <i class="fa-solid fa-triangle-exclamation fs-2 text-warning me-4"></i>
                                            <span class="fs-6 fw-bold text-gray-800">Aucune notification</span>
                                        </div>
                                    @else
                                        @foreach(auth()->user()->unreadNotifications as $notification)
                                            @if($notification->data['sector'] == 'engine')
                                                <div class="d-flex flex-stack py-4">
                                                    <!--begin::Section-->
                                                    <div class="d-flex align-items-center">
                                                        <!--begin::Symbol-->
                                                        <div class="symbol symbol-35px me-4">
                                                            <img src="{{ $notification->data['icon'] }}" alt="notifImage" class="w-35px" />
                                                        </div>
                                                        <!--end::Symbol-->
                                                        <!--begin::Title-->
                                                        <div class="mb-0 me-2">
                                                            <a href="#" class="fs-6 text-gray-800 text-hover-primary fw-bold">{{ $notification->data['title'] }}</a>
                                                            <div class="text-gray-400 fs-7">{{ $notification->data['description'] }}</div>
                                                        </div>
                                                        <!--end::Title-->
                                                    </div>
                                                    <!--end::Section-->
                                                    <!--begin::Label-->
                                                    <span class="badge badge-light fs-8" data-ago="{{ strtotime($notification->data['time']) }}">-</span>
                                                    <!--end::Label-->
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif

                                    <!--end::Item-->
                                </div>
                                <!--end::Items-->
                                <!--begin::View more-->
                                <div class="py-3 text-center border-top">
                                    <a href="../../demo35/dist/pages/user-profile/activity.html" class="btn btn-color-gray-600 btn-active-color-primary">View All
                                        <i class="ki-outline ki-arrow-right fs-5"></i></a>
                                </div>
                                <!--end::View more-->
                            </div>
                            <!--end::Menu-->
                            <!--end::Menu wrapper-->
                        </div>
                        <!--end::Notifications-->
                        <div class="app-navbar-item">
                            <a href="{{ route('vigimat.quit') }}" class="btn btn btn-icon-gray-600">
                                <i class="fa-solid fa-right-from-bracket fs-1"></i>
                            </a>
                        </div>
                        <!--begin::Header menu toggle-->
                        <div class="app-navbar-item d-lg-none ms-2 me-n3" title="Show header menu">
                            <div class="btn btn-icon btn-color-gray-500 btn-active-color-primary w-35px h-35px" id="kt_app_header_menu_toggle">
                                <i class="ki-outline ki-text-align-left fs-1"></i>
                            </div>
                        </div>
                        <!--end::Header menu toggle-->
                    </div>
                    <!--end::Navbar-->
                </div>
                <!--end::Header primary container-->
            </div>
            <!--end::Header primary-->
        </div>
        <!--end::Header-->
        <!--begin::Wrapper-->
        <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
            <!--begin::Wrapper container-->
            <div class="app-container container-xxl d-flex flex-row flex-column-fluid">
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content flex-column-fluid">
                            @yield("content")
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <div id="kt_app_footer" class="app-footer d-flex flex-column flex-md-row align-items-center flex-center flex-md-stack py-2 py-lg-4">
                        <!--begin::Copyright-->
                        <div class="text-dark order-2 order-md-1">
                            <span class="text-muted fw-semibold me-1">2023&copy;</span>
                            <a href="https://railway-manager.ovh" target="_blank" class="text-gray-800 text-hover-primary">Railway Manager</a> by <a href="https://vortechstudio.fr" target="_blank" class="text-gray-800 text-hover-primary">Vortech Studio</a>
                        </div>
                        <!--end::Copyright-->
                        <!--begin::Menu-->
                        <ul class="menu menu-gray-600 menu-hover-primary fw-semibold order-1">
                            <li class="menu-item">
                                <a href="https://patch.railway-manager.ovh" target="_blank" class="menu-link px-2">Changelog</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://vortechstudio.atlassian.net/servicedesk/customer/portal/3" target="_blank" class="menu-link px-2">Support</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://starter.productboard.com/vortechstudio-starter/1-railway-manager" target="_blank" class="menu-link px-2">Roadmap</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://status.vortechstudio.fr" target="_blank" class="menu-link px-2">Status</a>
                            </li>
                        </ul>
                        <!--end::Menu-->
                    </div>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper container-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
<!--end::App-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <i class="ki-outline ki-arrow-up"></i>
</div>
<!--end::Scrolltop-->
<!--begin::Javascript-->
<script>var hostUrl = "assets/";</script>
<!--begin::Global Javascript Bundle(mandatory for all pages)-->
<script src="/assets/plugins/global/plugins.bundle.js"></script>
<script src="/assets/js/scripts.bundle.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/v2.14.1/mapbox-gl.js"></script>
<script src="{{ asset('/assets/js/map.js') }}"></script>
<script src="https://cdn.socket.io/4.0.1/socket.io.min.js"></script>

@vite(['resources/js/sensor.js'])
<!--end::Global Javascript Bundle-->
@yield("script")
<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        document.querySelector('[data-vue="technicentre_name"]').innerHTML = '{{ Session::has('technicentre') ? Session::get('technicentre')[0]['name'] : 'Aucun' }}';

        @if(!Session::has('technicentre'))
        Swal.fire({
            title: 'Aucun technicentre désigné',
            icon: 'question',
            input: "select",
            inputOptions: {
                @foreach(auth()->user()->technicentres as $technicentre)
        {{ $technicentre->id }}: "{{ $technicentre->hub->hub->gare->name }}",
                @endforeach
            },
            showCancelButton: true,
            confirmButtonText: "Valider",
            showLoaderOnConfirm: true,
            preConfirm: async (technicentre_id) => {
                const response = await fetch('/api/vigimat/technicentre/'+ technicentre_id)
                if(!response.ok) {
                    return Swal.showValidationMessage(`${JSON.stringify(await response.json())}`);
                }
                return response.json()
            },
            allowOutsideClick: () => !Swal.isLoading(),
        }).then((result) => {
            if(result.isConfirmed) {
                window.location.reload()
            }
        })
        @endif
    })


</script>
</body>
<!--end::Body-->
</html>
