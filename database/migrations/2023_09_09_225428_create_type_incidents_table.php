<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('type_incidents', function (Blueprint $table) {
            $table->id();
            $table->string('designation')->comment("désignation de l'incident (ex: panne de courant à bord du train)");
            $table->enum('origine', [
                "infrastructures",
                "materiels",
                "humains",
            ])->comment("origine de l'incident");
            $table->integer('severity')->comment("gravité de l'incident allant de 1 à 3 (3 étant le plus grave et 1 le moins grave)");
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('type_incidents');
    }
};
