<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Core\Mouvement;
use App\Models\User;
use App\Models\User\UserCompany;
use App\Models\User\UserCompanySituation;
use App\Services\Jira\JiraService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::select('*');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="' . route('admin.user.show', $row->id) . '" class="btn btn-primary btn-sm">Voir</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.user.index');
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(Request $request)
    {
        $password = generatePassword(8);
        $faker = \Faker\Factory::create("fr_FR");

        $validation = \Validator::validate($request->all(), [
            "name" => "required|unique:users,name",
            "email" => "required|email|unique:users,email",
            "avatar" => "image",
            "name_company" => "required|unique:users,name_company",
            "name_secretary" => "required|unique:users,name_secretary",
            "logo_company" => "image",
            "avatar_secretary" => "image",
        ]);

        $user = \App\Models\User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => \Hash::make($password),
            "admin" => $request->has('admin'),
            "level_id" => $request->level_id ?? 1,
            "name_secretary" => $request->name_secretary,
            "name_company" => $request->name_company,
            "desc_company" => $request->desc_company ?? null,
            "premium" => $request->has('premium'),
            "tpoint" => $request->tpoint ?? 0,
            "research" => $request->research ?? 0,
            'name_conseiller' => $faker->name("Male"),
        ]);

        $company = UserCompany::automatedCreate($user);
        UserCompanySituation::automatedCreate($company);

        Mouvement::adding(
            'revenue',
            'Création de l\'entreprise - Reversion du capital',
            'divers',
            $request->argent ?? 0,
            $company->id
        );

        $customer_jira = JiraService::createCustomer($user->name, $user->email);
        app('freemobile')->send("Le compte JIRA pour l'utilisateur: {$user->name} a été créé avec succès, veuillez l'activé: ".$customer_jira["_links"]["self"]);


        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $avatarName = $user->id . '.' . $avatar->getClientOriginalExtension();
            $avatar->storeAs('users', $avatarName, 'avatar');
            $user->update(['avatar' => $avatarName]);
        }

        if ($request->hasFile('logo_company')) {
            $logo = $request->file('logo_company');
            $logoName = $user->id . '.' . $logo->getClientOriginalExtension();
            $logo->storeAs('company', $logoName, 'avatar');
            $user->update(['logo_company' => $logoName]);
        }

        if ($request->hasFile('avatar_secretary')) {
            $avatar = $request->file('avatar_secretary');
            $avatarName = $user->id . '.' . $avatar->getClientOriginalExtension();
            $avatar->storeAs('secretary', $avatarName, 'avatar');
            $user->update(['avatar_secretary' => $avatarName]);
        }

        $user->card_bonus()->create([
            'sim_offer' => 0,
            'audit_ext_offer' => 0,
            "audit_int_offer" => 0,
            "user_id" => $user->id,
        ]);

        event(new Registered($user));
        dispatch(new \App\Jobs\Admin\SendPasswordToUserJob($user, $password));

        toast()->success('L\'utilisateur a bien été créé');
        toast()->info("Le mot de passe temporaire du joueur à été envoyer par mail.");
        toast()->info("Un email de bienvenue lui à été envoyer");

        return redirect()->route('admin.user.index');
    }

    public function show($id, Request $request)
    {
        $user = \App\Models\User::with('social', 'company', 'hubs', 'engines', 'level', 'achievements')
            ->findOrFail($id);

        if ($request->ajax()) {
            $mouvement = $user->company->mouvement->sortByDesc('created_at');
            try {
                return DataTables::of($mouvement)
                    ->addIndexColumn()
                    ->make(true);
            } catch (\Exception $e) {
                return $e->getMessage();
            }

            $hubs = $user->hubs->with('hub')->sortByDesc('date_achat');
            dd($hubs->toArray());
            try {
                return DataTables::of($hubs)
                    ->addIndexColumn()
                    ->make(true);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        $mouvements = $user->company->mouvement->sortByDesc('created_at');


        return view('admin.user.show', compact('user', 'mouvements'));
    }
}
