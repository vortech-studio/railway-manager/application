<?php

namespace App\Traits;

trait SubventionTrait
{
    public static function getSubvention($ca_company): int
    {
        return match ($ca_company) {
            $ca_company <= 12000 => 10,
            $ca_company > 12000 && $ca_company <= 29000 => 23,
            $ca_company > 29000 && $ca_company <= 42000 => 32,
            $ca_company > 42000 && $ca_company <= 60000 => 41,
            $ca_company > 60000 && $ca_company <= 79000 => 56,
            $ca_company > 79000 && $ca_company <= 90000 => 68,
            $ca_company > 90000 && $ca_company <= 100000 => 72,
            $ca_company > 100000 => 80,
            default => 15,
        };
    }
}
