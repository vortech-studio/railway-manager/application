<?php

namespace App\Services\Mapping;

use Http;

class Mapbox
{
    public string $apiKey;
    public function __construct()
    {
        $this->apiKey = config('services.mapbox.api_key');
    }

    public function geocodeSearch(object $gare = null, string $nameGare = null)
    {
        if(isset($gare)) {
            $request = Http::get('https://api.mapbox.com/geocoding/v5/mapbox.places/Gare '.formatTextForSlugify($gare->gare_alias_libelle_noncontraint).'.json', [
                "language" => "fr",
                "access_token" => $this->apiKey,
                "types" => "poi"
            ]);
        } else {
            $request = Http::get('https://api.mapbox.com/geocoding/v5/mapbox.places/Gare '.formatTextForSlugify($nameGare).'.json', [
                "language" => "fr",
                "access_token" => $this->apiKey,
                "types" => "poi"
            ]);
        }

        if($request->successful()) {
            dd($request->object());
            return collect([
                'lat' => $request->object()->features[0]->center[0],
                'lng' => $request->object()->features[0]->center[1],
            ]);
        } else {
            return $request->serverError();
        }
    }

    public function geocodeMatchInfo(object $gare = null, string $nameGare = null)
    {
        if(isset($gare)) {
            $request = Http::get('https://api.mapbox.com/geocoding/v5/mapbox.places/Gare '.$gare->gare_alias_libelle_noncontraint.'.json', [
                "language" => "fr",
                "access_token" => $this->apiKey,
                "types" => "poi"
            ]);
        } else {
            $request = Http::get('https://api.mapbox.com/geocoding/v5/mapbox.places/Gare '.$nameGare.'.json', [
                "language" => "fr",
                "access_token" => $this->apiKey,
                "types" => "poi"
            ]);
        }


        if($request->successful()) {
            return collect([
                "region" => $request->object()->features[0]->context[3]->text_fr,
                "pays" => $request->object()->features[0]->context[4]->text_fr
            ]);
        } else {
            return $request->serverError();
        }
    }

    public function getMatrixInfo(string $point_start, string $point_end)
    {
        $request = Http::get('https://api.mapbox.com/directions-matrix/v1/mapbox/driving/'.$point_start.';'.$point_end.'?sources=1&annotations=distance,duration&fallback_speed=160&access_token='.$this->apiKey);

        if($request->successful()) {
            return [
                "distance" => $request->object()->distances[0][0] / 1000,
            ];
        }
    }
}
