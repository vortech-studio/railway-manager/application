<?php


use Illuminate\Support\Facades\Route;

Route::middleware(['web', 'verified'])->group(function () {
    Route::get('/', \App\Http\Controllers\Game\DashboardController::class)->name('home');

    Route::prefix('account')->group(function () {
        Route::get('/', [\App\Http\Controllers\Game\AccountController::class, 'index'])->name('account.index');
        Route::get('/objectif', [\App\Http\Controllers\Game\AccountController::class, 'objectif'])->name('account.objectif');
        Route::get('/premium', [\App\Http\Controllers\Game\AccountController::class, 'premium'])->name('account.premium');
        Route::post('/premium/subscribe', [\App\Http\Controllers\Game\AccountController::class, 'subscribePremium'])->name('account.premium.subscribe');

        Route::get('/notifications', [\App\Http\Controllers\Game\AccountController::class, 'notifications'])->name('account.notifications');

        Route::prefix('mailbox')->group(function () {
            Route::get('/', [\App\Http\Controllers\Game\MailboxController::class, 'index'])->name('mailbox.index');
            Route::get('{message_id}', [\App\Http\Controllers\Game\MailboxController::class, 'show'])->name('mailbox.show');
        });

        Route::prefix('shop')->group(function () {
            Route::get('/', [\App\Http\Controllers\Game\ShopController::class, 'index'])->name('shop.index');
            Route::post('/checkout', [\App\Http\Controllers\Game\ShopController::class, 'checkout'])->name('shop.checkout');
            Route::get('/checkout/confirm', [\App\Http\Controllers\Game\ShopController::class, 'checkoutConfirm'])->name('shop.checkout.confirm');
        });

        Route::prefix('cards')->group(function () {
            Route::get('/{uuid}', [\App\Http\Controllers\Game\CardController::class, 'show'])->name('card.show');
            Route::post('/{uuid}', [\App\Http\Controllers\Game\CardController::class, 'receive'])->name('card.receive');
        });
    });

    Route::prefix("network")->group(function () {
        Route::get('/', \App\Http\Controllers\Game\Network\NetworkController::class)->name('network');

        Route::prefix('hub')->group(function () {
            Route::get('/checkout', [\App\Http\Controllers\Game\Network\HubController::class, 'checkout'])->name('hub.checkout');
            Route::post('/checkout', [\App\Http\Controllers\Game\Network\HubController::class, 'checkoutConfirm'])->name('hub.checkout.confirm');
            Route::get('{id}', [\App\Http\Controllers\Game\Network\HubController::class, 'show'])->name('hub.show');
        });

        Route::prefix('line')->group(function () {
            Route::get('/checkout', [\App\Http\Controllers\Game\Network\LigneController::class, 'checkout'])->name('ligne.checkout');
            Route::post('/checkout', [\App\Http\Controllers\Game\Network\LigneController::class, 'checkoutConfirm'])->name('ligne.checkout.confirm');
            Route::get('{id}', [\App\Http\Controllers\Game\Network\LigneController::class, 'show'])->name('ligne.show');
        });

        Route::prefix('planning')->group(function () {
            Route::get('/', [\App\Http\Controllers\Game\Network\PlanningController::class, 'index'])->name('planning.index');
            Route::get('/editing', [\App\Http\Controllers\Game\Network\PlanningController::class, 'editing'])->name('planning.editing');
            Route::post('/editing', [\App\Http\Controllers\Game\Network\PlanningController::class, 'store'])->name('planning.store');
        });

        Route::prefix('travel')->group(function () {
            Route::get('/{id}', [\App\Http\Controllers\Game\Network\TravelController::class, 'show'])->name('travel.show');
        });
    });

    Route::prefix('engine')->group(function () {
        Route::get('/', [\App\Http\Controllers\Game\Engine\EngineController::class, 'index'])->name('engine.index');

        Route::prefix('/buy')->group(function () {
            Route::get('/', [\App\Http\Controllers\Game\Engine\BuyController::class, 'index'])->name('engine.buy.index');

            Route::prefix('checkout')->group(function () {
                Route::get('/', [\App\Http\Controllers\Game\Engine\BuyController::class, 'checkout'])->name('engine.buy.checkout');
                Route::get('/config', [\App\Http\Controllers\Game\Engine\BuyController::class, 'config'])->name('engine.buy.config');
                Route::post('/', [\App\Http\Controllers\Game\Engine\BuyController::class, 'checkoutConfirm'])->name('engine.buy.checkout.confirm');
            });

            Route::prefix('rental')->group(function () {
                Route::get('/', [\App\Http\Controllers\Game\Engine\RentalController::class, 'index'])->name('engine.rental.index');
                Route::get('/engine', [\App\Http\Controllers\Game\Engine\RentalController::class, 'engine'])->name('engine.rental.engine');
                Route::get('/config', [\App\Http\Controllers\Game\Engine\RentalController::class, 'config'])->name('engine.rental.config');
                Route::post('/', [\App\Http\Controllers\Game\Engine\RentalController::class, 'checkout'])->name('engine.rental.checkout');
            });
        });
        Route::prefix('/maintenance')->group(function () {
            Route::get('/book', [\App\Http\Controllers\Game\Engine\MaintenanceController::class, 'book'])->name('engine.maintenance.book');
            Route::get('/history', [\App\Http\Controllers\Game\Engine\MaintenanceController::class, 'history'])->name('engine.maintenance.history');
            Route::get('/group', [\App\Http\Controllers\Game\Engine\MaintenanceController::class, 'group'])->name('engine.maintenance.group');
            Route::post('/group', [\App\Http\Controllers\Game\Engine\MaintenanceController::class, 'confirmMaintenance'])->name('engine.maintenance.group.confirmMaintenance');
            Route::post('/auto', [\App\Http\Controllers\Game\Engine\MaintenanceController::class, 'autoMaintenance'])->name('engine.maintenance.group.autoMaintenance');
        });

        Route::get('{user_engine_id}', [\App\Http\Controllers\Game\Engine\EngineController::class, 'show'])->name('engine.show');


    });

    Route::prefix('compagnie')->group(function () {
        Route::get('/', \App\Http\Controllers\Game\Compagnie\CompagnieController::class)->name('compagnie.index');

        Route::prefix('bonus')->group(function () {
            Route::get('/', [\App\Http\Controllers\Game\Compagnie\BonusController::class, 'index'])->name('compagnie.bonus.index');
        });
        Route::prefix('customise')->group(function () {
            Route::get('/', [\App\Http\Controllers\Game\Compagnie\CustomiseController::class, 'index'])->name('compagnie.customise.index');
            Route::put('/', [\App\Http\Controllers\Game\Compagnie\CustomiseController::class, 'update'])->name('compagnie.customise.update');
        });
        Route::prefix('profil')->group(function() {
            Route::get('/', [\App\Http\Controllers\Game\Compagnie\ProfilController::class, 'index'])->name('compagnie.profil.index');
        });
    });

    Route::prefix('research')->group(function () {
        Route::get('/', [\App\Http\Controllers\Game\Research\ResearchController::class, 'index'])->name('research.index');
    });

    Route::prefix('chat')->group(function () {
        Route::get('/secretary', [\App\Http\Controllers\Game\Chat\ChatController::class, 'secretary'])->name('chat.secretary');
        Route::get('/conseiller', [\App\Http\Controllers\Game\Chat\ChatController::class, 'conseiller'])->name('chat.conseiller');
        Route::get('/hub/{id}/technicentre/responsable', [\App\Http\Controllers\Game\Chat\ChatController::class, 'responsableTechni'])->name('chat.hub.technicentre.responsable');
    });

    include("vigimat.php");
});
