@extends("admin.template")

@section("css")

@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Matériel Roulant: ".$engine->name,
        'action' => true,
        'data' => [
            [
                'url' => back()->getTargetUrl(),
                'title' => "<i class='la la-arrow-left me-2'></i> Retour",
                'class' => "secondary"
            ],
            [
                'url' => route('admin.engine.edit', $engine->id),
                'title' => "<i class='la la-edit me-2'></i> Modifier",
                'class' => "primary"
            ],
            [
                'url' => route('admin.engine.delete', $engine->id),
                'title' => "<i class='la la-trash me-2'></i> Supprimer",
                'class' => "danger delete",
            ]
        ]
    ])
@endsection

@section("content")
    @if($engine->type_engine == 'automotrice')
        <div class="d-flex flex-row w-100 justify-content-center align-items-baseline bg-white shadow-lg mb-10 p-10">
            <div class="p-auto">
                @for($i=0; $i < $engine->technical->nb_wagon; $i++)
                    <img src="/storage/engines/{{ $engine->type_engine }}/{{ \Illuminate\Support\Str::upper(Str::slug($engine->name)) }}-{{ $i }}.gif" class="my-5" alt="">
                @endfor
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                @if($engine->type_engine != 'automotrice')
                    <img src="/storage/engines/{{ $engine->type_engine }}/{{ $engine->image }}" class="w-135px my-5 mx-20" alt="">
                @endif
                <div class="card-body pt-5 scroll h-400px">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row">
                            <div class="symbol symbol-50px me-5 bg-light">
                                <img src="/storage/icons/ticket.png" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <span class="fw-bold fs-6">Prix d'achat</span>
                                <span class="fw-bold fs-5">{{ eur($engine->price_achat) }}</span>
                            </div>
                        </div>
                        <div class="separator separator-dashed my-5 border border-1 border-gray-400"></div>
                        <div class="d-flex flex-row">
                            <div class="symbol symbol-50px me-5 bg-light">
                                <img src="/storage/icons/maintenance.png" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <span class="fw-bold fs-6">Prix de la maintenance</span>
                                <span class="fw-bold fs-5">{{ eur($engine->price_maintenance) }} / <span class="fs-9">par jours</span></span>
                            </div>
                        </div>
                        <div class="separator separator-dashed my-5 border border-1 border-gray-400"></div>
                        <div class="d-flex flex-row">
                            <div class="symbol symbol-50px me-5 bg-light">
                                <img src="/storage/icons/bus.png" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <span class="fw-bold fs-6">Prix de la location</span>
                                <span class="fw-bold fs-5">{{ eur($engine->price_location) }} / <span class="fs-9">par jours</span></span>
                            </div>
                        </div>
                        <x-base.underline
                            title="Information technique"
                            size-text="fs-5" size="1" class="my-5"/>
                        <div class="d-flex flex-column">
                            <div class="d-flex flex-row">
                                <div class="symbol symbol-50px me-5 bg-light">
                                    <div class="symbol-label"><i class="fa-solid fa-train text-black fs-2tx"></i> </div>
                                </div>
                                <div class="d-flex flex-column">
                                    <span class="fw-bold fs-8">Type de Train</span>
                                    <span class="fw-bold fs-5">{{ $engine->type_train }}</span>
                                </div>
                            </div>
                            <div class="separator separator-dashed my-5 border border-1 border-gray-400"></div>
                            <div class="d-flex flex-row">
                                <div class="symbol symbol-50px me-5 bg-light">
                                    <img src="/storage/icons/train_engine.png" alt="">
                                </div>
                                <div class="d-flex flex-column">
                                    <span class="fw-bold fs-8">Construction du train</span>
                                    <span class="fw-bold fs-5">{{ $engine->type_engine }}</span>
                                </div>
                            </div>
                            <div class="separator separator-dashed my-5 border border-1 border-gray-400"></div>
                            <div class="d-flex flex-row">
                                <div class="symbol symbol-50px me-5 bg-light">
                                    <img src="/storage/icons/energy-system.png" alt="">
                                </div>
                                <div class="d-flex flex-column">
                                    <span class="fw-bold fs-8">Type de motorisation</span>
                                    <span class="fw-bold fs-5">{{ $engine->type_energy }}</span>
                                </div>
                            </div>
                            <div class="separator separator-dashed my-5 border border-1 border-gray-400"></div>
                            <div class="d-flex flex-row">
                                <div class="symbol symbol-50px me-5 bg-light">
                                    <img src="/storage/icons/speedometer.png" alt="">
                                </div>
                                <div class="d-flex flex-column">
                                    <span class="fw-bold fs-8">Vitesse Max</span>
                                    <span class="fw-bold fs-5">{{ $engine->velocity }} Kmh</span>
                                </div>
                            </div>
                            @if(isset($engine->nb_passager))
                                <div class="separator separator-dashed my-5 border border-1 border-gray-400"></div>
                                <div class="d-flex flex-row">
                                    <div class="symbol symbol-50px me-5 bg-light">
                                        <img src="/storage/icons/train-passenger.png" alt="">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <span class="fw-bold fs-8">Nombre de passager</span>
                                        <span class="fw-bold fs-5">{{ $engine->nb_passager }}</span>
                                    </div>
                                </div>
                            @endif
                            <div class="separator separator-dashed my-5 border border-1 border-gray-400"></div>
                            <div class="d-flex flex-row">
                                <div class="symbol symbol-50px me-5 bg-light">
                                    <img src="/storage/icons/maintenance.png" alt="">
                                </div>
                                <div class="d-flex flex-column">
                                    <span class="fw-bold fs-8">Durée de la maintenance</span>
                                    <span class="fw-bold fs-5">{{ $engine->duration_maintenance }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Fiche technique</h3>
                </div>
                <div class="card-body scroll h-400px">
                    <x-base.underline
                        title="Information de surface"
                        size="1"
                        size-text="fs-3" />

                    <div class="table-responsive">
                        <table class="table border border-2 border-gray-700 table-row-bordered table-row-gray-700 table-hover table-striped gy-5 gx-5">
                            <tbody>
                                <tr>
                                    <td class="px-5">Longueur</td>
                                    <td>{{ $engine->technical->longueur }} m</td>
                                </tr>
                                <tr>
                                    <td class="px-5">Largeur</td>
                                    <td>{{ $engine->technical->largeur }} m</td>
                                </tr>
                                <tr>
                                    <td class="px-5">Hauteur</td>
                                    <td>{{ $engine->technical->hauteur }} m</td>
                                </tr>
                            </tbody>
                            <tfoot class="bg-gray-400 fs-3 fw-bold">
                                <tr>
                                    <td class="px-5">Surface Total</td>
                                    <td>{{ number_format(round(\App\Models\Core\Engine::calcSurface($engine->technical->longueur, $engine->technical->largeur, $engine->technical->hauteur), 2), 2, ',', ' ') }} m²</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <x-base.underline
                        title="Information moteur"
                        size="1"
                        size-text="fs-3" />
                    <div class="table-responsive">
                        <table class="table border border-2 border-gray-700 table-row-bordered table-row-gray-700 table-hover table-striped gy-5 gx-5">
                            <tbody>
                            <tr>
                                <td class="px-5">Type d'essieux</td>
                                <td>{{ $engine->technical->essieux }}</td>
                            </tr>
                            <tr>
                                <td class="px-5">Vitesse Maximal</td>
                                <td>{{ $engine->technical->max_speed }} Kmh</td>
                            </tr>
                            <tr>
                                <td class="px-5">Poids</td>
                                <td>{{ $engine->technical->poids }} Tonnes</td>
                            </tr>
                            <tr>
                                <td class="px-5">Force de traction</td>
                                <td>{{ $engine->technical->traction_force }} kN</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <x-base.underline
                        title="Information de Traction"
                        size="1"
                        size-text="fs-3" />
                    <div class="table-responsive">
                        <table class="table border border-2 border-gray-700 table-row-bordered table-row-gray-700 table-hover table-striped gy-5 gx-5">
                            <tbody>
                            <tr>
                                <td class="px-5">Type de motorisation</td>
                                <td>{{ $engine->technical->type_motor }}</td>
                            </tr>
                            <tr>
                                <td class="px-5">Puissance</td>
                                <td>{{ number_format($engine->technical->power_motor, 0, ',', ' ') }} KW</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <x-base.underline
                        title="Information de Maintenance"
                        size="1"
                        size-text="fs-3" />
                    <div class="table-responsive">
                        <table class="table border border-2 border-gray-700 table-row-bordered table-row-gray-700 table-hover table-striped gy-5 gx-5">
                            <tbody>
                            <tr>
                                <td class="px-5">Début d'exploitation</td>
                                <td>{{ $engine->technical->start_exploi->format('Y') }}</td>
                            </tr>
                            <tr>
                                <td class="px-5">Fin d'exploitation</td>
                                <td>{{ $engine->technical->end_exploi->format('Y') }}</td>
                            </tr>
                            <tr>
                                <td class="px-5">Durée de la maintenance</td>
                                <td>{{ $engine->duration_maintenance }}</td>
                            </tr>
                            @if($engine->type_energy == 'diesel')
                                <tr>
                                    <td class="px-5">Gasoil</td>
                                    <td>{{ number_format($engine->technical->gasoil, 0, ',', ' ') }} L</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <x-base.underline
                        title="Information de Marchandise"
                        size="1"
                        size-text="fs-3" />
                    <div class="table-responsive">
                        <table class="table border border-2 border-gray-700 table-row-bordered table-row-gray-700 table-hover table-striped gy-5 gx-5">
                            <tbody>
                            <tr>
                                <td class="px-5">Type de marchandise</td>
                                <td>{{ $engine->technical->type_marchandise }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script type="text/javascript">
        document.querySelector('.delete').addEventListener('click', e => {
            e.preventDefault()
            Swal.fire({
                title: "Êtes-vous sûr ?",
                text: "Vous ne pourrez pas revenir en arrière !",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Oui, supprimer !",
                cancelButtonText: "Non, annuler !",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    axios.delete('/admin/engines/' + id)
                        .then(function (response) {
                            Swal.fire({
                                title: "Suppression !",
                                text: "L'engin a été supprimé avec succès !",
                                icon: "success",
                                confirmButtonText: "Fermer",
                            }).then(function (result) {
                                if (result.value) {
                                    window.location.href='/admin/engines'
                                }
                            });
                        })
                        .catch(function (error) {
                            Swal.fire({
                                title: "Erreur !",
                                text: "L'engin n'a pas été supprimé !",
                                icon: "error",
                                confirmButtonText: "Fermer",
                            });
                        });
                } else if (result.dismiss === "cancel") {
                    Swal.fire({
                        title: "Annulation",
                        text: "L'engin n'a pas été supprimé !",
                        icon: "info",
                        confirmButtonText: "Fermer",
                    });
                }
            });
        })
    </script>
@endsection
