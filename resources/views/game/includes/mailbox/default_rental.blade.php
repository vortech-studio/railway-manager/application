Cher Monsieur {{ auth()->user()->name }},

<p>
    Je me permets de vous écrire pour vous informer d'une situation délicate concernant le paiement de
    votre location de matériel roulant <strong>{{ $engine_name }}</strong> auprès de notre société,
    {{ $rental->name }}+. Malheureusement, le second prélèvement d'un montant de {{  eur($rental_amount) }} €
    n'a pu être effectué sur le compte de votre entreprise.
</p>
<p>
    En raison de cet impayé, conformément à nos conditions contractuelles, nous avons été contraints
    de reprendre immédiatement le matériel roulant loué par votre société. Nous tenons à souligner que
    cette mesure a été prise suite à un prélèvement déjà rejeter.
</p>
<p>
    En ce qui concerne la caution, nous avons procédé à un remboursement sur le compte de votre société.
    Cependant, veuillez noter que des frais de restitution rapide d'un montant de
    {{ eur($amount_fee) }} ont été imputés à ce remboursement. Ces frais couvrent les coûts
    supplémentaires encourus par notre société pour récupérer rapidement le matériel roulant et assurer
    sa remise en location.
</p>

<p>
    Nous vous prions d'agréer, cher Monsieur {{ auth()->user()->name }}, l'expression de nos salutations distinguées.
</p>

<p>
    Cordialement,
</p>

<p>
    Responsable des recouvrements,<br>
    {{ $rental->name }}
</p>
