@extends("admin.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Gestion des Hubs",
        'action' => true,
        'data' => [
            [
                'url' => route('admin.hub.create'),
                'title' => "Ajouter un nouveau HUB",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="card card-flush">
        <!--begin::Card header-->
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                    <input type="text" data-hub-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Rechercher un hub" />
                </div>
                <!--end::Search-->
            </div>
            <!--end::Card title-->
            <!--begin::Card toolbar-->
            <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                <div class="w-100 mw-150px">
                    <!--begin::Select2-->
                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Status" data-kt-ecommerce-product-filter="status">
                        <option></option>
                        <option value="all">All</option>
                        <option value="1">Actif</option>
                        <option value="0">Inactif</option>
                    </select>
                    <!--end::Select2-->
                </div>
                <!--begin::Add product-->
                <a href="{{ route('admin.hub.create') }}" class="btn btn-primary">Nouveau HUB</a>
                <button class="btn btn-outline btn-outline-primary" data-action="export-hub"><i class="fa-solid fa-upload me-2"></i> Exporter les HUB</button>
                <button class="btn btn-outline btn-outline-primary" data-action="import-hub"><i class="fa-solid fa-download me-2"></i> Importer les HUB</button>
                <!--end::Add product-->
            </div>
            <!--end::Card toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body pt-0">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_hubs">
                <thead>
                <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                    <th class="w-10px pe-2">
                        <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                            <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_ecommerce_products_table .form-check-input" value="1" />
                        </div>
                    </th>
                    <th class="min-w-200px">Gare</th>
                    <th class="text-end min-w-100px">Prix Brute</th>
                    <th class="text-end min-w-70px">Taxe Brut</th>
                    <th class="text-end min-w-100px">Passagers</th>
                    <th class="text-end min-w-100px">Statistique Gare</th>
                    <th class="text-end min-w-100px">Actif</th>
                    <th class="text-end min-w-70px">Actions</th>
                </tr>
                </thead>
                <tbody class="fw-semibold text-gray-600">
                @foreach($hubs as $hub)
                <tr>
                    <td>
                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                            <input class="form-check-input" type="checkbox" value="1" />
                        </div>
                    </td>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="ms-5">
                                <!--begin::Title-->
                                <a href="{{ route('admin.gare.show', $hub->gare->id) }}" class="text-gray-800 text-hover-primary fs-5 fw-bold" data-kt-ecommerce-product-filter="hub_name">{{ $hub->gare->name }}</a>
                                <!--end::Title-->
                            </div>
                        </div>
                    </td>
                    <td class="text-end pe-0">
                        <span class="fw-bold">{{ eur($hub->price) }}</span>
                    </td>
                    <td class="text-end pe-0" data-order="40">
                        <span class="fw-bold ms-3">{{ eur($hub->taxe_hub) }}</span>
                    </td>
                    <td class="text-end pe-0">
                        <div class="d-flex flex-column">
                            <div class="d-flex flex-row mb-2">
                                <span class="fw-bolder me-5">1ere Classe:</span> <span class="badge badge-pill badge-sm badge-primary">{{ $hub->passenger_first }}</span>
                            </div>
                            <div class="d-flex flex-row">
                                <span class="fw-bolder me-5">2nd Classe:</span> <span class="badge badge-pill badge-sm badge-secondary">{{ $hub->passenger_second }}</span>
                            </div>
                        </div>
                    </td>
                    <td class="text-end pe-0" data-order="rating-5">
                        <div class="d-flex flex-column">
                            <div class="d-flex flex-row mb-2">
                                <span class="fw-bolder me-5">Nb Commerce</span> <span class="badge badge-pill badge-sm badge-primary">{{ $hub->nb_slot_commerce }}</span>
                            </div>
                            <div class="d-flex flex-row mb-2">
                                <span class="fw-bolder me-5">Nb Publicité</span> <span class="badge badge-pill badge-sm badge-primary">{{ $hub->nb_slot_pub }}</span>
                            </div>
                            <div class="d-flex flex-row mb-2">
                                <span class="fw-bolder me-5">Nb Parking</span> <span class="badge badge-pill badge-sm badge-primary">{{ $hub->nb_slot_parking }}</span>
                            </div>
                        </div>
                    </td>
                    <td class="text-end pe-0" data-order="{{ $hub->active ? 'actif' : 'inactif' }}">
                        <!--begin::Badges-->
                        @if($hub->active)
                            <div class="badge badge-light-success">actif</div>
                            <div class="d-none">1</div>
                        @else
                            <div class="badge badge-light-danger">inactif</div>
                            <div class="d-none">0</div>
                        @endif
                        <!--end::Badges-->
                    </td>
                    <td class="text-end">
                        <a href="#" class="btn btn-sm btn-light btn-flex btn-center btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <i class="ki-duotone ki-down fs-5 ms-1"></i></a>
                        <!--begin::Menu-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                            <!--begin::Menu item-->
                            @if($hub->active)
                                <div class="menu-item px-3">
                                    <a data-action="desactive_hub" data-id="{{ $hub->id }}" class="menu-link px-3">Désactiver</a>
                                </div>
                            @else
                                <div class="menu-item px-3">
                                    <a data-action="active_hub" data-id="{{ $hub->id }}" class="menu-link px-3">Activer</a>
                                </div>
                            @endif
                            <div class="menu-item px-3">
                                <a href="{{ route('admin.hub.edit', $hub->id) }}" class="menu-link px-3">Editer</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3" data-kt-ecommerce-product-filter="delete_row" data-id="{{ $hub->id }}">Supprimer</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu-->
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let table = $('#liste_hubs').DataTable({
            info: !1,
            order: [],
            pageLength: 10,
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
        });

        document.querySelector('[data-hub-filter="search"]').addEventListener('keyup', function (e) {
            table.search(e.target.value).draw();
        });

        let filter_status = document.querySelector('[data-kt-ecommerce-product-filter="actif"]')
        $(filter_status).on('change', function (t) {
            console.log(t.target.value)
            let n = t.target.value;
            "all" === n && (n = ""), table.column(7).search(n).draw()
        })

        document.querySelectorAll('[data-action="desactive_hub"]').forEach(function (btn) {
            btn.addEventListener('click', function (e) {
                e.preventDefault()
                let id = btn.dataset.id
                $.ajax({
                    url: "/api/core/hub/" + id + "/desactive",
                    method: "PUT",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id
                    },
                    success: function (data) {
                        console.log(data)
                        btn.removeAttribute('data-kt-indicator')
                        toastr.success("Le hub à été désactivé")
                        setTimeout(() => {
                            window.location.reload()
                        }, 1300)
                    }
                })
            })
        })

        document.querySelectorAll('[data-action="active_hub"]').forEach(function (btn) {
            btn.addEventListener('click', function (e) {
                e.preventDefault()
                let id = btn.dataset.id
                $.ajax({
                    url: "/api/core/hub/" + id + "/active",
                    method: "PUT",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id
                    },
                    success: function (data) {
                        console.log(data)
                        btn.removeAttribute('data-kt-indicator')
                        toastr.success("Le hub à été activé")
                        setTimeout(() => {
                            window.location.reload()
                        }, 1300)
                    }
                })
            })
        })

        document.querySelectorAll('[data-kt-ecommerce-product-filter="delete_row"]').forEach(function (btn) {
            btn.addEventListener('click', function (e) {
                e.preventDefault()
                let id = btn.dataset.id
                $.ajax({
                    url: "/api/core/hub/" + id + "/delete",
                    method: "DELETE",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id
                    },
                    success: function (data) {
                        console.log(data)
                        btn.removeAttribute('data-kt-indicator')
                        toastr.success("Le hub à été supprimé")
                        setTimeout(() => {
                            window.location.reload()
                        }, 1300)
                    }
                })
            })
        })

        document.querySelector('[data-action="export-hub"]').addEventListener('click', function (e) {
            e.preventDefault()
            $.ajax({
                url: "/api/core/hub/export",
                method: "GET",
                success: function (data) {
                    console.log(data)
                    toastr.success("Les hubs ont été exportés")
                    setTimeout(() => {
                        window.location.reload()
                    }, 1300)
                }
            })
        })

        document.querySelector('[data-action="import-hub"]').addEventListener('click', function (e) {
            e.preventDefault()
            $.ajax({
                url: "/api/core/hub/import",
                method: "GET",
                success: function (data) {
                    console.log(data)
                    toastr.success("Les hubs ont été importés")
                    setTimeout(() => {
                        window.location.reload()
                    }, 1300)
                }
            })
        })

        const e = document.querySelector('[data-kt-ecommerce-product-filter="status"]')
        $(e).on('change', (e => {
            let o = e.target.value
            "all" === o && (o = ""), table.column(6).search(o).draw()
        }))
    </script>
@endsection
