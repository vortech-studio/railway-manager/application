<?php

namespace App\Http\Controllers\Game;

use App\Http\Controllers\Controller;
use App\Models\Core\CardHolder;
use App\Models\Core\Engine;
use App\Models\User\Mailbox;
use App\Models\User\UserCard;
use App\Traits\HasCheckout;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class ShopController extends Controller
{
    public function index()
    {
        seo()->title(config('app.name') . ' - Boutique');
        return view('game.shop.index');
    }

    public function checkout(Request $request)
    {
        try {
            match ($request->get('slug')) {
                "card-eco", "card-finance", "card-platinum", "card-epique" => $this->checkoutCardHolder($request, auth()->user()),
                "engine" => $this->checkoutEngine($request)
            };

            return response()->json([
                "success" => true,
                "message" => "Votre achat a bien été effectué !",
                "action" => $request->get('slug')
            ]);
        }catch (\Exception $exception) {
            \Log::critical($exception->getMessage());
            return response()->json([
                "success" => false,
                "message" => "Une erreur est survenue lors de votre achat !",
                "action" => $request->get('slug')
            ], 500);
        }
    }

    public function checkoutConfirm(Request $request)
    {
        Stripe::setApiKey(config('services.stripe.secret_key'));

        try {
            $intent = PaymentIntent::retrieve($request->get('payment_intent'));

            match ($intent->metadata->product) {
                "tpoint-140", "tpoint-160", "tpoint-260", "tpoint-300", "tpoint-700", "tpoint-1600", "tpoint-2700", "tpoint-4800", "tpoint-8100" => HasCheckout::checkoutTpoint($intent, auth()->user()),
            };

            $m = Mailbox::create([
                "from" => "Support Railway manager",
                "subject" => "Votre achat N°" . $intent->id,
                "message" => view('game.includes.mailbox.checkout_confirm', [
                    "user" => auth()->user(),
                ]),
                "user_id" => auth()->user()->id,
            ]);
            $m->actions()->create([
                "action_type" => "other",
                "action_link" => route('account.index'),
                "action_text" => "Espace personnel"
            ]);


            return redirect()->route('shop.index')->with('success', "Votre achat a bien été traité !");
        } catch (ApiErrorException $e) {
            \Log::critical($e->getMessage());
            return redirect()->route('shop.index')->with('error', "Une erreur est survenue lors du traitement de votre commande, un tickets de support à été ouvert !");
        }
    }

    private function checkoutEngine(Request $request)
    {
        $request->merge([
            "engine_id" => Engine::where('slug', $request->get('engine'))->first()->id
        ]);

        if($request->get('currency') == 'tpoint') {
            $request->user()->tpoint -= $request->get('amount');
            $request->user()->save();
        } elseif($request->get('currency') == 'argent') {
            $request->user()->argent -= $request->get('amount');
            $request->user()->save();
        } else {
            return response()->json([
                "success" => false,
                "message" => "Une erreur est survenue lors de votre achat !",
                "action" => $request->get('slug')
            ], 500);
        }

        return HasCheckout::checkoutEngine($request);
    }

    private function checkoutCardHolder(Request $request, \App\Models\User|\Illuminate\Contracts\Auth\Authenticatable|null $user)
    {
        try {
            HasCheckout::checkoutCardHolder($request->get('amount'), $user);

            $card_communes = CardHolder::where('card_holder_category_id', 1);
            $card_remarquables = CardHolder::where('card_holder_category_id', 2);
            $card_prestiges = CardHolder::where('card_holder_category_id', 3);
            $card_legends = CardHolder::where('card_holder_category_id', 4);

            $matching = match($request->get('slug')) {
                "card-eco" => $this->gatchaCardEco($card_communes, $user),
                "card-finance" => $this->gatchaCardFinance($card_communes, $card_remarquables, $user),
            };
        }catch (\Exception $exception) {
            \Log::critical($exception->getMessage());
        }

    }

    private function gatchaCardEco(Builder $card_communes, \App\Models\User|\Illuminate\Contracts\Auth\Authenticatable|null $user)
    {
        try {
            $com = $card_communes->get()->random(1);

            $card = UserCard::create([
                "uuid" => \Str::uuid(),
                "type_card" => "economique",
                "user_id" => $user->id,
            ]);


            $card->cards()->create([
                "user_card_id" => $card->id,
                "card_holder_id" => $com->first()->id,
            ]);


            $m = $user->messages()->create([
                "from" => $user->name_secretary,
                "subject" => "Nouveau porte carte économique disponible !",
                "message" => view('game.includes.mailbox.new_card_holder', [
                    "card" => $card,
                ]),
                "important" => true,
                "user_id" => $user->id,
            ]);
            $ac = Mailbox::latest()->first();

            $ac->actions()->create([
                "action_type" => "other",
                "action_text" => "Voir le porte carte",
                "action_link" => route('card.show', $card->uuid),
                "mailbox_id" => $m->id
            ]);

            return null;
        }catch (\Exception $exception) {
            \Log::critical($exception->getMessage());
            return null;
        }
    }
}
