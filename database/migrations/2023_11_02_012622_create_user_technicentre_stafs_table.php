<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTechnicentreStafsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_technicentre_stafs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('job');
            $table->string('satisfaction')->default(5);
            $table->integer('niveau');
            $table->float('salary');
            $table->enum('status', ['inactive', 'busy', 'active', 'formation'])->default('inactive');

            $table->foreignId('user_technicentre_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_technicentre_stafs');
    }
}
