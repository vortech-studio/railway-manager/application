<?php

namespace App\Models\Core;

use App\Models\User\UserEngineMaterialStatus;
use App\Models\User\UserIncident;
use Illuminate\Database\Eloquent\Model;

class EngineComposition extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function engine()
    {
        return $this->belongsTo(Engine::class);
    }

    public function incidents()
    {
        return $this->hasMany(UserIncident::class);
    }

    public static function generateTaskPreventif($name)
    {
        return match ($name) {
            "Moteur Termique" => "Vérification du moteur thermique",
            "Generateur" => "Vérification du générateur",
            "Convertisseur statique" => "Vérification du convertisseur statique",
            "Infrasture de caisse" => "Vérification de l'infrastructure de caisse",
            "Attelage" => "Vérification de l'attelage",
            "Cabine de conduite" => "Vérification de l'intégrité et des système de la cabine de conduite",
            "Bogie" => "Vérification des bogies",
            "Bloc électrique centrale" => "Vérification du bloc électrique centrale",
            "Bloc auxiliaire – batterie" => "Vérification du bloc auxiliaire - batterie",
            "Batterie" => "Vérification des batteries",
            "Semi-conducteurs de puissance" => "Vérification des semi-conducteurs de puissance",
            "Pantographe" => "Vérification du pantographe",
            "Ligne de toiture" => "Vérification de la ligne de toiture",
            "Liaison haute tension" => "Vérification de la liaison haute tension",
            "Transformateur Principal" => "Vérification du transformateur principal",
            "Sablière" => "Vérification de la sablière",
            "Lanterneau de sortie d’air" => "Vérification du lanterneau de sortie d’air",
            "Disjoncteur monophasé" => "Vérification du disjoncteur monophasé",
            "Groupe de filtration d’air" => "Vérification de la filtration d’air",
            "Boite de Vitesse" => "Vérification de la boite de vitesse",
            "Chauffage" => "Vérification des lignes de chauffage",
            "Climatisation" => "Vérification de la climatisation",
            "Convertisseur Diesel/Courant" => "Vérification du convertisseur Diesel/Courant",
            "Disjoncteur Multiphasé" => "Vérification du disjoncteur Multiphasé",
            "Eclairage" => "Vérification de l'éclairage",
            "Fenêtres" => "Vérification des fenêtres",
            "Luminaires" => "Vérification des luminaires",
            "Portes" => "Vérification des portes",
            "Sièges" => "Vérification des sièges",
            "Système de communication" => "Vérification des systèmes de communication",
            "Système de freinage" => "Vérification du système de freinage",
            "Système de sécurité" => "Vérification du système de sécurité",
            "Système de signalisation" => "Vérification du système de signalisation sol-train",
            "Système de ventilation" => "Vérification du système de ventilation",
            "Toilettes" => "Vérification des toilettes",
        };
    }
}
