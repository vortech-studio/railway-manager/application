<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Core\GareEquipement;
use App\Models\Core\Gares;
use App\Models\Core\GareTransportation;
use App\Models\Core\Hub;
use App\Services\DataGouv\DecoupageCommune;
use App\Services\DataTable\DataTable;
use App\Services\SNCF\GareVoyageur;
use App\Traits\GareTrait;
use Illuminate\Http\Request;
use RuntimeException;

class GareController extends Controller
{
    use GareTrait;
    public function index(DataTable $dataTable)
    {
        return view('admin.gare.index', [
            "gares" => Gares::all(),
        ]);
    }

    public function create()
    {
        return view('admin.gare.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            "name" => "required|unique:gares,name",
        ]);
        $api = new GareVoyageur();
        $cmn = new DecoupageCommune();
        $superficie = $this->calcSuperficie(null, $request->get('type_gare'));
        $frequentation = $api->setFrequentation($request->get('type_gare'));
        $gare = Gares::create([
            "name" => $request->get('name'),
            "code_uic" => $request->get('code_uic'),
            "type_gare" => $request->get('type_gare'),
            "latitude" => $request->get('longitude'),
            "longitude" => $request->get('latitude'),
            "region" => $request->get('region'),
            "pays" => $request->get('pays'),
            "superficie_infra" => $superficie,
            "superficie_quai" => $this->calcSuperficieQuai(null, $request->get('nb_quai'), $request->get('long_quai')),
            "long_quai" => $request->get('long_quai'),
            "nb_quai" => $request->get('nb_quai'),
            "commerce" => $this->hasCommerce(null, $request->get('type_gare')),
            "pub" => $this->hasPub(null, $request->get('type_gare')),
            "parking" => $this->hasParking(null, $superficie, $request->get('type_gare')),
            "frequentation" => $frequentation,
            "nb_habitant_city" => $cmn->getPopulation(null, $frequentation, $request->get('name')),
            "time_day_work" => $this->timeDayWork(null, $request->get('type_gare')),
        ]);

        GareEquipement::dispatching($gare);

        if($request->has('ter')) {
            GareTransportation::create([
                "type" => "ter",
                "gares_id" => $gare->id,
            ]);
        }
        if($request->has('tgv')) {
            GareTransportation::create([
                "type" => "tgv",
                "gares_id" => $gare->id,
            ]);
        }
        if($request->has('intercite')) {
            GareTransportation::create([
                "type" => "intercite",
                "gares_id" => $gare->id,
            ]);
        }
        if($request->has('transilien')) {
            GareTransportation::create([
                "type" => "transilien",
                "gares_id" => $gare->id,
            ]);
        }

        if($gare->type_gare == 'a') {
            Hub::create([
                "price" => $this->calcPrice($gare),
                "taxe_hub" => $this->calcTaxeHub(null, $gare),
                "passenger_first" => $this->calcPassengerFirst($gare),
                "passenger_second" => $this->calcPassengerSecond($gare),
                "nb_slot_commerce" => $this->calcNbSlotCommerce($gare),
                "nb_slot_pub" => $this->calcNbSlotPub($gare),
                "nb_slot_parking" => $this->calcNbSlotParking($gare),
                "active" => false,
                "gares_id" => $gare->id,
            ]);
        }


        toastr()->success("Gare enregistré");
        return redirect()->route('admin.gare.index');

    }

    public function show($id)
    {
        $gare = Gares::find($id)->load('hub');

        if (! $gare) {
            throw new RuntimeException('Gare not found');
        }

        return view('admin.gare.show', [
            'gare' => $gare,
        ]);
    }
}
