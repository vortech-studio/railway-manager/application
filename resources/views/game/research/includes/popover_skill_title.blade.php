<div class='d-flex flex-row align-items-center mb-5'>
    <div class='symbol symbol-40 me-5'>
        <img src='{{ asset('/storage/icons/research/' . $skill->id.'.png') }}' alt=''>
    </div>
    <div class='d-flex flex-column'>
        <span class='text-orange-700 fs-3'>{{ $skill->name }}</span>
        <span class='fw-bolder fs-3 mb-2'>{{ $skill->subname }}</span>
        <span class='text-primary fs-4 ms-3'>{{ $skill->influence }}</span>
    </div>
</div>
<div>
    <span>{{ $skill->description }}</span>
</div>
<div class='separator border border-white my-5'></div>
@if($skill->level_max > 1)
    @for($l = 1; $l <= $skill->level_max; $l++)
        @if($skill->users()->where('id', auth()->user()->id)->first()->pivot->level >= $l)
            <div class='d-flex align-items-center text-white fs-8 fs-italic'>
                <span class='bullet bullet-dot bullet-white me-3'></span> {{ $skill->influence }} : @if($skill->action_base_value <= 0) {{ $skill->getActionBaseValueFormat($skill->action_base_type, $skill->action_base_value * ($skill->level_multiplicator * $l)) }} @else + {{ $skill->getActionBaseValueFormat($skill->action_base_type, $skill->action_base_value * ($skill->level_multiplicator * $l)) }}  @endif
            </div>
        @else
            <div class='d-flex align-items-center text-gray-600 fs-8 fs-italic'>
                <span class='bullet bullet-dot bullet-white me-3'></span> {{ $skill->influence }} : @if($skill->action_base_value <= 0) {{ $skill->getActionBaseValueFormat($skill->action_base_type, $skill->action_base_value * ($skill->level_multiplicator * $l)) }} @else + {{ $skill->getActionBaseValueFormat($skill->action_base_type, $skill->action_base_value * ($skill->level_multiplicator * $l)) }}  @endif
            </div>
        @endif
    @endfor
@endif
<div class='separator border border-white my-5'></div>
<div class='d-flex flex-row justify-content-between align-items-center'>
    <div class='d-flex flex-row'>
        <img src='{{ asset('/storage/icons/euro.png') }}' alt='' class='w-15px h-15px me-2' />
        <span class='text-white fs-6'>{{ $skill->getActualCoast() }}</span>
    </div>
    <div class='d-flex flex-row'>
        <i class='fa-solid fa-clock fs-3 me-2'></i>
        <span class='text-white fs-6'>{{ $skill->getActualTime() }}</span>
    </div>
</div>
