@extends("game.template")

@section("css")

@endsection

@section("info_bar")

@endsection

@section("content")
    <div class="card shadow-lg">
        <div class="card-header">
            <div class="card-title">Mes Notifications</div>
            <div class="card-toolbar"></div>
        </div>
        <div class="card-body">
            @foreach(auth()->user()->notifications as $notification)
                <div class="d-flex align-items-center bg-light-{{ $notification->data['type'] }} rounded p-5 mb-5">
                    <img src="{{ $notification->data['icon'] }}" class="w-50px" alt="">
                    <div class="flex-grow-1 me-2">
                        <div class="d-flex flex-column">
                            <span class="fs-2 fw-bold">{{ $notification->data['title'] }}</span>
                            <span class="fs-4 fs-italic">{!! $notification->data['description'] !!}</span>
                        </div>
                    </div>
                    <span class="fs-5 fs-italic fw-semibold">{{ $notification->created_at->diffForHumans() }}</span>
                </div>
            @endforeach
        </div>
        <div class="card-footer">

        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">

    </script>
@endsection
