<?php

namespace App\Observer;

use App\Enums\SkillActionTypeEnum;
use App\Models\User\UserHub;

class UserHubObserver
{
    public function created(UserHub $userHub): void
    {
        $userHub->technicentre()->create([
            "user_id" => $userHub->user_id,
            "user_hub_id" => $userHub->id,
        ]);

        $userHub->skills()->create([
            "name" => "Dispatching Améliorer",
            "description" => "Permet d'augmenter le nombre de ligne accessible pour le hub.",
            "level" => true,
            "level_max" => 5,
            "level_multiplicator" => 1.2,
            "action" => "improveDispatch",
            "action_base_value" => round($userHub->hub->gare->nb_quai / 5, 0),
            "action_type" => SkillActionTypeEnum::PERCENT,
            "action_value" => 2,
            "coast_base" => 10000,
            "time_base" => 60,
            "requirements" => null,
            "user_hub_id" => $userHub->id,
            "image" => "dispatching.png"
        ]);

        $userHub->skills()->create([
            "name" => "Commerce",
            "description" => "Ouvre le hub au commerce.",
            "level" => false,
            "action" => "openCommerce",
            "action_type" => SkillActionTypeEnum::UNIT,
            "action_value" => 1,
            "coast_base" => 50000,
            "time_base" => 720,
            "requirements" => null,
            "user_hub_id" => $userHub->id,
            "image" => "commerce.png"
        ]);

        $userHub->skills()->create([
            "name" => "Emplacement Commercial",
            "description" => "Augmente de nombre d'emplacement commercial disponible dans un hub",
            "level" => true,
            "level_max" => 5,
            "level_multiplicator" => 1.5,
            "action" => "commercialLocation",
            "action_base_value" => intval($userHub->hub->nb_slot_commerce / 10),
            "action_type" => SkillActionTypeEnum::PERCENT,
            "action_value" => 15,
            "coast_base" => 10000,
            "time_base" => 45,
            "requirements" => json_encode(["user_hub_skill_id" => 2]),
            "user_hub_id" => $userHub->id,
            "image" => "commerce-location.png"
        ]);

        $userHub->skills()->create([
            "name" => "Publicité",
            "description" => "Ouvre le hub à l'affichage publicitaire.",
            "level" => false,
            "action" => "openPublicity",
            "action_type" => SkillActionTypeEnum::UNIT,
            "action_value" => 1,
            "coast_base" => 50000,
            "time_base" => 720,
            "requirements" => null,
            "user_hub_id" => $userHub->id,
            "image" => "publicity.png"
        ]);

        $userHub->skills()->create([
            "name" => "Emplacement Publicitaire",
            "description" => "Augmente de nombre d'emplacement publicitaire disponible dans un hub",
            "level" => true,
            "level_max" => 5,
            "level_multiplicator" => 1.5,
            "action" => "publicityLocation",
            "action_base_value" => intval($userHub->hub->nb_slot_pub / 10),
            "action_type" => SkillActionTypeEnum::PERCENT,
            "action_value" => 15,
            "coast_base" => 10000,
            "time_base" => 45,
            "requirements" => json_encode(["user_hub_skill_id" => 4]),
            "user_hub_id" => $userHub->id,
            "image" => "publicity-location.png"
        ]);

        $userHub->skills()->create([
            "name" => "Parking",
            "description" => "Ouvre le hub à la création d'un parking.",
            "level" => false,
            "action" => "openParking",
            "action_type" => SkillActionTypeEnum::UNIT,
            "action_value" => 1,
            "coast_base" => 50000,
            "time_base" => 720,
            "requirements" => null,
            "user_hub_id" => $userHub->id,
            "image" => "parking.png"
        ]);

        $userHub->skills()->create([
            "name" => "Emplacement Parking",
            "description" => "Augmente de nombre d'emplacement de parking disponible dans un hub",
            "level" => true,
            "level_max" => 2,
            "level_multiplicator" => 4.8,
            "action" => "parkingLocation",
            "action_base_value" => intval($userHub->hub->nb_slot_parking / 10),
            "action_type" => SkillActionTypeEnum::PERCENT,
            "action_value" => 20,
            "coast_base" => 32000,
            "time_base" => 165,
            "requirements" => json_encode(["user_hub_skill_id" => 6]),
            "user_hub_id" => $userHub->id,
            "image" => "parking-location.png"
        ]);
    }
}
