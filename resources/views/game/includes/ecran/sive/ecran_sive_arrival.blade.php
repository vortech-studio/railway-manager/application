<div id="loadContent"></div>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        let loadContent = document.getElementById('loadContent');
        update()

        function update() {
            $.ajax({
                url: '/api/game/ecran/travel/' + {{ $planning->id }}+'/arrival',
                success: data => {
                    loadContent.innerHTML = data.content;
                }
            })
        }
    });
</script>
