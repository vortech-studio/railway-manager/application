@extends("game.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/train.png') }}" alt="">
            </div>
            <x-base.underline
                title="Gestion des engins roulants"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.engine.partials.menu")
    </div>
    <div class="card shadow-lg bg-gray-400">
        <div class="card-body p-5">
            <div class="d-flex flex-row justify-content-center align-items-center nav mb-5">
                <button class="btn btn-outline btn-outline-primary active me-3" data-bs-toggle="tab" data-bs-target="#flotte">Gestion de la flotte</button>
                <button class="btn btn-outline btn-outline-primary" data-bs-toggle="tab" data-bs-target="#rents">Contrats de location</button>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="flotte" role="tabpanel">
                    <x-base.underline
                        :title="Str::upper('Livraisons en cours')"
                        size="6"
                        color="gray-800"
                        size-text="fs-2tx"
                        color-text="gray-600" />

                    <div class="d-flex flex-column w-75">
                        @if(auth()->user()->livraisons()->where('type', 'engine')->get()->count() == 0)
                            <div class="d-flex flex-row justify-content-center align-items-center">
                                <i class="fa-solid fa-exclamation-triangle text-warning fs-4x me-2"></i>
                                <span class="fs-1 fw-bolder">Aucune livraison en cours</span>
                            </div>
                        @else
                            @foreach(auth()->user()->livraisons()->where('type', 'engine')->get() as $livraison)
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="d-flex flex-row align-items-center">
                                        <img src="{{ $livraison->user_engine->engine->image_url }}" class="w-150px me-3" alt="">
                                        <span class="fs-1 fw-bolder">{{ $livraison->user_engine->engine->name }} - {{ $livraison->user_engine->number }}<span class="fw-semibold">/ {{ $livraison->user_engine->user_hub->hub->gare->name }}</span></span>
                                    </div>
                                    <span class="fw-bold fs-2" data-ago="{{ $livraison->end_at->timestamp }}"></span>
                                    <button class="btn btn-sm btn-primary btn-flush rounded p-3"
                                            data-action="accel"
                                            data-point="10"
                                            data-type="deliveryEngine"
                                            data-id="{{ $livraison->id }}"
                                    >
                                        <img src="{{ asset('/storage/icons/tpoint.png') }}" class="w-30px me-2" alt="">
                                        <span>Terminer</span>
                                    </button>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="separator border border-2 border-gray-500 my-5"></div>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-50px me-3" alt="">
                                <span>Gestion de la flotte</span>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="d-flex flex-stack bg-gray-300 shadow-box mb-5 p-5">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="fa-solid fa-magnifying-glass fs-1 position-absolute ms-6"></i>
                                    <input type="text" data-table-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Rechercher un engin">
                                </div>
                                <div class="d-flex justify-content-end" data-table-toolbar="base">
                                    <div>
                                        <button type="button" class="btn btn-light-primary me-3" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            <i class="ki-duotone ki-filter fs-2"><span class="path1"></span><span class="path2"></span></i>        Filter
                                        </button>
                                        <div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true" id="kt-toolbar-filter" style="">
                                            <!--begin::Header-->
                                            <div class="px-7 py-5">
                                                <div class="fs-4 text-gray-900 fw-bold">Filtre</div>
                                            </div>
                                            <!--end::Header-->

                                            <!--begin::Separator-->
                                            <div class="separator border-gray-200"></div>
                                            <!--end::Separator-->

                                            <div class="px-7 py-5">
                                                <!--begin::Input group-->
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fs-5 fw-semibold mb-3">Type de train:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <select class="form-select form-select-solid fw-bold" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true" data-table-filter="type_train" data-dropdown-parent="#kt-toolbar-filter">
                                                        <option></option>
                                                        <option value="ter">TER</option>
                                                        <option value="tgv">TGV</option>
                                                        <option value="ic">INTERCITE</option>
                                                        <option value="other">AUTRE</option>
                                                    </select>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fs-5 fw-semibold mb-3">Hub:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <select class="form-select form-select-solid fw-bold" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true" data-table-filter="hub" data-dropdown-parent="#kt-toolbar-filter">
                                                        <option></option>
                                                        @foreach(auth()->user()->hubs as $hub)
                                                            <option value="{{ $hub->id }}">{{ $hub->hub->gare->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Input group-->
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fs-5 fw-semibold mb-3">Payment Type:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Options-->
                                                    <div class="d-flex flex-column flex-wrap fw-semibold" data-kt-customer-table-filter="payment_type">
                                                        <!--begin::Option-->
                                                        <label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                            <input class="form-check-input" type="radio" name="payment_type" value="all" checked="checked" />
                                                            <span class="form-check-label text-gray-600">All</span>
                                                        </label>
                                                        <!--end::Option-->
                                                        <!--begin::Option-->
                                                        <label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                            <input class="form-check-input" type="radio" name="payment_type" value="visa" />
                                                            <span class="form-check-label text-gray-600">Visa</span>
                                                        </label>
                                                        <!--end::Option-->
                                                        <!--begin::Option-->
                                                        <label class="form-check form-check-sm form-check-custom form-check-solid mb-3">
                                                            <input class="form-check-input" type="radio" name="payment_type" value="mastercard" />
                                                            <span class="form-check-label text-gray-600">Mastercard</span>
                                                        </label>
                                                        <!--end::Option-->
                                                        <!--begin::Option-->
                                                        <label class="form-check form-check-sm form-check-custom form-check-solid">
                                                            <input class="form-check-input" type="radio" name="payment_type" value="american_express" />
                                                            <span class="form-check-label text-gray-600">American Express</span>
                                                        </label>
                                                        <!--end::Option-->
                                                    </div>
                                                    <!--end::Options-->
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Actions-->
                                                <div class="d-flex justify-content-end">
                                                    <button type="reset" class="btn btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true" data-kt-customer-table-filter="reset">Reset</button>
                                                    <button type="submit" class="btn btn-primary" data-kt-menu-dismiss="true" data-table-filter="filter">Appliquer</button>
                                                </div>
                                                <!--end::Actions-->
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-light-primary me-3" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            <i class="ki-duotone ki-filter fs-2"><span class="path1"></span><span class="path2"></span></i>        Classement
                                        </button>
                                        <div class="menu menu-sub menu-sub-dropdown w-600px w-md-325px" data-kt-menu="true" id="kt-toolbar-class" style="">
                                            <!--begin::Header-->
                                            <div class="px-7 py-5">
                                                <div class="fs-4 text-gray-900 fw-bold">Classement</div>
                                            </div>
                                            <!--end::Header-->

                                            <!--begin::Separator-->
                                            <div class="separator border-gray-200"></div>
                                            <!--end::Separator-->

                                            <div class="px-7 py-5">
                                                <div class="mb-3 d-flex flex-row" data-table-filter="date">
                                                    <label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                        <input class="form-check-input" type="radio" name="sort" value="purchaseDateAsc" checked="checked" />
                                                        <span class="form-check-label text-gray-600">Date achat +</span>
                                                    </label>
                                                    <label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                        <input class="form-check-input" type="radio" name="sort" value="purchaseDateDesc" />
                                                        <span class="form-check-label text-gray-600">Date achat -</span>
                                                    </label>
                                                </div>
                                                <div class="mb-3 d-flex flex-row" data-table-filter="used">
                                                    <label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                        <input class="form-check-input" type="radio" name="sort" value="usedAsc" checked="checked" />
                                                        <span class="form-check-label text-gray-600">Utilisation +</span>
                                                    </label>
                                                    <label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                        <input class="form-check-input" type="radio" name="sort" value="usedDesc" />
                                                        <span class="form-check-label text-gray-600">Utilisation -</span>
                                                    </label>
                                                </div>
                                                <div class="mb-3 d-flex flex-row" data-table-filter="mark">
                                                    <label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                        <input class="form-check-input" type="radio" name="sort" value="markAsc" checked="checked" />
                                                        <span class="form-check-label text-gray-600">Anciennete +</span>
                                                    </label>
                                                    <label class="form-check form-check-sm form-check-custom form-check-solid mb-3 me-5">
                                                        <input class="form-check-input" type="radio" name="sort" value="markDesc" />
                                                        <span class="form-check-label text-gray-600">Anciennete -</span>
                                                    </label>
                                                </div>
                                                <!--begin::Actions-->
                                                <div class="d-flex justify-content-end">
                                                    <button type="reset" class="btn btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true" data-table-filter="reset">Annuler</button>
                                                    <button type="submit" class="btn btn-primary" data-kt-menu-dismiss="true" data-table-filter="classing">Appliquer</button>
                                                </div>
                                                <!--end::Actions-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_engine">
                                <thead>
                                    <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                        <th class="w-10px pe-2"></th>
                                        <th class="min-w-50px"></th>
                                        <th class="min-w-125px">Matériel</th>
                                        <th class="min-w-125px">Hub</th>
                                        <th class="min-w-125px">Rayon d'action</th>
                                        <th class="min-w-125px">Utilisation</th>
                                        <th class="min-w-125px">Usure</th>
                                        <th class="min-w-125px">Ancienneté</th>
                                        <th class="min-w-125px">Place</th>
                                        <th class="min-w-125px">Résultat</th>
                                        <th class="text-end min-w-70px"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(auth()->user()->engines()->where('active', true)->get() as $engine)
                                        <tr>
                                            <td>
                                                <i class="fa-solid fa-eye fs-9" data-bs-toggle="popover" data-bs-placement="right" data-bs-custom-class="popover-inverse" data-bs-html="true" data-bs-content="<img src='{{ $engine->engine->image_url }}' alt='engine' />"></i>
                                            </td>
                                            <td>
                                                <img src="{{ asset('/storage/logos/logo_'.$engine->engine->type_train.'.svg') }}" class="w-40px" alt="">
                                                <span class="d-none">{{ $engine->engine->type_train }}</span>
                                            </td>
                                            <td>{{ $engine->engine->name }} - {{ $engine->number }}</td>
                                            <td>{{ $engine->user_hub->hub->gare->name }}</td>
                                            <td>{{ $engine->max_runtime_engine }} Km</td>
                                            <td>{{ $engine->calcUtilisationEngine() }} %</td>
                                            <td>{{ $engine->use_percent }} %</td>
                                            <td>{{ $engine->ancien }} %</td>
                                            <td>{{ $engine->engine->nb_passager }}</td>
                                            <td>{{ eur($engine->calcResultat()) }}</td>
                                            <td class="text-end">
                                                <a href="{{ route('engine.show', $engine->id) }}" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
                                                    <img src="{{ asset('/storage/icons/train.png') }}" class="w-25px" alt="">
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="rents" role="tabpanel">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-column w-50">
                            <div class="d-flex flex-row justify-content-between align-items-center mb-1">
                                <span>Total des cautions</span>
                                <span class="fw-bold fs-2">{{ eur(auth()->user()->rentals()->sum('amount_caution')) }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="separator border border-2 border-gray-500 my-5"></div>
                    <div class="card" data-sector="rent">
                        <div class="card-header">
                            <div class="card-title">
                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-50px me-3" alt="">
                                <span>Gestion de la flotte</span>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="d-flex flex-stack bg-gray-300 shadow-box mb-5 p-5">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="fa-solid fa-magnifying-glass fs-1 position-absolute ms-6"></i>
                                    <input type="text" data-table-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Rechercher un engin">
                                </div>
                                <div class="d-flex justify-content-end" data-table-toolbar="base">
                                    <div>
                                        <button type="button" class="btn btn-light-primary me-3" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            <i class="ki-duotone ki-filter fs-2"><span class="path1"></span><span class="path2"></span></i>        Filter
                                        </button>
                                        <div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true" id="kt-toolbar-filter" style="">
                                            <!--begin::Header-->
                                            <div class="px-7 py-5">
                                                <div class="fs-4 text-gray-900 fw-bold">Filtre</div>
                                            </div>
                                            <!--end::Header-->

                                            <!--begin::Separator-->
                                            <div class="separator border-gray-200"></div>
                                            <!--end::Separator-->

                                            <div class="px-7 py-5">
                                                <!--begin::Input group-->
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fs-5 fw-semibold mb-3">Type de train:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <select class="form-select form-select-solid fw-bold" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true" data-table-filter="type_train" data-dropdown-parent="#kt-toolbar-filter">
                                                        <option></option>
                                                        <option value="ter">TER</option>
                                                        <option value="tgv">TGV</option>
                                                        <option value="ic">INTERCITE</option>
                                                        <option value="other">AUTRE</option>
                                                    </select>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="mb-10">
                                                    <!--begin::Label-->
                                                    <label class="form-label fs-5 fw-semibold mb-3">Hub:</label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <select class="form-select form-select-solid fw-bold" data-kt-select2="true" data-placeholder="Select option" data-allow-clear="true" data-table-filter="hub" data-dropdown-parent="#kt-toolbar-filter">
                                                        <option></option>
                                                        @foreach(auth()->user()->hubs as $hub)
                                                            <option value="{{ $hub->id }}">{{ $hub->hub->gare->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Input group-->
                                                <!--begin::Actions-->
                                                <div class="d-flex justify-content-end">
                                                    <button type="reset" class="btn btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true" data-kt-customer-table-filter="reset">Reset</button>
                                                    <button type="submit" class="btn btn-primary" data-kt-menu-dismiss="true" data-table-filter="filter">Appliquer</button>
                                                </div>
                                                <!--end::Actions-->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_engine">
                                <thead>
                                <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                    <th class="w-10px pe-2"></th>
                                    <th class="min-w-50px"></th>
                                    <th class="min-w-125px">Matériel</th>
                                    <th class="min-w-125px">Hub</th>
                                    <th class="min-w-125px">Rayon d'action</th>
                                    <th class="min-w-125px">Utilisation</th>
                                    <th class="min-w-125px">Usure</th>
                                    <th class="min-w-125px">Ancienneté</th>
                                    <th class="min-w-125px">Prlv</th>
                                    <th class="min-w-125px">Résultat</th>
                                    <th class="text-end min-w-70px"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(auth()->user()->rentals() as $engine->engine)
                                    @if($engine->active == true)
                                        <tr>
                                            <td>
                                                <i class="fa-solid fa-eye fs-9" data-bs-toggle="popover" data-bs-placement="right" data-bs-custom-class="popover-inverse" data-bs-html="true" data-bs-content="<img src='{{ $engine->engine->image_url }}' alt='engine' />"></i>
                                            </td>
                                            <td>
                                                <img src="{{ asset('/storage/logos/logo_'.$engine->engine->type_train.'.svg') }}" class="w-40px" alt="">
                                                <span class="d-none">{{ $engine->engine->type_train }}</span>
                                            </td>
                                            <td>{{ $engine->engine->name }} - {{ $engine->number }}</td>
                                            <td>{{ $engine->user_hub->hub->gare->name }}</td>
                                            <td>{{ $engine->max_runtime_engine }} Km</td>
                                            <td>{{ $engine->calcUtilisationEngine() }} %</td>
                                            <td>{{ $engine->use_percent }} %</td>
                                            <td>{{ $engine->ancien }} %</td>
                                            <td>{{ $engine->user_rental->next_prlv->diffForHumans() }}</td>
                                            <td>{{ eur($engine->calcResultat()) }}</td>
                                            <td class="text-end">
                                                <a href="{{ route('engine.show', $engine->id) }}" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
                                                    <img src="{{ asset('/storage/icons/train.png') }}" class="w-25px" alt="">
                                                </a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let tableEngine = document.querySelector('#liste_engine')
        let tableEngineRent = document.querySelector('[data-sector="rent"]').querySelector('#liste_engine')
        let datatable = $(tableEngine).DataTable({
            info: !1,
            order: [],
            columnDefs: [{
                orderable: !1,
                targets: 0
            }, {
                orderable: !1,
                targets: 10
            }],
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
            'paging': false,
        })
        let datatableRent = $(tableEngineRent).DataTable({
            info: !1,
            order: [],
            columnDefs: [{
                orderable: !1,
                targets: 0
            }, {
                orderable: !1,
                targets: 10
            }],
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
            'paging': false,
        })

        document.querySelector('[data-table-filter="search"]').addEventListener('keyup', function (e) {
            datatable.search(e.target.value).draw()
        })
        document.querySelector('[data-sector="rent"]').querySelector('[data-table-filter="search"]').addEventListener('keyup', function (e) {
            datatable.search(e.target.value).draw()
        })

        let selectTypeTrain = $('[data-table-filter="type_train"]')
        let selectHub = $('[data-table-filter="hub"]')
        let optionDate = document.querySelectorAll('[data-table-filter="date"][name="sort"]')
        let optionUsed = document.querySelectorAll('[data-table-filter="used"][name="sort"]')
        let optionMark = document.querySelectorAll('[data-table-filter="mark"][name="sort"]')

        document.querySelector('[data-table-filter="filter"]').addEventListener('click', function () {
            const train = selectTypeTrain.val()
            const hub = selectHub.val()
            let c = ""
            optionDate.forEach(opt => {
                opt.checked && (c = opt.value), "all" === c && (c = "")
            });
            optionUsed.forEach(opt => {
                opt.checked && (c = opt.value), "all" === c && (c = "")
            });
            optionMark.forEach(opt => {
                opt.checked && (c = opt.value), "all" === c && (c = "")
            });

            const r = train + " "+ hub + " " + c;
            console.log(r)
            datatable.search(r).draw()
        })
        document.querySelector('[data-sector="rent"]').querySelector('[data-table-filter="filter"]').addEventListener('click', function () {
            const train = selectTypeTrain.val()
            const hub = selectHub.val()
            let c = ""
            optionDate.forEach(opt => {
                opt.checked && (c = opt.value), "all" === c && (c = "")
            });
            optionUsed.forEach(opt => {
                opt.checked && (c = opt.value), "all" === c && (c = "")
            });
            optionMark.forEach(opt => {
                opt.checked && (c = opt.value), "all" === c && (c = "")
            });

            const r = train + " "+ hub + " " + c;
            console.log(r)
            datatable.search(r).draw()
        })


    </script>
@endsection
