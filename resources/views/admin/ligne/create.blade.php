@extends("admin.template")

@section("css")

@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Nouvelle Ligne",
        'action' => true,
        'data' => [
            [
                'url' => back()->getTargetUrl(),
                'title' => "<i class='fa-solid fa-arrow-left me-2'></i> Retour",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <form action="{{ route('admin.ligne.store') }}" method="post">
        @csrf
        <div class="card shadow-lg">
            <div class="card-body">
                <div class="row mb-10">
                    <div class="col-md-10">
                        <label for="hub_id" class="control-label required">Hub affilié</label>
                        <select name="hub_id" id="hub_id" class="form-control selectpicker" data-live-search="true" required title="---- Selectionner un hub ----">
                            @foreach(\App\Models\Core\Hub::where('active', 1)->get() as $hub)
                                <option value="{{ $hub->id }}">{{ $hub->gare->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <x-form.input
                            name="nb_station"
                            required="true"
                            label="Nombre de station" />
                    </div>
                </div>
                <x-form.input
                    name="station_start_id"
                    required="true"
                    label="Station de départ" />
                <x-form.input
                    name="station_end_id"
                    required="true"
                    label="Station de départ" />
            </div>
            <div class="card-footer d-flex flex-end">
                <x-form.button class="btn-primary"/>
            </div>
        </div>
    </form>
@endsection

@section("js")

    <script type="text/javascript">

    </script>
@endsection
