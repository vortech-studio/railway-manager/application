<div class="d-flex flex-row min-h-500px">
    <div class="d-flex flex-column bg-color-eva w-100 rounded-3" style="overflow: hidden">
        <div class="d-flex flex-row mb-10 p-10">
            <div class="rounded-3 w-25 h-250px bg-gray-300">
                <div class="bg-gray-500 h-130px rounded-3 rounded-bottom-0 shadow-lg p-5">
                    <div class="d-flex flex-row justify-content-between align-items-center">
                        <div class="fw-bolder fs-2hx text-color-eva">{{ $planning->date_depart->format("H:i") }}</div>
                        <div class="rounded-3 bg-gray-300 p-3">
                            <div class="d-flex flex-row align-items-center">
                                {!! $planning->status_format !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-column-fluid flex-column w-100 h-auto p-5">
                    <div class="fw-bolder text-color-eva fs-2tx">{{ $planning->ligne->ligne->stationEnd->name }}</div>
                    <div class="bg-cyan-600 p-3">
                        <div class="text-color-eva fs-1">
                            {{ Str::upper($planning->engine->engine->type_train) }} {{ number_format($planning->number_travel, 0, '', ' ') }}
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-column-auto bg-gray-500 h-130px rounded-3 rounded-top-0 shadow-lg p-5">
                    <div class="d-flex flex-row justify-content-between align-items-center">
                        <span class="fs-3 text-color-eva">A Bord: </span>
                    </div>
                </div>
            </div>
            <div class="w-75 ms-20">
                <div class="d-flex flex-column align-items-start">
                    @foreach($planning->planning_stations as $station)
                        @if($station->ligneStation->gare->name == $planning->ligne->ligne->stationStart->name)
                            <div class="d-flex flex-row align-items-center mb-2">
                                <div class="min-w-100px">
                                    <div class="fw-semibold fs-2x text-white">{{ $station->departure_at->format("H:i") }}</div>
                                </div>
                                <div class="min-w-70px">
                                    <i class="fa-regular fa-circle text-white fs-3x"></i>
                                </div>
                                <div class="bg-color-eva-departure py-1 px-2 rounded">
                                    <div class="fw-semibold fs-3x text-white me-20">{{ $station->name }}</div>
                                </div>
                            </div>
                            @continue
                        @elseif($station->ligneStation->gare->name == $planning->ligne->ligne->stationEnd->name)
                            <div class="d-flex flex-row align-items-center mb-2">
                                <div class="min-w-100px">
                                    <div class="fw-semibold fs-2x text-white">{{ $station->departure_at->format("H:i") }}</div>
                                </div>
                                <div class="min-w-70px">
                                    <i class="fa-regular fa-circle text-white fs-3x"></i>
                                </div>
                                <div class="bg-color-eva-arrival py-1 px-2 rounded">
                                    <div class="fw-semibold fs-3x text-white me-20">{{ $station->name }}</div>
                                </div>
                            </div>
                            @break
                        @else
                            <div class="d-flex flex-row align-items-center mb-3">
                                <div class="min-w-100px">
                                    <div class="fw-semibold fs-2x text-white">{{ $station->departure_at->format("H:i") }}</div>
                                </div>
                                <div class="min-w-70px">
                                    <i class="fa-regular fa-circle text-white fs-3x"></i>
                                </div>
                                <div class="">
                                    <div class="fw-semibold fs-2x text-white">{{ $station->name }}</div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="flex flex-row-fluid m-0">
            @if($planning->engine->engine->type_train == 'tgv')
                <div class="d-flex flex-row align-items-center bg-blue-900 shadow-lg h-80px">
                    <div class="min-w-25 px-5">
                        <span class="text-white">N° Voiture</span>
                    </div>
                    <div class="d-flex flex-row justify-content-center align-items-center mx-auto">
                        @for($i=1; $i <= $planning->engine->engine->technical->nb_wagon; $i++)
                        <div class="bg-gray-300 rounded-3 shadow-lg w-60px h-45px p-2 d-flex justify-content-center align-items-center me-2">
                            @if($i == 1)

                            @elseif($i == $planning->engine->engine->technical->nb_wagon)

                            @elseif($i == $planning->engine->engine->technical->nb_wagon -5)
                                <i class="fa-solid fa-martini-glass fw-bolder text-color-eva fs-1"></i>
                            @else
                                <div class="fw-bolder text-color-eva fs-1">{{ $i }}</div>
                            @endif
                        </div>
                        @endfor
                    </div>
                </div>
                <div class="d-flex flex-row align-items-center bg-indigo-800 shadow-lg h-50px">
                    <div class="min-w-25 px-5">
                        <span class="text-white">Destination</span>
                    </div>
                    <div class="d-flex flex-row justify-content-center align-items-center mx-auto">
                        <div class="fw-semibold text-white fs-1">{{ $planning->ligne->ligne->stationEnd->name }}</div>
                    </div>
                </div>
            @endif
            @if($planning->logs()->count() != 0)
            <div class="d-flex flex-row align-items-center bg-orange-700 shadow-lg h-30px">
                <div class="d-flex flex-row justify-content-end align-items-end mx-auto animation-scroll-x"
                     style="animation-duration: 10.3319s; width: 100%;">
                    @foreach($planning->logs as $log)
                        <div class="fw-semibold text-white fs-1">{{ $log->message }}</div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="position-relative">
        <div class="position-absolute top-0 end-0 m-5">
            <div class="border border-color-eva-arrival rounded-2 p-5 w-150px">
                <span id="clock-hours" class="fs-1 text-white fw-semibold">23</span>
                <span class="animation-blink">
                            <span class="animation-blink-clock fs-1 text-white fw-semibold">:</span>
                        </span>
                <span id="clock-minutes" class="fs-1 text-white fw-semibold">32</span>
                <small id="clock-seconds" class="fs-3 ms-1 text-color-eva-arrival">37</small>
            </div>
        </div>
    </div>
</div>
