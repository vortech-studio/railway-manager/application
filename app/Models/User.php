<?php

namespace App\Models;

use App\Models\Core\Badge;
use App\Models\Core\Blog\Articles;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Core\Bonus;
use App\Models\Core\Research\SkillResearch;
use App\Models\User\Mailbox;
use App\Models\User\UserCard;
use App\Models\User\UserCardBonus;
use App\Models\User\UserCompany;
use App\Models\User\UserComposition;
use App\Models\User\UserDailyLogin;
use App\Models\User\UserDelivery;
use App\Models\User\UserEngine;
use App\Models\User\UserHub;
use App\Models\User\UserIncident;
use App\Models\User\UserLigne;
use App\Models\User\UserPlanning;
use App\Models\User\UserPlanningConstructor;
use App\Models\User\UserRental;
use App\Models\User\UserSetting;
use App\Models\User\UserSocial;
use App\Models\User\UserSubscription;
use App\Models\User\UserTechnicentre;
use App\Models\User\UserTuto;
use BeyondCode\Vouchers\Traits\CanRedeemVouchers;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use LevelUp\Experience\Concerns\GiveExperience;
use LevelUp\Experience\Concerns\HasAchievements;
use LevelUp\Experience\Concerns\HasStreaks;
use NotificationChannels\WebPush\HasPushSubscriptions;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, GiveExperience, HasAchievements, HasStreaks, CanRedeemVouchers, HasPushSubscriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'admin',
        'avatar',
        'name_secretary',
        'avatar_secretary',
        'name_company',
        'logo_company',
        'desc_company',
        'premium',
        'argent',
        'tpoint',
        'research',
        'name_conseiller',
        'installed',
        'automated_planning'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'admin' => 'boolean',
        'premium' => 'boolean',
        'argent' => 'decimal:2',
        'tpoint' => 'decimal:2',
        'research' => 'decimal:2',
        'last_login' => 'datetime',
    ];

    protected $appends = ['status_account', 'avatar_src', 'is_premium'];

    public function scopeAdmin($query)
    {
        return $query->where('admin', 1);
    }

    public function getIsPremiumAttribute()
    {
        return $this->premium;
    }

    public function articles(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Articles::class);
    }

    public function setting()
    {
        return $this->hasOne(UserSetting::class);
    }

    public function social()
    {
        return $this->hasOne(UserSocial::class);
    }

    public function company()
    {
        return $this->hasOne(UserCompany::class);
    }

    public function hubs()
    {
        return $this->hasMany(UserHub::class);
    }

    public function engines()
    {
        return $this->hasMany(UserEngine::class);
    }

    public function compositions()
    {
        return $this->hasMany(UserComposition::class);
    }

    public function lignes()
    {
        return $this->hasMany(UserLigne::class);
    }

    public function messages()
    {
        return $this->hasMany(Mailbox::class);
    }

    public function subscription()
    {
        return $this->hasOne(UserSubscription::class);
    }

    public function card_bonus()
    {
        return $this->hasOne(UserCardBonus::class);
    }

    public function cards()
    {
        return $this->hasMany(UserCard::class);
    }

    public function livraisons()
    {
        return $this->hasMany(UserDelivery::class);
    }

    public function plannings()
    {
        return $this->hasMany(UserPlanning::class);
    }

    public function planning_constructors()
    {
        return $this->hasMany(UserPlanningConstructor::class);
    }

    public function rentals()
    {
        return $this->hasMany(UserRental::class);
    }

    public function incidents()
    {
        return $this->hasMany(UserIncident::class);
    }

    public function technicentres()
    {
        return $this->hasMany(UserTechnicentre::class);
    }

    public function step()
    {
        return $this->hasOne(UserTuto::class);
    }

    public function badges()
    {
        return $this->belongsToMany(Badge::class, 'user_badges');
    }

    public function dailys()
    {
        return $this->hasMany(UserDailyLogin::class);
    }

    public function bonuses()
    {
        return $this->belongsToMany(Bonus::class, 'bonus_user', 'user_id', 'bonus_id')
            ->withTimestamps();
    }

    public function skills()
    {
        return $this->belongsToMany(SkillResearch::class, 'user_skill')->withPivot('level');
    }

    public function getStatusAccountAttribute()
    {
        if (! empty($this->email_verified_at)) {
            return '<span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2">Vérifier</span>';
        } else {
            return '<a href=""><span class="badge badge-light-danger fw-bold fs-8 px-2 py-1 ms-2">Non Vérifier</span></a>';
        }
    }

    public function getAvatarSrcAttribute()
    {
        if(! empty($this->avatar)) {
            return '/storage/avatar/users/'.$this->avatar;
        } else {
            return '/storage/avatar/users/default.png';
        }
    }

    public function notificationTypeFormat($type)
    {
        return match ($type) {
            'info' => 'info',
            'success' => 'success',
            'warning' => 'warning',
            'danger' => 'danger',
        };

    }
}
