<?php

namespace App\Http\Controllers\Game\Research;

use App\Http\Controllers\Controller;
use App\Models\Core\Research\SectorResearch;

class ResearchController extends Controller
{
    public function index()
    {
        seo()->title(config('app.name') . ' - R&D');
        return view('game.research.index');
    }
}
