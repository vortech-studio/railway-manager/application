@extends("admin.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Gestion de la ligne: ".$ligne->stationStart->name." - ".$ligne->stationEnd->name,
        'action' => true,
        'data' => [
            [
                'url' => back()->getTargetUrl(),
                'title' => "<i class='fa-solid fa-arrow-left me-2'></i> Retour",
                'class' => "primary"
            ],
            [
                'url' => back()->getTargetUrl(),
                'title' => "Calculer la distance de la ligne",
                'class' => "warning btnCalculateDistance"
            ],
            [
                'url' => back()->getTargetUrl(),
                'title' => "Calculer le prix de la ligne",
                'class' => "success btnCalculatePrice"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center rounded-3 bg-white p-5">
        <div class="d-flex flex-row justify-content-between align-items-center">
            <div class="d-flex flex-column">
                <div class="fw-bolder fs-3x">{{ $ligne->stationStart->name }} - {{ $ligne->stationEnd->name }}</div>
                <div class="d-flex flex-row mt-3">
                    <div class="card card-flush bg-primary w-200px  me-3">
                        <div class="card-body">
                            <div class="d-flex flex-column">
                                <div class="fw-bolder fs-3x">{{ $ligne->nb_station }}</div>
                                <div class="fw-bold fs-6 text-gray-400">Stations</div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-flush bg-info w-200px  me-3">
                        <div class="card-body">
                            <div class="d-flex flex-column">
                                <div class="fw-bolder fs-3x">{{ $ligne->distance }}</div>
                                <div class="fw-bold fs-6 text-gray-400">Km de trajet</div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-flush bg-warning w-200px  text-inverse me-3">
                        <div class="card-body">
                            <div class="d-flex flex-column">
                                <div class="fw-bolder fs-3x">{{ convertMinuteToHours($ligne->time_min) }}</div>
                                <div class="fw-bold fs-6">Temps de trajet</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="fs-2tx fw-bolder">Prix: </span> <span class="fs-2x fw-bolder text-success">{{ eur($ligne->price) }}</span>
    </div>
    <div class="card shadow-lg my-10 w-50 align-items-center mx-lg-20">
        <div class="card-body align-items-center">
            <div class="d-flex flex-column">
                @foreach($ligne->stations as $station)
                    <div class="d-flex flex-row align-items-center mb-2">
                        @if($ligne->stationStart->name == $station->gare->name || $ligne->stationEnd->name == $station->gare->name)
                            <img src="/storage/icons/hub.png" class="w-45px me-3" alt="" />
                            <span class="fs-1 fw-bold {{ $ligne->stationStart->name == $station->gare->name ? 'text-success' : '' }} {{ $ligne->stationEnd->name == $station->gare->name ? 'text-danger' : '' }}">{{ $station->gare->name }}</span>
                        @else
                            <img src="/storage/icons/train-passenger.png" class="w-20px me-3" alt="" />
                                <span class="fs-3 fw-bold">{{ $station->gare->name }}</span>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row mb-10">
        <div class="col-lg-8 col-sm-12">
            <div class="card shadow-lg mb-10">
                <div class="card-header">
                    <h3 class="card-title">Liste des Arrêts</h3>
                    <div class="card-toolbar">
                        <a href="#add-arret" class="btn btn-sm btn-primary" data-bs-toggle="modal">
                            <i class="fa-solid fa-plus me-2"></i> Ajouter un arrêt
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <tr class="bg-gray-800 p-5">
                                <th class="w-75">Arret</th>
                                <th>Temps</th>
                                <th>Actions</th>
                            </tr>
                            <tbody>
                                @foreach($ligne->stations as $station)
                                    <tr class="@if($station->gare->name == $ligne->stationStart->name) bg-success @endif @if($station->gare->name == $ligne->stationEnd->name) bg-danger @endif">
                                        <td>{{ $station->gare->name }}</td>
                                        <td>{{ \Carbon\CarbonInterval::minute($station->time)->forHumans()  }}</td>
                                        @if($station->gare->name != $ligne->stationStart->name && $station->gare->name != $ligne->stationEnd->name)
                                            <td>
                                                <x-base.button
                                                    class="btn btn-sm btn-icon btn-bg-light btn-active-color-danger btnDeleteStation"
                                                    text="<i class='fa-solid fa-trash'></i>"
                                                    text-indicator=""
                                                    :datas="[[
                                                        'name' => 'station',
                                                        'value' => $station->id
                                                    ]]" />
                                            </td>
                                        @else
                                        <td></td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-12">
            <div class="card shadow-lg mb-10">
                <div class="card-header">
                    <h3 class="card-title">Hub Requis</h3>
                    <div class="card-toolbar">
                        <a href="#add-requirement" class="btn btn-sm btn-primary" data-bs-toggle="modal">
                            <i class="fa-solid fa-plus me-2"></i> Ajouter un hub
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="d-flex flex-column">
                        @foreach($ligne->requirements as $requirement)
                            <div class="d-flex flex-row justify-content-between align-items-center py-2">
                                <div>
                                    <span class="bullet w-25px h-15px me-5"></span>
                                    <span class="fs-1">{{ $requirement->hub->gare->name }}</span>
                                </div>
                                <x-base.button
                                    class="btn btn-sm btn-icon btn-bg-light btn-active-color-danger btnDeleteHub"
                                    text="<i class='fa-solid fa-times fs-2'></i>"
                                    text-indicator=""
                                    :datas="[[
                                        'name' => 'requirement',
                                        'value' => $requirement->id
                                    ]]" />
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="add-arret" tabindex="-1" role="dialog" aria-labelledby="add-arret" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Ajout d'un arret</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form action="{{ route('admin.ligne.station.store', $ligne->id) }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-9 col-sm-12">
                                <label for="gares_id" class="control-label required">Gare desservi</label>
                                <select name="gares_id" id="gares_id" class="form-control selectpicker" data-live-search="true" required title="---- Selectionner une gare ----">
                                    @foreach(\App\Models\Core\Gares::all() as $gare)
                                        <option value="{{ $gare->id }}">{{ $gare->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Fermé</button>
                        <x-form.button />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-requirement" tabindex="-1" role="dialog" aria-labelledby="add-requirement" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Ajout d'un hub requis</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form action="{{ route('admin.ligne.station.storeRequirement', $ligne->id) }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <label for="hub_id" class="control-label required">Hub requis</label>
                                <select name="hub_id" id="hub_id" class="form-control selectpicker" data-live-search="true" required title="---- Selectionner une gare ----">
                                    @foreach(\App\Models\Core\Hub::where('active', true)->get() as $hub)
                                        <option value="{{ $hub->id }}">{{ $hub->gare->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Fermé</button>
                        <x-form.button />
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        document.querySelectorAll('.btnDeleteHub').forEach(btn => {
            btn.addEventListener('click', e => {
                e.preventDefault()
                btn.setAttribute('data-kt-indicator', 'on')

                $.ajax({
                    url: '/api/core/ligne/{{ $ligne->id }}/requirement/'+btn.dataset.requirement,
                    method: 'DELETE',
                    success: data => {
                        btn.parentNode.classList.add('d-none')
                        toastr.success(data.message)
                    }
                })
            })
        })

        document.querySelectorAll('.btnDeleteStation').forEach(btn => {
            btn.addEventListener('click', e => {
                e.preventDefault()
                btn.setAttribute('data-kt-indicator', 'on')

                $.ajax({
                    url: '/api/core/ligne/{{ $ligne->id }}/station/'+btn.dataset.station,
                    method: 'DELETE',
                    success: data => {
                        btn.parentNode.parentNode.classList.add('d-none')
                        toastr.success(data.message)
                    }
                })
            })
        })

        document.querySelector('.btnCalculateDistance').addEventListener('click', e => {
            e.preventDefault()
            e.target.setAttribute('data-kt-indicator', 'on')

            $.ajax({
                url: '/api/core/ligne/{{ $ligne->id }}/calculateDistance',
                method: 'GET',
                success: data => {
                    console.log(data)
                    toastr.success("La distance à été calculer")
                }
            })
        })

        document.querySelector('.btnCalculatePrice').addEventListener('click', e => {
            e.preventDefault()
            e.target.setAttribute('data-kt-indicator', 'on')

            $.ajax({
                url: '/api/core/ligne/{{ $ligne->id }}/calculatePrice',
                method: 'GET',
                success: data => {
                    console.log(data)
                    toastr.success("Le prix à été calculer")
                }
            })
        })
    </script>
@endsection
