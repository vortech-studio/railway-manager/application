<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Imports\GareImport;
use App\Imports\GareTransportationImport;
use App\Jobs\Console\InstallGare;
use App\Models\Core\GareEquipement;
use App\Models\Core\Gares;
use App\Models\Core\GareTransportation;
use Illuminate\Support\Facades\Process;
use Maatwebsite\Excel\Excel;

class ImportGareController extends Controller
{
    public function __invoke()
    {
        $file = fopen(public_path('/storage/gares.json'), 'r');
        $gares = json_decode(fread($file, filesize(public_path('/storage/gares.json'))), true);
        fclose($file);

        foreach ($gares as $gare) {
            if(Gares::find($gare['id'])) {
                Gares::find($gare['id'])->update([
                    "name" => $gare['name'],
                    "code_uic" => $gare['code_uic'],
                    "type_gare" => $gare['type_gare'],
                    "latitude" => $gare['latitude'],
                    "longitude" => $gare['longitude'],
                    "region" => $gare['region'],
                    "pays" => $gare['pays'],
                    "superficie_infra" => $gare['superficie_infra'],
                    "superficie_quai" => $gare['superficie_quai'],
                    "long_quai" => $gare['long_quai'],
                    "nb_quai" => $gare['nb_quai'],
                    "commerce" => $gare['commerce'],
                    "pub" => $gare['pub'],
                    "parking" => $gare['parking'],
                    "frequentation" => $gare['frequentation'],
                    "nb_habitant_city" => $gare['nb_habitant_city'],
                    "time_day_work" => $gare['time_day_work'],
                    "created_at" => $gare['created_at'],
                    "updated_at" => $gare['updated_at'],
                ]);
            } else {
                Gares::create([
                    "id" => $gare['id'],
                    "name" => $gare['name'],
                    "code_uic" => $gare['code_uic'],
                    "type_gare" => $gare['type_gare'],
                    "latitude" => $gare['latitude'],
                    "longitude" => $gare['longitude'],
                    "region" => $gare['region'],
                    "pays" => $gare['pays'],
                    "superficie_infra" => $gare['superficie_infra'],
                    "superficie_quai" => $gare['superficie_quai'],
                    "long_quai" => $gare['long_quai'],
                    "nb_quai" => $gare['nb_quai'],
                    "commerce" => $gare['commerce'],
                    "pub" => $gare['pub'],
                    "parking" => $gare['parking'],
                    "frequentation" => $gare['frequentation'],
                    "nb_habitant_city" => $gare['nb_habitant_city'],
                    "time_day_work" => $gare['time_day_work'],
                    "created_at" => $gare['created_at'],
                    "updated_at" => $gare['updated_at'],
                ]);
            }
        }

        $this->importTransportation();
        $this->importEquipement();

        return response()->json();
    }

    private function importGares()
    {
        \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\GareImport, '/storage/gares.csv', 'public', Excel::CSV);
    }

    private function importTransportation()
    {
        $file = fopen(public_path('/storage/gares_transportation.json'), 'r');
        $transportations = json_decode(fread($file, filesize(public_path('/storage/gares_transportation.json'))), true);
        fclose($file);

        foreach ($transportations as $transportation) {
            if(GareTransportation::find($transportation['id'])) {
                GareTransportation::find($transportation['id'])->update([
                    "type" => $transportation['type'],
                    "gares_id" => $transportation['gares_id'],
                    "created_at" => $transportation['created_at'],
                    "updated_at" => $transportation['updated_at'],
                ]);
            } else {
                GareTransportation::create([
                    "id" => $transportation['id'],
                    "type" => $transportation['type'],
                    "gares_id" => $transportation['gares_id'],
                    "created_at" => $transportation['created_at'],
                    "updated_at" => $transportation['updated_at'],
                ]);
            }
        }

        return response()->json();
    }

    private function importEquipement()
    {
        $file = fopen(public_path('/storage/gares_equipements.json'), 'r');
        $equipements = json_decode(fread($file, filesize(public_path('/storage/gares_equipements.json'))), true);
        fclose($file);

        foreach ($equipements as $equipement) {
            if(GareEquipement::find($equipement['id'])) {
                GareEquipement::find($equipement['id'])->update([
                    "code_uic" => $equipement['code_uic'],
                    "type_equipement" => $equipement['type_equipement'],
                    "gares_id" => $equipement['gares_id'],
                ]);
            } else {
                GareEquipement::create([
                    "id" => $equipement['id'],
                    "code_uic" => $equipement['code_uic'],
                    "type_equipement" => $equipement['type_equipement'],
                    "gares_id" => $equipement['gares_id'],
                ]);
            }
        }

        return response()->json();
    }
}
