@extends("admin.template")

@section("css")

@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Editer le hub: {$hub->gare->name}",
        'action' => true,
        'data' => [
            [
                'url' => back()->getTargetUrl(),
                'title' => "<i class='fa-solid fa-arrow-left me-2'></i> Retour",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="card shadow-lg">
        <form action="{{ route('admin.hub.update', $hub->id) }}" method="post">
            <div class="card-body">
                @csrf
                @method("PUT")
                <label for="gares_id" class="form-label">Gare affilié</label>
                <select name="gares_id" id="gares_id" class="form-control selectpicker" data-live-search="true">
                    <option value="{{ $hub->gare->id }}">{{ $hub->gare->name }}</option>
                    @foreach($gares as $id=>$name)
                        <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
                <span class="text-danger info d-none"><i class="fa-solid fa-exclamation-triangle text-danger me-2"></i> La gare est déjà indiquer comme un hub</span>

            </div>
            <div class="card-footer d-flex flex-end">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>
        </form>
    </div>
@endsection

@section("js")

    <script type="text/javascript">

    </script>
@endsection
