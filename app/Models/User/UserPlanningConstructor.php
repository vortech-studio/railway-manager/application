<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserPlanningConstructor extends Model
{
    public $timestamps = false;

    protected $casts = [
        'start_at' => 'datetime',
        'end_at' => 'datetime',
    ];
    protected $guarded = [];
    protected $appends = ['day_of_week_text'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function user_engine(): BelongsTo
    {
        return $this->belongsTo(UserEngine::class, 'user_engine_id');
    }

    public function getDayOfWeekTextAttribute()
    {
        foreach (json_decode($this->dayOfWeek) as $item) {

            return match ($item) {
                "1" => "Lundi",
                "2" => "Mardi",
                "3" => "Mercredi",
                "4" => "Jeudi",
                "5" => "Vendredi",
                "6" => "Samedi",
                "0" => "Dimanche",
            };
        }
    }
}
