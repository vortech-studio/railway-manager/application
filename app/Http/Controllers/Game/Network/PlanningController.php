<?php

namespace App\Http\Controllers\Game\Network;

use App\Http\Controllers\Controller;
use App\Models\Core\Ligne;
use App\Models\Core\LigneStation;
use App\Models\User\UserEngine;
use App\Models\User\UserPlanning;
use App\Models\User\UserPlanningConstructor;
use App\Models\User\UserPlanningStation;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PlanningController extends Controller
{
    public function index()
    {
        return view('game.network.planning.index');
    }

    public function editing()
    {
        return view('game.network.planning.editing', [
            "engines" => UserEngine::where('user_id', auth()->user()->id)->get(),
            "plannings" => UserPlanning::where('user_id', auth()->user()->id)->get(),
        ]);
    }

    public function store(Request $request)
    {
        $engine = UserEngine::find($request->get('engines_id'));
        $hour_ex = explode(':', $request->get('date_depart'));
        $heure_depart = Carbon::createFromTime($hour_ex[0], $hour_ex[1], 0);
        $heure_arrived = Carbon::createFromTime($hour_ex[0], $hour_ex[1], 0)->addMinutes($engine->user_ligne->ligne->time_min);

        if ($engine->planning_constructors()->exists()) {
            $first = $engine->planning_constructors()->first();
            $last = $engine->planning_constructors()->orderBy('id', 'desc')->first();

            if ($heure_depart >= $first->start_at && $heure_depart <= $last->end_at || in_array($request->get('day'), json_decode($first->dayOfWeek))) {
                toastr()->error('Le planning est déjà disponible');
                return redirect()->back();
            }
        }

        $this->construct($heure_depart, $heure_arrived, $request, $engine);

        toastr()->success('Planning enregistré avec succès');
        return redirect()->back();
    }

    private function construct($heure_depart, $heure_arrived, $request, $engine)
    {
        return UserPlanningConstructor::create([
            "start_at" => $heure_depart,
            "end_at" => $heure_arrived,
            "dayOfWeek" => json_encode($request->get('day')),
            "user_id" => auth()->user()->id,
            "user_engine_id" => $engine->id,
            "repeat" => $request->has('repeat'),
            "repeat_end_at" => $this->calcEndAtFromWeek($request)
        ]);
    }

    private function calcEndAtFromWeek($request)
    {
        return match ($request->get('repeat')) {
            "hebdo" => now()->startOfWeek()->addWeeks($request->get('number_repeat'))->endOfWeek(),
            "mensuel" => now()->startOfWeek()->addMonths($request->get('number_repeat'))->endOfWeek(),
            "trim" => now()->startOfWeek()->addMonths($request->get('number_repeat') * 3)->endOfWeek(),
            "sem" => now()->startOfWeek()->addMonths($request->get('number_repeat') * 6)->endOfWeek(),
            "annual" => now()->startOfWeek()->addYears($request->get('number_repeat'))->endOfWeek(),
            default => null
        };
    }
}
