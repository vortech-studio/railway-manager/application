<?php

namespace App\Jobs\Game;

use App\Models\User\UserDelivery;
use App\Models\User\UserHubSkill;
use App\Services\JobService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class HubSkillJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserHubSkill $userHubSkill, public UserDelivery $userDelivery)
    {
    }

    public function handle(): void
    {
        $this->executeAction();
    }

    private function executeAction()
    {
        match($this->userHubSkill->action) {
            "optimiseTime" => $this->optimiseTime(),
            "optimiseInfra" => $this->optimiseInfra()
        };

        $this->userHubSkill->level_actual += 1;
        $this->userHubSkill->save();

        $this->userHubSkill->user_hub->user->addPoints(100);

        $this->userDelivery->delete();
    }

    private function setActionValueFromCompetence()
    {
        return $this->userHubSkill->calcActionValueFromLevel($this->userHubSkill->level_actual + 1);
    }

    private function optimiseTime()
    {

    }

    private function optimiseInfra()
    {

    }
}
