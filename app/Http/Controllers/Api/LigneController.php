<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\Hub;
use App\Models\Core\Ligne;
use App\Models\Core\LigneRequirement;
use App\Models\Core\LigneStation;
use App\Services\Mapping\OpenRouteService;
use Illuminate\Support\Facades\Schema;

class LigneController extends Controller
{

    public function info($ligne_id)
    {
        $ligne = Ligne::findOrFail($ligne_id)
            ->load('stationStart', 'stationEnd', 'requirements', 'hub');

        $stations = LigneStation::where('ligne_id', $ligne_id)->get()->load('gare');
        $hub = Hub::find($ligne->hub_id)->load('gare');

        $stations_cos = [];
        foreach ($stations as $station) {
            $stations_cos[] = [
                $station->gare->latitude,
                $station->gare->longitude
            ];
        }

        return response()->json([
            "ligne" => $ligne,
            "stations" => $stations,
            "hub" => $hub,
            "stations_cos" => $stations_cos,
        ]);
    }
    public function calculateDistance($ligne_id)
    {
        $ligne = Ligne::findOrFail($ligne_id);

        $data = $ligne->stations()->sum('distance');

        $ligne->distance = $data;
        $ligne->save();

        return response()->json([
            'message' => "La distance a bien été calculée",
            'data' => $data,
        ]);
    }

    public function calculatePrice($ligne_id)
    {
        $ligne = Ligne::findOrFail($ligne_id);

        $ligne->price = Ligne::calculPrice($ligne);
        $ligne->save();

        return response()->json([
            'message' => "Le prix a bien été calculé",
            'data' => $ligne->price,
        ]);
    }

    public function export()
    {
        $lignes = Ligne::all();
        $file = fopen(public_path('/storage/lignes.json'), 'w');

        fwrite($file, json_encode($lignes));

        fclose($file);

        $this->exportRequirement();
        $this->exportStations();

        return response()->json();
    }

    public function import()
    {
        $file = fopen(public_path('/storage/lignes.json'), 'r');
        $lignes = json_decode(fread($file, filesize(public_path('/storage/lignes.json'))), true);
        fclose($file);

        foreach ($lignes as $ligne) {
            if(Ligne::find($ligne['id'])) {
                Ligne::find($ligne['id'])->update([
                    "nb_station" => $ligne['nb_station'],
                    "price" => $ligne['price'],
                    "distance" => $ligne['distance'],
                    "time_min" => $ligne['time_min'],
                    "station_start_id" => $ligne['station_start_id'],
                    "station_end_id" => $ligne['station_end_id'],
                    "active" => $ligne['active'],
                    "hub_id" => $ligne['hub_id'],
                    "type_ligne" => $ligne['type_ligne'],
                ]);
            } else {
                Ligne::create([
                    "id" => $ligne['id'],
                    "nb_station" => $ligne['nb_station'],
                    "price" => $ligne['price'],
                    "distance" => $ligne['distance'],
                    "time_min" => $ligne['time_min'],
                    "station_start_id" => $ligne['station_start_id'],
                    "station_end_id" => $ligne['station_end_id'],
                    "active" => $ligne['active'],
                    "hub_id" => $ligne['hub_id'],
                    "type_ligne" => $ligne['type_ligne'],
                ]);
            }

            $this->importRequirement();
            $this->importStations();

            return response()->json();
        }
    }

    private function importRequirement()
    {
        $file = fopen(public_path('/storage/lignes_requirements.json'), 'r');
        $requirements = json_decode(fread($file, filesize(public_path('/storage/lignes_requirements.json'))), true);
        fclose($file);
        Schema::disableForeignKeyConstraints();
        foreach ($requirements as $requirement) {
            if(LigneRequirement::find($requirement['id'])) {
                LigneRequirement::find($requirement['id'])->update([
                    "ligne_id" => $requirement['ligne_id'],
                    "hub_id" => $requirement['hub_id'],
                ]);
            } else {
                LigneRequirement::create([
                    "id" => $requirement['id'],
                    "ligne_id" => $requirement['ligne_id'],
                    "hub_id" => $requirement['hub_id'],
                ]);
            }
        }
        Schema::enableForeignKeyConstraints();
    }

    private function importStations()
    {
        $file = fopen(public_path('/storage/lignes_stations.json'), 'r');
        $stations = json_decode(fread($file, filesize(public_path('/storage/lignes_stations.json'))), true);
        fclose($file);

        Schema::disableForeignKeyConstraints();
        foreach ($stations as $station) {
            if(LigneStation::find($station['id'])) {
                LigneStation::find($station['id'])->update([
                    "time" => $station['time'],
                    "gares_id" => $station['gares_id'],
                    "ligne_id" => $station['ligne_id'],
                ]);
            } else {
                LigneStation::create([
                    "id" => $station['id'],
                    "time" => $station['time'],
                    "gares_id" => $station['gares_id'],
                    "ligne_id" => $station['ligne_id'],
                ]);
            }
        }
        Schema::enableForeignKeyConstraints();
    }

    private function exportRequirement()
    {
        $requirements = LigneRequirement::all();
        $file = fopen(public_path('/storage/lignes_requirements.json'), 'w');

        fwrite($file, json_encode($requirements));

        fclose($file);

        return;
    }

    private function exportStations()
    {
        $stations = LigneStation::all();
        $file = fopen(public_path('/storage/lignes_stations.json'), 'w');

        fwrite($file, json_encode($stations));

        fclose($file);

        return;
    }
}
