<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLigneTarifsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_ligne_tarifs', function (Blueprint $table) {
            $table->id();
            $table->timestamp('date_tarif');
            $table->enum('type', ['first', 'second', 'unique']);
            $table->integer('demande');
            $table->integer('offre');
            $table->decimal('price');

            $table->foreignId('user_ligne_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_ligne_tarifs');
    }
}
