<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Core\Achievement;
use App\Models\Core\Badge;
use App\Models\Core\BadgeReward;
use App\Models\Core\CardHolder;
use App\Models\Core\CardHolderCategory;
use App\Models\Core\Engine;
use App\Models\Core\Hub;
use App\Models\Core\Mouvement;
use App\Models\Core\Research\CategoryResearch;
use App\Models\Core\Research\SectorResearch;
use App\Models\Core\Research\SkillResearch;
use App\Models\Core\Reward;
use App\Models\Core\Setting;
use App\Models\Core\TypeIncident;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use LevelUp\Experience\Models\Level;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->settings();
        $this->typesIncident();
        $this->achievements();
        $this->levels();
        $this->cardCategory();
        $this->cardCommune();
        $this->cardRemarquable();
        $this->cardPrestige();
        $this->cardLengendaire();
        $this->badge();
    }

    protected function settings()
    {
        Setting::create([
            'name' => 'price_electricity',
            'value' => '0.15',
        ])->create([
            'name' => 'price_diesel',
            'value' => '1.5',
        ])->create([
            'name' => 'price_parking',
            'value' => '0.5',
        ])->create([
            'name' => 'version',
            'value' => '2023.1-e3716766',
        ])->create([
            "name" => "start_argent",
            "value"  => "3000000",
        ])->create([
            "name" => "start_tpoint",
            "value"  => "300",
        ])->create([
            "name" => "start_research",
            "value"  => "0",
        ])->create([
            "name" => "price_kilometer",
            "value"  => "1.89",
        ]);
    }

    private function typesIncident()
    {
        TypeIncident::create([
            "designation" => "Collision contre un animal",
            "origine" => "materiels",
            "severity" => 3
        ])->create([
            "designation" => "Collision contre un obstacle",
            "origine" => "materiels",
            "severity" => 3
        ])->create([
            "designation" => "Dépassement de vitesse autorisée",
            "origine" => "materiels",
            "severity" => 2
        ])->create([
            "designation" => "Franchissement d'un signal carré",
            "origine" => "materiels",
            "severity" => 2
        ])->create([
            "designation" => "Déraillement",
            "origine" => "materiels",
            "severity" => 3
        ])->create([
            "designation" => "Pantographes arrachés",
            "origine" => "materiels",
            "severity" => 2
        ])->create([
            "designation" => "Pantographe sortie de caténaire",
            "origine" => "materiels",
            "severity" => 2
        ])->create([
            "designation" => "Problème avec les feux de position",
            "origine" => "materiels",
            "severity" => 1
        ])->create([
            "designation" => "Attelage inopérant ou défectueux",
            "origine" => "materiels",
            "severity" => 1
        ])->create([
            "designation" => "Bloc de freinage défectueux",
            "origine" => "materiels",
            "severity" => 3
        ])->create([
            "designation" => "Bloc moteur défectueux",
            "origine" => "materiels",
            "severity" => 2
        ])->create([
            "designation" => "Bloc commun défectueux",
            "origine" => "materiels",
            "severity" => 2
        ])->create([
            "designation" => "Bogie moteur défectueux",
            "origine" => "materiels",
            "severity" => 3
        ])->create([
            "designation" => "Bogie secondaire en défaillance",
            "origine" => "materiels",
            "severity" => 3
        ])->create([
            "designation" => "Compresseur défectueux",
            "origine" => "materiels",
            "severity" => 3
        ])->create([
            "designation" => "Alimentation des lumières en voiture défectueuse",
            "origine" => "materiels",
            "severity" => 1
        ])->create([
            "designation" => "Ventilation en voiture défectueuse",
            "origine" => "materiels",
            "severity" => 1
        ])->create([
            "designation" => "Chauffage en voiture défectueuse",
            "origine" => "materiels",
            "severity" => 1
        ])->create([
            "designation" => "Incendie à bord du train",
            "origine" => "materiels",
            "severity" => 3
        ]);

        TypeIncident::create([
            "designation" => "Déformation de la voie",
            "origine" => "infrastructures",
            "severity" => 1
        ])->create([
            "designation" => "Défaillance d'un aiguillage",
            "origine" => "infrastructures",
            "severity" => 2
        ])->create([
            "designation" => "Défaillance d'un signal carré",
            "origine" => "infrastructures",
            "severity" => 3
        ])->create([
            "designation" => "Panne générale de courant",
            "origine" => "infrastructures",
            "severity" => 1
        ])->create([
            "designation" => "Caténaire arrachée, défectueuse ou sortie de son logement",
            "origine" => "infrastructures",
            "severity" => 3
        ])->create([
            "designation" => "Défaillance d'un passage à niveau",
            "origine" => "infrastructures",
            "severity" => 3
        ])->create([
            "designation" => "Défaillance d'un système de sécurité (ex: TIV, KVB, etc...)",
            "origine" => "infrastructures",
            "severity" => 3
        ])->create([
            "designation" => "Défaillance d'un système de signalisation (ex: BAL, BAPR, etc...)",
            "origine" => "infrastructures",
            "severity" => 3
        ])->create([
            "designation" => "Défaillance d'un système de contrôle de vitesse (ex: TVM, ETCS, etc...)",
            "origine" => "infrastructures",
            "severity" => 2
        ]);

        TypeIncident::create([
            "designation" => "Accident de personne",
            "origine" => "humains",
            "severity" => 3
        ])->create([
            "designation" => "Agression à bord du train",
            "origine" => "humains",
            "severity" => 2
        ])->create([
            "designation" => "Activation abusive du signal d'alarme",
            "origine" => "humains",
            "severity" => 1
        ])->create([
            "designation" => "Oubli d'ouverture / fermeture des portes",
            "origine" => "humains",
            "severity" => 1
        ]);
    }

    private function achievements()
    {
        Achievement::create([
            "name" => "Premium",
            "description" => "Acheter un abonnement premium",
            "is_secret" => false,
            "image" => "1.png"
        ]);

        Reward::create([
            "type" => "argent",
            "value" => "1000000",
            "achievement_id" => 1
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "450",
            "achievement_id" => 1
        ]);
        Reward::create([
            "type" => "tpoint",
            "value" => "20",
            "achievement_id" => 1
        ]);

        Achievement::create([
            "name" => "Mécano",
            "description" => "Réparer 10 trains",
            "is_secret" => false,
            "image" => "2.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "50000",
            "achievement_id" => 2
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 2
        ]);

        Achievement::create([
            "name" => "Mécano confirmé",
            "description" => "Réparer 100 trains",
            "is_secret" => false,
            "image" => "3.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "100000",
            "achievement_id" => 3
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 2
        ]);

        Achievement::create([
            "name" => "Mécano expert",
            "description" => "Réparer 1000 trains",
            "is_secret" => false,
            "image" => "4.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "150000",
            "achievement_id" => 4
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 4
        ]);

        Achievement::create([
            "name" => "Mécano légendaire",
            "description" => "Réparer 10000 trains",
            "is_secret" => false,
            "image" => "5.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "200000",
            "achievement_id" => 5
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 5
        ]);

        Achievement::create([
            "name" => "Mécano divin",
            "description" => "Réparer 100000 trains",
            "is_secret" => false,
            "image" => "6.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "250000",
            "achievement_id" => 6
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 6
        ]);

        Achievement::create([
            "name" => "Mécano ultime",
            "description" => "Réparer 1000000 trains",
            "is_secret" => false,
            "image" => "7.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "300000",
            "achievement_id" => 7
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 7
        ]);

        Achievement::create([
            "name" => "Chef de gare",
            "description" => "Achetez votre premier Hub",
            "is_secret" => false,
            "image" => "8.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "1000000",
            "achievement_id" => 8
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "450",
            "achievement_id" => 8
        ]);
        Reward::create([
            "type" => "tpoint",
            "value" => "20",
            "achievement_id" => 8
        ]);


        Achievement::create([
            "name" => "Chef de gare confirmé",
            "description" => "Achetez 10 Hubs",
            "is_secret" => false,
            "image" => "9.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "150000",
            "achievement_id" => 9
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "150",
            "achievement_id" => 9
        ]);

        Achievement::create([
            "name" => "Chef de gare expert",
            "description" => "Achetez 15 Hubs",
            "is_secret" => false,
            "image" => "10.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "200000",
            "achievement_id" => 10
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "150",
            "achievement_id" => 10
        ]);

        Achievement::create([
            "name" => "Chef de gare légendaire",
            "description" => "Achetez 20 Hubs",
            "is_secret" => false,
            "image" => "11.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "300000",
            "achievement_id" => 11
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "150",
            "achievement_id" => 11
        ]);

        Achievement::create([
            "name" => "Chef de gare divin",
            "description" => "Achetez 25 Hubs",
            "is_secret" => false,
            "image" => "12.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "350000",
            "achievement_id" => 12
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "150",
            "achievement_id" => 12
        ]);

        Achievement::create([
            "name" => "Aiguilleur",
            "description" => "Achetez votre premiere ligne",
            "is_secret" => false,
            "image" => "13.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "1000000",
            "achievement_id" => 13
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "450",
            "achievement_id" => 13
        ]);
        Reward::create([
            "type" => "tpoint",
            "value" => "20",
            "achievement_id" => 13
        ]);

        Achievement::create([
            "name" => "Aiguilleur confirmé",
            "description" => "Achetez 5 lignes",
            "is_secret" => false,
            "image" => "14.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "50000",
            "achievement_id" => 14
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 14
        ]);

        Achievement::create([
            "name" => "Aiguilleur expert",
            "description" => "Achetez 10 lignes",
            "is_secret" => false,
            "image" => "15.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "100000",
            "achievement_id" => 15
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 15
        ]);

        Achievement::create([
            "name" => "Aiguilleur légendaire",
            "description" => "Achetez 15 lignes",
            "is_secret" => false,
            "image" => "16.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "150000",
            "achievement_id" => 16
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 16
        ]);

        Achievement::create([
            "name" => "Aiguilleur divin",
            "description" => "Achetez 20 lignes",
            "is_secret" => false,
            "image" => "17.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "200000",
            "achievement_id" => 17
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 17
        ]);

        Achievement::create([
            "name" => "Aiguilleur ultime",
            "description" => "Achetez 25 lignes",
            "is_secret" => false,
            "image" => "18.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "250000",
            "achievement_id" => 18
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 18
        ]);

        Achievement::create([
            "name" => "Aiguilleur suprême",
            "description" => "Achetez 30 lignes",
            "is_secret" => false,
            "image" => "19.png"
        ]);
        Reward::create([
            "type" => "argent",
            "value" => "300000",
            "achievement_id" => 19
        ]);
        Reward::create([
            "type" => "experience",
            "value" => "50",
            "achievement_id" => 19
        ]);
    }

    private function levels()
    {
        Level::create(["level" => 1, "next_level_experience" => 0]);
        Level::create(["level" => 2, "next_level_experience" => 1000]);
        Level::create(["level" => 3, "next_level_experience" => 2000]);
        Level::create(["level" => 4, "next_level_experience" => 3000]);
        Level::create(["level" => 5, "next_level_experience" => 4000]);
        Level::create(["level" => 6, "next_level_experience" => 5000]);
        Level::create(["level" => 7, "next_level_experience" => 6000]);
        Level::create(["level" => 8, "next_level_experience" => 7000]);
        Level::create(["level" => 9, "next_level_experience" => 8000]);
        Level::create(["level" => 10, "next_level_experience" => 9000]);
        Level::create(["level" => 11, "next_level_experience" => 10000]);
        Level::create(["level" => 12, "next_level_experience" => 11000]);
        Level::create(["level" => 13, "next_level_experience" => 12000]);
        Level::create(["level" => 14, "next_level_experience" => 13000]);
        Level::create(["level" => 15, "next_level_experience" => 14000]);
        Level::create(["level" => 16, "next_level_experience" => 15000]);
        Level::create(["level" => 17, "next_level_experience" => 16000]);
        Level::create(["level" => 18, "next_level_experience" => 17000]);
        Level::create(["level" => 19, "next_level_experience" => 18000]);
        Level::create(["level" => 20, "next_level_experience" => 19000]);
        Level::create(["level" => 21, "next_level_experience" => 20000]);
        Level::create(["level" => 22, "next_level_experience" => 21000]);
        Level::create(["level" => 23, "next_level_experience" => 22000]);
        Level::create(["level" => 24, "next_level_experience" => 23000]);
        Level::create(["level" => 25, "next_level_experience" => 24000]);
        Level::create(["level" => 26, "next_level_experience" => 25000]);
        Level::create(["level" => 27, "next_level_experience" => 26000]);
        Level::create(["level" => 28, "next_level_experience" => 27000]);
        Level::create(["level" => 29, "next_level_experience" => 28000]);
        Level::create(["level" => 30, "next_level_experience" => 29000]);
        Level::create(["level" => 31, "next_level_experience" => 30000]);
        Level::create(["level" => 32, "next_level_experience" => 31000]);
        Level::create(["level" => 33, "next_level_experience" => 32000]);
        Level::create(["level" => 34, "next_level_experience" => 33000]);
        Level::create(["level" => 35, "next_level_experience" => 34000]);
        Level::create(["level" => 36, "next_level_experience" => 35000]);
        Level::create(["level" => 37, "next_level_experience" => 36000]);
        Level::create(["level" => 38, "next_level_experience" => 37000]);
        Level::create(["level" => 39, "next_level_experience" => 38000]);
        Level::create(["level" => 40, "next_level_experience" => 39000]);
        Level::create(["level" => 41, "next_level_experience" => 40000]);
        Level::create(["level" => 42, "next_level_experience" => 41000]);
        Level::create(["level" => 43, "next_level_experience" => 42000]);
        Level::create(["level" => 44, "next_level_experience" => 43000]);
        Level::create(["level" => 45, "next_level_experience" => 44000]);
    }

    private function cardCategory()
    {
        CardHolderCategory::create([
            "name" => "Commune",
        ])->create([
            "name" => "Remarquable",
        ])->create([
            "name" => "Prestigieuse",
        ])->create([
            "name" => "Légendaire",
        ]);
    }

    private function cardCommune()
    {
        CardHolder::create([
            "name" => "Taux R&D +0.20",
            "type_avantage" => "rate_research",
            "value" => 0.20,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "Taux R&D +0.10",
            "type_avantage" => "rate_research",
            "value" => 0.10,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "Taux R&D +0.30",
            "type_avantage" => "rate_research",
            "value" => 0.30,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 1 000 000 €",
            "type_avantage" => "research",
            "value" => 1000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 2 000 000 €",
            "type_avantage" => "research",
            "value" => 2000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 3 000 000 €",
            "type_avantage" => "research",
            "value" => 3000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 5 000 000 €",
            "type_avantage" => "research",
            "value" => 5000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 6M €",
            "type_avantage" => "research",
            "value" => 6000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 8M €",
            "type_avantage" => "research",
            "value" => 8000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 10M €",
            "type_avantage" => "research",
            "value" => 10000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "15 Audit gratuit",
            "type_avantage" => "audit_int",
            "value" => 15,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "5 Audit gratuit",
            "type_avantage" => "audit_int",
            "value" => 5,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "10 Audit gratuit",
            "type_avantage" => "audit_int",
            "value" => 10,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "20 Audit gratuit",
            "type_avantage" => "audit_int",
            "value" => 20,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "30 Audit gratuit",
            "type_avantage" => "audit_int",
            "value" => 30,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "7 Simulation gratuit",
            "type_avantage" => "simulation",
            "value" => 7,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "10 Simulation gratuit",
            "type_avantage" => "simulation",
            "value" => 10,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "20 Simulation gratuit",
            "type_avantage" => "simulation",
            "value" => 20,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "30 Simulation gratuit",
            "type_avantage" => "simulation",
            "value" => 30,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "5 Audit gratuit",
            "type_avantage" => "audit_ext",
            "value" => 5,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "10 Audit gratuit",
            "type_avantage" => "audit_ext",
            "value" => 10,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 1 000 000 €",
            "type_avantage" => "argent",
            "value" => 1000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 2 000 000 €",
            "type_avantage" => "argent",
            "value" => 2000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 5 000 000 €",
            "type_avantage" => "argent",
            "value" => 5000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "+ 10M €",
            "type_avantage" => "argent",
            "value" => 10000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "Impot - 1M €",
            "type_avantage" => "credit_impot",
            "value" => 1000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "Impot - 2M €",
            "type_avantage" => "credit_impot",
            "value" => 2000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "Impot - 3M €",
            "type_avantage" => "credit_impot",
            "value" => 3000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "Impot - 5M €",
            "type_avantage" => "credit_impot",
            "value" => 5000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "Impot - 7M €",
            "type_avantage" => "credit_impot",
            "value" => 7000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "Impot - 10M €",
            "type_avantage" => "credit_impot",
            "value" => 10000000,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "VTU B11",
            "type_avantage" => "engin",
            "value" => 10,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "VTU A10",
            "type_avantage" => "engin",
            "value" => 9,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "BB 25500",
            "type_avantage" => "engin",
            "value" => 3,
            "card_holder_category_id" => 1,
        ])->create([
            "name" => "BGC 3C",
            "type_avantage" => "engin",
            "value" => 40,
            "card_holder_category_id" => 1,
        ]);
    }

    private function cardRemarquable()
    {
        CardHolder::create([
            "name" => "Taux R&D +0.30",
            "type_avantage" => "rate_research",
            "value" => 0.30,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "Taux R&D +0.40",
            "type_avantage" => "rate_research",
            "value" => 0.40,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "Taux R&D +0.50",
            "type_avantage" => "rate_research",
            "value" => 0.50,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 20M €",
            "type_avantage" => "research",
            "value" => 20000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 23M €",
            "type_avantage" => "research",
            "value" => 23000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 26M €",
            "type_avantage" => "research",
            "value" => 26000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 30M €",
            "type_avantage" => "research",
            "value" => 30000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 35M €",
            "type_avantage" => "research",
            "value" => 35000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 39M €",
            "type_avantage" => "research",
            "value" => 39000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 45M €",
            "type_avantage" => "research",
            "value" => 45000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 23M €",
            "type_avantage" => "argent",
            "value" => 23000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 27M €",
            "type_avantage" => "argent",
            "value" => 27000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 30M €",
            "type_avantage" => "argent",
            "value" => 30000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "+ 45M €",
            "type_avantage" => "argent",
            "value" => 45000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "Impot - 27M €",
            "type_avantage" => "credit_impot",
            "value" => 27000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "Impot - 35M €",
            "type_avantage" => "credit_impot",
            "value" => 35000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "Impot - 40M €",
            "type_avantage" => "credit_impot",
            "value" => 40000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "Impot - 43M €",
            "type_avantage" => "credit_impot",
            "value" => 43000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "Impot - 46M €",
            "type_avantage" => "credit_impot",
            "value" => 46000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "Impot - 50M €",
            "type_avantage" => "credit_impot",
            "value" => 50000000,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "BGC 4C",
            "type_avantage" => "engin",
            "value" => 41,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "BB 12000",
            "type_avantage" => "engin",
            "value" => 1,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "BB 26000",
            "type_avantage" => "engin",
            "value" => 7,
            "card_holder_category_id" => 2,
        ])->create([
            "name" => "Z 7100",
            "type_avantage" => "engin",
            "value" => 20,
            "card_holder_category_id" => 2,
        ]);
    }
    private function cardPrestige()
    {
        CardHolder::create([
            "name" => "Taux R&D +0.50",
            "type_avantage" => "rate_research",
            "value" => 0.50,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "Taux R&D +0.60",
            "type_avantage" => "rate_research",
            "value" => 0.60,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "Taux R&D +0.70",
            "type_avantage" => "rate_research",
            "value" => 0.70,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 55M €",
            "type_avantage" => "research",
            "value" => 55000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 60M €",
            "type_avantage" => "research",
            "value" => 60000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 65M €",
            "type_avantage" => "research",
            "value" => 65000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 70M €",
            "type_avantage" => "research",
            "value" => 70000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 75M €",
            "type_avantage" => "research",
            "value" => 75000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 85M €",
            "type_avantage" => "research",
            "value" => 85000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 90M €",
            "type_avantage" => "research",
            "value" => 90000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 60M €",
            "type_avantage" => "argent",
            "value" => 60000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 70M €",
            "type_avantage" => "argent",
            "value" => 70000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 90M €",
            "type_avantage" => "argent",
            "value" => 90000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "+ 110M €",
            "type_avantage" => "argent",
            "value" => 110000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "Impot - 27M €",
            "type_avantage" => "credit_impot",
            "value" => 27000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "Impot - 65M €",
            "type_avantage" => "credit_impot",
            "value" => 65000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "Impot - 75M €",
            "type_avantage" => "credit_impot",
            "value" => 75000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "Impot - 85M €",
            "type_avantage" => "credit_impot",
            "value" => 85000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "Impot - 95M €",
            "type_avantage" => "credit_impot",
            "value" => 95000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "Impot - 105M €",
            "type_avantage" => "credit_impot",
            "value" => 105000000,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "B 84500",
            "type_avantage" => "engin",
            "value" => 45,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "BB 27000",
            "type_avantage" => "engin",
            "value" => 8,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "REGIO2N C",
            "type_avantage" => "engin",
            "value" => 47,
            "card_holder_category_id" => 3,
        ])->create([
            "name" => "TGV PSE",
            "type_avantage" => "engin",
            "value" => 56,
            "card_holder_category_id" => 3,
        ]);
    }
    private function cardLengendaire()
    {
        CardHolder::create([
            "name" => "Taux R&D +0.90",
            "type_avantage" => "rate_research",
            "value" => 0.90,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "Taux R&D +1.00",
            "type_avantage" => "rate_research",
            "value" => 1.00,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "Taux R&D +1.10",
            "type_avantage" => "rate_research",
            "value" => 1.10,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 100M €",
            "type_avantage" => "research",
            "value" => 100000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 110M €",
            "type_avantage" => "research",
            "value" => 110000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 120M €",
            "type_avantage" => "research",
            "value" => 120000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 130M €",
            "type_avantage" => "research",
            "value" => 130000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 150M €",
            "type_avantage" => "research",
            "value" => 150000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 170M €",
            "type_avantage" => "research",
            "value" => 170000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 180M €",
            "type_avantage" => "research",
            "value" => 180000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 120M €",
            "type_avantage" => "argent",
            "value" => 120000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 150M €",
            "type_avantage" => "argent",
            "value" => 150000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 200M €",
            "type_avantage" => "argent",
            "value" => 200000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "+ 230M €",
            "type_avantage" => "argent",
            "value" => 230000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "Impot - 125M €",
            "type_avantage" => "credit_impot",
            "value" => 125000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "Impot - 140M €",
            "type_avantage" => "credit_impot",
            "value" => 140000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "Impot - 165M €",
            "type_avantage" => "credit_impot",
            "value" => 165000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "Impot - 185M €",
            "type_avantage" => "credit_impot",
            "value" => 185000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "Impot - 200M €",
            "type_avantage" => "credit_impot",
            "value" => 200000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "Impot - 210M €",
            "type_avantage" => "credit_impot",
            "value" => 210000000,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "REGIO2N M",
            "type_avantage" => "engin",
            "value" => 48,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "Z 26500 4C",
            "type_avantage" => "engin",
            "value" => 8,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "TGV R Lacroix",
            "type_avantage" => "engin",
            "value" => 58,
            "card_holder_category_id" => 4,
        ])->create([
            "name" => "TGV Duplex",
            "type_avantage" => "engin",
            "value" => 53,
            "card_holder_category_id" => 4,
        ]);
    }

    private function badge()
    {
        Badge::create([
            "name" => "Bienvenue",
            "action" => "welcome",
            "action_count" => 1,
        ]);
        BadgeReward::create([
            "badge_id" => 1,
            "type" => "argent",
            "value" => 50000,
        ]);
        BadgeReward::create([
            "badge_id" => 1,
            "type" => "tpoint",
            "value" => 10,
        ]);

        Badge::create([
            "name" => "Acheter votre premier Hub",
            "action" => "checkout_hub",
            "action_count" => 1,
        ]);
        BadgeReward::create([
            "badge_id" => 2,
            "type" => "argent",
            "value" => 50000,
        ]);
        BadgeReward::create([
            "badge_id" => 2,
            "type" => "tpoint",
            "value" => 10,
        ]);

        Badge::create([
            "name" => "Réparer 10 trains",
            "action" => "repairEngine",
            "action_count" => 10,
        ]);
        BadgeReward::create([
            "badge_id" => 3,
            "type" => "argent",
            "value" => 100000,
        ]);
        BadgeReward::create([
            "badge_id" => 3,
            "type" => "tpoint",
            "value" => 20,
        ]);

        Badge::create([
            "name" => "Réparer 100 trains",
            "action" => "repairEngine",
            "action_count" => 100,
        ]);
        BadgeReward::create([
            "badge_id" => 4,
            "type" => "argent",
            "value" => 100000,
        ]);
        BadgeReward::create([
            "badge_id" => 4,
            "type" => "tpoint",
            "value" => 20,
        ]);

        Badge::create([
            "name" => "Réparer 1000 trains",
            "action" => "repairEngine",
            "action_count" => 1000,
        ]);
        BadgeReward::create([
            "badge_id" => 5,
            "type" => "argent",
            "value" => 100000,
        ]);
        BadgeReward::create([
            "badge_id" => 5,
            "type" => "tpoint",
            "value" => 20,
        ]);

        Badge::create([
            "name" => "Acheter 15 hubs",
            "action" => "checkout_hub",
            "action_count" => 15,
        ]);
        BadgeReward::create([
            "badge_id" => 6,
            "type" => "argent",
            "value" => 50000,
        ]);
        BadgeReward::create([
            "badge_id" => 6,
            "type" => "tpoint",
            "value" => 10,
        ]);

        Badge::create([
            "name" => "Acheter 20 hubs",
            "action" => "checkout_hub",
            "action_count" => 20,
        ]);
        BadgeReward::create([
            "badge_id" => 7,
            "type" => "argent",
            "value" => 50000,
        ]);
        BadgeReward::create([
            "badge_id" => 7,
            "type" => "tpoint",
            "value" => 10,
        ]);

        Badge::create([
            "name" => "Atteindre de niveau 2",
            "action" => "levelUp",
            "action_count" => 2
        ]);
        BadgeReward::create([
            "badge_id" => 8,
            "type" => "tpoint",
            "value" => 5
        ]);

        Badge::create([
            "name" => "Atteindre le niveau 10",
            "action" => "levelUp",
            "action_count" => 10
        ]);
        BadgeReward::create([
            "badge_id" => 9,
            "type" => "tpoint",
            "value" => 5
        ]);

        Badge::create([
            "name" => "Atteindre le niveau 25",
            "action" => "levelUp",
            "action_count" => 25
        ]);
        BadgeReward::create([
            "badge_id" => 10,
            "type" => "tpoint",
            "value" => 20
        ]);
    }
}
