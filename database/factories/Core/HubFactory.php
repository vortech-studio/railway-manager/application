<?php

namespace Database\Factories\Core;

use App\Models\Core\Gare;
use App\Models\Core\Hub;
use Illuminate\Database\Eloquent\Factories\Factory;

class HubFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Hub::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'gare_id' => Gare::factory(),
            'price' => $this->faker->randomFloat(2, 0, 999999.99),
            'taxe_hub' => $this->faker->randomFloat(2, 0, 999999.99),
            'passenger_first' => $this->faker->numberBetween(-10000, 10000),
            'passenger_second' => $this->faker->numberBetween(-10000, 10000),
            'nb_slot_commerce' => $this->faker->numberBetween(-10000, 10000),
            'nb_slot_pub' => $this->faker->numberBetween(-10000, 10000),
            'nb_slot_parking' => $this->faker->numberBetween(-10000, 10000),
        ];
    }
}
