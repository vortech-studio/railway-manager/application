<?php

namespace App\Jobs\Game;

use App\Models\User\UserDelivery;
use App\Models\User\UserLigne;
use App\Notifications\Game\LigneDeliveryNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LigneDeliveryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserDelivery $delivery, public UserLigne $ligne)
    {
    }

    public function handle(): void
    {
        $this->ligne->active = true;
        $this->ligne->save();

        $this->ligne->user->notify(new LigneDeliveryNotification($this->ligne));

        $this->delivery->delete();
    }
}
