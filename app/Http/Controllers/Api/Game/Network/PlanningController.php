<?php

namespace App\Http\Controllers\Api\Game\Network;

use App\Http\Controllers\Controller;
use App\Models\Core\LigneStation;
use App\Models\User\UserEngine;
use App\Models\User\UserPlanning;
use App\Services\Jira\JiraService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Jira\Laravel\Facades\Jira;

class PlanningController extends Controller
{
    public function index()
    {
        $querys = auth()->user()->plannings()->whereBetween('date_depart', [now()->startOfDay(), now()->endOfDay()])->get();
        $plannings = [];

        foreach ($querys as $item) {
            ob_start();
            ?>
            <div class="d-flex flex-column w-300px rounded-2 bg-white p-10">
                <div class="d-flex flex-row align-items-center">
                    <div class="symbol symbol-20px me-2">
                        <img src="<?= $item->ligne->ligne->type_ligne_logo ?>" alt="">
                    </div>
                    <div class="fw-bolder"><?= $item->ligne->ligne->name ?></div>
                </div>
                <div class="d-flex flex-column">
                    <div class="d-flex flex-row">
                        <div class="fw-bolder">Départ:</div>
                        <div class="ms-auto"><?= $item->date_depart->format('H:i') ?></div>
                    </div>
                    <div class="d-flex flex-row">
                        <div class="fw-bolder">Arrivée:</div>
                        <div
                            class="ms-auto"><?= $item->date_depart->addMinutes($item->ligne->ligne->time_min)->format('H:i') ?></div>
                    </div>
                    <div class="d-flex flex-row">
                        <div class="fw-bolder">Etat:</div>
                        <div class="ms-auto"><?= $item->status_format ?></div>
                    </div>
                </div>
            </div>
            <?php
            $tooltip = ob_get_clean();
            $plannings[] = [
                $item->engine->engine->name,
                $item->engine->engine->type_train_format . " - N°" . $item->number_travel,
                $tooltip,
                $item->date_depart,
                $item->date_depart->addMinutes($item->ligne->ligne->time_min),
            ];
        }

        return response()->json([
            "status" => "success",
            "data" => $plannings,
        ]);
    }

    public function engine(Request $request, $user_id, $engine_id)
    {
        $querys = UserEngine::find($engine_id)->plannings()->get();

        $plannings = [];

        foreach ($querys as $item) {
            $plannings[] = [
                "title" => $item->number_travel . " - " . $item->ligne->ligne->name,
                "desc" => "Lorem",
                "start" => $item->date_depart,
                "end" => $item->date_depart->addMinutes($item->ligne->ligne->time_min),
                "id" => $item->id,
                "time_min" => $item->ligne->ligne->time_min,
                "status" => $item->status,
                "planning_id" => $item->id,
                "engine_id" => $item->user_engine_id,
            ];
        }

        return response()->json([
            'plannings' => $plannings,
            "engine" => UserEngine::find($engine_id),
            "ligne_time" => UserEngine::find($engine_id)->user_ligne->ligne->time_min,
        ]);
    }

    public function checkAddEvent(Request $request, $user_id, $engine_id)
    {
        $planning = UserEngine::find($engine_id)->plannings()->whereBetween('date_depart', [Carbon::createFromTimestamp(strtotime($request->get('start'))), Carbon::createFromTimestamp(strtotime($request->get('start')))->addMinutes($request->get('time'))])->get()->count();

        if ($planning > 0) {
            return response()->json([
                "status" => "error",
                "message" => "Il y a déjà un voyage de prévu à cette heure-ci.",
            ]);
        } else {
            return response()->json([
                "status" => "success",
                "message" => "Vous pouvez créer un voyage à cette heure-ci.",
                "engine_id" => $engine_id,
            ]);
        }
    }

    public function addEvent(Request $request, $user_id, $engine_id)
    {
        $engine = UserEngine::find($engine_id);

        try {
            $planning = $engine->plannings()->create([
                "number_travel" => null,
                "date_depart" => Carbon::createFromTimestamp(strtotime($request->get('start'))),
                "date_arrived" => Carbon::createFromTimestamp(strtotime($request->get('start')))->addMinutes($engine->user_ligne->ligne->time_min),
                "user_ligne_id" => $engine->user_ligne->id,
                "user_engine_id" => $engine->id,
                "user_id" => $engine->user->id,
                "user_hub_id" => $engine->user_hub_id,
                "kilometer" => $engine->user_ligne->ligne->distance
            ]);
            $planning->number_travel = $planning->generateNumberTravel();
            $planning->save();

            $planning->travel()->create([
                "ca_billetterie" => 0,
                "ca_other" => 0,
                "frais_electrique" => 0,
                "frais_gasoil" => 0,
                "frais_autre" => 0,
                "user_planning_id" => $planning->id,
            ]);

            foreach ($planning->ligne->ligne->stations as $station) {
                if($station->gare->id != $planning->ligne->ligne->stationStart->id || $station->gare->id != $planning->ligne->ligne->stationEnd->id) {
                    $planning->planning_stations()->create([
                        "type_infra" => "station",
                        "departure_at" => $planning->date_depart->addMinutes($station->time),
                        "arrival_at" => $planning->date_depart->addMinutes($station->time),
                        "user_planning_id" => $planning->id,
                        "ligne_station_id" => $station->id,
                        "name" => $station->gare->name
                    ]);
                } else {
                    $planning->planning_stations()->create([
                        "type_infra" => "station",
                        "departure_at" => $planning->date_depart->addMinutes($station->time + LigneStation::time_stop_station_config($station)),
                        "arrival_at" => $planning->date_depart->addMinutes($station->time),
                        "user_planning_id" => $planning->id,
                        "ligne_station_id" => $station->id,
                        "name" => $station->gare->name
                    ]);
                }

            }

            return response()->json([
                "status" => "success",
                "message" => "Le voyage a été créé avec succès.",
                "engine" => $planning->number_travel . ' - ' . $planning->ligne->ligne->name,
                "start" => $planning->date_depart,
                "end" => $planning->date_depart->addMinutes($planning->ligne->ligne->time_min),
                "allDay" => false
            ]);
        } catch (\Exception $e) {
            JiraService::createTicket("Erreur lors de la création d'un voyage", $e->getMessage() . " - " . $e->getTraceAsString());
            \Log::critical($e->getMessage(), ["context" => $e->getTraceAsString()]);
            return response()->json([
                "status" => "error",
                "message" => "Une erreur est survenue lors de la création du voyage.",
            ]);
        }
    }

    public function editEvent(Request $request, $user_id, $engine_id)
    {
        $parse_time = explode(":", $request->get('heure_depart'));
        $parse_date = explode("-", $request->get('start_at'));
        $heure_depart = Carbon::create($parse_date[0], $parse_date[1], \Str::limit($parse_date[2], 2, ''), $parse_time[0], $parse_time[1], 0);

        try {
            $planning = UserPlanning::find($request->get('planning_id'));
            $planning->date_depart = $heure_depart;
            $planning->save();

            foreach ($planning->planning_stations as $planning_station) {
                $planning_station->update([
                    "departure_at" => $planning->date_depart->addMinutes($planning_station->ligneStation->time + LigneStation::time_stop_station_config($planning_station->ligneStation)),
                    "arrival_at" => $planning->date_depart->addMinutes($planning_station->ligneStation->time),
                ]);
            }

            if($request->has('repeat_mission')) {
                for($i = 1; $i <= $request->get('repeat_mission_number'); $i++) {
                    $date_depart = $heure_depart->addDays($i);
                    $p = UserPlanning::create([
                        "number_travel" => null,
                        "date_depart" => $date_depart,
                        "date_arrived" => $date_depart->addMinutes($planning->ligne->ligne->time_min),
                        "user_ligne_id" => $planning->user_ligne_id,
                        "user_engine_id" => $planning->user_engine_id,
                        "user_id" => $planning->user_id,
                        "user_hub_id" => $planning->user_hub_id,
                        "kilometer" => $planning->kilometer
                    ]);
                    $p->number_travel = $p->generateNumberTravel();
                    $p->save();
                }
            }

            return response()->json([
                "status" => "success",
                "message" => "Le voyage a été modifié avec succès.",
                "engine" => $planning->number_travel . ' - ' . $planning->ligne->ligne->name,
                "start" => $planning->date_depart,
                "end" => $planning->date_depart->addMinutes($planning->ligne->ligne->time_min),
                "allDay" => false
            ]);
        }catch (\Exception $exception) {
            JiraService::createTicket("Erreur lors de la modification d'un voyage", $exception->getMessage() . " - " . $exception->getTraceAsString());
            \Log::critical($exception->getMessage(), ["context" => $exception->getTraceAsString()]);
            return response()->json([
                "status" => "error",
                "message" => "Une erreur est survenue lors de la modification du voyage.",
            ]);
        }
    }

    public function deleteEvent(Request $request, $user_id, $engine_id)
    {
        try {
            $planning = UserPlanning::find($request->get('planning_id'));
            $planning->delete();

            return response()->json([
                "status" => "success",
                "message" => "Le voyage a été supprimé avec succès.",
            ]);
        }catch (\Exception $exception) {
            JiraService::createTicket("Erreur lors de la suppression d'un voyage", $exception->getMessage() . " - " . $exception->getTraceAsString());
            \Log::critical($exception->getMessage(), ["context" => $exception->getTraceAsString()]);
            return response()->json([
                "status" => "error",
                "message" => "Une erreur est survenue lors de la suppression du voyage.",
            ]);
        }
    }
}
