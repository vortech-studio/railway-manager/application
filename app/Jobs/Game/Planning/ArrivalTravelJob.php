<?php

namespace App\Jobs\Game\Planning;

use App\Models\Core\Mouvement;
use App\Models\User\UserLigneTarif;
use App\Models\User\UserPlanning;
use App\Notifications\Game\SendMessageNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ArrivalTravelJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserPlanning $planning)
    {
    }

    public function handle(): void
    {

        $this->planning->status = 'arrival';
        $this->planning->save();

        $ca_other = rand(0, $this->planning->passengers()->count()) * rand(1.02, 20);

        $this->planning->travel()->update([
            "ca_billetterie" => $this->calcRsultatBilletterie($this->planning->user_ligne_id),
            "ca_other" => $ca_other,
            "frais_electrique" => $this->calcElectricite(),
            "frais_gasoil" => $this->calcGasoil(),
            "frais_autre" => $this->planning->ligne->ligne->hub->taxe_hub
        ]);

        $this->planning->engine->use_percent += $this->planning->engine->calcUsureEngine();
        $this->planning->engine->save();

        Mouvement::adding(
            'revenue',
            'Vente de la ligne: ' . $this->planning->ligne->ligne->name,
            'billetterie',
            $this->calcRsultatBilletterie($this->planning->user_ligne_id),
            $this->planning->user->company->id,
            $this->planning->user_hub_id,
            $this->planning->user_ligne_id
        );

        Mouvement::adding(
            'revenue',
            'Vente Additionnel de la ligne: ' . $this->planning->ligne->ligne->name,
            'rent_trajet_aux',
            $ca_other,
            $this->planning->user->company->id,
            $this->planning->user_hub_id,
            $this->planning->user_ligne_id
        );

        Mouvement::adding(
            'charge',
            'Taxe de passage en gare: ' . $this->planning->ligne->ligne->name,
            'taxe',
            $this->planning->ligne->ligne->hub->taxe_hub,
            $this->planning->user->company->id,
            $this->planning->user_hub_id,
            $this->planning->user_ligne_id
        );

        $this->planning->user->addPoints($this->planning->ligne->ligne->distance / 10);

        $this->planning->logs()->create([
            'message' => "Arrivée du train en gare de ".$this->planning->ligne->ligne->stationEnd->name,
            "user_planning_id" => $this->planning->id,
        ]);

        $this->planning->user->notify(new SendMessageNotification(
            'Arrivée en gare !',
            "Le train " . $this->planning->engine->engine->name . " vient d'arriver en gare de ".$this->planning->ligne->ligne->stationEnd->name,
            "info",
            asset('/storage/icons/train_engine.png'),
            route('travel.show', $this->planning->id)
        ));
    }

    private function calcRsultatBilletterie($user_ligne_id)
    {
        $tarifs = UserLigneTarif::where('user_ligne_id', $user_ligne_id)
            ->where('date_tarif', now()->startOfDay())
            ->get();
        $sum = 0;

        foreach ($tarifs as $tarif) {
            $sum += $tarif->price * $this->planning->passengers()->where('type', $tarif->type)->sum('nb_passengers');
        }

        return $sum;
    }

    private function calcElectricite()
    {
        if ($this->planning->engine->engine->type_energy == 'electrique' || $this->planning->engine->engine->type_energy == 'hybride') {
            Mouvement::adding(
                'charge',
                'Frais d\'électricité pour le trajet: ' . $this->planning->ligne->ligne->name,
                'electricite',
                config('global.price_electricity') * $this->planning->ligne->ligne->distance,
                $this->planning->user->company->id,
                $this->planning->user_hub_id,
                $this->planning->user_ligne_id
            );
            return config('global.price_electricity') * $this->planning->ligne->ligne->distance;
        } else {
            return 0;
        }
    }

    private function calcGasoil()
    {
        if ($this->planning->engine->engine->type_energy == 'diesel' || $this->planning->engine->engine->type_energy == 'hybride') {
            Mouvement::adding(
                'charge',
                'Frais de gasoil pour le trajet: ' . $this->planning->ligne->ligne->name,
                'gasoil',
                config('global.price_gasoil') * $this->planning->ligne->ligne->distance,
                $this->planning->user->company->id,
                $this->planning->user_hub_id,
                $this->planning->user_ligne_id
            );
            return config('global.price_gasoil') * $this->planning->ligne->ligne->distance;
        } else {
            return 0;
        }
    }
}
