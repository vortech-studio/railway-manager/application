<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\Achievement;
use Illuminate\Http\Request;

class BadgeAchievementController extends Controller
{
    public function info($id)
    {
        $achievement = Achievement::with('rewards')->find($id);
        return response()->json([
            "achievement" => $achievement,
        ]);
    }

    public function destroy($id)
    {
        $achievement = Achievement::findOrFail($id);

        \Storage::disk('badges')->delete($achievement->image);

        $achievement->delete();
        return response()->json([
            "message" => "Mission Supprimée",
        ]);
    }

}
