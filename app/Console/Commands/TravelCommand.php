<?php

namespace App\Console\Commands;

use App\Models\Core\Mouvement;
use App\Models\User\UserEngine;
use App\Models\User\UserLigneTarif;
use App\Models\User\UserPlanning;
use App\Models\User\UserPlanningPassenger;
use App\Models\User\UserPlanningStation;
use App\Notifications\Game\SendMessageNotification;
use Illuminate\Console\Command;

class TravelCommand extends Command
{
    protected $signature = 'travel {action}';

    protected $description = 'Command description';

    public function handle(): void
    {
        match ($this->argument('action')) {
            'prepare' => $this->prepareTravel(),
            'departure' => $this->departureTravel(),
            'transit' => $this->transitTravel(),
            'in_station_arrival' => $this->inStationArrivalTravel(),
            'in_station' => $this->inStationTravel(),
            'in_station_departure' => $this->inStationDepartureTravel(),
            'arrival' => $this->arrivalTravel(),
        };
    }

    private function prepareTravel()
    {
        $plannings = UserPlanning::whereBetween('date_depart', [now()->addMinutes(20)
            ->startOfMinute(), now()->addMinutes(20)->endOfMinute()])
            ->where('status', 'initialized')
            ->orWhere('status', 'retarded')
            ->get();

        foreach ($plannings as $planning) {
            if($this->verifEngine($planning->engine)) {
                $this->cancelledTravel($planning);
            } else {
                try {
                    $this->prepareVoyageur($planning);

                    $planning->update([
                        "status" => "departure"
                    ]);

                    $planning->engine->update([
                        "status" => "travel"
                    ]);

                    $planning->logs()->create([
                        'message' => "Départ du train en direction de la gare de départ",
                        "user_planning_id" => $planning->id,
                    ]);
                    $planning->user->notify(new SendMessageNotification(
                        'Départ du train en direction de la gare de départ',
                        "Le train " . $planning->engine->engine->name . " vient de partir en direction de la gare de départ.",
                        "info",
                        asset('/storage/icons/train_engine.png'),
                        route('travel.show', $planning->id),
                        "travel"
                    ));
                }catch (\Exception $exception) {
                    \Log::emergency($exception->getMessage(), $exception->getTrace());
                }
            }
        }
    }

    private function departureTravel()
    {
        $plannings = UserPlanning::whereBetween('date_depart', [now()->startOfMinute(), now()->endOfMinute()])
            ->get();

        foreach ($plannings as $planning) {
            try {
                $planning->logs()->create([
                    'message' => $planning->engine->engine->name . " prêt au départ pour la gare de ".$planning->ligne->ligne->stationEnd->name,
                    "user_planning_id" => $planning->id,
                ]);

                $planning->user->notify(new SendMessageNotification(
                    'Attention au départ !',
                    $planning->engine->engine->name . " prêt au départ pour la gare de ".$planning->ligne->ligne->stationEnd->name,
                    "info",
                    asset('/storage/icons/train_engine.png'),
                    route('travel.show', $planning->id),
                    "travel"
                ));
            }catch (\Exception $exception) {
                \Log::emergency($exception->getMessage(), $exception->getTrace());
            }
        }
    }

    private function transitTravel()
    {
        $plannings = UserPlanning::where('date_depart', now()->subMinutes(2)->startOfMinute())
            ->get();

        foreach ($plannings as $planning) {
           try{
               $planning->update([
                   "status" => "travel"
               ]);

               $planning->logs()->create([
                   'message' => $planning->engine->engine->name . " en transit vers la gare de ".$planning->ligne->ligne->stationEnd->name,
                   "user_planning_id" => $planning->id,
               ]);

               $planning->user->notify(new SendMessageNotification(
                   'En transit !',
                   $planning->engine->engine->name . " en transit vers la gare de ".$planning->ligne->ligne->stationEnd->name,
                   "info",
                   asset('/storage/icons/train_engine.png'),
                   route('travel.show', $planning->id),
                   "travel"
               ));
           }catch (\Exception $exception) {
               \Log::emergency($exception->getMessage(), $exception->getTrace());
           }
        }
    }

    private function inStationArrivalTravel()
    {
        $stations = UserPlanningStation::where('arrival_at', now()->addMinutes(2)->startOfMinute())
            ->get();

        foreach ($stations as $station) {
            try {
                $station->update([
                    "status" => "arrival"
                ]);

                $station->userPlanning->logs()->create([
                    'message' => "Le ".$station->userPlanning->engine->engine->type_train_format." N° ".$station->userPlanning->number_travel." va entrer en gare de ".$station->name,
                    "user_planning_id" => $station->userPlanning->id,
                ]);
            }catch (\Exception $exception) {
                \Log::emergency($exception->getMessage(), $exception->getTrace());
            }
        }

    }

    private function inStationTravel()
    {
        $stations = UserPlanningStation::where('arrival_at', now()->startOfMinute())
            ->get();

        foreach ($stations as $station) {
            try {
                $station->userPlanning->update([
                    "status" => "in_station"
                ]);


                if($station->ligne_station_id != $station->userPlanning->ligne->ligne->stationStart->id || $station->ligne_station_id != $station->userPlanning->ligne->ligne->stationEnd->id) {
                    if($station->userPlanning->engine->engine->type_train == 'ter') {
                        $station->userPlanning->passengers()->create([
                            "type" => "second",
                            "nb_passengers" => rand(0, $station->ligneStation->gare->passenger_unique),
                            "user_planning_id" => $station->userPlanning->id,
                        ]);
                    } else {
                        $station->userPlanning->passengers()->create([
                            "type" => "first",
                            "nb_passengers" => rand(0, $station->ligneStation->gare->passenger_first),
                            "user_planning_id" => $station->userPlanning->id,
                        ]);

                        $station->userPlanning->passengers()->create([
                            "type" => "second",
                            "nb_passengers" => rand(0, $station->ligneStation->gare->passenger_second),
                            "user_planning_id" => $station->userPlanning->id,
                        ]);
                    }
                }

                $station->userPlanning->logs()->create([
                    'message' => "Le ".$station->userPlanning->engine->engine->type_train_format." N° ".$station->userPlanning->number_travel." est en gare de ".$station->name,
                    "user_planning_id" => $station->userPlanning->id,
                ]);
            }catch (\Exception $exception) {
                \Log::emergency($exception->getMessage(), $exception->getTrace());
            }
        }

    }
    private function inStationDepartureTravel()
    {
        $stations = UserPlanningStation::where('departure_at', now()->startOfMinute())
            ->get();

        foreach ($stations as $station) {
            try {
                $station->update([
                    "status" => "done"
                ]);

                $station->userPlanning->update([
                    "status" => "travel"
                ]);

                $station->userPlanning->logs()->create([
                    'message' => "Le ".$station->userPlanning->engine->engine->type_train_format." N° ".$station->userPlanning->number_travel." part de la gare de ".$station->name,
                    "user_planning_id" => $station->userPlanning->id,
                ]);
            }catch (\Exception $exception) {
                \Log::emergency($exception->getMessage(), $exception->getTrace());
            }
        }

    }

    private function arrivalTravel()
    {
        $plannings = UserPlanning::where('status', 'travel')
            ->get();



        foreach ($plannings as $planning) {
            if($planning->date_arrived->startOfMinute() == now()->startOfMinute()) {

                try {
                    $planning->status = 'arrival';
                    $planning->save();

                    $ca_other = rand(0, $planning->passengers()->sum('nb_passengers')) * rand(1.02, 20);

                    $planning->travel()->update([
                        "ca_billetterie" => $this->calcRsultatBilletterie($planning),
                        "ca_other" => $ca_other,
                        "frais_electrique" => $this->calcElectricite($planning),
                        "frais_gasoil" => $this->calcGasoil($planning),
                        "frais_autre" => $planning->ligne->ligne->hub->taxe_hub
                    ]);

                    $planning->engine->use_percent += $planning->engine->calcUsureEngine();
                    $planning->engine->save();

                    Mouvement::adding(
                        'revenue',
                        'Vente de la ligne: ' . $planning->ligne->ligne->name,
                        'billetterie',
                        $this->calcRsultatBilletterie($planning),
                        $planning->user->company->id,
                        $planning->user_hub_id,
                        $planning->user_ligne_id
                    );

                    Mouvement::adding(
                        'revenue',
                        'Vente Additionnel de la ligne: ' . $planning->ligne->ligne->name,
                        'rent_trajet_aux',
                        $ca_other,
                        $planning->user->company->id,
                        $planning->user_hub_id,
                        $planning->user_ligne_id
                    );

                    Mouvement::adding(
                        'charge',
                        'Taxe de passage en gare: ' . $planning->ligne->ligne->name,
                        'taxe',
                        $planning->ligne->ligne->hub->taxe_hub,
                        $planning->user->company->id,
                        $planning->user_hub_id,
                        $planning->user_ligne_id
                    );

                    $planning->user->addPoints($planning->ligne->ligne->distance / 10);
                    $planning->engine->update([
                        "status" => "free"
                    ]);

                    $planning->logs()->create([
                        'message' => "Arrivée du train en gare de ".$planning->ligne->ligne->stationEnd->name,
                        "user_planning_id" => $planning->id,
                    ]);

                    $planning->user->notify(new SendMessageNotification(
                        'Arrivée en gare !',
                        "Le train " . $planning->engine->engine->name . " vient d'arriver en gare de ".$planning->ligne->ligne->stationEnd->name,
                        "info",
                        asset('/storage/icons/train_engine.png'),
                        route('travel.show', $planning->id),
                        "travel"
                    ));
                }catch (\Exception $exception) {
                    \Log::emergency($exception->getMessage(), $exception->getTrace());
                }
            }
        }
    }

    private function verifEngine(UserEngine $engine)
    {
        $calc_distance_parcourue = $engine->plannings()->where('status', 'arrival')->sum('kilometer');
        return $calc_distance_parcourue >= $engine->max_runtime_engine && $engine->status == 'free';
    }

    private function cancelledTravel(UserPlanning $planning)
    {
        $planning->status = 'cancelled';
        $planning->save();

        $planning->logs()->create([
            'message' => "Matériel indisponible",
            "user_planning_id" => $planning->id,
        ]);
        $planning->user->notify(new SendMessageNotification(
            'Matériel indisponible pour le trajet :' . $planning->ligne->ligne->name,
            "Le métériel " . $planning->engine->engine->name . " n'est pas disponible car il doit effectuer une maintenance préventive.",
            "warning",
            asset('/storage/icons/engin.png'),
            route('travel.show', $planning->id),
            "travel"
        ));
    }

    private function prepareVoyageur(UserPlanning $planning)
    {
        if ($planning->engine->engine->type_train == 'ter') {
            $planning->passengers()->create([
                'type' => 'second',
                'nb_passengers' => rand(0, $planning->engine->engine->nb_passager),
                "user_planning_id" => $planning->id
            ]);
        } elseif ($planning->engine->engine->type_train == 'tgv' || $planning->engine->engine->type_train == 'ic') {
            $planning->passengers()->create([
                'type' => 'first',
                'nb_passengers' => rand(0, $planning->engine->engine->nb_passager),
                "user_planning_id" => $planning->id
            ]);
            $planning->passengers()->create([
                'type' => 'second',
                'nb_passengers' => rand(0, $planning->engine->engine->nb_passager),
                "user_planning_id" => $planning->id
            ]);
        } elseif ($planning->engine->engine->type_train == 'transilien') {
            $planning->passengers()->create([
                'type' => 'second',
                'nb_passengers' => rand(0, $planning->engine->engine->nb_passager),
                "user_planning_id" => $planning->id
            ]);
        }
    }


    private function calcRsultatBilletterie(UserPlanning $planning)
    {
        $tarifs = UserLigneTarif::where('user_ligne_id', $planning->user_ligne_id)
            ->where('date_tarif', now()->startOfDay())
            ->get();
        $sum = 0;

        foreach ($tarifs as $tarif) {
            $sum += $tarif->price * $planning->passengers()->where('type', $tarif->type == 'unique' ? 'second' : $tarif->type)->sum('nb_passengers');
        }

        return $sum;
    }

    private function calcElectricite(UserPlanning $planning)
    {
        if ($planning->engine->engine->type_energy == 'electrique' || $planning->engine->engine->type_energy == 'hybride') {
            Mouvement::adding(
                'charge',
                'Frais d\'électricité pour le trajet: ' . $planning->ligne->ligne->name,
                'electricite',
                config('global.price_electricity') * $planning->ligne->ligne->distance,
                $planning->user->company->id,
                $planning->user_hub_id,
                $planning->user_ligne_id
            );
            return config('global.price_electricity') * $planning->ligne->ligne->distance;
        } else {
            return 0;
        }
    }

    private function calcGasoil(UserPlanning $planning)
    {
        if ($planning->engine->engine->type_energy == 'diesel' || $planning->engine->engine->type_energy == 'hybride') {
            Mouvement::adding(
                'charge',
                'Frais de gasoil pour le trajet: ' . $planning->ligne->ligne->name,
                'gasoil',
                config('global.price_gasoil') * $planning->ligne->ligne->distance,
                $planning->user->company->id,
                $planning->user_hub_id,
                $planning->user_ligne_id
            );
            return config('global.price_gasoil') * $planning->ligne->ligne->distance;
        } else {
            return 0;
        }
    }


}
