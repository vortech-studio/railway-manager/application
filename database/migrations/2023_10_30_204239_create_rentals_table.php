<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalsTable extends Migration
{
    public function up(): void
    {
        Schema::create('rentals', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('contract_duration')->comment("durée de la location en semaine");
            $table->json("type")->comment("type de location: ter/tgv/ic/other");
            $table->timestamp('next_prlv')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('rentals');
    }
}
