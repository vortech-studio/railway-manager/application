<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToUserIncidents extends Migration
{
    public function up(): void
    {
        Schema::table('user_incidents', function (Blueprint $table) {
            $table->foreignId('user_engine_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_hub_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::table('user_incidents', function (Blueprint $table) {
            $table->dropColumn('user_engine_id');
        });
    }
}
