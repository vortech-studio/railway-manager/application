<?php

namespace App\Http\Controllers\Game\Engine;

use App\Http\Controllers\Controller;
use App\Models\User\UserEngine;
use Illuminate\Http\Request;

class MaintenanceController extends Controller
{
    public function book()
    {
        return view('game.engine.maintenance.book');
    }

    public function history(Request $request)
    {
        if($request->ajax()) {

        } else {
            return view('game.engine.maintenance.history');
        }
    }

    public function group(Request $request)
    {
        return view('game.engine.maintenance.group');
    }

    public function confirmMaintenance(Request $request)
    {
        return match ($request->get('type_maintenance')) {
            "preventif" => $this->generateMaintenancePrev($request),
            "curatif" => $this->generatemaintenanceCur($request)
        };
    }

    public function autoMaintenance(Request $request)
    {
        auth()->user()->setting->update([
            "auto_ter" => $request->has('auto_ter'),
            "auto_tgv" => $request->has('auto_tgv'),
            "auto_ic" => $request->has('auto_ic'),
            "auto_transilien" => $request->has('auto_transilien'),
            "auto_ter_max" => $request->get('wear_ter'),
            "auto_tgv_max" => $request->get('wear_tgv'),
            "auto_ic_max" => $request->get('wear_ic'),
            "auto_transilien_max" => 0, // TODO: Changer quand prise en compte des transiliens
        ]);

        toastr()->success("Programmation automatique activé et prise en compte", "Réparation Automatique");
        return redirect()->back();
    }

    private function generateMaintenancePrev(Request $request)
    {
        foreach ($request->get('engines_id') as $id) {
            $engine = UserEngine::find($id);
            $engine->createMaintenance('preventif');
        }

        toastr()->success("Maintenance préventive groupé prise en compte");
        return redirect()->back();
    }

    private function generatemaintenanceCur(Request $request)
    {
        foreach ($request->get('engines_id') as $id) {
            $engine = UserEngine::find($id);
            $engine->createMaintenance('curatif');
        }
        toastr()->success("Maintenance curative groupé prise en compte");
        return redirect()->back();
    }
}
