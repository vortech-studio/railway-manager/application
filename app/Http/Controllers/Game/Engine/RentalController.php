<?php

namespace App\Http\Controllers\Game\Engine;

use App\Http\Controllers\Controller;
use App\Jobs\Game\EngineDeliveryJob;
use App\Jobs\Game\Rental\RentalDebitJob;
use App\Jobs\Game\Rental\RentalEndingJob;
use App\Models\Core\Engine;
use App\Models\Core\Mouvement;
use App\Models\Core\Rental;
use App\Models\User\Mailbox;
use App\Models\User\UserDelivery;
use App\Models\User\UserEngine;
use Illuminate\Http\Request;

class RentalController extends Controller
{
    public function index()
    {
        return view('game.engine.rental.index');
    }

    public function engine(Request $request)
    {
        $rental = Rental::with('engines')->find($request->get('rental'));

        return view('game.engine.rental.engine', [
            'rental' => $rental
        ]);
    }

    public function config(Request $request)
    {
        $engine = Engine::with('technical', 'compositions', 'rentals')->find($request->get('engine_id'));

        return view('game.engine.rental.config', [
            'engine' => $engine,
            "rental" => $engine->rentals()->find($request->get('rental_id'))
        ]);
    }

    public function checkout(Request $request)
    {
        $engine = Engine::find($request->get('engine_id'));
        $rental = Rental::find($request->get('rental_id'));
        $user_hub = auth()->user()->hubs()->find($request->get('user_hub_id'));

        $amount_pay = $engine->price_location + $engine->getRentalCaution() + ($engine->getRentalFrais() * $request->get('contract_duration'));

        if (Engine::isOk($amount_pay, auth()->user(), 'argent')) {
            Mouvement::adding(
                'charge',
                'Location du matériel roulant: ' . $engine->name,
                'location_materiel',
                $amount_pay,
                auth()->user()->company->id,
                $user_hub->id
            );
        } else {
            toastr()->error("Vous n'avez pas suffisamment d'argent");
            return redirect()->route('engine.buy.config');
        }

        if($user_hub->user->admin) {
            $user_engine = auth()->user()->engines()->create([
                "max_runtime_engine" => $engine->calcMaxRuntimeEngine(),
                "active" => true,
                "in_maintenance" => false,
                "available" => true,
                "date_achat" => now()->startOfDay(),
                "user_id" => auth()->user()->id,
                "engine_id" => $engine->id,
                "user_hub_id" => $user_hub->id,
                "number" => UserEngine::defineNumberEngine()
            ]);

            $contract = auth()->user()->rentals()->create([
                "date_contract" => now()->startOfDay(),
                "duration_contract" => $request->get('contract_duration'),
                "amount_caution" => $engine->getRentalCaution(),
                "amount_fee" => $engine->getRentalFrais(),
                "user_id" => auth()->user()->id,
                "user_engine_id" => $user_engine->id,
                "rental_id" => $rental->id,
            ]);

            dispatch(new RentalEndingJob($contract))->delay(now()->addWeeks($request->get('contract_duration'))->startOfDay());

            for ($i = 1; $i <= $contract->duration_contract; $i++) {
                dispatch(new RentalDebitJob($contract))
                    ->delay(now()->addWeeks($i)->startOfDay())
                    ->onQueue('rental-debit');
            }
        } else {
            $user_engine = auth()->user()->engines()->create([
                "max_runtime_engine" => $engine->calcMaxRuntimeEngine(),
                "active" => false,
                "in_maintenance" => false,
                "available" => false,
                "date_achat" => now()->startOfDay(),
                "user_id" => auth()->user()->id,
                "engine_id" => $engine->id,
                "user_hub_id" => $user_hub->id,
                "number" => UserEngine::defineNumberEngine()
            ]);

            $contract = auth()->user()->rentals()->create([
                "date_contract" => now()->startOfDay(),
                "duration_contract" => $request->get('contract_duration'),
                "amount_caution" => $engine->getRentalCaution(),
                "amount_fee" => $engine->getRentalFrais(),
                "user_id" => auth()->user()->id,
                "user_engine_id" => $user_engine->id,
                "rental_id" => $rental->id,
            ]);

            $delivery = UserDelivery::create([
                "type" => "engine",
                "designation" => $engine->name,
                "start_at" => now(),
                "end_at" => null,
                "user_id" => auth()->user()->id,
                "user_engine_id" => $user_engine->id
            ]);

            $delivery->end_at = $delivery->start_at->addMinutes($delivery->getRateFromDelivery() / 4);
            $delivery->save();
            dispatch(new EngineDeliveryJob($delivery, $user_engine))->onQueue('delivery');
            dispatch(new RentalEndingJob($contract))->delay(now()->addWeeks($request->get('contract_duration'))->startOfDay());

            for ($i = 1; $i <= $contract->duration_contract; $i++) {
                dispatch(new RentalDebitJob($contract))
                    ->delay(now()->addWeeks($i)->startOfDay())
                    ->onQueue('rental-debit');
            }

            Mailbox::create([
                "from" => $rental->name,
                "subject" => "Contrat de location de matériel roulant - Engin ".$engine->name,
                "message" => view('game.includes.mailbox.new_rental', [
                    "rental_name" => $rental->name,
                    "engine_name" => $engine->name,
                    "duration" => $contract->duration_contract,
                    "amount_caution" => $engine->getRentalCaution(),
                    "amount_pay" => $engine->price_location
                ]),
                "user_id" => auth()->user()->id
            ]);
        }



        toastr()->success("Location effectué avec succès");
        return redirect()->route('engine.show', $user_engine->id);

    }
}
