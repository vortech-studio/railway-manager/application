<?php

namespace Database\Factories\Core;

use App\Models\Core\Gares;
use Illuminate\Database\Eloquent\Factories\Factory;

class GaresFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Gares::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'code_uic' => $this->faker->word,
            'type_gare' => $this->faker->regexify('[A-Za-z0-9]{1}'),
            'latitude' => $this->faker->latitude,
            'longitude' => $this->faker->longitude,
            'region' => $this->faker->word,
            'pays' => $this->faker->word,
            'superficie_infra' => $this->faker->word,
            'superficie_quai' => $this->faker->word,
            'long_quai' => $this->faker->numberBetween(-10000, 10000),
            'nb_quai' => $this->faker->numberBetween(-10000, 10000),
            'commerce' => $this->faker->boolean,
            'pub' => $this->faker->boolean,
            'parking' => $this->faker->boolean,
        ];
    }
}
