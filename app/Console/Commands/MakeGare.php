<?php

namespace App\Console\Commands;

use App\Models\Core\GareEquipement;
use App\Models\Core\Gares;
use Illuminate\Console\Command;
use function Laravel\Prompts\confirm;
use function Laravel\Prompts\error;
use function Laravel\Prompts\multiselect;
use function Laravel\Prompts\spin;
use function Laravel\Prompts\text;

class MakeGare extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:gare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assistant de création de gare';

    /**
     * Execute the console command.
     */

    public function handle()
    {
        $name = text(label: "Nom de la gare");
        $this->searchGare($name);
        $code_uic = spin(function () use ($name) {
            return $this->searchUicCode($name);
        }, "Recherche du code UIC de la gare");
        $type_gare = spin(function () use ($name) {
            return $this->searchTypeGare($name);
        }, "Recherche du type de gare");
        $nb_voie = text("Nombre de voie dans la gare", default: 2);
        $long_quai = text("Longueur des quai de la gare", default: $type_gare == "a" ? 400 : ($type_gare == 'b' ? 230 : ($type_gare == 'c' ?? 150)));
        $transportation = multiselect(
            label: "Type de transport", options: ["ter", "tgv", "intercite", "transilien"]
        );

        if(confirm("Etes-vous sur de vouloir créer la gare ?", yes: "Oui", no: "Non")) {
            \Laravel\Prompts\alert("Création de la gare");
            $gre = new Gares();
            $gare = $gre->createGareConsole(
                $name,
                $code_uic,
                $type_gare,
                $nb_voie,
                $long_quai
            );

            $this->createTransportation($transportation, $gare);
            $this->createEquipements($gare);

            if(isset($gare->hub)) {
                if(confirm("Cette gare est également considérer comme un hub, voulez-vous l'activé ?", yes: 'Oui', no: 'Non')) {
                    $hub = $gare->hub;
                    $hub->active = true;
                    $hub->save();
                    info("Hub activé");
                } else {
                    info("Hub non activé");
                }
            }
        } else {
            error("Création de la gare annulé");
        }
    }

    public function searchGare($name)
    {
        if(Gares::where('name', 'like', '%'.$name.'%')->exists()) {
            error("Cette gare existe déjà");
            return;
        }
    }


    private function createGare(string $name, string $code_uic, string $type_gare, string $nb_voie, string $long_quai, array $transportation)
    {
        $gare = new Gares();

        $gts = $gare->createGareConsole(
            $name,
            $code_uic,
            $type_gare,
            $nb_voie,
            $long_quai,
            collect($transportation)
        );

        return $gts;
    }

    private function createTransportation(array $transportations, Gares $gares)
    {
        foreach ($transportations as $transportation) {
            $gares->transportations()->create([
                "type" => $transportation,
                "gares_id" => $gares->id,
            ]);
        }
    }

    private function activateHub(object $gare)
    {
        if(isset($gare->hub)) {
            $hub = $gare->hub;
            $hub->active = true;
            $hub->save();
            $this->info("Hub activé");
        }
    }

    private function searchUicCode($name)
    {
        $request = \Http::withoutVerifying()
            ->get('https://ressources.data.sncf.com/api/explore/v2.1/catalog/datasets/referentiel-gares-voyageurs/records?where=alias_libelle_noncontraint%20like%22'.$name.'%22&limit=20')
            ->object();

        return $request->results[0]->uic_code;
    }

    private function searchTypeGare($name)
    {
        $request = \Http::withoutVerifying()
            ->get('https://ressources.data.sncf.com/api/explore/v2.1/catalog/datasets/referentiel-gares-voyageurs/records?where=alias_libelle_noncontraint%20like%22'.$name.'%22&limit=20')
            ->object();

        return $request->results[0]->segmentdrg_libelle;
    }

    private function createEquipements(Gares $gts)
    {
        $this->info("Création des équipements de la gare");
        $this->info("Type de gare: ".$gts->type_gare);

        GareEquipement::dispatching($gts);

        $this->info("Equipements créés");
    }
}
