<?php

namespace App\Observer;

use App\Models\User\UserTechnicentre;
use App\Models\User\UserTechnicentreStaf;

class UserTechnicentreObserver
{
    public function created(UserTechnicentre $userTechnicentre): void
    {
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i <= 5; $i++) {
            $userTechnicentre->staffs()->create([
                "name" => $faker->name,
                "job" => UserTechnicentreStaf::tableJob()->random(),
                "niveau" => 1,
                "salary" => 11.67 * 151.67,
                "status" => "inactive",
                "user_technicentre_id" => $userTechnicentre->id,
            ]);
        }
    }

    public function updated(UserTechnicentre $userTechnicentre): void
    {
    }

    public function deleted(UserTechnicentre $userTechnicentre): void
    {
    }

    public function restored(UserTechnicentre $userTechnicentre): void
    {
    }

    public function forceDeleted(UserTechnicentre $userTechnicentre): void
    {
    }
}
