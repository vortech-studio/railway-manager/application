<?php

namespace App\Console\Commands;

use App\Models\Core\Gares;
use App\Models\Core\Hub;
use App\Models\Core\Ligne;
use App\Models\Core\LigneRequirement;
use App\Services\Mapping\OpenRouteService;
use Illuminate\Console\Command;
use function Deployer\select;
use function Laravel\Prompts\confirm;
use function Laravel\Prompts\error;
use function Laravel\Prompts\search;
use function Laravel\Prompts\text;
use function Laravel\Prompts\table;

class MakeLigneCommand extends Command
{
    protected $signature = 'make:ligne';

    protected $description = "Assistant de création de ligne";


    public function handle()
    {
        $hub_id = \Laravel\Prompts\select(
            label: "Quel est le hub affilier ?",
            options: $this->formatHubs(),
            scroll: 100
        );
        $gare_depart = \Laravel\Prompts\select(
            label: "Quel est la gare de départ ?",options: $this->formatGaresDepart($hub_id)
        );
        $gare_arrive = search(
            label: "Quel est la gare d'arrivée ?",
            options: fn (string $value) => strlen($value) > 0 ?
                $this->formatGare($value) :
                [],
            scroll: 10
        );
        $nb_station = text(
            label: "Combien de station comporte la ligne ?"
        );
        $type_ligne = \Laravel\Prompts\select(
            label: "Quel est le type de ligne ?",
            options: ["ter", "tgv", "ic", "transilien"],
            default: "ter"
        );

        table(
            ["Designation", "Valeur"],
            [
                [
                    "designation" => "Hub Affilier",
                    "valeur" => $hub_id
                ],
                [
                    "designation" => "Gare de départ",
                    "valeur" => $gare_depart
                ],
                [
                    "designation" => "Gare d'arrivée",
                    "valeur" => $gare_arrive
                ],
                [
                    "designation" => "Nombre d'arrêts",
                    "valeur" => "$nb_station Arrêts"
                ],
                [
                    "designation" => "Type de Ligne",
                    "valeur" => \Str::upper($type_ligne)
                ],
            ]
        );

        if(confirm("Etes-vous sur de vouloir créer la ligne ?", yes: "Oui", no: "Non")) {
            \Laravel\Prompts\alert("Création de la ligne");
            $this->createLigne(
                $hub_id,
                $gare_depart,
                $gare_arrive,
                $nb_station,
                $type_ligne
            );
            \Laravel\Prompts\alert("Création de la ligne terminée");
        } else {
            error("Création annulée");
        }
    }

    private function formatHubs()
    {
        $hubs = Hub::where('active', true)->get();
        $hubs_array = [];
        foreach($hubs as $hub) {
            $hubs_array[$hub->id] = $hub->gare->name;
        }
        return $hubs_array;
    }

    private function formatGare($value)
    {
        $gares = Gares::where('name', 'like', "%".$value."%")->get();
        $gares_array = [];
        foreach($gares as $gare) {
            $gares_array[$gare->id] = $gare->name;
        }
        return $gares_array;
    }

    private function formatGaresDepart($hub_name)
    {
        $hub = Hub::find($hub_name);
        $gares = Gares::where('name', 'LIKE', "%".$hub_name."%")->get();
        return [$hub->gare->id => $hub->gare->name];
    }

    private function createLigne(int $hub_id, int $gare_depart, int $gare_arrive, string $nb_station, string $type_ligne): void
    {
        $ligne = Ligne::create([
            "nb_station" => $nb_station,
            "price" => 0,
            "distance" => 0,
            "time_min" => 0,
            "station_start_id" => $gare_depart,
            "station_end_id" => $gare_arrive,
            "active" => true,
            "hub_id" => $hub_id,
            "type_ligne" => $type_ligne,
        ]);

        if($ligne->stationStart->hub) {
            LigneRequirement::create([
                "ligne_id" => $ligne->id,
                "hub_id" => Gares::find($gare_depart)->hub->id,
            ]);
        }

        if($ligne->stationEnd->hub) {
            LigneRequirement::create([
                "ligne_id" => $ligne->id,
                "hub_id" => Gares::find($gare_arrive)->hub->id,
            ]);
        }

    }
}
