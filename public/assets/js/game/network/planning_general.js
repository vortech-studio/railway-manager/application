

$.ajax({
    url:'/api/account/'+document.querySelector('#timelinePlanning').getAttribute('data-id')+'/planning',
    success: data => {
        console.log(data);
        google.charts.load('current', {'packages':['timeline']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var container = document.getElementById('timelinePlanning');
            var chart = new google.visualization.Timeline(container);
            var dataTable = new google.visualization.DataTable();

            dataTable.addColumn({ type: 'string', id: 'President' });
            dataTable.addColumn({ type: 'string', id: 'Ligne' });
            dataTable.addColumn({ type: 'string', role: 'tooltip' });
            dataTable.addColumn({ type: 'date', id: 'Start' });
            dataTable.addColumn({ type: 'date', id: 'End' });
            let rows = [];
            data.data.forEach(element => {
                rows.push([element[0], element[1], element[2], moment(element[3]).locale('fr').toDate(), moment(element[4]).locale('fr').toDate()]);
            });

            dataTable.addRows(rows);

            var options = {
                timeline: { colorByRowLabel: true },
                avoidOverlappingGridLines: false
            };

            chart.draw(dataTable, options);
        }
    }
})
