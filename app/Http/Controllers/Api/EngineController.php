<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\Engine;
use App\Models\Core\EngineComposition;
use App\Models\Core\EngineTechnical;
use App\Models\User;
use Illuminate\Http\Request;

class EngineController extends Controller
{
    public function info($engine_id)
    {
        $engine = Engine::find($engine_id)->load('technical');

        return response()->json([
            "engine" => $engine
        ]);
    }

    public function calcPrice(Request $request)
    {
        return match($request->input('type_calcul')) {
            "checkout" => $this->calcPriceCheckout($request),
        };
    }

    public function export()
    {
        $engines = Engine::all();
        $file = fopen(public_path('/storage/engines.json'), 'w');
        fwrite($file, json_encode($engines));
        fclose($file);

        $this->exportTechnical();
        $this->exportEngineComposition();

        return response()->json();
    }

    public function list()
    {
        $engines = Engine::with('technical')->where('in_game', true)->get();

        return response()->json([
            "engines" => $engines
        ]);
    }

    private function exportTechnical()
    {
        $technical = EngineTechnical::all();
        $file = fopen(public_path('/storage/engines_technical.json'), 'w');
        fwrite($file, json_encode($technical));
        fclose($file);
    }

    private function exportEngineComposition()
    {
        $engine_composition = EngineComposition::all();
        $file = fopen(public_path('/storage/engines_composition.json'), 'w');
        fwrite($file, json_encode($engine_composition));
        fclose($file);
    }

    public function import()
    {
        $file = fopen(public_path('/storage/engines.json'), 'r');
        $engines = json_decode(fread($file, filesize(public_path('/storage/engines.json'))), true);
        fclose($file);

        foreach ($engines as $engine) {
            if(Engine::find($engine['id'])) {
                Engine::find($engine['id'])->update([
                    "name" => $engine['name'],
                    "slug" => $engine['slug'],
                    "type_train" => $engine['type_train'],
                    "type_engine" => $engine['type_engine'],
                    "type_energy" => $engine['type_energy'],
                    "velocity" => $engine['velocity'],
                    "nb_passager" => $engine['nb_passager'],
                    "price_achat" => $engine['price_achat'],
                    "price_maintenance" => $engine['price_maintenance'],
                    "duration_maintenance" => $engine['duration_maintenance'],
                    "price_location" => $engine['price_location'],
                    "image" => $engine['image'],
                    "in_shop" => $engine['in_shop'],
                    "in_game" => $engine['in_game'],
                    "price_shop" => $engine['price_shop'],
                    "money_shop" => $engine['money_shop'],
                ]);
            } else {
                Engine::create([
                    "id" => $engine['id'],
                    "name" => $engine['name'],
                    "slug" => $engine['slug'],
                    "type_train" => $engine['type_train'],
                    "type_engine" => $engine['type_engine'],
                    "type_energy" => $engine['type_energy'],
                    "velocity" => $engine['velocity'],
                    "nb_passager" => $engine['nb_passager'],
                    "price_achat" => $engine['price_achat'],
                    "price_maintenance" => $engine['price_maintenance'],
                    "duration_maintenance" => $engine['duration_maintenance'],
                    "price_location" => $engine['price_location'],
                    "image" => $engine['image'],
                    "in_shop" => $engine['in_shop'],
                    "in_game" => $engine['in_game'],
                    "price_shop" => $engine['price_shop'],
                    "money_shop" => $engine['money_shop'],
                ]);
            }
        }

        $this->importEngineTechnical();
        $this->importEngineComposition();
    }

    private function importEngineTechnical()
    {
        $file = fopen(public_path('/storage/engines_technical.json'), 'r');
        $technical = json_decode(fread($file, filesize(public_path('/storage/engines_technical.json'))), true);
        fclose($file);

        foreach ($technical as $engine) {
            if(EngineTechnical::find($engine['id'])) {
                EngineTechnical::find($engine['id'])->update([
                    "longueur" => $engine['longueur'],
                    "largeur" => $engine['largeur'],
                    "hauteur" => $engine['hauteur'],
                    "essieux" => $engine['essieux'],
                    "max_speed" => $engine['max_speed'],
                    "poids" => $engine['poids'],
                    "traction_force" => $engine['traction_force'],
                    "type_motor" => $engine['type_motor'],
                    "power_motor" => $engine['power_motor'],
                    "start_exploi" => $engine['start_exploi'],
                    "end_exploi" => $engine['end_exploi'],
                    "gasoil" => $engine['gasoil'],
                    "type_marchandise" => $engine['type_marchandise'],
                    "nb_type_marchandise" => $engine['nb_type_marchandise'],
                    "engine_id" => $engine['engine_id'],
                    "nb_wagon" => $engine['nb_wagon'],
                ]);
            } else {
                EngineTechnical::create([
                    "id" => $engine['id'],
                    "longueur" => $engine['longueur'],
                    "largeur" => $engine['largeur'],
                    "hauteur" => $engine['hauteur'],
                    "essieux" => $engine['essieux'],
                    "max_speed" => $engine['max_speed'],
                    "poids" => $engine['poids'],
                    "traction_force" => $engine['traction_force'],
                    "type_motor" => $engine['type_motor'],
                    "power_motor" => $engine['power_motor'],
                    "start_exploi" => $engine['start_exploi'],
                    "end_exploi" => $engine['end_exploi'],
                    "gasoil" => $engine['gasoil'],
                    "type_marchandise" => $engine['type_marchandise'],
                    "nb_type_marchandise" => $engine['nb_type_marchandise'],
                    "engine_id" => $engine['engine_id'],
                    "nb_wagon" => $engine['nb_wagon'],
                ]);
            }
        }
    }

    private function importEngineComposition()
    {
        $file = fopen(public_path('/storage/engines_composition.json'), 'r');
        $engine_composition = json_decode(fread($file, filesize(public_path('/storage/engines_composition.json'))), true);
        fclose($file);

        foreach ($engine_composition as $engine) {
            if(EngineComposition::find($engine['id'])) {
                EngineComposition::find($engine['id'])->update([
                    "name" => $engine['name'],
                    "importance" => $engine['importance'],
                    "maintenance_check_time" => $engine['maintenance_check_time'],
                    "maintenance_repar_time" => $engine['maintenance_repar_time'],
                    "engine_id" => $engine['engine_id'],
                    "type" => $engine['type']
                ]);
            } else {
                EngineComposition::create([
                    "id" => $engine['id'],
                    "name" => $engine['name'],
                    "importance" => $engine['importance'],
                    "maintenance_check_time" => $engine['maintenance_check_time'],
                    "maintenance_repar_time" => $engine['maintenance_repar_time'],
                    "engine_id" => $engine['engine_id'],
                    "type" => $engine['type']
                ]);
            }
        }
    }

    private function calcPriceCheckout(Request $request)
    {
        $surface = Engine::calcSurface($request->input('longueur'), $request->input('largeur'), $request->input('hauteur'));
        $traction_force = Engine::calcTractionForce($request->input('poids'));
        $power_traction = $request->input('puissance');

        $coef_essieux = Engine::getCoefTypeEssieux($request->input('essieux'));
        $coef_motorisation = Engine::getCoefTypeMotorisation($request->input('type_motor'));
        $coef_marchandise = Engine::getCoefTypeMarchandise($request->input('type_marchandise'));
        $coef_type_train = Engine::getCoefTypeTrain($request->input('type_engine'));

        $price = ($surface * $traction_force * $power_traction) * ($coef_essieux * $coef_motorisation * $coef_marchandise * $coef_type_train);

        return response()->json([
            "price" => eur($price),
            "price_normal" => $price
        ]);
    }

    public function available(Request $request, $user_id)
    {
        $user = User::find($user_id);

        if($request->has('type_ligne')) {
            $engs = [];
            $engines = $user->engines()
                ->where('available', true)
                ->where('active', true)
                ->get();

            foreach ($engines as $engine) {
                if($engine->engine->type_train == $request->input('type_ligne')) {
                    $engs[] = $engine;
                }
            }
        } else {
            $engs = $user->engines()
                ->where('available', true)
                ->where('active', true)
                ->get();
        }

        return response()->json([
            "engines" => collect($engs)
        ]);
    }

    public function charts(Request $request, $user_id)
    {
        return match ($request->get('type')) {
            "usure" => $this->getChartUsure($user_id),
            "ancien" => $this->getChartAncien($user_id),
        };
    }

    private function getChartUsure($user_id)
    {
        $useds = collect();
        $useds->push(["Usure", "value"]);

        $low_engines = User::find($user_id)->engines()->where('use_percent', '<=', 30)->get()->count();
        $half_engines = User::find($user_id)->engines()->where('use_percent', '>', 30)->where('use_percent', '<=', 70)->get()->count();
        $high_engines = User::find($user_id)->engines()->where('use_percent', '>=', 70)->get()->count();

        $useds->push([
            "name" => "Usure Basse [0-30%]",
            "value" => $low_engines
        ]);

        $useds->push([
            "name" => "Usure moyenne [30-70%]",
            "value" => $half_engines
        ]);

        $useds->push([
            "name" => "Usure Haute [70-100%]",
            "value" => $high_engines
        ]);


        return response()->json($useds->toArray());
    }

    private function getChartAncien($user_id)
    {
        $useds = collect();
        $useds->push(["Ancien", "value"]);

        $new_engine = User::find($user_id)->engines()->where('ancien', "<=", 1)->get()->count();
        $recent_engine = User::find($user_id)->engines()->where('ancien', ">=", 1)->where('ancien', "<=", 2)->get()->count();
        $half_aged_engine = User::find($user_id)->engines()->where('ancien', ">=", 2)->where('ancien', "<=", 3)->get()->count();
        $aged_engine = User::find($user_id)->engines()->where('ancien', ">=", 3)->where('ancien', "<=", 4)->get()->count();
        $new_age_engine = User::find($user_id)->engines()->where('ancien', ">=", 4)->where('ancien', "<", 5)->get()->count();
        $old_engine = User::find($user_id)->engines()->where('ancien', ">=", 5)->get()->count();

        $useds->push([
            "name" => "Nouveau [0-1]",
            "value" => $new_engine
        ]);

        $useds->push([
            "name" => "Recent [1-2]",
            "value" => $recent_engine
        ]);

        $useds->push([
            "name" => "Plutôt Agé [2-3]",
            "value" => $half_aged_engine
        ]);

        $useds->push([
            "name" => "Age [3-4]",
            "value" => $aged_engine
        ]);

        $useds->push([
            "name" => "Agé + [4-5]",
            "value" => $new_age_engine
        ]);

        $useds->push([
            "name" => "Ancien [5]",
            "value" => $old_engine
        ]);

        return response()->json($useds->toArray());
    }
}
