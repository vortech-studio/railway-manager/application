<?php

namespace App\Http\Controllers\Game;

use App\Http\Controllers\Controller;
use App\Models\Core\Engine;
use App\Models\User\UserCard;

class CardController extends Controller
{
    public function show($uuid)
    {
        $card = UserCard::where('uuid', $uuid)->firstOrFail();

        return view('game.shop.card', compact('card'));
    }

    public function receive($uuid)
    {
        $card = UserCard::with('cards', 'user')->where('uuid', $uuid)->firstOrFail();

        foreach ($card->cards()->with('cardHolder')->get() as $item) {
            match ($item->cardHolder->type_avantage) {
                "argent" => $this->putArgent($card->user, $item->cardHolder->value),
                "research" => $this->putResearch($card->user, $item->cardHolder->value),
                "rate_research" => $this->putRateResearch($card->user, $item->cardHolder->value),
                "credit_impot" => $this->putCreditImpot($card->user, $item->cardHolder->value),
                "engin" => $this->putEngin($card->user, $item->cardHolder->value),
                "audit_int" => $this->putAuditInt($card->user, $item->cardHolder->value),
                "audit_ext" => $this->putAuditExt($card->user, $item->cardHolder->value),
                "simulation" => $this->putSimulationInt($card->user, $item->cardHolder->value),
            };
        }

        $card->check_advantage = true;
        $card->save();

        return response()->json([
            "success" => true,
            "message" => "Vos avantages ont bien été pris en comptes !"
        ]);
    }

    private function putArgent(mixed $user, $value)
    {
        $user->update([
            "argent" => $user->argent + $value
        ]);
    }

    private function putResearch(mixed $user, $value)
    {
        $user->update([
            "research" => $user->research + $value
        ]);
    }

    private function putRateResearch(mixed $user, $value)
    {
        $user->company->update([
            "rate_research" => $user->company->rate_research + $value
        ]);
    }

    private function putCreditImpot(mixed $user, $value)
    {
        $user->company->update([
            "credit_impot" => $user->company->credit_impot + $value
        ]);
    }

    private function putEngin(mixed $user, $value)
    {
        $engin = Engine::find($value);

        if($user->engines()->where('engine_id', $value)->get()->count() != 0) {
            $user->engines()->create([
                "max_runtime_engine" => $engin->calcMaxRuntimeEngine(),
                "user_id" => $user->id,
                "engine_id" => $engin->id,
                "user_hub_id" => $user->hubs()->first()->id,
            ]);
        }
    }

    private function putAuditInt(mixed $user, $value)
    {
        $user->card_bonus->update([
            "audit_int_offer" => $user->card_bonus->audit_int_offer + $value
        ]);
    }

    private function putAuditExt(mixed $user, $value)
    {
        $user->card_bonus->update([
            "audit_ext_offer" => $user->card_bonus->audit_ext_offer + $value
        ]);
    }

    private function putSimulationInt(mixed $user, $value)
    {
        $user->card_bonus->update([
            "sim_offer" => $user->card_bonus->sim_offer + $value
        ]);
    }


}
