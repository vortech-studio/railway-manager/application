<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class OtherDatabaseCommand extends Command
{
    protected $signature = 'other:database';

    protected $description = 'Command description';

    public function handle(): void
    {
        $this->call('migrate', ['--path' => '/database/migrations/2023_1/']);
    }
}
