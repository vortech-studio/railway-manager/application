<?php

namespace App\Models\User;

use App\Models\Core\CardHolder;
use Illuminate\Database\Eloquent\Model;

class UserCardContent extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function userCard()
    {
        return $this->belongsTo(UserCard::class);
    }

    public function cardHolder()
    {
        return $this->belongsTo(CardHolder::class);
    }
}
