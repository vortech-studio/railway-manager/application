<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardHolderCategoriesTable extends Migration
{
    public function up(): void
    {
        Schema::create('card_holder_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('card_holder_categories');
    }
}
