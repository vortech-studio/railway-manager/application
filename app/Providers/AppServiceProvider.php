<?php

namespace App\Providers;

use App\Models\Core\Setting;
use App\Services\Jira\JiraService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Agent\Agent;
use Jira\Laravel\Facades\Jira;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        if ($this->app->environment('local')) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        config([
            'global' => Setting::all([
                'name', 'value',
            ])
                ->keyBy('name')
                ->transform(function ($setting) {
                    return $setting->value;
                })
                ->toArray(),
        ]);

        Schema::defaultStringLength(191);

        \Blade::if('premium', function () {
            return auth()->user()->premium;
        });

        \Blade::if('mobile', function () {
            $agent = new Agent();
            return $agent->isiPhone() || $agent->isAndroidOS();
        });

    }
}
