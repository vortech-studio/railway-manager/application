<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCompanySituationsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_company_situations', function (Blueprint $table) {
            $table->id();
            $table->timestamp('date');
            $table->decimal('turnover');
            $table->decimal('cost_travel');
            $table->decimal('remb_emprunt');
            $table->decimal('cost_location');
            $table->decimal('structural_cash');
            $table->decimal('benefice');

            $table->foreignId('user_company_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_company_situations');
    }
}
