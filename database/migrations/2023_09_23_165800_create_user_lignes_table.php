<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLignesTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_lignes', function (Blueprint $table) {
            $table->id();
            $table->timestamp('date_achat');
            $table->integer('nb_depart_jour');
            $table->integer('quai');
            $table->boolean('active')->default(false);

            $table->foreignId('user_hub_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('ligne_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_engine_id')
                ->nullable()
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_lignes');
    }
}
