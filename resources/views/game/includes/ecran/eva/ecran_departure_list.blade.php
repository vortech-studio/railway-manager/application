@mobile
    <div class="border border-radius-5 border-2 p-5 mb-5">
        <h3 class="fw-bolder">Prochains Départs</h3>
        <div class="contentLoadMobile">

        </div>
    </div>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', () => {
            let contentLoadMobile = document.querySelector('.contentLoadMobile')
            let blockContentLoadMobile = new KTBlockUI(contentLoadMobile, {
                overlayClass: 'bg-gray-500 bg-opacity-50'
            })

            blockContentLoadMobile.block()

            $.ajax({
                url: '/api/game/ecran/departure/list',
                data: {"user_id": {{ auth()->user()->id }}},
                success: function (data) {
                    blockContentLoadMobile.release()
                    contentLoadMobile.innerHTML = data.content.mobile
                }
            })

            setInterval(() => {
                $.ajax({
                    url: '/api/game/ecran/departure/list',
                    data: {"user_id": {{ auth()->user()->id }}},
                    success: function (data) {
                        blockContentLoadMobile.release()
                        contentLoadMobile.innerHTML = data.content.mobile
                    }
                })
            }, 10000)
        })
    </script>
@else
    <div class="d-flex flex-column bg-color-eva border border-5 border-primary rounded-3 shadow-lg ">
        <div class="d-flex flex-row align-items-center mb-2 p-5">
            <img src="{{ asset('storage/icons/station_hub.png') }}" class="w-75px me-2" alt="">
            <div class="fs-3x text-white fw-bolder">Prochains Départs</div>
        </div>
        <div class="contentLoadDesktop p-5">

        </div>
        <div class="d-flex flex-row justify-content-between align-items-center h-75px p-0 m-0 bg-gray-300">
            <div class=""></div>
            <div class="d-flex flex-row bg-color-eva text-white border border-2 border-white p-5 justify-content-center align-items-end rounded-3">
                <span id="clock-hours" class="fs-1">23</span>
                <span class="animation-blink">
                            <span class="animation-blink-clock fs-1">:</span>
                        </span>
                <span id="clock-minutes" class="fs-1">32</span>
                <small id="clock-seconds" class="fs-3 ms-1 text-orange-600">37</small>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', () => {
            let contentLoadDesktop = document.querySelector('.contentLoadDesktop')
            let blockContentLoadDesktop = new KTBlockUI(contentLoadDesktop, {
                overlayClass: 'bg-gray-500 bg-opacity-50'
            })

            blockContentLoadDesktop.block()

            $.ajax({
                url: '/api/game/ecran/departure/list',
                data: {"user_id": {{ auth()->user()->id }}},
                success: function (data) {
                    blockContentLoadDesktop.release()
                    contentLoadDesktop.innerHTML = data.content.desktop
                    let tooltips = document.querySelectorAll('[data-bs-toggle="tooltip"]')
                    tooltips.forEach(tooltip => {
                        new bootstrap.Tooltip(tooltip)
                    })
                }
            })

            setInterval(() => {
                $.ajax({
                    url: '/api/game/ecran/departure/list',
                    data: {"user_id": {{ auth()->user()->id }}},
                    success: function (data) {
                        blockContentLoadDesktop.release()
                        contentLoadDesktop.innerHTML = data.content.desktop
                        let tooltips = document.querySelectorAll('[data-bs-toggle="tooltip"]')
                        tooltips.forEach(tooltip => {
                            new bootstrap.Tooltip(tooltip)
                        })
                    }
                })
            }, 10000)
        })
    </script>
@endmobile


