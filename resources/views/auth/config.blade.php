<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic
Product Version: 8.1.8
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="{{ config('app.locale') }}">
<!--begin::Head-->
<head><base href="../../../"/>
    {{ seo()->render() }}
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="app-blank bgi-size-cover bgi-attachment-fixed bgi-position-center">
<!--begin::Theme mode setup on page load-->
<script>var defaultThemeMode = "light"; var themeMode; if ( document.documentElement ) { if ( document.documentElement.hasAttribute("data-bs-theme-mode")) { themeMode = document.documentElement.getAttribute("data-bs-theme-mode"); } else { if ( localStorage.getItem("data-bs-theme") !== null ) { themeMode = localStorage.getItem("data-bs-theme"); } else { themeMode = defaultThemeMode; } } if (themeMode === "system") { themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light"; } document.documentElement.setAttribute("data-bs-theme", themeMode); }</script>
<!--end::Theme mode setup on page load-->
<!--begin::Root-->
<div class="d-flex flex-column flex-root" id="kt_app_root">
    <!--begin::Page bg image-->
    <style>body { background-image: url('/storage/other/wall-rm.png'); } [data-bs-theme="dark"] body { background-image: url('/storage/other/wall-rm.png'); }</style>
    <!--end::Page bg image-->
    <!--begin::Authentication - Sign-in -->
    <div class="d-flex flex-column flex-lg-row flex-column-fluid">
        <!--begin::Aside-->
        <div class="d-flex flex-lg-row-fluid w-50">
            <!--begin::Content-->
            <div class="d-flex flex-column flex-center pb-0 pb-lg-10 p-10 w-100 bg-white bg-opacity-75">
                <div class="d-flex flex-row p-5 mb-10 shadow-lg">
                    <div class="symbol symbol-55px symbol-circle me-5">
                        <img src="/assets/media/avatars/300-1.jpg" alt="">
                    </div>
                    <div class="d-flex flex-column">
                        <div class="fs-1 fw-bold">Bonjour {{ $user->name }} et bienvenue sur {{ config('app.name') }} !</div>
                        <p>
                            Je me nomme {{ $user->name_conseiller }} et je serai votre nouveau conseiller.<br>
                            Vous êtes notre nouveau PDG en devenir de l'empire ferroviaire le plus ouvert sur le monde.<br>
                            Mais avant de commencer à travailler, certaines informations son obligatoire avant de découvrir votre nouvelle
                            structure.
                        </p>
                    </div>
                </div>
            </div>
            <!--end::Content-->
        </div>
        <!--begin::Aside-->
        <!--begin::Body-->
        <div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12 bg-white bg-opacity-50 w-75">
            <!--begin::Wrapper-->
            <div class="bg-body d-flex flex-column flex-center rounded-4 w-md-1000px p-10">
                <!--begin::Content-->
                <div class="d-flex flex-center flex-column align-items-stretch h-lg-100 w-md-800px">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-center flex-column flex-column-fluid pb-15 pb-lg-20">
                        <!--begin::Form-->
                        <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="{{ route('oauth.configStore') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <!--begin::Accordion-->
                            <div class="accordion mb-10" id="kt_accordion_1">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="kt_accordion_1_header_1">
                                        <button class="accordion-button fs-4 fw-semibold" type="button" data-bs-toggle="collapse" data-bs-target="#direction" aria-expanded="true" aria-controls="kt_accordion_1_body_1">
                                            En savoir plus sur vous et la société
                                        </button>
                                    </h2>
                                    <div id="direction" class="accordion-collapse collapse show" aria-labelledby="kt_accordion_1_header_1" data-bs-parent="#kt_accordion_1">
                                        <div class="accordion-body">
                                            <div class="row">
                                                <div class="col-6">
                                                    <x-base.underline
                                                        title="Logo de la société"
                                                        size="3"
                                                        size-text="fs-3"/>
                                                    <x-form.image-input
                                                        name="logo_company"
                                                        accept="jpg,png,jpeg,gif,svg" />
                                                    <x-form.input
                                                        name="name_company"
                                                        label="Nom de la société"
                                                        required="true" />

                                                </div>
                                                <div class="col-6">
                                                    <x-base.underline
                                                        title="Votre avatar"
                                                        size="3"
                                                        size-text="fs-3"/>

                                                    <x-form.image-input
                                                        name="avatar"
                                                        accept="jpg,png,jpeg,gif" />

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="kt_accordion_1_header_2">
                                        <button class="accordion-button fs-4 fw-semibold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#secretary" aria-expanded="false" aria-controls="kt_accordion_1_body_2">
                                            Votre secrétaire
                                        </button>
                                    </h2>
                                    <div id="secretary" class="accordion-collapse collapse" aria-labelledby="kt_accordion_1_header_2" data-bs-parent="#kt_accordion_1">
                                        <div class="accordion-body">
                                            <x-base.underline
                                                title="Photo de profil de votre secrétaire"
                                                size="3"
                                                size-text="fs-3"/>

                                            <x-form.image-input
                                                name="avatar_secretary"
                                                accept="jpg,png,jpeg,gif" />

                                            <x-form.input
                                                name="name_secretary"
                                                label="Nom de votre secrétaire"
                                                required="true" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--end::Accordion-->
                            <div class="d-flex flex-end">
                                <x-form.button />
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Wrapper-->

                </div>
                <!--end::Content-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Authentication - Sign-in-->
</div>
<!--end::Root-->
<!--begin::Javascript-->
<script>var hostUrl = "assets/";</script>
<!--begin::Global Javascript Bundle(mandatory for all pages)-->
<script src="assets/plugins/global/plugins.bundle.js"></script>
<script src="assets/js/scripts.bundle.js"></script>
<!--end::Global Javascript Bundle-->
<!--end::Javascript-->
</body>
<!--end::Body-->
</html>
