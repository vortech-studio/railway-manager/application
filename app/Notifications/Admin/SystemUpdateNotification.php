<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class SystemUpdateNotification extends Notification
{
    use Queueable;
    public function __construct(public string $message, public string $type = "null")
    {
    }

    public function via($notifiable): array
    {
        return ['slack', 'broadcast'];
    }

    public function toSlack($notifiable): SlackMessage
    {
        return match($this->type) {
            'error' => (new SlackMessage())
                ->error()
                ->content($this->message),
            'success' => (new SlackMessage())
                ->success()
                ->content($this->message),
            default => (new SlackMessage())
                ->content($this->message),
        };
    }

    public function toArray($notifiable): array
    {
        return [];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'type' => 'info',
            'icon' => 'ki-abstract-4',
            'title' => "System Update",
            'description' => $this->message,
            'time' => now(),
            'userId' => 1,
        ]);
    }

    public function broadcastOn(): array
    {
        return ['admin-channel'];
    }
}
