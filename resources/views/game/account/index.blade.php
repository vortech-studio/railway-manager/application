@extends("game.template")

@section("css")

@endsection

@section("info_bar")

@endsection

@section("content")
    <div class="card shadow-lg">
        <div class="card-header">
            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x my-5 fs-4">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#infos">Informations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#kt_tab_pane_5">Link 2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#kt_tab_pane_6">Link 3</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="infos" role="tabpanel">
                    <div class="d-flex flex-row justify-content-around mb-5">
                        <div class="d-flex flex-row me-3">
                            <span>Date d'inscription:</span>
                            <div class="fw-bolder">{{ auth()->user()->created_at->format("d/m/Y") }}</div>
                        </div>
                        <div class="d-flex flex-row me-3">
                            <span>Email d'inscription:</span>
                            <div class="fw-bolder">{{ auth()->user()->email }}</div>
                        </div>
                        <div class="d-flex flex-row me-3">
                            <span>ID de la companie:</span>
                            <div class="fw-bolder">{{ auth()->user()->company->id }}</div>
                        </div>
                    </div>
                    <div class="separator border-gray-300 my-10"></div>
                    <div class="d-flex flex-row justify-content-around mb-5">
                        <div class=" w-50 border-2 border-gray-500 bg-gray-300 rounded-3 p-5 me-3">
                            <x-base.underline
                                title="Modification du mot de passe"
                                size="3"
                                size-text="fs-3"
                                class="w-300px mt-5 mb-5" />
                            <form action="" method="post" id="updatePassword">
                                @csrf
                                <x-form.input
                                    type="password"
                                    name="password"
                                    label="Mot de passe"
                                    required="true" />

                                <x-form.input
                                    type="password"
                                    name="password_confirmation"
                                    label="Confirmation du mot de passe"
                                    required="true" />

                                <x-form.button />
                            </form>
                        </div>
                        <div class=" w-50 border-2 border-gray-500 bg-gray-300 rounded-3 p-5 me-3">
                            <x-base.underline
                                title="Modification de l'email"
                                size="3"
                                size-text="fs-3"
                                class="w-300px mt-5 mb-5" />
                            <form action="" method="post" id="updateEmail">
                                @csrf
                                <x-form.input
                                    type="email"
                                    name="old_email"
                                    label="Ancien email"
                                    required="true" />

                                <x-form.input
                                    type="email"
                                    name="email"
                                    label="Nouveau email"
                                    required="true" />

                                <x-form.button />
                            </form>
                        </div>
                    </div>

                    <div class="separator border-gray-300 my-10"></div>

                    <div class="w-100 border-2 border-gray-500 bg-gray-300 rounded-3 p-5">
                        <x-base.underline
                            title="Code d'échange"
                            size="3"
                            size-text="fs-3"
                            class="w-300px mt-5 mb-5" />
                        <form action="" method="post" id="exchangeCode">
                            @csrf
                            <x-form.input
                                type="text"
                                name="code"
                                label="Code d'échange"
                                required="true"
                                text="Entrez le code promotion pour obtenir votre gain." />

                            <x-form.button text="Obtenez votre gain"/>
                        </form>
                    </div>

                    <div class="separator border-gray-300 my-10"></div>
                    <!--begin::Alert-->
                    <div class="alert alert-danger d-flex flex-row justify-content-between align-items-center p-5">
                        <div class="d-flex flex-row align-items-center p-5">
                            <!--begin::Icon-->
                            <i class="fa-solid fa-exclamation-triangle fs-2hx text-danger me-4"></i>
                            <!--end::Icon-->

                            <!--begin::Wrapper-->
                            <div class="d-flex flex-column">
                                <!--begin::Title-->
                                <h4 class="mb-1 text-danger">Réinitialisation de votre compte</h4>
                                <!--end::Title-->

                                <!--begin::Content-->
                                <span>La réinitialisation de votre compte est définitive et toutes vos données de jeux vont être réinitialiser.</span>
                                <!--end::Content-->
                            </div>
                        </div>


                        <div class="d-flex flex-end">
                            <button class="btn btn-danger" id="btnResetAccount">Réinitialiser mon compte </button>
                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Alert-->
                </div>
                <div class="tab-pane fade" id="kt_tab_pane_5" role="tabpanel">
                    ...
                </div>
                <div class="tab-pane fade" id="kt_tab_pane_6" role="tabpanel">
                    ...
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        $("#updatePassword").on('submit', e => {
            e.preventDefault()
            let form = $("#updatePassword")
            let data = form.serialize()
            let btn = form.find("button")
            let url = "/api/account/{{ auth()->user()->id }}/update-password"

            $.ajax({
                url: url,
                data: data,
                type: "POST",
                beforeSend: () => {
                    btn.attr("data-kt-indicator", true)
                },
                success: (res) => {
                    btn.removeAttr("data-kt-indicator")
                    toastr.success(res.message)
                    form.trigger("reset")
                },
                error: (err) => {
                    btn.removeAttr("data-kt-indicator")
                    toastr.error(err.responseJSON.message)
                }
            })
        })
        $("#updateEmail").on('submit', e => {
            e.preventDefault()
            let form = $("#updateEmail")
            let data = form.serialize()
            let btn = form.find("button")
            let url = "/api/account/{{ auth()->user()->id }}/update-email"

            $.ajax({
                url: url,
                data: data,
                type: "POST",
                beforeSend: () => {
                    btn.attr("data-kt-indicator", true)
                },
                success: (res) => {
                    btn.removeAttr("data-kt-indicator")
                    toastr.success(res.message)
                    form.trigger("reset")
                },
                error: (err) => {
                    btn.removeAttr("data-kt-indicator")
                    toastr.error(err.responseJSON.message)
                }
            })
        })
        $("#exchangeCode").on('submit', e => {
            e.preventDefault()
            let form = $("#exchangeCode")
            let data = form.serialize()
            let btn = form.find("button")
            let url = "/api/account/{{ auth()->user()->id }}/exchange-code"

            $.ajax({
                url: url,
                data: data,
                type: "POST",
                beforeSend: () => {
                    btn.attr("data-kt-indicator", true)
                },
                success: (res) => {
                    btn.removeAttr("data-kt-indicator")
                    toastr.success(res.message)
                    form.trigger("reset")
                },
                error: (err) => {
                    btn.removeAttr("data-kt-indicator")
                    toastr.error(err.responseJSON.message)
                }
            })
        })

        document.querySelector('#btnResetAccount').addEventListener('click', e => {
            e.preventDefault()

            Swal.fire({
                title: 'Êtes-vous sûr?',
                text: "Vous ne pourrez pas revenir en arrière!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Oui, réinitialiser!',
                cancelButtonText: 'Non, annuler!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    let url = "/api/account/{{ auth()->user()->id }}/reset-account"
                    let btn = $("#btnResetAccount")
                    $.ajax({
                        url: url,
                        type: "POST",
                        beforeSend: () => {
                            btn.attr("data-kt-indicator", true)
                        },
                        success: (res) => {
                            btn.removeAttr("data-kt-indicator")
                            toastr.success(res.message)
                            setTimeout(() => {
                                window.location.href = "/auth/config"
                            }, 2000)
                        },
                        error: (err) => {
                            btn.removeAttr("data-kt-indicator")
                            toastr.error(err.responseJSON.message)
                        }
                    })
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    Swal.fire(
                        'Annulé',
                        'Votre compte n\'a pas été réinitialisé :)',
                        'error'
                    )
                }
            })
        })
    </script>
@endsection
