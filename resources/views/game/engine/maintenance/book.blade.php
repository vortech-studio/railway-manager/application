@extends("game.template")

@section("css")
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/technician.png') }}" alt="">
            </div>
            <x-base.underline
                title="CARNET D'ENTRETIEN"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.engine.partials.menu")
    </div>
    <div class="card shadow-lg bg-green-900 bg-gradient-to-b text-white mb-10">
        <div class="card-body">
            <div class="row">
                <div class="col-9">
                    <div id="latestIncident" style="height: 200px"></div>
                </div>
                <div class="col-3 align-self-center justify-self-center">
                    <div class="d-flex flex-column align-items-center bg-gray-900 text-white fs-9 p-10 mb-5">
                        <span class="text-center pb-2">MONTANT DES RÉPARATIONS IMPRÉVUES DEPUIS 24H</span>
                        <span class="fs-7 fw-bold">{{ eur(auth()->user()->incidents()->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])->sum('amount_reparation')) }}</span>
                    </div>
                    <a href="{{ route('engine.maintenance.history') }}" class="btn btn-sm btn-primary btn-flex align-items-center">
                        <img src="{{ asset('/storage/icons/train_incident.png') }}" alt="" class="w-35px me-2">
                        <span class="">Historique des incidents</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-10">
        <div class="col-6">
            <div class="card shadow-lg bg-gray-400 text-white card-stretch h-500px">
                <div class="card-header">
                    <div class="card-title">
                        <img src="{{ asset('/storage/icons/train.png') }}" alt="Hub" class="w-30px h-30px me-3">
                        <span class="">RÉPARTITION DES ENGINS PAR USURE</span>
                    </div>
                </div>
                <div class="card-body">
                    <x-base.alert
                        type="light"
                        color="primary"
                        icon="info-circle"
                        title="Informations"
                        content="31.3 % de vos avions sont usés ou très usés. Vous devriez les réparer rapidement" />

                    <div id="usedEngines" style="height: 250px"></div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card shadow-lg bg-gray-400 text-white card-stretch h-500px">
                <div class="card-header">
                    <div class="card-title">
                        <img src="{{ asset('/storage/icons/train.png') }}" alt="Hub" class="w-30px h-30px me-3">
                        <span class="">RÉPARTITION DES ENGINS PAR ANCIENNETE</span>
                    </div>
                </div>
                <div class="card-body">
                    <x-base.alert
                        type="light"
                        color="primary"
                        icon="info-circle"
                        title="Informations"
                        content="75 % de vos appareils n'ont pas eu de grande visite (Maintenance curative) depuis trop longtemps. Ils s'usent donc plus rapidement." />

                    <div id="ancienEngines" style="height: 250px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="card shadow-lg bg-gray-400 text-white card-stretch mb-10">
        <div class="card-header">
            <div class="card-title">
                <img src="{{ asset('/storage/icons/monitoring.png') }}" alt="Hub" class="w-30px h-30px me-3">
                <span class="">Maintenances / Réparations</span>
            </div>
        </div>
        <div class="card-body">
            <div class="d-flex flex-row justify-content-around align-items-center">
                <a href="" class="btn btn-primary rounded-4">Réparation Individuelle</a>
                <a href="{{ route('engine.maintenance.group') }}" class="btn btn-primary rounded-4">Réparation Groupée</a>
            </div>
        </div>
    </div>
    @premium
    <div class="card shadow-lg bg-gray-400 text-white card-stretch mb-10">
        <div class="card-header">
            <div class="card-title">
                <img src="{{ asset('/storage/icons/premium.png') }}" alt="Hub" class="w-30px h-30px me-3">
                <span class="">Maintenances / Réparations Automatique</span>
            </div>
        </div>
        <div class="card-body">
            <div class="d-flex flex-row justify-content-around align-items-center">
                @mobile
                    <button class="btn btn-primary rounded-4" data-bs-toggle="modal" data-bs-target="#modal_auto_maintenance">Réparation Automatique</button>
                @else
                    <a href="" id="btnAutoMaintenance" class="btn btn-primary rounded-4" data-kt-toggle="true" data-kt-toggle-state="d-block" data-kt-toggle-target="#autoMaintenance">Réparation Automatique</a>
                @endmobile
            </div>
        </div>
    </div>
    <div
        id="auto_maintenance"
        class=""
        data-kt-drawer="true"
        data-kt-drawer-activate="true"
        data-kt-drawer-toggle="#btnAutoMaintenance"
        data-kt-drawer-overlay="true"
        data-kt-drawer-close="#kt_drawer_example_dismiss_close"
        data-kt-drawer-width="{default:'100%', 'md': '75%'}">

        <div class="card w-100">
            <div class="card-header">
                <h3 class="card-title">Configuration de la réparation/maintenance automatique</h3>
                <div class="card-toolbar">
                    <button class="btn btn-flush btn-hover-danger btn-icon" data-kt-drawer-dismiss="true"><i class="fa-solid fa-times fs-2"></i> </button>
                </div>
            </div>
            <div class="card-body">
                <div class="alert alert-dismissible bg-light-primary border border-primary d-flex flex-column flex-sm-row align-items-center p-5 mb-10">
                    <div class="symbol symbol-70px symbol-circle me-3">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}" alt="">
                    </div>

                    <div class="d-flex flex-column pe-0 pe-sm-10">
                        <h5 class="mb-1">Information</h5>
                        <p class="text-gray-700">
                            Sélectionnez le pourcentage d'usure à partir duquel vos engins seront réparés automatiquement. Lorsque vous avez terminé, cliquez sur la case correspondante pour activer la maintenance automatique, puis sur le bouton "VALIDER" pour confirmer votre choix.
                        </p>
                        <p class="text-danger fs-italic">
                            Attention, si vous ne possédez pas assez de fonds pour financer la maintenance automatique, celle-ci n'aura pas lieu. La maintenance automatique reprendra aussitôt que les fonds nécessaires seront disponibles.
                        </p>
                    </div>

                    <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                        <i class="ki-duotone ki-cross fs-1 text-success"><span class="path1"></span><span class="path2"></span></i>
                    </button>
                </div>
                <form action="{{ route('engine.maintenance.group.autoMaintenance') }}" method="POST">
                    @csrf
                    <div class="d-flex flex-row justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center w-25">
                            <div class="form-check form-check-custom form-check-ter form-check-solid me-10">
                                <input class="form-check-input h-40px w-40px" type="checkbox" @if(auth()->user()->setting->auto_ter) checked="checked" @endif name="ter" value="1" id="flexCheckbox40"/>
                                <label class="form-check-label" for="flexCheckbox40">
                                    <div class="symbol symbol-150px me-5">
                                        <img src="{{ asset('/storage/logos/logo_ter.svg') }}" alt="Logo" />
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="w-75">
                            <div class="text-center fs-2 fw-bolder">Usure de l'engin</div>
                            <div class="rounded pt-15 px-5 pb-5 shadow-box bg-gray-800 border border-3 border-gray-600 text-white mb-5">
                                <div class="mb-0">
                                    <div id="slide_used_ter"></div>
                                    <input type="hidden" name="wear_ter" value="0">

                                    <div class="d-flex flex-row justify-content-between pt-15">
                                        <div class="fw-semibold mb-2 fs-2">Très Bon Etat</div>
                                        <div class="mb-2 fs-6 w-25">Sélectionne tous les appareils dont le taux d'usure est entre <span class="text-blue-400 fw-bolder" id="used_slide_ter">20</span> % et 100 %</div>
                                        <div class="fw-semibold mb-2 fs-2">Très Mauvais Etat</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="separator border-gray-800 my-5"></div>
                    <div class="d-flex flex-row justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center w-25">
                            <div class="form-check form-check-custom form-check-tgv form-check-solid me-10">
                                <input class="form-check-input h-40px w-40px" type="checkbox" @if(auth()->user()->setting->auto_tgv) checked="checked" @endif name="tgv" value="1" id="flexCheckbox40"/>
                                <label class="form-check-label" for="flexCheckbox40">
                                    <div class="symbol symbol-150px me-5">
                                        <img src="{{ asset('/storage/logos/logo_tgv.svg') }}" alt="Logo" />
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="w-75">
                            <div class="text-center fs-2 fw-bolder">Usure de l'engin</div>
                            <div class="rounded pt-15 px-5 pb-5 shadow-box bg-gray-800 border border-3 border-gray-600 text-white mb-5">
                                <div class="mb-0">
                                    <div id="slide_used_tgv"></div>
                                    <input type="hidden" name="wear_tgv" value="0">

                                    <div class="d-flex flex-row justify-content-between pt-15">
                                        <div class="fw-semibold mb-2 fs-2">Très Bon Etat</div>
                                        <div class="mb-2 fs-6 w-25">Sélectionne tous les appareils dont le taux d'usure est entre <span class="text-blue-400 fw-bolder" id="used_slide_tgv">20</span> % et 100 %</div>
                                        <div class="fw-semibold mb-2 fs-2">Très Mauvais Etat</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="separator border-gray-800 my-5"></div>
                    <div class="d-flex flex-row justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center w-25">
                            <div class="form-check form-check-custom form-check-ic form-check-solid me-10">
                                <input class="form-check-input h-40px w-40px" type="checkbox" name="ic" @if(auth()->user()->setting->auto_ter) checked="checked" @endif value="1" id="flexCheckbox40"/>
                                <label class="form-check-label" for="flexCheckbox40">
                                    <div class="symbol symbol-150px me-5">
                                        <img src="{{ asset('/storage/logos/logo_intercite.svg') }}" alt="Logo" />
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="w-75">
                            <div class="text-center fs-2 fw-bolder">Usure de l'engin</div>
                            <div class="rounded pt-15 px-5 pb-5 shadow-box bg-gray-800 border border-3 border-gray-600 text-white mb-5">
                                <div class="mb-0">
                                    <div id="slide_used_ic"></div>
                                    <input type="hidden" name="wear_ic" value="0">

                                    <div class="d-flex flex-row justify-content-between pt-15">
                                        <div class="fw-semibold mb-2 fs-2">Très Bon Etat</div>
                                        <div class="mb-2 fs-6 w-25">Sélectionne tous les appareils dont le taux d'usure est entre <span class="text-blue-400 fw-bolder" id="used_slide_ic">20</span> % et 100 %</div>
                                        <div class="fw-semibold mb-2 fs-2">Très Mauvais Etat</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="my-5"></div>
                    <div class="d-flex flex-wrap justify-content-center">
                        <button type="submit" class="btn btn-primary rounded-4 w-50">Valider</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    @endpremium
@endsection

@section("script")
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        $.ajax({
            url: '{{ route('api.account.compta.getChartInfo', auth()->user()->id) }}',
            data: {
                "type": "incident"
            },
            success: resultat => {
                let chartDom = document.getElementById('latestIncident');
                let chartLatestIncidents = echarts.init(chartDom);
                let options;

                options = {
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ["Incidents"]
                    },
                    xAxis: {
                        type: 'category',
                        data: resultat.labels,
                        boundaryGap: false,
                        inverse: true,
                    },
                    yAxis: {
                        type: 'value',

                    },
                    series: [
                        {
                            data: resultat.data,
                            type: 'line',
                            smooth: true,
                            symbolSize: 10,
                            name: "Incidents"
                        }
                    ],
                }

                options && chartLatestIncidents.setOption(options);
            }
        })
        $.ajax({
            url: '{{ route('api.account.engine.charts', auth()->user()->id) }}',
            data: {
                "type": "usure"
            },
            success: resultat => {
                let chartDom = document.querySelector('#usedEngines')
                let chartUsedEngine = echarts.init(chartDom)
                let options;

                options = {
                    tooltip: {
                        trigger: 'items',
                    },
                    legend: {
                        top: '0',
                        left: 'center',
                    },
                    series: [
                        {
                            name: "Taux d'usure",
                            type: 'pie',
                            radius: ['40%', '70%'],
                            avoidLabelOverlap: false,
                            itemStyle: {
                                borderRadius: 10,
                                borderColor: '#fff',
                                borderWidth: 2
                            },
                            label: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    fontSize: '40',
                                    fontWeight: 'bold'
                                }
                            },
                            labelLine: {
                                show: false
                            },
                            data: resultat
                        }
                    ]
                };
                options && chartUsedEngine.setOption(options);
            }
        })
        $.ajax({
            url: '{{ route('api.account.engine.charts', auth()->user()->id) }}',
            data: {
                "type": "ancien"
            },
            success: resultat => {
                let chartDom = document.querySelector('#ancienEngines')
                let chartUsedEngine = echarts.init(chartDom)
                let options;

                options = {
                    tooltip: {
                        trigger: 'items',
                    },
                    legend: {
                        top: '0',
                        left: 'center',
                    },
                    series: [
                        {
                            name: "Ancienneté",
                            type: 'pie',
                            radius: ['40%', '70%'],
                            avoidLabelOverlap: false,
                            itemStyle: {
                                borderRadius: 10,
                                borderColor: '#fff',
                                borderWidth: 2
                            },
                            label: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    fontSize: '40',
                                    fontWeight: 'bold'
                                }
                            },
                            labelLine: {
                                show: false
                            },
                            data: resultat
                        }
                    ]
                };
                options && chartUsedEngine.setOption(options);
            }
        })
        let sliderUsureTer = document.querySelector("#slide_used_ter");
        noUiSlider.create(sliderUsureTer, {
            start: [{{ auth()->user()->setting->auto_ter_max }}],
            step: 1,
            connect: true,
            tooltips: [wNumb({ decimals: 0 })],
            range: {
                "min": 0,
                "max": 100
            },
            format: wNumb({
                decimals: 0,
            }),
            pips: {
                mode: 'range',
                density: 3
            }
        });
        let inputwearter = document.querySelector('[name="wear_ter"]')
        sliderUsureTer.noUiSlider.on('update', (value) => {
            inputwearter.value = value[0];
            document.querySelector('#used_slide_ter').innerHTML = value[0];
        })

        let sliderUsureTgv = document.querySelector("#slide_used_tgv");
        noUiSlider.create(sliderUsureTgv, {
            start: [{{ auth()->user()->setting->auto_tgv_max }}],
            step: 1,
            connect: true,
            tooltips: [wNumb({ decimals: 0 })],
            range: {
                "min": 0,
                "max": 100
            },
            format: wNumb({
                decimals: 0,
            }),
            pips: {
                mode: 'range',
                density: 3
            }
        });
        let inputweartertgv = document.querySelector('[name="wear_tgv"]')
        sliderUsureTgv.noUiSlider.on('update', (value) => {
            inputweartertgv.value = value[0];
            document.querySelector('#used_slide_tgv').innerHTML = value[0];
        })

        let sliderUsureIc = document.querySelector("#slide_used_ic");
        noUiSlider.create(sliderUsureIc, {
            start: [{{ auth()->user()->setting->auto_ter_max }}],
            step: 1,
            connect: true,
            tooltips: [wNumb({ decimals: 0 })],
            range: {
                "min": 0,
                "max": 100
            },
            format: wNumb({
                decimals: 0,
            }),
            pips: {
                mode: 'range',
                density: 3
            }
        });
        let inputwearteric = document.querySelector('[name="wear_ic"]')
        sliderUsureIc.noUiSlider.on('update', (value) => {
            inputwearteric.value = value[0];
            document.querySelector('#used_slide_ic').innerHTML = value[0];
        })
    </script>
@endsection
