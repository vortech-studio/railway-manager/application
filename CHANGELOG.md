# Release Notes

## [0.0.18]

### Core
 - Les pages d&#039;erreurs et de maintenance ont été implémentés

### Interface administration
 - La Gestion des Gares à été implémentés
 - La Gestion des Hubs à été implémentés
 - La gestion des Lignes à été implémentés
 - La gestion des matériels d&#039;infrastructure à été ajoutés
 - La gestion des matériels roulants à été ajoutés
 - La gestion des types d&#039;incidents à été ajoutés
 - Le menu à été organiser mais d&#039;autre onglets peuvent être ajoutées
 - Le template principal de la zone d&#039;administration à été mis en place

### Problèmes connues
 - Le logo Principal n&#039;est pas visible
 - Le système de fil d&#039;attente (REDIS) à été implémenté et est fonctionnel

