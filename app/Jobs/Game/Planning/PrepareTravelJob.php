<?php

namespace App\Jobs\Game\Planning;

use App\Models\User\UserEngine;
use App\Models\User\UserPlanning;
use App\Notifications\Game\SendMessageNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PrepareTravelJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserPlanning $planning)
    {
    }

    public function handle(): void
    {
        $verif_engine = $this->verifEngine($this->planning->engine);

        if ($verif_engine) {
            $this->cancelledTravel($this->planning);
        } else {
            $this->prepareVoyageur($this->planning);

            $this->planning->update([
                "status" => "departure",
            ]);

            $this->planning->logs()->create([
                'message' => "Départ du train en direction de la gare de départ",
                "user_planning_id" => $this->planning->id,
            ]);
            $this->planning->user->notify(new SendMessageNotification(
                'Départ du train en direction de la gare de départ',
                "Le train " . $this->planning->engine->engine->name . " vient de partir en direction de la gare de départ.",
                "info",
                asset('/storage/icons/train_engine.png'),
                route('home')
            ));

            dispatch(new DepartureTravelJob($this->planning))->onQueue('travel')->delay(now()->addMinutes(18));
        }
    }

    private function verifEngine(UserEngine $engine)
    {
        $calc_distance_parcourue = $engine->plannings()->where('status', 'arrival')->sum('kilometer');
        return $calc_distance_parcourue >= $engine->max_runtime_engine;
    }

    private function cancelledTravel(UserPlanning $planning)
    {
        $planning->status = 'cancelled';
        $planning->save();

        $planning->logs()->create([
            'message' => "Matériel indisponible",
            "user_planning_id" => $planning->id,
        ]);
        $planning->user->notify(new SendMessageNotification(
            'Matériel indisponible pour le trajet :' . $planning->ligne->ligne->name,
            "Le métériel " . $planning->engine->engine->name . " n'est pas disponible car il doit effectuer une maintenance préventive.",
            "warning",
            asset('/storage/icons/engin.png'),
            route('home')
        ));
    }

    private function prepareVoyageur(UserPlanning $planning)
    {
        if ($planning->engine->engine->type_train == 'ter') {
            $planning->passengers()->create([
                'type' => 'second',
                'nb_passengers' => rand(0, $planning->engine->engine->nb_passager),
                "user_planning_id" => $planning->id
            ]);
        } elseif ($planning->engine->engine->type_train == 'tgv' || $planning->engine->engine->type_train == 'ic') {
            $planning->passengers()->create([
                'type' => 'first',
                'nb_passengers' => rand(0, $planning->engine->engine->nb_passager),
                "user_planning_id" => $planning->id
            ]);
            $planning->passengers()->create([
                'type' => 'second',
                'nb_passengers' => rand(0, $planning->engine->engine->nb_passager),
                "user_planning_id" => $planning->id
            ]);
        } elseif ($planning->engine->engine->type_train == 'transilien') {
            $planning->passengers()->create([
                'type' => 'second',
                'nb_passengers' => rand(0, $planning->engine->engine->nb_passager),
                "user_planning_id" => $planning->id
            ]);
        }
    }
}
