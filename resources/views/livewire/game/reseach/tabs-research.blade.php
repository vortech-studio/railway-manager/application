<div class="bg-gray-300 rounded p-5">
    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6">
        @foreach($sectors as $sector)
            <li class="nav-item">
                <a href="#{{ Str::snake($sector->name) }}" data-bs-toggle="tab" class="nav-link text-dark">{{ $sector->name }}</a>
            </li>
        @endforeach
    </ul>

    <div class="tab-content" id="myTabContent">
        @foreach($sectors as $sector)
        <div class="tab-pane fade" id="{{ Str::snake($sector->name) }}" role="tabpanel">
            <div class="accordion" id="kt_accordion_{{ $sector->id }}">
                @foreach($sector->categories as $category)
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="kt_accordion_{{ $sector->id }}_header_{{ $category->id }}">
                            <button class="accordion-button fs-4 fw-semibold" type="button" data-bs-toggle="collapse" data-bs-target="#kt_accordion_{{ $sector->id }}_body_{{ $category->id }}" aria-expanded="true" aria-controls="kt_accordion_{{ $sector->id }}_body_{{ $category->id }}">
                                {{ $category->name }}
                            </button>
                        </h2>
                        <div id="kt_accordion_{{ $sector->id }}_body_{{ $category->id }}" class="accordion-collapse collapse show" aria-labelledby="kt_accordion_{{ $sector->id }}_header_{{ $category->id }}" data-bs-parent="#kt_accordion_{{ $sector->id }}">
                            <div class="accordion-body">
                                <div class="d-flex flex-wrap justify-content-center align-items-center gap-5">
                                    @foreach($category->skills as $skill)
                                        <a wire:click="checkout({{ $skill->id }})" class="symbol symbol-150px border border-3 border-primary shadow-lg bg-grey-600" data-bs-trigger="hover" data-bs-custom-class="popover-inverse" data-bs-toggle="popover" data-bs-placement="bottom" data-bs-html="true"
                                           title="<div class='d-flex flex-column'>
                                           <div class='d-flex flex-row align-items-center mb-5'>
    <div class='symbol symbol-40 me-5'>
        <img src='{{ asset('/storage/icons/research/' . $skill->id.'.png') }}' alt=''>
    </div>
    <div class='d-flex flex-column'>
        <span class='text-orange-700 fs-3'>{{ $skill->name }}</span>
        <span class='fw-bolder fs-3 mb-2'>{{ $skill->subname }}</span>
        <span class='text-primary fs-4 ms-3'>{{ $skill->influence }}</span>
    </div>
</div>
<div>
    <span>{{ $skill->description }}</span>
</div>
<div class='separator border border-white my-5'></div>
@if($skill->level_max > 1)
    @for($l = 1; $l <= $skill->level_max; $l++)
        @if($skill->users()->where('user_id', auth()->user()->id)->first()->pivot->level >= $l)
            <div class='d-flex align-items-center text-white fs-8 fs-italic'>
                <span class='bullet bullet-dot bullet-white me-3'></span> {{ $skill->influence }} : @if($skill->action_base_value <= 0) {{ $skill->getActionBaseValueFormat($skill->action_base_type, $skill->action_base_value * ($skill->level_multiplicator * $l)) }} @else + {{ $skill->getActionBaseValueFormat($skill->action_base_type, $skill->action_base_value * ($skill->level_multiplicator * $l)) }}  @endif
            </div>
        @else
            <div class='d-flex align-items-center text-gray-600 fs-8 fs-italic'>
                <span class='bullet bullet-dot bullet-white me-3'></span> {{ $skill->influence }} : @if($skill->action_base_value <= 0) {{ $skill->getActionBaseValueFormat($skill->action_base_type, $skill->action_base_value * ($skill->level_multiplicator * $l)) }} @else + {{ $skill->getActionBaseValueFormat($skill->action_base_type, $skill->action_base_value * ($skill->level_multiplicator * $l)) }}  @endif
            </div>
        @endif
    @endfor
@endif
<div class='separator border border-white my-5'></div>
<div class='d-flex flex-row justify-content-between align-items-center'>
    <div class='d-flex flex-row'>
        <img src='{{ asset('/storage/icons/euro.png') }}' alt='' class='w-15px h-15px me-2' />
        <span class='text-white fs-6'>{{ $skill->getActualCoast() }}</span>
    </div>
    <div class='d-flex flex-row'>
        <i class='fa-solid fa-clock fs-3 me-2'></i>
        <span class='text-white fs-6'>{{ $skill->getActualTime() }}</span>
    </div>
</div>

</div>">
                                            <img alt="Logo" src="{{ asset('/storage/icons/research/' . $skill->id.'.png') }}" />
                                            <span class="symbol-badge w-100 top-100 start-50 translate-middle">
                                                @for($l = 1; $l <= $skill->level_max; $l++)
                                                    @if($skill->users()->first()->pivot->level >= $l)
                                                        <span class="badge badge-circle bg-grey-600 border border-success shadow"><i class="fa-solid fa-check text-success"></i> </span>
                                                    @else
                                                        <span class="badge badge-circle bg-grey-600 border border-gray-900 shadow">&nbsp;</span>
                                                    @endif
                                                @endfor
                                            </span>

                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @endforeach
    </div>

</div>
