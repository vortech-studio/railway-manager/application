<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlanningsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_plannings', function (Blueprint $table) {
            $table->id();
            $table->integer('number_travel')->nullable();
            $table->timestamp('date_depart');

            $table->enum('status', [
                'travel',
                'in_station',
                'departure',
                'arrival',
                'initialized',
                'retarded',
                'canceled'
            ])->default('initialized');
            $table->integer('retarded_time')->nullable()->comment("Si retarded time en minute");

            $table->foreignId('user_hub_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_ligne_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_engine_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_plannings');
    }
}
