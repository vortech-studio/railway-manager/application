<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class Mailbox extends Model
{
    protected $guarded = [];
    protected $casts = [
        'read_at' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function actions()
    {
        return $this->hasMany(MailboxAction::class);
    }

    public static function generateFrom(string $type_action, Model $model)
    {
        return match ($type_action) {
            "vente_ligne" => self::generateVenteLigne($model),
            "vente_hub" => self::generateVenteHub($model),
            "vente_engine" => self::generateVenteEngine($model),
        };
    }

    private static function generateVenteLigne(Model $model)
    {
        return match ($model->ligne->type_ligne) {
            "ter" => "Conseil Régionnal ".$model->ligne->hub->gare->region,
            "tgv", "ic" => "Service Public - ".$model->ligne->hub->gare->pays,
            "transilien" => "Ville de Paris",
        };
    }

    private static function generateVenteHub(Model $model)
    {
        return "Service Public - ".$model->hub->gare->pays;
    }

    private static function generateVenteEngine(Model $model)
    {
        return "Service Public - ".$model->user_hub->hub->gare->pays;
    }
}
