<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserEnginesTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_engines', function (Blueprint $table) {
            $table->id();
            $table->integer('max_runtime_engine')->comment("Nombre de fois utiliser avant maintenance");
            $table->boolean('active')->default(false);
            $table->boolean('in_maintenance')->default(false);
            $table->boolean('available')->default(false);
            $table->timestamp('date_achat')->useCurrent();

            $table->foreignId('user_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('engine_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_hub_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_engines');
    }
}
