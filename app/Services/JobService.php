<?php

namespace App\Services;

use Carbon\Carbon;
use DB;
use Illuminate\Queue\Jobs\Job as LaravelJob;

abstract class JobService extends LaravelJob
{
    public function getJobId()
    {
        return $this->payload()['id'];
    }
}
