<?php

namespace App\Http\Controllers\Game\Compagnie;

use App\Http\Controllers\Controller;

class ProfilController extends Controller
{
    public function index()
    {
        return view('game.compagnie.profil.index');
    }
}
