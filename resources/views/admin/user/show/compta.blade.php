<div class="d-flex flex-column mb-10">
    <div class="d-flex flex-row justify-content-between align-items-center border border-bottom-1">
        <div class="d-flex align-items-center">
            <div class="symbol symbol-30px me-5">
                <i class="fa-solid fa-euro-sign fs-3x text-primary"></i>
            </div>
            <span class="fw-bold fs-1">Argent en banque</span>
        </div>
        <div class="d-flex flex-end fs-2x fw-bolder">
            @if($user->argent < 0)
                <span class="text-danger text-end">{{ eur($user->argent) }}</span>
            @else
                <span class="text-success text-end">{{ eur($user->argent) }}</span>
            @endif
        </div>
    </div>
    <div class="separator border-1 border-gray-400 my-5"></div>
    <div class="d-flex flex-row justify-content-between align-items-center border border-bottom-1">
        <div class="d-flex align-items-center">
            <div class="symbol symbol-30px me-5">
                <i class="fa-solid fa-wallet fs-3x text-primary"></i>
            </div>
            <span class="fw-bold fs-1">Trésorerie Structurel de la semaine</span>
        </div>
        <div class="d-flex flex-end fs-2x fw-bolder">
            @if(\App\Models\Core\Mouvement::getTresoStructurel(now()) < 0)
                <span class="text-danger text-end">{{ eur(\App\Models\Core\Mouvement::getTresoStructurel(now())) }}</span>
            @else
                <span class="text-success text-end">{{ eur(\App\Models\Core\Mouvement::getTresoStructurel(now())) }}</span>
            @endif
        </div>
    </div>
    <div class="separator border-1 border-gray-400 my-5"></div>
    <div class="d-flex flex-row justify-content-between align-items-center border border-bottom-1">
        <div class="d-flex align-items-center">
            <div class="symbol symbol-30px me-5">
                <i class="fa-solid fa-chart-line fs-3x text-primary"></i>
            </div>
            <span class="fw-bold fs-1">CA de la semaine</span>
        </div>
        <div class="d-flex flex-end fs-2x fw-bolder">
            @if(eur(\App\Models\Core\Mouvement::getCAFromWeek(now()->subWeek())) < 0)
                <span class="text-danger text-end">{{ eur(\App\Models\Core\Mouvement::getCAFromWeek(now()->subWeek())) }}</span>
            @else
                <span class="text-success text-end">{{ eur(\App\Models\Core\Mouvement::getCAFromWeek(now()->subWeek())) }}</span>
            @endif
        </div>
    </div>
    <div class="separator border-1 border-gray-400 my-5"></div>
    <div class="d-flex flex-row justify-content-between align-items-center border border-bottom-1">
        <div class="d-flex align-items-center">
            <div class="symbol symbol-30px me-5">
                <i class="fa-solid fa-arrow-up fs-3x text-primary"></i>
            </div>
            <span class="fw-bold fs-1">Bénéfice des trajets</span>
        </div>
        <div class="d-flex flex-end fs-2x fw-bolder">
            @if(eur(\App\Models\Core\Mouvement::getBenefice(now())) < 0)
                <span class="text-danger text-end">{{ eur(\App\Models\Core\Mouvement::getBenefice(now())) }}</span>
            @else
                <span class="text-success text-end">{{ eur(\App\Models\Core\Mouvement::getBenefice(now())) }}</span>
            @endif
        </div>
    </div>
</div>
<x-base.underline
    title="Historique des mouvements" />
<table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_mouvements">
    <thead>
        <tr>
            <th class="w-30px">Type</th>
            <th>Libellé</th>
            <th class="w-100px">Date</th>
            <th class="w-25">Montant</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>

