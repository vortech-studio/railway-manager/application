<?php

namespace App\Jobs\Admin;

use Aws\Api\ErrorParser\JsonParserTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TransferImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, JsonParserTrait;

    public function __construct(public string $folder, public $pathname, public string $nameFile)
    {
    }

    public function handle(): void
    {
        $encoding = mb_detect_encoding($this->pathname,'UTF-8, ISO-8859-1, WINDOWS-1252, WINDOWS-1251', true);
        $this->pathname = mb_convert_encoding($this->pathname, 'UTF-8', $encoding);
        $image = \Storage::get($this->pathname);
        $uploadImage = \Storage::disk($this->folder)->put($this->nameFile, $image);
    }
}
