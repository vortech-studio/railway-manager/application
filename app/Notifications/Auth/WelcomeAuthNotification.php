<?php

namespace App\Notifications\Auth;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WelcomeAuthNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public function __construct(protected User $user, protected string $password)
    {
    }

    public function via($notifiable): array
    {
        return ['mail'];
    }

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->from("no-reply@railway-manager.ovh", "Railway Manager")
            ->view('emails.auth.welcome_oauth', [
                'user' => $this->user,
                'password' => $this->password
            ]);
    }

    public function toArray($notifiable): array
    {
        return [];
    }
}
