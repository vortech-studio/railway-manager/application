<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Core\Achievement;
use App\Models\Core\Engine;
use App\Models\Core\Hub;
use App\Models\Core\Ligne;
use App\Models\User;
use App\Models\User\UserHub;
use App\Notifications\Auth\WelcomeAuthNotification;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use LevelUp\Experience\Events\AchievementAwarded;

class OAuthController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $userSocial = Socialite::driver($provider)
            ->user();

        $password = generatePassword(8);
        $user = User::updateOrCreate([
            'email' => $userSocial->getEmail(),
        ], [
            'name' => $userSocial->getName(),
            'email' => $userSocial->getEmail(),
            'email_verified_at' => now(),
            "password" => \Hash::make($password)
        ]);


        $user->social()->updateOrCreate([
            $provider.'_id' => $userSocial->getId(),
        ],[
            $provider.'_id' => $userSocial->getId(),
            'user_id' => $user->id,
        ]);

        $user->notify(new WelcomeAuthNotification($user, $password));

        \Auth::login($user, true);

        return redirect()->route('oauth.config');
    }

    public function config(Request $request)
    {
        if(auth()->user()->installed) {
            return redirect()->route('home');
        }

        if($request->get('step') == "2") {
            seo()->title(config('app.name').' - Achat de votre premier hub');
            seo()->csrfToken();

            return view('auth.firstCheckout', [
                "user" => auth()->user(),
                "hubs" => Hub::with('gare')->where('active', true)->get(),
            ]);
        } elseif ($request->get('step') == "3") {
            seo()->title(config('app.name').' - Connexion');
            seo()->csrfToken();

            seo()->title(config('app.name').' - Achat de votre première ligne');
            seo()->csrfToken();
            return view('auth.ligneCheckout', [
                "user" => auth()->user(),
                "lignes" => auth()->user()->hubs()->orderBy('date_achat', 'desc')->first()->hub->lignes()->where('active', true)->get(),
                "hub" => auth()->user()->hubs()->orderBy('date_achat', 'desc')->first()->hub
            ]);
        } elseif($request->get('step') == 4) {
            seo()->title(config('app.name').' - Achat de votre premier train');
            seo()->csrfToken();

            //dd(Engine::where('type_engine', 'automotrice')->get()->load('technical'));
            return view('auth.engineCheckout', [
                "user" => auth()->user(),
                "engines" => Engine::where('type_engine', 'automotrice')->get()->load('technical'),
                "ligne" => auth()->user()->hubs()->orderBy('date_achat', 'desc')->first()->lignes()->orderBy('date_achat', 'desc')->first()->load('ligne'),
                "hub" => auth()->user()->hubs()->orderBy('date_achat', 'desc')->first()->hub
            ]);
        } elseif ($request->get('step') == 5) {
            //dd(auth()->user()->engines()->first()->load('engine'));
            seo()->title(config('app.name').' - Vérification de votre compte');
            seo()->csrfToken();

            return view('auth.finish', [
                "user" => auth()->user(),
                "hub" => auth()->user()->hubs()->orderBy('date_achat', 'desc')->first(),
                "ligne" => auth()->user()->hubs()->orderBy('date_achat', 'desc')->first()->lignes()->orderBy('date_achat', 'desc')->first()->load('ligne'),
                "engine" => auth()->user()->engines()->first()->load('engine')
            ]);
        } else {
            seo()->title(config('app.name').' - Bienvenue sur '.config('app.name').' !');
            seo()->csrfToken();

            return view('auth.config', [
                "user" => auth()->user(),
                "faker" => \Faker\Factory::create("fr_FR"),
            ]);
        }
    }

    public function configStore(Request $request)
    {
        $user = $request->user();
        if(!$request->has('step')) {
            $user->update([
                "name_company" => $request->get('name_company'),
                "name_secretary" => $request->get('name_secretary')
            ]);

            if ($request->hasFile('avatar')) {
                $avatar = $request->file('avatar');
                $avatarName = $user->id . '.' . $avatar->getClientOriginalExtension();
                $avatar->storeAs('users', $avatarName, 'avatar');
                $user->update(['avatar' => $avatarName]);
            }

            if ($request->hasFile('logo_company')) {
                $logo = $request->file('logo_company');
                $logoName = $user->id . '.' . $logo->getClientOriginalExtension();
                $logo->storeAs('company', $logoName, 'avatar');
                $user->update(['logo_company' => $logoName]);
            }

            if ($request->hasFile('avatar_secretary')) {
                $avatar = $request->file('avatar_secretary');
                $avatarName = $user->id . '.' . $avatar->getClientOriginalExtension();
                $avatar->storeAs('secretary', $avatarName, 'avatar');
                $user->update(['avatar_secretary' => $avatarName]);
            }

            return redirect()->intended(route('oauth.config').'?step=2');
        } else {
            if($request->get('step') == "2") {
                $hub = $user->hubs()->create([
                    "date_achat" => now(),
                    "km_ligne" => 0,
                    "user_id" => $user->id,
                    "hub_id" => $request->get('hub_id'),
                    "nb_salarie_iv" => 0,
                    "nb_salarie_com" => 0,
                    "nb_salarie_ir" => 0,
                    "nb_salarie_technicentre" => 0,
                    'active' => true,
                ]);

                $hub->update([
                    "nb_salarie_iv" => UserHub::calcNbSalarieIV($hub->hub),
                    "nb_salarie_ir" => UserHub::calcNbSalarieIR($hub->hub),
                ]);

                return redirect()->intended(route('oauth.config').'?step=3');
            } elseif ($request->get('step') == "3") {
                $hub = $user->hubs()->orderBy('date_achat', 'desc')->first();

                $ligne = $hub->lignes()->create([
                    "date_achat" => now(),
                    "nb_depart_jour" => 0,
                    "quai" => rand(1,$hub->hub->gare->nb_quai),
                    "user_hub_id" => $hub->id,
                    "ligne_id" => $request->get('ligne_id'),
                    "user_id" => $user->id,
                    "active" => true,
                ]);

                $l = Ligne::find($request->get('ligne_id'));
                $hub->update(["km_ligne" => $hub->km_ligne + $l->distance]);

                $ligne->update([
                    "nb_depart_jour" => User\UserLigne::calcNbDepartJour($ligne, $hub->hub),
                ]);

                return redirect()->intended(route('oauth.config').'?step=4');
            } elseif($request->get('step') == "4") {
                try {
                    Engine::checkoutEngine($request);
                    return redirect()->intended(route('oauth.config').'?step=5');
                }catch (\Exception $exception) {
                    \Log::critical($exception->getMessage());
                    return redirect()->back()->with('error', $exception->getMessage());
                }
            } else {
                // Calcul des tarifs
                $ligne = $user->hubs()->orderBy('date_achat', 'desc')->first()->lignes()->orderBy('date_achat', 'desc')->first();


                $tarif = User\UserLigneTarif::create([
                    "date_tarif" => now(),
                    "demande" => $ligne->ligne->hub->passenger_first + $ligne->ligne->hub->passenger_second,
                    "offre" => intval($ligne->ligne->hub->passenger_first + $ligne->ligne->hub->passenger_second * rand(60,100) / 100),
                    "price" => User\UserLigneTarif::calcTarifSecond($ligne),
                    "first_class" => $ligne->ligne->hub->passenger_first,
                    "second_class" => $ligne->ligne->hub->passenger_second,
                    "user_ligne_id" => $ligne->id
                ]);

                $user->card_bonus()->create([
                    'sim_offer' => 0,
                    'audit_ext_offer' => 0,
                    "audit_int_offer" => 0,
                    "user_id" => $user->id,
                ]);

                $ach = Achievement::find(8);
                $ach2 = Achievement::find(13);

                auth()->user()->grantAchievement($ach);
                auth()->user()->grantAchievement($ach2);
                event(new AchievementAwarded($ach, auth()->user()));
                event(new AchievementAwarded($ach2, auth()->user()));
                auth()->user()->addPoints(200);

                $user->installed = true;
                $user->save();

                return redirect()->intended(route('home'));

            }
        }
    }
}
