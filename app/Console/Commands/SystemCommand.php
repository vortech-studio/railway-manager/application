<?php

namespace App\Console\Commands;

use App\Models\Core\Engine;
use App\Models\Core\GareWeather;
use App\Models\Core\Hub;
use App\Models\Core\LigneStation;
use App\Models\Core\Mouvement;
use App\Models\Core\Setting;
use App\Models\User;
use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;
use function Laravel\Prompts\info;
use function Laravel\Prompts\alert;
use function Laravel\Prompts\text;
use function Laravel\Prompts\confirm;

class SystemCommand extends Command
{
    protected $signature = 'system {type}';

    protected $description = 'Commande Système';

    public function handle(): void
    {
        match ($this->argument('type')) {
            default => $this->error('Commande inconnue'),
            "sitemap" => $this->generateSitemap(),
            "user" => $this->createUser(),
            "planning_today" => $this->planningToday(),
            "weather" => $this->updateWeather(),
            "tarif" => $this->updateTarif(),
            "purge_notification" => $this->purgeNotification(),
            "update-user" => $this->updateUser(),
            "update_search" => $this->updateSearch()
        };
    }

    private function generateSitemap(): void
    {
        SitemapGenerator::create(config('app.url'))->writeToFile(public_path('sitemap.xml'));
    }

    public function createUser()
    {
        $faker = \Faker\Factory::create('fr_FR');
        alert("-- Création d'un utilisateur --");

        $name = text(
            label: "Nom d'utilisateur",
            default: "Administrateur",
            required: true
        );

        $email = text(
            label: "Email",
            default: $faker->email,
            required: true
        );

        $admin = confirm(
            label: "S'agit-il d'un administrateur ?",
            yes: 'Oui',
            no: 'Non',
        );

        $premium = confirm(
            label: "S'agit-il d'un utilisateur premium ?",
            yes: 'Oui',
            no: 'Non',
        );

        $argent = text(
            label: "Argent",
            default: 0,
            required: true
        );

        $tpoint = text(
            label: "TPoint",
            default: 0,
            required: true
        );

        $research = text(
            label: "Recherche",
            default: 0,
            required: true
        );

        $password = generatePassword();

        alert("Création de l'utilisateur");
        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => \Hash::make($password),
            'admin' => $admin,
            'premium' => $premium,
            'argent' => 0,
            'tpoint' => 0,
            'research' => 0,
            "name_company" => $faker->company,
            "desc_company" => $faker->text,
            "name_secretary" => $faker->name,
            "name_conseiller" => $faker->name,
            "installed" => true
        ]);
        $company = User\UserCompany::automatedCreate($user);
        User\UserCompanySituation::automatedCreate($company);

        Mouvement::adding("revenue", "Création de l'entreprise - Reversion du capital", "subvention", $argent, $company->id);
        $user->update([
            "tpoint" => $tpoint,
            "research" => $research,
        ]);

        $hub = Hub::where('active', true)->get()->random();

        $h = $user->hubs()->create([
            "nb_salarie_iv" => User\UserHub::calcNbSalarieIV($hub),
            "nb_salarie_com" => 0,
            "nb_salarie_ir" => User\UserHub::calcNbSalarieIR($hub),
            "nb_salarie_technicentre" => 0,
            "date_achat" => now(),
            "km_ligne" => 0,
            "user_id" => $user->id,
            "hub_id" => $hub->id,
            'active' => true
        ]);

        $ligne = $hub->lignes()->where('active', true)->get();

        $l = $user->lignes()->create([
            "date_achat" => now(),
            "nb_depart_jour" => 0,
            "quai" => rand(1, $hub->gare->nb_quai),
            "user_hub_id" => $h->id,
            "ligne_id" => $ligne->random()->id,
            "user_composition_track_id" => null,
            "user_id" => $user->id,
            "active" => true
        ]);

        $l->nb_depart_jour = User\UserLigne::calcNbDepartJour($l, $hub);
        $l->save();


        $engine = Engine::where('type_engine', 'automotrice')
            ->where('type_train', $l->ligne->type_ligne)
            ->get()->random();

        $e = $user->engines()->create([
            "max_runtime_engine" => 0,
            "user_id" => $user->id,
            "engine_id" => $engine->id,
            "user_hub_id" => $h->id,
            "active" => true,
            "number" => User\UserEngine::defineNumberEngine()
        ]);
        $e->max_runtime_engine = $engine->calcMaxRuntimeEngine();
        $e->save();


        $l->user_engine_id = $e->id;
        $l->save();

        User\UserLigneTarif::createTarif($l, $e->engine, $hub);

        User\Mailbox::create([
            "from" => $user->name_conseiller,
            "subject" => "Bienvenue sur Railway Manager",
            "message" => "Bonjour, je suis votre conseiller, je suis là pour vous aider à démarrer votre entreprise. N'hésitez pas à me contacter si vous avez des questions.",
            "important" => true,
            "user_id" => $user->id,
        ]);
        $user->addPoints(0);
        $user->card_bonus()->create([
            'sim_offer' => 0,
            'audit_ext_offer' => 0,
            "audit_int_offer" => 0,
            "user_id" => $user->id,
        ]);

        if ($admin) {
            $user->email_verified_at = now();
            $user->save();
            info("Mot de passe: ".$password);
        }

        $this->call('system', ['type' => 'planning_today']);

        info("L'utilisateur à été créer avec succès");
    }



    private function planningToday()
    {
        $users = User::all();
        $dayWork = now()->dayOfWeek;

        foreach ($users as $user) {
            if($user->automated_planning) {
                foreach ($user->lignes()->where('active', true)->get() as $ligne) {
                    for ($i = 1; $i <= $ligne->nb_depart_jour; $i++) {
                        $min_day = now()->startOfDay()->diffInMinutes(now()->endOfDay());
                        $diff = $min_day / $ligne->nb_depart_jour;

                        // Génération du planning
                        $planning = $ligne->plannings()->create([
                            "number_travel" => null,
                            "date_depart" => now()->startOfDay()->addMinutes($diff * $i),
                            "user_ligne_id" => $ligne->id,
                            "user_engine_id" => $ligne->user_engine->id,
                            "user_id" => $user->id,
                            "kilometer" => $ligne->ligne->distance,
                            "user_hub_id" => $ligne->user_hub_id,
                        ]);
                        $planning->number_travel = $planning->generateNumberTravel();
                        $planning->save();

                        // Génération du voyage
                        $planning->travel()->create([
                            "ca_billetterie" => 0,
                            "ca_other" => 0,
                            "frais_electrique" => 0,
                            "frais_gasoil" => 0,
                            "frais_autre" => 0,
                            "user_planning_id" => $planning->id,
                        ]);

                        // Génération des stations
                        foreach ($planning->ligne->ligne->stations as $station) {
                            if($station->gare->id != $planning->ligne->ligne->stationStart->id || $station->gare->id != $planning->ligne->ligne->stationEnd->id) {
                                $planning->planning_stations()->create([
                                    "type_infra" => "station",
                                    "departure_at" => $planning->date_depart->addMinutes($station->time),
                                    "arrival_at" => $planning->date_depart->addMinutes($station->time),
                                    "user_planning_id" => $planning->id,
                                    "ligne_station_id" => $station->id,
                                    "name" => $station->gare->name
                                ]);
                            } else {
                                $planning->planning_stations()->create([
                                    "type_infra" => "station",
                                    "departure_at" => $planning->date_depart->addMinutes($station->time + LigneStation::time_stop_station_config($station)),
                                    "arrival_at" => $planning->date_depart->addMinutes($station->time),
                                    "user_planning_id" => $planning->id,
                                    "ligne_station_id" => $station->id,
                                    "name" => $station->gare->name
                                ]);
                            }

                        }


                    }
                }
            } else {
                foreach ($user->planning_constructors as $planning_constructor) {
                    if(in_array($dayWork, json_decode($planning_constructor->dayOfWeek))) {
                        $planning = $planning_constructor->user_engine->plannings()->create([
                            "number_travel" => null,
                            "date_depart" => now()->setTime($planning_constructor->start_at->hour, $planning_constructor->start_at->minute),
                            "date_arrived" => now()->setTime($planning_constructor->end_at->hour, $planning_constructor->end_at->minute),
                            "user_hub_id" => $planning_constructor->user_engine->user_hub_id,
                            "user_ligne_id" => $planning_constructor->user_engine->user_ligne->id,
                            "user_engine_id" => $planning_constructor->user_engine->id,
                            "user_id" => $user->id,
                            "kilometer" => $planning_constructor->user_engine->user_ligne->ligne->distance
                        ]);
                        $planning->number_travel = $planning->generateNumberTravel();
                        $planning->save();

                        // Génération du voyage
                        $planning->travel()->create([
                            "ca_billetterie" => 0,
                            "ca_other" => 0,
                            "frais_electrique" => 0,
                            "frais_gasoil" => 0,
                            "frais_autre" => 0,
                            "user_planning_id" => $planning->id,
                        ]);

                        foreach ($planning->ligne->ligne->stations as $station) {
                            if($station->gare->id != $planning->ligne->ligne->stationStart->id || $station->gare->id != $planning->ligne->ligne->stationEnd->id) {
                                $planning->planning_stations()->create([
                                    "type_infra" => "station",
                                    "departure_at" => $planning->date_depart->addMinutes($station->time),
                                    "arrival_at" => $planning->date_depart->addMinutes($station->time),
                                    "user_planning_id" => $planning->id,
                                    "ligne_station_id" => $station->id,
                                    "name" => $station->gare->name
                                ]);
                            } else {
                                $planning->planning_stations()->create([
                                    "type_infra" => "station",
                                    "departure_at" => $planning->date_depart->addMinutes($station->time + LigneStation::time_stop_station_config($station)),
                                    "arrival_at" => $planning->date_depart->addMinutes($station->time),
                                    "user_planning_id" => $planning->id,
                                    "ligne_station_id" => $station->id,
                                    "name" => $station->gare->name
                                ]);
                            }

                        }
                    }
                }
            }
        }
    }

    private function updateWeather()
    {
        $gares = \App\Models\Core\Gares::all();
        $bar = $this->output->createProgressBar(count($gares));

        $bar->start();
        foreach ($gares as $gare) {
            $weather = new \App\Services\Weather();
            $w = $weather->getWeather($gare->name);

            if(isset($w->current)) {
                if(GareWeather::where('gares_id', $gare->id)->exists())
                    $gare->weather()->where('gares_id', $gare->id)->first()->update([
                        "designation" => $w->current->condition->text,
                        "temperature" => $w->current->temp_c,
                        "icon_url" => $w->current->condition->icon,
                    ]);
                else
                    $gare->weather()->create([
                        "designation" => $w->current->condition->text,
                        "temperature" => $w->current->temp_c,
                        "icon_url" => $w->current->condition->icon,
                        "gares_id" => $gare->id,
                    ]);

                $bar->advance();
            } else {
                if(GareWeather::where('gares_id', $gare->id)->exists())
                    $gare->weather()->where('gares_id', $gare->id)->first()->update([
                        "designation" => "Inconnue",
                        "temperature" => rand(-25,40),
                        "icon_url" => "icon",
                    ]);
                else
                    $gare->weather()->create([
                        "designation" => "Inconnue",
                        "temperature" => rand(-25,40),
                        "icon_url" => "icon",
                        "gares_id" => $gare->id,
                    ]);

                $bar->advance();
            }
        }
        $bar->finish();
        \Log::notice("Mise à jour de la météo terminé");
    }

    private function updateTarif()
    {
        $faker = \Faker\Factory::create('fr_FR');
        Setting::find(8)->update(["value" => $faker->randomFloat(2, 0.85, 2.90)]);
        $lignes = User\UserLigne::all();

        foreach ($lignes as $ligne) {
            if($ligne->tarifs()->where('date_tarif', now()->startOfDay())->count() == 0) {
                User\UserLigneTarif::createTarif($ligne, $ligne->user_engine->engine, $ligne->ligne->hub);
            }
        }
    }

    private function purgeNotification()
    {
        $users = User::where('admin', 0)->get();

        foreach ($users as $user) {
            $user->unreadNotifications->markAsRead();
        }

        return null;
    }

    private function updateUser()
    {
        $email = $this->ask("Adresse Mail du compte:");

        $user = User::where('email', $email)->first();

        $password = $this->ask('Nouveau mot de passe:');

        $user->update([
            "password" => \Hash::make($password),
        ]);

        $this->info("Mot de passe mis à jour");
    }

    private function updateSearch()
    {
        foreach (User::all() as $user) {
            Mouvement::adding(
                'charge',
                "Transfert R&D",
                "divers",
                $user->company->research_coast_base,
                $user->company->id,
            );

            $user->research += $user->company->research_coast_base;
            $user->save();
        }
    }
}
