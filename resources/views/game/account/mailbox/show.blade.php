@extends("game.template")

@section("css")

@endsection

@section("info_bar")

@endsection

@section("content")
    <div class="flex-lg-row-fluid ms-lg-7 ms-xl-10">

        <!--begin::Card-->
        <div class="card">
            <div class="card-header align-items-center py-5 gap-5">
                <!--begin::Actions-->
                <div class="d-flex">
                    <!--begin::Back-->
                    <a href="{{ route('mailbox.index') }}" class="btn btn-sm btn-icon btn-clear btn-active-light-primary me-3" data-bs-toggle="tooltip" data-bs-placement="top" aria-label="Back" data-bs-original-title="Retour" data-kt-initialized="1">
                        <i class="ki-duotone ki-arrow-left fs-1 m-0"><span class="path1"></span><span class="path2"></span></i>    </a>
                    <!--end::Back-->

                    <!--begin::Delete-->
                    <a href="#" class="btn btn-sm btn-icon btn-light btn-active-light-primary me-2" data-bs-toggle="tooltip" data-bs-placement="top" aria-label="Delete" data-bs-original-title="Delete" data-kt-initialized="1">
                        <i class="ki-duotone ki-trash fs-2 m-0"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i>    </a>
                    <!--end::Delete-->
                </div>
                <!--end::Actions-->

                <!--begin::Pagination-->
                <div class="d-flex align-items-center">
                    <!--begin::Pages info-->
                    <span class="fw-semibold text-muted me-2">{{ $message->id }} sur {{ auth()->user()->messages()->count() }}</span>
                    <!--end::Pages info-->
                    <!--end::Toggle-->
                </div>
                <!--end::Pagination-->
            </div>

            <div class="card-body">
                <!--begin::Title-->
                <div class="d-flex flex-wrap gap-2 justify-content-between mb-8">
                    <div class="d-flex align-items-center flex-wrap gap-2">
                        <!--begin::Heading-->
                        <h2 class="fw-semibold me-3 my-1">{{ $message->subject }}</h2>
                        <!--begin::Heading-->

                        <!--begin::Badges-->
                        <!--<span class="badge badge-light-primary my-1 me-2">inbox</span>
                        <span class="badge badge-light-danger my-1">important</span>-->
                        <!--end::Badges-->
                    </div>
                </div>
                <!--end::Title-->

                <!--begin::Message accordion-->
                <div data-kt-inbox-message="message_wrapper">
                    <!--begin::Message header-->
                    <div class="d-flex flex-wrap gap-2 flex-stack cursor-pointer" data-kt-inbox-message="header">
                        <!--begin::Author-->
                        <div class="d-flex align-items-center">
                            <!--begin::Avatar-->
                            <div class="symbol symbol-50 me-4">
                                <div class="symbol-label bg-light-{{ random_color() }}">
                                    <span class="text-{{ random_color() }} fs-2">{{ Str::limit($message->from, 1, '') }}</span>
                                </div>
                            </div>
                            <!--end::Avatar-->

                            <div class="pe-5">
                                <!--begin::Author details-->
                                <div class="d-flex align-items-center flex-wrap gap-1">
                                    <a href="#" class="fw-bold text-dark text-hover-primary">{{ $message->from }}</a>
                                    <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                                        <span class="path1"></span>
                                        <span class="path2"></span>
                                    </i>
                                    <span class="text-muted fw-bold">
                                        @if($message->created_at->isToday())
                                            {{ $message->created_at->format('H:i') }}
                                        @else
                                            {{ $message->created_at->format('d/m/Y') }}
                                        @endif
                                    </span>
                                </div>
                                <!--end::Author details-->

                                <!--begin::Message details-->
                                <div data-kt-inbox-message="details">
                                    <span class="text-muted fw-semibold">à moi</span>

                                    <!--begin::Menu toggle-->
                                    <a href="#" class="me-1" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start">
                                        <i class="ki-duotone ki-down fs-5 m-0"></i>
                                    </a>
                                    <!--end::Menu toggle-->

                                    <!--begin::Menu-->
                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-300px p-4" data-kt-menu="true">
                                        <!--begin::Table-->
                                        <table class="table mb-0">
                                            <tbody>
                                            <tr>
                                                <td class="w-75px text-muted">De</td>
                                                <td>{{ $message->from }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-muted">Date</td>
                                                <td>{{ $message->created_at->format("d/m/Y à H:i") }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-muted">Sujet</td>
                                                <td>{{ $message->subject }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--end::Menu-->
                                </div>
                                <!--end::Message details-->
                            </div>
                        </div>
                        <!--end::Author-->

                        <!--begin::Actions-->
                        <div class="d-flex align-items-center flex-wrap gap-2">
                            <!--begin::Date-->
                            <span class="fw-semibold text-muted text-end me-3">{{ $message->created_at->format("d M Y, H:i") }}</span>
                            <!--end::Date-->

                            <div class="d-flex">
                                <!--begin::Star-->
                                <a href="#" class="btn btn-sm btn-icon btn-clear btn-active-light-primary me-3" data-bs-toggle="tooltip" data-bs-placement="top" aria-label="Star" data-bs-original-title="Star" data-kt-initialized="1">
                                    <i class="ki-duotone ki-star {{ $message->important ? 'text-warning' : '' }} fs-2  m-0"></i>
                                </a>
                                <!--end::Star-->
                            </div>
                        </div>
                        <!--end::Actions-->    </div>
                    <!--end::Message header-->

                    <!--begin::Message content-->
                    <div class="collapse fade show" data-kt-inbox-message="message">
                        <div class="py-5">
                            {!! $message->message !!}
                        </div>
                    </div>
                    <!--end::Message content-->
                </div>
                <!--end::Message accordion-->

                <div class="separator my-6"></div>

                <div class="d-flex flex-center gap-2 mb-8">
                    @foreach($message->actions as $action)
                        <a href="{{ $action->action_link }}" class="btn btn-sm btn-primary btn-active-light-primary">{{ $action->action_text }}</a>
                    @endforeach
                </div>
            </div>
        </div>
        <!--end::Card-->

    </div>
@endsection

@section("script")
    <script type="text/javascript">

    </script>
@endsection
