<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\LigneStation;

class LigneStationController extends Controller
{
    public function destroy($ligne_id, $station_id)
    {
        $station = LigneStation::findOrFail($station_id);
        $ligne = $station->ligne;

        $ligne->time_min = $ligne->time_min - $station->time;
        $ligne->save();

        $station->delete();

        return response()->json([
            'message' => "La station a bien été supprimée de la ligne {$station->ligne->name}"
        ]);
    }
}
