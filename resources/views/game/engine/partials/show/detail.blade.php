@if($engine->engine->type_engine == 'automotrice')
    <div class="card card-flush shadow-lg">
        <div class="card-header">
            <div class="card-title align-items-center">
                <img src="{{ asset('/storage/logos/logo_'.$engine->engine->type_train.'.svg') }}" class="w-35px me-3" alt="">
                <span class="me-3">{{ $engine->engine->name }} - {{ $engine->number }} | </span>
                <span>
                    <span class="badge bg-amber-500 text-white me-3">Hub</span>
                    {{ $engine->user_hub->hub->gare->name }} / {{ $engine->user_hub->hub->gare->region }} / {{ $engine->user_hub->hub->gare->pays }}
                </span>
            </div>
        </div>
        <div class="card-body">
            <div class="d-flex flex-row w-100 justify-content-center align-items-baseline bg-white shadow-lg mb-10 p-10">
                <div class="p-auto">
                    @for($i=0; $i < $engine->engine->technical->nb_wagon; $i++)
                        <img src="/storage/engines/{{ $engine->engine->type_engine }}/{{ \Illuminate\Support\Str::upper(Str::slug($engine->engine->name)) }}-{{ $i }}.gif" class="my-5" alt="">
                    @endfor
                </div>
            </div>
            <div class="d-flex flex-row justify-content-around align-items-center mb-10">
                <div class="rounded bg-blue-600 text-white p-5 me-5 w-200px">
                    <div class="d-flex flex-column justify-content-center align-items-end">
                        <span class="fw-bold fs-2x">{{ $engine->max_runtime_engine }} Km</span>
                        <span>Rayon d'action de l'engin roulant</span>
                    </div>
                </div>
                <div class="rounded bg-amber-600 text-white p-5 me-5 w-200px">
                    <div class="d-flex flex-column justify-content-center align-items-end">
                        <span class="fw-bold fs-2x">{{ $engine->engine->velocity }} Km/h</span>
                        <span>Vitesse Max</span>
                    </div>
                </div>
                <div class="rounded bg-indigo-600 text-white p-5 me-5 w-200px">
                    <div class="d-flex flex-column justify-content-center align-items-end">
                        <span class="fw-bold fs-2x">{{ $engine->engine->nb_passager }}</span>
                        <span>Nombre de siège disponible</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-6 mb-5">
                    <div class="card shadow-lg">
                        <div class="card-header">
                            <div class="card-title">
                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px h-30px me-3" alt="">
                                <span>Caractéristique de l'engins</span>
                            </div>
                            @premium
                            <div class="card-toolbar">
                                <a href="" class="btn btn-sm rounded-4 btn-danger">VIGIMAT</a>
                            </div>
                            @endpremium
                        </div>
                        <div class="card-body">
                            @premium
                            <x-base.alert
                                type="solid"
                                color="primary"
                                title="information"
                                icon="info-circle"
                                content="Vous pouvez avoir plus d'information sur vos engins roulants en accedant à l'interface VIGIMAT !" />
                            @else
                                <x-base.alert
                                    type="solid"
                                    color="primary"
                                    title="information"
                                    icon="info-circle"
                                    content="En ayant souscrit à un abonnement premium, vous accès à l'interface VIGIMAT qui vous permet d'avoir plus d'information sur vos matériels roulants, avoir le suivi des maintenances en cours et à venir !" />
                            @endpremium

                                <div class="d-flex flex-column">
                                    <div class="d-flex flex-row align-items-center mb-3 shadow-lg p-5">
                                        <div class="symbol symbol-70px me-3">
                                            <img src="{{ asset('/storage/icons/train_engine.png') }}" alt="">
                                        </div>
                                        <div class="d-flex flex-column">
                                            <span class="fs-6 fw-bold">Type de matériel roulant</span>
                                            <span class="fs-3">{{ $engine->engine->type_engine_format }}</span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row align-items-center mb-3 shadow-lg p-5">
                                        <div class="symbol symbol-70px me-3">
                                            <img src="{{ asset('/storage/icons/energy-system.png') }}" alt="">
                                        </div>
                                        <div class="d-flex flex-column">
                                            <span class="fs-6 fw-bold">Type de motorisation</span>
                                            <span class="fs-3">{{ $engine->engine->type_energy_format }}</span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row align-items-center mb-3 shadow-lg p-5">
                                        <div class="symbol symbol-70px me-3">
                                            <img src="{{ asset('/storage/icons/speedometer.png') }}" alt="">
                                        </div>
                                        <div class="d-flex flex-column">
                                            <span class="fs-6 fw-bold">Vitesse Max</span>
                                            <span class="fs-3">{{ $engine->engine->velocity }} Km/h</span>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6 mb-5">
                    <div class="card shadow-lg">
                        <div class="card-header">
                            <div class="card-title">
                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px h-30px me-3" alt="">
                                <span>Informations générales</span>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-3">
                                    <span>Date d'achat</span>
                                    <span class="fw-bold">{{ $engine->date_achat->format("d/m/Y") }}</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-3">
                                    <span>Prix d'achat</span>
                                    <span class="fw-bold text-red-500">{{ eur($engine->engine->price_achat) }}</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-3">
                                    <span>Résultat cumulé</span>
                                    <span class="fw-bold text-green-500">{{ eur($engine->calcResultat()) }}</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-3">
                                    <span>Rentabilité</span>
                                    @if($engine->rentability <= 100)
                                        <span class="fw-bold text-red-500">{{ $engine->rentability  }} %</span>
                                    @else
                                        <span class="fw-bold text-green-500">{{ $engine->rentability  }} %</span>
                                    @endif
                                </div>
                                <div class="shadow-lg p-5 mb-3">
                                    @if($engine->engine->type_train == 'ter')
                                        <div class="fw-bolder mb-1">Répartition des sièges ({{ $engine->engine->nb_passager }})</div>
                                        <div class="h-20px progress-stacked rounded">
                                            <div data-bs-toggle="tooltip" data-placement="bottom" title="2eme classe" class="progress h-20px" role="progressbar" aria-label="Segment one" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                <div class="progress-bar"></div>
                                            </div>
                                        </div>
                                    @elseif($engine->engine->type_train == 'tgv' || $engine->engine->type_train == 'ic')
                                        <div class="fw-bolder mb-1">Répartition des sièges</div>
                                        <div class="h-20px progress-stacked rounded">
                                            <div data-bs-toggle="tooltip" data-placement="bottom" title="1ere classe" class="progress h-20px" role="progressbar" aria-label="Segment one" aria-valuenow="{{ $engine->engine->getComposition()[0] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $engine->engine->getComposition()[0] }}%">
                                                <div class="progress-bar bg-red-700"></div>
                                            </div>
                                            <div data-bs-toggle="tooltip" data-placement="bottom" title="2eme classe" class="progress h-20px" role="progressbar" aria-label="Segment two" aria-valuenow="{{ $engine->engine->getComposition()[1] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $engine->engine->getComposition()[1] }}%">
                                                <div class="progress-bar bg-blue-600"></div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card bg-gray-400 shadow-box">
                <div class="card-header">
                    <div class="card-title">
                        <img src="{{ asset('/storage/icons/technician.png') }}" class="w-30px h-30px me-3" alt="">
                        <span class="fw-bold fs-1">Compte rendu de maintenance</span>
                    </div>
                    @if($engine->in_maintenance)
                        <div class="badge bg-amber-600 text-white animation-blink animation-blink-1">Maintenance en cours</div>
                    @endif
                </div>
                <div class="card-body">
                    <div class="fs-1 text-center text-uppercase mb-5">
                        Usé à <span class="fw-bolder">{{ $engine->use_percent }} %</span>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-2">
                                <span class="fs-1">Nombre de trajets</span>
                                <span class="fs-1 fw-bold">{{ $engine->nombre_trajet }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-2">
                                <span class="fs-1">Heure de trajet</span>
                                <span class="fs-1 fw-bold">{{ $engine->heure_trajet }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-2">
                                <span class="fs-1">Age</span>
                                <span class="fs-1 fw-bold">{{ $engine->date_achat->longAbsoluteDiffForHumans() }}</span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column align-items-center mb-10">
                                <span class="fs-1">Note d'ancienneté</span>
                                <span class="fs-1 fw-bold">{{ $engine->calcAncienneteEngine() }}/5</span>
                                <span class="fs-2 fs-italic">({{ $engine->calcUsureEngine() }} % d'usure / 100h de trajet)</span>
                            </div>
                            <div class="d-flex flex-row justify-content-around align-items-center">
                                <div class="d-flex flex-column align-items-center">
                                    <button data-action="maintenance" data-type="preventif" class="btn btn-primary rounded-4">Préventive</button>
                                    <span>{{ eur($engine->calcAmountMaintenancePrev()) }}</span>
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <button data-action="maintenance" data-type="curatif" class="btn btn-primary rounded-4">Curatif</button>
                                    <span>{{ eur($engine->calcAmountMaintenanceCur()) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="card card-flush shadow-lg">
        <div class="card-header">
            <div class="card-title align-items-center">
                <img src="{{ asset('/storage/logos/logo_'.$engine->engine->type_train.'.svg') }}" class="w-35px me-3" alt="">
                <span class="me-3">{{ $engine->engine->name }} | </span>
                <span>
                    <span class="badge bg-amber-500 text-white me-3">Hub</span>
                    {{ $engine->user_hub->hub->gare->name }} / {{ $engine->user_hub->hub->gare->region }} / {{ $engine->user_hub->hub->gare->pays }}
                </span>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-lg-6 mb-5">
                    <img src="/storage/engines/{{ $engine->type_engine }}/{{ $engine->image }}" class="w-135px my-5 mx-20" alt="">
                </div>
                <div class="col-sm-12 col-lg-6 mb-5">
                    <div class="d-flex flex-row justify-content-around align-items-center mb-10">
                        <div class="rounded bg-blue-600 text-white p-5 me-5 w-200px">
                            <div class="d-flex flex-column justify-content-center align-items-end">
                                <span class="fw-bold fs-2x">{{ $engine->max_runtime_engine }} Km</span>
                                <span>Rayon d'action de l'engin roulant</span>
                            </div>
                        </div>
                        <div class="rounded bg-amber-600 text-white p-5 me-5 w-200px">
                            <div class="d-flex flex-column justify-content-center align-items-end">
                                <span class="fw-bold fs-2x">{{ $engine->engine->velocity }} Km/h</span>
                                <span>Vitesse Max</span>
                            </div>
                        </div>
                        <div class="rounded bg-indigo-600 text-white p-5 me-5 w-200px">
                            <div class="d-flex flex-column justify-content-center align-items-end">
                                <span class="fw-bold fs-2x">{{ $engine->engine->nb_passager }}</span>
                                <span>Nombre de siège disponible</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-6 mb-5">
                    <div class="card shadow-lg">
                        <div class="card-header">
                            <div class="card-title">
                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px h-30px me-3" alt="">
                                <span>Caractéristique de l'engins</span>
                            </div>
                            @premium
                            <div class="card-toolbar">
                                <a href="" class="btn btn-sm rounded-4 btn-danger">VIGIMAT</a>
                            </div>
                            @endpremium
                        </div>
                        <div class="card-body">
                            @premium
                            <x-base.alert
                                type="solid"
                                color="primary"
                                title="information"
                                icon="info-circle"
                                content="Vous pouvez avoir plus d'information sur vos engins roulants en accedant à l'interface VIGIMAT !" />
                            @else
                                <x-base.alert
                                    type="solid"
                                    color="primary"
                                    title="information"
                                    icon="info-circle"
                                    content="En ayant souscrit à un abonnement premium, vous accès à l'interface VIGIMAT qui vous permet d'avoir plus d'information sur vos matériels roulants, avoir le suivi des maintenances en cours et à venir !" />
                                @endpremium

                                <div class="d-flex flex-column">
                                    <div class="d-flex flex-row align-items-center mb-3 shadow-lg p-5">
                                        <div class="symbol symbol-70px me-3">
                                            <img src="{{ asset('/storage/icons/train_engine.png') }}" alt="">
                                        </div>
                                        <div class="d-flex flex-column">
                                            <span class="fs-6 fw-bold">Type de matériel roulant</span>
                                            <span class="fs-3">{{ $engine->engine->type_engine_format }}</span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row align-items-center mb-3 shadow-lg p-5">
                                        <div class="symbol symbol-70px me-3">
                                            <img src="{{ asset('/storage/icons/energy-system.png') }}" alt="">
                                        </div>
                                        <div class="d-flex flex-column">
                                            <span class="fs-6 fw-bold">Type de motorisation</span>
                                            <span class="fs-3">{{ $engine->engine->type_energy_format }}</span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row align-items-center mb-3 shadow-lg p-5">
                                        <div class="symbol symbol-70px me-3">
                                            <img src="{{ asset('/storage/icons/speedometer.png') }}" alt="">
                                        </div>
                                        <div class="d-flex flex-column">
                                            <span class="fs-6 fw-bold">Vitesse Max</span>
                                            <span class="fs-3">{{ $engine->engine->velocity }} Km/h</span>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6 mb-5">
                    <div class="card shadow-lg">
                        <div class="card-header">
                            <div class="card-title">
                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px h-30px me-3" alt="">
                                <span>Informations générales</span>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-3">
                                    <span>Date d'achat</span>
                                    <span class="fw-bold">{{ $engine->date_achat->format("d/m/Y") }}</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-3">
                                    <span>Prix d'achat</span>
                                    <span class="fw-bold text-red-500">{{ eur($engine->engine->price_achat) }}</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-3">
                                    <span>Résultat cumulé</span>
                                    <span class="fw-bold text-green-500">{{ eur($engine->calcResultat()) }}</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-3">
                                    <span>Rentabilité</span>
                                    @if($engine->rentability <= 100)
                                        <span class="fw-bold text-red-500">{{ $engine->rentability  }} %</span>
                                    @else
                                        <span class="fw-bold text-green-500">{{ $engine->rentability  }} %</span>
                                    @endif
                                </div>
                                <div class="shadow-lg p-5 mb-3">
                                    @if($engine->engine->type_train == 'ter')
                                        <div class="fw-bolder mb-1">Répartition des sièges ({{ $engine->engine->nb_passager }})</div>
                                        <div class="h-20px progress-stacked rounded">
                                            <div data-bs-toggle="tooltip" data-placement="bottom" title="2eme classe" class="progress h-20px" role="progressbar" aria-label="Segment one" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                <div class="progress-bar"></div>
                                            </div>
                                        </div>
                                    @elseif($engine->engine->type_train == 'tgv' || $engine->engine->type_train == 'ic')
                                        <div class="fw-bolder mb-1">Répartition des sièges</div>
                                        <div class="h-20px progress-stacked rounded">
                                            <div data-bs-toggle="tooltip" data-placement="bottom" title="1ere classe" class="progress h-20px" role="progressbar" aria-label="Segment one" aria-valuenow="{{ $engine->engine->getComposition()[0] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $engine->engine->getComposition()[0] }}%">
                                                <div class="progress-bar bg-red-700"></div>
                                            </div>
                                            <div data-bs-toggle="tooltip" data-placement="bottom" title="2eme classe" class="progress h-20px" role="progressbar" aria-label="Segment two" aria-valuenow="{{ $engine->engine->getComposition()[1] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $engine->engine->getComposition()[1] }}%">
                                                <div class="progress-bar bg-blue-600"></div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card bg-gray-400 shadow-box">
                <div class="card-header">
                    <div class="card-title">
                        <img src="{{ asset('/storage/icons/technician.png') }}" class="w-30px h-30px me-3" alt="">
                        <span class="fw-bold fs-1">Compte rendu de maintenance</span>
                    </div>
                    @if($engine->in_maintenance)
                        <div class="badge bg-amber-600 text-white animation-blink animation-blink-1">Maintenance en cours</div>
                    @endif
                </div>
                <div class="card-body">
                    <div class="fs-1 text-center text-uppercase mb-5">
                        Usé à <span class="fw-bolder">{{ $engine->use_percent }} %</span>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-2">
                                <span class="fs-1">Nombre de trajets</span>
                                <span class="fs-1 fw-bold">{{ $engine->nombre_trajet }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-2">
                                <span class="fs-1">Heure de trajet</span>
                                <span class="fs-1 fw-bold">{{ $engine->heure_trajet }}</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center shadow-lg p-5 mb-2">
                                <span class="fs-1">Age</span>
                                <span class="fs-1 fw-bold">{{ $engine->date_achat->longAbsoluteDiffForHumans() }}</span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column align-items-center mb-10">
                                <span class="fs-1">Note d'ancienneté</span>
                                <span class="fs-1 fw-bold">4.5/5</span>
                                <span class="fs-2 fs-italic">({{ $engine->calcUsureEngine() }} % d'usure / 100h de trajet)</span>
                            </div>
                            <div class="d-flex flex-row justify-content-around align-items-center">
                                <div class="d-flex flex-column align-items-center">
                                    <button data-action="maintenance" data-type="preventif" class="btn btn-primary rounded-4">Préventive</button>
                                    <span>{{ eur($engine->calcAmountMaintenancePrev()) }}</span>
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <button data-action="maintenance" data-type="curatif" class="btn btn-primary rounded-4">Curatif</button>
                                    <span>{{ eur($engine->calcAmountMaintenanceCur()) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
