<?php

namespace App\Notifications\Game;

use App\Models\User;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

class SendMessageNotification extends Notification
{
    public function __construct(
        public string $title,
        public string $message,
        public string $type = 'info',
        public string $icon = 'ki-abstract-4',
        public string|null $url = "",
        public string|array $sector = ""
    )
    {
    }

    public function via($notifiable): array
    {
        return ['database', 'broadcast', WebPushChannel::class];
    }

    public function toDatabase($notifiable): array
    {
        return [
            'type' => $this->type,
            'icon' => $this->icon,
            'title' => $this->title,
            'description' => $this->message,
            'time' => now(),
            "sector" => $this->sector
        ];
    }

    public function toArray($notifiable): array
    {
        return [
            'type' => $this->type,
            'icon' => $this->icon,
            'title' => $this->title,
            'description' => $this->message,
            'time' => now(),
            "sector" => $this->sector
        ];
    }

    public function toBroadcast($notifiable): BroadcastMessage
    {
        return new BroadcastMessage([
            'type' => $this->type,
            'icon' => $this->icon,
            'title' => $this->title,
            'description' => $this->message,
            'time' => now(),
            'userId' => $notifiable->id,
            "sector" => $this->sector
        ]);
    }

    public function broadcastOn(): array
    {
        return [$this->sector.'-notification'];
    }

    public function toWebPush($notifiable, $notification): WebPushMessage
    {
        return (new WebPushMessage)
            ->title($this->title)
            ->icon($this->icon)
            ->body($this->message)
            ->action('Voir', $this->url)
            ->data(['userId' => $notification->id]);
    }
}
