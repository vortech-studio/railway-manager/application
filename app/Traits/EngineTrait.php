<?php

namespace App\Traits;

use Carbon\Carbon;
use Carbon\CarbonInterval;

trait EngineTrait
{
    public static function calcSurface(mixed $longueur, mixed $largeur, mixed $hauteur): float|int
    {
        return $longueur * $largeur * $hauteur;
    }

    public static function calcTractionForce(mixed $poids): float
    {
        return $poids * 0.036;
    }

    protected function getCoefTypeEssieux($type_essieux)
    {
        return match ($type_essieux) {
            "BB" => 1.5,
            "CC" => 3,
            "BOBO" => 2.3,
            "COCO" => 3.3
        };
    }

    protected function getCoefTypeMotorisation($type_motorisation)
    {
        return match ($type_motorisation) {
            "diesel" => 5,
            "electrique 1500V" => 4,
            "electrique 25000V" => 3.5,
            "electrique 1500V/25000V" => 3,
            "hybride" => 2,
            "vapeur" => 5.3,
            default => 1,
        };
    }

    protected function getCoefTypeMarchandise($type_marchandise)
    {
        return match ($type_marchandise) {
            "marchandises" => 2,
            "passagers" => 1.3,
            "none" => 1,
        };
    }

    protected function getCoefTypeTrain($type_train)
    {

        return match ($type_train) {
            "motrice" => 1.1,
            "remorque" => 1.2,
            "automotrice" => 1.9,
        };
    }

    public function calcDureeMaintenance($end_year, $surface, $start_year = 2023): string
    {
        if($end_year == 2023) {
            return $surface/45;
        } else {
            return $surface/($end_year-$start_year);
        }
    }

    public function calcPriceCheckout($longueur, $largeur, $hauteur, $poids, $essieux, $type_motor, $type_marchandise, $type_engine, $power_motor): float|int
    {
        $surface = self::calcSurface($longueur, $largeur, $hauteur);
        $traction_force = self::calcTractionForce($poids);

        $coef_essieux = self::getCoefTypeEssieux($essieux);
        $coef_motorisation = self::getCoefTypeMotorisation($type_motor);
        $coef_marchandise = self::getCoefTypeMarchandise($type_marchandise);
        $coef_type_train = self::getCoefTypeTrain($type_engine);

        return ($surface + $traction_force + $power_motor) * $coef_essieux * $coef_motorisation * $coef_marchandise * $coef_type_train;
    }

    public function calcPriceMaintenance($type_energy, $priceCheckout, $dureeMaintenance, $gasoil): float|int
    {
        if($type_energy == "diesel") {
            return ($priceCheckout/$dureeMaintenance)+($gasoil/20);
        } else {
            return ($priceCheckout/$dureeMaintenance);
        }
    }

    public function calcPriceLocation($priceCheckout): float
    {
        return $priceCheckout/30/1.2;
    }
}
