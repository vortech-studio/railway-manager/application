@extends("game.template")

@section("css")
    <link src="{{ asset('/plugins/leaflet/leaflet.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <input type="hidden" name="user_hub_id" value="{{ $hub->id }}">
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/network.png') }}" alt="">
            </div>
            <x-base.underline
                title="Réseau Férroviaire"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.network.partials.menu")
    </div>
    <div class="d-flex flex-wrap justify-content-center align-items-center bg-gray-400 px-5 py-3 rounded-bottom-3 mb-10">
        <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
            <li class="nav-item">
                <a class="nav-link text-white active" data-bs-toggle="tab" href="#detail_hub">
                    <span class="nav-icon me-2">
                        <img src="{{ asset('/storage/icons/hub.png') }}" class="w-35px" alt="">
                    </span>
                    <span class="nav-text">Détail du hub</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" data-bs-toggle="tab" href="#liste_ligne">
                    <span class="nav-icon me-2">
                        <img src="{{ asset('/storage/icons/ligne.png') }}" class="w-35px" alt="">
                    </span>
                    <span class="nav-text">Liste des lignes</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" data-bs-toggle="tab" href="#liste_engins">
                    <span class="nav-icon">
                        <img src="{{ asset('/storage/icons/train.png') }}" class="w-35px" alt="">
                    </span>
                    <span class="nav-text">Liste des engins</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" data-bs-toggle="tab" href="#planning_hub">
                    <span class="nav-icon">
                        <img src="{{ asset('/storage/icons/planning.png') }}" class="w-35px" alt="">
                    </span>
                    <span class="nav-text">Trajets sur le hub</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" data-bs-toggle="tab" href="#lab_hub">
                    <span class="nav-icon">
                        <img src="{{ asset('/storage/icons/research.png') }}" class="w-35px" alt="">
                    </span>
                    <span class="nav-text">Recherche & développement</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" data-bs-toggle="tab" href="#sell_hub">
                    <span class="nav-icon">
                        <img src="{{ asset('/storage/icons/hub_checkout.png') }}" class="w-35px" alt="">
                    </span>
                    <span class="nav-text">Vendre le hub</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="detail_hub" role="tabpanel">
            @premium
            <div class="row">
                <div class="col-sm-12 col-lg-6 mb-5">
                    <div class="card bg-gray-600 mb-10">
                        <div class="card-body h-450">
                            <div id="map" style="height: 430px"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6 mb-5">
                    @include("game.includes.ecran.eva.ecran_departure_list")
                </div>
            </div>
            @else
                @include("game.includes.ecran.eva.ecran_departure_list")
            @endpremium

            <x-base.underline
                title="<img src='{{ asset('/storage/icons/hub.png') }}' alt='hub' class='w-35px'> Détail du hub"
                size="3"
                size-text="fs-1" />
            <div class="card card-flush mb-10">
                <div class="card-header">
                    <div class="card-title">
                        <div class="badge badge-warning">Hub</div>
                        <div class="fs-3 fw-bolder ms-2">{{ $hub->hub->gare->name }} / {{ $hub->hub->gare->pays }}</div>
                    </div>
                    <div class="card-toolbar">
                        @if($hub->hub->status)
                            <div class="badge badge-lg badge-success">Ouvert</div>
                        @else
                            <div class="badge badge-lg badge-danger">Fermé</div>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Date d'achat :</div>
                                    <div class="fw-bolder">{{ $hub->date_achat->format("d/m/Y à H:i") }}</div>
                                </div>
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Passagers annuels :</div>
                                    <div class="fw-bolder">{{ number_format($hub->hub->gare->frequentation, 0, '.', ' ') }}</div>
                                </div>
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Performance des lignes :</div>
                                    {!! $hub->calcRatioPerformance() !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Kilométrage de ligne sur ce hub :</div>
                                    <div class="fw-bolder">{{ $hub->km_ligne }} Km</div>
                                </div>
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Taxe sur ce hub :</div>
                                    <div class="fw-bolder">{{ eur($hub->hub->taxe_hub) }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <x-base.underline
                title="<img src='{{ asset('/storage/icons/hub.png') }}' alt='hub' class='w-35px'> Statistiques"
                size="3"
                size-text="fs-1" />
            <div class="card card-flush mb-10">
                <div class="card-body w-25">
                    <div class="fw-bold fs-4">Chiffre d'affaires</div>
                    <div class="d-flex flex-column mb-5 mt-2">
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                            <div class="">Aujourd'hui</div>
                            <div class="fw-semibold">{{ eur($hub->calcCA(now(), now())) }}</div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                            <div class="">Hier</div>
                            <div class="fw-semibold">{{ eur($hub->calcCA(now()->subDay(), now()->subDay())) }}</div>
                        </div>
                    </div>
                    <div class="fw-bold fs-4">Bénéfices</div>
                    <div class="d-flex flex-column mb-5 mt-2">
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                            <div class="">Aujourd'hui</div>
                            <div class="fw-semibold">{{ eur($hub->calcBenefice(now(), now())) }}</div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                            <div class="">Hier</div>
                            <div class="fw-semibold">{{ eur($hub->calcBenefice(now()->subDay(), now()->subDay())) }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <x-base.underline
                title="<img src='{{ asset('/storage/icons/hub.png') }}' alt='hub' class='w-35px'> Résumé Financier"
                size="3"
                size-text="fs-1" />
            <div class="card card-flush mb-10">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="fw-bold fs-4">Détails Financiers</div>
                            <div class="d-flex flex-column mb-5 mt-2">
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Chiffre d'affaire</div>
                                    <div class="fw-semibold">{{ eur($hub->calcCA()) }}</div>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Frais de carburant</div>
                                    <div class="fw-semibold text-danger">{{ eur($hub->calcFraisCarburant()) }}</div>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Frais Electrique</div>
                                    <div class="fw-semibold text-danger">{{ eur($hub->calcFraisElectrique()) }}</div>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Taxes</div>
                                    <div class="fw-semibold text-danger">{{ eur($hub->calcTaxes()) }}</div>
                                </div>
                            </div>
                            <div class="fw-bold fs-4">Informations Incidents</div>
                            <div class="d-flex flex-column mb-5 mt-2">
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Nombre d'incidents (30 J)</div>
                                    <div class="fw-semibold">0</div>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Coût des incidents (30J)</div>
                                    <div class="fw-semibold text-danger">0,00 €</div>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Nombre de trajets non effectué (30J)</div>
                                    <div class="fw-semibold">0</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="fw-bold fs-4">Nombre de passagers</div>
                            <div class="d-flex flex-column mb-5 mt-2">
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Première classe</div>
                                    <div class="fw-semibold">{{ $hub->calcSumPassengers('first') }}</div>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Deuxième classe</div>
                                    <div class="fw-semibold">{{ $hub->calcSumPassengers('second') }}</div>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="fw-bold">Total</div>
                                    <div class="fw-bold text-danger">{{ $hub->calcSumPassengers('first') + $hub->calcSumPassengers('second') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="liste_ligne" role="tabpanel">
            @foreach(auth()->user()->hubs()->find($hub->id)->lignes as $ligne)
                <div class="d-flex flex-column p-5 mb-5 shadow-lg rounded-2 {{ $ligne->active ? 'bg-light-primary' : 'bg-gray-500' }}" @if($ligne->active == 0) data-bs-toggle="tooltip" title="Livraison en cours"  @endif>
                    <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                        <div class="d-flex flex-row">
                            <div class="badge badge-primary w-auto">Ligne</div>
                            <div class="fs-4 text-dark fw-bolder ms-5 me-3">{{ $ligne->ligne->name }}</div>
                        </div>
                        @if($ligne->active)
                        <a href="{{ route('ligne.show', $ligne->id) }}" class="btn btn-sm btn-flex btn-primary">
                            <img src="{{ asset('/storage/icons/ligne.png') }}" class="w-30px me-2" alt="">
                            Détail de la ligne
                        </a>
                        @endif
                    </div>
                    <div class="d-flex flex-row align-items-center mt-10">
                        {!! $ligne->calcPerformance() !!}
                        <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                        <span>
                                <span class="">Distance :</span>
                                <span class="fw-bolder">{{ $ligne->ligne->distance }} Km</span>
                            </span>
                        <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                        <span>
                                <span class="">Offre Restante (J-1) :</span>
                                <span class="fw-bolder">{{ $ligne->calcOffreRestante(now()->subDay(), now()->subDay()) }}</span>
                            </span>
                        <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                        <span>
                                <span class="">Chiffre d'affaires :</span>
                                <span class="fw-bolder">{{ eur($ligne->calcCA()) }}</span>
                            </span>
                        <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                        <span>
                                <span class="">Bénéfices :</span>
                                <span class="fw-bolder">{{ eur($ligne->calcBenefice()) }}</span>
                            </span>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="tab-pane fade" id="liste_engins" role="tabpanel">
            <div class="card card-flush mb-10">
                <div class="card-header">
                    <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x nav-stretch d-flex flex-row justify-content-around fs-4 w-100">
                        <li class="nav-item">
                            <a class="nav-link active d-flex flex-column" data-bs-toggle="tab" href="#all">
                                <span class="fw-bolder">Matériels Roulants</span>
                                <span class="fs-5">Tous</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex flex-column" data-bs-toggle="tab" href="#ter">
                                <span class="fw-bolder">Matériels Roulants</span>
                                <span class="fs-5">TER</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex flex-column" data-bs-toggle="tab" href="#tgv">
                                <span class="fw-bolder">Matériels Roulants</span>
                                <span class="fs-5">TGV</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex flex-column" data-bs-toggle="tab" href="#ic">
                                <span class="fw-bolder">Matériels Roulants</span>
                                <span class="fs-5">Intercité</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex flex-column" data-bs-toggle="tab" href="#other">
                                <span class="fw-bolder">Matériels Roulants</span>
                                <span class="fs-5">Autres</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="all" role="tabpanel">
                            @foreach(auth()->user()->hubs()->find($hub->id)->engines as $engine)
                                <div class="d-flex flex-column bg-light-primary p-5 mb-5 shadow-lg rounded-2 @if(!$engine->active) opacity-75 @endif" @if(!$engine->active) data-bs-toggle="tooltip" title="Livraison en cours" @endif>
                                    <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                                        <div class="d-flex flex-row">
                                            @if($engine->engine->type_train != 'other')
                                                <img
                                                    src="{{ asset('/storage/logos/logo_'.$engine->engine->type_train.'.svg') }}"
                                                    alt="" class="w-20px me-3">
                                            @else
                                                <img
                                                    src="{{ asset('/storage/icons/train.png') }}"
                                                    alt="" class="w-20px me-3">
                                            @endif
                                            <div class="fs-4 text-dark fw-bolder ms-5">{{ $engine->engine->name }} / {{ $engine->engine->number }}</div>
                                        </div>
                                        <a href="{{ route('engine.show', $engine->id) }}" class="btn btn-sm btn-flex btn-primary">
                                            <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px me-2" alt="">
                                            Détail du matériel
                                        </a>
                                    </div>
                                    <div class="d-flex flex-row align-items-center mt-10">
                                        <div class="d-flex flex-row align-items-center justify-content-center">
                                            @if($engine->engine->type_engine == 'automotrice')
                                                <img src="{{ asset('/storage/engines/'.$engine->engine->type_engine.'/'.$engine->engine->slug.'-0.gif') }}" alt="" class="w-150px me-3" />
                                            @else
                                                <img src="{{ asset('/storage/engines/'.$engine->engine->type_engine.'/'.$engine->engine->image) }}" alt="" class="w-150px me-3" />
                                            @endif
                                        </div>
                                        <div class="d-flex flex-row">
                                            <span>
                                                <span class="">Rayon Action :</span>
                                                <span class="fw-bolder">{{ number_format($engine->max_runtime_engine, 0, '', ' ') }} Km</span>
                                            </span>
                                            <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                            <span>
                                                <span class="">Places :</span>
                                                <span class="fw-bolder">{{ $engine->engine->nb_passager }}</span>
                                            </span>
                                            <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                            <span>
                                                <span class="">Usures :</span>
                                                <span class="fw-bolder">{{ $engine->calcUsureEngine() }} %</span>
                                            </span>
                                            <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                            <span>
                                                <span class="">Ancienneté :</span>
                                                <span class="fw-bolder">{{ $engine->calcAncienneteEngine() }}/5</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="ter" role="tabpanel">
                            @foreach(auth()->user()->hubs()->find($hub->id)->engines as $engine)
                                @if($engine->engine->type_train == 'ter')
                                    <div class="d-flex flex-column bg-light-primary p-5 mb-5 shadow-lg rounded-2 @if(!$engine->active) opacity-75 @endif" @if(!$engine->active) data-bs-toggle="tooltip" title="Livraison en cours" @endif>                                        <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                                            <div class="d-flex flex-row">
                                                @if($engine->engine->type_train != 'other')
                                                    <img
                                                        src="{{ asset('/storage/logos/logo_'.$engine->engine->type_train.'.svg') }}"
                                                        alt="" class="w-20px me-3">
                                                @else
                                                    <img
                                                        src="{{ asset('/storage/icons/train.png') }}"
                                                        alt="" class="w-20px me-3">
                                                @endif
                                                    <div class="fs-4 text-dark fw-bolder ms-5">{{ $engine->engine->name }} / {{ $engine->engine->number }}</div>
                                            </div>
                                            <a href="{{ route('engine.show', $engine->id) }}" class="btn btn-sm btn-flex btn-primary">
                                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px me-2" alt="">
                                                Détail du matériel
                                            </a>
                                        </div>
                                        <div class="d-flex flex-row align-items-center mt-10">
                                            <div class="d-flex flex-row align-items-center justify-content-center">
                                                @if($engine->engine->type_engine == 'automotrice')
                                                    <img src="{{ asset('/storage/engines/'.$engine->engine->type_engine.'/'.$engine->engine->slug.'-0.gif') }}" alt="" class="w-150px me-3" />
                                                @else
                                                    <img src="{{ asset('/storage/engines/'.$engine->engine->type_engine.'/'.$engine->engine->image) }}" alt="" class="w-150px me-3" />
                                                @endif
                                            </div>
                                            <div class="d-flex flex-row">
                                            <span>
                                                <span class="">Rayon Action :</span>
                                                <span class="fw-bolder">{{ number_format($engine->max_runtime_engine, 0, '', ' ') }} Km</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Places :</span>
                                                <span class="fw-bolder">{{ $engine->engine->nb_passager }}</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Usures :</span>
                                                <span class="fw-bolder">{{ $engine->calcUsureEngine() }} %</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Ancienneté :</span>
                                                <span class="fw-bolder">{{ $engine->calcAncienneteEngine() }}/5</span>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="tgv" role="tabpanel">
                            @foreach(auth()->user()->hubs()->find($hub->id)->engines as $engine)
                                @if($engine->engine->type_train == 'tgv')
                                    <div class="d-flex flex-column bg-light-primary p-5 mb-5 shadow-lg rounded-2 @if(!$engine->active) opacity-75 @endif" @if(!$engine->active) data-bs-toggle="tooltip" title="Livraison en cours" @endif>                                        <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                                            <div class="d-flex flex-row">
                                                @if($engine->engine->type_train != 'other')
                                                    <img
                                                        src="{{ asset('/storage/logos/logo_'.$engine->engine->type_train.'.svg') }}"
                                                        alt="" class="w-20px me-3">
                                                @else
                                                    <img
                                                        src="{{ asset('/storage/icons/train.png') }}"
                                                        alt="" class="w-20px me-3">
                                                @endif
                                                    <div class="fs-4 text-dark fw-bolder ms-5">{{ $engine->engine->name }} / {{ $engine->engine->number }}</div>
                                            </div>
                                            <a href="{{ route('engine.show', $engine->id) }}" class="btn btn-sm btn-flex btn-primary">
                                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px me-2" alt="">
                                                Détail du matériel
                                            </a>
                                        </div>
                                        <div class="d-flex flex-row align-items-center mt-10">
                                            <div class="d-flex flex-row align-items-center justify-content-center">
                                                @if($engine->engine->type_engine == 'automotrice')
                                                    <img src="{{ asset('/storage/engines/'.$engine->engine->type_engine.'/'.$engine->engine->slug.'-0.gif') }}" alt="" class="w-150px me-3" />
                                                @else
                                                    <img src="{{ asset('/storage/engines/'.$engine->engine->type_engine.'/'.$engine->engine->image) }}" alt="" class="w-150px me-3" />
                                                @endif
                                            </div>
                                            <div class="d-flex flex-row">
                                            <span>
                                                <span class="">Rayon Action :</span>
                                                <span class="fw-bolder">{{ number_format($engine->max_runtime_engine, 0, '', ' ') }} Km</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Places :</span>
                                                <span class="fw-bolder">{{ $engine->engine->nb_passager }}</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Usures :</span>
                                                <span class="fw-bolder">{{ $engine->calcUsureEngine() }} %</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Ancienneté :</span>
                                                <span class="fw-bolder">{{ $engine->calcAncienneteEngine() }}/5</span>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="ic" role="tabpanel">
                            @foreach(auth()->user()->hubs()->find($hub->id)->engines as $engine)
                                @if($engine->engine->type_train == 'ic')
                                    <div class="d-flex flex-column bg-light-primary p-5 mb-5 shadow-lg rounded-2 @if(!$engine->active) opacity-75 @endif" @if(!$engine->active) data-bs-toggle="tooltip" title="Livraison en cours" @endif>                                        <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                                            <div class="d-flex flex-row">
                                                @if($engine->engine->type_train != 'other')
                                                    <img
                                                        src="{{ asset('/storage/logos/logo_'.$engine->engine->type_train.'.svg') }}"
                                                        alt="" class="w-20px me-3">
                                                @else
                                                    <img
                                                        src="{{ asset('/storage/icons/train.png') }}"
                                                        alt="" class="w-20px me-3">
                                                @endif
                                                    <div class="fs-4 text-dark fw-bolder ms-5">{{ $engine->engine->name }} / {{ $engine->engine->number }}</div>
                                            </div>
                                            <a href="{{ route('engine.show', $engine->id) }}" class="btn btn-sm btn-flex btn-primary">
                                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px me-2" alt="">
                                                Détail du matériel
                                            </a>
                                        </div>
                                        <div class="d-flex flex-row align-items-center mt-10">
                                            <div class="d-flex flex-row align-items-center justify-content-center">
                                                @if($engine->engine->type_engine == 'automotrice')
                                                    <img src="{{ asset('/storage/engines/'.$engine->engine->type_engine.'/'.$engine->engine->slug.'-0.gif') }}" alt="" class="w-150px me-3" />
                                                @else
                                                    <img src="{{ asset('/storage/engines/'.$engine->engine->type_engine.'/'.$engine->engine->image) }}" alt="" class="w-150px me-3" />
                                                @endif
                                            </div>
                                            <div class="d-flex flex-row">
                                            <span>
                                                <span class="">Rayon Action :</span>
                                                <span class="fw-bolder">{{ number_format($engine->max_runtime_engine, 0, '', ' ') }} Km</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Places :</span>
                                                <span class="fw-bolder">{{ $engine->engine->nb_passager }}</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Usures :</span>
                                                <span class="fw-bolder">{{ $engine->calcUsureEngine() }} %</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Ancienneté :</span>
                                                <span class="fw-bolder">{{ $engine->calcAncienneteEngine() }}/5</span>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="other" role="tabpanel">
                            @foreach(auth()->user()->hubs()->find($hub->id)->engines as $engine)
                                @if($engine->engine->type_train == 'other')
                                    <div class="d-flex flex-column bg-light-primary p-5 mb-5 shadow-lg rounded-2 @if(!$engine->active) opacity-75 @endif" @if(!$engine->active) data-bs-toggle="tooltip" title="Livraison en cours" @endif>                                        <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                                            <div class="d-flex flex-row">
                                                @if($engine->engine->type_train != 'other')
                                                    <img
                                                        src="{{ asset('/storage/logos/logo_'.$engine->engine->type_train.'.svg') }}"
                                                        alt="" class="w-20px me-3">
                                                @else
                                                    <img
                                                        src="{{ asset('/storage/icons/train.png') }}"
                                                        alt="" class="w-20px me-3">
                                                @endif
                                                    <div class="fs-4 text-dark fw-bolder ms-5">{{ $engine->engine->name }} / {{ $engine->engine->number }}</div>
                                            </div>
                                            <a href="{{ route('engine.show', $engine->id) }}" class="btn btn-sm btn-flex btn-primary">
                                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px me-2" alt="">
                                                Détail du matériel
                                            </a>
                                        </div>
                                        <div class="d-flex flex-row align-items-center mt-10">
                                            <div class="d-flex flex-row align-items-center justify-content-center">
                                                @if($engine->engine->type_engine == 'automotrice')
                                                    <img src="{{ asset('/storage/engines/'.$engine->engine->type_engine.'/'.$engine->engine->slug.'-0.gif') }}" alt="" class="w-150px me-3" />
                                                @else
                                                    <img src="{{ asset('/storage/engines/'.$engine->engine->type_engine.'/'.$engine->engine->image) }}" alt="" class="w-150px me-3" />
                                                @endif
                                            </div>
                                            <div class="d-flex flex-row">
                                            <span>
                                                <span class="">Rayon Action :</span>
                                                <span class="fw-bolder">{{ number_format($engine->max_runtime_engine, 0, '', ' ') }} Km</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Places :</span>
                                                <span class="fw-bolder">{{ $engine->engine->nb_passager }}</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Usures :</span>
                                                <span class="fw-bolder">{{ $engine->calcUsureEngine() }} %</span>
                                            </span>
                                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                                <span>
                                                <span class="">Ancienneté :</span>
                                                <span class="fw-bolder">{{ $engine->calcAncienneteEngine() }}/5</span>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="planning_hub" role="tabpanel">
            <ul class="pagination mb-10">
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(6) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-6</span>
                        <small>{{ now()->subDays(6)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(5) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-5</span>
                        <small>{{ now()->subDays(5)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(4) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-4</span>
                        <small>{{ now()->subDays(4)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(3) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-3</span>
                        <small>{{ now()->subDays(3)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(2) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-2</span>
                        <small>{{ now()->subDays(2)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(1) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-1</span>
                        <small>{{ now()->subDays(1)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item active">
                    <a href="#" data-date="{{ now() }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-white">Aujourd'hui</span>
                        <small>{{ now()->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(1) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+1</span>
                        <small>{{ now()->addDays(1)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(2) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+2</span>
                        <small>{{ now()->addDays(2)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(3) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+3</span>
                        <small>{{ now()->addDays(3)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(4) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+4</span>
                        <small>{{ now()->addDays(4)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(5) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+5</span>
                        <small>{{ now()->addDays(5)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(6) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+6</span>
                        <small>{{ now()->addDays(6)->format("d/m/Y") }}</small>
                    </a>
                </li>
            </ul>
            <div class="card shadow-lg">
                <div class="card-body" id="loadTable">
                    <div class="table-responsive">
                        <div class="table-loading-message">
                            Chargement...
                        </div>
                        <table class="table table-rounded gs-2 gy-2 gx-2 align-middle" id="liste_travel">
                            <thead>
                            <tr>
                                <th class="bg-success text-white fw-semibold">Train</th>
                                <th class="bg-primary text-white fw-semibold text-end">Ligne</th>
                                <th class="bg-primary text-white fw-semibold text-end">Départ / Arrivée</th>
                                <th class="bg-primary text-white fw-semibold text-center">PAX</th>
                                <th class="bg-primary text-white fw-semibold text-end">Chiffre d'affaire</th>
                                <th class="bg-primary text-white fw-semibold text-end">Résultat</th>
                                <th class="bg-primary"></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($hub->plannings()->whereBetween('date_depart', [now()->startOfDay(), now()->endOfDay()])->get() as $planning)
                                    <tr @if($planning->status == 'initialized') class="table-active text-gray-500" data-bs-toggle="tooltip" data-bs-placement="top" title="Ce trajet n'à pas encore commencée" data-bs-custom-class="tooltip-inverse" @endif>
                                        <td><a href="" class="btn btn-link @if($planning->status == 'initialized') text-gray-300 @endif">{{ $planning->engine->engine->name }} - {{ $planning->engine->number }}</a></td>
                                        <td class="text-end">
                                            <span class="" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ $planning->ligne->ligne->name }}">
                                                {{ Str::upper(Str::limit($planning->ligne->ligne->stationStart->name, 3, '')) }}/{{ Str::upper(Str::limit($planning->ligne->ligne->stationEnd->name, 3, '')) }} <img src="{{ asset('/storage/logos/logo_'.$planning->ligne->ligne->type_ligne.'.svg') }}" alt="" class="w-15px" />
                                            </span>
                                        </td>
                                        <td class="text-end">{{ $planning->date_depart->format("H:i") }} / {{ $planning->date_arrived->format("H:i") }}</td>
                                        <td class="text-center">
                                            <span data-bs-toggle="popover" data-bs-html="true" title="{{ $planning->hub->calcSumPassengers(null, $planning->date_depart, $planning->date_arrived) }} Passagers" data-bs-content="Première classe: <span class='fw-bold'>{{ $planning->hub->calcSumPassengers('first', $planning->date_depart, $planning->date_arrived) }}</span><br>Seconde classe: <span class='fw-bold'>{{ $planning->hub->calcSumPassengers('second', $planning->date_depart, $planning->date_arrived) }}</span>">
                                                {{ $planning->hub->calcSumPassengers('first', $planning->date_depart, $planning->date_arrived) +  $planning->hub->calcSumPassengers('second', $planning->date_depart, $planning->date_arrived) }} pax
                                            </span>
                                        </td>
                                        <td class="text-end">{{ eur($planning->travel->calcCa()) }}</td>
                                        <td class="text-end">{{ eur($planning->travel->calcResultat()) }}</td>
                                        @if($planning->status != 'initialized')
                                        <td>
                                            <a href="{{ route('travel.show', $planning->id) }}" class="btn btn-link"><i class="fa-solid fa-magnifying-glass fs-3"></i></a>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="lab_hub" role="tabpanel">
            <livewire:game-skill-content :skills="$hub->skills"/>
        </div>
        <div class="tab-pane fade" id="sell_hub" role="tabpanel">
            @if(auth()->user()->hubs()->count() == 1)
                <!--begin::Alert-->
                <div class="alert alert-dismissible bg-danger d-flex flex-center flex-column py-10 px-10 px-lg-20 mb-10">
                    <!--begin::Close-->
                    <button type="button" class="position-absolute top-0 end-0 m-2 btn btn-icon btn-icon-danger" data-bs-dismiss="alert">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </button>
                    <!--end::Close-->

                    <!--begin::Icon-->
                    <i class="ki-duotone ki-information-5 fs-5tx text-light-danger mb-5"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                    <!--end::Icon-->

                    <!--begin::Wrapper-->
                    <div class="text-center">
                        <!--begin::Title-->
                        <h1 class="fw-bold mb-5 text-white">Ceci est votre dernier HUB</h1>
                        <!--end::Title-->

                        <!--begin::Separator-->
                        <div class="separator separator-dashed border-danger opacity-25 mb-5"></div>
                        <!--end::Separator-->

                        <!--begin::Content-->
                        <div class="mb-9 text-white">
                            Vous ne pouvez pas vendre votre dernier HUB, vous devez en posséder au moins un.
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Alert-->
            @else
                <div class="row">
                    <div class="col-8">
                        <div class="card shadow-lg mb-10">
                            <div class="card-header">
                                <div class="card-title">
                                    <x-base.underline
                                        title="<img src='{{ asset('/storage/icons/hub.png') }}' alt='Hub' class='w-35px'> Vente d'un hub'"
                                        size="3"
                                        size-text="fs-1" />
                                </div>
                            </div>
                            <div class="card-body">
                                <x-base.alert
                                    type="solid"
                                    color="warning"
                                    icon="exclamation-triangle"
                                    title="Attention"
                                    content="La vente d'un hub se fait sur la base de son utilisation sur les 7 derniers jours.<br>L'ensemble de vos lignes vont également être vendue." />

                                <div class="d-flex flex-column bg-light-primary p-5 mb-5 shadow-lg rounded-2">
                                    <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                                        <div class="d-flex flex-row">
                                            <div class="badge badge-primary">Hub</div>
                                            <div class="fs-4 text-dark fw-bolder ms-5">{{ $hub->hub->gare->name }}</div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row align-items-center mt-10">
                                        <i class="fa-solid fa-arrow-up text-success fs-2"></i>
                                        <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                        <!-- TODO: A Modifier lors de l'ajout de la gestion des services du hub -->
                                        <span>
                                        <span class="">Nombre de Commerce :</span>
                                        <span class="fw-bolder">0</span>
                                    </span>
                                        <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                        <span>
                                            <span class="">Nombre de Spot Publicitaire</span>
                                            <span class="fw-bolder">0</span>
                                        </span>
                                        <span>
                                            <span class="">Nombre de place de parking</span>
                                            <span class="fw-bolder">0</span>
                                        </span>
                                        <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                        <span>
                                        <span class="">Chiffre d'affaires :</span>
                                        <span class="fw-bolder">{{ eur($hub->CAseven()) }}</span>
                                    </span>
                                        <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                        <span>
                                        <span class="">Bénéfices :</span>
                                        <span class="fw-bolder">{{ eur($hub->calcResultatSeven()) }}</span>
                                    </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card shadow-lg">
                            <div class="card-header">
                                <h3 class="card-title">Vente</h3>
                            </div>
                            <div class="card-body">
                                <div class="d-flex flex-column">
                                    <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-500 p-5 mb-2">
                                        <div class="">Prix Achat</div>
                                        <div class="">{{ eur($hub->hub->price) }}</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-500 p-5 mb-2">
                                        <div class="">Bénéfice</div>
                                        <div class="">{{ eur($hub->calcResultatSeven()) }}</div>
                                    </div>
                                    <div class="separator border-2 border-gray-600 my-5"></div>
                                    <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-500 p-5 mb-2">
                                        <div class="">Ventes des lignes</div>
                                        @php
                                            $selling = 0;
                                            foreach($hub->lignes as $ligne) {
                                                $selling += $ligne->simulateSelling();
                                            }
                                        @endphp
                                        <div class="">{{ eur($selling) }}</div>
                                    </div>
                                    <div class="separator border-2 border-gray-600 my-5"></div>
                                    <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-500 fs-2 fw-bold p-5 mb-2">
                                        <div class="">Total de la vente <small>(Hors lignes)</small></div>
                                        <div class="p-2 text-success">+ {{ eur($hub->simulateSelling()) }}</div>
                                    </div>
                                    <button class="btn btn-flex btn-lg btn-light-success text-center sell" data-amount="{{ eur($hub->simulateSelling()) }}">
                                        <i class="fa-solid fa-euro-sign fs-2 me-2 text-success"></i>
                                        Vendre le hub
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>


@endsection

@section("script")
    <script src="{{ asset('/plugins/leaflet/leaflet.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/map.js') }}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinysort/2.3.6/tinysort.js"></script>
    <script src="{{ asset('/assets/js/game/network/show.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.ajax({
            url: '/api/game/hub/{{ $hub->id }}',
            data: {"user_id": {{ auth()->user()->id }}},
            success: data => {
                let map = initMap()
                map.on('load', () => {
                    formatForHub(map, data.hub)

                    let upLignes = [];
                    data.hub.lignes.forEach(ligne => {
                        markerForStationEnd(map, ligne)
                        upLignes.push(ligne)
                        ligne.stations.forEach(station => {
                            if(station.gares_id !== ligne.station_start.id && station.gares_id !== ligne.station_end.id ) {
                                markerForStation(map, station)
                            }
                        })
                    })
                    formatForLigne(map, upLignes)
                })

            }
        })

        /*$.ajax({
            url: '{{ route('api.game.network.hub.lines', $hub->id) }}',
            method: 'GET',
            data: {"user_id": {{ auth()->user()->id }}},
            success: data => {
                MapInitLignes(data.lignes, 'mapLigne')
            }
        })*/

        if(document.querySelector('.sell')) {
            document.querySelector('.sell').addEventListener('click', e => {
                e.preventDefault()

                Swal.fire({
                    title: 'Vendre le hub',
                    text: "Êtes-vous sûr de vouloir vendre le hub ainsi que les lignes affiliés ?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Vendre',
                    cancelButtonText: 'Annuler'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: '{{ route('api.game.network.hub.sell', [$hub->id]) }}',
                            method: 'POST',
                            data: {"user_id": {{ auth()->user()->id }}, 'amount': e.target.dataset.amount},
                            success: () => {
                                Swal.fire({
                                    title: 'Vendu !',
                                    text: "Le hub a bien été vendu",
                                    icon: 'success',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Ok',
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        window.location.href = '{{ route('network') }}'
                                    }
                                })
                            }
                        })
                    }
                })
            })
        }
    </script>
    @if(auth()->user()->livraisons()->where('type', 'research')->exists())
        <script type="text/javascript">
            let blockElement = document.querySelector("#gameSkillContent")
            let blockUi = new KTBlockUI(blockElement, {
                overlayClass: 'bg-opacity-75 bg-grey-200',
                message: `<div class="blockui-message fs-2"><span class="spinner-border text-primary"></span> Recherche en cours...</div>`
            })

            blockUi.block()

        </script>
    @endif
@endsection
