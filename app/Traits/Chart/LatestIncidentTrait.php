<?php

namespace App\Traits\Chart;

use App\Models\User\UserIncident;
use App\Models\User\UserTechnicentre;

trait LatestIncidentTrait
{
    public static function get()
    {
        $technicentre = UserTechnicentre::find(\Session::get('technicentre')[0]['id']);

        $labels = [];

        for($i=0; $i < 30; $i++) {
            $date = \Carbon\Carbon::now()->subDays($i)->format('d/m');
            $labels[] = $date;
        }

        $datas = [];
        for ($i=0; $i < 30; $i++) {
            $datas[] = UserIncident::join('type_incidents', 'type_incidents.id', '=', 'user_incidents.type_incident_id')
                ->where('user_hub_id', $technicentre->user_hub_id)
                ->where('type_incidents.origine', 'materiels')
                ->whereBetween('created_at', [now()->subDays($i)->startOfDay(), now()->subDays($i)->endOfDay()])
                ->count();
        }

        $datas = collect($datas);

        return response()->json([
            "labels" => $labels,
            "content" => $datas->toArray()
        ]);

    }
}
