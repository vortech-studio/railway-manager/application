<?php

namespace App\Http\Controllers\Api\Game\Maintenance;

use App\Http\Controllers\Controller;
use App\Models\User\UserIncident;
use Illuminate\Http\Request;

class IncidentController extends Controller
{
    public function list(Request $request)
    {
        if($request->has('planning_id')) {
            return response()->json([
                "incidents" => UserIncident::with('type')->where('user_planning_id', $request->get('planning_id'))->get()
            ]);
        } else {
            return response()->json([
                "incidents" => UserIncident::with('type')->where('user_id', $request->get('user_id'))->get()
            ]);
        }
    }

    public function info(Request $request, $incident_id)
    {
        return response()->json([
            "incident" => UserIncident::with('type')->find($incident_id)
        ]);
    }
}
