<div class="fw-bolder">Bonjour, {{ $user->name }},</div>
<p>
    Votre achat a bien été effectué.<br>
    Vous pouvez retrouver le détail de votre achat dans votre espace personnel.
</p>
<p>
    Cordialement,<br>
    L'équipe {{ config('app.name') }}
</p>
