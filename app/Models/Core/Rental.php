<?php

namespace App\Models\Core;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function engines()
    {
        return $this->belongsToMany(Engine::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
