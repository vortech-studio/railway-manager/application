<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGareEquipementsTable extends Migration
{
    public function up(): void
    {
        Schema::create('gare_equipements', function (Blueprint $table) {
            $table->id();
            $table->string('code_uic');
            $table->string('type_equipement');

            $table->foreignId('gares_id')
               ->nullable()
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('gare_equipements');
    }
}
