<?php

namespace App\Models\Core;

use App\Models\User\UserIncident;
use Illuminate\Database\Eloquent\Model;

class TypeIncident extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $appends = ['image_origin_url', 'origin_text'];

    public function incidents()
    {
        return $this->hasMany(UserIncident::class);
    }

    public function getImageOriginUrlAttribute()
    {
        return match ($this->origine) {
            "infrastructures" => asset('/storage/other/incident_infrastructure.jpg'),
            "materiels" => asset('/storage/other/incident_materiel.webp'),
            "humains" => asset('/storage/other/incident_humain.jpg'),
        };
    }

    public function getOriginTextAttribute()
    {
        return match ($this->origine) {
            "infrastructures" => "Infrastructure",
            "materiels" => "Materiel",
            "humains" => "Humain",
        };
    }
}
