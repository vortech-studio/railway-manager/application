<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User\UserEngine;
use App\Models\User\UserTechnicentre;
use App\Models\User\UserTechnicentreStaf;
use App\Models\User\UserTechnicentreTask;
use App\Services\Osmose\Engine;
use App\Traits\Chart\LatestIncidentEngineChartTrait;
use App\Traits\Chart\LatestIncidentTrait;
use App\Traits\Chart\StatVigimatTrait;
use App\Traits\Chart\SumKilometerChartTrait;
use App\Traits\Chart\UsedEnginesTrait;
use App\Traits\UserEngineTrait;
use Illuminate\Http\Request;

class VigimatController extends Controller
{
    public function technicentre($technicentre_id)
    {
        $technicentre = UserTechnicentre::find($technicentre_id);

        $info = [
            "name" => $technicentre->hub->hub->gare->name,
            "id" => $technicentre_id,
        ];

        \Session::push('technicentre', $info);

        return response()->json();
    }

    public function charts(Request $request)
    {
        return match($request->get('type')) {
            "latestIncident" => LatestIncidentTrait::get(),
            "usedEngines" => UsedEnginesTrait::get(),
            "stat" => StatVigimatTrait::get(),
            "statKilometer" => SumKilometerChartTrait::get($request->get('engine_id')),
            "latestIncidentEngine" => LatestIncidentEngineChartTrait::get($request->get('engine_id')),
        };
    }

    public function audit()
    {
        $engines = UserTechnicentre::find(\Session::get('technicentre')[0]['id'])->engines;
        $rapport = [];

        foreach ($engines as $engine) {
            $tauxUsureInitial = $engine->engine->engine->calcVitesseUsure();
            $limitKm = $engine->engine->max_runtime_engine;
            $kilometrageParcourue = $engine->engine->calcTravelKilometer();
            $maintenancePreventiveEffectuee = $engine->engine->calcLatestMaintenancePreventive();
            $anciennete = $engine->engine->calcAncienneteEngine();

            $usureTotale = UserEngineTrait::calculerUsureTotale(
                $tauxUsureInitial,
                $kilometrageParcourue,
                $limitKm,
            );

            $maintenanceCurativeRequired = $anciennete < 1 || $usureTotale >= 90;

            // Générer une description et des conseils personnalisés
            $description = "Le matériel {$engine->engine->engine->name} a une usure totale de {$usureTotale}%.";
            $conseils = [];

            if ($usureTotale >= $limitKm) {
                $conseils[] = "La maintenance préventive est recommandée en raison de l'usure élevée.";
            }

            if ($maintenanceCurativeRequired) {
                $conseils[] = "Une maintenance curative est recommandée en raison de l'ancienneté ou de l'usure critique.";
            }

            $rapport[] = [
                'id' => $engine->engine->id,
                'nom' => $engine->engine->engine->name." | ".$engine->engine->number,
                'anciennete' => $anciennete,
                'usure_totale' => $usureTotale,
                'maintenance_preventive_required' => $usureTotale >= $limitKm,
                'maintenance_curative_required' => $maintenanceCurativeRequired,
                'description' => $description,
                'conseils' => $conseils,
            ];
        }

        return response()->json([
            "rapport" => $rapport
        ]);
    }

    public function userEngine(Request $request, $engine_id)
    {
        return response()->json([
            "engine" => UserEngine::with('engine', 'user_hub', 'plannings', 'user_ligne', 'incidents', 'tasks', 'livraisons')->find($engine_id)
        ]);
    }

    public function userEngineAudit($engine_id)
    {
        $engine = UserEngine::find($engine_id);
        $tauxUsureInitial = $engine->engine->calcVitesseUsure();
        $limitKm = $engine->max_runtime_engine;
        $kilometrageParcourue = $engine->calcTravelKilometer();
        $maintenancePreventiveEffectuee = $engine->calcLatestMaintenancePreventive();
        $anciennete = $engine->calcAncienneteEngine();

        $usureTotale = UserEngineTrait::calculerUsureTotale(
            $tauxUsureInitial,
            $kilometrageParcourue,
            $limitKm,
        );

        $maintenanceCurativeRequired = $anciennete < 1 || $usureTotale >= 90;

        // Générer une description et des conseils personnalisés
        $description = "Le matériel {$engine->engine->name} a une usure totale de {$usureTotale}%.";
        $conseils = [];

        if ($usureTotale >= $limitKm) {
            $conseils[] = "La maintenance préventive est recommandée en raison de l'usure élevée.";
        }

        if ($maintenanceCurativeRequired) {
            $conseils[] = "Une maintenance curative est recommandée en raison de l'ancienneté ou de l'usure critique.";
        }

        $rapport[] = [
            'id' => $engine->id,
            'nom' => $engine->engine->name." | ".$engine->number,
            'anciennete' => $anciennete,
            'usure_totale' => $usureTotale,
            'maintenance_preventive_required' => $usureTotale >= $limitKm,
            'maintenance_curative_required' => $maintenanceCurativeRequired,
            'description' => $description,
            'conseils' => $conseils,
        ];

        return response()->json([
            "rapport" => $rapport
        ]);
    }

    public function maintenancePreventive(Request $request, $engine_id)
    {
        $engine = UserEngine::find($engine_id);

        try {
            $engine->createMaintenanceFromDate('preventif', $request->get('date'));

            return response()->json(null, 200);
        }catch (\Exception $exception) {
            \Log::emergency($exception->getMessage(), [$exception]);
            return response()->json(["message" => "Erreur lors de la création de la maintenance"], 500);
        }
    }

    public function showMaintenancePreventive($engine_id, $maintenance_id)
    {
        $engine = UserEngine::find($engine_id);

        return response()->json($engine->technicentre->technicentre->tasks()->with('tasks', 'staff', 'equipement', 'engine')->find($maintenance_id));
    }

    public function osmoseEngine($user_engine_id)
    {
        $osmose = new Engine();
        $data = $osmose->retrieve($user_engine_id);

        return response()->json($data);
    }
}
