<?php

namespace App\Jobs\Game\Rental;

use App\Models\Core\Mouvement;
use App\Models\User\Mailbox;
use App\Models\User\UserRental;
use App\Notifications\Game\SendMessageNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RentalEndingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserRental $userRental)
    {
    }

    public function handle(): void
    {
        if($this->userRental->status == 'active') {
            Mouvement::adding(
                'revenue',
                'Restitution de la location du matériel roulant: ' . $this->userRental->rental->name,
                "location_materiel",
                $this->userRental->amount_caution,
                auth()->user()->company->id,
                $this->userRental->user_engine->user_hub->id,
            );

            $this->userRental->user_engine->plannings()->delete();
            $this->userRental->user_engine->delete();

            $this->userRental->status = 'terminated';
            $this->userRental->save();

            $this->userRental->user->notify(new SendMessageNotification(
                'Location de matériel roulant',
                "Location de matériel roulant: " . $this->userRental->rental->name . " terminé.",
                "info",
                asset('/storage/icons/train_rent.png'),
                route('engine.show', $this->userRental->user_engine->engine->id),
                ["engine", "finance"]
            ));

            Mailbox::create([
                "from" => $this->userRental->rental->name,
                "subject" => "Fin de contrat de location ".$this->userRental->user_engine->engine->name." - Restitution du matériel et de la caution",
                "message" => view('game.includes.mailbox.ending_rental', [
                    "rental_name" => $this->userRental->rental->name,
                    "duration" => $this->userRental->duration_contract,
                    "name_engine" => $this->userRental->user_engine->engine->name,
                    "amount_caution" => $this->userRental->amount_caution
                ]),
                "user_id" => $this->userRental->user->id,
            ]);
        }
    }
}
