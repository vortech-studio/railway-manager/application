<?php

namespace App\Traits;

use App\Models\Core\Mouvement;
use App\Models\User\UserCompany;
use Carbon\Carbon;
use Spatie\SchemaOrg\Car;

trait MouvementTrait
{
    /**
     * @param string $type // charge / revenue
     * @param string $title
     * @param string $mouvement / 'electricite','gasoil','salaire_technicien','salaire_commercial','salaire_administratif','salaire_reseau','pret','interet','taxe','maintenance_vehicule','maintenance_technicentre','billetterie','rent_trajet_aux','commerce','publicite','parking','impot','subvention','achat_materiel','achat_ligne','location_materiel','cout_trajet','divers','vente_hub','vente_ligne','vente_engins','achat_hub'
     * @param float $amount Le montant doit être positif ou négatif
     * @param int $company_id
     * @param int|null $hub_id
     * @param int|null $ligne_id
     * @return Mouvement
     */
    public static function adding(string $type, string $title, string $mouvement, float $amount, int $company_id, int $hub_id = null, int $ligne_id = null): Mouvement
    {
        return match ($type) {
            "charge" => self::charging($title, $mouvement, $amount, $company_id, $hub_id, $ligne_id),
            "revenue" => self::revenue($title, $mouvement, $amount, $company_id, $hub_id, $ligne_id),
        };
    }

    private static function charging(string $title, string $mouvement, float $amount, int $company_id, int $hub_id = null, int $ligne_id = null)
    {
        $mvm = new Mouvement();
        $mvm->title = $title;
        $mvm->amount = $amount;
        $mvm->type_account = "charge";
        $mvm->type_mvm = $mouvement;
        $mvm->user_company_id = $company_id;
        $mvm->user_hub_id = $hub_id ?? null;
        $mvm->user_ligne_id = $ligne_id ?? null;
        $mvm->save();

        $company = UserCompany::find($company_id);

        $company->user->update([
            'argent' => $company->user->argent - $amount
        ]);

        return $mvm->orderBy('created_at', 'desc')->first();
    }

    private static function revenue(string $title, string $mouvement, float $amount, int $company_id, int $hub_id = null, int $ligne_id = null)
    {
        $mvm = new Mouvement();
        $mvm->title = $title;
        $mvm->amount = $amount;
        $mvm->type_account = "revenue";
        $mvm->type_mvm = $mouvement;
        $mvm->user_company_id = $company_id;
        $mvm->user_hub_id = $hub_id ?? null;
        $mvm->user_ligne_id = $ligne_id ?? null;
        $mvm->save();

        $company = UserCompany::find($company_id);

        $company->user->update([
            'argent' => $company->user->argent + $amount
        ]);

        return $mvm->orderBy('created_at', 'desc')->first();
    }

    public static function getCAFromWeek(Carbon $week)
    {
        return intval(Mouvement::where('type_account', 'revenue')
            ->whereBetween('created_at', [$week->startOfWeek(), $week->endOfWeek()])
            ->sum('amount'));
    }

    public static function getCostTravelFromWeek(Carbon $week)
    {
        $mouvements = Mouvement::whereBetween('created_at', [$week->startOfWeek(), $week->endOfWeek()])->get();
        $cost = 0;
        foreach ($mouvements as $mouvement) {
            if ($mouvement->type_mvm === "cout_trajet") {
                $cost += $mouvement->amount;
            }
        }
        return $cost;
    }

    public static function getResultatTravelFromWeek(Carbon $week)
    {
        $ca = Mouvement::getCAFromWeek($week);
        $cost = Mouvement::getCostTravelFromWeek($week);

        return $ca - $cost;
    }

    public static function getRembEmpruntFromWeek(Carbon $week)
    {
        $mouvements = Mouvement::whereBetween('created_at', [$week->startOfWeek(), $week->endOfWeek()])->get();
        $remb = 0;
        foreach ($mouvements as $mouvement) {
            if ($mouvement->type_mvm === "pret") {
                $remb += $mouvement->amount;
            }
        }
        return $remb;
    }

    public static function getCostLocationFromWeek(Carbon $week)
    {
        $mouvements = Mouvement::whereBetween('created_at', [$week->startOfWeek(), $week->endOfWeek()])->get();
        $cost = 0;
        foreach ($mouvements as $mouvement) {
            if ($mouvement->type_mvm === "location_materiel") {
                $cost += $mouvement->amount;
            }
        }
        return $cost;
    }

    public static function getTresoStructurel(Carbon $day)
    {
        $salaire = Mouvement::where('type_mvm', 'salaire_technicien')
            ->orWhere('type_mvm', 'salaire_commercial')
            ->orWhere('type_mvm', 'salaire_administratif')
            ->orWhere('type_mvm', 'salaire_reseau')
            ->whereBetween('created_at', [$day->subDay()->startOfDay(), $day->subDay()->endOfDay()])
            ->sum('amount');

        $maintenance = Mouvement::where('type_mvm', 'maintenance_vehicule')
            ->orWhere('type_mvm', 'maintenance_technicentre')
            ->whereBetween('created_at', [$day->subDay()->startOfDay(), $day->subDay()->endOfDay()])
            ->sum('amount');

        $location = Mouvement::where('type_mvm', 'location_materiel')
            ->whereBetween('created_at', [$day->startOfWeek(), $day])
            ->sum('amount')/7;

        $remboursement = Mouvement::where('type_mvm', 'pret')
            ->whereBetween('created_at', [$day->startOfWeek(), $day])
            ->sum('amount')/7;

        $impot = Mouvement::where('type_mvm', 'impot')
            ->whereBetween('created_at', [$day->startOfWeek(), $day])
            ->sum('amount')/7;

        $revenue = Mouvement::where('type_account', 'revenue')
            ->whereBetween('created_at', [$day->subDay()->startOfDay(), $day->subDay()->endOfDay()])
            ->sum('amount')/7;

        return $revenue - $salaire - $maintenance - $location - $remboursement - $impot;
    }

    public static function getBenefice(Carbon $day)
    {
        $billetterie = Mouvement::where('type_mvm', 'billetterie')
            ->whereBetween('created_at', [$day->subDay()->startOfDay(), $day->subDay()->endOfDay()])
            ->sum('amount');

        $cout = Mouvement::where('type_mvm', 'cout_trajet')
            ->whereBetween('created_at', [$day->subDay()->startOfDay(), $day->subDay()->endOfDay()])
            ->sum('amount');

        return $billetterie - $cout;
    }
}
