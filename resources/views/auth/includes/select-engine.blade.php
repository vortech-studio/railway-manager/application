<div class="d-flex flex-row align-items-center">
    <div class="symbol symbol-50px symbol-2by3 me-4">
        <img src="/storage/engines/{{ $engine->type_engine }}/{{ $engine->image }}" alt="Logo" />
    </div>
    <div class="d-flex flex-column">
        <div class="fs-3 mb-5">{{ $engine->name }}</div>
        <div class="d-flex flex-row justify-content-between align-items-center">
            <div class="border border-2 border-gray-300 rounded-2 px-2 py-1">
                <img src="/storage/icons/type_energy/{{ $engine->type_energy }}.png" alt="" data-bs-toggle="tooltip" title="{{ Str::ucfirst($engine->type_energy) }}">
            </div>
        </div>
    </div>
</div>
