<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic
Product Version: 8.1.8
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="{{ config('app.locale') }}">
<!--begin::Head-->
<head><base href="../../../"/>
    {{ seo()->render() }}
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    @vite(["resources/css/app.css"])
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="app-blank bgi-size-cover bgi-attachment-fixed bgi-position-center">
<!--begin::Theme mode setup on page load-->
<script>var defaultThemeMode = "light"; var themeMode; if ( document.documentElement ) { if ( document.documentElement.hasAttribute("data-bs-theme-mode")) { themeMode = document.documentElement.getAttribute("data-bs-theme-mode"); } else { if ( localStorage.getItem("data-bs-theme") !== null ) { themeMode = localStorage.getItem("data-bs-theme"); } else { themeMode = defaultThemeMode; } } if (themeMode === "system") { themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light"; } document.documentElement.setAttribute("data-bs-theme", themeMode); }</script>
<!--end::Theme mode setup on page load-->
<!--begin::Root-->
<div class="d-flex flex-column flex-root" id="kt_app_root">
    <!--begin::Page bg image-->
    <style>body { background-image: url('/storage/other/wall-rm.png'); } [data-bs-theme="dark"] body { background-image: url('/storage/other/wall-rm.png'); }</style>
    <!--end::Page bg image-->
    <!--begin::Authentication - Sign-in -->
    <div class="d-flex flex-column flex-lg-row flex-column-fluid">
        <!--begin::Aside-->
        <div class="d-flex flex-lg-row-fluid w-50">
            <!--begin::Content-->
            <div class="d-flex flex-column flex-center pb-0 pb-lg-10 p-10 w-100 bg-white bg-opacity-75">
                <div class="d-flex flex-row p-5 mb-10 shadow-lg">
                    <div class="symbol symbol-55px symbol-circle me-5">
                        <img src="/assets/media/avatars/300-1.jpg" alt="">
                    </div>
                    <div class="d-flex flex-column">
                        <p>
                            Dans {{ config('app.name') }}, vous avez la charge de votre entreprise ferroviaire.<br>
                            Afin de bien demarrer, il vous faut acheter votre premier <strong>HUB</strong>.<br>
                            Un <strong>HUB</strong> est une gare principal ou vos engins roulant parcouront des lignes
                            en generant des profits en fonctions de plusieurs point, mais nous verrons cela plus tard.
                        </p>
                        <p>Veuillez donc choisir votre premier <strong>HUB</strong>.</p>
                    </div>
                </div>
            </div>
            <!--end::Content-->
        </div>
        <!--begin::Aside-->
        <!--begin::Body-->
        <div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12 bg-white bg-opacity-50 w-75">
            <!--begin::Wrapper-->
            <div class="bg-body d-flex flex-column flex-center rounded-4 w-md-1000px p-10">
                <!--begin::Content-->
                <div class="d-flex flex-center flex-column align-items-stretch h-lg-100 w-md-800px">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-center flex-column flex-column-fluid pb-15 pb-lg-20">
                        <!--begin::Form-->
                        <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="{{ route('oauth.configStore', ["step" => "2"]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-10">
                                <label for="hub_id" class="form-label required">Hub Disponible</label>
                                <select id="hub_id" class="form-control selectpicker" name="hub_id" data-live-search="true">
                                    <option value="">-- Choisir un hub --</option>
                                    @foreach($hubs as $hub)
                                        <option value="{{ $hub->id }}">{{ $hub->gare->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row mb-10" data-content="loadInfoHub">
                                <div class="col-8">
                                    <!-- Carte -->
                                    <div id="map" style="height: 450px;"></div>
                                </div>
                                <div class="col-4">
                                    <div class="d-flex flex-column">
                                        <div class="d-flex flex-row">
                                            <div class="fs-2 fw-bolder">Information sur le Hub</div>
                                        </div>
                                        <div class="separator border-2 border-gray-800 py-3"></div>
                                        <div class="d-flex flex-row justify-content-between align-items-center p-5">
                                            <div class="fw-bold">Nom du Hub</div>
                                            <div data-ajax-load="nameHub">-</div>
                                        </div>
                                        <div class="separator border-1 border-gray-400 py-3"></div>
                                        <div class="d-flex flex-row justify-content-between align-items-center p-5">
                                            <div class="fw-bold">Prix <div class="fs-9 fs-italic">(Hors Subvention)</div></div>
                                            <div data-ajax-load="priceNoSub">-</div>
                                        </div>
                                        <div class="separator border-1 border-gray-400 py-3"></div>
                                        <div class="d-flex flex-row justify-content-between align-items-center p-5">
                                            <div class="fw-bold">Taxe de passage de ligne</div>
                                            <div data-ajax-load="taxe">-</div>
                                        </div>
                                        <div class="separator border-1 border-gray-400 py-3"></div>
                                        <div class="d-flex flex-row justify-content-around border border-1 border-gray-500">
                                            <div class="d-flex flex-column justify-content-center align-items-center m-5">
                                                <div class="fs-6 text-center">Nb Voyageur First Classe</div>
                                                <div class="fs-4 fw-bolder" data-ajax-load="passengerFirst">-</div>
                                            </div>
                                            <div class="d-flex flex-column justify-content-center align-items-center m-5">
                                                <div class="fs-6 text-center">Nb Voyageur Second Classe</div>
                                                <div class="fs-4 fw-bolder" data-ajax-load="passengerSecond">-</div>
                                            </div>
                                        </div>
                                        <div class="separator border-1 border-gray-400 py-3"></div>
                                        <div class="d-flex flex-row justify-content-around border border-1 border-gray-500">
                                            <div class="d-flex flex-column justify-content-center align-items-center m-5">
                                                <div class="fs-6 text-center">Commerce</div>
                                                <div class="fs-4 fw-bolder" data-ajax-load="nbCommerce">-</div>
                                            </div>
                                            <div class="d-flex flex-column justify-content-center align-items-center m-5">
                                                <div class="fs-6 text-center">Publicité</div>
                                                <div class="fs-4 fw-bolder" data-ajax-load="nbPublicite">-</div>
                                            </div>
                                            <div class="d-flex flex-column justify-content-center align-items-center m-5">
                                                <div class="fs-6 text-center">Parking</div>
                                                <div class="fs-4 fw-bolder" data-ajax-load="nbParking">-</div>
                                            </div>
                                        </div>
                                        <div class="separator border-1 border-gray-400 py-3"></div>
                                        <div class="d-flex flex-row justify-content-between align-items-center mt-5">
                                            <div class="fs-3">Subvention de l'état</div>
                                            <div class="fs-2 fw-bolder">100 %</div>
                                        </div>
                                        <div class="d-flex flex-row justify-content-between align-items-center">
                                            <div class="fs-3">Total à Payer</div>
                                            <div class="fs-1 fw-bolder text-success border border-3 border-success p-1">0,00 €</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-end">
                                <x-form.button />
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Wrapper-->

                </div>
                <!--end::Content-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Authentication - Sign-in-->
</div>
<!--end::Root-->
<!--begin::Javascript-->
<script>var hostUrl = "assets/";</script>
<!--begin::Global Javascript Bundle(mandatory for all pages)-->
<script src="assets/plugins/global/plugins.bundle.js"></script>
<script src="assets/js/scripts.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta3/dist/js/bootstrap-select.min.js"></script>
<script src="https://unpkg.com/react@17.0.2/umd/react.production.min.js"></script>
<script src="https://unpkg.com/react-dom@17.0.2/umd/react-dom.production.min.js"></script>
@vite(["resources/js/app.js"])
<!--end::Global Javascript Bundle-->
<script type="text/javascript">
    let followMarker = true;
    let coords = {};
    document.querySelector('#hub_id').addEventListener('change', e => {
        console.log(e.target.value)
        $.ajax({
            url: '/api/core/hub/'+e.target.value,
            success: data => {
                console.log(data)
                document.querySelector('[data-ajax-load="nameHub"]').innerHTML = data.hub.gare.name
                document.querySelector('[data-ajax-load="priceNoSub"]').innerHTML = data.hub.format_price
                document.querySelector('[data-ajax-load="taxe"]').innerHTML = data.hub.format_taxe + ' / Voyage'
                document.querySelector('[data-ajax-load="passengerFirst"]').innerHTML = data.hub.passenger_first
                document.querySelector('[data-ajax-load="passengerSecond"]').innerHTML = data.hub.passenger_second
                document.querySelector('[data-ajax-load="nbCommerce"]').innerHTML = new Intl.NumberFormat('fr-FR').format(data.hub.nb_slot_commerce)
                document.querySelector('[data-ajax-load="nbPublicite"]').innerHTML = new Intl.NumberFormat('fr-FR').format(data.hub.nb_slot_pub)
                document.querySelector('[data-ajax-load="nbParking"]').innerHTML = new Intl.NumberFormat('fr-FR').format(data.hub.nb_slot_parking)

                let container = L.DomUtil.get('map');
                if(container != null) {
                    container._leaflet_id = null;
                }
                let map = null;

                const latLng = {
                    lat: data.hub.gare.latitude,
                    lng: data.hub.gare.longitude
                }

                const zoomLevel = 17
                const accessToken = 'pk.eyJ1Ijoic2FtZnJvbWZyYW5jZSIsImEiOiJjazBoenNpM3owN2N5M2hxcG5vc2FsNm1mIn0.RqjLQuhXOEazL8PH7uKDbw';
                map = L.map('map').setView([latLng.lat, latLng.lng], zoomLevel);

                const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 19,
                    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                });

                mainLayer.addTo(map);
                addMarker({
                    lat: latLng.lat,
                    lng: latLng.lng,
                    title: data.hub.gare.name,
                    draggable: true
                }, map);
            }
        })
    })

    function addMarker(options, map) {
        const icon = L.icon({
            iconUrl: '/storage/icons/train_map.png',
            iconSize: [32,37],
            iconAnchor:   [16, 18], // point of the icon which will correspond to marker's location
            popupAnchor:  [1, -9]
        })
        const marker = L.marker([options.lat, options.lng], { title: options.title, draggable: options.draggable, icon: icon });
        marker.addTo(map);

        marker.on('dragend', function(event) {
            coords = event.target._latlng;
            showNewCoords(event.target._latlng, event.target);
            if (followMarker) {
                map.setView(event.target._latlng);
            }
        });
    }

    function showNewCoords(coords, marker) {
        marker
            .bindPopup(`lat: ${coords.lat} - lng: ${coords.lng}`)
            .openPopup();
    }
</script>
<!--end::Javascript-->
</body>
<!--end::Body-->
</html>
