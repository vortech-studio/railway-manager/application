<?php

namespace App\Traits;

use Carbon\Carbon;

trait UserEngineTrait
{
    /**
     * Calcule l'indice d'ancienneté en fonction de la date d'achat et de la date de la dernière visite curative.
     *
     * @param string $dateAchat La date d'achat au format 'Y-m-d'.
     * @param string $dateDerniereVisiteCurative La date de la dernière visite curative au format 'Y-m-d'.
     * @return float L'indice d'ancienneté, compris entre 0 et 5.
     */
    public static function calculerIndiceAnciennete($dateAchat, $dateDerniereVisiteCurative = null)
    {
        // Convertir les dates en objets Carbon pour faciliter le calcul
        $dateAchat = Carbon::parse($dateAchat);

        // Si la date de la dernière visite curative n'est pas fournie, utiliser la date actuelle
        if ($dateDerniereVisiteCurative === null) {
            $dateDerniereVisiteCurative = Carbon::now();
        } else {
            $dateDerniereVisiteCurative = Carbon::parse($dateDerniereVisiteCurative);
        }

        // Calculer la durée totale de vie de l'engin en années à partir de la date d'achat jusqu'à la dernière visite curative
        $dureeVieTotale = max(1, $dateDerniereVisiteCurative->diffInYears($dateAchat)); // Utilisation de max(1, ...) pour éviter une division par zéro

        // Calculer la base en divisant la durée totale par 4
        $base = max(1, $dureeVieTotale / 4); // Utilisation de max(1, ...) pour éviter une division par zéro

        // Calculer la durée écoulée depuis la dernière visite curative en années
        $dureeEcouléeDepuisVisiteCurative = $dateDerniereVisiteCurative->diffInYears(Carbon::now());

        // Calculer l'indice d'ancienneté en inversant la division pour s'approcher de 5 lorsque le matériel est neuf
        $indiceAnciennete = 5 - ($dureeEcouléeDepuisVisiteCurative / $base);

        // Limiter l'indice d'ancienneté à une échelle de 0 à 5
        $indiceAnciennete = max(0, min(5, $indiceAnciennete));

        return $indiceAnciennete;
    }

    public static function calculerUsureTotale($tauxUsureInitial, $kilometrageParcouru, $limiteKm)
    {
        // Calculer l'usure totale en fonction du taux d'usure initial et du kilométrage parcouru
        $usureTotale = $tauxUsureInitial * ($kilometrageParcouru / 100);

        // Limiter l'usure totale à la limite spécifiée
        $usureTotale = min($usureTotale, $limiteKm);

        return $usureTotale;
    }


}
