<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Select extends Component
{
    /**
     * Create a new component instance.
     *
     * @param $name
     * @param array $datas
     * @param string $label
     * @param string|null $placeholder
     * @param bool $required
     * @param string|int|null $value
     * @param string|null $name_function
     * @param string|null $value_function
     */
    public function __construct(
        public string          $name,
        public array           $datas,
        public string          $label,
        public ?string         $placeholder = null,
        public bool            $required = false,
        public string|int|null $value = null,
        public ?string         $class = "form-control selectpicker"

    )
    {

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.select');
    }
}
