<?php

namespace App\Observer;

use App\Models\User;

class UserObserver
{
    public function created(User $user): void
    {
        User\UserTuto::create([
            "user_id" => $user->id
        ]);

        User\UserSetting::create([
            "user_id" => $user->id
        ]);
    }

    public function updated(User $user): void
    {
    }

    public function deleted(User $user): void
    {
    }

    public function restored(User $user): void
    {
    }

    public function forceDeleted(User $user): void
    {
    }
}
