<?php

namespace App\Imports;

use App\Models\Core\GareTransportation;
use Maatwebsite\Excel\Concerns\ToModel;

class GareTransportationImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new GareTransportation([
            "id" => $row["id"],
            "type" => $row["type"],
            "gares_id" => $row["gares_id"],
            "created_at" => $row["created_at"],
            "updated_at" => $row["updated_at"],
        ]);
    }
}
