@extends("game.template")

@section("css")
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/mouvement/achat_materiel.png') }}" alt="">
            </div>
            <x-base.underline
                title="Achat d'un matériel roulant"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.engine.partials.menu")
    </div>
    <div class="card shadow-lg bg-gray-300 mb-10">
        <div class="card-body">
            <ul id="filters" class="nav d-flex flex-center justify-content-center">
                <button class="btn btn-flex btn-outline btn-outline-primary active h-70px me-3 nav-link" type="button" data-bs-toggle="tab" data-bs-target="#all" role="tab" aria-controls="home" aria-selected="true">
                        <span class="d-flex flex-column justify-content-center align-items-center ">
                            TOUS
                        </span>
                </button>
                <button class="btn btn-flex btn-outline btn-outline-primary me-3" type="button" data-bs-toggle="tab" data-bs-target="#ter" role="tab">
                    <span class="d-flex flex-column justify-content-center align-items-center ">
                        <span class="symbol symbol-50">
                            <img src="{{ asset('/storage/logos/logo_ter.svg') }}" alt="">
                        </span>
                    </span>
                </button>
                <button class="btn btn-flex btn-outline btn-outline-primary me-3" type="button" data-bs-toggle="tab" data-bs-target="#tgv" role="tab">
                    <span class="d-flex flex-column justify-content-center align-items-center ">
                        <span class="symbol symbol-50">
                            <img src="{{ asset('/storage/logos/logo_tgv.svg') }}" alt="">
                        </span>
                    </span>
                </button>
                <button class="btn btn-flex btn-outline btn-outline-primary me-3" type="button" data-bs-toggle="tab" data-bs-target="#ic" role="tab">
                    <span class="d-flex flex-column justify-content-center align-items-center ">
                        <span class="symbol symbol-50">
                            <img src="{{ asset('/storage/logos/logo_intercite.svg') }}" alt="">
                        </span>
                    </span>
                </button>
                <button class="btn btn-flex btn-outline btn-outline-primary me-3 h-70px" type="button" data-bs-toggle="tab" data-bs-target="#other" role="tab">
                    <span class="d-flex flex-column justify-content-center align-items-center ">
                        Autres
                    </span>
                </button>
            </ul>
        </div>
    </div>
    <div class="card bg-gray-300 mb-10">
        <div class="card-body">
            <form id="formCheckoutEngine" class="d-flex flex-column tab-content" method="post">
                @csrf
                <div class="tab-pane fade show active" id="all" role="tabpanel">
                    @foreach(\App\Models\Core\Engine::where('in_game', true)->orderBy('price_achat')->get() as $engine)
                        <div class="d-flex flex-column bg-gray-500 rounded-3 p-5 mb-5">
                            <div class="d-flex flex-row border-bottom border-bottom-gray-900 justify-content-between align-items-center pb-5">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="symbol symbol-30px me-3">
                                        <img alt="Logo" src="{{ asset('/storage/logos/logo_'.$engine->type_train.'.svg') }}"/>
                                    </div>
                                    <div class="fw-semibold fs-3 text-white name">{{ $engine->name }}</div>
                                </div>
                                @if($engine->in_shop)
                                    <div class="symbol symbol-30px" data-bs-toggle="tooltip" title="Ce matériel roulant vous coutera {{ $engine->price_shop }} Tpoint supplémentaires">
                                        <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                                    </div>
                                @endif
                            </div>
                            <div class="d-flex flex-row align-items-center p-10 text-white">
                                <div class="me-4">
                                    <img alt="Logo" src="{{ $engine->image_url }}" class="w-120px"/>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center w-75">
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Informations</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Mise en service: <span class="fw-bold">{{ $engine->technical->start_exploi->year }}</span></span>
                                            <span class="me-2">Place: <span class="fw-bold">{{ $engine->nb_passager ?? "Aucun" }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Rayon d'action: <span class="fw-bold">{{ round($engine->calcMaxRuntimeEngine(), 2) }} Km</span></span>
                                            <span class="me-2">Vitesse: <span class="fw-bold">{{ $engine->velocity }} Km/h</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Vitesse d'usure: <span class="fw-bold">{{ $engine->calcVitesseUsure() }} % / 100h</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Prix</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Brut: <span class="fw-bold">{{ eur($engine->price_achat) }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Subvention ({{ auth()->user()->company->subvention }}%): <span class="fw-bold">{{ eur($engine->price_achat * auth()->user()->company->subvention / 100) }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Total: <span class="fw-bold text-color-eva">{{ eur($engine->price_achat - ($engine->price_achat * auth()->user()->company->subvention / 100)) }}</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column w-150px">
                                        <div class="d-flex flex-row justify-content-between align-items-center">
                                            <div class="mb-10 me-3">
                                                <label for="qte" class="control-label ">Quantité</label>
                                                <input type="text" name="qte" value="1" class="form-control">
                                            </div>
                                            <button type="button" class="btn btn-sm btn-primary" data-action="checkout" data-engine="{{ $engine->id }}"><i class="fa-solid fa-shopping-cart"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="engine_id" value="{{ $engine->id }}">
                    @endforeach
                </div>
                <div class="tab-pane fade" id="ter" role="tabpanel">
                    @foreach(\App\Models\Core\Engine::where('in_game', true)->where('type_train', 'ter')->orderBy('price_achat')->get() as $engine)
                        <div class="d-flex flex-column bg-gray-500 rounded-3 p-5 mb-5">
                            <div class="d-flex flex-row border-bottom border-bottom-gray-900 justify-content-between align-items-center pb-5">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="symbol symbol-30px me-3">
                                        <img alt="Logo" src="{{ asset('/storage/logos/logo_'.$engine->type_train.'.svg') }}"/>
                                    </div>
                                    <div class="fw-semibold fs-3 text-white name">{{ $engine->name }}</div>
                                </div>
                                @if($engine->in_shop)
                                    <div class="symbol symbol-30px" data-bs-toggle="tooltip" title="Ce matériel roulant vous coutera {{ $engine->price_shop }} Tpoint supplémentaires">
                                        <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                                    </div>
                                @endif
                            </div>
                            <div class="d-flex flex-row align-items-center p-10 text-white">
                                <div class="me-4">
                                    <img alt="Logo" src="{{ $engine->image_url }}" class="w-120px"/>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center w-75">
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Informations</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Mise en service: <span class="fw-bold">{{ $engine->technical->start_exploi->year }}</span></span>
                                            <span class="me-2">Place: <span class="fw-bold">{{ $engine->nb_passager ?? "Aucun" }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Rayon d'action: <span class="fw-bold">{{ round($engine->calcMaxRuntimeEngine(), 2) }} Km</span></span>
                                            <span class="me-2">Vitesse: <span class="fw-bold">{{ $engine->velocity }} Km/h</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Vitesse d'usure: <span class="fw-bold">{{ $engine->calcVitesseUsure() }} % / 100h</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Prix</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Brut: <span class="fw-bold">{{ eur($engine->price_achat) }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Subvention ({{ auth()->user()->company->subvention }}%): <span class="fw-bold">{{ eur($engine->price_achat * auth()->user()->company->subvention / 100) }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Total: <span class="fw-bold text-color-eva">{{ eur($engine->price_achat - ($engine->price_achat * auth()->user()->company->subvention / 100)) }}</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column w-150px">
                                        <div class="d-flex flex-row justify-content-between align-items-center">
                                            <div class="mb-10 me-3">
                                                <label for="qte" class="control-label ">Quantité</label>
                                                <input type="text" name="qte" value="1" class="form-control">
                                            </div>
                                            <button type="button" class="btn btn-sm btn-primary" data-action="checkout" data-engine="{{ $engine->id }}"><i class="fa-solid fa-shopping-cart"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="engine_id" value="{{ $engine->id }}">
                    @endforeach
                </div>
                <div class="tab-pane fade" id="tgv" role="tabpanel">
                    @foreach(\App\Models\Core\Engine::where('in_game', true)->where('type_train', 'tgv')->orderBy('price_achat')->get() as $engine)
                        <div class="d-flex flex-column bg-gray-500 rounded-3 p-5 mb-5">
                            <div class="d-flex flex-row border-bottom border-bottom-gray-900 justify-content-between align-items-center pb-5">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="symbol symbol-30px me-3">
                                        <img alt="Logo" src="{{ asset('/storage/logos/logo_'.$engine->type_train.'.svg') }}"/>
                                    </div>
                                    <div class="fw-semibold fs-3 text-white name">{{ $engine->name }}</div>
                                </div>
                                @if($engine->in_shop)
                                    <div class="symbol symbol-30px" data-bs-toggle="tooltip" title="Ce matériel roulant vous coutera {{ $engine->price_shop }} Tpoint supplémentaires">
                                        <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                                    </div>
                                @endif
                            </div>
                            <div class="d-flex flex-row align-items-center p-10 text-white">
                                <div class="me-4">
                                    <img alt="Logo" src="{{ $engine->image_url }}" class="w-120px"/>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center w-75">
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Informations</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Mise en service: <span class="fw-bold">{{ $engine->technical->start_exploi->year }}</span></span>
                                            <span class="me-2">Place: <span class="fw-bold">{{ $engine->nb_passager ?? "Aucun" }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Rayon d'action: <span class="fw-bold">{{ round($engine->calcMaxRuntimeEngine(), 2) }} Km</span></span>
                                            <span class="me-2">Vitesse: <span class="fw-bold">{{ $engine->velocity }} Km/h</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Vitesse d'usure: <span class="fw-bold">{{ $engine->calcVitesseUsure() }} % / 100h</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Prix</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Brut: <span class="fw-bold">{{ eur($engine->price_achat) }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Subvention ({{ auth()->user()->company->subvention }}%): <span class="fw-bold">{{ eur($engine->price_achat * auth()->user()->company->subvention / 100) }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Total: <span class="fw-bold text-color-eva">{{ eur($engine->price_achat - ($engine->price_achat * auth()->user()->company->subvention / 100)) }}</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column w-150px">
                                        <div class="d-flex flex-row justify-content-between align-items-center">
                                            <div class="mb-10 me-3">
                                                <label for="qte" class="control-label ">Quantité</label>
                                                <input type="text" name="qte" value="1" class="form-control">
                                            </div>
                                            <button type="button" class="btn btn-sm btn-primary" data-action="checkout" data-engine="{{ $engine->id }}"><i class="fa-solid fa-shopping-cart"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="engine_id" value="{{ $engine->id }}">
                    @endforeach
                </div>
                <div class="tab-pane fade" id="ic" role="tabpanel">
                    @foreach(\App\Models\Core\Engine::where('in_game', true)->where('type_train', 'ic')->orderBy('price_achat')->get() as $engine)
                        <div class="d-flex flex-column bg-gray-500 rounded-3 p-5 mb-5">
                            <div class="d-flex flex-row border-bottom border-bottom-gray-900 justify-content-between align-items-center pb-5">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="symbol symbol-30px me-3">
                                        <img alt="Logo" src="{{ asset('/storage/logos/logo_'.$engine->type_train.'.svg') }}"/>
                                    </div>
                                    <div class="fw-semibold fs-3 text-white name">{{ $engine->name }}</div>
                                </div>
                                @if($engine->in_shop)
                                    <div class="symbol symbol-30px" data-bs-toggle="tooltip" title="Ce matériel roulant vous coutera {{ $engine->price_shop }} Tpoint supplémentaires">
                                        <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                                    </div>
                                @endif
                            </div>
                            <div class="d-flex flex-row align-items-center p-10 text-white">
                                <div class="me-4">
                                    <img alt="Logo" src="{{ $engine->image_url }}" class="w-120px"/>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center w-75">
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Informations</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Mise en service: <span class="fw-bold">{{ $engine->technical->start_exploi->year }}</span></span>
                                            <span class="me-2">Place: <span class="fw-bold">{{ $engine->nb_passager ?? "Aucun" }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Rayon d'action: <span class="fw-bold">{{ round($engine->calcMaxRuntimeEngine(), 2) }} Km</span></span>
                                            <span class="me-2">Vitesse: <span class="fw-bold">{{ $engine->velocity }} Km/h</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Vitesse d'usure: <span class="fw-bold">{{ $engine->calcVitesseUsure() }} % / 100h</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Prix</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Brut: <span class="fw-bold">{{ eur($engine->price_achat) }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Subvention ({{ auth()->user()->company->subvention }}%): <span class="fw-bold">{{ eur($engine->price_achat * auth()->user()->company->subvention / 100) }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Total: <span class="fw-bold text-color-eva">{{ eur($engine->price_achat - ($engine->price_achat * auth()->user()->company->subvention / 100)) }}</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column w-150px">
                                        <div class="d-flex flex-row justify-content-between align-items-center">
                                            <div class="mb-10 me-3">
                                                <label for="qte" class="control-label ">Quantité</label>
                                                <input type="text" name="qte" value="1" class="form-control">
                                            </div>
                                            <button type="button" class="btn btn-sm btn-primary" data-action="checkout" data-engine="{{ $engine->id }}"><i class="fa-solid fa-shopping-cart"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="engine_id" value="{{ $engine->id }}">
                    @endforeach
                </div>
                <div class="tab-pane fade" id="other" role="tabpanel">
                    @foreach(\App\Models\Core\Engine::where('in_game', true)->where('type_train', 'other')->orderBy('price_achat')->get() as $engine)
                        <div class="d-flex flex-column bg-gray-500 rounded-3 p-5 mb-5">
                            <div class="d-flex flex-row border-bottom border-bottom-gray-900 justify-content-between align-items-center pb-5">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="symbol symbol-30px me-3">
                                        <img alt="Logo" src="{{ asset('/storage/logos/logo_'.$engine->type_train.'.svg') }}"/>
                                    </div>
                                    <div class="fw-semibold fs-3 text-white name">{{ $engine->name }}</div>
                                </div>
                                @if($engine->in_shop)
                                    <div class="symbol symbol-30px" data-bs-toggle="tooltip" title="Ce matériel roulant vous coutera {{ $engine->price_shop }} Tpoint supplémentaires">
                                        <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                                    </div>
                                @endif
                            </div>
                            <div class="d-flex flex-row align-items-center p-10 text-white">
                                <div class="me-4">
                                    <img alt="Logo" src="{{ $engine->image_url }}" class="w-120px"/>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center w-75">
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Informations</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Mise en service: <span class="fw-bold">{{ $engine->technical->start_exploi->year }}</span></span>
                                            <span class="me-2">Place: <span class="fw-bold">{{ $engine->nb_passager ?? "Aucun" }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Rayon d'action: <span class="fw-bold">{{ round($engine->calcMaxRuntimeEngine(), 2) }} Km</span></span>
                                            <span class="me-2">Vitesse: <span class="fw-bold">{{ $engine->velocity }} Km/h</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Vitesse d'usure: <span class="fw-bold">{{ $engine->calcVitesseUsure() }} % / 100h</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Prix</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Brut: <span class="fw-bold">{{ eur($engine->price_achat) }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Subvention ({{ auth()->user()->company->subvention }}%): <span class="fw-bold">{{ eur($engine->price_achat * auth()->user()->company->subvention / 100) }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Total: <span class="fw-bold text-color-eva">{{ eur($engine->price_achat - ($engine->price_achat * auth()->user()->company->subvention / 100)) }}</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column w-150px">
                                        <div class="d-flex flex-row justify-content-between align-items-center">
                                            <div class="mb-10 me-3">
                                                <label for="qte" class="control-label ">Quantité</label>
                                                <input type="text" name="qte" value="1" class="form-control">
                                            </div>
                                            <button type="button" class="btn btn-sm btn-primary" data-action="checkout" data-engine="{{ $engine->id }}"><i class="fa-solid fa-shopping-cart"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="engine_id" value="{{ $engine->id }}">
                    @endforeach
                </div>

            </form>
        </div>
    </div>
@endsection

@section("script")
    <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
    <script type="text/javascript">
        document.querySelectorAll('[data-action="checkout"]').forEach(btn => {
            btn.addEventListener('click',e => {
                e.preventDefault()


                $.ajax({
                    url: '{{ route('engine.buy.config') }}',
                    method: 'GET',
                    data: {"engine_id": btn.dataset.engine, "qte": e.target.parentNode.parentNode.querySelector('input').value},
                    success: data => {
                        window.location.href = data
                    }
                })
            })
        })
    </script>
@endsection
