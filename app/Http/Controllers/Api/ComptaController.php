<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\MouvementTrait;
use Illuminate\Http\Request;

class ComptaController extends Controller
{
    public function getChartInfo(Request $request, $user_id)
    {
        return match ($request->get('type')) {
            "resultat" => $this->getChartResultat($user_id),
            "ca" => $this->getChartCA($user_id),
            "incident" => $this->getChartIncident($user_id),
        };
    }

    public function subvention(Request $request, $user_id)
    {
        return response()->json(["subvention" => User::find($user_id)->company->subvention]);
    }

    private function getChartResultat($user_id)
    {
        $labels = [
            "Sem. ".now()->week,
            "Sem. ".now()->subWeek()->week,
            "Sem. ".now()->subWeeks(2)->week,
            "Sem. ".now()->subWeeks(3)->week,
            "Sem. ".now()->subWeeks(4)->week,
        ];

        $data = [
            eur(MouvementTrait::getResultatTravelFromWeek(now())),
            eur(MouvementTrait::getResultatTravelFromWeek(now()->subWeek())),
            eur(MouvementTrait::getResultatTravelFromWeek(now()->subWeeks(2))),
            eur(MouvementTrait::getResultatTravelFromWeek(now()->subWeeks(3))),
            eur(MouvementTrait::getResultatTravelFromWeek(now()->subWeeks(4))),
        ];

        return response()->json([
            "labels" => $labels,
            "data" => $data
        ]);

    }

    private function getChartCA($user_id)
    {
        $labels = [
            "Sem. ".now()->week,
            "Sem. ".now()->subWeek()->week,
            "Sem. ".now()->subWeeks(2)->week,
            "Sem. ".now()->subWeeks(3)->week,
            "Sem. ".now()->subWeeks(4)->week,
        ];

        $data = [
            eur(MouvementTrait::getCAFromWeek(now())),
            eur(MouvementTrait::getCAFromWeek(now()->subWeek())),
            eur(MouvementTrait::getCAFromWeek(now()->subWeeks(2))),
            eur(MouvementTrait::getCAFromWeek(now()->subWeeks(3))),
            eur(MouvementTrait::getCAFromWeek(now()->subWeeks(4))),
        ];

        return response()->json([
            "labels" => $labels,
            "data" => $data
        ]);
    }

    private function getChartIncident($user_id)
    {
        $incidents = collect();
        $incidents->push(["Day", "Incidents"]);

        $labels = collect();
        $data = collect();

        for ($i = 0; $i <= 30; $i++) {
            $labels->push([now()->subDays($i -30)->format("d/m/Y")]);
        }

        for ($i = 0; $i <= 30; $i++) {
            $data->push([User\UserIncident::where('user_id', $user_id)->whereBetween('created_at', [now()->subDays($i -30)->startOfDay(), now()->endOfDay()])->count()]);
        }


        return response()->json([
            "labels" => $labels->toArray(),
            "data" => $data->toArray()
        ]);
    }
}
