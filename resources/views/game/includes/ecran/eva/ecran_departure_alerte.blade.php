@mobile

@else
    <div class="d-flex flex-column bg-red-600 border border-5 border-red-400 rounded-3 shadow-lg h-400px">
        <div class="bg-red-800 h-30px bg-striped">&nbsp;</div>
        <div class="d-flex flex-row justify-content-center align-items-center h-100 gx-5">
            <img src="{{ asset('/storage/icons/passenger_forbiden.png') }}" class="w-250px animation-blink animation-blink-2 me-20" alt="">
            <div class="d-flex flex-column flex-shrink-1 w-145px">
                <div class="fs-2hx fw-bolder text-white fs-uppercase">Départs en cours</div>
                <div class="fs-2x text-white">Pour votre sécurité, merci de vous éloigner de la bordure du quai.</div>
            </div>
        </div>
    </div>
@endmobile
