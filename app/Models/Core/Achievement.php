<?php

namespace App\Models\Core;

class Achievement extends \LevelUp\Experience\Models\Achievement
{
    protected $appends = ['is_secret_icon'];
    public function rewards()
    {
        return $this->hasMany(Reward::class);
    }

    public function getIsSecretIconAttribute()
    {
        if($this->is_secret) {
            return "<i class='fas fa-eye-slash text-danger fs-1'></i>";
        } else {
            return "<i class='fas fa-eye text-success fs-1'></i>";
        }
    }
}
