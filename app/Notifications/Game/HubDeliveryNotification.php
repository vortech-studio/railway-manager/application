<?php

namespace App\Notifications\Game;

use App\Models\User\UserHub;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;

class HubDeliveryNotification extends Notification
{
    use Queueable;
    public function __construct(public UserHub $hub)
    {
    }

    public function via($notifiable): array
    {
        return ['database', 'broadcast', WebPushChannel::class];
    }

    public function toDatabase($notifiable): array
    {
        return [
            'type' => 'success',
            'icon' => 'ki-delivery-time',
            'title' => 'Nouvelle livraison',
            'description' => "Le hub ".$this->hub->hub->gare->name." est maintenant actif",
            'time' => now(),
            'sector' => 'delivery'
        ];
    }

    public function toArray($notifiable): array
    {
        return [
            'type' => 'success',
            'icon' => 'ki-delivery-time',
            'title' => 'Nouvelle livraison',
            'description' => "Le hub ".$this->hub->hub->gare->name." est maintenant actif",
            'time' => now(),
            'sector' => 'delivery'
        ];
    }

    public function toBroadcast($notifiable): BroadcastMessage
    {
        return new BroadcastMessage([
            'type' => 'success',
            'icon' => 'ki-delivery-time',
            'title' => 'Nouvelle livraison',
            'description' => "Le hub ".$this->hub->hub->gare->name." est maintenant actif",
            'time' => now(),
            "user_id" => $this->hub->user_id,
            'sector' => 'delivery'
        ]);
    }

    public function broadcastOn()
    {
        return ['delivery-channel'];
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new \NotificationChannels\WebPush\WebPushMessage)
            ->title('Nouvelle livraison')
            ->icon(asset('/storage/icons/fast-delivery.png'))
            ->body("Le hub ".$this->hub->hub->gare->name." est maintenant actif")
            ->action('Voir', route('hub.show', $this->hub->hub_id));
    }
}
