<?php

namespace App\Http\Controllers\Game\Vigimat;

use App\Http\Controllers\Controller;
use App\Models\User\UserEngine;
use App\Services\Osmose\Engine;

class EngineController extends Controller
{
    public function index()
    {
        return view('vigimat.engine.index');
    }

    public function show($engine_id)
    {
        $osmose = new Engine();
        //dd($osmose->retrieve($engine_id));
        return view('vigimat.engine.show', [
            "engine" => UserEngine::with('engine', 'user_hub', 'plannings', 'user_ligne', 'incidents', 'tasks', 'livraisons')->find($engine_id),
            "osmose_engine" => $osmose->retrieve($engine_id)
        ]);
    }
}
