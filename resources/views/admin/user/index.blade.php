@extends("admin.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Gestion des utilisateurs",
        'action' => true,
        'data' => [
            [
                'url' => route('admin.user.create'),
                'title' => "Ajouter un nouveau joueur",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="card card-flush">
        <!--begin::Card header-->
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                    <input type="text" data-user-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Rechercher un joueur" />
                </div>
                <!--end::Search-->
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body pt-0">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_joueur">
                <thead>
                <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                    <th class="">Pseudo & Infos</th>
                    <th class="">Statistiques Financières</th>
                    <th class="">Statistique Matériels</th>
                    <th class="">Actions</th>
                </tr>
                </thead>
                <tbody class="fw-semibold text-gray-600">
                @foreach(\App\Models\User::where('admin', 0)->get() as $user)
                <tr>
                    <td>
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-50px symbol-2by3 me-5">
                                <img src="/storage/avatar/users/{{ $user->avatar }}" alt="Avatar" />
                                @if($user->premium)
                                <span class="symbol-badge badge badge-circle bg-body start-100" data-bs-toggle="tooltip" title="Premium">
                                    <img src="/storage/icons/premium.png" alt="" class="w-15px">
                                </span>
                                @endif
                            </div>
                            <div class="d-flex flex-column">
                                <a href="#" class="text-gray-800 text-hover-primary mb-1">{{ $user->name }}</a>
                                <span>{{ $user->email }}</span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex flex-column">
                            <div class="d-flex flex-row align-items-center mb-5">
                                <img src="/storage/icons/euro.png" alt="" class="w-25px me-3" data-bs-toggle="tooltip" title="Argent">
                                <span class="text-gray-800 text-hover-primary mb-1"> {{ eur($user->argent) }}</span>
                            </div>
                            <div class="d-flex flex-row align-items-center mb-5">
                                <img src="/storage/icons/tpoint.png" alt="" class="w-25px me-3" data-bs-toggle="tooltip" title="T Point">
                                <span class="text-gray-800 text-hover-primary mb-1"> {{ eur($user->tpoint) }}</span>
                            </div>
                            <div class="d-flex flex-row align-items-center mb-5">
                                <img src="/storage/icons/research.png" alt="" class="w-25px me-3" data-bs-toggle="tooltip" title="Alloué à la recherche">
                                <span class="text-gray-800 text-hover-primary mb-1"> {{ eur($user->research) }}</span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="d-flex flex-row rounded-3 shadow-lg bg-body align-items-center p-5">
                            <div class="d-flex flex-column justify-content-center align-items-center me-5">
                                <div class="symbol symbol-50px">
                                    <img src="/storage/icons/hub.png" alt="" class="w-40px">
                                </div>
                                <div class="text-gray-800">Nombre de Hub</div>
                                <div class="fw-bolder fs-1">{{ $user->hubs()->count() }}</div>
                            </div>
                            <div class="d-flex flex-column justify-content-center align-items-center">
                                <div class="symbol symbol-50px">
                                    <img src="/storage/icons/train.png" alt="" class="w-40px">
                                </div>
                                <div class="text-gray-800">Nombre de matériels roulants</div>
                                <div class="fw-bolder fs-1">{{ $user->engines()->count() }}</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <a href="{{ route('admin.user.show', $user->id) }}" class="btn btn-sm btn-icon btn-info me-2"><i class="fa-solid fa-eye"></i> </a>
                        <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-sm btn-icon btn-primary me-2"><i class="fa-solid fa-edit"></i> </a>
                        <a href="#" data-action="delete" data-id="{{ $user->id }}" class="btn btn-sm btn-icon btn-danger me-2"><i class="fa-solid fa-trash"></i> </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let table = $('#liste_joueur').DataTable({
            info: !1,
            order: [],
            pageLength: 10,
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
        });

        document.querySelector('[data-user-filter="search"]').addEventListener('keyup', function (e) {
            table.search(e.target.value).draw();
        });

    </script>
@endsection
