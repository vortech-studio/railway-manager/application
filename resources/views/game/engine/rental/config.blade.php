@extends("game.template")

@section("css")
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/train_rent.png') }}" alt="">
            </div>
            <x-base.underline
                    title="Location d'un matériel roulant"
                    size="3"
                    size-text="fs-1" />
        </div>
        @include("game.engine.partials.menu")
    </div>
    <form action="{{ route('engine.rental.checkout') }}" method="post">
        @csrf
        <input type="hidden" name="engine_id" value="{{ $engine->id }}">
        <input type="hidden" name="rental_id" value="{{ $rental->id }}">
        <div class="card shadow-lg bg-gray-500 text-white">
            <div class="card-body">
                <x-base.underline
                        title="Configuration du matériels roulant"
                        size="1"
                        size-text="fs-3" />

                <div class="row">
                    <div class="col-9">
                        <div class="d-flex flex-column bg-gray-700 rounded-3 p-5 mb-5">
                            <div class="d-flex flex-row border-bottom border-bottom-gray-900 justify-content-between align-items-center pb-5">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="symbol symbol-30px me-3">
                                        <img alt="Logo" src="{{ asset('/storage/logos/logo_'.$engine->type_train.'.svg') }}"/>
                                    </div>
                                    <div class="fw-semibold fs-3 text-white name">{{ $engine->name }}</div>
                                </div>
                            </div>
                            <div class="d-flex flex-row align-items-center p-10 text-white">
                                <div class="me-4">
                                    <img alt="Logo" src="{{ $engine->image_url }}" class="w-120px"/>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center w-75">
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Informations</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Mise en service: <span class="fw-bold">{{ $engine->technical->start_exploi->year }}</span></span>
                                            <span class="me-2">Place: <span class="fw-bold">{{ $engine->nb_passager ?? "Aucun" }}</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Rayon d'action: <span class="fw-bold">{{ round($engine->calcMaxRuntimeEngine(), 2) }} Km</span></span>
                                            <span class="me-2">Vitesse: <span class="fw-bold">{{ $engine->velocity }} Km/h</span></span>
                                        </div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Vitesse d'usure: <span class="fw-bold">{{ $engine->calcVitesseUsure() }} % / 100h</span></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder fs-2">Prix</div>
                                        <div class="d-flex flex-row mb-1">
                                            <span class="me-2">Prix Brut: <span class="fw-bold">{{ eur($engine->price_location) }} / par semaine</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-10">
                            <label for="contract_duration" class="control-label required">Durée de la location</label>
                            <select name="contract_duration" id="contract_duration" class="form-control form-control-solid selectpicker w-25">
                                @for($i = 1; $i <= $rental->contract_duration; $i++)
                                    <option value="{{ $i }}">{{ $i }} semaines</option>
                                @endfor
                            </select>
                        </div>
                        <div class="mb-10">
                            <label for="user_hub_id" class="control-label required">Hub de livraison</label>
                            <select name="user_hub_id" id="user_hub_id" class="form-control form-control-solid selectpicker w-25" data-placeholder="--- Selectionner un hub ---">
                                @foreach(\App\Models\User\UserHub::where('active', true)->get() as $hub)
                                    <option value="{{ $hub->id }}">{{ $hub->hub->gare->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-row align-items-center m-2">
                            <span class="me-2">Prélèvement Hebdomadaires:</span>
                            <span class="fw-semibold">{{ eur($engine->price_location) }}</span>
                        </div>
                        <div class="d-flex flex-row align-items-center m-2">
                            <span class="me-2">Caution:</span>
                            <span class="fw-semibold">{{ eur($engine->getRentalCaution()) }}</span>
                        </div>
                        <div class="d-flex flex-row align-items-center m-2">
                            <span class="me-2">Total des frais:</span>
                            <span class="fw-semibold total" data-content="total">{{ eur($engine->price_location + $engine->getRentalCaution() + $engine->getRentalFrais()) }}</span>
                        </div>
                        <x-base.alert
                            type="solid"
                            color="info"
                            icon="file-circle-check"
                            title="Informations sur le contrat"
                            content="Inclus dans le contrat: Le coût d'une maintenance préventive sera déduit de la caution lors de la restitution de l'appareil" />

                        <div class="d-flex flex-wrap w-100 justify-content-center">
                            <button type="submit" class="btn btn-sm btn-primary rounded-4 me-3">Valider</button>
                            <a href="{{ route('engine.rental.index') }}" class="btn btn-sm btn-danger rounded-4">Annuler</a>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="text-center fw-semibold fs-3 mb-2">Tête de train</div>
                        <div class="border border-white h-600px rounded-top-5 p-8 scroll">
                            <div class="d-flex flex-row max-w-250px justify-content-around align-items-center">
                                @if($engine->type_train == 'ter')
                                    <div class="row justify-content-center">
                                        <div class="fw-bold fs-1">Classe Unique</div>
                                        @for($i = 0; $i < $engine->getComposition()[0]; $i++)
                                            <div class="col-4 rounded bg-blue-500 w-30px h-30px me-5 mb-2">&nbsp;</div>
                                        @endfor
                                    </div>
                                @else
                                    <div class="row justify-content-center">
                                        <div class="fw-bold fs-1">1ere Classe ({{ $engine->getComposition()[0] }})</div>
                                        @for($i = 0; $i < $engine->getComposition()[0]; $i++)
                                            <div class="col-1 rounded bg-red-500 w-15px h-15px me-5 mb-2">&nbsp;</div>
                                        @endfor
                                        <div class="fw-bold fs-1">2eme Classe ({{ $engine->getComposition()[1] }})</div>
                                        @for($i = 0; $i < $engine->getComposition()[1]; $i++)
                                            <div class="col-1 rounded bg-blue-500 w-15px h-15px me-5 mb-2">&nbsp;</div>
                                        @endfor
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script type="text/javascript">
        document.querySelector('#contract_duration').addEventListener('change', e => {
            e.preventDefault()
            let calc = {{ $engine->price_location + $engine->getRentalCaution() }} + ({{ $engine->getRentalFrais() }} * e.target.value)
            document.querySelector('.total').textContent = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(calc)
        });
    </script>
@endsection
