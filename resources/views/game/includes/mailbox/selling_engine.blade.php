Cher PDG,<br>
<br>
<p>
    Je tiens à vous informer que la vente du matériel roulant {{ Str::upper($engine->engine->name) }} pour un montant de {{ eur($amount) }} a été confirmée.
    Je vous remercie pour votre vente et je tiens à vous confirmer que le paiement a déjà été effectué sur votre compte avec succès.
</p>
<p>
    En tant que responsable du service des ventes du matériel ferroviaire sur votre secteur, je m'engage à fournir un service de qualité et à veiller à votre entière satisfaction tout au long de votre expérience lors de la vente de votre matériel roulant.<br>
    Je vous remercie pour votre confiance et je reste à votre disposition pour toute information complémentaire.
</p>
<p>
    Cordialement,<br>
    <strong>{{ $name_responsable }}</strong><br>
    <i>Responsable des ventes des lignes ferroviaires</i><br>
    <b>{{ \App\Models\User\Mailbox::generateFrom('vente_engine', $engine) }}</b>
</p>
