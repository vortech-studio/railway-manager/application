<?php

namespace App\Jobs\Game\Maintenance;

use App\Events\RepairEngineEvent;
use App\Models\Core\Mouvement;
use App\Models\User\UserEngine;
use App\Models\User\UserTechnicentreStaf;
use App\Models\User\UserTechnicentreTask;
use App\Notifications\Game\SendMessageNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class EndMaintenanceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserTechnicentreTask $task)
    {
    }

    public function handle(): void
    {
        \Log::debug($this->job->getJobId());
        $this->task->update([
            "status" => "finished",
        ]);

        UserTechnicentreStaf::find($this->task->user_technicentre_staf_id)->update([
            "status" => "inactive",
        ]);

        UserEngine::find($this->task->user_engine_id)->update([
            "in_maintenance" => false,
            "available" => true,
            "use_percent" => 0,
            "status" => "free",
        ]);


        Mouvement::adding(
            'charge',
            $this->getTitleWithType(),
            $this->getMouvementWithType(),
            $this->getAmountWithType(),
            $this->task->engine->user->company->id,
            $this->task->engine->user_hub->id,
        );

        event(new RepairEngineEvent($this->task->engine->user));


        $this->task->engine->user->notify(new SendMessageNotification(
            "Maintenance Terminer",
            $this->getTitleWithType()." Terminer",
            "success",
            asset('/storage/icons/maintenance_check.png'),
            null,
            "vigimat"
        ));

    }

    /**
     * Retrieves the amount based on the task type.
     *
     * @return int|float The calculated amount.
     */

    private function getAmountWithType(): float|int
    {
        return match ($this->task->type) {
            "engine_prev" => $this->task->engine->calcAmountMaintenancePrev(),
            "engine_cur" => $this->task->engine->calcAmountMaintenanceCur(),
            "infra" => $this->task->equipement->getAmountMaintenance(),
        };
    }

    /**
     * Retrieves the movement type based on the task type.
     *
     * @return string The movement type.
     */
    private function getMouvementWithType(): string
    {
        return match ($this->task->type) {
            "engine_prev", "engine_cur" => "maintenance_vehicule",
            "infra" => 'maintenance_technicentre',
        };
    }

    /**
     * Returns the title with type based on the task type.
     *
     * @return string The title with type.
     */
    private function getTitleWithType(): string
    {
        return match ($this->task->type) {
            "engine_prev" => 'Maintenance préventive: ' . $this->task->engine->engine->name,
            "engine_cur" => 'Maintenance curative: ' . $this->task->engine->engine->name,
            "infra" => "Maintenance Infrastructure sur le hub ".$this->task->equipement->gare->name." / ".$this->task->equipement->type_equipement,
        };
    }
}
