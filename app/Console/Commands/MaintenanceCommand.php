<?php

namespace App\Console\Commands;

use App\Jobs\Game\Maintenance\EndMaintenanceJob;
use App\Models\Core\EngineComposition;
use App\Models\User;
use App\Models\User\UserTechnicentreStaf;
use App\Models\User\UserTechnicentreTask;
use App\Models\User\UserTechnicentreTasking;
use App\Notifications\Game\SendMessageNotification;
use Exception;
use Illuminate\Console\Command;

class MaintenanceCommand extends Command
{
    protected $signature = 'maintenance {action}';

    protected $description = 'Command description';

    public function handle(): void
    {
        match ($this->argument('action')) {
            "verifTask" => $this->verifyTask(),
            "verifTasking" => $this->verifyTasking(),
            "autoMaintenance" => $this->autoMaintenance(),
        };
    }

    /**
     * Updates the status of tasks based on their start and end times.
     *
     * This function retrieves all tasks from the UserTechnicentreTasking model
     * and checks if their start or end time falls within the current minute.
     * If a task's start time falls within the current minute, its status is
     * updated to 'progresses'. If a task's end time falls within the current
     * minute, its status is updated to 'finished'.
     *
     * @throws Exception If there is an error updating the task status.
     */
    private function verifyTasking()
    {
        $tasks = UserTechnicentreTasking::all();

        foreach ($tasks as $task) {

            try {
                if ($task->start_at->between(now()->startOfMinute(), now()->endOfMinute())) {
                    $task->update([
                        'status' => 'progresses'
                    ]);
                } elseif ($task->end_at->between(now()->startOfMinute(), now()->endOfMinute())) {
                    $task->update([
                        'status' => 'finished'
                    ]);
                }
            }catch (Exception $exception) {
                \Log::emergency($exception->getMessage(), $exception->getTrace());
            }
        }
    }

    /**
     * Executes the auto maintenance process.
     *
     * This function retrieves all users with premium status and performs maintenance tasks on their engines.
     * It iterates through each user and collects the active and available engines.
     * Depending on the user's settings, it calls the appropriate maintenance function for each engine type.
     *
     * @throws \Exception If an error occurs during the maintenance process.
     */
    private function autoMaintenance(): void
    {
        $users = User::where('premium', true)->get();
        foreach ($users as $user) {
            $engines = $user->engines()->where('active', true)
                ->where('available', true)
                ->get();
            if ($user->setting->auto_ter) {
                $this->autoMaintenanceTER($user, $engines);
            } elseif ($user->setting->auto_tgv) {
                $this->autoMaintenanceTGV($user, $engines);
            } elseif ($user->setting->auto_ic) {
                $this->autoMaintenanceIC($user, $engines);
            } else {
                $this->autoMaintenanceTransilien($user, $engines);
            }
        }
    }

    /**
     * Perform auto maintenance for TER engines.
     *
     * @param mixed $user the user object
     * @param \Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\User\_IH_UserEngine_C|array $engines the engines to perform maintenance on
     * @return void
     * @throws Exception description of exception
     */
    private function autoMaintenanceTER(mixed $user, \Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\User\_IH_UserEngine_C|array $engines): void
    {
        foreach ($engines as $engine) {
            if ($engine->engine->type_train === "ter" && $engine->use_percent >= $user->setting->auto_ter_max) {
                try {
                    $technicentre = $engine->technicentre;
                    $this->updateEngine($engine);
                    $sumTime = intval($engine->engine->compositions()->sum('maintenance_repar_time')) / $engine->user_hub->technicentre->level;
                    $task = $this->createTechnicentreTask($sumTime, $technicentre, $engine);
                    $this->createTasking($task, $engine);
                    $task->engine->user->notify(new SendMessageNotification(
                        'Maintenance en cours',
                        "Une maintenance est actuellement en cours de le technicentre de " . $engine->user_hub->hub->gare->name,
                        "warning",
                        asset('/storage/icons/maintenance.png'),
                        null,
                        ["engine", "vigimat"]
                    ));

                    dispatch(new EndMaintenanceJob($task))->delay(now()->addMinutes($sumTime))->onQueue('maintenance');
                }catch (Exception $exception) {
                    \Log::emergency($exception->getMessage(), $exception->getTrace());
                }
            }
        }
    }
    /**
     * Executes the auto maintenance for TGV engines.
     *
     * @param mixed $user The user object.
     * @param \Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\User\_IH_UserEngine_C|array $engines The engines to be processed.
     * @return void
     * @throws \Exception If an error occurs during the execution.
     */
    /**
     * Executes the auto maintenance for TGV engines.
     *
     * @param mixed $user The user object.
     * @param \Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\User\_IH_UserEngine_C|array $engines The engines to be processed.
     * @return void
     * @throws \Exception If an error occurs during the execution.
     */
    private function autoMaintenanceTGV(mixed $user, \Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\User\_IH_UserEngine_C|array $engines): void
    {
        foreach ($engines as $engine) {
            if ($engine->engine->type_train == "tgv" && $engine->use_percent >= $user->setting->auto_tgv_max) {
                try {
                    $technicentre = $engine->technicentre;
                    $this->updateEngine($engine);
                    $sumTime = intval($engine->engine->compositions()->sum('maintenance_repar_time')) / $engine->user_hub->technicentre->level;
                    $task = $this->createTechnicentreTask($sumTime, $technicentre, $engine);
                    $this->createTasking($task, $engine);
                    $task->engine->user->notify(new SendMessageNotification(
                        'Maintenance en cours',
                        "Une maintenance est actuellement en cours de le technicentre de " . $engine->user_hub->hub->gare->name,
                        "warning",
                        asset('/storage/icons/maintenance.png'),
                        null,
                        "vigimat"
                    ));

                    dispatch(new EndMaintenanceJob($task))->delay(now()->addMinutes($sumTime))->onQueue('maintenance');
                }catch (Exception $exception) {
                    \Log::emergency($exception->getMessage(), $exception->getTrace());
                }
            }
        }
    }

    /**
     * A description of the autoMaintenanceIC function.
     *
     * @param mixed $user description
     * @param \Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\User\_IH_UserEngine_C|array $engines description
     * @return void
     * @throws Some_Exception_Class description of exception
     */
    private function autoMaintenanceIC(mixed $user, \Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\User\_IH_UserEngine_C|array $engines): void
    {
        foreach ($engines as $engine) {
            if ($engine->engine->type_train === "ic" && $engine->use_percent >= $user->setting->auto_ic_max) {
                try {
                    $technicentre = $engine->technicentre;
                    $this->updateEngine($engine);

                    $sumTime = intval($engine->engine->compositions()->sum('maintenance_repar_time')) / $engine->user_hub->technicentre->level;
                    $task = $this->createTechnicentreTask($sumTime, $technicentre, $engine);
                    $this->createTasking($task, $engine);

                    $message = "Une maintenance est actuellement en cours de le technicentre de " . $engine->user_hub->hub->gare->name;
                    $notification = new SendMessageNotification('Maintenance en cours', $message, "warning", asset('/storage/icons/maintenance.png'), null, "vigimat");
                    $engine->engine->user->notify($notification);

                    $delay = now()->addMinutes($sumTime);
                    dispatch(new EndMaintenanceJob($task))->delay($delay)->onQueue('maintenance');
                }catch (Exception $exception) {
                    \Log::emergency($exception->getMessage(), $exception->getTrace());
                }
            }
        }
    }

    /**
     * Executes the auto maintenance process for Transilien engines.
     *
     * @param object $user The user object.
     * @param array $engines The array of engines to process.
     * @return void
     * @throws Exception If an error occurs during the process.
     */
    private function autoMaintenanceTransilien($user, $engines): void
    {
        foreach ($engines as $engine) {
            if ($engine->engine->type_train == "transilien" && $engine->use_percent >= $user->setting->auto_ic_max) {
                try {
                    $technicentre = $engine->technicentre;
                    $this->updateEngine($engine);

                    $sum_time = $engine->engine->compositions()->sum('maintenance_repar_time') / $engine->user_hub->technicentre->level;

                    $task = $this->createTechnicentreTask($sum_time, $technicentre, $engine);
                    $this->createTasking($task, $engine);

                    $message = "Une maintenance est actuellement en cours de le technicentre de " . $engine->user_hub->hub->gare->name;
                    $this->notifyMaintenanceInProgress($task, $message);

                    $delay = now()->addMinutes($sum_time);
                    dispatch(new EndMaintenanceJob($task))->delay($delay)->onQueue('maintenance');
                }catch (Exception $exception) {
                    \Log::emergency($exception->getMessage(), $exception->getTrace());
                }
            }
        }
    }

    /**
     * Met à jour l'objet UserEngine donné.
     *
     * @param User\UserEngine $engine L'objet UserEngine à mettre à jour.
     */
    private function updateEngine(User\UserEngine $engine)
    {
        $engine->update([
            "available" => false,
            "in_maintenance" => true,
            "status" => "in_maintenance"
        ]);
    }

    /**
     * Crée une tâche Technicentre.
     *
     * @param int $sum_time La somme de temps pour la tâche.
     * @param User\UserTechnicentreEngine $technicentre Le technicentre.
     * @param User\UserEngine $engine Le moteur utilisateur.
     * @return UserTechnicentreTask La tâche Technicentre créée.
     */
    private function createTechnicentreTask(int $sum_time, User\UserTechnicentreEngine $technicentre, User\UserEngine $engine)
    {
        return UserTechnicentreTask::create([
            "type" => 'engine_prev',
            "status" => "plannified",
            "start_at" => now()->addMinute()->startOfMinute(),
            "end_at" => now()->addMinutes($sum_time)->startOfMinute(),
            "user_technicentre_id" => $technicentre->user_technicentre_id,
            "user_technicentre_staf_id" => UserTechnicentreStaf::where('user_technicentre_id', $technicentre->user_technicentre_id)->where('job', 'LIKE', '%Opérateur Maintenance%')->where('status', 'inactive')->get()->random()->id,
            "user_engine_id" => $engine->id,
        ]);
    }

    /**
     * Crée une tâche pour une tâche de technicentre utilisateur.
     *
     * @param UserTechnicentreTask $task La tâche de technicentre utilisateur.
     * @param User\UserEngine $engine Le moteur utilisateur.
     * @return void
     * @throws Some_Exception_Class Une description de l'exception.
     */
    private function createTasking(UserTechnicentreTask $task, User\UserEngine $engine)
    {
        foreach ($engine->engine->compositions as $composition) {
            if (!UserTechnicentreTasking::where('user_technicentre_task_id', $task->id)->exists()) {
                UserTechnicentreTasking::create([
                    "subject" => EngineComposition::generateTaskPreventif($composition->name),
                    "status" => "planified",
                    "start_at" => $task->start_at->addMinute()->startOfMinute(),
                    "end_at" => $task->start_at->addMinutes($composition->maintenance_repar_time / $engine->user_hub->technicentre->level)->startOfMinute(),
                    "user_technicentre_task_id" => $task->id,
                ]);
            } else {
                $tasking = $task->tasks()->orderBy('start_at', 'desc')->first();
                UserTechnicentreTasking::create([
                    "subject" => EngineComposition::generateTaskPreventif($composition->name),
                    "status" => "planified",
                    "start_at" => $tasking->end_at->startOfMinute(),
                    "end_at" => $tasking->end_at->addMinutes($composition->maintenance_repar_time / $engine->user_hub->technicentre->level)->startOfMinute(),
                    "user_technicentre_task_id" => $task->id,
                ]);
            }
        }
    }

    private function verifyTask()
    {
        $maintenances = UserTechnicentreTask::all();

        foreach ($maintenances as $maintenance) {;
            try {
                if(now()->startOfMinute() >= $maintenance->start_at && now()->startOfMinute() <= $maintenance->end_at) {
                    $this->info("PROGRESS");
                    $maintenance->update([
                        'status' => 'progressed'
                    ]);
                } elseif (now()->startOfMinute() >= $maintenance->end_at) {
                    $this->info("FINISH");
                    $maintenance->update([
                        'status' => 'finished'
                    ]);
                } else {
                    $this->info("NOT");
                    break;
                }
            }catch (Exception $exception) {
                \Log::emergency($exception->getMessage(), $exception->getTrace());
            }
        }
    }
}
