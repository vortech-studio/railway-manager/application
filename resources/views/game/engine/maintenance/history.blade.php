@extends("game.template")

@section("css")
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/train_incident.png') }}" alt="">
            </div>
            <x-base.underline
                title="Historique des incidents"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.engine.partials.menu")
    </div>
    <div class="card shadow-lg bg-opacity-100 text-white mb-10">
        <div class="card-body">
            <div class="d-flex flex-row justify-content-around align-items-center h-400px shadow-lg" style="background: url('{{ asset('/storage/other/wall-rm.png') }}')">
                <div>
                    <div class="right-arrow"></div>
                    <div class="shop-description">
                        <h3>Voici les pannes qui ont été constatées.</h3>
                        <div class="d-flex flex-row align-items-center mb-1 text-black">
                            <div class="symbol symbol-40px me-3">
                                <img src="{{ asset('/storage/icons/warning_low.png') }}" alt="">
                            </div>
                            <span class="fs-3">Panne mineure n'ayant pas d'influence réelle sur le trajet.</span>
                        </div>
                        <div class="d-flex flex-row align-items-center mb-1 text-black">
                            <div class="symbol symbol-40px me-3">
                                <img src="{{ asset('/storage/icons/warning_middle.png') }}" alt="">
                            </div>
                            <span class="fs-3">Panne majeur demandant une intervention à distance sur le trajet. (Retard Possible ~2m à 15m)</span>
                        </div>
                        <div class="d-flex flex-row align-items-center mb-1 text-black">
                            <div class="symbol symbol-40px me-3">
                                <img src="{{ asset('/storage/icons/warning_high.png') }}" alt="">
                            </div>
                            <span class="fs-3">Grave incident provoquant l'arret du train sur le trajet ou le retrait pur et simple du trajet (Annulée).<br><small>Si le train est en voie, le retard peut etre supérieur à 15m <= 1H.</small></span>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="symbol symbol-circle symbol-200px">
                        <img src="{{ asset('/storage/avatar/secretary/2.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card shadow-lg mb-10">
        <div class="card-header bg-light-primary">
            <div class="card-title">&nbsp;</div>
            <div class="card-toolbar">
                <div class="btn-group w-100 w-lg-50" data-kt-button="true" data-kt-buttons-target="[data-kt-button]">
                    <label class="btn btn-outline btn-color-muted btn-active-success" data-kt-button="true">
                        <input class="btn-check" type="radio" name="method" checked="checked" value="simple"/>
                        Simple
                    </label>
                    <label class="btn btn-outline btn-color-muted btn-active-success" data-kt-button="true">
                        <input class="btn-check" type="radio" name="method" value="advanced"/>
                        Avancée
                    </label>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include("game.engine.partials.incident", ["incidents" => auth()->user()->incidents(), "style" => "simple"])
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        document.querySelectorAll('[name="method"]').forEach(btn => {
            btn.addEventListener('click', e => {
                console.log(e.target.value)
                e.target.setAttribute('checked', 'checked')
                if(e.target.value === "simple"){
                    document.getElementById('simple').style.display = "block";
                    document.getElementById('advanced').style.display = "none";
                } else {
                    document.getElementById('simple').style.display = "none";
                    document.getElementById('advanced').style.display = "block";
                }
            })
        })
    </script>
@endsection
