<div class="alert alert-dismissible bg-warning d-flex flex-center flex-column py-10 px-10 px-lg-20 mb-10">
    <button type="button" class="position-absolute top-0 end-0 m-2 btn btn-icon btn-icon-light-warning" data-bs-dismiss="alert">
        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
    </button>

    <i class="fa-solid fa-exclamation-triangle fs-5tx text-light-warning mb-5"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>

    <div class="text-center">
        <h1 class="fw-bold mb-5">Vente d'un matériel roulant</h1>

        <div class="separator separator-dashed border-danger opacity-25 mb-5"></div>

        <div class="mb-9 text-gray-900">
            <p>Vendre ce matériel roulant pour <strong>{{ eur($engine->calcSell()) }}</strong></p>
            <p>
                Les trajets planifiés seront supprimés.<br>
                L'historique de trajet sera supprimé.
            </p>
        </div>

        <div class="d-flex flex-center flex-wrap">
            <button data-action="selling" class="btn btn-danger m-2">Valider</button>
        </div>
    </div>
</div>
