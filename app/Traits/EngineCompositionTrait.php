<?php

namespace App\Traits;

use App\Models\Core\Engine;

trait EngineCompositionTrait
{
    protected static function tableEngineMoteur(Engine $engine, string $type_transmission): void
    {
        if ($type_transmission == 'diesel') {
            $engine->compositions()->create([
                "name" => "Moteur Termique",
                "importance" => 4,
                "maintenance_check_time" => 15,
                "maintenance_repar_time" => 30,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "motorisation"
            ]);

            $engine->compositions()->create([
                "name" => "Boite de Vitesse",
                "importance" => 3,
                "maintenance_check_time" => 5,
                "maintenance_repar_time" => 10,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "motorisation"
            ]);

            $engine->compositions()->create([
                "name" => "Infrasture de caisse",
                "importance" => 2,
                "maintenance_check_time" => 45,
                "maintenance_repar_time" => 90,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "infrastructure"
            ]);

            $engine->compositions()->create([
                "name" => "Attelage",
                "importance" => 2,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "infrastructure"
            ]);

            $engine->compositions()->create([
                "name" => "Cabine de conduite",
                "importance" => 1,
                "maintenance_check_time" => 50,
                "maintenance_repar_time" => 100,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "cabine"
            ]);

            $engine->compositions()->create([
                "name" => "Bogie",
                "importance" => 4,
                "maintenance_check_time" => 120,
                "maintenance_repar_time" => 240,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "bogie"
            ]);

            $engine->compositions()->create([
                "name" => "Bloc électrique centrale",
                "importance" => 1,
                "maintenance_check_time" => 13,
                "maintenance_repar_time" => 26,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);

            $engine->compositions()->create([
                "name" => "Bloc auxiliaire – batterie",
                "importance" => 1,
                "maintenance_check_time" => 13,
                "maintenance_repar_time" => 26,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);

            $engine->compositions()->create([
                "name" => "Transformateur Principal",
                "importance" => 1,
                "maintenance_check_time" => 13,
                "maintenance_repar_time" => 26,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Sablière",
                "importance" => 0,
                "maintenance_check_time" => 3,
                "maintenance_repar_time" => 6,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "divers"
            ]);
            $engine->compositions()->create([
                "name" => "Lanterneau de sortie d’air",
                "importance" => 0,
                "maintenance_check_time" => 6,
                "maintenance_repar_time" => 12,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "ventilation"
            ]);
            $engine->compositions()->create([
                "name" => "Disjoncteur monophasé",
                "importance" => 2,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Groupe de filtration d’air",
                "importance" => 1,
                "maintenance_check_time" => 15,
                "maintenance_repar_time" => 30,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "ventilation"
            ]);
        } elseif ($type_transmission == 'electrique') {
            $engine->compositions()->create([
                "name" => "Moteur Termique",
                "importance" => 4,
                "maintenance_check_time" => 15,
                "maintenance_repar_time" => 30,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "motorisation"
            ]);

            $engine->compositions()->create([
                "name" => "Generateur",
                "importance" => 3,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 23,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Convertisseur statique",
                "importance" => 3,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 23,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);

            $engine->compositions()->create([
                "name" => "Infrasture de caisse",
                "importance" => 2,
                "maintenance_check_time" => 45,
                "maintenance_repar_time" => 90,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "infrastructure"
            ]);

            $engine->compositions()->create([
                "name" => "Attelage",
                "importance" => 2,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "infrastructure"
            ]);

            $engine->compositions()->create([
                "name" => "Cabine de conduite",
                "importance" => 1,
                "maintenance_check_time" => 50,
                "maintenance_repar_time" => 100,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "cabine"
            ]);

            $engine->compositions()->create([
                "name" => "Bogie",
                "importance" => 4,
                "maintenance_check_time" => 120,
                "maintenance_repar_time" => 240,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "bogie"
            ]);

            $engine->compositions()->create([
                "name" => "Bloc électrique centrale",
                "importance" => 1,
                "maintenance_check_time" => 13,
                "maintenance_repar_time" => 26,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);

            $engine->compositions()->create([
                "name" => "Bloc auxiliaire – batterie",
                "importance" => 1,
                "maintenance_check_time" => 13,
                "maintenance_repar_time" => 26,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Batterie",
                "importance" => 2,
                "maintenance_check_time" => 3,
                "maintenance_repar_time" => 6,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Semi-conducteurs de puissance",
                "importance" => 3,
                "maintenance_check_time" => 6,
                "maintenance_repar_time" => 12,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Pantographe",
                "importance" => 3,
                "maintenance_check_time" => 29,
                "maintenance_repar_time" => 58,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "infrastructure"
            ]);
            $engine->compositions()->create([
                "name" => "Ligne de toiture",
                "importance" => 1,
                "maintenance_check_time" => 32,
                "maintenance_repar_time" => 64,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "infrastructure"
            ]);
            $engine->compositions()->create([
                "name" => "Liaison haute tension",
                "importance" => 2,
                "maintenance_check_time" => 47,
                "maintenance_repar_time" => 94,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);

            $engine->compositions()->create([
                "name" => "Transformateur Principal",
                "importance" => 1,
                "maintenance_check_time" => 13,
                "maintenance_repar_time" => 26,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Sablière",
                "importance" => 0,
                "maintenance_check_time" => 3,
                "maintenance_repar_time" => 6,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "divers"
            ]);
            $engine->compositions()->create([
                "name" => "Lanterneau de sortie d’air",
                "importance" => 0,
                "maintenance_check_time" => 6,
                "maintenance_repar_time" => 12,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "ventilation"
            ]);
            $engine->compositions()->create([
                "name" => "Disjoncteur monophasé",
                "importance" => 2,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Groupe de filtration d’air",
                "importance" => 1,
                "maintenance_check_time" => 15,
                "maintenance_repar_time" => 30,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "ventilation"
            ]);
        } else {
            $engine->compositions()->create([
                "name" => "Moteur Termique",
                "importance" => 4,
                "maintenance_check_time" => 15,
                "maintenance_repar_time" => 30,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "motorisation"
            ]);

            $engine->compositions()->create([
                "name" => "Generateur",
                "importance" => 3,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 23,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"

            ]);
            $engine->compositions()->create([
                "name" => "Convertisseur statique",
                "importance" => 3,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 23,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Convertisseur Diesel/Courant",
                "importance" => 3,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 23,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);

            $engine->compositions()->create([
                "name" => "Boite de Vitesse",
                "importance" => 3,
                "maintenance_check_time" => 5,
                "maintenance_repar_time" => 10,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "motorisation"
            ]);

            $engine->compositions()->create([
                "name" => "Infrasture de caisse",
                "importance" => 2,
                "maintenance_check_time" => 45,
                "maintenance_repar_time" => 90,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "infrastructure"
            ]);

            $engine->compositions()->create([
                "name" => "Attelage",
                "importance" => 2,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "infrastructure"
            ]);

            $engine->compositions()->create([
                "name" => "Cabine de conduite",
                "importance" => 1,
                "maintenance_check_time" => 50,
                "maintenance_repar_time" => 100,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "cabine"
            ]);

            $engine->compositions()->create([
                "name" => "Bogie",
                "importance" => 4,
                "maintenance_check_time" => 120,
                "maintenance_repar_time" => 240,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "bogie"
            ]);

            $engine->compositions()->create([
                "name" => "Bloc électrique centrale",
                "importance" => 1,
                "maintenance_check_time" => 13,
                "maintenance_repar_time" => 26,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);

            $engine->compositions()->create([
                "name" => "Bloc auxiliaire – batterie",
                "importance" => 1,
                "maintenance_check_time" => 13,
                "maintenance_repar_time" => 26,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);

            $engine->compositions()->create([
                "name" => "Transformateur Principal",
                "importance" => 1,
                "maintenance_check_time" => 13,
                "maintenance_repar_time" => 26,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Sablière",
                "importance" => 0,
                "maintenance_check_time" => 3,
                "maintenance_repar_time" => 6,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "divers"
            ]);
            $engine->compositions()->create([
                "name" => "Lanterneau de sortie d’air",
                "importance" => 0,
                "maintenance_check_time" => 6,
                "maintenance_repar_time" => 12,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "ventilation"
            ]);
            $engine->compositions()->create([
                "name" => "Disjoncteur Multiphasé",
                "importance" => 3,
                "maintenance_check_time" => 13,
                "maintenance_repar_time" => 26,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "electricite"
            ]);
            $engine->compositions()->create([
                "name" => "Groupe de filtration d’air",
                "importance" => 1,
                "maintenance_check_time" => 15,
                "maintenance_repar_time" => 30,
                "engine_id" => $engine->id,
                "type" => "motrice",
                "sub_type" => "ventilation"
            ]);
        }
    }

    protected static function tableEngineRemorque(Engine $engine, string $type_marchandise): void
    {
        $engine->compositions()->create([
            "name" => "Infrasture de caisse",
            "importance" => 2,
            "maintenance_check_time" => 45,
            "maintenance_repar_time" => 90,
            "engine_id" => $engine->id,
            "type" => "voiture",
            "sub_type" => "infrastructure"
        ]);

        $engine->compositions()->create([
            "name" => "Attelage",
            "importance" => 2,
            "maintenance_check_time" => 10,
            "maintenance_repar_time" => 20,
            "engine_id" => $engine->id,
            "type" => "voiture",
            "sub_type" => "infrastructure"
        ]);

        $engine->compositions()->create([
            "name" => "Bogie",
            "importance" => 4,
            "maintenance_check_time" => 32,
            "maintenance_repar_time" => 64,
            "engine_id" => $engine->id,
            "type" => "voiture",
            "sub_type" => "bogie"
        ]);

        $engine->compositions()->create([
            "name" => "Système de freinage",
            "importance" => 2,
            "maintenance_check_time" => 10,
            "maintenance_repar_time" => 20,
            "engine_id" => $engine->id,
            "type" => "voiture",
            "sub_type" => "infrastructure"
        ]);

        if($type_marchandise == 'passagers')
        {
            $engine->compositions()->create([
                "name" => "Luminaires",
                "importance" => 1,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "electricite"
            ]);

            $engine->compositions()->create([
                "name" => "Portes",
                "importance" => 1,
                "maintenance_check_time" => 14,
                "maintenance_repar_time" => 28,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "infrastructure"
            ]);

            $engine->compositions()->create([
                "name" => "Fenêtres",
                "importance" => 1,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "infrastructure"
            ]);

            $engine->compositions()->create([
                "name" => "Sièges",
                "importance" => 1,
                "maintenance_check_time" => 32,
                "maintenance_repar_time" => 64,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "infrastructure"
            ]);

            $engine->compositions()->create([
                "name" => "Climatisation",
                "importance" => 1,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "ventilation"
            ]);

            $engine->compositions()->create([
                "name" => "Chauffage",
                "importance" => 1,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "ventilation"
            ]);

            $engine->compositions()->create([
                "name" => "Toilettes",
                "importance" => 1,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "infrastructure"
            ]);

            $engine->compositions()->create([
                "name" => "Eclairage",
                "importance" => 1,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "electricite"
            ]);

            $engine->compositions()->create([
                "name" => "Système de sécurité",
                "importance" => 1,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "system"
            ]);

            $engine->compositions()->create([
                "name" => "Système de communication",
                "importance" => 1,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "system"
            ]);

            $engine->compositions()->create([
                "name" => "Système de ventilation",
                "importance" => 1,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "ventilation"
            ]);

            $engine->compositions()->create([
                "name" => "Système de signalisation",
                "importance" => 1,
                "maintenance_check_time" => 10,
                "maintenance_repar_time" => 20,
                "engine_id" => $engine->id,
                "type" => "voiture",
                "sub_type" => "system"
            ]);
        }
    }

    protected static function tableEngineAutomotrice(Engine $engine, string $type_energy): void
    {
        $engine->tableEngineMoteur($engine, $type_energy);
        $engine->tableEngineRemorque($engine, 'passagers');
    }
}
