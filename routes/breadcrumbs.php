<?php

use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

Breadcrumbs::for('home', function (BreadcrumbTrail $trail): void {
    $trail->push('Home', route('home'));
});

Breadcrumbs::for('account.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push('Mon Compte', route('account.index'));
});

Breadcrumbs::for('account.objectif', function (BreadcrumbTrail $trail): void {
    $trail->parent('account.index');
    $trail->push('Mes Objectifs', route('account.objectif'));
});

Breadcrumbs::for('account.premium', function (BreadcrumbTrail $trail): void {
    $trail->parent('account.index');
    $trail->push('Premium', route('account.premium'));
});
Breadcrumbs::for('mailbox.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('account.index');
    $trail->push('Ma Messagerie', route('mailbox.index'));
});
Breadcrumbs::for('account.notifications', function (BreadcrumbTrail $trail): void {
    $trail->parent('account.index');
    $trail->push('Mes notifications', route('account.notifications'));
});

Breadcrumbs::for('mailbox.show', function (BreadcrumbTrail $trail, $message): void {
    $trail->parent('account.index');
    $trail->parent('mailbox.index');
    $trail->push(\App\Models\User\Mailbox::find($message)->subject, route('mailbox.show', $message));
});

Breadcrumbs::for('shop.index', function (BreadcrumbTrail $trail): void {
    $trail->push('Boutique', route('shop.index'));
});

Breadcrumbs::for('shop.checkout', function (BreadcrumbTrail $trail): void {
    $trail->parent('shop.index');
    $trail->push('Paiement', route('shop.checkout'));
});

Breadcrumbs::for('card.show', function (BreadcrumbTrail $trail, $card): void {
    $trail->parent('shop.index');
    $trail->push("Porte Carte ", route('card.show', $card));
});

Breadcrumbs::for('network', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push('Réseau', route('network'));
});

Breadcrumbs::for('hub.show', function (BreadcrumbTrail $trail, $hub): void {
    $trail->parent('network');
    $trail->push("Hub", route('hub.show', $hub));
});
Breadcrumbs::for('hub.checkout', function (BreadcrumbTrail $trail): void {
    $trail->parent('network');
    $trail->push("Achat d'un Hub", route('hub.checkout'));
});
Breadcrumbs::for('ligne.show', function (BreadcrumbTrail $trail, $ligne): void {
    $trail->parent('network');
    $trail->push("Ligne", route('ligne.show', $ligne));
});

Breadcrumbs::for('ligne.checkout', function (BreadcrumbTrail $trail): void {
    $trail->parent('network');
    $trail->push("Achat d'une ligne", route('ligne.checkout'));
});

Breadcrumbs::for('planning.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('network');
    $trail->push("Planning Générale", route('planning.index'));
});

Breadcrumbs::for('planning.editing', function (BreadcrumbTrail $trail): void {
    $trail->parent('network');
    $trail->push("Planning d'édition", route('planning.editing'));
});

Breadcrumbs::for('travel.show', function (BreadcrumbTrail $trail, $travel): void {
    $trail->parent('network');
    $trail->push("Voyage", route('travel.show', $travel));
});

Breadcrumbs::for('engine.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Ma Flotte", route('engine.index'));
});

Breadcrumbs::for('engine.show', function (BreadcrumbTrail $trail, $engine): void {
    $trail->parent('home');
    $trail->push("Vue d'un engin", route('engine.show', $engine));
});

Breadcrumbs::for('engine.buy.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Achat de matériels roulants", route('engine.buy.index'));
});

Breadcrumbs::for('engine.buy.checkout', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Achat de matériels roulants", route('engine.buy.checkout'));
});

Breadcrumbs::for('engine.buy.config', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Achat de matériels roulants", route('engine.buy.config'));
});

Breadcrumbs::for('engine.rental.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Location de matériels roulants", route('engine.rental.index'));
});

Breadcrumbs::for('engine.rental.engine', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Location de matériels roulants", route('engine.rental.engine'));
});

Breadcrumbs::for('engine.rental.config', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Location de matériels roulants", route('engine.rental.config'));
});

Breadcrumbs::for('engine.maintenance.book', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Carnet d'entretient", route('engine.maintenance.book'));
});

Breadcrumbs::for('engine.maintenance.history', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Carnet d'entretient", route('engine.maintenance.history'));
});

Breadcrumbs::for('engine.maintenance.group', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Carnet d'entretient", route('engine.maintenance.group'));
});
Breadcrumbs::for('compagnie.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Ma Compagnie", route('compagnie.index'));
});
Breadcrumbs::for('compagnie.bonus.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Connexion quotidienne", route('compagnie.bonus.index'));
});
Breadcrumbs::for('compagnie.customise.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Connexion quotidienne", route('compagnie.customise.index'));
});
Breadcrumbs::for('compagnie.profil.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Connexion quotidienne", route('compagnie.profil.index'));
});

Breadcrumbs::for('research.index', function (BreadcrumbTrail $trail): void {
    $trail->parent('home');
    $trail->push("Recherches & Développements", route('research.index'));
});



