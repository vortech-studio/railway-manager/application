@extends("game.template")

@section("css")

@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="card shadow-lg">
        <div class="card-header">
            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x my-5 fs-4">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#card">Porte Carte</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#tpoint">TP Point</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#engines">Matériels Roulants</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="card" role="tabpanel">
                    <div class="border-2 border-gray-800 rounded-2 bg-gray-400 p-2">
                        <x-base.underline
                            title="Bienvenue sur la page des portes cartes"
                            size="4"
                            size-text="fs-1"/>

                        <div class="d-flex flex-row align-items-center mb-10">
                            <div class="d-flex flex-column justify-content-center align-items-center w-50 px-20">
                                <p>Ici, vous pouvez échanger vos TP Point contre des Porte-Cartes.
                                    Les Porte-Cartes contiennent des éléments utiles pour votre compagnie : matériels
                                    roulants, Argent, réduction d'impôt...</p>
                                <p>De plus, Le PORTE-CARTE ÉCONOMIQUE est GRATUIT toutes les 24 heures !</p>
                                <button class="btn btn-sm btn-primary">Consulter la liste des cartes</button>
                            </div>
                            <div class="d-flex flex-center justify-content-center align-items-center w-50">
                                <img src="{{ asset('/storage/icons/gatcha.png') }}" alt="Porte Carte" class="w-250px">
                            </div>
                        </div>
                        <div class="separator border-gray-600 border-2 my-10 shadow-sm"></div>
                        <div class="text-center text-gray-800 fs-2x fw-bolder mb-5">CHOISISSEZ UN PORTE-CARTE</div>
                        <div class="row">
                            <div class="col-md-3">
                                <div
                                    class="d-flex flex-column justify-content-center align-items-center border-2 border-gray-800 bg-gray-500">
                                    <div class="bg-success p-3 w-100">
                                        <div class="text-center">Economique</div>
                                    </div>
                                    <div class="py-10">
                                        <img src="{{ asset('/storage/icons/card_eco.png') }}" alt="Porte Carte"
                                             class="w-100px">
                                    </div>
                                    <div class="d-flex flex-column justify-center-center bg-gray-600 w-100 p-3">
                                        @if(auth()->user()->cards()->where('type_card', 'economique')->whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])->count() > 0)
                                            <button class="btn btn-sm btn-primary" data-action="checkout"
                                                    data-currency="tpoint" data-slug="card-eco" data-amount="0" disabled>Gratuit
                                            </button>
                                            <p class="fw-bolder text-white text-center">Prochain tirage {{ now()->addDay()->startOfDay()->longAbsoluteDiffForHumans() }}</p>
                                        @else
                                            <button class="btn btn-sm btn-primary" data-action="checkout"
                                                    data-currency="tpoint" data-slug="card-eco" data-amount="0">Gratuit
                                            </button>
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="d-flex flex-column justify-content-center align-items-center border-2 border-gray-800 bg-gray-500">
                                    <div class="bg-success p-3 w-100">
                                        <div class="text-center">Finance</div>
                                    </div>
                                    <div class="py-10">
                                        <img src="{{ asset('/storage/icons/card_finance.png') }}" alt="Porte Carte"
                                             class="w-100px">
                                    </div>
                                    <div class="d-flex flex-center bg-gray-600 w-100 p-3">
                                        <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                                data-action="checkout" data-slug="card-finance" data-currency="tpoint"
                                                data-amount="150">
                                            <span class="fs-6 me-1">150</span>
                                            <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="TP Point"
                                                 class="w-20px">
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="d-flex flex-column justify-content-center align-items-center border-2 border-gray-800 bg-gray-500">
                                    <div class="bg-success p-3 w-100">
                                        <div class="text-center">Platinum</div>
                                    </div>
                                    <div class="py-10">
                                        <img src="{{ asset('/storage/icons/card_platinum.png') }}" alt="Porte Carte"
                                             class="w-100px">
                                    </div>
                                    <div class="d-flex flex-center bg-gray-600 w-100 p-3">
                                        <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                                data-action="checkout" data-slug="card-platinum" data-currency="tpoint"
                                                data-amount="500">
                                            <span class="fs-6 me-1">500</span>
                                            <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="TP Point"
                                                 class="w-20px">
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="d-flex flex-column justify-content-center align-items-center border-2 border-gray-800 bg-gray-500">
                                    <div class="bg-success p-3 w-100">
                                        <div class="text-center">Epique</div>
                                    </div>
                                    <div class="py-10">
                                        <img src="{{ asset('/storage/icons/card_epique.png') }}" alt="Porte Carte"
                                             class="w-100px">
                                    </div>
                                    <div class="d-flex flex-center bg-gray-600 w-100 p-3">
                                        <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                                data-action="checkout" data-slug="card-epique" data-currency="tpoint"
                                                data-amount="1300">
                                            <span class="fs-6 me-1">1 300</span>
                                            <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="TP Point"
                                                 class="w-20px">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="separator border-gray-600 border-2 my-10 shadow-sm"></div>
                        <div class="text-center text-gray-800 fs-2x fw-bolder mb-5">BONUS PORTE-CARTE</div>
                        <div class="d-flex flex-row justify-content-around align-items-center">
                            <div
                                class="d-flex flex-row align-items-center bg-gray-800 rounded-2 p-5 h-100px text-white">
                                <div class="symbol symbol-50px me-5">
                                    <img src="{{ asset('/storage/icons/simulation.png') }}" alt="" class="w-50px">
                                </div>
                                <div class="d-flex flex-column justify-content-center align-items-center">
                                    <div class="fs-3 border-bottom border-white fw-bold">Simulations Offertes</div>
                                    <div
                                        class="fs-6 pt-3">{{ auth()->user()->card_bonus->sim_offer }} {{ Str::plural('restante', auth()->user()->card_bonus->sim_offer) }}</div>
                                </div>
                            </div>
                            <div
                                class="d-flex flex-row align-items-center bg-gray-800 rounded-2 p-5 h-100px text-white">
                                <div class="symbol symbol-50px me-5">
                                    <img src="{{ asset('/storage/icons/audit.png') }}" alt="" class="w-50px">
                                </div>
                                <div class="d-flex flex-column justify-content-center align-items-center">
                                    <div class="fs-3 border-bottom border-white fw-bold">Audit Interne Offert</div>
                                    <div
                                        class="fs-6 pt-3">{{ auth()->user()->card_bonus->audit_int_offer }} {{ Str::plural('restante', auth()->user()->card_bonus->audit_int_offer) }}</div>
                                </div>
                            </div>
                            <div
                                class="d-flex flex-row align-items-center bg-gray-800 rounded-2 p-5 h-100px text-white">
                                <div class="symbol symbol-50px me-5">
                                    <img src="{{ asset('/storage/icons/auditor.png') }}" alt="" class="w-50px">
                                </div>
                                <div class="d-flex flex-column justify-content-center align-items-center">
                                    <div class="fs-3 border-bottom border-white fw-bold">Audit Externe Offert</div>
                                    <div
                                        class="fs-6 pt-3">{{ auth()->user()->card_bonus->audit_ext_offer }} {{ Str::plural('restante', auth()->user()->card_bonus->audit_ext_offer) }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tpoint" role="tabpanel">
                    <div class="border-2 border-gray-800 rounded-2 bg-gray-400 p-2">
                        <x-base.underline
                            title="Bienvenue sur le TPoint Shop"
                            size="4"
                            size-text="fs-1"/>

                        <div class="d-flex flex-row align-items-center mb-10">
                            <div class="d-flex flex-column justify-content-center align-items-center w-50 px-20">
                                <p>Achetez des TP Points et n'attendez plus la livraison de vos matériels roulants, de
                                    vos recherches et de vos prêts.</p>
                                <p>Votre stock de TP Points vous permettra certainement de goûter au confort de
                                    l'abonnement Premium (suppression de la publicité et carte des trajets en direct et
                                    plus de 10 autres nouveautés)</p>
                            </div>
                            <div class="d-flex flex-center justify-content-center align-items-center w-50">
                                <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="Porte Carte" class="w-250px">
                            </div>
                        </div>
                        <div class="separator border-gray-600 border-2 my-10 shadow-sm"></div>
                        <div class="text-center text-gray-800 fs-2x fw-bolder mb-5">CHOISISSEZ UN PACK</div>
                        <div class="row mb-10">
                            <div class="col-md-4">
                                <div
                                    class="d-flex flex-column justify-content-center align-items-center border-2 border-gray-800 bg-gray-500 h-200px">
                                    <div class="d-flex flex-row">
                                        <div class="symbol symbol-50 me-2">
                                            <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="TP Point"
                                                 class="w-50px">
                                        </div>
                                        <div class="d-flex flex-column justify-content-center align-items-center">
                                            <div class="fs-1 fw-bolder text-white">140</div>
                                            <div class="fs-6 text-color-eva-departure">TP Point</div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-center bg-gray-600 w-100 p-3">
                                        <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                                data-action="checkout" data-slug="tpoint-140" data-currency="euro"
                                                data-amount="300">
                                            <span class="fs-6 me-1">{{ eur(3.00) }}</span>
                                            <img src="{{ asset('/storage/icons/euro.png') }}" alt="Euro" class="w-20px">
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div
                                    class="d-flex flex-column justify-content-center align-items-center border-2 border-gray-800 bg-gray-500 h-200px">
                                    <div class="d-flex flex-row">
                                        <div class="symbol symbol-50 me-2">
                                            <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="TP Point"
                                                 class="w-50px">
                                        </div>
                                        <div class="d-flex flex-column justify-content-center align-items-center">
                                            <div class="fs-1 fw-bolder text-white">160</div>
                                            <div class="fs-6 text-color-eva-departure">TP Point</div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-center bg-gray-600 w-100 p-3">
                                        <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                                data-action="checkout" data-slug="tpoint-160" data-currency="euro"
                                                data-amount="350">
                                            <span class="fs-6 me-1">{{ eur(3.50) }}</span>
                                            <img src="{{ asset('/storage/icons/euro.png') }}" alt="Euro" class="w-20px">
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div
                                    class="d-flex flex-column justify-content-center align-items-center border-2 border-gray-800 bg-gray-500 h-200px">
                                    <div class="d-flex flex-row">
                                        <div class="symbol symbol-50 me-2">
                                            <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="TP Point"
                                                 class="w-50px">
                                        </div>
                                        <div class="d-flex flex-column justify-content-center align-items-center">
                                            <div class="fs-1 fw-bolder text-white">260</div>
                                            <div class="fs-6 text-color-eva-departure">TP Point</div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-center bg-gray-600 w-100 p-3">
                                        <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                                data-action="checkout" data-slug="tpoint-260" data-currency="euro"
                                                data-amount="450">
                                            <span class="fs-6 me-1">{{ eur(4.50) }}</span>
                                            <img src="{{ asset('/storage/icons/euro.png') }}" alt="Euro" class="w-20px">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center bg-gray-800 rounded-2 p-10 h-150px text-white mb-5">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-50px me-5">
                                    <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="" class="w-50px">
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fs-3 border-bottom border-white fw-bold mb-5">La Sacoche</div>
                                    <div class="fs-1 fw-bolder">300</div>
                                    <div class="fs-6 text-color-eva-arrival">TP Point</div>
                                </div>
                            </div>
                            <div class="d-flex flex-end align-items-center">
                                <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                        data-action="checkout" data-slug="tpoint-300" data-currency="euro"
                                        data-amount="499">
                                    <span class="fs-6 me-1">{{ eur(4.99) }}</span>
                                    <img src="{{ asset('/storage/icons/euro.png') }}" alt="Euro" class="w-20px">
                                </button>
                            </div>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center bg-gray-800 rounded-2 p-10 h-150px text-white mb-5">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-50px me-5">
                                    <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="" class="w-50px">
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fs-3 border-bottom border-white fw-bold mb-5">La Besace</div>
                                    <div class="fs-1 fw-bolder">700</div>
                                    <div class="fs-6 text-color-eva-arrival">TP Point</div>
                                </div>
                            </div>
                            <div class="d-flex flex-end align-items-center">
                                <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                        data-action="checkout" data-slug="tpoint-700" data-currency="euro"
                                        data-amount="999">
                                    <span class="fs-6 me-1">{{ eur(9.99) }}</span>
                                    <img src="{{ asset('/storage/icons/euro.png') }}" alt="Euro" class="w-20px">
                                </button>
                            </div>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center bg-gray-800 rounded-2 p-10 h-150px text-white mb-5">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-50px me-5">
                                    <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="" class="w-50px">
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fs-3 border-bottom border-white fw-bold mb-5">Le sac à dos</div>
                                    <div class="fs-1 fw-bolder">1 600</div>
                                    <div class="fs-6 text-color-eva-arrival">TP Point</div>
                                </div>
                            </div>
                            <div class="d-flex flex-end align-items-center">
                                <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                        data-action="checkout" data-slug="tpoint-1600" data-currency="euro"
                                        data-amount="1990">
                                    <span class="fs-6 me-1">{{ eur(19.90) }}</span>
                                    <img src="{{ asset('/storage/icons/euro.png') }}" alt="Euro" class="w-20px">
                                </button>
                            </div>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center bg-gray-800 rounded-2 p-10 h-150px text-white mb-5">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-50px me-5">
                                    <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="" class="w-50px">
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fs-3 border-bottom border-white fw-bold mb-5">La valise cabine</div>
                                    <div class="fs-1 fw-bolder">2 700</div>
                                    <div class="fs-6 text-color-eva-arrival">TP Point</div>
                                </div>
                            </div>
                            <div class="d-flex flex-end align-items-center">
                                <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                        data-action="checkout" data-slug="tpoint-2700" data-currency="euro"
                                        data-amount="2990">
                                    <span class="fs-6 me-1">{{ eur(29.90) }}</span>
                                    <img src="{{ asset('/storage/icons/euro.png') }}" alt="Euro" class="w-20px">
                                </button>
                            </div>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center bg-gray-800 rounded-2 p-10 h-150px text-white mb-5">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-50px me-5">
                                    <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="" class="w-50px">
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fs-3 border-bottom border-white fw-bold mb-5">La grande valise</div>
                                    <div class="fs-1 fw-bolder">4 800</div>
                                    <div class="fs-6 text-color-eva-arrival">TP Point</div>
                                </div>
                            </div>
                            <div class="d-flex flex-end align-items-center">
                                <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                        data-action="checkout" data-slug="tpoint-4800" data-currency="euro"
                                        data-amount="4990">
                                    <span class="fs-6 me-1">{{ eur(49.90) }}</span>
                                    <img src="{{ asset('/storage/icons/euro.png') }}" alt="Euro" class="w-20px">
                                </button>
                            </div>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center bg-gray-800 rounded-2 p-10 h-150px text-white mb-5">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-50px me-5">
                                    <img src="{{ asset('/storage/icons/tpoint.png') }}" alt="" class="w-50px">
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fs-3 border-bottom border-white fw-bold mb-5">Le container</div>
                                    <div class="fs-1 fw-bolder">8 100</div>
                                    <div class="fs-6 text-color-eva-arrival">TP Point</div>
                                </div>
                            </div>
                            <div class="d-flex flex-end align-items-center">
                                <button class="btn btn-sm btn-primary btn-flex flex-row align-items-center"
                                        data-action="checkout" data-slug="tpoint-8100" data-currency="euro"
                                        data-amount="7990">
                                    <span class="fs-6 me-1">{{ eur(79.90) }}</span>
                                    <img src="{{ asset('/storage/icons/euro.png') }}" alt="Euro" class="w-20px">
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" tabindex="-1" id="modalStripeCheckout">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">Paiement par stripe de votre objets</h3>

                                    <!--begin::Close-->
                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                         data-bs-dismiss="modal" aria-label="Close">
                                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span
                                                class="path2"></span></i>
                                    </div>
                                    <!--end::Close-->
                                </div>

                                <div class="modal-body">
                                    <div id="express-checkout-element">
                                        <!-- Express Checkout Element will be inserted here -->
                                    </div>
                                    <div id="error-message">
                                        <!-- Display error message to your customers here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="engines" role="tabpanel">
                    <div class="border-2 border-gray-800 rounded-2 bg-gray-400 p-2">
                        <x-base.underline
                            title="BIENVENUE SUR LA BOUTIQUE DES MATERIELS ROULANTS !"
                            size="4"
                            size-text="fs-1"/>

                        <div class="d-flex flex-row align-items-center mb-10">
                            <div class="d-flex flex-column justify-content-center align-items-center w-50 px-20">
                                <p>Redonnez vie aux légendes du ferroviaire en débloquant des modèles de collection à l'aide de vos TP Point et de quelques euros. Bien sûr, leurs performances ne sont pas au niveau des autres modèles disponibles en R&D.</p>
                            </div>
                            <div class="d-flex flex-center justify-content-center align-items-center w-50">
                                <img src="{{ asset('/storage/icons/train.png') }}" alt="Train" class="w-250px">
                            </div>
                        </div>
                        <div class="separator border-gray-600 border-2 my-10 shadow-sm"></div>
                        <div class="text-center text-gray-800 fs-2x fw-bolder mb-5">CHOISISSEZ UN MATERIEL ROULANT</div>
                        @foreach(\App\Models\Core\Engine::where('in_shop', true)->get() as $engine)
                            <div class="d-flex flex-column w-100 bg-gray-800 p-10 mb-5 shadow-lg" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ $engine->name }}">
                            <div class="d-flex justify-content-center align-items-center">
                                <img src="{{ asset('/storage/engines/'.$engine->type_engine.'/'.$engine->slug.'.gif') }}" alt="{{ $engine->name }}" class="w-450px">
                            </div>
                            <div class="fw-bolder text-white fs-3">{{ Str::ucfirst($engine->type_engine_format) }} {{ $engine->type_energy_format }}</div>
                            <div class="d-flex flex-row justify-content-between align-items-baseline">
                                <span class="text-white fs-semibold">{{ $engine->name }}</span>
                                <div class="d-flex flex-row">
                                    <a href="" class="btn btn-flush btn-text-active-primary btn-icon"><i class="fa-solid fa-eye fs-3"></i> </a>
                                    <button class="btn btn-sm btn-primary" data-action="checkout-engine" data-slug="{{ $engine->slug }}" data-currency="{{ $engine->money_shop }}" data-amount="{{ $engine->money_shop != 'euro' ? $engine->price_shop : $engine->price_shop * 10 }}">
                                        <i class="fa-solid fa-shopping-cart me-3"></i> Activer pour
                                        @if($engine->money_shop != 'euro')
                                            {{ $engine->price_shop }} {{ Str::plural('TP Point', $engine->price_shop) }}
                                        @else
                                            {{ eur($engine->price_shop) }}
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">
        const stripe = Stripe('{{ config('services.stripe.public_key') }}');

        document.querySelectorAll('[data-action="checkout"]').forEach(btn => {
            btn.addEventListener('click', e => {
                e.preventDefault()
                if (btn.dataset.currency === 'tpoint') {
                    $.ajax({
                        url: '{{ route('shop.checkout') }}',
                        method: 'POST',
                        data: {
                            slug: btn.dataset.slug,
                            amount: btn.dataset.amount
                        },
                        success: data => {
                            console.log(data)
                            toastr.success(data.message)
                        }
                    })
                } else if (btn.dataset.currency === 'euro') {
                    @if(config('app.env') == 'local')
                        stripe.verify_ssl_certs = false
                    @endif
                    const options = {
                        mode: 'payment',
                        amount: parseInt(btn.dataset.amount),
                        currency: 'eur',
                        lineItems: [
                            {
                                name: btn.dataset.slug,
                                amount: btn.dataset.amount,
                            }
                        ],
                    }

                    const elements = stripe.elements(options)
                    const expressCheckoutElement = elements.create('expressCheckout');
                    expressCheckoutElement.mount('#express-checkout-element');
                    const modalExpressCheckout = new bootstrap.Modal(document.getElementById('modalStripeCheckout'))
                    modalExpressCheckout.show()

                    const handleError = (error) => {
                        const messageContainer = document.querySelector('#error-message');
                        messageContainer.textContent = error.message;
                    }

                    expressCheckoutElement.on('confirm', async (event) => {
                        const {error: submitError} = await elements.submit();
                        if (submitError) {
                            handleError(submitError);
                            return;
                        }

                        // Create the PaymentIntent and obtain clientSecret
                        const res = await fetch('{{ route('api.core.users.checkout', auth()->user()->id) }}', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                amount: options.amount,
                                currency: options.currency,
                                product: btn.dataset.slug,
                            }),
                        });
                        const {client_secret: clientSecret} = await res.json();

                        const {error} = await stripe.confirmPayment({
                            // `elements` instance used to create the Express Checkout Element
                            elements,
                            // `clientSecret` from the created PaymentIntent
                            clientSecret,
                            confirmParams: {
                                return_url: '{{ route('shop.checkout.confirm') }}',
                            },
                        });

                        if (error) {
                            // This point is only reached if there's an immediate error when
                            // confirming the payment. Show the error to your customer (for example, payment details incomplete)
                            handleError(error);
                        } else {
                            // The payment UI automatically closes with a success animation.
                            // Your customer is redirected to your `return_url`.
                        }
                    });

                }
            })
        })

        document.querySelectorAll('[data-action="checkout-engine"]').forEach(btn => {
            btn.addEventListener('click', e => {
                e.preventDefault()

                $.ajax({
                    url: '{{ route('shop.checkout') }}',
                    method: "POST",
                    data: {
                        slug: "engine",
                        amount: btn.dataset.amount,
                        engine: btn.dataset.slug,
                        currency: btn.dataset.currency,
                    },
                    success: data => {
                        toastr.success(data.message)
                    }
                })
            })
        })
    </script>
@endsection
