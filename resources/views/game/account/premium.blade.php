@extends("game.template")

@section("css")

@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="card shadow-lg">
        <div class="card-header">
            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x my-5 fs-4">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#desc">Description</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#abo">Abonnement</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="desc" role="tabpanel">
                    @if(auth()->user()->premium)
                        <div class="rounded bg-light-success border border-success p-10 mb-10">
                            <div class="d-flex flex-row justify-content-between align-items-center">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="symbol symbol-100px me-5">
                                        <img alt="Logo" src="{{ asset('/storage/icons/premium.png') }}" />
                                    </div>
                                    <div class="d-flex flex-column justify-content-end">
                                        <div class="fs-1 text-dark fw-bold">Vous possédez déjà un abonnement Premium</div>
                                        <div class="fs-5 text-dark fw-bold">Votre abonnement est encore valide pour une durée de 18 jours</div>
                                    </div>
                                </div>
                                <div class="d-flex flex-end">
                                    <form action="{{ route('account.premium.subscribe') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="session-id" value="{{ auth()->user()->subscription->token ?? "" }}">
                                        <input type="hidden" name="action" value="manage">
                                        <button id="checkout-and-portal-button" class="btn btn-success btn-lg" type="submit">Gérer mon abonnement</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="rounded bg-light-danger border border-danger p-10 mb-10">
                            <div class="d-flex flex-row justify-content-between align-items-center">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="symbol symbol-100px me-5">
                                        <img alt="Logo" src="{{ asset('/storage/icons/premium.png') }}" />
                                    </div>
                                    <div class="d-flex flex-column justify-content-end">
                                        <div class="fs-1 text-dark fw-bold">Vous souhaitez souscrire à un abonnement Premium</div>
                                        <div class="fs-5 text-dark fw-bold">La durée de l'abonnement est de 1 mois reconductible à date et résiliable à tous moments.</div>
                                    </div>
                                </div>
                                <div class="d-flex flex-end">
                                    <form action="{{ route('account.premium.subscribe') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="lookup_key" value="premium">
                                        <input type="hidden" name="action" value="subscribe">
                                        <button class="btn btn-danger btn-lg" id="checkout-and-portal-button" type="submit">Souscrire pour 4,90 € / par mois</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                    <x-base.underline
                        title="Description de l'abonnement PREMIUM"
                        size="3"
                        size-text="fs-1" />
                    <div class="d-flex flex-column mb-10">
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-100px me-5">
                                <img src="{{ asset('/storage/icons/hq.png') }}" alt="Carte Interactive">
                            </div>
                            <div class="d-flex flex-column justify-content-end">
                                <div class="fs-1 text-dark fw-bold">Carte Interactive</div>
                                <div class="fs-5 text-dark">
                                    <p>Suivez vos trajets en direct sur la carte pour une meilleure immersion.</p>
                                    <p>Ainsi, chaque page disposant de la fonctionnalité vous affichera automatiquement le ou les engins en cours de voyage. Ces appareils suivront leur trajectoire en temps réel. Immersion garantie !</p>
                                </div>
                            </div>
                        </div>
                        <div class="separator border-2 border-gray-400 my-10"></div>
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-100px me-5">
                                <img src="{{ asset('/storage/icons/financial.png') }}" alt="Carte Interactive">
                            </div>
                            <div class="d-flex flex-column justify-content-end">
                                <div class="fs-1 text-dark fw-bold">MICRO-GESTION AVANCÉE</div>
                                <div class="fs-5 text-dark">
                                    <p>Obtenez davantage d'informations sur les pages de gestion afin de faciliter vos décisions.</p>
                                    <p>Que ce soit sur la gestion des tarifs, l'historique de la demande, ou une facilité d'accès aux informations de l'audit Interne, la gestion devient plus simple grâce aux nouvelles données affichées.</p>
                                </div>
                            </div>
                        </div>
                        <div class="separator border-2 border-gray-400 my-10"></div>
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-100px me-5">
                                <img src="{{ asset('/storage/icons/home.png') }}" alt="Carte Interactive">
                            </div>
                            <div class="d-flex flex-column justify-content-end">
                                <div class="fs-1 text-dark fw-bold">NOUVELLE PAGE D'ACCUEIL</div>
                                <div class="fs-5 text-dark">
                                    <p>À la connexion, ayez un regard sur l'ensemble de votre compagnie ferroviaire grâce à un nouveau tableau de bord sur la page d'accueil.</p>
                                    <p>4 nouveaux emplacements : la carte des trajets en direct sur la page d'accueil, le tableau des prochains départs façon Infogare EVA, les prochaines échéances des emprunts et un nouveau graphique de l'évolution de votre chiffre d'affaires.</p>
                                </div>
                            </div>
                        </div>
                        <div class="separator border-2 border-gray-400 my-10"></div>
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-100px me-5">
                                <img src="{{ asset('/storage/icons/no_pub.png') }}" alt="Carte Interactive">
                            </div>
                            <div class="d-flex flex-column justify-content-end">
                                <div class="fs-1 text-dark fw-bold">PAS DE PUBLICITÉ</div>
                                <div class="fs-5 text-dark">
                                    <p>Fluidifier votre navigation en supprimant la publicité du jeu.</p>
                                    <p>Ainsi, tant que votre compte Premium sera actif, aucune publicité ne sera affichée. Parce que les bandeaux ne se chargent pas, votre navigation sur le jeu devient plus rapide.</p>
                                </div>
                            </div>
                        </div>
                        <div class="separator border-2 border-gray-400 my-10"></div>
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-100px me-5">
                                <img src="{{ asset('/storage/icons/mouvement/location_materiel.png') }}" alt="Carte Interactive">
                            </div>
                            <div class="d-flex flex-column justify-content-end">
                                <div class="fs-1 text-dark fw-bold">LOCATION À VIE</div>
                                <div class="fs-5 text-dark">
                                    <p>Finie la crainte de perdre un engin roulant lors de la fin de la location !</p>
                                    <p>Grâce à cette option, vous pourrez demander à ce qu'une location d'appareil soit automatiquement reconduite jusqu'à ce que vous décidiez de l'arrêter (tant que votre abonnement Premium sera actif). Attention : le contrat de location reste payant et vous serez régulièrement débité du prix de la location pendant la durée du contrat.</p>
                                </div>
                            </div>
                        </div>
                        <div class="separator border-2 border-gray-400 my-10"></div>
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-100px me-5">
                                <img src="{{ asset('/storage/icons/fast-delivery.png') }}" alt="Carte Interactive">
                            </div>
                            <div class="d-flex flex-column justify-content-end">
                                <div class="fs-1 text-dark fw-bold">DÉLAI DE LIVRAISON RÉDUIT</div>
                                <div class="fs-5 text-dark">
                                    <p>En devenant Membre Premium, vos délais de livraison sont réduits de 50 %. Privilège Premium !</p>
                                    <p>Que ce soit l'achat d'un engin roulant, une recherche en attente ou encore un prêt en cours de rédaction, vous passerez devant tout le monde avec une réduction de 50 % des délais de livraison.</p>
                                </div>
                            </div>
                        </div>
                        <div class="separator border-2 border-gray-400 my-10"></div>
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-100px me-5">
                                <img src="{{ asset('/storage/icons/delivery-check.png') }}" alt="Carte Interactive">
                            </div>
                            <div class="d-flex flex-column justify-content-end">
                                <div class="fs-1 text-dark fw-bold">VALIDER UNIQUEMENT LES LIVRAISONS TERMINÉES</div>
                                <div class="fs-5 text-dark">
                                    <p>Vos livraisons d'avions doivent être faites immédiatement, mais vous ne souhaitez pas dépenser vos précieux TPoint en finissant une recherche trop rapidement ?</p>
                                    <p>L'avantage Premium vous permet désormais de finir vos livraisons instantanément, indépendamment du reste de vos tâches en attente. Ainsi, vous pouvez compléter votre flotte selon vos propres critères, à la vitesse de votre choix.</p>
                                </div>
                            </div>
                        </div>
                        <div class="separator border-2 border-gray-400 my-10"></div>
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-100px me-5">
                                <img src="{{ asset('/storage/icons/technician.png') }}" alt="Carte Interactive">
                            </div>
                            <div class="d-flex flex-column justify-content-end">
                                <div class="fs-1 text-dark fw-bold">LA MAINTENANCE AUTOMATIQUE</div>
                                <div class="fs-5 text-dark">
                                    <p>Votre flotte s'use petit à petit mais vous ne souhaitez pas la surveiller constamment pour éviter les incidents ? Bonne nouvelle : vous pouvez désormais réparer automatiquement.</p>
                                    <p>Pour cela, il suffit de cocher la ou les case(s) correspondant à la catégorie d'engins à réparer et ajuster le curseur sur le pourcentage à partir duquel une maintenance sera automatiquement lancée.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="abo" role="tabpanel">
                    @if(!auth()->user()->premium)
                        <div class="rounded bg-light-danger border border-danger p-10 mb-10">
                            <div class="d-flex flex-row justify-content-between align-items-center">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="symbol symbol-100px me-5">
                                        <img alt="Logo" src="{{ asset('/storage/icons/premium.png') }}" />
                                    </div>
                                    <div class="d-flex flex-column justify-content-end">
                                        <div class="fs-1 text-dark fw-bold">Vous souhaitez souscrire à un abonnement Premium</div>
                                        <div class="fs-5 text-dark fw-bold">La durée de l'abonnement est de 1 mois reconductible à date et résiliable à tous moments.</div>
                                    </div>
                                </div>
                                <div class="d-flex flex-end">
                                    <form action="{{ route('account.premium.subscribe') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="lookup_key" value="premium">
                                        <button class="btn btn-danger btn-lg" id="checkout-and-portal-button" type="submit">Souscrire pour 4,90 € / par mois</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', async () => {
            let searchParams = new URLSearchParams(window.location.search);
            if (searchParams.has('session_id')) {
                const session_id = searchParams.get('session_id');
                document.querySelector('[name="session-id"]').setAttribute('value', session_id);
            }
        });
    </script>
@endsection
