<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTechnicentreTasksTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_technicentre_tasks', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['engine_prev', 'engine_cur', 'infra']);
            $table->enum('status', ["plannified", 'progressed', "finished", "canceled"]);
            $table->timestamp('start_at');
            $table->timestamp('end_at')->nullable();

            $table->foreignId('user_technicentre_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_technicentre_staf_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_engine_id')
               ->nullable()
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('gare_equipement_id')
                ->nullable()
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_technicentre_tasks');
    }
}
