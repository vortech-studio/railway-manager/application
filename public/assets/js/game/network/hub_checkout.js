let typeIcon = {
    'hub': {url: '/storage/icons/hub.png', talle: [45,48]},
    'train': {url: '/storage/icons/train_map.png', talle: [32,37]},
    'station': {url: '/storage/icons/train-station.png', talle: [32,37]}
}
function MapInitHub(data, containerDiv) {
    let followMarker = false;
    let coords = {};
    const zoomLevel = 15

    let container = L.DomUtil.get(containerDiv);
    if(container != null) {
        container._leaflet_id = null;
    }
    let map = null;

    const hubsLatLng = [];

    map = L.map(containerDiv).setView([data.gare.latitude, data.gare.longitude], zoomLevel);
    const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    mainLayer.addTo(map);

    const hubOptions = {
        lat: data.gare.latitude,
        lng: data.gare.longitude,
        title: data.gare.name,
        draggable: false,
    };

    let marker = addMarker(hubOptions, map, 'hub');

}

function addMarker(options, map, type = 'hub') {
    const icon = L.icon({
        iconUrl: typeIcon[type].url,
        iconSize:    typeIcon[type].talle, // size of the icon
        iconAnchor:   [16, 18], // point of the icon which will correspond to marker's location
        popupAnchor:  [1, -9]
    })
    const marker = L.marker([options.lat, options.lng], { title: options.title, draggable: options.draggable, icon: icon });
    marker.addTo(map);

    marker.on('dragend', function(event) {
        coords = event.target._latlng;
        showNewCoords(event.target._latlng, event.target);
        if (followMarker) {
            map.setView(event.target._latlng);
        }
    });

    return marker;
}

function addPopup(marker, popupContent) {
    marker.bindPopup('', {'className': 'custom'})
    let popup = marker.getPopup()
    popup.setContent(popupContent)
    marker.openPopup()
}


function showNewCoords(coords, marker) {
    marker
        .bindPopup(`lat: ${coords.lat} - lng: ${coords.lng}`)
        .openPopup();
}
