<?php

namespace App\Models\User;

use App\Models\Core\Rental;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserRental extends Model
{
    public $timestamps = false;

    protected $casts = [
        'date_contract' => 'datetime',
        'next_prlv' => 'datetime',
    ];
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rental()
    {
        return $this->belongsTo(Rental::class);
    }

    public function user_engine()
    {
        return $this->belongsTo(UserEngine::class);
    }

}
