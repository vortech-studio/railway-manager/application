<?php

namespace App\Observer;

use App\Models\User\UserEngine;
use App\Models\User\UserTechnicentreEngine;
use App\Services\Osmose\Engine;

class UserEngineObserver
{
    public function created(UserEngine $userEngine): void
    {
        UserTechnicentreEngine::create([
            "user_engine_id" => $userEngine->id,
            "user_technicentre_id" => $userEngine->user_hub->technicentre->id
        ]);
    }

    public function updated(UserEngine $userEngine): void
    {
    }

    public function deleted(UserEngine $userEngine): void
    {
    }

    public function restored(UserEngine $userEngine): void
    {
    }

    public function forceDeleted(UserEngine $userEngine): void
    {
    }
}
