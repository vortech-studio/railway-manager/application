<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('lignes', function (Blueprint $table) {
            $table->id();
            $table->integer('nb_station');
            $table->decimal('price', 16)->nullable();
            $table->integer('distance')->nullable();
            $table->string('time_min')->default(0);
            $table->bigInteger('station_start_id')->unsigned();
            $table->bigInteger('station_end_id')->unsigned();
            $table->boolean('active')->default(false);
            $table->enum('type_ligne', [
                "ter",
                "tgv",
                "ic",
                "transilien",
            ]);

            $table->foreignId('hub_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('station_start_id')->references('id')->on('gares')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('station_end_id')->references('id')->on('gares')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('lignes');
    }
};
