<?php

namespace App\Imports;

use App\Models\Core\Gares;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class GareImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return Gares|null
     */
    public function model(array $row): ?Gares
    {
        return new Gares([
            "id" => $row["id"],
            "name" => $row["name"],
            "code_uic" => $row["code_uic"],
            "type_gare" => $row["type_gare"],
            "latitude" => $row["latitude"],
            "longitude" => $row["longitude"],
            "region" => $row["region"],
            "pays" => $row["pays"],
            "superficie_infra" => $row["superficie_infra"],
            "superficie_quai" => $row["superficie_quai"],
            "long_quai" => $row["long_quai"],
            "nb_quai" => $row["nb_quai"],
            "commerce" => $row["commerce"],
            "pub" => $row["pub"],
            "parking" => $row["parking"],
            "frequentation" => $row["frequentation"],
            "nb_habitant_city" => $row["nb_habitant_city"],
            "time_day_work" => $row["time_day_work"],
            "created_at" => $row["created_at"],
            "updated_at" => $row["updated_at"],
        ]);
    }
}
