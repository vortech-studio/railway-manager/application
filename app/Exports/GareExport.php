<?php

namespace App\Exports;

use App\Models\Core\Gares;
use Maatwebsite\Excel\Concerns\FromCollection;

class GareExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection(): \Illuminate\Support\Collection
    {
        return Gares::all();
    }
}
