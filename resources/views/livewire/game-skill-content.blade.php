<div id="gameSkillContent" class="bg-gray-400 border border-2 border-primary rounded-2 p-5">
    <div class="d-flex flex-wrap justify-content-center align-items-center">
        @foreach($skills as $skill)
            <livewire:game-skill-item :skill="$skill"/>
        @endforeach
    </div>
</div>
