<?php

namespace App\Http\Controllers\Game\Compagnie;

use App\Http\Controllers\Controller;
use App\Models\User\UserPlanning;

class CompagnieController extends Controller
{
    public function __invoke()
    {
        return view('game.compagnie.index', [
            'infoCompagnie' => $this->infoCompagnie()
        ]);
    }

    private function infoCompagnie()
    {
        return collect([
            "CASeven" => $this->CASeven(),
            "nb_passenger" => $this->NbPassenger(),
            "kilometrage" => $this->calcKilometrage()
        ]);
    }

    private function CASeven()
    {
        $sum = 0;
        foreach (auth()->user()->hubs()->get() as $hub) {
            $sum += $hub->CAseven();
        }

        return $sum;
    }

    private function NbPassenger()
    {
        $plannings = UserPlanning::where('user_id', auth()->user()->id)->get();
        $count = 0;

        foreach ($plannings as $planning) {
            $count += $planning->passengers()->count('nb_passengers');
        }

        return $count;
    }

    private function calcKilometrage()
    {
        $lignes = auth()->user()->lignes()->get();
        $sum = 0;

        foreach ($lignes as $ligne) {
            $sum += $ligne->ligne->distance;
        }

        return $sum;
    }
}
