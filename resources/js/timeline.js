$(function(){
    let inputs = $('.input');
    let paras = $('.description-flex-container').find('p');
    inputs.click(function(){
        let t = $(this),
            ind = t.index(),
            matchedPara = paras.eq(ind);

        t.add(matchedPara).addClass('active');
        inputs.not(t).add(paras.not(matchedPara)).removeClass('active');
    });
});

