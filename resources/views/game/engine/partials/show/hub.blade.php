<div class="card card-flush shadow-lg">
    <div class="card-header">
        <div class="card-title align-items-center">
            <img src="{{ asset('/storage/logos/logo_'.$engine->engine->type_train.'.svg') }}" class="w-35px me-3" alt="">
            <span class="me-3">{{ $engine->engine->name }} - {{ $engine->number }} | </span>
            <span>
                    <span class="badge bg-amber-500 text-white me-3">Hub</span>
                    {{ $engine->user_hub->hub->gare->name }} / {{ $engine->user_hub->hub->gare->region }} / {{ $engine->user_hub->hub->gare->pays }}
                </span>
        </div>
    </div>
    <div class="card-body">
        @if($engine->engine->type_engine == 'automotrice')
            <div class="d-flex flex-row w-100 justify-content-center align-items-baseline bg-white shadow-lg mb-10 p-10">
                <div class="p-auto">
                    @for($i=0; $i < $engine->engine->technical->nb_wagon; $i++)
                        <img src="/storage/engines/{{ $engine->engine->type_engine }}/{{ \Illuminate\Support\Str::upper(Str::slug($engine->engine->name)) }}-{{ $i }}.gif" class="my-5" alt="">
                    @endfor
                </div>
            </div>
            <div class="d-flex flex-row justify-content-around align-items-center mb-10">
                <div class="rounded bg-blue-600 text-white p-5 me-5 w-200px">
                    <div class="d-flex flex-column justify-content-center align-items-end">
                        <span class="fw-bold fs-2x">{{ $engine->max_runtime_engine }} Km</span>
                        <span>Rayon d'action de l'engin roulant</span>
                    </div>
                </div>
                <div class="rounded bg-amber-600 text-white p-5 me-5 w-200px">
                    <div class="d-flex flex-column justify-content-center align-items-end">
                        <span class="fw-bold fs-2x">{{ $engine->engine->velocity }} Km/h</span>
                        <span>Vitesse Max</span>
                    </div>
                </div>
                <div class="rounded bg-indigo-600 text-white p-5 me-5 w-200px">
                    <div class="d-flex flex-column justify-content-center align-items-end">
                        <span class="fw-bold fs-2x">{{ $engine->engine->nb_passager }}</span>
                        <span>Nombre de siège disponible</span>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-sm-12 col-lg-6 mb-5">
                    <img src="/storage/engines/{{ $engine->type_engine }}/{{ $engine->image }}" class="w-135px my-5 mx-20" alt="">
                </div>
                <div class="col-sm-12 col-lg-6 mb-5">
                    <div class="d-flex flex-row justify-content-around align-items-center mb-10">
                        <div class="rounded bg-blue-600 text-white p-5 me-5 w-200px">
                            <div class="d-flex flex-column justify-content-center align-items-end">
                                <span class="fw-bold fs-2x">{{ $engine->max_runtime_engine }} Km</span>
                                <span>Rayon d'action de l'engin roulant</span>
                            </div>
                        </div>
                        <div class="rounded bg-amber-600 text-white p-5 me-5 w-200px">
                            <div class="d-flex flex-column justify-content-center align-items-end">
                                <span class="fw-bold fs-2x">{{ $engine->engine->velocity }} Km/h</span>
                                <span>Vitesse Max</span>
                            </div>
                        </div>
                        <div class="rounded bg-indigo-600 text-white p-5 me-5 w-200px">
                            <div class="d-flex flex-column justify-content-center align-items-end">
                                <span class="fw-bold fs-2x">{{ $engine->engine->nb_passager }}</span>
                                <span>Nombre de siège disponible</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Assigner à un hub</h3>
            </div>
            <div class="card-body">
                @foreach(\App\Models\User\UserHub::where('active', 1)->get() as $hub)
                   <div class="d-flex flex-column p-5 mb-5 shadow-lg rounded-2 {{ $hub->engines()->where('id', $engine->id)->count() == 0 ? 'bg-light-warning' : 'd-none' }}" @if($hub->engines()->where('id', $engine->id)->count() != 0) data-bs-toggle="tooltip" title="Assignation impossible"  @endif>
                       <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                           <div class="d-flex flex-row">
                               <div class="badge badge-warning">Hub</div>
                               <div class="fs-4 text-dark fw-bolder ms-5">{{ $hub->hub->gare->name }} / {{ Str::ucfirst(Str::lower($hub->hub->gare->region)) }} / {{ $hub->hub->gare->pays }}</div>
                           </div>
                           @if($hub->engines()->where('id', $engine->id)->count() == 0)
                           <button class="btn btn-sm btn-primary rounded-4" data-action="TransferToHub" data-prev="{{ $engine->user_hub->id }}" data-to="{{ $hub->id }}">Assigner à ce hub</button>
                           @endif
                       </div>
                       <div class="d-flex flex-row align-items-center mt-10">
                           <i class="fa-solid fa-arrow-up text-success fs-2"></i>
                           <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                           <span>
                               <span class="">Chiffre d'affaire :</span>
                               <span class="fw-bolder">{{ eur($hub->calcCA()) }}</span>
                           </span>
                           <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                           <span>
                           <span class="">Bénéfice :</span>
                               <span class="fw-bolder">{{ eur($hub->calcBenefice()) }}</span>
                           </span>
                       </div>
                        </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

