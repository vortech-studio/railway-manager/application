<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLigneSkillsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_ligne_skills', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->boolean('level');
            $table->integer('level_actual')->default(0);
            $table->integer('level_max')->nullable();
            $table->float('level_multiplicator')->nullable();
            $table->string('action');
            $table->string("action_base_value")->nullable();
            $table->float('action_value');
            $table->string('action_type');
            $table->float('coast_base');
            $table->integer('time_base')->default(0);
            $table->json('requirements')->nullable();

            $table->foreignId('user_ligne_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_ligne_skills');
    }
}
