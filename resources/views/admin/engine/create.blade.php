@extends("admin.template")

@section("css")
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Création d'un engins",
        'action' => true,
        'data' => [
            [
                'url' => back()->getTargetUrl(),
                'title' => "<i class='la la-arrow-left me-2'></i> Retour",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <form action="{{ route('admin.engine.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-9">
                <div class="card card-flush mb-10">
                    <div class="card-header">
                        <ul class="nav nav-tabs nav-line-tabs nav-stretch fs-6 border-0">
                            <li class="nav-item">
                                <a class="nav-link active" data-bs-toggle="tab" href="#general">Générales</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#technique">Technique</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#marchandise">Marchandise</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="tab_info">
                            <div class="tab-pane fade show active" id="general" role="tabpanel">
                                <x-form.input
                                    name="name"
                                    label="Désignation du matériel roulant"
                                    required="true"
                                    class="form-control-lg" />
                                <div class="row mb-10">
                                    <div class="col-md-4">
                                        <x-form.select
                                            name="type_train"
                                            label="Type de train"
                                            required="true"
                                            :datas=\App\Models\Core\Engine::getDataTypeTrain() />
                                    </div>
                                    <div class="col-md-4">
                                        <x-form.select
                                            name="type_engine"
                                            label="Type d'engin"
                                            required="true"
                                            :datas=\App\Models\Core\Engine::getDataTypeEngine() />
                                    </div>
                                    <div class="col-md-4">
                                        <x-form.select
                                            name="type_energy"
                                            label="Type d'énergie"
                                            required="true"
                                            :datas=\App\Models\Core\Engine::getDataTypeEnergy() />
                                    </div>
                                </div>
                                <x-form.input-file
                                    name="image"
                                    label="Image du matériel roulant"
                                    required="true"
                                    class="form-control-lg" />

                                <x-form.switch
                                    name="in_game"
                                    label="Disponible en jeu"
                                    value="1"
                                    check="checked" />

                                <x-form.switch
                                    name="in_shop"
                                    label="Disponible en boutique"
                                    value="1"
                                    check="" />
                            </div>
                            <div class="tab-pane fade" id="technique" role="tabpanel">
                                <x-base.underline
                                    size="3"
                                    size-text="fs-2"
                                    class="mb-10"
                                    title="Caractéristiques générales" />
                                <div class="row align-items-center">
                                    <div class="row col-md-3">
                                        <x-form.input
                                            name="longueur"
                                            label="Longueur"
                                            required="true"
                                            class="form-control-lg" />
                                    </div>
                                    <div class="row col-md-3">
                                        <x-form.input
                                            name="largeur"
                                            label="Largeur"
                                            required="true"
                                            class="form-control-lg" />
                                    </div>
                                    <div class="row col-md-3">
                                        <x-form.input
                                            name="hauteur"
                                            label="Hauteur"
                                            required="true"
                                            class="form-control-lg" />
                                    </div>
                                </div>
                                <div class="separator border border-1 border-gray-400 my-5"></div>
                                <x-base.underline
                                    size="3"
                                    size-text="fs-2"
                                    class="mb-10"
                                    title="Motorisation" />
                                <div class="row align-items-center">
                                    <div class="col-md-4">
                                        <x-form.select
                                            name="essieux"
                                            label="Type d'essieux"
                                            required="true"
                                            :datas=\App\Models\Core\Engine::getDataTypeEssieu() />
                                    </div>
                                    <div class="col-md-4">
                                        <x-form.input
                                            name="max_speed"
                                            label="Vitesse maximale"
                                            required="true"
                                            class="form-control-lg" />
                                    </div>
                                    <div class="col-md-4">
                                        <x-form.input
                                            name="poids"
                                            label="Poids"
                                            required="true"
                                            class="form-control-lg" />
                                    </div>
                                    <div class="col-md-4">
                                        <x-form.input-group
                                            name="traction_force"
                                            symbol="<button type='button' class='btn btn-icon btn-sm btn-outline-info' data-bs-toggle='tooltip' title='Calculer' onclick='calculForceTraction()'><i class='fa-solid fa-calculator'></i></button>"
                                            placement="right"
                                            label="Force de traction"
                                            required="true"
                                            class="form-control-lg" />

                                    </div>
                                    <div class="col-md-4">
                                        <x-form.select
                                            name="type_motor"
                                            label="Type de moteur"
                                            required="true"
                                            :datas=\App\Models\Core\Engine::getDataTypeMotor() />
                                    </div>
                                    <div class="col-md-4">
                                        <x-form.input
                                            name="puissance"
                                            label="Puissance (KW)"
                                            required="true"
                                            class="form-control-lg" />
                                    </div>
                                    <div class="col-md-4 showGasoil d-none">
                                        <x-form.input
                                            name="gasoil"
                                            label="Gasoil (L)"
                                            class="form-control-lg" />
                                    </div>
                                </div>
                                <div class="separator border border-1 border-gray-400 my-5"></div>
                                <x-base.underline
                                    size="3"
                                    size-text="fs-2"
                                    class="mb-10"
                                    title="Vie de l'engin" />
                                <div class="row align-items-center">
                                    <div class="col-md-6">
                                        <x-form.input-date
                                            name="date_mise_service"
                                            label="Date de mise en service"
                                            required="true"
                                            class="form-control-lg" />
                                    </div>
                                    <div class="col-md-6">
                                        <x-form.input-date
                                            name="date_mise_hors_service"
                                            label="Date de mise hors service"
                                            required="true"
                                            value="{{ \Carbon\Carbon::now()->format('Y') }}"
                                            class="form-control-lg" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="marchandise" role="tabpanel">
                                <x-base.underline
                                    size="3"
                                    size-text="fs-2"
                                    class="mb-10"
                                    title="Marchandise transporté" />
                                <x-form.select
                                    name="type_marchandise"
                                    label="Type de marchandise"
                                    required="true"
                                    value="none"
                                    :datas=\App\Models\Core\Engine::getDataTypeMarchandise() />
                                <div class="showNbMarchandise d-none">
                                    <x-form.input
                                        name="nb_type_marchandise"
                                        label="Nombre de marchandise"
                                        class="form-control-lg" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex flex-end">
                        <x-form.button />
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("js")
    <script type="text/javascript">
        let divCalcSurgace = document.querySelector("[data-content='calcSurface']")

        document.querySelector('[name="type_motor"]').addEventListener('change', e => {
            console.log(e.target.value)
            if(e.target.value === 'diesel') {
                document.querySelector('.showGasoil').classList.remove('d-none')
            } else {
                document.querySelector('.showGasoil').classList.add('d-none')
            }
        })

        document.querySelector('[name="type_marchandise"]').addEventListener('change', e => {
            if(e.target.value === 'none') {
                document.querySelector('.showNbMarchandise').classList.add('d-none')
            } else {
                document.querySelector('.showNbMarchandise').classList.remove('d-none')
            }
        })

        function calculForceTraction() {
            let masse = document.querySelector("[name='poids']").value
            document.querySelector("[name='traction_force']").value = (masse * 0.035).toFixed(2)
        }

    </script>
@endsection
