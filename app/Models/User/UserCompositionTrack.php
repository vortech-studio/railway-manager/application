<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserCompositionTrack extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function user_composition()
    {
        return $this->belongsTo(UserComposition::class);
    }

    public function user_engine()
    {
        return $this->belongsTo(UserEngine::class);
    }
}
