<?php

namespace App\Models\User;

use App\Models\Core\Hub;
use App\Models\Core\Mouvement;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserHub extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    protected $casts = [
        'date_achat' => 'datetime',
    ];

    protected $appends = ['count_ligne'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function hub()
    {
        return $this->belongsTo(Hub::class);
    }

    public function lignes()
    {
        return $this->hasMany(UserLigne::class);
    }


    public function mouvements()
    {
        return $this->hasMany(Mouvement::class);
    }

    public function plannings()
    {
        return $this->hasMany(UserPlanning::class);
    }

    public function engines()
    {
        return $this->hasMany(UserEngine::class);
    }

    public function livraisons(): HasMany
    {
        return $this->hasMany(UserDelivery::class);
    }

    public function technicentre()
    {
        return $this->hasOne(UserTechnicentre::class);
    }

    public function incidents()
    {
        return $this->hasMany(UserIncident::class);
    }

    public function skills()
    {
        return $this->hasMany(UserHubSkill::class);
    }

    public function getCountLigneAttribute()
    {
        return $this->lignes()->where('active', true)->count();
    }

    public static function calcNbSalarieIV(Hub $hub)
    {
        $calc = $hub->gare->superficie_infra / 1000;
        return intval(round($calc, 0));
    }

    public static function calcNbSalarieIR(Hub $hub)
    {
        $calc = ($hub->gare->superficie_quai / 1000) * 2;
        return intval(round($calc, 0));
    }

    public function calcCA(Carbon $from = null, Carbon $to = null)
    {
        if($from && $to) {
            return $this->mouvements()
                ->where('type_account', 'revenue')
                ->where('user_hub_id', $this->id)
                ->whereBetween('created_at', [$from->startOfDay(), $to->endOfDay()])
                ->sum('amount');
        } else {
            return $this->mouvements()
                ->where('type_account', 'revenue')
                ->where('user_hub_id', $this->id)
                ->sum('amount');
        }
    }

    public function calcBenefice(Carbon $from = null, Carbon $to = null)
    {
        if($from && $to) {
            $billetterie = $this->mouvements()
                ->where('type_mvm', 'billetterie')
                ->where('user_hub_id', $this->id)
                ->whereBetween('created_at', [$from->startOfDay(), $to->endOfDay()])
                ->sum('amount');

            $rent_trajet = $this->mouvements()
                ->where('type_mvm', 'rent_trajet_aux')
                ->where('user_hub_id', $this->id)
                ->whereBetween('created_at', [$from->startOfDay(), $to->endOfDay()])
                ->sum('amount');

            $cout = $this->mouvements()
                ->where('type_mvm', 'cout_trajet')
                ->where('user_hub_id', $this->id)
                ->whereBetween('created_at', [$from->startOfDay(), $to->endOfDay()])
                ->sum('amount');

        } else {
            $billetterie = $this->mouvements()
                ->where('type_mvm', 'billetterie')
                ->where('user_hub_id', $this->id)
                ->sum('amount');

            $rent_trajet = $this->mouvements()
                ->where('type_mvm', 'rent_trajet_aux')
                ->where('user_hub_id', $this->id)
                ->sum('amount');

            $cout = $this->mouvements()
                ->where('type_mvm', 'cout_trajet')
                ->where('user_hub_id', $this->id)
                ->sum('amount');

        }
        return ($billetterie + $rent_trajet) - $cout;
    }

    public function calcFraisCarburant(Carbon $from = null, Carbon $to = null)
    {
        if ($from && $to) {
            return $this->mouvements()
                ->where('type_mvm', 'gasoil')
                ->where('user_hub_id', $this->id)
                ->whereBetween('created_at', [$from->startOfDay(), $to->endOfDay()])
                ->sum('amount');
        } else {
            return $this->mouvements()
                ->where('type_mvm', 'gasoil')
                ->where('user_hub_id', $this->id)
                ->sum('amount');
        }
    }

    public function calcFraisElectrique(Carbon $from = null, Carbon $to = null)
    {
        if ($from && $to) {
            return $this->mouvements()
                ->where('type_mvm', 'electricite')
                ->where('user_hub_id', $this->id)
                ->whereBetween('created_at', [$from->startOfDay(), $to->endOfDay()])
                ->sum('amount');
        } else {
            return $this->mouvements()
                ->where('type_mvm', 'electricite')
                ->where('user_hub_id', $this->id)
                ->sum('amount');
        }
    }

    public function calcTaxes(Carbon $from = null, Carbon $to = null)
    {
        if ($from && $to) {
            return $this->mouvements()
                ->where('type_mvm', 'taxe')
                ->where('user_hub_id', $this->id)
                ->whereBetween('created_at', [$from->startOfDay(), $to->endOfDay()])
                ->sum('amount');
        } else {
            return $this->mouvements()
                ->where('type_mvm', 'taxe')
                ->where('user_hub_id', $this->id)
                ->sum('amount');
        }
    }

    public function CAseven($type = null)
    {
        if ($type) {
            return $this->mouvements()
                ->where('user_hub_id', $this->id)
                ->where('type_mvm', $type)
                ->whereBetween('created_at', [now()->subDays(7)->startOfDay(), now()->endOfDay()])
                ->sum('amount');
        } else {
            return $this->mouvements()
                ->where('user_hub_id', $this->id)
                ->where('type_account', 'revenue')
                ->whereBetween('created_at', [now()->subDays(7)->startOfDay(), now()->endOfDay()])
                ->sum('amount');
        }
    }

    public function calcResultatSeven()
    {
        $ca = $this->CAseven();
        $cout = $this->CAseven('electricite') + $this->CAseven('gasoil') + $this->CAseven('taxe') + $this->CAseven('maintenance_vehicule') + $this->CAseven('cout_trajet');

        return $ca - $cout;
    }

    public function calcRatioPerformanceLignes()
    {
        $nb_week = 5;
        $sum = 0;
        foreach ($this->lignes as $ligne) {
            for ($i=0; $i < $nb_week; $i++) {
                $sum += $ligne->calcCA($i);
            }
        }
        return $sum / $nb_week;
    }

    public function calcRatioPerformance()
    {
        $yesteday_result = $this->calcBenefice(now()->subDay(), now()->subDay());
        $ref_result = $this->calcBenefice(now()->subDays(2), now()->subDays(2));

        if($yesteday_result > $ref_result) {
            return "<i class='fa-solid fa-arrow-up text-success fs-2'></i>";
        } elseif($yesteday_result == $ref_result) {
            return "<i class='fa-solid fa-arrow-right text-warning fs-2'></i>";
        } else {
            return "<i class='fa-solid fa-arrow-down text-danger fs-2'></i>";
        }
    }

    public function calcSumPassengers($type_passenger = null, Carbon $from = null, Carbon $to = null)
    {
        $sum = 0;

        if($from && $to) {
            if($type_passenger == 'first') {
                foreach ($this->plannings()->whereBetween('date_depart', [$from->startOfDay(), $to->endOfDay()])->get() as $planning) {
                    $sum += $planning->passengers()->where('type', 'first')->sum('nb_passengers');
                }
            } elseif($type_passenger == 'second') {
                foreach ($this->plannings()->whereBetween('date_depart', [$from->startOfDay(), $to->endOfDay()])->get() as $planning) {
                    $sum += $planning->passengers()->where('type', 'second')->sum('nb_passengers');
                }
            } else {
                foreach ($this->plannings()->whereBetween('date_depart', [$from->startOfDay(), $to->endOfDay()])->get() as $planning) {
                    $sum += $planning->passengers()
                        ->where('type', 'first')
                        ->where('type', 'second')
                        ->sum('nb_passengers');
                }
            }
        } else {
            if($type_passenger == 'first') {
                foreach ($this->plannings() as $planning) {
                    $sum += $planning->passengers()->where('type', 'first')->sum('nb_passengers');
                }
            } elseif($type_passenger == 'second') {
                foreach ($this->plannings() as $planning) {
                    $sum += $planning->passengers()->where('type', 'second')->sum('nb_passengers');
                }
            } else {
                foreach ($this->plannings() as $planning) {
                    $sum += $planning->passengers()
                        ->where('type', 'first')
                        ->where('type', 'second')
                        ->sum('nb_passengers');
                }
            }
        }

        return $sum;
    }

    public function simulateSelling()
    {
        return ($this->hub->price) / 2 + $this->calcResultatSeven();
    }
}
