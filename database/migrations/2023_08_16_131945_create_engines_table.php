<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('engines', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->nullable();
            $table->enum('type_train', ['ter', 'tgv', 'ic', 'other']);
            $table->enum('type_engine', [
                'motrice',
                'remorque',
                'automotrice',
            ]);
            $table->enum('type_energy', [
                'diesel',
                'electrique',
                'hybride',
                'vapeur',
                'aucun'
            ])->nullable();
            $table->integer('velocity');
            $table->integer('nb_passager');
            $table->decimal('price_achat', 16);
            $table->boolean('in_reduction')->default(false);
            $table->integer('percent_reduction')->nullable();
            $table->decimal('price_maintenance', 16);
            $table->time('duration_maintenance')->default('00:00:00');
            $table->decimal('price_location', 16);
            $table->string('image')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('engines');
    }
};
