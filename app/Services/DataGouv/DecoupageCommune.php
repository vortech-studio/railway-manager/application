<?php

namespace App\Services\DataGouv;

class DecoupageCommune
{
    public function getPopulation(object $gare = null, string $freq = null, string $name_gare = null)
    {
        if($gare) {
            try {
                $call = \Http::withoutVerifying()
                    ->get('https://geo.api.gouv.fr/communes?nom='.$gare->gare_alias_libelle_noncontraint.'&fields=code,nom,population')
                    ->object();

                if (isset($call[0])) {
                    return $call[0]->population;
                } else {
                    return $this->generatePopulation($freq);
                }

            } catch (\Exception $exception) {
                return $exception->getMessage();
            }
        } else {
            try {
                $call = \Http::withoutVerifying()
                    ->get('https://geo.api.gouv.fr/communes?nom='.$name_gare.'&fields=code,nom,population')
                    ->object();

                if (isset($call[0])) {
                    return $call[0]->population;
                } else {
                    return $this->generatePopulation($freq);
                }

            } catch (\Exception $exception) {
                return $exception->getMessage();
            }
        }
    }

    protected function generatePopulation($freq): float|int
    {
        return intval($freq) * rand(2, 6);
    }
}
