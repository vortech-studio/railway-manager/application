@extends("vigimat.template")

@section("css")

@endsection

@section("content")

    <div class="row mb-5">
        <div class="col-sm-12 col-lg-4">
            <div class="d-flex flex-row align-items-center rounded rounded-bottom-0 bg-gray-400 p-5 ribbon ribbon-top ribbon-vertical">
                <div class="ribbon-label bg-{{ $osmose_engine->operationnal->color }}">
                    {{ $osmose_engine->operationnal->value }}
                </div>
                <div class="symbol symbol-70px me-3">
                    <img src="{{ asset('/storage/icons/train.png') }}" alt="">
                </div>
                <div class="d-flex flex-column">
                    <div class="fw-bolder fs-1 text-color-eva" data-vue="indetifiant"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></div>
                    <div class="fs-4" data-vue="nameEngine"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-8">
            <div class="nav d-flex flex-wrap justify-content-center align-items-center bg-white rounded rounded-bottom-0 pt-2">
                <a href="#overview" class="nav-item nav-link active fs-6 text-active-light text-black bg-active-primary rounded-2" data-bs-toggle="tab">Aperçu</a>
                <a href="#technical" class="nav-item nav-link fs-6 text-active-light text-black bg-active-primary rounded-2" data-bs-toggle="tab">Technique</a>
                <a href="#usage" class="nav-item nav-link fs-6 text-active-light text-black bg-active-primary rounded-2" data-bs-toggle="tab">Utilisations</a>
                <a href="#status" class="nav-item nav-link fs-6 text-active-light text-black bg-active-primary rounded-2" data-bs-toggle="tab">Status</a>
                <a href="#prevent" class="nav-item nav-link fs-6 text-active-light text-black bg-active-primary rounded-2" data-bs-toggle="tab">Maintenance préventive</a>
                <a href="#intervention" class="nav-item nav-link fs-6 text-active-light text-black bg-active-primary rounded-2" data-bs-toggle="tab">Interventions</a>
                <a href="#equipements" class="nav-item nav-link fs-6 text-active-light text-black bg-active-primary rounded-2" data-bs-toggle="tab">Equipements</a>
                <a href="#finances" class="nav-item nav-link fs-6 text-active-light text-black bg-active-primary rounded-2" data-bs-toggle="tab">Finances</a>
            </div>
            <div class="d-flex flex-row justify-content-between align-items-center bg-white rounded rounded-top-0 pb-5 pt-2">
                <div class="ms-2">
                    <span class="fw-bolder">Status OSMOSE:</span> <span class="" data-vue="osmose-status"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </div>
                <div>
                    <button id="btnAuditMaterial" class="btn btn-sm btn-primary rounded-5 me-5">Audit du matériel</button>
                    <a href="{{ route('vigimat.engine') }}" class="btn btn-sm btn-secondary rounded-5 me-5">Retour</a>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="overview" role="tabpanel">
            <div class="card shadow-lg">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-lg-4">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                                    <div class="symbol symbol-70px me-3">
                                        <img src="{{ asset('/storage/icons/train.png') }}" alt="">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fs-3 fw-bolder">Type de train</div>
                                        <div class="fs-4" data-vue="typeTrain"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></div>
                                    </div>
                                </div>
                                <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                                    <div class="symbol symbol-70px me-3">
                                        <img src="{{ asset('/storage/icons/train_engine.png') }}" alt="">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fs-3 fw-bolder">Type de composant</div>
                                        <div class="fs-4" data-vue="typeEngin"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></div>
                                    </div>
                                </div>
                                <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                                    <div class="symbol symbol-70px me-3">
                                        <img src="{{ asset('/storage/icons/energy-system.png') }}" alt="">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fs-3 fw-bolder">Energie</div>
                                        <div class="fs-4" data-vue="typeEnergy"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></div>
                                    </div>
                                </div>
                                <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                                    <div class="symbol symbol-70px me-3" data-vue="statusIcon">
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fs-3 fw-bolder">Status</div>
                                        <div class="fs-4" data-vue="statusText"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></div>
                                    </div>
                                </div>
                                <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                                    <div class="symbol symbol-70px me-3">
                                        <img src="{{ asset('/storage/icons/train_buy.png') }}" alt="">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fs-3 fw-bolder">Date d'achat</div>
                                        <div class="fs-4" data-vue="dateAchat"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-8">
                            <div class="d-flex flex-row justify-content-around align-items-center mb-10">
                                <div class="d-flex flex-row align-items-center bg-red-400 text-white rounded shadow-sm me-2 mb-3 p-5">
                                    <div class="symbol symbol-70px me-3">
                                        <img src="{{ asset('/storage/icons/station_hub.png') }}" alt="">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fs-5 fw-bolder">Hub D'appartenance</div>
                                        <div class="fs-3" data-vue="hub"><span class="spinner-border spinner-border w-25px h-25px align-middle ms-2"></span></div>
                                    </div>
                                </div>
                                <div class="d-flex flex-row align-items-center bg-cyan-700 text-white rounded shadow-sm me-2 mb-3 p-5">
                                    <div class="symbol symbol-70px me-3">
                                        <img src="{{ asset('/storage/icons/ligne.png') }}" alt="">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fs-5 fw-bolder">Nombre de trajet effectuer</div>
                                        <div class="fs-3" data-vue="nbTravel"><span class="spinner-border spinner-border w-25px h-25px align-middle ms-2"></span></div>
                                    </div>
                                </div>
                                <div class="d-flex flex-row align-items-center bg-cyan-700 text-white rounded shadow-sm me-2 mb-3 p-5">
                                    <div class="symbol symbol-70px me-3">
                                        <img src="{{ asset('/storage/icons/depot_color.png') }}" alt="">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <div class="fs-5 fw-bolder">Dernière maintenance curative</div>
                                        <div class="fs-3" data-vue="latestCurative"><span class="spinner-border spinner-border w-25px h-25px align-middle ms-2"></span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-row">
                                <div class="d-flex flex-column w-45 me-3">
                                    <div class="d-flex flex-row justify-content-between bg-gray-200 shadow-sm rounded align-items-center fs-5 p-2 mb-5">
                                        <span>Prix d'achat:</span>
                                        <span class="text-red-500 fw-bolder" data-vue="priceAchat"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between bg-gray-200 shadow-sm rounded align-items-center fs-5 p-2 mb-5">
                                        <span>Résultat cumulé:</span>
                                        <span class="text-green-600 fw-bolder" data-vue="resultatCumul"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between bg-gray-200 shadow-sm rounded align-items-center fs-5 p-2 mb-5">
                                        <span>Rentabilité:</span>
                                        <span class="text-red-500 fw-bolder" data-vue="rentability"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </div>
                                    <div class="bg-gray-200 shadow-sm rounded p-5 mb-3">
                                        <div class="fw-bolder mb-1">Répartition des sièges <span class="spinner-border spinner-border-sm align-middle ms-2"></span></div>
                                        <div class="h-20px progress-stacked rounded" data-vue="repartitionSiege">
                                            <div data-bs-toggle="tooltip" data-placement="bottom" title="2eme classe" class="progress h-20px" role="progressbar" aria-label="Segment one" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                <div class="progress-bar"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center w-100">
                                    <div id="map" class="w-100 h-100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="technical" role="tabpanel">
            <div class="row">
                <div class="col-sm-12 col-lg-4">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Type de train</div>
                                <div class="fs-4" data-vue="typeTrain">{{ $engine->engine->type_train_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train_engine.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Type de composant</div>
                                <div class="fs-4" data-vue="typeEngin">{{ $engine->engine->type_engine_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/energy-system.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Energie</div>
                                <div class="fs-4" data-vue="typeEnergy">{{ $engine->engine->type_engine_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3" data-vue="statusIcon">
                                <i class="fa-solid fa-{{ $engine->status_icon }} text-{{ $engine->status_color }} fs-3x"></i>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Status</div>
                                <div class="fs-4" data-vue="statusText">{{ $engine->status_text }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train_buy.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Date d'achat</div>
                                <div class="fs-4" data-vue="dateAchat">{{ $engine->date_achat_format }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8">
                    <div class="d-flex align-items-baseline rounded bg-white scroll scroll-x p-5 mb-10">
                        @for($i=0; $i <= $engine->engine->technical->nb_wagon -1; $i++)
                            <img src="{{ asset('/storage/engines/automotrice/'.$engine->engine->slug.'-'.$i.'.gif') }}" class="w-120px" alt="">
                        @endfor
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-6 mb-10">
                            <div class="card shadow-lg h-100">
                                <div class="card-header">
                                    <div class="card-title">Information de performance</div>
                                </div>
                                <div class="card-body">
                                    <div class="d-flex flex-row justify-content-between rounded bg-gray-200 fs-5 p-5 mb-5">
                                        <span>Vitesse Maximal</span>
                                        <div class="fw-bolder">{{ $engine->engine->technical->max_speed }} Km/h</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between rounded bg-gray-200 fs-5 p-5 mb-5">
                                        <span>Force de Traction</span>
                                        <div class="fw-bolder">{{ $engine->engine->technical->traction_force }} Kn</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 mb-10">
                            <div class="card shadow-lg">
                                <div class="card-header">
                                    <div class="card-title">Motorisation</div>
                                </div>
                                <div class="card-body">
                                    <div class="d-flex flex-row justify-content-between rounded bg-gray-200 fs-5 p-5 mb-5">
                                        <span>Type</span>
                                        <div class="fw-bolder">{{ $engine->engine->technical->type_motor }}</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between rounded bg-gray-200 fs-5 p-5 mb-5">
                                        <span>Puissance </span>
                                        <div class="fw-bolder">{{ $engine->engine->technical->power_motor }} Kw</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between rounded bg-gray-200 fs-5 p-5 mb-5">
                                        <span>Essieux Principal </span>
                                        <div class="fw-bolder">{{ $engine->engine->technical->essieux }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 mb-10">
                            <div class="card shadow-lg">
                                <div class="card-header">
                                    <div class="card-title">Performance & Entretien</div>
                                </div>
                                <div class="card-body">
                                    <div class="d-flex flex-row justify-content-between rounded bg-gray-200 fs-5 p-5 mb-5">
                                        <span>Charge Maximal transporté</span>
                                        <div class="fw-bolder">{{ $engine->other_info['maxCharged'] }}</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between rounded bg-gray-200 fs-5 p-5 mb-5">
                                        <span>Ancienneté</span>
                                        <div class="fw-bolder">{{ $engine->calcAncienneteEngine() }} / 5</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between rounded bg-gray-200 fs-5 p-5 mb-5">
                                        <span>Usure Global</span>
                                        <div class="fw-bolder">{{ $engine->calcUsureEngine() }} %</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 mb-10">
                            <div class="card shadow-lg h-100">
                                <div class="card-header">
                                    <div class="card-title">Maintenance Théorique</div>
                                </div>
                                <div class="card-body">
                                    <div class="d-flex flex-row justify-content-between rounded bg-gray-200 fs-5 p-5 mb-5">
                                        <span>Temps de maintenance Préventive</span>
                                        <div class="fw-bolder">{{ convertMinuteToHours($engine->engine->compositions()->sum('maintenance_check_time')) }}</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between rounded bg-gray-200 fs-5 p-5 mb-5">
                                        <span>Temps de maintenance Curative</span>
                                        <div class="fw-bolder">{{ convertMinuteToHours($engine->engine->compositions()->sum('maintenance_repar_time')) }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="usage" role="tabpanel">
            <div class="row">
                <div class="col-sm-12 col-lg-4">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Type de train</div>
                                <div class="fs-4" data-vue="typeTrain">{{ $engine->engine->type_train_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train_engine.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Type de composant</div>
                                <div class="fs-4" data-vue="typeEngin">{{ $engine->engine->type_engine_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/energy-system.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Energie</div>
                                <div class="fs-4" data-vue="typeEnergy">{{ $engine->engine->type_engine_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3" data-vue="statusIcon">
                                <i class="fa-solid fa-{{ $engine->status_icon }} text-{{ $engine->status_color }} fs-3x"></i>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Status</div>
                                <div class="fs-4" data-vue="statusText">{{ $engine->status_text }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train_buy.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Date d'achat</div>
                                <div class="fs-4" data-vue="dateAchat">{{ $engine->date_achat_format }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8">
                    <div class="card shadow-lg mb-5">
                        <div class="card-header">
                            <div class="card-title">Statistique Générale</div>
                        </div>
                        <div class="card-body">
                            <div class="d-flex flex-row justify-content-between p-2">
                                <span class="fs-4">Distance parcourue</span>
                                <span class="fs-3 fw-semibold" data-vue="allDistance">{{ $engine->total_distance }} Km</span>
                            </div>
                            <div class="separator border-gray-600 my-1"></div>
                            <div class="d-flex flex-row justify-content-between p-2">
                                <span class="fs-4">Temps total de service</span>
                                <span class="fs-3 fw-semibold" data-vue="timeService">{{ $engine->heure_trajet }}</span>
                            </div>
                            <div class="separator border-gray-600 my-1"></div>

                            <div class="d-flex flex-row justify-content-between p-2">
                                <span class="fs-4">Nombre de trajet</span>
                                <span class="fs-3 fw-semibold" data-vue="nbTrajet">{{ $engine->nombre_trajet }}</span>
                            </div>
                            @if($engine->engine->type_energy == 'diesel' || $engine->engine->type_energy == 'hybride')
                                <div class="separator border-gray-600 my-1"></div>
                                <!-- TODO: A Faire lorsque le système de consomation sera pris en charge -->
                                <div class="d-flex flex-row justify-content-between bg-gray-400 text-gray-300 p-2" data-bs-toggle="tooltip" title="prochainement">
                                    <span class="fs-4">Consommation de carburant</span>
                                    <span class="fs-3 fw-semibold" data-vue="consoDiesel">0</span>
                                </div>
                            @endif
                            <div class="separator border-gray-600 my-1"></div>
                            <div class="d-flex flex-row justify-content-between p-2">
                                <span class="fs-4">Moyenne de vitesse</span>
                                <span class="fs-3 fw-semibold" data-vue="moySpeed">{{ $engine->other_info['speedAverage'] }} Km/h</span>
                            </div>

                        </div>
                    </div>
                    <div class="card shadow-lg mb-5">
                        <div class="card-header">
                            <div class="card-title">Derniers trajet effectuer</div>
                            <div class="card-toolbar"></div>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped gap-5 gs-7">
                                <thead>
                                    <tr>
                                        <th>Date / Heure</th>
                                        <th>Ligne</th>
                                        <th>Nb Incidents</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($engine->plannings()->where('status', 'arrived')->orderBy('date_arrived', 'desc')->get() as $planning)
                                        <tr class="align-middle">
                                            <td class="d-flex flex-column text-center">
                                                <span>{{ $planning->date_depart->format("d/m/Y") }}</span>
                                                <span>{{ $planning->date_depart->format("H:i") }} - {{ $planning->date_arrived->format("H:i") }}</span>
                                            </td>
                                            <td>{{ $planning->ligne->ligne->name }}</td>
                                            <td>{{ $planning->incidents()->count() }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="card shadow-lg h-100">
                                <div class="card-body">
                                    <div class="fw-bolder fs-4">Distance Parcourue</div>
                                    <div id="chartKilometer" style="height: 300px"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="card shadow-lg">
                                <div class="card-body">
                                    <div class="fw-bolder fs-4">Utilisations</div>
                                    <small class="text-gray-600">L'utilisation est basée sur la programmation potentiel hebdomadaire de ce matériel roulant sur la ligne qui lui est affilié.</small>
                                    <div id="chartUsed" style="height: 300px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="status" role="tabpanel">
            <div class="row">
                <div class="col-sm-12 col-lg-4">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Type de train</div>
                                <div class="fs-4" data-vue="typeTrain">{{ $engine->engine->type_train_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train_engine.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Type de composant</div>
                                <div class="fs-4" data-vue="typeEngin">{{ $engine->engine->type_engine_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/energy-system.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Energie</div>
                                <div class="fs-4" data-vue="typeEnergy">{{ $engine->engine->type_engine_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3" data-vue="statusIcon">
                                <i class="fa-solid fa-{{ $engine->status_icon }} text-{{ $engine->status_color }} fs-3x"></i>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Status</div>
                                <div class="fs-4" data-vue="statusText">{{ $engine->status_text }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train_buy.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Date d'achat</div>
                                <div class="fs-4" data-vue="dateAchat">{{ $engine->date_achat_format }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8">
                    <div class="row mb-5">
                        <div class="col-sm-12 col-lg-6">
                            <div class="card shadow-lg card-flushed bg-indigo-600 text-white bgi-size-contain bgi-size-lg-auto bgi-no-repeat bgi-position-center rounded" style="background-image: url('{{ asset('/storage/icons/maintenance_target.png') }}')">
                                <div class="card-body">
                                    <div class="d-flex flex-column">
                                        <div class="fs-3 fw-bolder d-flex flex-row align-items-center">
                                            <span class="me-1">Prochaine Maintenance Préventive</span>
                                            <i class="fa-solid fa-info-circle text-white fs-2" data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Cette date est préventive, elle tient compte des plannings déjà établie sur la durée."></i>
                                        </div>
                                        <div class="fs-4" data-vue="latestMaintenancePrev">{{ now()->addDays(70)->format("d/m/Y") }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 h-100">
                            <div class="d-flex flex-row justify-content-between align-items-center rounded bg-cyan-200 p-5">
                                <span class="fs-2 fw-semibold">Usure du matériel</span>
                                <div class="fs-1 fs-bolder text-{{ $engine->other_info['usure']['color'] }}">{{ $engine->other_info['usure']['value'] }} %</div>
                            </div>
                        </div>
                    </div>
                    <div class="card shadow-lg mb-5">
                        <div class="card-header">
                            <div class="card-title">Derniers incidents sur 30 jours</div>
                        </div>
                        <div class="card-body">
                            <div id="chartLatestIncident" style="height: 250px"></div>
                        </div>
                    </div>
                    <div class="card shadow-lg">
                        <div class="card-header">
                            <div class="card-title">Liste des incidents sur le matériels</div>
                        </div>
                        <div class="card-body">
                            <table id="listeIncident" class="table table-striped table-dark gs-5 gx-5 gy-5 gap-3">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Gravité</th>
                                        <th>Problème</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($engine->incidents->count() > 0)
                                    @foreach($engine->incidents as $incident)
                                        <tr>
                                            <td>{{ $incident->created_at->format("d/m/Y") }}</td>
                                            <td>{{ $incident->type->designation }}</td>
                                            <td>{{ $incident->niveau_label }}</td>
                                            <td>{{ $incident->designation }}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" class="text-center">
                                                <i class="fa-solid fa-check-circle text-success fs-1 me-3"></i>
                                                <span class="fs-1 fw-semibold">Aucun incidents</span>
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="prevent" role="tabpanel">
            <div class="row">
                <div class="col-sm-12 col-lg-4">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Type de train</div>
                                <div class="fs-4" data-vue="typeTrain">{{ $engine->engine->type_train_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train_engine.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Type de composant</div>
                                <div class="fs-4" data-vue="typeEngin">{{ $engine->engine->type_engine_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/energy-system.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Energie</div>
                                <div class="fs-4" data-vue="typeEnergy">{{ $engine->engine->type_engine_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3" data-vue="statusIcon">
                                <i class="fa-solid fa-{{ $engine->status_icon }} text-{{ $engine->status_color }} fs-3x"></i>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Status</div>
                                <div class="fs-4" data-vue="statusText">{{ $engine->status_text }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train_buy.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Date d'achat</div>
                                <div class="fs-4" data-vue="dateAchat">{{ $engine->date_achat_format }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8">
                    <div class="card shadow-lg">
                        <div class="card-header">
                            <div class="card-title">Prochaine maintenance Préventive</div>
                            <div class="card-toolbar">
                                <a href="#addMaintenancePrev" data-bs-toggle="modal" class="btn btn-sm btn-primary rounded-5">Nouvelle maintenance</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="listeMaintenancePrevent" class="table table-striped table-dark gx-5 gs-5 gy-5 gap-3">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Personnel</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($engine->tasks as $task)
                                        <tr>
                                            <td>{{ $task->start_at->format('d/m/Y à H:i') }}</td>
                                            <td>{{ $task->staff->name }}</td>
                                            <td>{!! $task->status_label !!}</td>
                                            <td>
                                                <button data-sector="maintenance" data-action="view" data-id="{{ $task->id }}" class="btn btn-sm btn-secondary btn-icon" data-bs-toggle="tooltip" title="Accès à la maintenance"><i class="fa-solid fa-eye"></i> </button>
                                                <button data-sector="maintenance" data-action="retarded" data-id="{{ $task->id }}" class="btn btn-sm btn-warning btn-icon" data-bs-toggle="tooltip" title="Retarder"><i class="fa-solid fa-clock"></i> </button>
                                                <button data-sector="maintenance" data-action="canceled" data-id="{{ $task->id }}" class="btn btn-sm btn-danger btn-icon" data-bs-toggle="tooltip" title="Annuler la maintenance"><i class="fa-solid fa-trash"></i> </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="equipements" role="tabpanel">
            <div class="row">
                <div class="col-sm-12 col-lg-4">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Type de train</div>
                                <div class="fs-4" data-vue="typeTrain">{{ $engine->engine->type_train_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train_engine.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Type de composant</div>
                                <div class="fs-4" data-vue="typeEngin">{{ $engine->engine->type_engine_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/energy-system.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Energie</div>
                                <div class="fs-4" data-vue="typeEnergy">{{ $engine->engine->type_engine_format }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3" data-vue="statusIcon">
                                <i class="fa-solid fa-{{ $engine->status_icon }} text-{{ $engine->status_color }} fs-3x"></i>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Status</div>
                                <div class="fs-4" data-vue="statusText">{{ $engine->status_text }}</div>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center bg-gray-300 rounded shadow-sm p-5 mb-5">
                            <div class="symbol symbol-70px me-3">
                                <img src="{{ asset('/storage/icons/train_buy.png') }}" alt="">
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fs-3 fw-bolder">Date d'achat</div>
                                <div class="fs-4" data-vue="dateAchat">{{ $engine->date_achat_format }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8">
                    <!--begin::Accordion-->
                    <div class="accordion" id="kt_accordion_1">
                        @foreach($osmose_engine->engine->components as $component)
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="kt_accordion_1_header_{{ $component->name_slugify }}">
                                <button class="accordion-button btn-flex fs-4 fw-semibold" type="button" data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_{{ $component->name_slugify }}" aria-expanded="true" aria-controls="kt_accordion_1_{{ $component->name_slugify }}">
                                    <span class="symbol symbol-40px me-3">
                                        <img src="{{ asset('/storage/icons/vigimat/'.$component->name_slugify.'.png') }}" alt="" class="w-40px h-auto me-3" />
                                        <span class="symbol-badge badge badge-circle bg-{{ $component->status_color }} top-100 start-100" data-bs-toggle="tooltip" title="{{ $component->status_text }}"><i class="fa-solid fa-circle text-white"></i></span>
                                    </span>
                                    <span>{{ $component->name }}</span>
                                </button>
                            </h2>
                            <div id="kt_accordion_1_{{ $component->name_slugify }}" class="accordion-collapse collapse show" aria-labelledby="kt_accordion_1_header_{{ $component->name_slugify }}" data-bs-parent="#kt_accordion_1">
                                <div class="accordion-body">
                                    <div class="d-flex flex-wrap justify-content-between align-items-center">
                                        @foreach($component->metrics as $metric)
                                        <div class="d-flex flex-row justify-content-between align-items-center w-40 me-5 p-5 rounded-2 bg-gray-200">
                                            <span class="fs-bold fs-4 flex-grow-1">{{ $metric->name }}</span>
                                            <span class="text-{{ $metric->latest_logs_color }} fs-3 fw-semibold">{{ $metric->latest_logs_value }} {{ $metric->unit }}</span>
                                            <div class="vr mx-2 my-0 p-0"></div>
                                            <div class="fs-8 fs-italic">{{ $metric->latest_logs_date }}</div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="kt_accordion_1_header_2">
                                <button class="accordion-button fs-4 fw-semibold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_2" aria-expanded="false" aria-controls="kt_accordion_1_body_2">
                                    Accordion Item #2
                                </button>
                            </h2>
                            <div id="kt_accordion_1_body_2" class="accordion-collapse collapse" aria-labelledby="kt_accordion_1_header_2" data-bs-parent="#kt_accordion_1">
                                <div class="accordion-body">
                                    ...
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="kt_accordion_1_header_3">
                                <button class="accordion-button fs-4 fw-semibold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_3" aria-expanded="false" aria-controls="kt_accordion_1_body_3">
                                    Accordion Item #3
                                </button>
                            </h2>
                            <div id="kt_accordion_1_body_3" class="accordion-collapse collapse" aria-labelledby="kt_accordion_1_header_3" data-bs-parent="#kt_accordion_1">
                                <div class="accordion-body">
                                    ...
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Accordion-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="auditEngine">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Audit de votre parc actuel</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <p data-vue="auditDescription"></p>
                    <table class="table table-striped gy-5 gs-7">
                        <thead>
                        <tr class="fs-2 fw-semibold bg-gray-300">
                            <th>Matériel</th>
                            <th>Anciennete</th>
                            <th>Usure</th>
                            <th>Préventive</th>
                            <th>Curative</th>
                            <th>Description</th>
                            <th>Conseil</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="rapportBody"></tbody>
                    </table>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="addMaintenancePrev">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Programmation d'une maintenance Préventive</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form action="{{ route('api.vigimat.engines.maintenance.preventive', $engine->id) }}" method="POST" id="formaddMaintenancePrev">
                    @csrf
                    <div class="modal-body">
                        <x-base.alert
                            type="solid"
                            title="Informations"
                            color="info"
                            icon="info-circle"
                            content="La maintenance préventive vérifiera tous les points importants du matériels roulants. Il remettra à 0% sont usure." />
                        <x-form.input-date
                            name="date"
                            label="Date de la maintenance"
                            required="true" />
                    </div>

                    <div class="modal-footer">
                        <x-form.button />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="modalShowMaintenanceInfo">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title"></h3>

                    <div data-vue="status"></div>
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <div class="d-flex flex-row-fluid align-items-start">
                        <div class="d-flex flex-column w-25 bg-gray-300 shadow-sm p-5">
                            <div class="d-flex flex-row justify-content-between align-items-center border border-bottom-2 border-top-0 border-right-0 border-left-0 border-gray-500 p-2 mb-5">
                                <span class="fs-3">Date de la maintenance</span>
                                <span class="fw-bold fs-3" data-vue="start_at">12/08/2023</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center border border-bottom-2 border-top-0 border-right-0 border-left-0 border-gray-500 p-2 mb-5">
                                <span class="fs-3">Type de maintenance</span>
                                <span class="fw-bold fs-3" data-vue="typeText">Maintenance Préventive</span>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center border border-bottom-2 border-top-0 border-right-0 border-left-0 border-gray-500 p-2 mb-5">
                                <span class="fs-3">Opérateur</span>
                                <span class="fw-bold fs-3" data-vue="staff">Mockelyn Maxime</span>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap w-70 p-5">
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                            <div class="d-flex align-items-center me-5">
                                <span class="bullet  me-3"></span> Testing
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/fr.js"></script>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function() {
            let div = {
                btnAudit: document.querySelector('#btnAuditMaterial'),
            }

            let modal = {
                modalAudit: document.querySelector('#auditEngine'),
                modalShowMaintenanceInfo: document.querySelector('#modalShowMaintenanceInfo')
            }

            let block = {

            }

            let form = {
                formAddMaintenancePrev: document.querySelector('#formaddMaintenancePrev')
            }

            @if(Session::has('technicentre'))
            verifyOsmose()
            generalInfo()
            postAddMaintenencePrev()
            action()
            getEngineFromOsmose()
            div.btnAudit.addEventListener('click', () => {
                div.btnAudit.setAttribute('data-kt-indicator', 'on')
                generateAudit()
            })
            $(document.querySelector('[name="date"]')).flatpickr({
                dateFormat: 'Y-m-d H:i:s',
                locale: 'fr',
                weekNumbers: true,
                minDate: "today",
                altFormat: "d/m/Y"
            })

            @endif

            function verifyOsmose() {
                if(statusOsmose) {
                    document.querySelector('[data-vue="osmose-status"]').classList.add('text-success');
                    document.querySelector('[data-vue="osmose-status"]').innerHTML = "En Ligne";
                } else {
                    document.querySelector('[data-vue="osmose-status"]').classList.add('text-danger');
                    document.querySelector('[data-vue="osmose-status"]').innerHTML = "Hors Ligne";
                }
            }

            function generalInfo() {
                $.ajax({
                    url: '/api/vigimat/engines/{{ $engine->id }}',
                    success: data => {
                        document.querySelector('[data-vue="indetifiant"]').innerHTML = "Train "+ data.engine.number;
                        document.querySelector('[data-vue="nameEngine"]').innerHTML = data.engine.engine.name + ' - '+ data.engine.engine.type_engine_format + " "+ data.engine.engine.type_energy_format;
                        document.querySelector('[data-vue="typeTrain"]').innerHTML = data.engine.engine.type_train_format;
                        document.querySelector('[data-vue="typeEngin"]').innerHTML = data.engine.engine.type_engine_format;
                        document.querySelector('[data-vue="typeEnergy"]').innerHTML = data.engine.engine.type_energy_format;
                        document.querySelector('[data-vue="statusIcon"]').innerHTML = `<i class="fa-solid fa-${data.engine.status_icon} text-${data.engine.status_color} fs-3x"></i>`;
                        document.querySelector('[data-vue="statusText"]').innerHTML = data.engine.status_text;
                        document.querySelector('[data-vue="dateAchat"]').innerHTML = data.engine.date_achat_format;
                        document.querySelector('[data-vue="priceAchat"]').innerHTML = data.engine.engine.format_price_checkout;
                        document.querySelector('[data-vue="resultatCumul"]').innerHTML = data.engine.other_info.resulat;
                        document.querySelector('[data-vue="rentability"]').innerHTML = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(data.engine.rentability);
                        document.querySelector('[data-vue="hub"]').innerHTML = data.engine.user_hub.hub.gare.name
                        document.querySelector('[data-vue="nbTravel"]').innerHTML = data.engine.nombre_trajet
                        document.querySelector('[data-vue="latestCurative"]').innerHTML = data.engine.other_info.latestMaintenanceCur ?? 'Aucun Maintenance'

                        if(data.engine.engine.type_train === 'ter') {
                            document.querySelector('[data-vue="repartitionSiege"]').innerHTML = `
                            <div data-bs-toggle="tooltip" data-placement="bottom" title="2eme classe" class="progress h-20px" role="progressbar" aria-label="Segment one" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: ${data.engine.other_info.compositions[0]}%">
                                 <div class="progress-bar"></div>
                            </div>
                            `
                        } else {
                            document.querySelector('[data-vue="repartitionSiege"]').innerHTML = `
                            <div data-bs-toggle="tooltip" data-placement="bottom" title="1ere classe" class="progress h-20px" role="progressbar" aria-label="Segment one" aria-valuenow="${data.engine.other_info.compositions[0]}" aria-valuemin="0" aria-valuemax="100" style="width: ${data.engine.other_info.compositions[0]}%">
                                <div class="progress-bar bg-red-700"></div>
                            </div>
                            <div data-bs-toggle="tooltip" data-placement="bottom" title="2eme classe" class="progress h-20px" role="progressbar" aria-label="Segment two" aria-valuenow="${data.engine.other_info.compositions[1]}" aria-valuemin="0" aria-valuemax="100" style="width: ${data.engine.other_info.compositions[1]}%">
                                <div class="progress-bar bg-blue-600"></div>
                            </div>
                            `
                        }

                        mapLatestPosition(data.engine.other_info.latestPosition)
                        chartKilometer()
                        chartUsed(data.engine.other_info.usedPlanning)
                        chartsInfo()
                    }
                })
            }

            function chartsInfo() {
                $.ajax({
                    url: '/api/vigimat/charts',
                    data: {type: 'latestIncidentEngine', engine_id: '{{ $engine->id }}'},
                    success: data => {
                        console.log(data)
                        chartLatestIncident(data.data)
                    }
                })
            }

            function mapLatestPosition(location) {
                let map = initMap(location, 15)
                map.on('load', () => {
                    let el = document.createElement('div')
                    el.className = 'marker'
                    el.style.backgroundImage = `url('/storage/icons/train_point.png')`;
                    el.style.backgroundSize = '100%'
                    el.style.width = '40px'
                    el.style.height = '40px'

                    new mapboxgl.Marker(el)
                        .setOffset([0, -45])
                        .setLngLat(location)
                        .addTo(map)
                })
            }

            function generateAudit() {
                $.ajax({
                    url:'/api/vigimat/engines/'+{{ $engine->id }}+'/audit',
                    success: data => {
                        console.log(data)
                        let mds = new bootstrap.Modal(modal.modalAudit)
                        div.btnAudit.removeAttribute('data-kt-indicator')
                        mds.show()
                        let rapportBody = document.querySelector('#rapportBody')
                        rapportBody.innerHTML = ''
                        data.rapport.forEach(element => {
                            let row = rapportBody.insertRow()
                            let cellNom = row.insertCell(0)
                            let cellAnciennete = row.insertCell(1)
                            let cellUsure = row.insertCell(2)
                            let cellPreventive = row.insertCell(3)
                            let cellCurative = row.insertCell(4)
                            let cellDescription = row.insertCell(5)
                            let cellConseil = row.insertCell(6)
                            let cellActions = row.insertCell(7);

                            cellNom.innerHTML = element.nom
                            cellAnciennete.innerHTML = element.anciennete+" / 5"
                            cellUsure.innerHTML = element.usure_totale+" %"
                            cellPreventive.innerHTML = element.maintenancePreventive ? "Oui" : "Non"
                            cellCurative.innerHTML = element.maintenanceCurative ? "Oui" : "Non"
                            cellDescription.innerHTML = element.description
                            cellConseil.innerHTML = element.conseils.join('<br>')

                            // Créer les boutons d'action
                            let boutonMaintenancePreventive = document.createElement('button');
                            boutonMaintenancePreventive.innerText = 'Maintenance Préventive';
                            boutonMaintenancePreventive.className = 'btn btn-primary btn-sm';
                            boutonMaintenancePreventive.onclick = function() {
                                declencherMaintenancePreventive(element.id, element.nom);
                            };
                            cellActions.appendChild(boutonMaintenancePreventive);

                            let boutonMaintenanceCurative = document.createElement('button');
                            boutonMaintenanceCurative.innerText = 'Maintenance Curative';
                            boutonMaintenanceCurative.className = 'btn btn-danger btn-sm';
                            boutonMaintenanceCurative.onclick = function() {
                                declencherMaintenanceCurative(element.id, element.nom);
                            };
                            cellActions.appendChild(boutonMaintenanceCurative);

                        })
                    }
                })
            }

            // Fonction pour déclencher la maintenance préventive
            function declencherMaintenancePreventive(id, nomMateriel) {
                // Ajouter la logique pour déclencher la maintenance préventive pour le matériel avec le nom spécifié
                $.ajax({
                    url: '/api/game/maintenance/maintenance',
                    method: 'POST',
                    data: {engine_id: id, type: 'prefentif'},
                    success: data => {
                        toastr.success('Maintenance préventive déclenchée pour le matériel '+nomMateriel);
                    }
                })
            }

            // Fonction pour déclencher la maintenance curative
            function declencherMaintenanceCurative(id, nomMateriel) {
                $.ajax({
                    url: '/api/game/maintenance/maintenance',
                    method: 'POST',
                    data: {engine_id: id, type: 'curatif'},
                    success: data => {
                        toastr.success('Maintenance Curative déclenchée pour le matériel '+nomMateriel);
                    }
                })
            }

            function chartLatestIncident(data) {
                am5.ready(() => {
                    let root = am5.Root.new('chartLatestIncident');

                    root.setThemes([
                        am5themes_Animated.new(root)
                    ]);

                    let chart = root.container.children.push(am5xy.XYChart.new(root, {
                        panX: true,
                        panY: true,
                        wheelX: "panX",
                        wheelY: "zoomX",
                        pinchZoomX:true
                    }));

                    let cursor = chart.set("cursor", am5xy.XYCursor.new(root, {
                        behavior: "none"
                    }));
                    cursor.lineY.set("visible", false);

                    let xAxis = chart.xAxes.push(am5xy.DateAxis.new(root, {
                        maxDeviation: 0.2,
                        baseInterval: {
                            timeUnit: "day",
                            count: 1
                        },
                        renderer: am5xy.AxisRendererX.new(root, {}),
                        tooltip: am5.Tooltip.new(root, {})
                    }));
                    let yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                        maxPrecision: 0,
                        min: 0,
                        renderer: am5xy.AxisRendererY.new(root, {
                            pan:"zoom"
                        })
                    }));

                    let series = chart.series.push(am5xy.LineSeries.new(root, {
                        name: "Incidents",
                        xAxis: xAxis,
                        yAxis: yAxis,
                        valueYField: "value",
                        valueXField: "date",
                        tooltip: am5.Tooltip.new(root, {
                            labelText: "{valueY}"
                        })
                    }));

                    chart.set("scrollbarX", am5.Scrollbar.new(root, {
                        orientation: "horizontal"
                    }));

                    series.data.setAll(data);

                    series.appear(1000);
                    chart.appear(1000, 100);
                })

            }

            function chartKilometer() {
                $.ajax({
                    url: '/api/vigimat/charts',
                    data: {type: 'statKilometer', engine_id: {{ $engine->id }}},
                    success: data => {
                        console.log(data)
                        let root = am5.Root.new('chartKilometer');
                        let chart = root.container.children.push(
                            am5xy.XYChart.new(root, {
                                panY: false,
                                layout: root.verticalLayout
                            })
                        )

                        let xAxis = chart.xAxes.push(
                            am5xy.CategoryAxis.new(root, {
                                categoryField: 'mois',
                                renderer: am5xy.AxisRendererX.new(root, {}),
                            })
                        );
                        xAxis.data.setAll(data)

                        let yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                            renderer: am5xy.AxisRendererY.new(root, {})
                        }));

                        let serie1 = chart.series.push(
                            am5xy.ColumnSeries.new(root, {
                                name: "Distance Parcourue",
                                xAxis: xAxis,
                                yAxis: yAxis,
                                valueYField: "kilometers",
                                categoryXField: "mois",
                                maxDeviation: 0.3,
                                tooltip: am5.Tooltip.new(root, {}),
                            })
                        );
                        serie1.data.setAll(data);

                        let legend = chart.children.push(am5.Legend.new(root, {}));
                        legend.data.setAll(chart.series.values);
                        chart.set("cursor", am5xy.XYCursor.new(root, {}));
                    }
                })
            }

            function chartUsed(valueUsed) {
                am5.ready(() => {
                    let root = am5.Root.new('chartUsed');

                    root.setThemes([
                        am5themes_Animated.new(root)
                    ]);

                    let chart = root.container.children.push(
                        am5radar.RadarChart.new(root, {
                            panX: false,
                            panY: false,
                            startAngle: 180,
                            endAngle: 360
                        })
                    );

                    let axisRenderer = am5radar.AxisRendererCircular.new(root, {
                        strokeOpacity: 1,
                        strokeWidth: 15,
                        innerRadius: -10,
                        strokeGradient: am5.LinearGradient.new(root, {
                            rotation: 0,
                            stops: [
                                { color: am5.color(0x19d228) },
                                { color: am5.color(0xf4fb16) },
                                { color: am5.color(0xf6d32b) },
                                { color: am5.color(0xfb7116) }
                            ]
                        })
                    });

                    let xAxis = chart.xAxes.push(
                        am5xy.ValueAxis.new(root, {
                            maxDeviation: 0,
                            min: 0,
                            max: 100,
                            strictMinMax: true,
                            renderer: axisRenderer
                        })
                    );

                    let axisDataItem = xAxis.makeDataItem({});
                    axisDataItem.set("value", 0);

                    let bullet = axisDataItem.set("bullet", am5xy.AxisBullet.new(root, {
                        sprite: am5radar.ClockHand.new(root, {
                            radius: am5.percent(99)
                        })
                    }));

                    xAxis.createAxisRange(axisDataItem);

                    axisDataItem.get("grid").set("visible", false);

                    axisDataItem.animate({
                        key: "value",
                        to: valueUsed,
                        duration: 800,
                        easing: am5.ease.out(am5.ease.cubic)
                    });

                    chart.appear(1000, 100);

                })
            }

            function postAddMaintenencePrev() {
                $(form.formAddMaintenancePrev).on('submit', e => {
                    e.preventDefault()
                    let form = $(e.currentTarget)
                    let url = form.attr('action')
                    let data = form.serializeArray()

                    $.ajax({
                        url: url,
                        method: "POST",
                        data: data,
                        success: data => {
                            toastr.success('Maintenance préventive programmée')
                            setTimeout(() => {
                                window.location.reload()
                            }, 1200)
                        }
                    })
                })
            }

            function action() {
                document.querySelectorAll('[data-sector="maintenance"]').forEach(maintenance => {
                    maintenance.addEventListener('click', e => {
                        e.preventDefault()
                        if(maintenance.getAttribute('data-action') === 'view') {
                            $.ajax({
                                url: '/api/vigimat/engines/{{ $engine->id }}/maintenance/'+maintenance.getAttribute('data-id'),
                                success: data => {
                                    let modalsys = new bootstrap.Modal(modal.modalShowMaintenanceInfo)
                                    modalsys.show()
                                    modal.modalShowMaintenanceInfo.querySelector('.modal-title').innerHTML = `Maintenance Préventive du ${data.start_at_format}`
                                    modal.modalShowMaintenanceInfo.querySelector('[data-vue="status"]').innerHTML = data.status_label
                                }
                            })
                        }
                    })
                })
            }

            function getEngineFromOsmose() {
                $.ajax({
                    url: '/api/vigimat/engines/{{ $engine->id }}/osmose',
                    success: data => {
                        console.log(data)
                    }
                })
            }
        })
    </script>
@endsection
