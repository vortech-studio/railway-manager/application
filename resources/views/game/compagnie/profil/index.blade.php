@extends("game.template")

@section("css")
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/adjust.png') }}" alt="">
            </div>
            <x-base.underline
                title="Personnalisation"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.compagnie.partials.menu")
    </div>
    <div class="card shadow-lg">
        <div class="card-body">
            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#overview">Général</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#network">Réseau & Flotte</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="overview" role="tabpanel">
                    <div class="d-flex flex-row justify-content-between mb-10">
                        <div class="d-flex flex-row">
                            <img src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}" class="h-50px w-auto me-3" alt="">
                            <div class="d-flex flex-column">
                                <div class="fs-1 text-dark fw-bold fs-underline">{{ auth()->user()->name_company }}</div>
                                <div class="fs-5 text-dark fw-bold">
                                    <i class="fa-solid fa-star text-yellow-600"></i>
                                </div>
                            </div>
                        </div>
                        <!--<div class="d-flex flex-row">
                            <div class="d-flex flex-column">
                                <span class="border border-2 border-top-0 border-left-0 border-right-0 fs-2 fw-semibold">Général:</span>
                                <div class="d-flex flex-row">
                                    <img src="{{ asset('/storage/icons/ranking.png') }}" class="w-30px" alt="">
                                    <span class="fs-2">12 563</span>
                                </div>
                            </div>
                            <div class="vr mx-3"></div>
                            <div class="d-flex flex-column">
                                <span class="border border-2 border-top-0 border-left-0 border-right-0 fs-2 fw-semibold">Général:</span>
                                <div class="d-flex flex-row">
                                    <img src="{{ asset('/storage/icons/ranking.png') }}" class="w-30px" alt="">
                                    <span class="fs-2">12 563</span>
                                </div>
                            </div>
                        </div>-->
                    </div>
                    <div class="d-flex flex-row h-500px bgi-no-repeat bgi-size-cover bgi-position-y-center rounded-2 mb-10" style="background-image: url('{{ asset('/storage/other/wall-rm.png') }}')">
                        <div class="d-flex flex-row w-70">
                            <img src="{{ asset('/storage/other/directeur.png') }}" alt="">
                            <img src="{{ asset('/storage/other/secretary.png') }}" alt="">
                        </div>
                        <div class="d-flex flex-column w-30">
                            <div class="rounded bg-opacity-25 bg-white p-5 m-10">
                                <div class="d-flex flex-row justify-content-between p-5 border border-right-0 border-left-0 border-top-0 border-2 mb-1">
                                    <span>Directeur</span>
                                    <span>{{ auth()->user()->name }}</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between p-5 border border-right-0 border-left-0 border-top-0 border-2 mb-1">
                                    <span>Secrétaire</span>
                                    <span>{{ auth()->user()->name_secretary }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap justify-content-between mb-5">
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-40 me-5">
                            <span>Création de l'entreprise</span>
                            <span>{{ auth()->user()->created_at->format('d/m/Y') }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-40 me-5">
                            <span>Dernière connexion</span>
                            <span>{{ auth()->user()->dailys()->first()->date->format("d/m/Y à H:i") }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-40 me-5">
                            <span>Valorisation</span>
                            <span>{{ eur(auth()->user()->company->valorisation) }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-40 me-5">
                            <span>Solde</span>
                            <span>{{ eur(auth()->user()->argent) }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-40 me-5">
                            <span>Subvention</span>
                            <span>{{ auth()->user()->company->subvention }} %</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-40 me-5">
                            <span>Crédit d'impot</span>
                            <span>{{ eur(auth()->user()->company->credit_impot) }}</span>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap justify-content-between">
                        <div class="d-flex flex-column w-40 me-5">
                            <x-base.underline
                                title="Passagers"
                                size-text="fs-1"
                                size="3" class="w-100 mb-5 fs-uppercase"/>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                                <span>Distraction</span>
                                <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->distraction }}" data-bs-content="Les distractions sont très appréciés durant les longs trajets et en gares">
                                    <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->distraction) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                                <span>Tarifications</span>
                                <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->tarification }}" data-bs-content="Les clients qui effectue un cours trajets recherchent avant tous des prix attractifs.">
                                    <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->tarification) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                                <span>Ponctualité</span>
                                <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->ponctualite }}" data-bs-content="Les clients de classe unique ou seconde classe apprécient la ponctualité">
                                    <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->ponctualite) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                                <span>Sécurité</span>
                                <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->secutite }}" data-bs-content="Les clients apprécients d'être en sécurité lors des trajets.">
                                    <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->secutite) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                                <span>Confort</span>
                                <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->confort }}" data-bs-content="Les clients débourseront plus si le confort est augmenté.">
                                    <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->confort) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                                <span>Revenues Auxiliaires</span>
                                <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->rent_aux }}" data-bs-content="Augmenté les revenus auxiliaires vous permette d'avoir plus de ventes à bord voir même des subventions.">
                                    <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->rent_aux) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                                <span>Frais</span>
                                <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->frais }}" data-bs-content="Réduire les frais permet de diminuer les charges sur les trajets, les augmentés revoir vos prix à la hausse.">
                                    <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->frais) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center mb-2 p-5">
                                <span>Livraison</span>
                                <div class="h-8px mx-3 w-50 bg-primary bg-opacity-50 rounded" data-bs-toggle="popover" data-bs-trigger="hover" title="+ {{ auth()->user()->company->livraison }}" data-bs-content="Plus le niveau est augmenté et plus le service livraison exécutera les tâches rapidement.">
                                    <div class="bg-primary rounded h-8px" role="progressbar" style="width: {{ auth()->user()->company->getValueToPercent(auth()->user()->company->livraison) }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="10"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="network" role="tabpanel">
                    <div id="map" class="w-100 h-450px mb-5 p-5 rounded bg-gray-300"></div>
                    <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
                        <div class="col-sm-12 col-lg-4">
                            <div class="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-xl-100" style="background-color: #146aef;background-image:url('{{ asset('/storage/icons/station_hub.png') }}');">
                                <!--begin::Header-->
                                <div class="card-header pt-5 mb-3">
                                    <!--begin::Icon-->
                                    <div class="d-flex flex-center rounded-circle h-80px w-80px" style="border: 1px dashed rgba(255, 255, 255, 0.4);background-color: #146aef">
                                        <i class="fa-solid fa-building text-white fs-2qx lh-0"></i>
                                    </div>
                                    <!--end::Icon-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Card body-->
                                <div class="card-body d-flex align-items-end mb-3">
                                    <!--begin::Info-->
                                    <div class="d-flex align-items-center">
                                        <span class="fs-4hx text-white fw-bold me-6" data-vue="nbActifEngine">{{ auth()->user()->hubs()->count() }}</span>

                                        <div class="fw-bold fs-6 text-white">
                                            <span class="d-block">Hubs</span>
                                            <span class="">Actif</span>
                                        </div>
                                    </div>
                                    <!--end::Info-->
                                </div>
                                <!--end::Card body-->
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-xl-100" style="background-color: #e93434;background-image:url('{{ asset('/storage/icons/train.png') }}');">
                                <!--begin::Header-->
                                <div class="card-header pt-5 mb-3">
                                    <!--begin::Icon-->
                                    <div class="d-flex flex-center rounded-circle h-80px w-80px" style="border: 1px dashed rgba(255, 255, 255, 0.4);background-color: #e93434">
                                        <i class="fa-solid fa-train text-white fs-2qx lh-0"></i>
                                    </div>
                                    <!--end::Icon-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Card body-->
                                <div class="card-body d-flex align-items-end mb-3">
                                    <!--begin::Info-->
                                    <div class="d-flex align-items-center">
                                        <span class="fs-4hx text-white fw-bold me-6" data-vue="nbActifEngine">{{ auth()->user()->engines()->count() }}</span>

                                        <div class="fw-bold fs-6 text-white">
                                            <span class="d-block">Matériels Roulants</span>
                                            <span class="">Actif</span>
                                        </div>
                                    </div>
                                    <!--end::Info-->
                                </div>
                                <!--end::Card body-->
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-xl-100" style="background-color: #4f0f5d;background-image:url('{{ asset('/storage/icons/ligne.png') }}');">
                                <!--begin::Header-->
                                <div class="card-header pt-5 mb-3">
                                    <!--begin::Icon-->
                                    <div class="d-flex flex-center rounded-circle h-80px w-80px" style="border: 1px dashed rgba(255, 255, 255, 0.4);background-color: #4f0f5d">
                                        <i class="fa-solid fa-code-fork text-white fs-2qx lh-0"></i>
                                    </div>
                                    <!--end::Icon-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Card body-->
                                <div class="card-body d-flex align-items-end mb-3">
                                    <!--begin::Info-->
                                    <div class="d-flex align-items-center">
                                        <span class="fs-4hx text-white fw-bold me-6" data-vue="nbActifEngine">{{ auth()->user()->lignes()->count() }}</span>

                                        <div class="fw-bold fs-6 text-white">
                                            <span class="d-block">Lignes</span>
                                            <span class="">Actif</span>
                                        </div>
                                    </div>
                                    <!--end::Info-->
                                </div>
                                <!--end::Card body-->
                            </div>
                        </div>
                    </div>
                    <x-base.underline
                        title="<img src='{{ asset('/storage/icons/identity.png') }}' alt'' class='w-40px' /> Flotte de la compagnie"
                        size="4"
                        size-text="fs-2x" />
                    <div class="d-flex flex-wrap justify-content-center align-items-center">
                        @foreach(\App\Models\Core\Engine::all() as $engine)
                            @if(auth()->user()->engines()->where('engine_id', $engine->id)->exists())
                                <div class="d-flex flex-column rounded bg-gray-300 m-3 p-5">
                                    @if($engine->type_engine == 'automotrice')
                                        <img src="{{ asset('/storage/engines/automotrice/'.$engine->slug.'-0.gif') }}" alt="" class="w-120px mb-1">
                                    @else
                                        <img src="{{ asset('/storage/engines/'.$engine->type_engine.'/'.$engine->image) }}" alt="" class="w-120px mb-1">
                                    @endif
                                    <span class="fs-3 fw-semibold">{{ $engine->name }}</span>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset('/assets/js/map.js') }}"></script>
    <script type="text/javascript">
        $.ajax({
            url: '/api/core/hub/liste',
            data: {'user_id': '{{ auth()->user()->id }}'},
            success: data => {
                console.log(data)
                data.hubs.forEach(hub => {
                    let map = initMap()

                    map.on('load', () => {
                        formatForHub(map, hub)
                        hub.lignes.forEach(ligne => {
                            formatForLigne(map, ligne)
                            markerForStationEnd(map, ligne)
                        })
                    })
                })
            }
        })
    </script>
@endsection
