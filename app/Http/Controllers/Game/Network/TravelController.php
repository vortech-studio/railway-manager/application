<?php

namespace App\Http\Controllers\Game\Network;

use App\Http\Controllers\Controller;
use App\Models\User\UserPlanning;
use Illuminate\Http\Request;

class TravelController extends Controller
{
    public function show(Request $request, $planning_id)
    {
        $planning = UserPlanning::with('ligne', 'engine', 'hub', 'passengers', 'travel', 'planning_stations')->find($planning_id);

        return view('game.network.travel.show', [
            "planning" => $planning
        ]);
    }
}
