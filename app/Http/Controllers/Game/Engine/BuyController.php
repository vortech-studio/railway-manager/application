<?php

namespace App\Http\Controllers\Game\Engine;

use App\Http\Controllers\Controller;
use App\Jobs\Game\EngineDeliveryJob;
use App\Models\Core\Engine;
use App\Models\Core\Mouvement;
use App\Models\User\UserDelivery;
use App\Models\User\UserEngine;
use Illuminate\Http\Request;

class BuyController extends Controller
{
    public function index()
    {
        return view('game.engine.buy.index');
    }

    public function checkout()
    {
        return view('game.engine.buy.checkout');
    }

    public function config(Request $request)
    {
        if(\request()->ajax()) {
            return response()->json(route('engine.buy.config', ["engine_id" => $request->get('engine_id'), "qte" => $request->get('qte')]));
        } else {
            return view('game.engine.buy.config', [
                "engine" => Engine::find($request->get('engine_id')),
                "qte" => $request->get('qte')
            ]);
        }
    }

    public function checkoutConfirm(Request $request)
    {
        $engine = Engine::find($request->get('engine_id'));
        $amount = $engine->calcCart($request->get('qte'), 'total');
        $hub_user = auth()->user()->hubs()->find($request->get('user_hub_id'));

        if(Engine::isOk($amount, auth()->user(), 'argent')) {
            Mouvement::adding(
                'charge',
                'Achat du matériel roulant: ' . $engine->name." x".$request->get('qte'),
                'achat_materiel',
                $amount,
                auth()->user()->company->id,
                $hub_user->id,
            );
        } else {
            toastr()->error("Vous n'avez pas suffisamment d'argent");
            return redirect()->route('engine.buy.config');
        }


        for ($i = 1; $i <= $request->get('qte'); $i++) {
            if($hub_user->user->admin) {
                $user_e = auth()->user()->engines()->create([
                    "max_runtime_engine" => $engine->calcMaxRuntimeEngine(),
                    "active" => true,
                    "in_maintenance" => false,
                    "available" => true,
                    "date_achat" => now()->startOfDay(),
                    "user_id" => auth()->user()->id,
                    "engine_id" => $engine->id,
                    "user_hub_id" => $hub_user->id,
                    "number" => UserEngine::defineNumberEngine()
                ]);


            } else {
                $user_e = auth()->user()->engines()->create([
                    "max_runtime_engine" => $engine->calcMaxRuntimeEngine(),
                    "active" => false,
                    "in_maintenance" => false,
                    "available" => false,
                    "date_achat" => now()->startOfDay(),
                    "user_id" => auth()->user()->id,
                    "engine_id" => $engine->id,
                    "user_hub_id" => $hub_user->id,
                    "number" => UserEngine::defineNumberEngine()
                ]);

                $delivery = UserDelivery::create([
                    "type" => "engine",
                    "designation" => $engine->name,
                    "start_at" => now(),
                    "end_at" => null,
                    "user_id" => auth()->user()->id,
                    "user_engine_id" => $user_e->id
                ]);
                $delivery->end_at = $delivery->start_at->addMinutes($delivery->getRateFromDelivery());
                $delivery->save();
                dispatch(new EngineDeliveryJob($delivery, $user_e))->onQueue('delivery');

                auth()->user()->messages()->create([
                    "from" => auth()->user()->name_secretary,
                    "subject" => "Confirmation d'achat de nouveau matériel roulant",
                    "message" => view('game.includes.mailbox.checkout_engine', [
                        "engine" => $engine,
                        "amount" => $amount,
                        "hub_name" => $hub_user->hub->gare->name,
                        "delivery_at" => $delivery->end_at->format('d/m/Y à H:i'),
                        "user" => auth()->user(),
                    ]),
                    "important" => true,
                    "user_id" => auth()->user()->id
                ]);
            }

        }

        $hub_user->user->addPoints(150);
        $user_engine = auth()->user()->engines()->where('date_achat', now()->startOfDay())->first();

        toastr()->success("Achat effectué avec succès");
        return redirect()->route('engine.show', $user_engine->id);
    }
}
