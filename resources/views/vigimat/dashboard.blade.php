@extends("vigimat.template")

@section("css")

@endsection

@section("content")
    <div class="row mb-10">
        <div class="col-sm-12 col-lg-5">
            <div class="card shadow-lg">
                <div class="card-header">
                    <div class="card-title">
                        <div class="fs-3">Derniers incidents (30 Derniers jours)</div>
                    </div>
                    <div class="card-toolbar"></div>
                </div>
                <div class="card-body">
                    <div id="chartLatestIncident" class="h-100 w-100"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-5">
            <div class="card shadow-lg">
                <div class="card-header">
                    <div class="card-title">
                        <div class="fs-3">Utilisation du matériel roulant (15 derniers jours)</div>
                    </div>
                    <div class="card-toolbar"></div>
                </div>
                <div class="card-body">
                    <div id="chartUsedEngines" class="h-100 w-100"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-2">
            <div class="bg-white rounded p-5 shadow-lg">
                <div id="clock" style="width: 100%; height: 400px"></div>
            </div>
        </div>
    </div>
    <div class="row h-100">
        <div class="col-sm-12 col-lg-4 h-100">
            <div class="bg-white rounded p-5 shadow-lg">
                <div class="d-flex flex-row align-items-center mb-5">
                    <div class="symbol symbol-30px symbol-circle me-3">
                        <img src="{{ asset('/storage/icons/train.png') }}" alt="">
                    </div>
                    <span class="fs-2 fw-bold">Matériel roulant</span>
                </div>
                <div class="d-flex flex-column">
                    <div
                        class="d-flex flex-row justify-content-between align-items-center bg-gray-300 shadow-sm p-5 mb-2">
                        <span class="fs-2 fw-semibold">Engin Actif (Trajet en cours)</span>
                        <span class="text-success fs-1 fw-bolder" data-vue="nbActifEngine"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </div>
                    <div
                        class="d-flex flex-row justify-content-between align-items-center bg-gray-300 shadow-sm p-5 mb-2">
                        <span class="fs-2 fw-semibold">Engin Inactif</span>
                        <span class="text-gray-700 fs-1 fw-bolder" data-vue="nbInactifEngine"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </div>
                    <div
                        class="d-flex flex-row justify-content-between align-items-center bg-gray-300 shadow-sm p-5 mb-2">
                        <span class="fs-2 fw-semibold">Engin Présentant un défaut</span>
                        <span class="text-danger fs-1 fw-bolder" data-vue="nbFaultyEngine"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-4 h-auto">
            <div class="bg-white rounded p-5 shadow-lg">
                <div class="d-flex flex-row align-items-center mb-5">
                    <div class="symbol symbol-30px symbol-circle me-3">
                        <img src="{{ asset('/storage/icons/maintenance_target.png') }}" alt="">
                    </div>
                    <span class="fs-2 fw-bold">Interventions</span>
                </div>
                <div class="d-flex flex-column">
                    <div
                        class="d-flex flex-row justify-content-between align-items-center bg-gray-300 shadow-sm p-5 mb-2">
                        <span class="fs-2 fw-semibold">Total</span>
                        <span class="text-success fs-1 fw-bolder" data-vue="totalIntervention"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </div>
                    <div
                        class="d-flex flex-row justify-content-between align-items-center bg-gray-300 shadow-sm p-5 mb-2">
                        <span class="fs-2 fw-semibold">Intervention Préventive</span>
                        <span class="text-gray-700 fs-1 fw-bolder" data-vue="totalPreventive"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </div>
                    <div
                        class="d-flex flex-row justify-content-between align-items-center bg-gray-300 shadow-sm p-5 mb-2">
                        <span class="fs-2 fw-semibold">Intervention Curative</span>
                        <span class="text-danger fs-1 fw-bolder" data-vue="totalCurative"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </div>
                    <div
                        class="d-flex flex-row justify-content-between align-items-center bg-gray-300 shadow-sm p-5 mb-2">
                        <span class="fs-2 fw-semibold">Intervention Reussi</span>
                        <span class="text-danger fs-1 fw-bolder" data-vue="checkIntervention"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </div>
                    <div
                        class="d-flex flex-row justify-content-between align-items-center bg-gray-300 shadow-sm p-5 mb-2">
                        <span class="fs-2 fw-semibold">Intervention Annulée</span>
                        <span class="text-danger fs-1 fw-bolder" data-vue="cancelIntervention"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-4 h-auto">
            <div class="bg-white rounded p-5 shadow-lg">
                <div class="d-flex flex-row align-items-center mb-5">
                    <div class="symbol symbol-30px symbol-circle me-3">
                        <img src="{{ asset('/storage/icons/technician.png') }}" alt="">
                    </div>
                    <span class="fs-2 fw-bold">Personnelles</span>
                </div>
                <div class="d-flex flex-column">
                    <div
                        class="d-flex flex-row justify-content-between align-items-center bg-gray-300 shadow-sm p-5 mb-2">
                        <span class="fs-2 fw-semibold">Nombre de personnel</span>
                        <span class="text-success fs-1 fw-bolder" data-vue="totalTechnician"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </div>
                    <div
                        class="d-flex flex-row justify-content-between align-items-center bg-gray-300 shadow-sm p-5 mb-2">
                        <span class="fs-2 fw-semibold">Actif</span>
                        <span class="text-gray-700 fs-1 fw-bolder" data-vue="totalActifTechnician"><span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>
    <script type="text/javascript">
        let div = {
            chartLatestIncident: document.querySelector('#chartLatestIncident'),
            chartUsedEngines: document.querySelector('#chartUsedEngines'),
        }

        let block = {
            chartLatestIncident: new KTBlockUI(div.chartLatestIncident, {
                message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Loading...</div>',
                overlayClass: "bg-danger bg-opacity-25",
            }),
            chartUsedEngines: new KTBlockUI(div.chartUsedEngines, {
                message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Loading...</div>',
                overlayClass: "bg-danger bg-opacity-25",
            })
        }

        @if(Session::has('technicentre'))
        loadChartLatestIncident();
        loadChartUsedEngine();
        displayClock()
        updateStatistique()

        @endif

        async function loadChartLatestIncident() {
            block.chartLatestIncident.block()

            $.ajax({
                url: '/api/vigimat/charts',
                data: {'type': "latestIncident"},
                success: data => {
                    block.chartLatestIncident.release()
                    console.log(data)
                    let options = {
                        chart: {
                            height: 300,
                            type: 'line',
                            stacked: false,
                            toolbar: {
                                show: false,
                            },
                        },
                        dataLabels: {
                            enabled: false
                        },
                        color: ["#e93434"],
                        series: [
                            {
                                name: 'Incidents',
                                data: data.content
                            }
                        ],
                        stroke: {
                            width: [3]
                        },
                        plotOptions: {
                            bar: {
                                columnWidth: "20%"
                            }
                        },
                        xaxis: {
                            categories: data.labels
                        },
                        yaxis: [
                            {
                                axisTicks: {
                                    show: true
                                },
                                axisBorder: {
                                    show: true,
                                    color: "#FF1654"
                                },
                                labels: {
                                    style: {
                                        colors: "#FF1654"
                                    }
                                },
                                title: {
                                    text: "Incidents",
                                    style: {
                                        color: "#FF1654"
                                    }
                                }
                            },
                        ],
                        tooltip: {
                            enabled: true,
                            formatter: undefined,
                            offsetY: 0,
                            style: {
                                fontSize: '12px'
                            },
                            x: {
                                show: true,
                                format: 'dd MMM',
                                formatter: undefined,
                            },
                            y: {
                                formatter: function (value, {series, seriesIndex, dataPointIndex, w}) {
                                    return value
                                },
                                title: {
                                    formatter: (seriesName) => seriesName,
                                },
                            },
                        },
                        legend: {
                            horizontalAlign: "left",
                            offsetX: 40
                        }
                    };
                    let chart = new ApexCharts(div.chartLatestIncident, options)
                    chart.render()
                }
            })
        }

        async function loadChartUsedEngine() {
            block.chartUsedEngines.block()

            $.ajax({
                url: '/api/vigimat/charts',
                data: {'type': "usedEngines"},
                success: data => {
                    block.chartUsedEngines.release()
                    console.log(data)
                    let options = {
                        chart: {
                            height: 300,
                            type: 'line',
                            stacked: false,
                            toolbar: {
                                show: false,
                            },
                        },
                        dataLabels: {
                            enabled: false
                        },
                        color: ["#e93434"],
                        series: [
                            {
                                name: 'Nombre de trajets',
                                data: data.content
                            }
                        ],
                        stroke: {
                            width: [3]
                        },
                        plotOptions: {
                            bar: {
                                columnWidth: "20%"
                            }
                        },
                        xaxis: {
                            categories: data.labels
                        },
                        yaxis: [
                            {
                                axisTicks: {
                                    show: true
                                },
                                axisBorder: {
                                    show: true,
                                    color: "#FF1654"
                                },
                                labels: {
                                    style: {
                                        colors: "#FF1654"
                                    }
                                },
                                title: {
                                    text: "Nombre de trajets",
                                    style: {
                                        color: "#FF1654"
                                    }
                                }
                            },
                        ],
                        tooltip: {
                            enabled: true,
                            formatter: undefined,
                            offsetY: 0,
                            style: {
                                fontSize: '12px'
                            },
                            x: {
                                show: true,
                                format: 'dd MMM',
                                formatter: undefined,
                            },
                            y: {
                                formatter: function (value, {series, seriesIndex, dataPointIndex, w}) {
                                    return value
                                },
                                title: {
                                    formatter: (seriesName) => seriesName,
                                },
                            },
                        },
                        legend: {
                            horizontalAlign: "left",
                            offsetX: 40
                        }
                    };
                    let chart = new ApexCharts(div.chartUsedEngines, options)
                    chart.render()
                }
            })
        }

        async function updateStatistique() {
            $.ajax({
                url: '/api/vigimat/charts',
                data: {'type': "stat"},
                success: data => {
                    document.querySelector('[data-vue="cancelIntervention"]').innerHTML = data.cancelIntervention
                    document.querySelector('[data-vue="checkIntervention"]').innerHTML = data.checkIntervention
                    document.querySelector('[data-vue="totalIntervention"]').innerHTML = data.totalIntervention
                    document.querySelector('[data-vue="totalPreventive"]').innerHTML = data.totalPreventive
                    document.querySelector('[data-vue="totalCurative"]').innerHTML = data.totalCurative
                    document.querySelector('[data-vue="nbActifEngine"]').innerHTML = data.nbActifEngine
                    document.querySelector('[data-vue="nbInactifEngine"]').innerHTML = data.nbInactifEngine
                    document.querySelector('[data-vue="nbFaultyEngine"]').innerHTML = data.nbFaultyEngine
                    document.querySelector('[data-vue="totalTechnician"]').innerHTML = data.totalTechnician
                    document.querySelector('[data-vue="totalActifTechnician"]').innerHTML = data.totalActifTechnician

                }
            })
        }

        function displayClock() {
            am5.ready(function () {

// Create root element
// https://www.amcharts.com/docs/v5/getting-started/#Root_element
                var root = am5.Root.new("clock");


// Set themes
// https://www.amcharts.com/docs/v5/concepts/themes/
                root.setThemes([
                    am5themes_Animated.new(root)
                ]);


// Create chart
// https://www.amcharts.com/docs/v5/charts/radar-chart/
                var chart = root.container.children.push(am5radar.RadarChart.new(root, {
                    panX: false,
                    panY: false
                }));


// Create axis and its renderer
// https://www.amcharts.com/docs/v5/charts/radar-chart/gauge-charts/#Axes
                var axisRenderer = am5radar.AxisRendererCircular.new(root, {
                    innerRadius: -10,
                    strokeOpacity: 1,
                    strokeWidth: 8
                });

                var xAxis = chart.xAxes.push(am5xy.ValueAxis.new(root, {
                    maxDeviation: 0,
                    min: 0,
                    max: 12,
                    strictMinMax: true,
                    renderer: axisRenderer,
                    maxPrecision: 0
                }));

// hides 0 value
                axisRenderer.labels.template.setAll({
                    minPosition: 0.02,
                    textType: "adjusted",
                    inside: true,
                    radius: 25
                });
                axisRenderer.grid.template.set("strokeOpacity", 1);


// Add clock hands
// https://www.amcharts.com/docs/v5/charts/radar-chart/gauge-charts/#Clock_hands

// hour
                var hourDataItem = xAxis.makeDataItem({});

                var hourHand = am5radar.ClockHand.new(root, {
                    radius: am5.percent(50),
                    topWidth: 12,
                    bottomWidth: 12,
                    pinRadius: 8,
                    layer: 5
                })

                hourDataItem.set("bullet", am5xy.AxisBullet.new(root, {
                    sprite: hourHand
                }));

                xAxis.createAxisRange(hourDataItem);

                hourDataItem.get("grid").set("visible", false);

// minutes
                var minutesDataItem = xAxis.makeDataItem({});

                var minutesHand = am5radar.ClockHand.new(root, {
                    radius: am5.percent(80),
                    topWidth: 8,
                    bottomWidth: 8,
                    pinRadius: 7,
                    layer: 5
                })

                minutesDataItem.set("bullet", am5xy.AxisBullet.new(root, {
                    sprite: minutesHand
                }));

                xAxis.createAxisRange(minutesDataItem);

                minutesDataItem.get("grid").set("visible", false);

// seconds
                var secondsDataItem = xAxis.makeDataItem({});

                var secondsHand = am5radar.ClockHand.new(root, {
                    radius: am5.percent(80),
                    topWidth: 3,
                    bottomWidth: 3,
                    pinRadius: 3,
                    layer: 5
                })

                secondsHand.hand.set("fill", am5.color(0xff0000));
                secondsHand.pin.set("fill", am5.color(0xff0000));

                secondsDataItem.set("bullet", am5xy.AxisBullet.new(root, {
                    sprite: secondsHand
                }));

                xAxis.createAxisRange(secondsDataItem);

                secondsDataItem.get("grid").set("visible", false);


                setInterval(function () {
                    updateHands(300)
                }, 1000);

                function updateHands(duration) {
                    // get current date
                    var date = new Date();
                    var hours = date.getHours();

                    if (hours > 12) {
                        hours -= 12;
                    }

                    var minutes = date.getMinutes();
                    var seconds = date.getSeconds();

                    // set hours
                    hourDataItem.set("value", hours + minutes / 60 + seconds / 60 / 60);
                    // set minutes
                    minutesDataItem.set("value", 12 * (minutes + seconds / 60) / 60);
                    // set seconds
                    var current = secondsDataItem.get("value");
                    var value = 12 * date.getSeconds() / 60;
                    // otherwise animation will go from 59 to 0 and the hand will move backwards
                    if (value == 0) {
                        value = 11.999;
                    }
                    // if it's more than 11.99, set it to 0
                    if (current > 11.99) {
                        current = 0;
                    }
                    secondsDataItem.animate({
                        key: "value",
                        from: current,
                        to: value,
                        duration: duration
                    });
                }

                updateHands(0);

// Make stuff animate on load
                chart.appear(1000, 100);

// update on visibility
                document.addEventListener("visibilitychange", function () {
                    updateHands(0)
                });

            }); // end am5.ready()
        }
    </script>
@endsection
