<?php

namespace App\Traits;

trait HasSlug
{
    public static function bootHasSlug()
    {
        static::saving(function ($model) {
            if (empty($model->slug) && $model->name) {
                $model->slug = \Str::upper(\Str::slug($model->name));
            }
        });
    }
}
