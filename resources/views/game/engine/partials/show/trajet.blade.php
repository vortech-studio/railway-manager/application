<ul class="pagination mb-10">
    <li class="page-item ">
        <a href="#" data-date="{{ now()->subDays(6) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J-6</span>
            <small>{{ now()->subDays(6)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->subDays(5) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J-5</span>
            <small>{{ now()->subDays(5)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->subDays(4) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J-4</span>
            <small>{{ now()->subDays(4)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->subDays(3) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J-3</span>
            <small>{{ now()->subDays(3)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->subDays(2) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J-2</span>
            <small>{{ now()->subDays(2)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->subDays(1) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J-1</span>
            <small>{{ now()->subDays(1)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item active">
        <a href="#" data-date="{{ now() }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-white">Aujourd'hui</span>
            <small>{{ now()->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->addDays(1) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J+1</span>
            <small>{{ now()->addDays(1)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->addDays(2) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J+2</span>
            <small>{{ now()->addDays(2)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->addDays(3) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J+3</span>
            <small>{{ now()->addDays(3)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->addDays(4) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J+4</span>
            <small>{{ now()->addDays(4)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->addDays(5) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J+5</span>
            <small>{{ now()->addDays(5)->format("d/m/Y") }}</small>
        </a>
    </li>
    <li class="page-item ">
        <a href="#" data-date="{{ now()->addDays(6) }}" class="page-link d-flex flex-column align-items-center">
            <span class="text-primary">J+6</span>
            <small>{{ now()->addDays(6)->format("d/m/Y") }}</small>
        </a>
    </li>
</ul>
<div class="card shadow-lg">
    <div class="card-body" id="loadTable">
        <div class="table-responsive">
            <div class="table-loading-message">
                Chargement...
            </div>
            <table class="table table-rounded gs-2 gy-2 gx-2 align-middle" id="liste_travel">
                <thead>
                <tr>
                    <th class="bg-success text-white fw-semibold">Train</th>
                    <th class="bg-primary text-white fw-semibold text-end">Ligne</th>
                    <th class="bg-primary text-white fw-semibold text-end">Départ / Arrivée</th>
                    <th class="bg-primary text-white fw-semibold text-center">PAX</th>
                    <th class="bg-primary text-white fw-semibold text-end">Chiffre d'affaire</th>
                    <th class="bg-primary text-white fw-semibold text-end">Résultat</th>
                    <th class="bg-primary"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($engine->plannings()->whereBetween('date_depart', [now()->startOfDay(), now()->endOfDay()])->get() as $planning)
                    @include('game.includes.table.table_travel', ["planning" => $planning])
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
