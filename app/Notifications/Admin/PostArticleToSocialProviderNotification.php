<?php

namespace App\Notifications\Admin;

use App\Models\Core\Blog\Articles;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Discord\DiscordChannel;
use NotificationChannels\Discord\DiscordMessage;
use NotificationChannels\FacebookPoster\FacebookPosterChannel;
use NotificationChannels\FacebookPoster\FacebookPosterPost;
use NotificationChannels\Twitter\Exceptions\CouldNotSendNotification;
use NotificationChannels\Twitter\TwitterChannel;
use NotificationChannels\Twitter\TwitterStatusUpdate;

class PostArticleToSocialProviderNotification extends Notification
{
    use Queueable;
    public function __construct(public Articles $article)
    {
    }

    public function via($notifiable): array
    {
        return [
            'mail',
            FacebookPosterChannel::class,
            TwitterChannel::class,
            DiscordChannel::class,
            'broadcast'
        ];
    }

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    public function toFacebookPoster($notifiable): FacebookPosterPost
    {
        return (new FacebookPosterPost($this->article->titre))
            ->withLink(route('blog.article.show', $this->article->id));
    }

    public function toTwitter($notifiable): TwitterStatusUpdate
    {
        return (new TwitterStatusUpdate($this->article->titre))
            ->withImage(\Storage::disk('public')->url('articles/' . $this->article->id . '.png'));
    }

    public function toDiscord($notifiable): DiscordMessage
    {
        return DiscordMessage::create("Un article à été publier sur les réseaux sociaux")
            ->embed(function ($embed) {
                $embed->title($this->article->titre)
                    ->description($this->article->content)
                    ->url(route('blog.article.show', $this->article->id))
                    ->image(\Storage::disk('public')->url('articles/' . $this->article->id . '.png'))
                    ->timestamp(now());
            });
    }

    public function toArray($notifiable): array
    {
        return [
            'type' => 'info',
            'icon' => 'ki-social-media',
            'title' => 'Un article à été publier sur les réseaux sociaux',
            'description' => $this->article->titre,
            'time' => now(),
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'type' => 'info',
            'icon' => 'ki-social-media',
            'title' => 'Un article à été publier sur les réseaux sociaux',
            'description' => $this->article->titre,
            'time' => now(),
            'userId' => $notifiable->id,
        ]);
    }

    public function broadcastOn()
    {
        return ['admin-notification'];
    }
}
