<?php

namespace App\Models\Core;

use BeyondCode\Vouchers\Models\Voucher;
use Illuminate\Database\Eloquent\Model;

class Vouchers extends Voucher
{

    protected $appends = ['model_type_name', "type_action_name"];
    public function getModelTypeNameAttribute()
    {
        return match ($this->model_type) {
            \App\Models\Core\Engine::class => "Engine",
            \App\Models\Core\Hub::class => "Hub",
            \App\Models\Core\Ligne::class => "Ligne",
            default => null
        };
    }

    public function getTypeActionNameAttribute()
    {
        return match ($this->data['action_type']) {
            'gift' => "Cadeaux",
            'reduce' => "Réduction",
            default => null
        };
    }
}
