<?php

namespace App\Http\Controllers\Api\Game;

use App\Http\Controllers\Controller;
use App\Models\User\UserHub;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HubController extends Controller
{
    public function list(Request $request)
    {
        $querys = UserHub::where('user_id', $request->get('user_id'))->get();

        $hubs = [];

        foreach ($querys as $item) {
            $lignes = [];
            foreach ($item->lignes as $ligne) {
                $stations_cos = [];

                foreach ($ligne->stations as $station) {
                    $stations_cos[] = [
                        $station->gare->longitude,
                        $station->gare->latitude,
                    ];
                }

                $stations = [];

                foreach ($ligne->ligne->stations as $station) {
                    $stations[] = [
                        "name" => $station->gare->name,
                        "nb_voyageur" => intval($station->gare->getPrevPassenger()),
                        "latitude" => $station->gare->latitude,
                        "longitude" => $station->gare->longitude,
                        "gares_id" => $station->gares_id
                    ];
                }
                if($ligne->plannings()->where('date_depart', '>=', Carbon::now())->count() != 0) {
                    $lignes[] = [
                        "id" => $ligne->id,
                        "name" => $ligne->ligne->name,
                        "type" => $ligne->ligne->type_ligne,
                        "next_departure" => $ligne->plannings()->count() != 0 ? $ligne->plannings()->where('date_depart', '>=', Carbon::now())->first()->date_depart->format("H:i") : "Non Planifier",
                        "station_start" => $ligne->ligne->stationStart,
                        "station_end" => $ligne->ligne->stationEnd,
                        "stations" => $stations,
                        "coordinates" => $stations_cos,
                    ];
                }else {
                    $lignes[] = [
                        "id" => $ligne->id,
                        "name" => $ligne->ligne->name,
                        "type" => $ligne->ligne->type_ligne,
                        "next_departure" => "Non Planifier",
                        "station_start" => $ligne->ligne->stationStart,
                        "station_end" => $ligne->ligne->stationEnd,
                        "stations" => $stations,
                        "coordinates" => $stations_cos,
                    ];
                }
            }

            $plannings = [];

            foreach ($item->plannings()->whereBetween('date_depart', [now()->startOfDay(), now()->endOfDay()])->get() as $planning) {
                $plannings[] = [
                    "heure_depart" => $planning->date_depart->format("H:i"),
                    "status_format" => $planning->status_format,
                    "destination" => $planning->ligne->ligne->stationEnd->name,
                    "voie" => $planning->ligne->quai,
                    "number" => $planning->number_travel,
                    "type" => [
                        "name" => $planning->ligne->ligne->type_ligne,
                        "logo" => $planning->ligne->ligne->type_ligne_logo
                    ]
                ];
            }

            $hubs[] = [
                "id" => $item->id,
                "name" => $item->hub->gare->name,
                "latitude" => $item->hub->gare->latitude,
                "longitude" => $item->hub->gare->longitude,
                "nb_slot_commerce" => $item->hub->nb_slot_commerce,
                "nb_slot_pub" => $item->hub->nb_slot_pub,
                "nb_slot_parking" => $item->hub->nb_slot_parking,
                "status" => $item->hub->getStatus(),
                "freq_actual" => $item->hub->calcFreqActual(),
                "lignes" => $lignes,
                "plannings" => $plannings
            ];
        }

        $hubs = collect($hubs);

        return response()->json(["hubs" => $hubs]);
    }

    public function info(Request $request, $user_hub_id)
    {
        $hub = UserHub::find($user_hub_id);
        $lignes = [];

        $plannings = [];

        foreach ($hub->plannings()->whereBetween('date_depart', [now()->startOfDay(), now()->endOfDay()])->get() as $planning) {
            $plannings[] = [
                "heure_depart" => $planning->date_depart->format("H:i"),
                "status_format" => $planning->status_format,
                "destination" => $planning->ligne->ligne->stationEnd->name,
                "voie" => $planning->ligne->quai,
                "number" => $planning->number_travel,
                "type" => [
                    "name" => $planning->ligne->ligne->type_ligne,
                    "logo" => $planning->ligne->ligne->type_ligne_logo
                ]
            ];
        }

        foreach ($hub->lignes as $ligne) {
            $stations_cos = [];

            foreach ($ligne->ligne->stations as $station) {
                $stations_cos[] = [
                    $station->gare->longitude,
                    $station->gare->latitude,
                ];
            }

            $stations = [];

            foreach ($ligne->ligne->stations as $station) {
                $stations[] = [
                    "name" => $station->gare->name,
                    "nb_voyageur" => intval($station->gare->getPrevPassenger()),
                    "latitude" => $station->gare->latitude,
                    "longitude" => $station->gare->longitude,
                    "gares_id" => $station->gares_id
                ];
            }
            if($ligne->plannings()->where('date_depart', '>=', Carbon::now())->count() != 0) {
                $lignes[] = [
                    "id" => $ligne->id,
                    "name" => $ligne->ligne->name,
                    "type" => $ligne->ligne->type_ligne,
                    "next_departure" => $ligne->plannings()->count() != 0 ? $ligne->plannings()->where('date_depart', '>=', Carbon::now())->first()->date_depart->format("H:i") : "Non Planifier",
                    "station_start" => $ligne->ligne->stationStart,
                    "station_end" => $ligne->ligne->stationEnd,
                    "stations" => $stations,
                    "coordinates" => $stations_cos,
                ];
            }else {
                $lignes[] = [
                    "id" => $ligne->id,
                    "name" => $ligne->ligne->name,
                    "type" => $ligne->ligne->type_ligne,
                    "next_departure" => "Non Planifier",
                    "station_start" => $ligne->ligne->stationStart,
                    "station_end" => $ligne->ligne->stationEnd,
                    "stations" => $stations,
                    "coordinates" => $stations_cos,
                ];
            }
        }

        $hub = [
            "id" => $hub->id,
            "name" => $hub->hub->gare->name,
            "latitude" => $hub->hub->gare->latitude,
            "longitude" => $hub->hub->gare->longitude,
            "nb_slot_commerce" => $hub->hub->nb_slot_commerce,
            "nb_slot_pub" => $hub->hub->nb_slot_pub,
            "nb_slot_parking" => $hub->hub->nb_slot_parking,
            "status" => $hub->hub->getStatus(),
            "freq_actual" => $hub->hub->calcFreqActual(),
            "lignes" => $lignes,
            "plannings" => $plannings
        ];

        $hub = collect($hub);

        return response()->json(["hub" => $hub]);
    }
}
