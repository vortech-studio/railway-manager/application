<?php

namespace App\Http\Controllers\Game\Engine;

use App\Http\Controllers\Controller;
use App\Models\User\UserEngine;

class EngineController extends Controller
{
    public function index()
    {
        return view('game.engine.index');
    }

    public function show($user_engine_id)
    {
        return view('game.engine.show', [
            "engine" => UserEngine::find($user_engine_id)
        ]);
    }
}
