<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTechnicentreEngineTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_technicentre_engine', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('user_technicentre_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_engine_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_technicentre_engine');
    }
}
