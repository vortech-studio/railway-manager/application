<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic
Product Version: 8.1.8
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->
<head>
    <base href=""/>
    {{ seo()->render() }}
    <title></title>
    <meta charset="utf-8"/>
    <meta name="description"
          content="Créer votre empire ferroviaire - JEUX DE STRATÉGIE / GESTION DANS LE DOMAINE DU FERROVIAIRE"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name=”theme-color” content=”#6777ef”/>
    <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}" />
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Vendor Stylesheets(used for this page only)-->
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <link href="https://cdn.jsdelivr.net/npm/intro.js@7.2.0/minified/introjs.min.css" rel="stylesheet">
    <link href="https://api.mapbox.com/mapbox-gl-js/v2.14.1/mapbox-gl.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@animxyz/core">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <!--end::Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/style.bundle.css" rel="stylesheet" type="text/css"/>
    @vite(["resources/sass/custom.scss", "resources/css/app.css"])
    @livewireStyles
    @yield("css")
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_app_body" data-kt-app-header-fixed-mobile="true" data-kt-app-toolbar-enabled="true" class="app-default">
<!--begin::Theme mode setup on page load-->
<script>
    var defaultThemeMode = "light";
    var themeMode;
    if (document.documentElement) {
        if (document.documentElement.hasAttribute("data-bs-theme-mode")) {
            themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
        } else {
            if (localStorage.getItem("data-bs-theme") !== null) {
                themeMode = localStorage.getItem("data-bs-theme");
            } else {
                themeMode = defaultThemeMode;
            }
        }
        if (themeMode === "system") {
            themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
        }
        document.documentElement.setAttribute("data-bs-theme", themeMode);
    }
</script>
<!--end::Theme mode setup on page load-->
<!--begin::App-->
<div class="d-flex flex-column flex-root app-root" id="kt_app_root" data-intro="Bienvenue sur {{ config('app.name') }}<br>Nous allons découvrir ensemble comment construire votre empire ferroviaire !">
    <!--begin::Page-->
    <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
        <!--begin::Header-->
        <div id="kt_app_header" class="app-header" data-kt-sticky="true" data-kt-sticky-activate="{default: false, lg: true}" data-kt-sticky-name="app-header-sticky" data-kt-sticky-offset="{default: false, lg: '300px'}">
            <!--begin::Header container-->
            <div class="app-container container-xxl d-flex align-items-stretch justify-content-between" id="kt_app_header_container">
                <!--begin::Header mobile toggle-->
                <div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show sidebar menu">
                    <div class="btn btn-icon btn-color-gray-600 btn-active-color-primary w-35px h-35px" id="kt_app_header_menu_toggle">
                        <i class="ki-outline ki-abstract-14 fs-2"></i>
                    </div>
                </div>
                <!--end::Header mobile toggle-->
                <!--begin::Logo-->
                <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0 me-lg-15">
                    <a href="{{ route('home') }}">
                        <img alt="Logo" src="{{ asset('/storage/logos/logo-carre-color.png') }}" class="h-25px d-lg-none" />
                        <img alt="Logo" src="{{ asset('/storage/logos/logo-long-color.png') }}" class="h-25px d-none d-lg-inline app-sidebar-logo-default theme-light-show" />
                        <img alt="Logo" src="{{ asset('/storage/logos/logo-long-white.png') }}" class="h-25px d-none d-lg-inline app-sidebar-logo-default theme-dark-show" />
                    </a>
                </div>
                <!--end::Logo-->
                @include("game.includes.header")
            </div>
            <!--end::Header container-->
        </div>
        <!--end::Header-->
        <!--begin::Wrapper-->
        <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
            <!--begin::Toolbar-->
            <div id="kt_app_toolbar" class="app-toolbar py-6" style="background-color: #1a237e">
                <!--begin::Toolbar container-->
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex align-items-start">
                    <!--begin::Toolbar container-->
                    <div class="d-flex flex-column flex-row-fluid" data-intro="Dans cette barre de status, vous pourrez suivre vos finances et votre parc de manière général">
                        <!--begin::Toolbar wrapper-->
                        <div class="d-flex align-items-center pt-1">
                            {{ Breadcrumbs::render() }}
                        </div>
                        <!--end::Toolbar wrapper=-->
                        @yield("info_bar")
                    </div>
                    <!--end::Toolbar container=-->
                </div>
                <!--end::Toolbar container-->
            </div>
            <!--end::Toolbar-->
            <!--begin::Wrapper container-->
            <div class="app-container container-xxl">
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content">
                            @include("admin.includes.alerts")
                            @yield("content")
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <div id="kt_app_footer" class="app-footer d-flex flex-column flex-md-row align-items-center flex-center flex-md-stack py-2 py-lg-4">
                        <!--begin::Copyright-->
                        <div class="text-dark order-2 order-md-1">
                            <span class="text-muted fw-semibold me-1">2023&copy;</span>
                            <a href="https://railway-manager.ovh" target="_blank" class="text-gray-800 text-hover-primary">Railway Manager</a> by <a href="https://vortechstudio.fr" target="_blank" class="text-gray-800 text-hover-primary">Vortech Studio</a>
                        </div>
                        <!--end::Copyright-->
                        <!--begin::Menu-->
                        <ul class="menu menu-gray-600 menu-hover-primary fw-semibold order-1">
                            <li class="menu-item">
                                <a href="https://patch.railway-manager.ovh" target="_blank" class="menu-link px-2">Changelog</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://vortechstudio.atlassian.net/servicedesk/customer/portal/3" target="_blank" class="menu-link px-2">Support</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://starter.productboard.com/vortechstudio-starter/1-railway-manager" target="_blank" class="menu-link px-2">Roadmap</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://status.vortechstudio.fr" target="_blank" class="menu-link px-2">Status</a>
                            </li>
                        </ul>
                        <!--end::Menu-->
                    </div>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper container-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
<!--end::App-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <i class="ki-outline ki-arrow-up"></i>
</div>
<input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
<released-widget channel-id="d8f08f94-1544-4dd7-acba-8eeaf08ddf3b"></released-widget>
<!--end::Scrolltop-->
<!--begin::Javascript-->
<script>var hostUrl = "/assets/";</script>
<!--begin::Global Javascript Bundle(mandatory for all pages)-->
<script src="/assets/plugins/global/plugins.bundle.js"></script>
<script src="/assets/js/scripts.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta3/dist/js/bootstrap-select.min.js"></script>
<script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src="https://code.iconify.design/iconify-icon/1.0.7/iconify-icon.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/intro.js@7.2.0/intro.min.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/v2.14.1/mapbox-gl.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/5.4.3/echarts.min.js" integrity="sha512-EmNxF3E6bM0Xg1zvmkeYD3HDBeGxtsG92IxFt1myNZhXdCav9MzvuH/zNMBU1DmIPN6njrhX1VTbqdJxQ2wHDg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
@auth
    <script src="{{ asset('/assets/js/enable-push.js') }}" defer></script>
@endauth
<script src="{{ asset('/assets/js/game/ago.js') }}"></script>
<script src="{{ asset('/assets/js/engineSpeed.js') }}"></script>
<script src="{{ asset('/assets/js/action.js') }}"></script>
@include('game.includes.tour')

<!--end::Global Javascript Bundle-->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-V2W6EBBN8X"></script>
<script src="https://embed.released.so/1/embed.js" defer></script>
<script src="{{ asset('/sw.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-V2W6EBBN8X');

    Pusher.logToConsole = true;
    let pusher = new Pusher('{{ config('broadcasting.connections.pusher.key') }}', {
        cluster: '{{ config('broadcasting.connections.pusher.options.cluster') }}' ?? 'mt1',
    });

    let channel_delivery = pusher.subscribe('delivery-channel');
    channel_delivery.bind("Illuminate\\Notifications\\Events\\BroadcastNotificationCreated", function(data) {

        if(data.user_id == {{ auth()->user()->id }}) {
            toastr({
                type: data.type,
                title: data.title,
                message: data.description,
                options: {
                    "closeButton": true,
                    "progressBar": true,
                    "positionClass": "toast-bottom-left",
                    "timeOut": "5000",
                    "extendedTimeOut": "2000",
                }
            })
        }
    });
    @if(auth()->user()->admin)
    let channel_admin = pusher.subscribe('admin-channel');
    channel_admin.bind("Illuminate\\Notifications\\Events\\BroadcastNotificationCreated", function(data) {

        if(data.user_id == {{ auth()->user()->id }}) {
            toastr({
                type: data.type,
                title: data.title,
                message: data.description,
                options: {
                    "closeButton": true,
                    "progressBar": true,
                    "positionClass": "toast-top-left",
                    "timeOut": "5000",
                    "extendedTimeOut": "2000",
                }
            })
        }
    });
    @endif
    document.addEventListener('DOMContentLoaded', function() {
        if ("serviceWorker" in navigator) {
            // Register a service worker hosted at the root of the
            // site using the default scope.
            navigator.serviceWorker.register("/sw.js").then(
                (registration) => {
                    console.log("Service worker registration succeeded:", registration);
                },
                (error) => {
                    console.error(`Service worker registration failed: ${error}`);
                },
            );
        } else {
            console.error("Service workers are not supported.");
        }
    });
</script>
<!--end::Javascript-->
@livewireScripts
@yield("script")
</body>
<!--end::Body-->
</html>
