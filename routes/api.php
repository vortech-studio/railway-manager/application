<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('command')->group(function () {
    Route::get('/import-gare', \App\Http\Controllers\Api\ImportGareController::class)->name('api.command.import-gare');
    Route::get('/export-gare', \App\Http\Controllers\Api\ExportGareController::class)->name('api.command.export-gare');
});

Route::prefix('core')->group(function () {
    Route::post('/calcSuperficie', [\App\Http\Controllers\Api\CoreController::class, 'calcSuperficie'])->name('api.core.calcSuperficie');
    Route::prefix('gare')->group(function () {
        Route::get('/', [\App\Http\Controllers\Api\GareController::class, 'index'])->name('api.core.gare.index');
        Route::get('/search', [\App\Http\Controllers\Api\GareController::class, 'search'])->name('api.core.gare.search');
        Route::get('/{gare_id}', [\App\Http\Controllers\Api\GareController::class, 'show'])->name('api.core.gare.show');
        Route::get('/{gare_id}/is-hub', [\App\Http\Controllers\Api\GareController::class, 'is_hub'])->name('api.core.gare.is_hub');
        Route::delete('/{gare_id}', [\App\Http\Controllers\Api\GareController::class, 'destroy'])->name('api.core.gare.destroy');
    });

    Route::prefix('hub')->group(function () {
        Route::get('/liste', [\App\Http\Controllers\Api\HubController::class, 'liste'])->name('api.core.hub.list');
        Route::get('/export', [\App\Http\Controllers\Api\HubController::class, 'export'])->name('api.core.hub.export');
        Route::get('/import', [\App\Http\Controllers\Api\HubController::class, 'import'])->name('api.core.hub.import');
        Route::put('{hub_id}/desactive', [\App\Http\Controllers\Api\HubController::class, 'desactive'])->name('api.core.hub.desactive');
        Route::put('{hub_id}/active', [\App\Http\Controllers\Api\HubController::class, 'active'])->name('api.core.hub.active');
        Route::delete('{hub_id}/delete', [\App\Http\Controllers\Api\HubController::class, 'destroy'])->name('api.core.hub.destroy');
        Route::get('{hub_id}', [\App\Http\Controllers\Api\HubController::class, 'info'])->name('api.core.hub.info');
        Route::get('{hub_id}/info_ligne/{ligne_id}', [\App\Http\Controllers\Api\HubController::class, 'infoLigne'])->name('api.core.hub.infoLigne');
    });

    Route::prefix('ligne')->group(function () {
        Route::get('/export', [\App\Http\Controllers\Api\LigneController::class, 'export'])->name('api.core.ligne.export');
        Route::get('/import', [\App\Http\Controllers\Api\LigneController::class, 'import'])->name('api.core.ligne.import');
        Route::get('{id}', [\App\Http\Controllers\Api\LigneController::class, 'info'])->name('api.core.ligne.info');
        Route::get('{id}/calculateDistance', [\App\Http\Controllers\Api\LigneController::class, 'calculateDistance'])->name('api.core.ligne.calculateDistance');
        Route::get('{id}/calculatePrice', [\App\Http\Controllers\Api\LigneController::class, 'calculatePrice'])->name('api.core.ligne.calculatePrice');
        Route::prefix('{id}/station')->group(function () {
            Route::delete('{station_id}', [\App\Http\Controllers\Api\LigneStationController::class, 'destroy'])->name('api.core.ligne.station.destroy');
        });
        Route::prefix('{id}/requirement')->group(function () {
            Route::delete('{requirement_id}', [\App\Http\Controllers\Api\LigneRequirementController::class, 'destroy'])->name('api.core.ligne.requirement.destroy');
        });

    });

    Route::prefix('engine')->group(function () {
        Route::get('/import', [\App\Http\Controllers\Api\EngineController::class, 'import'])->name('api.core.engine.import');
        Route::get('/export', [\App\Http\Controllers\Api\EngineController::class, 'export'])->name('api.core.engine.export');
        Route::get('/list', [\App\Http\Controllers\Api\EngineController::class, 'list'])->name('api.core.engine.list');
        Route::get('{id}', [\App\Http\Controllers\Api\EngineController::class, 'info'])->name('api.core.engine.info');
    });

    Route::prefix("infra")->group(function () {
        Route::get('/import', [\App\Http\Controllers\Api\InfraController::class, 'import'])->name('api.core.infra.import');
        Route::get('/export', [\App\Http\Controllers\Api\InfraController::class, 'export'])->name('api.core.infra.export');
    });

    Route::prefix('blog')->group(function () {
        Route::get('/list', [\App\Http\Controllers\Api\BlogController::class, 'list'])->name('api.core.blog.list');
        Route::put('/{id}/dispublish', [\App\Http\Controllers\Api\BlogController::class, 'dispublish'])->name('api.core.blog.dispublish');
        Route::put('/{id}/publish', [\App\Http\Controllers\Api\BlogController::class, 'publish'])->name('api.core.blog.publish');
    });

    Route::prefix('badges')->group(function() {
        Route::prefix('achievements')->group(function() {
            Route::get('/{id}', [\App\Http\Controllers\Api\BadgeAchievementController::class, 'info'])->name('api.core.badges.achievements.show');
            Route::post('/', [\App\Http\Controllers\Api\BadgeAchievementController::class, 'store'])->name('api.core.badges.achievements.store');
            Route::put('/{id}', [\App\Http\Controllers\Api\BadgeAchievementController::class, 'update'])->name('api.core.badges.achievements.update');
            Route::delete('/{id}', [\App\Http\Controllers\Api\BadgeAchievementController::class, 'destroy'])->name('api.core.badges.achievements.destroy');
        });

        Route::prefix('levels')->group(function() {
            Route::get('/{id}', [\App\Http\Controllers\Api\BadgeLevelController::class, 'info'])->name('api.core.badges.levels.show');
            Route::delete('/{id}', [\App\Http\Controllers\Api\BadgeLevelController::class, 'destroy'])->name('api.core.badges.levels.destroy');
        });
    });

    Route::prefix('voucher')->group(function () {
        Route::get('/import', [\App\Http\Controllers\Api\VoucherController::class, 'import'])->name('api.core.voucher.import');
        Route::get('/export', [\App\Http\Controllers\Api\VoucherController::class, 'export'])->name('api.core.voucher.export');

        Route::get('/liste-product', [\App\Http\Controllers\Api\VoucherController::class, 'listeProduct'])->name('api.core.voucher.listeProduct');
    });

    Route::prefix('users')->group(function () {
        Route::get('{id}', [\App\Http\Controllers\Api\UserController::class, 'info'])->name('api.core.users.info');
        Route::post('{id}/checkout', [\App\Http\Controllers\Api\UserController::class, 'checkout'])->name('api.core.users.checkout');
        Route::get('{id}/hubs', [\App\Http\Controllers\Api\UserController::class, 'hubs'])->name('api.core.users.hubs');
        Route::post('{id}/hubs', [\App\Http\Controllers\Api\UserController::class, 'addHub'])->name('api.core.users.addHub');
        Route::get('{id}/lignes', [\App\Http\Controllers\Api\UserController::class, 'lignes'])->name('api.core.users.lignes');
        Route::post('{id}/lignes', [\App\Http\Controllers\Api\UserController::class, 'addLigne'])->name('api.core.users.addLigne');
    });

});

Route::prefix('account/{user_id}')->group(function () {
    Route::post('/update-password', [\App\Http\Controllers\Api\AccountController::class, 'updatePassword'])->name('api.account.updatePassword');
    Route::post('/update-email', [\App\Http\Controllers\Api\AccountController::class, 'updateEmail'])->name('api.account.updateEmail');
    Route::post('/exchange-code', [\App\Http\Controllers\Api\AccountController::class, 'exchangeCode'])->name('api.account.exchangeCode');
    Route::post('/reset-account', [\App\Http\Controllers\Api\AccountController::class, 'resetAccount'])->name('api.account.resetAccount');
    Route::post('/automated-planning', [\App\Http\Controllers\Api\AccountController::class, 'autoPlanning'])->name('api.account.autoPlanning');

    Route::prefix('compta')->group(function () {
        Route::get('/getChartInfo', [\App\Http\Controllers\Api\ComptaController::class, 'getChartInfo'])->name('api.account.compta.getChartInfo');
        Route::get('/subvention', [\App\Http\Controllers\Api\ComptaController::class, 'subvention'])->name('api.account.compta.subvention');
    });

    Route::prefix('engine')->group(function () {
        Route::get('/available', [\App\Http\Controllers\Api\EngineController::class, 'available'])->name('api.account.engine.available');
        Route::get('/charts', [\App\Http\Controllers\Api\EngineController::class, 'charts'])->name('api.account.engine.charts');
    });

    Route::prefix('/planning')->group(function () {
        Route::get('/', [\App\Http\Controllers\Api\Game\Network\PlanningController::class, 'index'])->name('api.account.planning.index');
        Route::get('/engine/{engine_id}', [\App\Http\Controllers\Api\Game\Network\PlanningController::class, 'engine'])->name('api.account.planning.engine');
        Route::get('/engine/{engine_id}/check', [\App\Http\Controllers\Api\Game\Network\PlanningController::class, 'checkAddEvent'])->name('api.account.planning.checkAddEvent');
        Route::post('/engine/{engine_id}/add', [\App\Http\Controllers\Api\Game\Network\PlanningController::class, 'addEvent'])->name('api.account.planning.addEvent');
        Route::post('/engine/{engine_id}/edit', [\App\Http\Controllers\Api\Game\Network\PlanningController::class, 'editEvent'])->name('api.account.planning.editEvent');
        Route::post('/engine/{engine_id}/delete', [\App\Http\Controllers\Api\Game\Network\PlanningController::class, 'deleteEvent'])->name('api.account.planning.deleteEvent');

        Route::prefix('travel')->group(function () {
            Route::get('/{id}', [\App\Http\Controllers\Api\Game\Network\PlanningTravelController::class, 'info'])->name('api.account.planning.travel.info');
        });
    });
});

Route::prefix('game')->group(function () {
    Route::get("/gatcha", [\App\Http\Controllers\Api\GameController::class, 'gatcha'])->name('api.game.gatcha');
    Route::get("/accel", [\App\Http\Controllers\Api\GameController::class, 'accel'])->name('api.game.accel');
    Route::get('/claim-bonus', [\App\Http\Controllers\Api\GameController::class, 'claimBonus'])->name('api.game.claimBonus');
    Route::get('verify', [\App\Http\Controllers\Api\GameController::class, 'verify'])->name('api.game.verify');
    Route::get('accelerate', [\App\Http\Controllers\Api\GameController::class, 'accelerate'])->name('api.game.accelerate');

    Route::prefix('network')->group(function () {
        Route::get('/', [\App\Http\Controllers\Api\Game\Network\HubController::class, 'list'])->name('api.game.network.hub.list');
        Route::get('/{id}', [\App\Http\Controllers\Api\Game\Network\HubController::class, 'info'])->name('api.game.network.hub.info');
        Route::post('/{id}/sell', [\App\Http\Controllers\Api\Game\Network\HubController::class, 'hubSell'])->name('api.game.network.hub.sell');
        Route::get('/{id}/plannings', [\App\Http\Controllers\Api\Game\Network\HubController::class, 'plannings'])->name('api.game.network.hub.plannings');
        Route::get('/{id}/lines', [\App\Http\Controllers\Api\Game\Network\HubController::class, 'lines'])->name('api.game.network.hub.lines');
        Route::get('/{id}/lines/{line_id}', [\App\Http\Controllers\Api\Game\Network\HubController::class, 'line'])->name('api.game.network.hub.line');
        Route::post('/{id}/lines/{line_id}/sell', [\App\Http\Controllers\Api\Game\Network\HubController::class, 'lineSell'])->name('api.game.network.ligne.sell');
    });

    Route::prefix('hub')->group(function () {
        Route::get('/list', [\App\Http\Controllers\Api\Game\HubController::class, 'list'])->name('api.game.hub.list');
        Route::get('/{id}', [\App\Http\Controllers\Api\Game\HubController::class, 'info'])->name('api.game.hub.info');
    });

    Route::get('/travel/{id}', [\App\Http\Controllers\Api\Game\Network\PlanningTravelController::class, 'map'])->name('api.game.network.travel.map');

    Route::prefix('maintenance')->group(function () {
        Route::prefix('incident')->group(function () {
            Route::get('/list', [\App\Http\Controllers\Api\Game\Maintenance\IncidentController::class, 'list'])->name('api.game.maintenance.incident.list');
            Route::get('/{id}', [\App\Http\Controllers\Api\Game\Maintenance\IncidentController::class, 'info'])->name('api.game.maintenance.incident.info');
        });

        Route::prefix('maintenance')->group(function () {
            Route::post('/group/wear', [\App\Http\Controllers\Api\Game\Maintenance\GroupController::class, 'wear'])->name('api.game.maintenance.group.wear');
            Route::post('/', [\App\Http\Controllers\Api\Game\Maintenance\GroupController::class, 'store'])->name('api.game.maintenance.store');
        });

        Route::prefix('engine')->group(function () {
            Route::get('/transfer', [\App\Http\Controllers\Api\Game\Maintenance\EngineController::class, 'transfer'])->name('api.game.maintenance.engine.transfer');
            Route::get('/planning', [\App\Http\Controllers\Api\Game\Maintenance\EngineController::class, 'planning'])->name('api.game.maintenance.engine.planning');
            Route::get('/travels', [\App\Http\Controllers\Api\Game\Maintenance\EngineController::class, 'travels'])->name('api.game.maintenance.engine.travels');
            Route::delete('/', [\App\Http\Controllers\Api\Game\Maintenance\EngineController::class, 'sell'])->name('api.game.maintenance.engine.sell');
        });
    });

    Route::prefix('ecran')->group(function () {
        Route::get('/departure/list', [\App\Http\Controllers\Api\Game\Ecran\DepartureController::class, 'list'])->name('api.game.ecran.departure.list');
        Route::get('/departure/{travel_id}', [\App\Http\Controllers\Api\Game\Ecran\DepartureController::class, 'travel'])->name('api.game.ecran.departure.travel');
        Route::get('/travel/{travel_id}', [\App\Http\Controllers\Api\Game\Ecran\TravelController::class, 'travel'])->name('api.game.ecran.travel');
        Route::get('/travel/{travel_id}/map', [\App\Http\Controllers\Api\Game\Ecran\TravelController::class, 'getMapUri'])->name('api.game.ecran.travel.map');
        Route::get('/travel/{travel_id}/arrival', [\App\Http\Controllers\Api\Game\Ecran\TravelController::class, 'arrival'])->name('api.game.ecran.travel.arrival');
    });
});

Route::prefix('vigimat')->group(function () {
    Route::get('/technicentre/{technicentre_id}', [\App\Http\Controllers\Api\VigimatController::class, 'technicentre'])->name('api.vigimat.technicentre');
    Route::get('/charts', [\App\Http\Controllers\Api\VigimatController::class, 'charts'])->name('api.vigimat.charts');

    Route::prefix('engines')->group(function () {
        Route::get('/audit', [\App\Http\Controllers\Api\VigimatController::class, 'audit'])->name('api.vigimat.engines.audit');
        Route::get('{user_engine_id}', [\App\Http\Controllers\Api\VigimatController::class, 'userEngine'])->name('api.vigimat.engines.userEngine');
        Route::get('{user_engine_id}/osmose', [\App\Http\Controllers\Api\VigimatController::class, 'osmoseEngine'])->name('api.vigimat.engines.userEngine');
        Route::get('{user_engine_id}/audit', [\App\Http\Controllers\Api\VigimatController::class, 'userEngineAudit'])->name('api.vigimat.engines.userEngineAudit');

        Route::prefix('{user_engine_id}/maintenance')->group(function () {
            Route::post('/preventive', [\App\Http\Controllers\Api\VigimatController::class, 'maintenancePreventive'])->name('api.vigimat.engines.maintenance.preventive');
            Route::get('/{maintenance_id}', [\App\Http\Controllers\Api\VigimatController::class, 'showMaintenancePreventive'])->name('api.vigimat.engines.maintenance.showPreventive');
        });
    });
});
