<?php

namespace App\Jobs\Game\Planning;

use App\Models\User\UserPlanning;
use App\Notifications\Game\SendMessageNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TransitTravelJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserPlanning $planning)
    {
    }

    public function handle(): void
    {
        $this->planning->planning_stations()->first()->update([
            'status' => 'done',
        ]);

        $this->planning->update([
            "status" => "travel",
        ]);

        $this->planning->logs()->create([
            'message' => $this->planning->engine->engine->name . " en transit vers la gare de ".$this->planning->ligne->ligne->stationEnd->name,
            "user_planning_id" => $this->planning->id,
        ]);

        $this->planning->user->notify(new SendMessageNotification(
            'En transit !',
            $this->planning->engine->engine->name . " en transit vers la gare de ".$this->planning->ligne->ligne->stationEnd->name,
            "info",
            asset('/storage/icons/train_engine.png'),
            route('travel.show', $this->planning->id)
        ));

        foreach ($this->planning->planning_stations() as $station) {
            if($station->ligne_station_id != $this->planning->ligne->ligne->stationStart->id && $station->ligne_station_id != $this->planning->ligne->ligne->stationEnd->id) {
                dispatch(new InStationTravelJob($station))->delay(now()->addMinutes($station->ligneStation->time));
            }
        }
    }
}
