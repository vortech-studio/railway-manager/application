<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddingFieldsToUsers extends Migration
{
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('avatar')->default('default.png');
            $table->string('name_secretary')->nullable();
            $table->string('avatar_secretary')->default('default.png');
            $table->string('name_company')->nullable();
            $table->string('logo_company')->default('logo.png');
            $table->text('desc_company')->nullable();
            $table->boolean('premium')->default(false);
            $table->decimal('argent', 64, 0)->default(0);
            $table->decimal('tpoint', 64, 0)->default(0);
            $table->decimal('research', 64, 0)->default(0);
            $table->timestamp('last_login')->nullable();
            $table->enum('status', ['active', 'inactive', 'busy'])->default('inactive');
            $table->boolean('automated_planning')->default(false);
        });
    }

    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->removeColumn('avatar');
            $table->removeColumn('name_secretary');
            $table->removeColumn('avatar_secretary');
            $table->removeColumn('name_company');
            $table->removeColumn('logo_company');
            $table->removeColumn('desc_company');
            $table->removeColumn('premium');
            $table->removeColumn('argent');
            $table->removeColumn('tpoint');
            $table->removeColumn('reseach');
        });
    }
}
