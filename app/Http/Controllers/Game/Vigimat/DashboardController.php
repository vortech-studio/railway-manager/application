<?php

namespace App\Http\Controllers\Game\Vigimat;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return view('vigimat.dashboard');
    }

    public function quit()
    {
        \Session::remove('technicentre');

        return redirect()->route('home');
    }
}
