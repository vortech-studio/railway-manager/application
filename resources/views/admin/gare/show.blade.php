@extends("admin.template")

@section("css")
    <link rel="stylesheet" href="/assets/plugins/custom/leaflet/leaflet.bundle.css" />
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Gare: ".$gare->name,
        'action' => true,
        'data' => [
            [
                'url' => 'https://fr.wikipedia.org/wiki/Gare_'.formatTextForSlugify($gare->name),
                'title' => "En savoir plus sur la gare",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="/storage/gares/{{ $gare->id }}.jpg" alt="">
                <div class="card-body">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row align-items-center justify-content-between pb-3">
                            <span class="fw-bolder">Nom de la gare</span>
                            <span class="">{{ $gare->name }}</span>
                        </div>
                        <div class="separator separator-2"></div>
                        <div class="d-flex flex-row align-items-center justify-content-between pt-3 pb-3">
                            <span class="fw-bolder">Type de gare</span>
                            <span class="">{{ $gare->type_gare_string }}</span>
                        </div>
                        <div class="separator separator-2"></div>
                        <div class="d-flex flex-row align-items-center justify-content-between pt-3 pb-3">
                            <span class="fw-bolder">Région</span>
                            <span class="">{{ $gare->region }}</span>
                        </div>
                        <div class="separator separator-2"></div>
                        <div class="d-flex flex-row align-items-center justify-content-between pt-3 pb-3">
                            <span class="fw-bolder">Féquentation annuel</span>
                            <span class="">{{ number_format($gare->frequentation, 0, '', ' ') }}</span>
                        </div>
                        <div class="separator separator-2"></div>
                        <div class="d-flex flex-row align-items-center justify-content-between pt-3 pb-3">
                            <span class="fw-bolder">Nombre d'habitants</span>
                            <span class="">{{ number_format($gare->nb_habitant_city, 0, '', ' ') }}</span>
                        </div>
                        <div class="separator separator-2"></div>
                        <div class="d-flex flex-row align-items-center justify-content-between pt-3 pb-3">
                            <span class="fw-bolder">Temps de travail journalier de départ</span>
                            <span class="">{{ $gare->time_day_work }} heures</span>
                        </div>
                        <div class="separator separator-2"></div>
                        <div class="d-flex flex-row align-items-center justify-content-between pt-3 pb-3">
                            <span class="fw-bolder">Hub Transport</span>
                            {!! $gare->isHubIcon('fs-2') !!}
                        </div>
                        <div class="separator separator-2"></div>
                        <div class="d-flex flex-row align-items-center justify-content-between pt-3 pb-3">
                            <span class="fw-bolder">Désservie par</span>
                            {{ $gare->stations()->count() }} lignes
                        </div>
                        <div class="separator separator-2">
                            <div class="d-flex flex-row align-items-center justify-content-between pt-3 pb-3">
                                @foreach($gare->equipements as $equipement)
                                    <i class="fa-solid {{ $equipement->type_equipement_icon }} fs-2x" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ $equipement->type_equipement }}"></i>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="d-flex flex-row justify-content-between mb-5">
                <div class="d-flex flex-row justify-content-between">
                    @foreach($gare->transportations as $transportation)
                        <div class="symbol symbol-50px me-3">
                            <img src="/storage/logos/logo_{{ $transportation->type }}.svg" alt="{{ $transportation->type }}">
                        </div>
                    @endforeach

                </div>
                <div class="d-flex flex-row justify-content-end align-items-center">
                    {!! $gare->gareAccessIcon('commerce') !!}
                    {!! $gare->gareAccessIcon('pub') !!}
                    {!! $gare->gareAccessIcon('parking') !!}
                </div>
            </div>
            <div class="card shadow-sm mb-10">
                <div id="map" style="height: 300px" data-lat="{{ $gare->latitude }}" data-lng="{{ $gare->longitude }}"></div>
            </div>
            <div class="d-flex flex-row justify-content-between">
                <div class="d-flex flex-row rounded-3 bg-primary shadow-lg p-10 mx-3">
                    <div class="symbol symbol-50 me-3">
                        <span class="symbol-label bg-transparent">
                            <i class="fa-solid fa-road-barrier fs-3x text-white"></i>
                        </span>
                    </div>
                    <div class="d-flex flex-column">
                        <span class="fw-bolder fs-3 text-white">Nombre de quai</span>
                        <span class="fs-5 text-white text-end">{{ $gare->nb_quai }} de <strong>{{ $gare->long_quai }} m</strong></span>
                    </div>
                </div>
                <div class="d-flex flex-row rounded-3 bg-warning shadow-lg p-10 mx-3">
                    <div class="symbol symbol-50 me-3">
                        <span class="symbol-label bg-transparent">
                            <i class="fa-solid fa-ruler-combined fs-3x text-white"></i>
                        </span>
                    </div>
                    <div class="d-flex flex-column">
                        <span class="fw-bolder fs-3 text-white">Superficie Infrastructure</span>
                        <span class="fs-5 text-white text-end">{{ number_format($gare->superficie_infra, 0, '', ' ') }} m²</span>
                    </div>
                </div>
                <div class="d-flex flex-row rounded-3 bg-warning shadow-lg p-10 mx-3">
                    <div class="symbol symbol-50 me-3">
                        <span class="symbol-label bg-transparent">
                            <i class="fa-solid fa-ruler-combined fs-3x text-white"></i>
                        </span>
                    </div>
                    <div class="d-flex flex-column">
                        <span class="fw-bolder fs-3 text-white">Superficie Quai</span>
                        <span class="fs-5 text-white text-end">{{ number_format($gare->superficie_quai, 0, '', ' ') }} m²</span>
                    </div>
                </div>
            </div>
            @isset($gare->hub)
                <div class="separator separator-2 border-gray-500 my-5"></div>
                <x-base.underline
                    title="Hub Transport"
                    size="3"
                    size-text="fs-1" />
                <div class="row">
                    <div class="col-md-8">
                        <div class="d-flex flex-row justify-content-between align-items-center mb-3">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-30px me-3">
                                    <img src="/storage/icons/first-class.png" alt="First Class">
                                </div>
                                <span class="fs-3 text-gray-500">Passagers première classe</span>
                            </div>
                            <span class="fs-2 text-gray-500 fw-bolder border border-2 rounded-circle border-inoui-first p-2">{{ $gare->hub->passenger_first }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-30px me-3">
                                    <img src="/storage/icons/second-class.png" alt="First Class">
                                </div>
                                <span class="fs-3 text-gray-500">Passagers Seconde classe</span>
                            </div>
                            <span class="fs-2 text-gray-500 fw-bolder border border-2 rounded-circle border-inoui-second p-2">{{ $gare->hub->passenger_second }}</span>
                        </div>
                        <div class="separator separator-2 border-gray-500 my-5"></div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-3">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-30px me-3">
                                    <i class="fa-solid fa-dumpster fs-1 text-dark"></i>
                                </div>
                                <span class="fs-3 text-gray-500">Nombre de commerce disponible</span>
                            </div>
                            <span class="fs-2 text-gray-500 fw-bolder">{{ $gare->hub->nb_slot_commerce }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-3">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-30px me-3">
                                    <i class="fa-solid fa-rectangle-ad fs-1 text-dark"></i>
                                </div>
                                <span class="fs-3 text-gray-500">Nombre d'espace pub disponible</span>
                            </div>
                            <span class="fs-2 text-gray-500 fw-bolder">{{ $gare->hub->nb_slot_pub }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-3">
                            <div class="d-flex flex-row align-items-center">
                                <div class="symbol symbol-30px me-3">
                                    <i class="fa-solid fa-square-parking fs-1 text-dark"></i>
                                </div>
                                <span class="fs-3 text-gray-500">Nombre de place de parking disponible</span>
                            </div>
                            <span class="fs-2 text-gray-500 fw-bolder">{{ $gare->hub->nb_slot_parking }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                            <span class="fw-bolder fs-3 text-gray-500">Prix du Hub</span>
                            <span class="fs-5 text-gray-500 text-end">{{ $gare->hub->format_price }}</span>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                            <span class="fw-bolder fs-3 text-gray-500">Taxe de voyage</span>
                            <span class="fs-5 text-gray-500 text-end">{{ $gare->hub->format_taxe }} / par voyage</span>
                        </div>
                    </div>
                </div>
            @endisset
        </div>
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/leaflet/leaflet.bundle.js"></script>
    <script type="text/javascript">
        function initMap() {
            let mapDiv = document.querySelector('#map')
            let latitude = mapDiv.getAttribute('data-lat')
            let longitude = mapDiv.getAttribute('data-lng')
            let map

            map = L.map(mapDiv).setView([latitude, longitude], 16)

            const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            })

            mainLayer.addTo(map)
            addMarker({title: '{{ $gare->name }}', lat: latitude, lng: longitude}, map)

        }

        function addMarker(options, map) {
            let marker = L.marker([options.lat, options.lng], { title: options.title })
            marker.addTo(map)
        }

        initMap()
    </script>
@endsection
