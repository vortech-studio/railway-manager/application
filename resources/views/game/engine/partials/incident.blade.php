@if($style == 'simple')
    <div id="simple" class="rounded bg-bluegrey-600 border border-5 border-gray-400">
        <div class="d-flex flex-column w-100">
            @foreach($incidents->orderBy('created_at')->get() as $incident)
                <div class="d-flex flex-row h-150px border-bottom border-gray-500 ">
                    <div class="d-flex flex-column justify-content-center align-items-center border-gray-500 border-end-dashed p-5">
                        <img class="mb-1" src="{{ $incident->planning->engine->engine->image_url }}" alt="">
                        <span class="fs-2 text-white">{{ $incident->planning->engine->engine->name }} - {{ $incident->planning->engine->number }}</span>
                        <small class="text-white">{{ $incident->planning->number_travel }}</small>
                    </div>
                    <div class="d-flex flex-column justify-content-center align-items-center border-gray-500 border-end-dashed w-25 p-5">
                        <div class="text-white fs-3">Ligne</div>
                        <span class="fs-2 text-white fw-bolder text-center">{{ $incident->planning->ligne->ligne->name }}</span>
                    </div>
                    <div class="d-flex flex-column justify-content-center align-items-center border-gray-500 border-end-dashed p-5 w-200px">
                        <div class="text-white fs-3">{{ $incident->planning->date_depart->format("d/m/Y") }}</div>
                        <span class="fs-2 text-white fw-bolder" data-bs-toggle="tooltip" title="Heure de départ">{{ $incident->planning->date_depart->format("H:i") }}</span>
                    </div>
                    <div class="d-flex flex-row justify-content-between align-items-center p-0">
                        <img src="{{ $incident->type->image_origin_url }}" alt="" class="w-200px ps-5 opacity-50 shadow-lg  me-10" data-bs-toggle="tooltip" title="{{ $incident->type->origin_text }}">
                        <div class="d-flex flex-column">
                            <button  data-incident-id="{{ $incident->id }}" class="btn btn-sm btn-primary btn-flex rounded-4 mb-2 showIncident">
                                <img src="{{ asset('/storage/icons/warning_'.$incident->niveau_indicator.'.png') }}" alt="" class="w-30px me-2 animation-blink">
                                <span>Détail de l'incident</span>
                            </button>
                            <a href="{{ route('engine.show', $incident->user_engine->id) }}" class="btn btn-sm btn-primary btn-flex rounded-4">
                                <img src="{{ asset('/storage/icons/train.png') }}" alt="" class="w-30px me-2">
                                <span>Détail de l'engin</span>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@elseif($style == 'advanced')
    <div id="advanced" class="rounded bg-bluegrey-600 border border-5 border-gray-400">
        <table id="listIncident" class="table table-bordered table-row-dashed table-row-gray-500 ">
            <tbody>
            @foreach($incidents->orderBy('created_at')->get() as $incident)
                <tr class="align-middle">
                    <td class="min-w-70px">
                        <img src="{{ $incident->planning->ligne->ligne->type_ligne_logo }}" class="w-50px" alt="">
                    </td>
                    <td class="min-w-100px fs-3 text-center">
                        <span class="text-white">{{ $incident->planning->engine->engine->name }} - {{ $incident->planning->engine->number }}</span>
                    </td>
                    <td class="min-w-100px fs-3 text-center">
                        <span class="text-white">{{ $incident->planning->number_travel }}</span>
                    </td>
                    <td class="min-w-100px fs-3 text-center">
                        <span class="text-white">{{ $incident->planning->date_depart->format("d/m/Y H:i") }}</span>
                    </td>
                    <td class="min-w-100px fs-3 text-center">
                        <span class="text-white">{{ $incident->planning->ligne->ligne->name }}</span>
                    </td>
                    <td class="min-w-50px text-center">
                        <img src="{{ $incident->niveau_image }}" class="w-45px" alt="">
                    </td>
                    <td class="min-w-100px fs-3 text-end">
                        <span class="text-white">- {{ $incident->amount_reparation_format }}</span>
                    </td>
                    <td class="text-center">
                        <a href="{{ route('engine.show', $incident->planning->engine->id) }}" class="btn btn-icon btn-flush">
                            <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px" alt="">
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@else
    <div id="simple" class="rounded bg-bluegrey-600 border border-5 border-gray-400">
        <div class="d-flex flex-column w-100">
            @foreach($incidents->orderBy('created_at')->get() as $incident)
                <div class="d-flex flex-row h-150px border-bottom border-gray-500 ">
                    <div class="d-flex flex-column justify-content-center align-items-center border-gray-500 border-end-dashed p-5">
                        <img class="mb-1" src="{{ $incident->planning->engine->engine->image_url }}" alt="">
                        <span class="fs-2 text-white">{{ $incident->planning->engine->engine->name }} - {{ $incident->planning->engine->number }}</span>
                        <small class="text-white">{{ $incident->planning->number_travel }}</small>
                    </div>
                    <div class="d-flex flex-column justify-content-center align-items-center border-gray-500 border-end-dashed w-25 p-5">
                        <div class="text-white fs-3">Ligne</div>
                        <span class="fs-2 text-white fw-bolder text-center">{{ $incident->planning->ligne->ligne->name }}</span>
                    </div>
                    <div class="d-flex flex-column justify-content-center align-items-center border-gray-500 border-end-dashed p-5 w-200px">
                        <div class="text-white fs-3">{{ $incident->planning->date_depart->format("d/m/Y") }}</div>
                        <span class="fs-2 text-white fw-bolder" data-bs-toggle="tooltip" title="Heure de départ">{{ $incident->planning->date_depart->format("H:i") }}</span>
                    </div>
                    <div class="d-flex flex-row justify-content-between align-items-center p-0">
                        <img src="{{ $incident->type->image_origin_url }}" alt="" class="w-200px ps-5 opacity-50 shadow-lg  me-10" data-bs-toggle="tooltip" title="{{ $incident->type->origin_text }}">
                        <div class="d-flex flex-column">
                            <button  data-incident-id="{{ $incident->id }}" class="btn btn-sm btn-primary btn-flex rounded-4 mb-2 showIncident">
                                <img src="{{ asset('/storage/icons/warning_'.$incident->niveau_indicator.'.png') }}" alt="" class="w-30px me-2 animation-blink">
                                <span>Détail de l'incident</span>
                            </button>
                            <a href="{{ route('engine.show', $incident->user_engine->id) }}" class="btn btn-sm btn-primary btn-flex rounded-4">
                                <img src="{{ asset('/storage/icons/train.png') }}" alt="" class="w-30px me-2">
                                <span>Détail de l'engin</span>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div id="advanced" class="rounded bg-bluegrey-600 border border-5 border-gray-400">
        <table id="listIncident" class="table table-bordered table-row-dashed table-row-gray-500 ">
            <tbody>
            @foreach($incidents->orderBy('created_at')->get() as $incident)
                <tr class="align-middle">
                    <td class="min-w-70px">
                        <img src="{{ $incident->planning->ligne->ligne->type_ligne_logo }}" class="w-50px" alt="">
                    </td>
                    <td class="min-w-100px fs-3 text-center">
                        <span class="text-white">{{ $incident->planning->engine->engine->name }} - {{ $incident->planning->engine->number }}</span>
                    </td>
                    <td class="min-w-100px fs-3 text-center">
                        <span class="text-white">{{ $incident->planning->number_travel }}</span>
                    </td>
                    <td class="min-w-100px fs-3 text-center">
                        <span class="text-white">{{ $incident->planning->date_depart->format("d/m/Y H:i") }}</span>
                    </td>
                    <td class="min-w-100px fs-3 text-center">
                        <span class="text-white">{{ $incident->planning->ligne->ligne->name }}</span>
                    </td>
                    <td class="min-w-50px text-center">
                        <img src="{{ $incident->niveau_image }}" class="w-45px" alt="">
                    </td>
                    <td class="min-w-100px fs-3 text-end">
                        <span class="text-white">- {{ $incident->amount_reparation_format }}</span>
                    </td>
                    <td class="text-center">
                        <a href="{{ route('engine.show', $incident->planning->engine->id) }}" class="btn btn-icon btn-flush">
                            <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px" alt="">
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif

<div class="modal fade" tabindex="-1" id="incidentModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-gray-500 modal-flush">
            <div class="modal-header shadow-lg bg-gray-700 text-white">
                <h3 class="modal-title text-white">
                    <img data-content="warning" src="" alt="" class="w-30px" />
                    <span data-content="title"></span>
                </h3>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">
                <div class="d-flex flex-row align-items-center">
                    <img data-content="image_origin" src="" class="w-250px rounded-2 me-5" alt="">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Status:</span>
                            <div data-content="status">

                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Heure de l'incident:</span>
                            <div data-content="date">

                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Type d'incident:</span>
                            <div data-content="type_incident">

                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Problème:</span>
                            <div data-content="probleme">

                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Montant des réparations:</span>
                            <div data-content="amount">

                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Répercution sur le traffic:</span>
                            <div data-content="traffic">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', () => {
        document.querySelectorAll('.showIncident').forEach(btn => {
            btn.addEventListener('click', (e) => {
                const incidentId = btn.getAttribute('data-incident-id');
                let modal = document.querySelector('#incidentModal');

                $.ajax({
                    url: '/api/game/maintenance/incident/'+incidentId,
                    data: {user_id: {{ auth()->user()->id }}},
                    success: data => {
                        modal.querySelector('.modal-title').querySelector('[data-content="warning"]').setAttribute('src', '/storage/icons/warning_'+data.incident.niveau_indicator+'.png');
                        modal.querySelector('.modal-title').querySelector('[data-content="title"]').innerHTML = data.incident.designation
                        modal.querySelector('.modal-body').querySelector('[data-content="date"]').innerHTML = data.incident.date_format
                        modal.querySelector('.modal-body').querySelector('[data-content="status"]').innerHTML = data.incident.status_label
                        modal.querySelector('.modal-body').querySelector('[data-content="type_incident"]').innerHTML = data.incident.type.designation
                        modal.querySelector('.modal-body').querySelector('[data-content="image_origin"]').setAttribute('src', data.incident.type.image_origin_url)
                        modal.querySelector('.modal-body').querySelector('[data-content="probleme"]').innerHTML = data.incident.designation
                        modal.querySelector('.modal-body').querySelector('[data-content="amount"]').innerHTML = data.incident.amount_reparation_format
                        modal.querySelector('.modal-body').querySelector('[data-content="traffic"]').innerHTML = data.incident.repercute_traffic_format
                    }
                })

                new bootstrap.Modal(modal).show();
            })
        })
    })
</script>
