@extends("admin.template")

@section("css")

@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Création d'une gare",
        'action' => false,
        'data' => [
            /*[
                'url' => route('admin.dashboard'),
                'title' => "Tableau de Bord",
                'class' => "primary"
            ],
            [
                'url' => route('admin.dashboard'),
                'title' => "Tableau de Bord",
                'class' => "primary"
            ]*/
        ]
    ])
@endsection

@section("content")
    <div class="card shadow-lg">
        <form action="{{ route('admin.gare.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <x-form.input
                            name="name"
                            label="Nom de la gare"
                            required="true" />
                    </div>
                    <div class="col-2">
                        <x-form.input
                            name="code_uic"
                            label="Code UIC de la gare"
                            required="true" />
                    </div>
                </div>
                <div class="mb-10">
                    <label for="type_gare" class="control-label required">Type de Gare</label>
                    <select class="form-control selectpicker" name="type_gare" id="type_gare" required="true">
                        <option value="">Sélectionnez un type de gare</option>
                        <option value="a">Grande Gare</option>
                        <option value="b">Moyenne Gare</option>
                        <option value="c">Petite Gare</option>
                        <option value="d">Halte</option>
                    </select>
                </div>
                <div class="row mb-10 align-items-center">
                    <div class="col-5">
                        <x-form.input
                            name="latitude"
                            label="Coordonnées GPS (Latitude)"
                            required="true" />
                    </div>
                    <div class="col-5">
                        <x-form.input
                            name="longitude"
                            label="Coordonnées GPS (Longitude)"
                            required="true" />
                    </div>
                    <div class="col-2">
                        <x-base.button
                            type="button"
                            id="getCoordonnees"
                            text="<i class='fa-solid fa-search me-2'></i>Rechercher"
                            class="btn-sm btn-outline btn-outline-info"
                            data-action="getCoordonnees" />

                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <x-form.input
                            name="region"
                            label="Zone régionnal de la gare"
                            required="true" />
                    </div>
                    <div class="col-4">
                        <x-form.input
                            name="pays"
                            label="Pays de résidence de la gare"
                            required="true" />
                    </div>
                </div>
                <div class="row mb-10 align-items-center">
                    <div class="col-4">
                        <x-form.input
                            name="nb_quai"
                            label="Nombre de voie de la gare"
                            required="true" />
                    </div>
                    <div class="col-4">
                        <x-form.input
                            name="long_quai"
                            label="Longueur des quais (m)"
                            required="true" />
                    </div>
                </div>


                <x-form.checkbox
                    name="ter"
                    label="Train TER"
                    value="true" />
                <x-form.checkbox
                    name="tgv"
                    label="TGV INOUI"
                    value="true" />
                <x-form.checkbox
                    name="intercite"
                    label="Train INTERCITÉS"
                    value="true" />
                <x-form.checkbox
                    name="transilien"
                    label="Transilien"
                    value="true" />
            </div>
            <div class="card-footer d-flex flex-end">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>
        </form>
    </div>
@endsection

@section("js")
    <script type="text/javascript">
        document.querySelector('#getCoordonnees').addEventListener('click', e => {
            e.preventDefault()
            let btn = e.target
            btn.setAttribute('data-kt-indicator', 'on')
            if(document.querySelector('[name="name"]').value === '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Veuillez renseigner le nom de la gare',
                })
                return false
            }

            $.ajax({
                url: '{{ route('api.core.gare.search') }}',
                data: {'type': 'coordonnees', 'query': document.querySelector('[name="name"]').value},
                success: function (data) {
                    if (data.status === 'success') {
                        document.querySelector('[name="latitude"]').value = data.data.latitude
                        document.querySelector('[name="longitude"]').value = data.data.longitude
                        document.querySelector('[name="region"]').value = data.data.region
                        document.querySelector('[name="pays"]').value = data.data.pays
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: data.message,
                        })
                    }
                },
                error: () => {
                    $.ajax({
                        url: '{{ route('api.core.gare.search') }}',
                        data: {'type': 'infos', 'name': document.querySelector('[name="name"]').value},
                        success: data => {
                            document.querySelector('[name="code_uic"]').value = data.uic_code;
                            document.querySelector('[name="longitude"]').value = data.longitude_entreeprincipale_wgs84;
                            document.querySelector('[name="latitude"]').value = data.latitude_entreeprincipale_wgs84;
                            document.querySelector('[name="region"]').value = data.gare_regionsncf_libelle;
                            document.querySelector('[name="pays"]').value = "France";
                        },
                        error: err => {
                            toastr.error("Impossible de trouver la gare distante, veuillez enregistrer les informations manuellement.")
                        }
                    })
                }
            })
        })

        document.querySelector('[name="name"]').addEventListener('blur', () => {
            $.ajax({
                url: '{{ route('api.core.gare.search') }}',
                data: {'type': 'infos', 'name': document.querySelector('[name="name"]').value},
                success: data => {
                    document.querySelector('[name="code_uic"]').value = data.uic_code;
                    document.querySelector('[name="longitude"]').value = data.longitude_entreeprincipale_wgs84;
                    document.querySelector('[name="latitude"]').value = data.latitude_entreeprincipale_wgs84;
                    document.querySelector('[name="region"]').value = data.gare_regionsncf_libelle;
                    document.querySelector('[name="pays"]').value = "France";
                },
                error: err => {
                    toastr.error("Impossible de trouver la gare distante, veuillez enregistrer les informations manuellement.")
                }
            })
        })
    </script>
@endsection
