<?php

namespace App\Models\Core;

use App\Services\DataGouv\DecoupageCommune;
use App\Services\Mapping\Mapbox;
use App\Services\Mapping\OpenRouteService;
use App\Services\SNCF\GareVoyageur;
use App\Services\SNCF\Quai;
use App\Traits\GareTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Gares extends Model
{
    use HasFactory, GareTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'commerce' => 'boolean',
        'pub' => 'boolean',
        'parking' => 'boolean',
    ];

    protected $appends = ['type_gare_string', 'passenger_unique', 'passenger_first', 'passenger_second'];

    public function transportations()
    {
        return $this->hasMany(GareTransportation::class);
    }

    public function hub()
    {
        return $this->hasOne(Hub::class);
    }

    public function stations()
    {
        return $this->hasMany(LigneStation::class);
    }

    public function equipements()
    {
        return $this->hasMany(GareEquipement::class);
    }

    public function weather()
    {
        return $this->hasOne(GareWeather::class);
    }

    public function getTypeGareStringAttribute(): string
    {
        return match ($this->type_gare) {
            'a' => 'Grande Gare',
            'b' => 'Moyenne Gare',
            'c' => 'Petite Gare',
            default => 'Gare de quartier'
        };
    }

    private function isHub()
    {
        return $this->type_gare == 'a';
    }

    public function isHubIcon($style = null)
    {
        if ($this->isHub()) {
            return "<i class='fa-solid fa-check text-success ".$style."'></i>";
        } else {
            return "<i class='fa-solid fa-times text-danger ".$style."'></i>";
        }
    }

    public function gareAccessIcon($type)
    {
        return match ($type) {
            'commerce' => $this->commerce ? "<i class='fa-solid fa-dumpster text-success fs-2x me-2' data-bs-toggle='tooltip' title='Commerce'></i>" : "<i class='fa-solid fa-dumpster text-danger fs-2x me-2' data-bs-toggle='tooltip' title='Commerce'></i>",
            'pub' => $this->pub ? "<i class='fa-solid fa-rectangle-ad text-success fs-2x me-2' data-bs-toggle='tooltip' title='Publicité'></i>" : "<i class='fa-solid fa-rectangle-ad text-danger fs-2x me-2' data-bs-toggle='tooltip' title='Publicité'></i>",
            'parking' => $this->parking ? "<i class='fa-solid fa-square-parking text-success fs-2x me-2' data-bs-toggle='tooltip' title='Parking'></i>" : "<i class='fa-solid fa-square-parking text-danger fs-2x me-2' data-bs-toggle='tooltip' title='Parking'></i>",
        };
    }

    public function createGareConsole($name, $code_uic, $type_gare, $nb_quai, $long_quai, Collection $transportation = null)
    {
        $api = new GareVoyageur();
        $cmn = new DecoupageCommune();
        $route = new OpenRouteService();
        $quai = new Quai();

        $superficie = $this->calcSuperficie(null, $type_gare);
        $frequentation = $api->setFrequentation($type_gare);

        $gare = Gares::create([
            "name" => $name,
            "code_uic" => $code_uic,
            "type_gare" => $type_gare,
            "latitude" => $route->geocodeSearch(null, $name)->get('lng'),
            "longitude" => $route->geocodeSearch(null, $name)->get('lat'),
            "region" => $route->geocodeMatchInfo(null, $name)->get('region'),
            "pays" => $route->geocodeMatchInfo(null, $name)->get('pays'),
            "superficie_infra" => $superficie,
            "superficie_quai" => $this->calcSuperficieQuai(null, $nb_quai, $long_quai),
            "long_quai" => $long_quai,
            "nb_quai" => $nb_quai,
            "commerce" => $this->hasCommerce(null, $type_gare),
            "pub" => $this->hasPub(null, $type_gare),
            "parking" => $this->hasParking(null, $superficie, $type_gare),
            "frequentation" => $frequentation,
            "nb_habitant_city" => $cmn->getPopulation(null, $frequentation, $name),
            "time_day_work" => $this->timeDayWork(null, $type_gare),
        ]);

        if($type_gare == 'a') {
            Hub::create([
                "price" => $this->calcPrice($gare),
                "taxe_hub" => $this->calcTaxeHub(null, $gare),
                "passenger_first" => $this->calcPassengerFirst($gare),
                "passenger_second" => $this->calcPassengerSecond($gare),
                "nb_slot_commerce" => $this->calcNbSlotCommerce($gare),
                "nb_slot_pub" => $this->calcNbSlotPub($gare),
                "nb_slot_parking" => $this->calcNbSlotParking($gare),
                "active" => false,
                "gares_id" => $gare->id,
            ]);
        }

        if($transportation->has('ter')) {
            GareTransportation::create([
                "type" => "ter",
                "gares_id" => $gare->id,
            ]);
        }

        if ($transportation->has('tgv')) {
            GareTransportation::create([
                "type" => "tgv",
                "gares_id" => $gare->id,
            ]);
        }

        if ($transportation->has('intercite')) {
            GareTransportation::create([
                "type" => "intercite",
                "gares_id" => $gare->id,
            ]);
        }

        if ($transportation->has('transilien')) {
            GareTransportation::create([
                "type" => "transilien",
                "gares_id" => $gare->id,
            ]);
        }

        return $gare;

    }

    public function getPrevPassenger()
    {
        return $this->getPassengerUniqueAttribute();
    }

    public function getPassengerUniqueAttribute()
    {
        return $this->frequentation / 365 / 24;
    }

    public function getPassengerFirstAttribute()
    {
        return $this->getPassengerUniqueAttribute() * 20 / 100;
    }

    public function getPassengerSecondAttribute()
    {
        return $this->getPassengerUniqueAttribute() * 80 / 100;
    }
}
