<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserTechnicentreTasking extends Model
{
    public $timestamps = false;

    protected $casts = [
        'start_at' => 'datetime',
        'end_at' => 'datetime',
    ];

    protected $guarded = [];

    public function task(): BelongsTo
    {
        return $this->belongsTo(UserTechnicentreTask::class, 'user_technicentre_task_id');
    }
}
