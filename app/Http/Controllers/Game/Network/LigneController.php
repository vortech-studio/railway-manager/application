<?php

namespace App\Http\Controllers\Game\Network;

use App\Http\Controllers\Controller;
use App\Jobs\Game\LigneDeliveryJob;
use App\Models\Core\Engine;
use App\Models\Core\Hub;
use App\Models\Core\Ligne;
use App\Models\Core\Mouvement;
use App\Models\User\UserEngine;
use App\Models\User\UserLigne;
use App\Models\User\UserLigneTarif;
use App\Traits\HasCheckout;
use Illuminate\Http\Request;

class LigneController extends Controller
{
    public function show($line_id)
    {
        $ligne = UserLigne::find($line_id);

        return view('game.network.line.show', compact('ligne'));
    }

    public function checkout()
    {
        seo()->title(config('app.name') . ' - Achat d\'une ligne');
        $hubs = Hub::where('active', true)->get();

        return view('game.network.line.checkout', compact('hubs'));
    }

    public function checkoutConfirm(Request $request)
    {
        $user = auth()->user();
        $hub = Hub::find($request->input('hub_id'));
        $ligne = Ligne::find($request->input('ligne_id'));
        $engine = UserEngine::find($request->input('engines_id'));

        if (HasCheckout::isOk($request->input('amount'), $user, 'argent')) {
            try {
                Mouvement::adding(
                    'charge',
                    'Achat d\'une ligne: ' . $ligne->name,
                    'achat_ligne',
                    $request->input('amount'),
                    $user->company->id,
                );

                if($user->admin) {
                    $user_ligne = UserLigne::create([
                        "date_achat" => now(),
                        "nb_depart_jour" => 0,
                        "quai" => rand(1, $ligne->hub->gare->nb_quai),
                        "active" => 1,
                        "user_hub_id" => $user->hubs()->where('hub_id', $request->input('hub_id'))->first()->id,
                        "ligne_id" => $ligne->id,
                        "user_composition_track_id" => null,
                        "user_id" => $user->id,
                        "user_engine_id" => $engine->id
                    ]);
                    $user_ligne->nb_depart_jour = UserLigne::calcNbDepartJour($user_ligne, $hub);
                    $user_ligne->save();

                    UserLigneTarif::createTarif($user_ligne, $user_ligne->user_engine->engine, $user_ligne->userHub->hub);
                } else {
                    $user_ligne = UserLigne::create([
                        "date_achat" => now(),
                        "nb_depart_jour" => 0,
                        "quai" => rand(1, $ligne->hub->gare->nb_quai),
                        "active" => 0,
                        "user_hub_id" => $user->hubs()->where('hub_id', $request->input('hub_id'))->first()->id,
                        "ligne_id" => $ligne->id,
                        "user_composition_track_id" => null,
                        "user_id" => $user->id,
                        "user_engine_id" => $engine->id
                    ]);


                    $user_ligne->nb_depart_jour = UserLigne::calcNbDepartJour($user_ligne, $hub);
                    $user_ligne->save();

                    UserLigneTarif::createTarif($user_ligne, $user_ligne->user_engine->engine, $user_ligne->userHub->hub);

                    $delivery = $user->livraisons()->create([
                        "type" => "ligne",
                        "designation" => "Ligne: " . $ligne->name,
                        "user_id" => $user->id,
                        "user_ligne_id" => $user_ligne->id
                    ]);

                    $delivery->end_at = now()->addMinutes($delivery->getRateFromDelivery());
                    $delivery->save();

                    $user->messages()->create([
                        "from" => $user->name_secretary,
                        "subject" => "Achat d'une nouvelle ligne: " . $ligne->name,
                        "message" => view('game.includes.mailbox.checkout_ligne', [
                            "ligne_name" => $ligne->name,
                            "amount" => $request->input('amount'),
                            "delivery_at" => $delivery->end_at->format('d/m/Y à H:i'),
                            "user" => $user
                        ]),
                        "important" => true,
                        "user_id" => $user->id,
                    ]);

                    dispatch(new LigneDeliveryJob($delivery, $user_ligne))->delay(now()->addMinutes($delivery->getRateFromDelivery()));
                }
                $user->addPoints(150);
                toastr()->success('Votre ligne a bien été achetée !');
            }catch (\Exception $exception) {
                \Log::emergency($exception->getMessage(), $exception->getTrace());
                toastr()->error('Une erreur s\'est produite. Veuillez rafraichir la page et reessayer.');
                return redirect()->back();
            }
        } else {
            toastr()->error('Vous n\'avez pas assez d\'argent pour acheter cette ligne !');
        }
        return redirect()->route('network');
    }
}
