<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToEngines extends Migration
{
    public function up(): void
    {
        Schema::table('engines', function (Blueprint $table) {
            $table->boolean('in_shop')->default(false);
            $table->boolean('in_game')->default(true);
            $table->integer('price_shop')->nullable();
            $table->enum('money_shop', ["tpoint", "argent", "euro"])->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('engines', function (Blueprint $table) {
            $table->dropColumn('in_shop');
            $table->dropColumn('in_game');
            $table->dropColumn('price_shop');
            $table->dropColumn('money_shop');
        });
    }
}
