<?php

namespace App\Observer;

use App\Enums\SkillActionTypeEnum;
use App\Models\User\UserLigne;
use App\Models\User\UserLigneTarif;

class UserLigneObserver
{
    public function created(UserLigne $userLigne): void
    {
        $userLigne->skills()->create([
            "name" => "Optimisation des horaires",
            "description" => "Grâce à l'équipe de dispatching ferroviaire, le nombre de départ pour la ligne est optimisé",
            "level" => true,
            "level_max" => 5,
            "level_multiplicator" => 1.2,
            "action" => "optimiseTime",
            "action_type" => SkillActionTypeEnum::PERCENT,
            "action_value" => 2,
            "coast_base" => 35000,
            "time_base" => 135,
            "requirements" => null,
            "user_ligne_id" => $userLigne->id,
            "image" => "optimize-time.png",
            "action_base_value" => UserLigne::calcNbDepartJour($userLigne, $userLigne->userHub->hub) / 5
        ]);

        $userLigne->skills()->create([
            "name" => "Amélioration des infrastuctures",
            "description" => "Grâce à l'équipe réseau & infrastructure, le nombre de passager pouvant emprunter la ligne est augmenté.",
            "level" => true,
            "level_max" => 5,
            "level_multiplicator" => 2,
            "action" => "optimiseInfra",
            "action_type" => SkillActionTypeEnum::PERCENT,
            "action_value" => 10,
            "coast_base" => 15000,
            "time_base" => 300,
            "requirements" => null,
            "user_ligne_id" => $userLigne->id,
            "image" => "optimize-infra.png",
            "action_base_value" => $userLigne->calcDemande() / 5
        ]);
    }

    public function updated(UserLigne $userLigne): void
    {
    }

    public function deleted(UserLigne $userLigne): void
    {
    }

    public function restored(UserLigne $userLigne): void
    {
    }

    public function forceDeleted(UserLigne $userLigne): void
    {
    }
}
