<div id="root">
    <div class="rows rows-departures">
        <div class="row-background" style="background-image: url('{{ asset('/storage/other/infogare/departure.svg') }}')"></div>
        <div class="row-group row-group-bar">
            <div class="row">
                <div class="col-first">
                    <div class="bar-informations text-scroll-x" style="padding-left: 0%;"></div>
                </div>
                <div class="col-second">
                    <div class="bar-clock">
                        <span id="clock-hours">23</span>
                        <span class="animation-blink">
                            <span class="animation-blink-clock">:</span>
                        </span>
                        <span id="clock-minutes">32</span>
                        <small id="clock-seconds">37</small></div>
                </div>
            </div>
        </div>
        <div id="group">
            @foreach(auth()->user()->plannings()->whereBetween('date_depart', [now(), now()->endOfDay()])->limit(7)->get() as $depart)
            <div class="row-train row-group-2 row-group py2">
                <div class="row">
                    <div class="col-first">
                        <div class="train-logo train-logo-ter">
                            <img src="{{ $depart->ligne->ligne->type_ligne_logo }}" alt="">
                        </div>
                    </div>
                    <div class="col-second-first">
                        <div class="animation-blink">
                            <div class="animation-blink-1">
                                <span class="text-type">{{ $depart->ligne->ligne->type_ligne_name }}</span><br>
                                <span class="text-number">{{ $depart->number_travel }}</span>
                            </div>
                            <div class="animation-blink-2 text-features-1">à l'heure</div>
                        </div>
                    </div>
                    <div class="col-second-second text-time">{{ $depart->date_depart->format("H:i") }}</div>
                    <div class="col-second-third"><span>{{ $depart->ligne->ligne->stationEnd->name }}</span></div>
                    <div class="col-hide"></div>
                    @if($depart->date_depart->diffInMinutes(now()) < 5)
                    <div class="col-third">
                        <div class="train-track train-track-view">
                            <span>{{ $depart->ligne->quai }}</span>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-first"></div>
                    <div class="col-second">
                        <div class="text-scroll-x animation-scroll-x"
                             style="animation-duration: 19.3319s; padding-left: 100%;">
                            @foreach($depart->ligne->ligne->stations as $station)
                                <span>{{ $station->gare->name }}</span>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-third">
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
