@extends("game.template")

@section("css")
    <link src="{{ asset('/plugins/leaflet/leaflet.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center my-5">
        <div class="d-flex flex-column p-5 rounded bg-white text-dark">
            <img src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}" class="w-80px"/>
            <div class="fs-3 fw-bold">{{ auth()->user()->name_company }}</div>
        </div>
        <button class="btn btn-sm btn-primary" id="btnShowActu"><i class="fa-solid fa-newspaper me-2"></i> Vie du rail
        </button>
    </div>
    <div class="row mb-10">
        <div class="col-md-4">
            <div class="card shadow-lg"
                 data-intro="La vie est plus sympa quand ont à des défis à réaliser, l'équipe de développeur vous à préparer une séries de défi à réaliser avec à la clef des récompenses.">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa-solid fa-calendar fs-2 text-dark me-3"></i> Agenda</h3>
                </div>
                <div class="card-body scroll h-200px">
                    <div class="d-flex flex-column">
                        @foreach(\App\Models\Core\Badge::all() as $achievement)
                            <a href=""
                               class="d-flex flex-row justify-content-between align-items-center mb-5 text-hover-primary">
                                <div class="d-flex flex-row">
                                    <div class="symbol symbol-40 bg-light-{{ random_color() }} me-5">
                                        <img src="{{ asset('/storage/badges/'.$achievement->action.'.png') }}"
                                             alt="Badge"
                                             class="">
                                        @if($achievement->action_count > 1)
                                            <span
                                                class="symbol-badge badge badge-circle bg-primary start-100">{{ $achievement->action_count }}</span>
                                        @endif
                                    </div>
                                    <div class="d-flex flex-column text-dark">
                                        <span class="fw-bolder">{{ $achievement->name }}</span>
                                    </div>
                                </div>
                                @if(auth()->user()->badges()->where('badge_id', $achievement->id)->exists())
                                    <i class="fa-solid fa-circle-check fs-1 text-success"></i>
                                @endif
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex flex-center">
                        <a href="{{ route('account.objectif') }}"
                           class="btn btn-color-gray-600 btn-active-color-primary">Voir les objectifs <i
                                class="ki-outline ki-arrow-right fs-5"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow-lg"
                 data-intro="Sur cette carte, vous avez les informations financières global de votre firme.">
                <div class="card-header">
                    <h3 class="card-title fs-5">
                        <img src="{{ asset('/storage/icons/hq.png') }}" alt="Hub" class="w-20px h-20px me-3">
                        SITUATION DE VOTRE COMPAGNIE (S-1)
                    </h3>
                </div>
                <div class="card-body scroll h-200px">
                    <div class="d-flex flex-column">
                        <div
                            class="d-flex flex-row justify-content-between align-items-center fs-6 border-bottom border-gray-400 py-3">
                            <span>Chiffres d'affaires:</span>
                            <span>{{ eur(\App\Models\Core\Mouvement::getCAFromWeek(now()->subWeek())) }}</span>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center fs-6 border-bottom border-gray-400 py-3">
                            <span>Coût des trajets:</span>
                            <span>{{ eur(\App\Models\Core\Mouvement::getCostTravelFromWeek(now()->subWeek())) }}</span>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center fs-6 border-bottom border-gray-400 py-3">
                            <span>Résultats des trajets:</span>
                            <span>{{ eur(\App\Models\Core\Mouvement::getResultatTravelFromWeek(now()->subWeek())) }}</span>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center fs-6 border-bottom border-gray-400 py-3">
                            <span>Remboursement d'emprunt:</span>
                            <span>{{ eur(\App\Models\Core\Mouvement::getRembEmpruntFromWeek(now()->subWeek())) }}</span>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center fs-6 border-bottom border-gray-400 py-3">
                            <span>Coût des locations:</span>
                            <span>{{ eur(\App\Models\Core\Mouvement::getCostLocationFromWeek(now()->subWeek())) }}</span>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center fs-6 border-bottom border-gray-400 py-3">
                            <span>Trésorerie structurelle:</span>
                            <span>{{ eur(\App\Models\Core\Mouvement::getTresoStructurel(now()->subDay())) }}</span>
                        </div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center fs-6 border-bottom border-gray-400 py-3">
                            <span>Investissement R&D:</span>
                            <span>{{ eur(auth()->user()->research) }}</span>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex flex-center">
                        <a href="#" class="btn btn-color-gray-600 btn-active-color-primary">Comptabilité Générale <i
                                class="ki-outline ki-arrow-right fs-5"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow-lg"
                 data-intro="Nous somme sûr et certain que vous allez emprunter un peut d'argent à la banque, vous verrez ici un aperçu des prochaines échéances.">
                <div class="card-header">
                    <h3 class="card-title fs-5">
                        <img src="{{ asset('/storage/icons/bank.png') }}" alt="Hub" class="w-20px h-20px me-3">
                        EMPRUNTS, PROCHAINES ÉCHÉANCES
                    </h3>
                </div>
                <div class="card-body scroll h-200px">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Minibank</td>
                            <td class="text-danger text-end">1 852 000 €</td>
                            <td class="text-end">24/10/2023</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="d-flex flex-center">
                        <a href="#" class="btn btn-color-gray-600 btn-active-color-primary">Comptabilité Générale <i
                                class="ki-outline ki-arrow-right fs-5"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-10"
         data-intro="Ces graphiques représentes l'état de votre structures à plus ou moins long terme, n'hésitez pas à les consulter afin de prendre les bonnes décisions.">
        <div class="col-md-4">
            <div class="card shadow-lg">
                <div class="card-header">
                    <h3 class="card-title fs-5">
                        <img src="{{ asset('/storage/icons/financial.png') }}" alt="Hub" class="w-20px h-20px me-3">
                        ÉVOLUTION DE VOS RÉSULTATS
                    </h3>
                </div>
                <div class="card-body h-200px">
                    <div id="evoRsultatChart" style="height: 200px"></div>
                </div>
                <div class="card-footer">
                    <div class="d-flex flex-center">
                        <a href="#" class="btn btn-color-gray-600 btn-active-color-primary">Comptabilité Générale <i
                                class="ki-outline ki-arrow-right fs-5"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow-lg">
                <div class="card-header">
                    <h3 class="card-title fs-5">
                        <img src="{{ asset('/storage/icons/financial.png') }}" alt="Hub" class="w-20px h-20px me-3">
                        ÉVOLUTION DE VOTRE CA
                    </h3>
                </div>
                <div class="card-body h-200px">.
                    <div id="evoCa" style="height: 200px"></div>
                </div>
                <div class="card-footer">
                    <div class="d-flex flex-center">
                        <a href="#" class="btn btn-color-gray-600 btn-active-color-primary">Comptabilité Générale <i
                                class="ki-outline ki-arrow-right fs-5"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow-lg">
                <div class="card-header">
                    <h3 class="card-title fs-5">
                        <img src="{{ asset('/storage/icons/technician.png') }}" alt="Hub" class="w-20px h-20px me-3">
                        ÉVOLUTION DES INCIDENTS DE VOS MATERIELS ROULANTS
                    </h3>
                </div>
                <div class="card-body h-200px">
                    <div id="incident" style="height: 200px"></div>
                </div>
                <div class="card-footer">
                    <div class="d-flex flex-center">
                        <a href="#" class="btn btn-color-gray-600 btn-active-color-primary">Carnet de maintenance <i
                                class="ki-outline ki-arrow-right fs-5"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @premium
    <div class="row mb-10"
         data-intro="Vous êtes premium, alors très bien, cette carte représente l'ensemble de vos hubs et lignes de votre structure, impressionnant !">
        <div class="col-md-12">
            <div id="map" style="height: 450px;"></div>
        </div>
    </div>
    @endpremium
    @include("game.includes.ecran.eva.ecran_departure_list")

    <div class="modal fade" tabindex="-1" id="showActu" data-bs-backdrop="static">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="d-flex flex-row justify-content-between align-items-center">
                        <h3 class="modal-title" data-content="title"></h3>
                    </div>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                         aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body" data-content="content"></div>
            </div>
        </div>
    </div>
@endsection

@section("script")

    <script src="{{ asset('/assets/js/map.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        document.querySelector('#btnShowActu').addEventListener('click', e => {
            e.preventDefault()

            $.ajax({
                url: '/api/core/blog/list',
                type: 'GET',
                success: data => {
                    console.log(data)
                    let news = data[0]
                    let modal = new bootstrap.Modal(document.querySelector('#showActu'))

                    document.querySelector('#showActu .modal-title').innerHTML = news.titre
                    document.querySelector('#showActu .modal-body').innerHTML = news.content

                    modal.show()
                }
            })
        })

        @premium
        $.ajax({
            url: '/api/core/hub/liste',
            data: {"user_id": {{ auth()->user()->id }}},
            success: data => {
                data.hubs.forEach(hub => {
                    let map = initMap()
                    map.on('load', () => {
                        formatForHub(map, hub)
                        let upLigne = [];
                        hub.lignes.forEach(ligne => {
                            upLigne.push(ligne)
                            markerForStationEnd(map, ligne)
                        })
                        formatForLigne(map, upLigne)
                    })
                })
            }
        })
        @endpremium

        $.ajax({
            url: '{{ route('api.account.compta.getChartInfo', auth()->user()->id) }}',
            data: {
                "type": "resultat"
            },
            success: resultat => {
                let chartDom = document.getElementById('evoRsultatChart');
                let chartEvoRslt = echarts.init(chartDom);
                let options;

                options = {
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ["Résultat"]
                    },
                    xAxis: {
                        type: 'category',
                        data: resultat.labels,
                        boundaryGap: false,
                        inverse: true
                    },
                    yAxis: {
                        type: 'value',

                    },
                    series: [
                        {
                            data: resultat.data,
                            type: 'line',
                            smooth: true,
                            symbolSize: 10,
                            name: "Résultat"
                        }
                    ],
                }

                options && chartEvoRslt.setOption(options);

            }
        })
        $.ajax({
            url: '{{ route('api.account.compta.getChartInfo', auth()->user()->id) }}',
            data: {
                "type": "ca"
            },
            success: resultat => {
                let chartDom = document.getElementById('evoCa');
                let chartEvoRslt = echarts.init(chartDom);
                let options;

                options = {
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ["Chiffre d'affaire"]
                    },
                    xAxis: {
                        type: 'category',
                        data: resultat.labels,
                        boundaryGap: false,
                        inverse: true,
                    },
                    yAxis: {
                        type: 'value',

                    },
                    series: [
                        {
                            data: resultat.data,
                            type: 'line',
                            smooth: true,
                            symbolSize: 10,
                            name: "Chiffre d'affaire"
                        }
                    ],
                }

                options && chartEvoRslt.setOption(options);
            }
        })
        $.ajax({
            url: '{{ route('api.account.compta.getChartInfo', auth()->user()->id) }}',
            data: {
                "type": "incident"
            },
            success: resultat => {
                let chartDom = document.getElementById('incident');
                let chartEvoRslt = echarts.init(chartDom);
                let options;

                options = {
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ["Incidents"]
                    },
                    xAxis: {
                        type: 'category',
                        data: resultat.labels,
                        boundaryGap: false,
                        inverse: true,
                    },
                    yAxis: {
                        type: 'value',

                    },
                    series: [
                        {
                            data: resultat.data,
                            type: 'line',
                            smooth: true,
                            symbolSize: 10,
                            name: "Incidents"
                        }
                    ],
                }

                options && chartEvoRslt.setOption(options);
            }
        })
    </script>
@endsection
