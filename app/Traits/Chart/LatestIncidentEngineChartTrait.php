<?php

namespace App\Traits\Chart;

use App\Models\User\UserTechnicentre;

trait LatestIncidentEngineChartTrait
{
    public static function get($engine_id)
    {
        $technicentre = UserTechnicentre::find(\Session::get('technicentre')[0]['id']);
        $engine = $technicentre->engines()->find($engine_id);

        $data = [];

        for ($i=0; $i <= 30; $i++) {
            $data[] = [
                "date" => now()->subDays($i)->format('d/m'),
                "value" => $engine->engine->incidents()
                    ->join('type_incidents', 'type_incidents.id', '=', 'user_incidents.type_incident_id')
                    ->where('type_incidents.origine', 'materiels')
                    ->whereBetween('created_at', [now()->subDays($i)->startOfDay(), now()->subDays($i)->endOfDay()])
                    ->count()
            ];
        }

        return response()->json([
            "data" => $data
        ]);
    }
}
