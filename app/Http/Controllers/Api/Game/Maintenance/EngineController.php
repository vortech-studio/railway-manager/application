<?php

namespace App\Http\Controllers\Api\Game\Maintenance;

use App\Http\Controllers\Controller;
use App\Models\Core\Mouvement;
use App\Models\User\Mailbox;
use App\Models\User\UserEngine;
use App\Notifications\Game\SendMessageNotification;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Http\Request;

class EngineController extends Controller
{

    public function transfer(Request $request)
    {
        try {
            UserEngine::findOrFail($request->get('engine_id'))->update([
                'user_hub_id' => $request->get('to_engine_id')
            ]);
            return response()->json();
        } catch (\Exception $exception) {
            \Log::critical($exception->getMessage(), [$exception]);
            return response()->json(["message" => "Une erreur est survenue"], 500);
        }
    }

    public function sell(Request $request)
    {
        try {
            $engine = UserEngine::findOrFail($request->get('engine_id'));
            $amount_selling = $engine->calcSell();

            Mouvement::adding(
                'revenue',
                "Vente de matériel roulant: " . $engine->engine->name,
                $amount_selling,
                $engine->user->id,
                $engine->user_hub_id,
                $engine->user_ligne->id
            );

            $faker = Factory::create('fr_FR');
            Mailbox::create([
                "from" => Mailbox::generateFrom('vente_engine', $engine),
                "subject" => "Vente de matériel roulant: " . $engine->engine->name,
                "message" => view('game.includes.mailbox.selling_engine', [
                    "engine" => $engine,
                    "amount" => $amount_selling,
                    "name_responsable" => $faker->name,
                ]),
                "important" => true,
                "user_id" => $engine->user->id,
            ]);

            $engine->user->notify(new SendMessageNotification(
                "Vente de matériel roulant: " . $engine->engine->name,
                "La vente à été effectué avec succès",
                "success",
                asset('/storage/icons/train_check.png'),
                null,
                ["engine", "vigimat"]
            ));

            $engine->delete();
            return response()->json();

        } catch (\Exception $exception) {
            \Log::critical($exception->getMessage(), [$exception]);
            return response()->json(["message" => "Une erreur est survenue"], 500);
        }
    }

    public function planning(Request $request)
    {
        $engine = UserEngine::findOrFail($request->get('engine_id'));
        $querys = $engine->plannings()
            ->whereBetween('date_depart', [now()->startOfDay(), now()->endOfDay()])
            ->get();
        $plannings = [];

        foreach ($querys as $item) {
            $plannings[] = [
                "x" => $item->ligne->ligne->name,
                "y" => [
                    $item->date_depart->timestamp,
                    $item->date_depart->addMinutes($item->ligne->ligne->time_min)->timestamp,
                ]
            ];
        }

        return response()->json([
            "status" => "success",
            "data" => [
                "name" => $engine->engine->name,
                "data" => $plannings
            ],
        ]);
    }

    public function travels(Request $request)
    {
        $engine = UserEngine::findOrFail($request->get('engine_id'));
        if($request->has('date')) {
            $date = Carbon::createFromTimestamp(strtotime($request->get('date')));
            $diffInDay = $date->startOfDay()->diffInDays(now()->startOfDay());

            $querys = $engine->plannings()
                ->whereBetween('date_depart', [now()->addDays($diffInDay)->startOfDay(), $date->endOfDay()])
                ->get();

            ob_start();
            foreach ($querys as $item) {
                echo view('game.includes.table.table_travel', ["planning" => $item])->render();
            }
            $content = ob_get_clean();
        } else {
            $querys = $engine->plannings()
                ->get();
            $content = null;
        }

        return response()->json([
            "plannings" => $querys,
            "content" => $content
        ]);
    }
}
