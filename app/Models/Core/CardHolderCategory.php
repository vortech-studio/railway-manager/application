<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class CardHolderCategory extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function cards()
    {
        return $this->hasMany(CardHolder::class);
    }
}
