<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRentalsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_rentals', function (Blueprint $table) {
            $table->id();
            $table->timestamp('date_contract');
            $table->integer('duration_contract');
            $table->float('amount_caution');
            $table->float('amount_fee');
            $table->enum('status', ["active", "terminated", "default"])->default('active');

            $table->foreignId('user_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('rental_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('user_engine_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_rentals');
    }
}
