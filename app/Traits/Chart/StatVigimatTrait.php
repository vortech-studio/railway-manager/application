<?php

namespace App\Traits\Chart;

use App\Models\User\UserTechnicentre;

trait StatVigimatTrait
{
    public static function get()
    {
        $technicentre = UserTechnicentre::find(\Session::get('technicentre')[0]['id']);

        return response()->json([
            "nbActifEngine" => $technicentre->hub->engines()->where('status', 'travel')->count(),
            "nbInactifEngine" => $technicentre->hub->engines()->where('status', 'free')->count(),
            "nbFaultyEngine" => $technicentre->hub->engines()->where('status', 'default')->orWhere('status', 'in_maintenance')->count(),
            "totalIntervention" => $technicentre->tasks()->count(),
            "totalPreventive" => $technicentre->tasks()->where('type', 'engine_prev')->count(),
            "totalCurative" => $technicentre->tasks()->where('type', 'engine_cur')->where('type', 'infra')->count(),
            "checkIntervention" => $technicentre->tasks()->where('status', 'finished')->count(),
            "cancelIntervention" => $technicentre->tasks()->where('status', 'canceled')->count(),
            "totalTechnician" => $technicentre->staffs()->count(),
            "totalActifTechnician" => $technicentre->staffs()->where('status', 'active')->where('status', 'formation')->count(),
        ]);
    }
}
