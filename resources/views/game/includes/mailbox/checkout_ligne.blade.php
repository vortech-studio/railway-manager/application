Cher PDG,<br>
<br>
<p>Je me permets de vous contacter pour vous confirmer que l'achat de la ligne de transport ferroviaire <strong>{{ $ligne_name }}</strong> a été effectué avec succès pour un montant de <strong>{{ eur($amount) }}</strong>.</p>
<p>Le contrat d'achat a été signé ce jour, et nous avons effectué le paiement conformément aux conditions générales convenu. Nous nous attendons à ce que cette ligne de transport ferroviaire soit opérationnel d'ici le <strong>{{ $delivery_at }}</strong>.</p>
<p>Je suis convaincu que cette acquisition aura un impact significatif sur nos opérations et notre compétitivité.</p>
<p>Je vous prie d'agréer, Cher PDG, l'expression de ma considération distinguée.</p>
<p>{{ $user->name_secretary }}</p>
