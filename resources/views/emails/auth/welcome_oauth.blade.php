@extends("emails.template")

@section("media_card")
    <img alt="Logo" src="{{ asset('/assets/media/email/icon-positive-vote-1.svg') }}" />
@endsection

@section("synopsis")
    <!--begin:Text-->
    <div style="font-size: 14px; font-weight: 500; margin-bottom: 27px; font-family:Arial,Helvetica,sans-serif;">
        <p style="margin-bottom:9px; color:#181C32; font-size: 22px; font-weight:700">Hey {{ $user->name }}, bienvenue sur Railway Manager!</p>
        <p style="margin-bottom:2px; color:#7E8299">
            Vous avez choisi de vous connecter via un provider, votre compte a été authentifié avec succès.<br>
            Toute l'équipe de Railway Manager vous souhaite la bienvenue dans le simulateur ferroviaire Railway Manager.
        </p>
    </div>
    <!--end:Text-->
    <!--begin:Action-->
    <a href='{{ route('oauth.config') }}' target="_blank" style="background-color:#50cd89; border-radius:6px;display:inline-block; padding:11px 19px; color: #FFFFFF; font-size: 14px; font-weight:500;">Accès à Railway Manager</a>
    <!--begin:Action-->
@endsection

@section("content")

@endsection
