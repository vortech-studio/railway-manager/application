<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageToUserHubSkills extends Migration
{
    public function up(): void
    {
        Schema::table('user_hub_skills', function (Blueprint $table) {
            $table->string('image')->nullable();
        });

        Schema::table('user_ligne_skills', function (Blueprint $table) {
            $table->string('image')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('user_hub_skills', function (Blueprint $table) {
            $table->dropColumn('image');
        });

        Schema::table('user_ligne_skills', function (Blueprint $table) {
            $table->dropColumn('image');
        });

    }
}
