@extends("admin.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Gestion des codes d'échanges",
        'action' => true,
        'data' => [
            [
                'url' => route('admin.vourcher.create'),
                'title' => "Ajouter un nouveau code",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="card card-flush">
        <!--begin::Card header-->
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                    <input type="text" data-exchange-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Rechercher un code" />
                </div>
                <!--end::Search-->
            </div>
            <!--end::Card title-->
            <!--begin::Card toolbar-->
            <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                <button class="btn btn-outline btn-outline-primary" data-action="export-code"><i class="fa-solid fa-upload me-2"></i> Exporter les codes</button>
                <button class="btn btn-outline btn-outline-primary" data-action="import-code"><i class="fa-solid fa-download me-2"></i> importer les codes</button>
                <!--end::Add product-->
            </div>
            <!--end::Card toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body pt-0">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_code">
                <thead>
                <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                    <th class="min-w-200px">Code</th>
                    <th class="text-end min-w-100px">Secteur de promo</th>
                    <th class="text-end min-w-100px">Date expiration</th>
                    <th class="text-end min-w-70px">Actions</th>
                </tr>
                </thead>
                <tbody class="fw-semibold text-gray-600">
                @foreach($vourchers as $vourcher)
                <tr>
                    <td>{{ $vourcher->code }}</td>
                    <td>
                        {{ $vourcher->model_type_name }} ({{ $vourcher->model->name ?? $vourcher->model->gare->name  }})<br>
                        Type: <span class="fw-bold">{{ $vourcher->type_action_name }} {{ $vourcher->data["action_type"] != "gift" ? "(".$vourcher->data["action_value"]." %)" : "" }}</span>
                    </td>
                    <td>{{ $vourcher->expires_at->format("d/m/Y à H:i") }}</td>
                    <td class="text-end">
                        <a href="#" class="btn btn-sm btn-light btn-flex btn-center btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <i class="ki-duotone ki-down fs-5 ms-1"></i></a>
                        <!--begin::Menu-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3" data-kt-ecommerce-product-filter="delete_row" data-id="{{ $vourcher->id }}">Supprimer</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu-->
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let table = $('#liste_code').DataTable({
            info: !1,
            order: [],
            pageLength: 10,
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
        });

        document.querySelector('[data-exchange-filter="search"]').addEventListener('keyup', function (e) {
            table.search(e.target.value).draw();
        });


        document.querySelectorAll('[data-kt-ecommerce-product-filter="delete_row"]').forEach(function (btn) {
            btn.addEventListener('click', function (e) {
                e.preventDefault()
                let id = btn.dataset.id
                $.ajax({
                    url: "/api/core/hub/" + id + "/delete",
                    method: "DELETE",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id
                    },
                    success: function (data) {
                        console.log(data)
                        btn.removeAttribute('data-kt-indicator')
                        toastr.success("Le hub à été supprimé")
                        setTimeout(() => {
                            window.location.reload()
                        }, 1300)
                    }
                })
            })
        })

        document.querySelector('[data-action="export-code"]').addEventListener('click', function (e) {
            e.preventDefault()
            $.ajax({
                url: "/api/core/voucher/export",
                method: "GET",
                success: function (data) {
                    console.log(data)
                    toastr.success("Les codes ont été exportés")
                    setTimeout(() => {
                        window.location.reload()
                    }, 1300)
                }
            })
        })

        document.querySelector('[data-action="import-code"]').addEventListener('click', function (e) {
            e.preventDefault()
            $.ajax({
                url: "/api/core/voucher/import",
                method: "GET",
                success: function (data) {
                    console.log(data)
                    toastr.success("Les codes ont été importés")
                    setTimeout(() => {
                        window.location.reload()
                    }, 1300)
                }
            })
        })

    </script>
@endsection
