<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use LevelUp\Experience\Models\Level;

class BadgeLevelController extends Controller
{
   /**
    * Stocke un nouveau niveau.
    *
    * @param Request $request L'objet de requête contenant les données du niveau.
    * @throws \Illuminate\Validation\ValidationException Si la validation échoue.
    * @return \Illuminate\Http\RedirectResponse La réponse de redirection vers la page d'index des badges.
    */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            "level" => "required|integer",
            "next_level_experience" => "required|integer",
        ]);

        Level::create($validatedData);

        toast()->success('Niveau ajouté');
        return redirect()->route('admin.badge.index');
    }

    /**
     * Modifie un niveau.
     *
     * @param int $id L'identifiant du niveau à modifier.
     * @param Request $request Les données de la requête.
     * @return \Illuminate\Http\RedirectResponse La réponse de redirection.
     */
    public function edit($id, Request $request)
    {
        $request->validate([
            "level" => "required|integer",
            "next_level_experience" => "required|integer",
        ]);

        $level = Level::findOrFail($id);
        $level->level = $request->level;
        $level->next_level_experience = $request->next_level_experience;
        $level->save();

        toast()->success('Niveau modifié');
        return redirect()->route('admin.badge.index');
    }
}
