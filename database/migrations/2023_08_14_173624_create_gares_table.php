<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gares', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code_uic');
            $table->string('type_gare', 1)->default('c');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('region');
            $table->string('pays')->default('France');
            $table->string('superficie_infra');
            $table->string('superficie_quai');
            $table->integer('long_quai');
            $table->integer('nb_quai')->default(1);
            $table->boolean('commerce')->default(false);
            $table->boolean('pub')->default(false);
            $table->boolean('parking')->default(false);
            $table->string('frequentation');
            $table->string('nb_habitant_city');
            $table->integer('time_day_work');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gares');
    }
};
