Cher PDG,<br>
<br>
<p>Je me permets de vous contacter pour vous confirmer que l'achat du matériel roulant <strong>{{ $engine->name }}</strong> a été effectué avec succès pour un montant de <strong>{{ eur($amount) }}</strong>.</p>
<p>Le contrat d'achat a été signé ce jour, et nous avons effectué le paiement conformément aux conditions générales convenu. Nous nous attendons à ce que ce matériel soit livrée et opérationnel dans le hub <strong>{{ $hub_name }}</strong> d'ici le <strong>{{ $delivery_at }}</strong>.</p>
<p>De plus, j'ai déjà contacté la personne responsable des plannings des lignes pour ce hub de transport et il doit vous contacter une fois le matériel livrée afin de planifier le carnet de route de ce matériel roulant.</p>
<p>Je suis convaincu que cette acquisition aura un impact significatif sur nos opérations et notre compétitivité.</p>
<p>Je vous prie d'agréer, Cher PDG, l'expression de ma considération distinguée.</p>
<p>{{ $user->name_secretary }}</p>
