<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('engine_technicals', function (Blueprint $table) {
            $table->id();
            $table->decimal('longueur');
            $table->decimal('largeur');
            $table->decimal('hauteur');
            $table->enum('essieux', [
                "BB",
                "BOBO",
                "CC",
                "COCO"
            ]);
            $table->integer('max_speed');
            $table->integer('poids');
            $table->decimal('traction_force');
            $table->enum('type_motor', [
                "diesel",
                "electrique 1500V",
                "electrique 25000V",
                "electrique 1500V/25000V",
                "vapeur",
                "hybride",
                "autre"
            ]);
            $table->integer('power_motor');
            $table->dateTime('start_exploi');
            $table->dateTime('end_exploi');
            $table->integer('gasoil')->nullable();
            $table->enum('type_marchandise', [
                "none",
                "passagers",
                "marchandises",
            ]);
            $table->integer('nb_type_marchandise')->nullable();
            $table->integer('nb_wagon')->nullable()->comment("Uniquement si Automotrice");

            $table->foreignId('engine_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('engine_technicals');
    }
};
