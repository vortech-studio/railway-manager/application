.PHONY: testing
testing:
	php artisan update testing
	php artisan generate:bonus

changelog:
	php artisan changelog:add

install-staging:
	composer install --no-interaction --prefer-dist --optimize-autoloader
	npm install
	npm run build
	cp .env.testing .env
	php artisan storage:link
	php artisan key:generate
	php artisan down
	chmod -R 777 storage bootstrap/cache
	php artisan update testing
	php artisan generate:bonus
	php artisan system sitemap
	php artisan up

install-production:
	composer install --no-interaction --prefer-dist --optimize-autoloader
	npm install
	npm run build
	cp .env.production .env
	php artisan storage:link
	php artisan key:generate
	php artisan down
	chmod -R 777 storage bootstrap/cache
	php artisan update production
	php artisan generate:bonus
	php artisan system sitemap
	php artisan up

update-staging:
	php artisan down
	php artisan update testing
	php artisan generate:bonus
	php artisan system sitemap
	make clear
	php artisan up

update-production:
	php artisan down
	php artisan update production
	php artisan system sitemap
	make clear
	php artisan up

clear:
	php artisan cache:clear
	php artisan config:clear
	php artisan route:clear
	php artisan clear

