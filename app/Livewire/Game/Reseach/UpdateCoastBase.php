<?php

namespace App\Livewire\Game\Reseach;

use Livewire\Component;

class UpdateCoastBase extends Component
{
    public int $coastBase;

    public function updateCoastBase()
    {
        auth()->user()->company()->update([
            "research_coast_base" => $this->coastBase,
        ]);

        toastr()->success("Allocation de recherche mise à jours");
        $this->redirect(route('research.index'));
    }

    public function render()
    {
        return view('livewire.game.reseach.update-coast-base');
    }
}
