<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserTechnicentreStaf extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function technicentre()
    {
        return $this->belongsTo(UserTechnicentre::class, 'user_technicentre_id');
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(UserTechnicentreTask::class);
    }

    public function calcSalary()
    {
        $brut_horaire = 11.52 * ($this->niveau / 10) + 11.52;

        return $brut_horaire * 151.67;
    }

    public static function tableJob()
    {
        return collect([
            "Opérateur Maintenance",
            "Opérateur Sécurité",
            "Opérateur Infrastructure"
        ]);
    }
}
