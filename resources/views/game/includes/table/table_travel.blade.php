<tr @if($planning->status == 'initialized') class="table-active text-gray-500" data-bs-toggle="tooltip" data-bs-placement="top" title="Ce trajet n'à pas encore commencée" data-bs-custom-class="tooltip-inverse" @endif>
    <td><a href="" class="btn btn-link @if($planning->status == 'initialized') text-gray-300 @endif">{{ $planning->engine->engine->name }}</a></td>
    <td class="text-end">
                            <span class="" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ $planning->ligne->ligne->name }}">
                                {{ Str::upper(Str::limit($planning->ligne->ligne->stationStart->name, 3, '')) }}/{{ Str::upper(Str::limit($planning->ligne->ligne->stationEnd->name, 3, '')) }} <img src="{{ asset('/storage/logos/logo_'.$planning->ligne->ligne->type_ligne.'.svg') }}" alt="" class="w-15px" />
                            </span>
    </td>
    <td class="text-end">{{ $planning->date_depart->format("H:i") }} / {{ $planning->date_arrived->format("H:i") }}</td>
    <td class="text-center">
                            <span data-bs-toggle="popover" data-bs-html="true" title="{{ $planning->hub->calcSumPassengers(null, $planning->date_depart, $planning->date_arrived) }} Passagers" data-bs-content="Première classe: <span class='fw-bold'>{{ $planning->hub->calcSumPassengers('first', $planning->date_depart, $planning->date_arrived) }}</span><br>Seconde classe: <span class='fw-bold'>{{ $planning->hub->calcSumPassengers('second', $planning->date_depart, $planning->date_arrived) }}</span>">
                                {{ $planning->hub->calcSumPassengers('first', $planning->date_depart, $planning->date_arrived) +  $planning->hub->calcSumPassengers('second', $planning->date_depart, $planning->date_arrived) }} pax
                            </span>
    </td>
    <td class="text-end">{{ eur($planning->travel->calcCa()) }}</td>
    <td class="text-end">{{ eur($planning->travel->calcResultat()) }}</td>
    @if($planning->status != 'initialized')
        <td>
            <a href="{{ route('travel.show', $planning->id) }}" class="btn btn-link"><i class="fa-solid fa-magnifying-glass fs-3"></i></a>
        </td>
    @endif
</tr>
