<?php

namespace App\Jobs\Game\Planning;

use App\Models\User\UserPlanning;
use App\Notifications\Game\SendMessageNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DepartureTravelJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserPlanning $planning)
    {
    }

    public function handle(): void
    {

        $this->planning->logs()->create([
            'message' => $this->planning->engine->engine->name . " prêt au départ pour la gare de ".$this->planning->ligne->ligne->stationEnd->name,
            "user_planning_id" => $this->planning->id,
        ]);

        $this->planning->user->notify(new SendMessageNotification(
            'Attention au départ !',
            $this->planning->engine->engine->name . " prêt au départ pour la gare de ".$this->planning->ligne->ligne->stationEnd->name,
            "info",
            asset('/storage/icons/train_engine.png'),
            route('travel.show', $this->planning->id)
        ));

        dispatch(new TransitTravelJob($this->planning))->delay(now()->addMinutes(2));
        dispatch(new ArrivalTravelJob($this->planning))->delay(now()->addMinutes($this->planning->ligne->ligne->time_min));
    }
}
