<?php

namespace App\Services\SNCF;

use Http;

class LigneVoyageur
{
    public static function getListeLigneByUic($code_uic)
    {
        $call = Http::withBasicAuth('afab9dec-0e14-448c-8d44-c58ade24d0e2', '')
            ->get('https://api.navitia.io/v1/coverage/sncf/stop_areas/stop_area:SNCF:'.$code_uic.'/lines?count=400&');
        return $call->object()->lines;
    }

    public static function getRouteByRouteId($route_id)
    {
        $route = Http::withBasicAuth('afab9dec-0e14-448c-8d44-c58ade24d0e2', '')
            ->get('https://api.navitia.io/v1/coverage/sncf/routes/'.$route_id);

        return $route->object();
    }

    public static function getRouteScheduleByRouteId($route_id)
    {
        $route = Http::withBasicAuth('afab9dec-0e14-448c-8d44-c58ade24d0e2', '')
            ->get('https://api.navitia.io/v1/coverage/sncf/routes/'.$route_id.'/route_schedules');

        return $route->object();
    }

    public static function getStopScheduleByRouteId($route_id)
    {
        $stops = Http::withBasicAuth('afab9dec-0e14-448c-8d44-c58ade24d0e2', '')
            ->get('https://api.navitia.io/v1/coverage/sncf/routes/'.$route_id.'/stop_schedules');

        return $stops->object();
    }
}
