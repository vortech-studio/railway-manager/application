@extends("admin.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Gestion des Matériels Roulants",
        'action' => true,
        'data' => [
            [
                'url' => route('admin.engine.create'),
                'title' => "Ajouter un nouveau matériel roulant",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="card card-flush">
        <!--begin::Card header-->
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                    <input type="text" data-engine-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Rechercher un engin" />
                </div>
                <!--end::Search-->
            </div>
            <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                <div class="w-100 mw-150px">
                    <!--begin::Select2-->
                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Type de train" data-kt-engine-table-filter="type_engine">
                        <option></option>
                        <option value="all">All</option>
                        <option value="motrice">Motrice</option>
                        <option value="remorque">Remorque</option>
                        <option value="automotrice">Automotrice</option>
                    </select>
                    <!--end::Select2-->
                </div>
                <button class="btn btn-outline btn-outline-primary" data-action="export-engine"><i class="fa-solid fa-upload me-2"></i> Exporter les matériels roulants</button>
                <button class="btn btn-outline btn-outline-primary" data-action="import-engine"><i class="fa-solid fa-download me-2"></i> Importer les matériels roulants</button>
            </div>
            <!--end::Card title-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body pt-0">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_engine">
                <thead>
                <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                    <th class="w-10px pe-2">
                        <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                            <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_ecommerce_products_table .form-check-input" value="1" />
                        </div>
                    </th>
                    <th class="min-w-200px">Désignation</th>
                    <th class="text-end min-w-100px">Type de Train</th>
                    <th class="text-end min-w-70px">Actions</th>
                </tr>
                </thead>
                <tbody class="fw-semibold text-gray-600">
                @foreach($engines as $engine)
                <tr>
                    <td>
                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                            <input class="form-check-input" type="checkbox" value="1" />
                        </div>
                    </td>
                    <td>
                        <div class="d-flex flex-row align-items-center">
                            @if($engine->type_engine == 'automotrice')
                                <img src="/storage/engines/automotrice/{{ Str::upper(Str::slug($engine->name)) }}-0.gif" class="w-50px me-3" alt="">
                            @else
                                <img src="/storage/engines/{{ $engine->type_engine }}/{{ $engine->image }}" class="w-50px me-3" alt="">
                            @endif
                            <a href="{{ route('admin.engine.show', $engine->id) }}" class="btn btn-flush">{{ $engine->name }}</a>
                        </div>
                    </td>
                    <td class="text-center pe-0">
                        {{ $engine->type_engine_format }}
                        <div class="d-none">{{ $engine->type_engine }}</div>
                    </td>
                    <td class="text-end">
                        <a href="#" class="btn btn-sm btn-light btn-flex btn-center btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <i class="ki-duotone ki-down fs-5 ms-1"></i></a>
                        <!--begin::Menu-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                            <div class="menu-item px-3">
                                <a href="{{ route('admin.engine.show', $engine->id) }}" class="menu-link px-3">Voir la fiche</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3" data-action="delete" data-id="{{ $engine->id }}">Supprimer</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu-->
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let table = $('#liste_engine').DataTable({
            info: !1,
            order: [],
            pageLength: 10,
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
        });

        document.querySelector('[data-engine-filter="search"]').addEventListener('keyup', function (e) {
            table.search(e.target.value).draw();
        });

        document.querySelectorAll('[data-action="delete"]').forEach(function (element) {
            element.addEventListener('click', function (e) {
                e.preventDefault();
                let id = element.getAttribute('data-id');
                Swal.fire({
                    title: "Êtes-vous sûr ?",
                    text: "Vous ne pourrez pas revenir en arrière !",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Oui, supprimer !",
                    cancelButtonText: "Non, annuler !",
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        axios.delete('/admin/engines/' + id)
                            .then(function (response) {
                                Swal.fire({
                                    title: "Suppression !",
                                    text: "L'engin a été supprimé avec succès !",
                                    icon: "success",
                                    confirmButtonText: "Fermer",
                                }).then(function (result) {
                                    if (result.value) {
                                        element.closest('tr').remove();
                                    }
                                });
                            })
                            .catch(function (error) {
                                Swal.fire({
                                    title: "Erreur !",
                                    text: "L'engin n'a pas été supprimé !",
                                    icon: "error",
                                    confirmButtonText: "Fermer",
                                });
                            });
                    } else if (result.dismiss === "cancel") {
                        Swal.fire({
                            title: "Annulation",
                            text: "L'engin n'a pas été supprimé !",
                            icon: "info",
                            confirmButtonText: "Fermer",
                        });
                    }
                });
            });
        });

        document.querySelector('[data-action="export-engine"]').addEventListener('click', function (e) {
            e.preventDefault()
            $.ajax({
                url: "/api/core/engine/export",
                method: "GET",
                success: function (data) {
                    console.log(data)
                    toastr.success("Les Matériels Roulants ont été exportés")
                    setTimeout(() => {
                        window.location.reload()
                    }, 1300)
                }
            })
        })

        document.querySelector('[data-action="import-engine"]').addEventListener('click', function (e) {
            e.preventDefault()
            $.ajax({
                url: "/api/core/engine/import",
                method: "GET",
                success: function (data) {
                    console.log(data)
                    toastr.success("Les Matériels Roulants ont été importés")
                    setTimeout(() => {
                        window.location.reload()
                    }, 1300)
                }
            })
        })

        const engine = document.querySelector('[data-kt-engine-table-filter="type_engine"]')
        $(engine).on('change', (e => {
            console.log("CHANGE")
            let o = e.target.value
            "all" === o && (o = ""), table.column(2).search(o).draw()
        }))

    </script>
@endsection
