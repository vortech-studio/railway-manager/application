<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserCompanySituation extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    protected $casts = [
        'date' => 'datetime',
    ];

    public function company()
    {
        return $this->belongsTo(UserCompany::class, 'user_company_id');
    }

    public static function automatedCreate(UserCompany $company)
    {
        return self::create([
            "date" => now()->subWeek()->endOfWeek(),
            "turnover" => 0,
            "cost_travel" => 0,
            "remb_emprunt" => 0,
            "cost_location" => 0,
            "structural_cash" => 0,
            "benefice" => 0,
            "user_company_id" => $company->id,
        ]);
    }
}
