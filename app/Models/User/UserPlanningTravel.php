<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserPlanningTravel extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'user_planning_travels';

    public function userPlanning()
    {
        return $this->belongsTo(UserPlanning::class);
    }

    public function calcCA()
    {
        return $this->ca_billetterie + $this->ca_other;
    }

    public function calcCoast()
    {
        return $this->frais_electrique + $this->frais_gasoil + $this->frais_autre;
    }

    public function calcResultat()
    {
        $depense = $this->frais_electrique + $this->frais_gasoil + $this->frais_autre;
        return $this->calcCA() - $depense;
    }
}
