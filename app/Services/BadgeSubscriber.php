<?php

namespace App\Services;

use App\Models\Core\Badge;
use App\Models\Core\BadgeReward;
use App\Models\User;
use App\Models\User\UserHub;
use App\Notifications\Game\SendMessageNotification;
use LevelUp\Experience\Events\UserLevelledUp;

class BadgeSubscriber
{
    public function __construct(public Badge $badge)
    {
    }

    public function subscribe($events)
    {
        $events->listen('eloquent.created: App\Models\User\UserHub', [$this, 'onCheckoutHub']);
        $events->listen('eloquent.created: App\Models\User', [$this, 'newUser']);
        $events->listen('App\Events\RepairEngineEvent', [$this, 'repairEngine']);
        $events->listen(UserLevelledUp::class, [$this, 'levelUp']);
    }

    public function onCheckoutHub(UserHub $hub)
    {
        $user = $hub->user;
        $hub_count = $user->hubs()->count();
        $badge = $this->badge->unlockActionFor($user, 'checkout_hub', $hub_count);

        foreach ($badge->rewards as $reward) {
            $this->getRewardsForUser($user, $reward);
        }

        toastr()->success("Vous avez obtenu le badge : {$reward->badge->name}");
        $user->notify(new SendMessageNotification(
            'Vous avez obtenu le badge : ' . $reward->badge->name,
            "Vous avez obtenu le badge : {$reward->badge->name}",
            "success",
            asset('/storage/icons/ranking.png'),
            null,
            "badge"
        ));
    }
    public function newUser(User $user)
    {
        $badge = $this->badge->unlockActionFor($user, 'welcome', 1);

        foreach ($badge->rewards as $reward) {
            $this->getRewardsForUser($user, $reward);
        }

        toastr()->success("Vous avez obtenu le badge : {$reward->badge->name}");
        $user->notify(new SendMessageNotification(
            'Vous avez obtenu le badge : ' . $reward->badge->name,
            "Vous avez obtenu le badge : {$reward->badge->name}",
            "success",
            asset('/storage/icons/ranking.png'),
            null,
            "badge"
        ));
    }

    public function repairEngine(User $user)
    {
        $count_tasks = 0;

        foreach ($user->technicentres as $technicentre) {
            $count_tasks += $technicentre->tasks()
                ->where('status', 'finished')
                ->where('type', 'engine_prev')
                ->orWhere('type', 'engine_cur')
                ->count();
        }

        $badge = $this->badge->unlockActionFor($user, 'repairEngine', $count_tasks);

        foreach ($badge->rewards as $reward) {
            $this->getRewardsForUser($user, $reward);
        }

        toastr()->success("Vous avez obtenu le badge : {$reward->badge->name}");
        $user->notify(new SendMessageNotification(
            'Vous avez obtenu le badge : ' . $reward->badge->name,
            "Vous avez obtenu le badge : {$reward->badge->name}",
            "success",
            asset('/storage/icons/ranking.png'),
            null,
            "badge"
        ));
    }

    public function levelUp(UserLevelledUp $level)
    {
        $user = User::find($level->user->id);
        $badge = $this->badge->unlockActionFor($user, 'levelUp', $level->level);

        if($badge !== null) {
            foreach ($badge->rewards as $reward) {
                $this->getRewardsForUser($user, $reward);
            }

            toastr()->success("Vous avez obtenu le badge : {$this->badge->name}");
            $user->notify(new SendMessageNotification(
                'Vous avez obtenu le badge : ' . $this->badge->name,
                "Vous avez obtenu le badge : {$this->badge->name}",
                "success",
                asset('/storage/icons/ranking.png'),
                null,
                "badge"
            ));
        }

    }

    private function getRewardsForUser(mixed $user, BadgeReward $reward)
    {
        match ($reward->type) {
            'argent' => $this->rewardArgent($user, $reward->value),
            'tpoint' => $this->rewardTpoint($user, $reward->value),
            'research' => $this->rewardResearch($user, $reward->value),
        };
    }

    private function rewardArgent(mixed $user, mixed $value)
    {
        $user->argent += $value;
        $user->save();
    }

    private function rewardTpoint(mixed $user, mixed $value)
    {
        $user->tpoint += $value;
        $user->save();
    }

    private function rewardResearch(mixed $user, mixed $value)
    {
        $user->research += $value;
        $user->save();
    }


}
