<?php

namespace App\Services\SNCF;

class LigneInfra
{
    public function allLigne()
    {
        $file = \Storage::disk('public')->get('lignes-par-statut.json');

        return collect(json_decode($file))->all();
    }

    public function getAllKVBfromLigne(string $ligne_id = null)
    {
        $file = \Storage::disk('public')->get('lignes-equipees-de-kvb.json');
        if (!isset($ligne_id)) {
            return collect(json_decode($file))->all();
        } else {
            return collect(json_decode($file))->where('code_ligne', $ligne_id)->all();
        }
    }

    public function getAllRadioSolfromLigne(string $ligne_id = null)
    {
        $file = \Storage::disk('public')->get('lignes-equipees-de-liaison-radio-sol-train.json');
        if (!isset($ligne_id)) {
            return collect(json_decode($file))->all();
        } else {
            return collect(json_decode($file))->where('code_ligne', $ligne_id)->all();
        }
    }

    public function getAllPassageNiveaufromLigne(string $ligne_id = null)
    {
        $file = \Storage::disk('public')->get('liste-des-passages-a-niveau.json');
        if (!isset($ligne_id)) {
            return collect(json_decode($file))->all();
        } else {
            return collect(json_decode($file))->where('code_ligne', $ligne_id)->all();
        }
    }

    public function getAllPassageTerrainfromLigne(string $ligne_id = null)
    {
        $file = \Storage::disk('public')->get('liste-des-passages-souterrains.json');
        if (!isset($ligne_id)) {
            return collect(json_decode($file))->all();
        } else {
            return collect(json_decode($file))->where('code_ligne', $ligne_id)->all();
        }
    }

    public function getAllSousStationfromLigne(string $ligne_id = null)
    {
        $file = \Storage::disk('public')->get('liste-des-sous-stations.json');
        if (!isset($ligne_id)) {
            return collect(json_decode($file))->all();
        } else {
            return collect(json_decode($file))->where('code_ligne', $ligne_id)->all();
        }
    }

    public function getAllStationRadiofromLigne(string $ligne_id = null)
    {
        $file = \Storage::disk('public')->get('liste-des-stations-radio-fixes.json');
        if (!isset($ligne_id)) {
            return collect(json_decode($file))->all();
        } else {
            return collect(json_decode($file))->where('code_ligne', $ligne_id)->all();
        }
    }

    public function getEquipementGarefromGare(string $code_uic = null)
    {
        $file = \Storage::disk('public')->get('equipements-accessibilite-en-gares.json');
        if (!isset($code_uic)) {
            return collect(json_decode($file))->all();
        } else {
            return collect(json_decode($file))->where('uic', $code_uic)->all();
        }
    }

    public function getWifiFromGare(string $uic)
    {
        $file = \Storage::disk('public')->get('gares-equipees-du-wifi.json');
        return collect(json_decode($file))->where('uic', $uic)->all();
    }
}
