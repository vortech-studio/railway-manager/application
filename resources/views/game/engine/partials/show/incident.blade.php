<div class="rounded bg-bluegrey-600 border border-5 border-gray-400 shadow-box p-5">
    <div class="d-flex flex-column w-100">
        @include('game.includes.table.table_incident', ["incidents" => $engine->incidents()->orderBy('created_at', 'desc')->get()])
    </div>
</div>
<div class="modal fade" tabindex="-1" id="incidentModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-gray-500 modal-flush">
            <div class="modal-header shadow-lg bg-gray-700 text-white">
                <h3 class="modal-title text-white">
                    <img data-content="warning" src="" alt="" class="w-30px" />
                    <span data-content="title"></span>
                </h3>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">
                <div class="d-flex flex-row align-items-center">
                    <img data-content="image_origin" src="" class="w-250px rounded-2 me-5" alt="">
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Status:</span>
                            <div data-content="status">

                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Heure de l'incident:</span>
                            <div data-content="date">

                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Type d'incident:</span>
                            <div data-content="type_incident">

                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Problème:</span>
                            <div data-content="probleme">

                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Montant des réparations:</span>
                            <div data-content="amount">

                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1">
                            <span class="fw-bold me-2">Répercution sur le traffic:</span>
                            <div data-content="traffic">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
