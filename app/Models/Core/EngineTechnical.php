<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class EngineTechnical extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    protected $casts = [
        'start_exploi' => 'datetime',
        'end_exploi' => 'datetime',
    ];


    public function engine()
    {
        return $this->belongsTo(Engine::class);
    }
}
