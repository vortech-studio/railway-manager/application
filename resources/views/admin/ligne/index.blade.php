@extends("admin.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Gestion des Lignes",
        'action' => true,
        'data' => [
            [
                'url' => route('admin.ligne.create'),
                'title' => "Ajouter une nouvelle ligne",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <div class="card card-flush">
        <!--begin::Card header-->
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <!--begin::Card title-->
            <div class="card-title">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                    <input type="text" data-ligne-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Rechercher une ligne" />
                </div>
                <!--end::Search-->
            </div>
            <!--end::Card title-->
            <!--begin::Card toolbar-->
            <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                <div class="w-100 mw-150px">
                    <!--begin::Select2-->
                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Status" data-kt-ligne-table-filter="status">
                        <option></option>
                        <option value="all">All</option>
                        <option value="1">Actif</option>
                        <option value="0">Inactif</option>
                    </select>
                    <!--end::Select2-->
                </div>
                <div class="w-100 mw-150px">
                    <!--begin::Select2-->
                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="false" data-placeholder="Hubs" data-kt-ligne-table-filter="hubs">
                        <option></option>
                        <option value="all">All</option>
                        @foreach(\App\Models\Core\Hub::where('active', 1)->get() as $hub)
                            <option value="{{ $hub->id }}">{{ $hub->gare->name }}</option>
                        @endforeach
                    </select>
                    <!--end::Select2-->
                </div>

                <button class="btn btn-outline btn-outline-primary" data-action="export-ligne"><i class="fa-solid fa-upload me-2"></i> Exporter les lignes</button>
                <button class="btn btn-outline btn-outline-primary" data-action="import-ligne"><i class="fa-solid fa-download me-2"></i> importer les lignes</button>
                <!--end::Add product-->
            </div>
            <!--end::Card toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body pt-0">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_lignes">
                <thead>
                <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                    <th class="w-10px pe-2">
                        <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                            <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_ecommerce_products_table .form-check-input" value="1" />
                        </div>
                    </th>
                    <th class="min-w-200px">Ligne</th>
                    <th class="text-end min-w-100px">Hub Origine</th>
                    <th class="text-end min-w-70px">Prix Brut</th>
                    <th class="text-end min-w-100px">Statistique</th>
                    <th class="text-end min-w-100px">Hub Requis</th>
                    <th class="text-center min-w-100px">Status</th>
                    <th class="text-end min-w-70px">Actions</th>
                </tr>
                </thead>
                <tbody class="fw-semibold text-gray-600">
                @foreach($lignes as $ligne)
                <tr>
                    <td>
                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                            <input class="form-check-input" type="checkbox" value="1" />
                        </div>
                    </td>
                    <td>
                        <div class="d-flex flex-row align-items-center">
                            <div class="symbol symbol-30px me-3">
                                <img src="{{ $ligne->type_ligne_logo }}" alt="">
                            </div>
                            <a href="{{ route('admin.ligne.show', $ligne->id) }}" class="btn btn-flush">{{ $ligne->stationStart->name }} - {{ $ligne->stationEnd->name }}</a>
                        </div>
                    </td>
                    <td class="text-center pe-0">
                        {{ $ligne->hub->gare->name }}
                        <div class="d-none">{{ $ligne->hub_id }}</div>
                    </td>
                    <td class="pe-0" data-order="40">
                        <span class="fw-bold ms-3">{{ eur($ligne->price) }}</span>
                    </td>
                    <td class="pe-0">
                        <div class="d-flex flex-column">
                            <div class="d-flex flex-row align-items-center mb-2">
                                <span class="fw-bolder me-1">Distance:</span> <span class="text-gray-600">{{ $ligne->distance }} Km</span>
                            </div>
                            <div class="d-flex flex-row align-items-center mb-2">
                                <span class="fw-bolder me-1">Temps de trajet:</span> <span class="text-gray-600">{{ convertMinuteToHours($ligne->time_min) }}</span>
                            </div>
                            <div class="d-flex flex-row align-items-center mb-2">
                                <span class="fw-bolder me-1">Nombre de station:</span> <span class="badge badge-light-primary">{{ $ligne->nb_station }}</span>
                            </div>
                        </div>
                    </td>
                    <td class="text-end pe-0" data-order="rating-5">
                        @if($ligne->requirements->count() != 0)
                            <div class="d-flex flex-column">
                                @foreach($ligne->requirements as $requirement)
                                    <li class="d-flex align-items-center py-2">
                                        <span class="bullet bg-primary me-2"></span> {{ $requirement->hub->gare->name }}
                                    </li>
                                @endforeach
                            </div>
                        @else
                            <div class="d-flex flex-center">
                                <span class="badge badge-light-danger">Aucun hub requis</span>
                            </div>
                        @endif
                    </td>
                    <td class="text-center">
                        @if($ligne->active == '1')
                            <div class="badge badge-success">Actif</div>
                            <div class="d-none">1</div>
                        @else
                            <div class="badge badge-danger">Inactif</div>
                            <div class="d-none">0</div>
                        @endif
                    </td>
                    <td class="text-end">
                        <a href="#" class="btn btn-sm btn-light btn-flex btn-center btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                            <i class="ki-duotone ki-down fs-5 ms-1"></i></a>
                        <!--begin::Menu-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                            <div class="menu-item px-3">
                                <a href="{{ route('admin.ligne.show', $ligne->id) }}" class="menu-link px-3">Voir la ligne</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3" data-kt-ecommerce-product-filter="delete_row" data-id="{{ $ligne->id }}">Supprimer</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu-->
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let table = $('#liste_lignes').DataTable({
            info: !1,
            order: [],
            pageLength: 10,
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
        });

        document.querySelector('[data-ligne-filter="search"]').addEventListener('keyup', function (e) {
            table.search(e.target.value).draw();
        });


        document.querySelectorAll('[data-kt-ecommerce-product-filter="delete_row"]').forEach(function (btn) {
            btn.addEventListener('click', function (e) {
                e.preventDefault()
                let id = btn.dataset.id
                $.ajax({
                    url: "/api/core/hub/" + id + "/delete",
                    method: "DELETE",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id
                    },
                    success: function (data) {
                        console.log(data)
                        btn.removeAttribute('data-kt-indicator')
                        toastr.success("Le hub à été supprimé")
                        setTimeout(() => {
                            window.location.reload()
                        }, 1300)
                    }
                })
            })
        })

        document.querySelector('[data-action="export-ligne"]').addEventListener('click', function (e) {
            e.preventDefault()
            $.ajax({
                url: "/api/core/ligne/export",
                method: "GET",
                success: function (data) {
                    console.log(data)
                    toastr.success("Les lignes ont été exportés")
                    setTimeout(() => {
                        window.location.reload()
                    }, 1300)
                }
            })
        })

        document.querySelector('[data-action="import-ligne"]').addEventListener('click', function (e) {
            e.preventDefault()
            $.ajax({
                url: "/api/core/ligne/import",
                method: "GET",
                success: function (data) {
                    console.log(data)
                    toastr.success("Les lignes ont été importés")
                    setTimeout(() => {
                        window.location.reload()
                    }, 1300)
                }
            })
        })

        const status = document.querySelector('[data-kt-ligne-table-filter="status"]')
        $(status).on('change', (e => {
            console.log("CHANGE")
            let o = e.target.value
            "all" === o && (o = ""), table.column(6).search(o).draw()
        }))

        const hubs = document.querySelector('[data-kt-ligne-table-filter="hubs"]')
        $(hubs).on('change', (e => {
            console.log("CHANGE")
            let o = e.target.value
            "all" === o && (o = ""), table.column(2).search(o).draw()
        }))

    </script>
@endsection
