console.log('network system loaded')
function MapInit(data, containerDiv) {
    console.log(data)
    let followMarker = false;
    let coords = {};
    const zoomLevel = 6

    let container = L.DomUtil.get(containerDiv);
    if(container != null) {
        container._leaflet_id = null;
    }
    let map = null;

    const hubsLatLng = [];
    data.hubs.forEach(hub => {
        hubsLatLng.push([hub.latitude, hub.longitude]);
    })

    map = L.map(containerDiv).setView(hubsLatLng[0], zoomLevel);
    const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    mainLayer.addTo(map);

    data.hubs.forEach(hub => {
        console.log(hub)
        const hubOptions = {
            lat: hub.latitude,
            lng: hub.longitude,
            title: hub.name,
            draggable: false,
        };
        let marker = addMarker(hubOptions, map, 'hub');
        marker.on('click', function(event) {
            addPopup(marker, `
                <h3>${hub.name}</h3>
                <table class="table table-row-bordered table-row-gray-800 border border-gray-800 table-striped gy-2 gx-2 gs-2 mb-2">
                    <thead>
                        <tr>
                            <th>Commerce</th>
                            <th>Publicité</th>
                            <th>Parking</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${hub.nb_slot_commerce}</td>
                            <td>${hub.nb_slot_pub}</td>
                            <td>${hub.nb_slot_parking}</td>
                        </tr>
                    </tbody>
                </table>
                <strong class="mb-1">Fréquence Actuel:</strong> ${hub.freq_actual} / <small>par heure</small><br>
                <strong class="me-1">Etat de la Gare: </strong> <span class="badge bg-${hub.status === true ? 'success' : 'danger'}">${hub.status === true ? 'Ouvert' : 'Fermer'}</span> <br>
            `);
        })

        hub.lignes.forEach(ligne => {
            const ligneOptions = {
                lat: ligne.station_end.latitude,
                lng: ligne.station_start.longitude,
                title: ligne.name,
                draggable: false,
            };
            let marker = addMarker(ligneOptions, map, 'train');
            marker.on('click', function(event) {
                addPopup(marker, `
                    <h3>${ligne.name}</h3>
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row justify-content-between align-items-center p-2">
                            <div class="fw-bolder">Départ:</div> ${ligne.station_start.name}
                        </div>
                        <div class="separator border-gray-700 my-1"></div>
                        <div class="d-flex flex-row justify-content-between align-items-center p-2">
                            <div class="fw-bolder">Arrivée:</div> ${ligne.station_end.name}
                        </div>
                        <div class="separator border-gray-700 my-1"></div>
                        <div class="d-flex flex-row justify-content-between align-items-center p-2">
                            <div class="fw-bolder"><i class="fa-solid fa-clock me-1"></i> Prochain départ:</div> ${ligne.next_departure}
                        </div>
                    </div>
                `);
            })

            let typeLine = {
                "ter": {color: 'blue'},
                "tgv": {color: 'red'},
                "ic": {color: 'purple'},
                "othe": {color: 'black'},
            }

            let polyline = L.polyline([ligne.coordinates], {color: typeLine[ligne.type].color}).addTo(map)
        })


    });


}

function addMarker(options, map, type = 'hub') {
    let typeIcon = {
        'hub': {url: '/storage/icons/hub.png', talle: [45,48]},
        'train': {url: '/storage/icons/train_map.png', talle: [32,37]},
    }

    const icon = L.icon({
        iconUrl: typeIcon[type].url,
        iconSize:    typeIcon[type].talle, // size of the icon
        iconAnchor:   [16, 18], // point of the icon which will correspond to marker's location
        popupAnchor:  [1, -9]
    })
    const marker = L.marker([options.lat, options.lng], { title: options.title, draggable: options.draggable, icon: icon });
    marker.addTo(map);

    marker.on('dragend', function(event) {
        coords = event.target._latlng;
        showNewCoords(event.target._latlng, event.target);
        if (followMarker) {
            map.setView(event.target._latlng);
        }
    });

    return marker;
}

function addPopup(marker, popupContent) {
    marker.bindPopup('', {'className': 'custom'})
    let popup = marker.getPopup()
    popup.setContent(popupContent)
    marker.openPopup()
}


function showNewCoords(coords, marker) {
    marker
        .bindPopup(`lat: ${coords.lat} - lng: ${coords.lng}`)
        .openPopup();
}
