<?php

namespace App\Jobs\Game\Rental;

use App\Models\Core\Engine;
use App\Models\Core\Mouvement;
use App\Models\User\UserRental;
use App\Notifications\Game\SendMessageNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RentalDebitJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserRental $userRental)
    {
    }

    public function handle(): void
    {
        if(Engine::isOk($this->userRental->user_engine->engine->price_location, $this->userRental->user, 'argent')) {
            if($this->userRental->status == 'default') {
                $amount_pay = ($this->userRental->user_engine->engine->price_location * (2 / 100)) + $this->userRental->user_engine->engine->price_location;

                Mouvement::adding(
                    'charge',
                    "Prélèvement de location du matériel roulant par " . $this->userRental->rental->name,
                    'location_materiel',
                    $amount_pay,
                    auth()->user()->company->id,
                    $this->userRental->user_engine->user_hub->id
                );

            } else {
                Mouvement::adding(
                    'charge',
                    "Prélèvement de location du matériel roulant par " . $this->userRental->rental->name,
                    'location_materiel',
                    $this->userRental->user_engine->engine->price_location,
                    auth()->user()->company->id,
                    $this->userRental->user_engine->user_hub->id
                );

            }
            $this->userRental->user->notify(new SendMessageNotification(
                'Prélèvement effectué',
                "Le prélèvement pour la location N°{$this->userRental->id} a été effectué.",
                'success',
                asset('/storage/icons/gateway_check.png'),
                route('engine.show', $this->userRental->user_engine->engine->id),
                ["engine", "finance"]
            ));
        } else {
            if($this->userRental->status == 'default') {
                $this->userRental->user_engine->delete();
                $amount_caution_restore = $this->userRental->amount_caution / 2;

                $this->userRental->user->argent += $amount_caution_restore;
                $this->userRental->user->save();

                $this->userRental->user->notify(new SendMessageNotification(
                    'Second prélèvement echoué',
                    "Le prélèvement pour la location N°{$this->userRental->id} a échoué une seconde fois.",
                    'danger',
                    asset('/storage/icons/gateway_error.png'),
                    route('engine.show', $this->userRental->user_engine->engine->id),
                    ["engine", "finance"]
                ));

                $this->userRental->user->messages()->create([
                    "from" => "Service de recouvrement de ".$this->userRental->rental->name,
                    "subject" => "Impayé et restitution du matériel roulant",
                    "message" => view('game.includes.mailbox.default_rental', [
                        "engine_name" => $this->userRental->user_engine->engine->name,
                        "rental" => $this->userRental->rental,
                        "rental_amount" => $this->userRental->user_engine->engine->price_location,
                        "amount_fee" => $amount_caution_restore,
                    ]),
                    "important" => true,
                    "user_id" => $this->userRental->user->id
                ]);
            } else {

                $this->userRental->user->notify(new SendMessageNotification(
                    'Prélèvement echoué',
                    "Le prélèvement pour la location N°{$this->userRental->id} a échoué. Nouvelle tentative dans 24H.",
                    'warning',
                    asset('/storage/icons/gateway_warning.png'),
                    route('engine.show', $this->userRental->user_engine->engine->id),
                    ["engine", "finance"]
                ));

                dispatch(new RentalDebitJob($this->userRental))
                    ->delay(now()->addDay()->startOfDay())
                    ->onQueue('rental-debit');

            }
        }
    }
}
