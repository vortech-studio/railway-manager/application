<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        seo()->title(config('app.name').' - Connexion');
        seo()->csrfToken();

        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $request->authenticate();

        $request->session()->regenerate();

        $request->user()->update([
            'last_login' => now(),
            'status' => 'active',
        ]);

        if(!$request->user()->dailys()->whereDate('date', now()->toDateString())->exists()) {
            $request->user()->dailys()->create([
                'date' => now()->toDateString(),
            ]);
            toastr()->info("N'oubliez pas d'allez récupérer votre bonus journalier !");
        }

        $this->verifyAccount($request->user());

        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    private function verifyAccount(User $user)
    {
        if(!$user->hasVerifiedEmail()) {
            $user->sendEmailVerificationNotification();
            toast()->warning("Veuillez confirmer votre adresse email");
        }

        if($user->dailys()->whereDate('date', now()->toDateString())->where('claim_daily', 0)->count() != 0) {
            toastr()->info("N'oubliez pas d'allez sélectionner votre bonus journalier !");
        }
    }
}
