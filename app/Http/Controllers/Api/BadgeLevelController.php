<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use LevelUp\Experience\Models\Level;

class BadgeLevelController extends Controller
{
    public function info($id)
    {
        $level = Level::find($id);
        return response()->json([
            "level" => $level,
        ]);
    }

    public function destroy($id)
    {
        $level = Level::findOrFail($id);
        $level->delete();
        return response()->json([
            "message" => "Niveau Supprimé",
        ]);
    }
}
