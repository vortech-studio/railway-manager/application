<?php

namespace App\Console\Commands;

use App\Models\Core\Engine;
use Carbon\Carbon;
use Illuminate\Console\Command;
use function Laravel\Prompts\confirm;
use function Laravel\Prompts\intro;
use function Laravel\Prompts\text;
use function Laravel\Prompts\select;
use function Laravel\Prompts\table;
use function Laravel\Prompts\warning;

class MakeEngineCommand extends Command
{
    protected $signature = 'make:engine';

    protected $description = 'Assistant de création d\'un matériel roulant';

    public function handle()
    {
        intro($this->description);
        $name = text(
            label: 'Quel est le nom du matériel roulant ?',
            placeholder: 'Z 26400', required: true
        );
        $type_engine = select(
            label: "Quel est le type de train ?",
            options: ['motrice', 'remorque', 'automotrice']
        );
        if ($type_engine == 'automotrice') {
            $type_train = select(
                label: 'Quel est le type hierarchique du train ?',
                options: ['ter', 'tgv', 'ic', 'other'],
                required: true
            );
            $nb_wagon = text(
                label: "Nombre de voiture supporter par l'automotrice ?",
                hint: "Motrice comprise"
            );
        } else {
            $type_train = 'other';
            $nb_wagon = 0;
        }

        $type_energy = select(
            label: "Quel est le type d'énergie utiliser par le matériel roulant ?",
            options: ['diesel', 'electrique', 'hybride', 'vapeur', 'aucun']
        );
        $velocity = text(
            label: 'Vitesse maximal du train ?',
        );
        $type_marchandise = select(
            label: 'Type de marchandise transporté ?',
            options: ['passagers', 'marchandises', 'none']
        );
        if ($type_marchandise != 'none') {
            $nb_marchandise = text(
                label: 'Nombre de marchandises ou de passagers pouvent être transporter ?',
                default: 0
            );
        } else {
            $nb_marchandise = 0;
        }

        $in_game = confirm(
            label: 'Est-il disponible dans le jeu ?', yes: 'Oui', no: 'Non'
        );
        $in_shop = confirm(
            label: 'Est-il disponible dans la boutique ?', yes: 'Oui', no: 'Non'
        );

        if ($in_shop) {
            $money_shop = select(
                label: 'Quel est la monnaie utiliser',
                options: ['argent', 'tpoint', 'euro'],
                default: 'tpoint'
            );
            $price_shop = text(
                label: 'Quel est le montant'
            );
        } else {
            $money_shop = null;
            $price_shop = null;
        }

        \Laravel\Prompts\info("Information sur les capacité technique du matériel roulant");
        info("SURFACE");
        $longeur = text("Quel est la longueur total du matériel ?");
        $largeur = text("Quel est la largeur total du matériel ?", '', 2.36);
        $hauteur = text("Quel est la largeur total du matériel ?", '', 4.25);
        $poids = text("Quel est le poids total du matériel ?", '', '', '', null, 'Le poids doit être en tonne');

        info("MOTORISATION");
        $essieux = select("Quel est le type d'essieux", ['BB', 'BOBO', 'CC', 'COCO']);

        info("PUISSANCES");
        $motor = select("Quel est le type de motorisation ?", ['diesel', 'electrique 1500V', 'electrique 25000V', 'electrique 1500V/25000V', 'vapeur', 'hybride', 'autre']);
        $puissance = text(
            label: "Quel est la puissance du moteur ?",
            hint: "La puissance est exprimer en Kw"
        );
        if ($motor == "diesel" || $motor == 'hybride') {
            $gasoil = text(
                label: "Quel est la capacité global du/des réservoir de gasoil ?",
                hint: "Préciser en litre"
            );
        } else {
            $gasoil = 0;
        }

        info("VIE DU MATERIEL");
        $start_year = text("Quel est l'année de mise en service du matériel ?");
        $end_year = text("Quel est l'année de réforme du matériel ?");

        \Laravel\Prompts\alert("RECAPITULATIF");
        info("Information Global");
        table(
            ['Designation', "Valeur"],
            [
                [
                    "designation" => "Identification",
                    "valeur" => $name
                ],
                [
                    "designation" => "Type de Train",
                    "valeur" => $type_train
                ],
                [
                    "designation" => "Type de matériel roulant",
                    "valeur" => $type_engine
                ],
                [
                    "designation" => "Type d'énergie",
                    "valeur" => $type_energy
                ],
                [
                    "designation" => "Vitesse Maximal",
                    "valeur" => $velocity. "Km/h"
                ],
                [
                    "designation" => "Type de marchandise",
                    "valeur" => $type_marchandise
                ],
                [
                    "designation" => "Nombre de marchandise",
                    "valeur" => $type_marchandise == 'none'? "Aucun" : $nb_marchandise
                ],
            ],
        );

        info("Information Géométriques & spécifiques");
        table(
            ['Designation', "Valeur"],
            [
                [
                    "designation" => "Longueur",
                    "value" => $longeur. " M"
                ],
                [
                    "designation" => "Largeur",
                    "value" => $largeur. " M"
                ],
                [
                    "designation" => "Hauteur",
                    "value" => $hauteur. " M"
                ],
                [
                    "designation" => "Poids",
                    "value" => $poids. " M"
                ],
                [
                    "designation" => "Type d'éssieux",
                    "value" => $essieux
                ],
                [
                    "designation" => "Date de mise en service",
                    "value" => $start_year
                ],
                [
                    "designation" => "Date de réforme",
                    "value" => $end_year
                ]
            ],
        );

        info("Information de motorisation");
        table(
            ['Designation', "Valeur"],
            [
                [
                    "designation" => "Type de motorisation",
                    "value" => $motor
                ],
                [
                    "designation" => "Puissance",
                    "value" => $puissance. " Kw"
                ],
                [
                    "designation" => "Capacité du reservoir",
                    "value" => $motor == 'diesel' || $motor == 'hybride' ? "$gasoil Litre" : "Non Prise en charge",
                ]
            ],
        );

        if (confirm(
            label: "Etes-vous sur de vouloir installer ce matériel ?",
            yes: "Oui",
            no: "Non",
        )) {
            \Laravel\Prompts\alert("Création du matériel roulant:".$name);
            $this->createEngine(
                $name,
                $type_engine,
                $type_train,
                $type_energy,
                $velocity,
                $type_marchandise,
                $nb_marchandise,
                $longeur,
                $largeur,
                $hauteur,
                $essieux,
                $poids,
                $motor,
                $puissance,
                $gasoil,
                $start_year,
                $end_year,
                $nb_wagon,
                $in_game,
                $in_shop,
                $price_shop,
                $money_shop,
            );
            alert("Création du matériel roulant terminé");
        } else {
            warning("Création du matériel roulant annulée");
        }
    }

    private function createEngine(
        mixed        $name,
        array|string $typeEngine,
        array|string $typeTrain,
        array|string $typeEnergy,
        mixed        $velocity,
        array|string $typeMarchandise,
        mixed        $nbMarchandise,
        mixed        $longeur,
        mixed        $largeur,
        mixed        $hauteur,
        array|string $essieux,
        mixed        $poids,
        string       $motor,
        mixed        $puissance,
        mixed        $gasoil,
        int          $startYear,
        int          $endYear,
        int          $nbWagon,
        bool         $inGame,
        bool         $inShop,
        int|null     $priceShop,
        string|null  $moneyShop
    ): Engine
    {
        $engine = new Engine();
        $priceCheckout = $engine->calcPriceCheckout(
            $longeur,
            $largeur,
            $hauteur,
            $poids,
            $essieux,
            $motor,
            $typeMarchandise,
            $typeEngine,
            $puissance
        );
        $eng = $engine->create([
            "name" => $name,
            "type_train" => $typeTrain,
            "type_engine" => $typeEngine,
            "type_energy" => $typeEnergy,
            "velocity" => $velocity,
            "nb_passager" => $nbMarchandise,
            "price_achat" => $priceCheckout,
            "price_maintenance" => 0,
            "duration_maintenance" => "00:00:00",
            "price_location" => $engine->calcPriceLocation($priceCheckout),
            "image" => \Str::upper(\Str::slug($name)) . '.gif',
            "in_game" => $inGame,
            "in_shop" => $inShop,
            "price_shop" => $priceShop,
            "money_shop" => $moneyShop,
        ]);

        $eng->technical()->create([
            "longueur" => $longeur,
            "largeur" => $largeur,
            "hauteur" => $hauteur,
            "essieux" => $essieux,
            "max_speed" => $velocity,
            "poids" => $poids,
            "traction_force" => Engine::calcTractionForce($poids),
            "type_motor" => $motor,
            "power_motor" => $puissance,
            "start_exploi" => Carbon::create($startYear, 1, 1),
            "end_exploi" => Carbon::create($endYear, 1, 1),
            "gasoil" => $gasoil,
            "type_marchandise" => $typeMarchandise,
            "nb_type_marchandise" => $nbMarchandise,
            "engine_id" => $eng->id,
            "nb_wagon" => $nbWagon,
        ]);

        Engine::dispatching($eng, $typeEngine);

        $allTimeCheckMaintenance = $eng->compositions()->sum('maintenance_check_time');
        $timeMaintenance = now()->startOfDay()->addMinutes($allTimeCheckMaintenance)->format('H:i:s');

        $eng->update([
            "duration_maintenance" => $timeMaintenance,
            "price_maintenance" => $engine->calcPriceMaintenance($eng->type_energy, $priceCheckout, $allTimeCheckMaintenance, $gasoil),
        ]);

        return $engine;
    }
}
