<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailboxActionsTable extends Migration
{
    public function up(): void
    {
        Schema::create('mailbox_actions', function (Blueprint $table) {
            $table->id();
            $table->enum('action_type', ["contract", "subvention", "other"]);
            $table->string('action_link');
            $table->string('action_text');

            $table->foreignId('mailbox_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('mailbox_actions');
    }
}
