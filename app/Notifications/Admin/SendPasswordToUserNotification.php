<?php

namespace App\Notifications\Admin;

use App\Models\User;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendPasswordToUserNotification extends Notification
{
    public function __construct(public readonly User $user, public readonly string $password)
    {
    }

    public function via($notifiable): array
    {
        return ['mail'];
    }

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject(config('app.name').' - Votre mot de passe temporaire à été définie')
            ->line('Votre mot de passe temporaire est : ' . $this->password)
            ->line('Vous pouvez le changer dans votre profil')
            ->action('Mon profil', url('/game/user/profil'))
            ->line("L'équipe de " . config('app.name') . " vous remercie pour votre confiance.");
    }

    public function toArray($notifiable): array
    {
        return [];
    }
}
