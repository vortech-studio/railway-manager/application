<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Core\Blog\Articles;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        return view('admin.blog.index', [
            "articles" => Articles::all()
        ]);
    }

    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Enregistre un nouvel article.
     *
     * @param Request $request Les données de la requête.
     * @return \Illuminate\Http\RedirectResponse Redirige vers la liste des articles.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content_annonce' => 'required',
            'type_article' => 'required',
        ]);

        Articles::create([
            'titre' => $request->title,
            'content' => $request->content_annonce,
            'type_article' => $request->type_article,
            'user_id' => auth()->user()->id,
            'published' => false,
        ]);

        toast()->success('L\'article a bien été créé');
        return redirect()->route('admin.blog.index');
    }

    public function edit($id)
    {
        return view('admin.blog.edit', [
            "article" => Articles::find($id)
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "title" => "required",
            "content_annonce" => "required",
            "type_article" => "required",
        ]);

        $article = Articles::find($id);
        $article->titre = $request->title;
        $article->content = $request->content_annonce;
        $article->type_article = $request->type_article;
        $article->save();

        toast()->success("L'article a bien été modifié");
        return redirect()->route('admin.blog.index');
    }

    public function delete($id)
    {
        Articles::find($id)->delete();
        return response()->json();
    }
}
