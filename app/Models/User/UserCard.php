<?php

namespace App\Models\User;

use App\Models\Core\CardHolder;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    protected $casts = [
        'uuid' => 'string',
    ];
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cards()
    {
        return $this->hasMany(UserCardContent::class);
    }
}
