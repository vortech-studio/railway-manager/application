
let loadTable = document.querySelector('#loadTable')
let user_hub_id = document.querySelector('[name="user_hub_id"]').value

document.querySelectorAll('.page-item a').forEach(item => {
    item.addEventListener('click', event => {
        event.preventDefault()
        let table = loadTable.querySelector('#liste_travel')
        let date = item.dataset.date
        loadTable.querySelector('.table-responsive').classList.add('table-loading')

        $.ajax({
            url: '/api/game/network/'+user_hub_id+'/plannings',
            method: 'GET',
            data: {date: date},
            success: function (data) {
                table.querySelector('tbody').innerHTML = ''

                loadTable.querySelector('.table-responsive').classList.remove('table-loading')
            }
        })
    })
})
