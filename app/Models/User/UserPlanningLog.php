<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserPlanningLog extends Model
{
    protected $guarded = [];

    public function planning()
    {
        return $this->belongsTo(UserPlanning::class, 'user_planning_id');
    }
}
