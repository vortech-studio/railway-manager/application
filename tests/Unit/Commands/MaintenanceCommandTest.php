<?php

namespace Tests\Unit\Commands;

use App\Models\Core\Engine;
use App\Models\User;
use App\Models\User\UserTechnicentreTasking;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreatesApplication;
use Tests\TestCase;

class MaintenanceCommandTest extends TestCase
{
    use RefreshDatabase;
}
