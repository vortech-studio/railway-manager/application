<?php

namespace App\Services\DataGouv;

class Calling
{
    public function getPriceElec()
    {
        $call = \Http::withoutVerifying()
            ->get('https://data.statistiques.developpement-durable.gouv.fr/dido/api/v1/datafiles/1d1b0e79-0979-4db9-98e6-ec7c0dea68d2/json?millesime=2023-07')
            ->object();
        $collect = collect($call);

        return $collect->where('PERIODE', '2022-12')->first();
    }

    public function getPriceGazoil()
    {
        $call = \Http::withoutVerifying()
            ->get('https://data.statistiques.developpement-durable.gouv.fr/dido/api/v1/datafiles/daf4715a-0795-4098-bdb1-d90b6e6a568d/json?millesime=2023-08')
            ->object();
        $collect = collect($call);

        return $collect->where('PERIODE', now()->subMonth()->format('Y-m'))->first();
    }
}
