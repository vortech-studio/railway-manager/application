<?php

namespace App\Models\Core;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function rewards()
    {
        return $this->hasMany(BadgeReward::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_badges', 'badge_id', 'user_id');
    }

    public function isUnlockFor(User $user)
    {
        return $this->users()
            ->where('user_id', $user->id)
            ->exists();
    }

    public function unlockActionFor(mixed $user, string $action, int $count = 0)
    {
        $badge = $this->newQuery()
            ->where('action', $action)
            ->where('action_count', $count)
            ->first();

        if($badge && !$badge->isUnlockFor($user)) {
            $user->badges()->attach($badge);
            return $badge;
        }

        return null;
    }
}
