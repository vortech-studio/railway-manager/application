@extends("game.template")

@section("css")
    <link src="{{ asset('/plugins/leaflet/leaflet.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/ligne.png') }}" alt="">
            </div>
            <x-base.underline
                title="Détail d'une ligne"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.network.partials.menu")
    </div>
    <div class="d-flex flex-wrap justify-content-center align-items-center bg-gray-400 px-5 py-3 rounded-bottom-3 mb-10">
        <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
            <li class="nav-item">
                <a class="nav-link text-white active" data-bs-toggle="tab" href="#detail_ligne">
                    <span class="nav-icon me-2">
                        <img src="{{ asset('/storage/icons/ligne.png') }}" class="w-35px" alt="">
                    </span>
                    <span class="nav-text">Détail d'une ligne</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" data-bs-toggle="tab" href="#planning_ligne">
                    <span class="nav-icon me-2">
                        <img src="{{ asset('/storage/icons/planning.png') }}" class="w-35px" alt="">
                    </span>
                    <span class="nav-text">Trajet sur la ligne</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" data-bs-toggle="tab" href="#lab_ligne">
                    <span class="nav-icon me-2">
                        <img src="{{ asset('/storage/icons/research.png.png') }}" class="w-35px" alt="">
                    </span>
                    <span class="nav-text">Recherche & Developpement</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" data-bs-toggle="tab" href="#sell_ligne">
                    <span class="nav-icon">
                        <img src="{{ asset('/storage/icons/ligne_checkout.png') }}" class="w-35px" alt="">
                    </span>
                    <span class="nav-text">Vendre la ligne</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="detail_ligne" role="tabpanel">
            @premium
            <div class="card bg-gray-600 mb-10">
                <div class="card-body h-450">
                    <div id="map" style="height: 430px"></div>
                </div>
            </div>
            @endpremium
            <x-base.underline
                title="<img src='{{ asset('/storage/icons/ligne.png') }}' alt='Ligne' class='w-35px'> Détail de la ligne"
                size="3"
                size-text="fs-1" />
            <div class="card card-flush mb-10">
                <div class="card-header">
                    <div class="card-title">
                        <div class="badge badge-warning me-2">Ligne</div>
                        <img src="{{ $ligne->ligne->type_ligne_logo }}" alt="" class="w-30px">
                        <div class="fs-3 fw-bolder ms-2">{{ $ligne->ligne->name }}</div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Date d'achat :</div>
                                    <div class="fw-bolder">{{ $ligne->date_achat->format("d/m/Y à H:i") }}</div>
                                </div>
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Engins affectés :</div>
                                    <div class="fw-bolder">{{ $ligne->user_engine()->count() }}</div>
                                </div>
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Nombre de trajet hebdomadaire :</div>
                                    <div class="fw-bolder">{{ $ligne->nb_depart_jour * 8 }}</div>
                                </div>
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Hub Affilié :</div>
                                    <div class="fw-bolder">{{ $ligne->userHub->hub->gare->name }} / {{ $ligne->userHub->hub->gare->region }} / {{ $ligne->userHub->hub->gare->pays }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Distance :</div>
                                    <div class="fw-bolder">{{ $ligne->ligne->distance }} Km</div>
                                </div>
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Temps de trajet :</div>
                                    <div class="fw-bolder">{{ $ligne->ligne->time_format }}</div>
                                </div>
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Taxe sur ce hub :</div>
                                    <div class="fw-bolder">{{ eur($ligne->userHub->hub->taxe_hub) }}</div>
                                </div>
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Gare de départ :</div>
                                    <div class="fw-bolder">{{ $ligne->ligne->stationStart->name }}</div>
                                </div>
                                <div class="d-flex flex-row mb-2">
                                    <div class="me-1">Gare d'arrivée :</div>
                                    <div class="fw-bolder">{{ $ligne->ligne->stationEnd->name }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <x-base.underline
                title="<img src='{{ asset('/storage/icons/ligne.png') }}' alt='Ligne' class='w-35px'> Statistique"
                size="3"
                size-text="fs-1" />
            <div class="card card-flush shadow-lg mb-10">
                <div class="card-body w-100">
                    <div class="fw-bold fs-4 mb-2">Chiffre d'affaires</div>
                    <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-25">
                            <div class="">Aujourd'hui</div>
                            <div class="fw-semibold">{{ eur($ligne->calcCA(0)) }}</div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-25">
                            <div class="">Hier</div>
                            <div class="fw-semibold">{{ eur($ligne->calcCA(1)) }}</div>
                        </div>
                    </div>

                    <div class="fw-bold fs-4 mb-2">Coût</div>
                    <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-25">
                            <div class="">Aujourd'hui</div>
                            <div class="fw-semibold">{{ eur($ligne->calcCout(0)) }}</div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-25">
                            <div class="">Hier</div>
                            <div class="fw-semibold">{{ eur($ligne->calcCout(1)) }}</div>
                        </div>
                    </div>

                    <div class="fw-bold fs-4 mb-2">Revenues Auxiliaires</div>
                    <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-25">
                            <div class="">Aujourd'hui</div>
                            <div class="fw-semibold">{{ eur($ligne->calcRentTrajetAux(0)) }}</div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-25">
                            <div class="">Hier</div>
                            <div class="fw-semibold">{{ eur($ligne->calcRentTrajetAux(1)) }}</div>
                        </div>
                    </div>

                    <div class="fw-bold fs-4 mb-2">Bénéfices des trajets</div>
                    <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-25">
                            <div class="">Aujourd'hui</div>
                            <div class="fw-semibold">{{ eur($ligne->calcBenefice(0)) }}</div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-25">
                            <div class="">Hier</div>
                            <div class="fw-semibold">{{ eur($ligne->calcBenefice(1)) }}</div>
                        </div>
                    </div>

                    <div class="fw-bold fs-4 mb-2">Coût des incidents</div>
                    <div class="d-flex flex-row justify-content-between align-items-center mb-5">
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-25">
                            <div class="">Aujourd'hui</div>
                            <div class="fw-semibold">{{ eur($ligne->calcCoutIncident(0)) }}</div>
                        </div>
                        <div class="d-flex flex-row justify-content-between align-items-center mb-2 w-25">
                            <div class="">Hier</div>
                            <div class="fw-semibold">{{ eur($ligne->calcCoutIncident(1)) }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <x-base.underline
                title="<img src='{{ asset('/storage/icons/ligne.png') }}' alt='Ligne' class='w-35px'> Tarifications"
                size="3"
                size-text="fs-1" />
            <div class="card card-flush shadow-lg mb-10">
                <div class="card-header">
                    <div class="card-title">
                        <x-form.radio
                            name="date"
                            value="today"
                            label="Aujourd'hui"
                            for="date"
                            checked="true" />
                        <x-form.radio
                            name="date"
                            value="yesterday"
                            label="Hier"
                            for="date" />
                    </div>
                    <div class="card-toolbar">
                        <a href="#" class="btn btn-info"><i class="fa-solid fa-euro-sign fs-2 me-2 text-white"></i> Tarif de la ligne</a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered table-hover gs-2 gx-2 gy-2">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="text-end">Première Classe</th>
                                <th class="text-end">Deuxième Classe</th>
                                <th class="text-end">Unique</th>
                            </tr>
                        </thead>
                        <tbody class="fs-bold" id="todayTarifBody">
                            @foreach($ligne->tarifs()->whereBetween('date_tarif', [now()->startOfDay(), now()->endOfDay()])->get() as $tarif)
                            <tr>
                                <td>Demande</td>
                                <td class="text-end">
                                    {{ $tarif->type == 'first' ? $tarif->demande : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'second' ? $tarif->demande : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'unique' ? $tarif->demande : '-' }}
                                </td>
                            </tr>
                            <tr>
                                <td>Offre</td>
                                <td class="text-end">
                                    {{ $tarif->type == 'first' ? $tarif->offre : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'second' ? $tarif->offre : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'unique' ? $tarif->offre : '-' }}
                                </td>
                            </tr>
                            <tr>
                                <td>Prix Moyen</td>
                                <td class="text-end">
                                    {{ $tarif->type == 'first' ? eur($tarif->price) : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'second' ? eur($tarif->price) : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'unique' ? eur($tarif->price) : '-' }}
                                </td>
                            </tr>
                            <tr>
                                <td>CA Prévisionnel</td>
                                <td class="text-end">
                                    {{ $tarif->type == 'first' ? eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->demande)).' / '.eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->offre)) : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'second' ? eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->demande)).' / '.eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->offre)) : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'unique' ? eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->demande)).' / '.eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->offre)) : '-' }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tbody class="fs-bold" id="yestedayTarifBody">
                            @foreach($ligne->tarifs()->whereBetween('date_tarif', [now()->subDay()->startOfDay(), now()->subDay()->endOfDay()])->get() as $tarif)
                            <tr>
                                <td>Demande</td>
                                <td class="text-end">
                                    {{ $tarif->type == 'first' ? $tarif->demande : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'second' ? $tarif->demande : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'unique' ? $tarif->demande : '-' }}
                                </td>
                            </tr>
                            <tr>
                                <td>Offre</td>
                                <td class="text-end">
                                    {{ $tarif->type == 'first' ? $tarif->offre : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'second' ? $tarif->offre : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'unique' ? $tarif->offre : '-' }}
                                </td>
                            </tr>
                            <tr>
                                <td>Prix Moyen</td>
                                <td class="text-end">
                                    {{ $tarif->type == 'first' ? eur($tarif->price) : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'second' ? eur($tarif->price) : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'unique' ? eur($tarif->price) : '-' }}
                                </td>
                            </tr>
                            <tr>
                                <td>CA Prévisionnel</td>
                                <td class="text-end">
                                    {{ $tarif->type == 'first' ? eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->demande)).' / '.eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->offre)) : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'second' ? eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->demande)).' / '.eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->offre)) : '-' }}
                                </td>
                                <td class="text-end">
                                    {{ $tarif->type == 'unique' ? eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->demande)).' / '.eur($tarif->price * ($tarif->ligne->nb_depart_jour * $tarif->offre)) : '-' }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if(auth()->user()->premium)
            <div class="card card-flush mb-10 shadow-lg">
                <div class="card-body bg-light-primary">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered gs-2 gx-2 gy-2">
                            <thead>
                                <tr>
                                    <th>Total</th>
                                    <th class="text-end">
                                        <div class="text-primary">J-5</div>
                                        <span>{{ now()->subDays(5)->format("d/m/Y") }}</span>
                                    </th>
                                    <th class="text-end">
                                        <div class="text-primary">J-4</div>
                                        <span>{{ now()->subDays(4)->format("d/m/Y") }}</span>
                                    </th>
                                    <th class="text-end">
                                        <div class="text-primary">J-3</div>
                                        <span>{{ now()->subDays(3)->format("d/m/Y") }}</span>
                                    </th>
                                    <th class="text-end">
                                        <div class="text-primary">J-2</div>
                                        <span>{{ now()->subDays(2)->format("d/m/Y") }}</span>
                                    </th>
                                    <th class="text-end">
                                        <div class="text-primary">J-1</div>
                                        <span>{{ now()->subDay()->format("d/m/Y") }}</span>
                                    </th>
                                    <th class="text-end">
                                        <div class="text-primary">Aujourd'hui</div>
                                        <span>{{ now()->format("d/m/Y") }}</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Demande</td>
                                    <td class="text-end">{{ $ligne->calcDemande(now()->subDays(5), now()) }}</td>
                                    <td class="text-end">{{ $ligne->calcDemande(now()->subDays(4), now()) }}</td>
                                    <td class="text-end">{{ $ligne->calcDemande(now()->subDays(3), now()) }}</td>
                                    <td class="text-end">{{ $ligne->calcDemande(now()->subDays(2), now()) }}</td>
                                    <td class="text-end">{{ $ligne->calcDemande(now()->subDays(1), now()) }}</td>
                                    <td class="text-end">{{ $ligne->calcDemande(now()->subDays(0), now()) }}</td>
                                </tr>
                                <tr>
                                    <td>Offre</td>
                                    <td class="text-end">{{ $ligne->calcOffreRestante(now()->subDays(5), now()) }}</td>
                                    <td class="text-end">{{ $ligne->calcOffreRestante(now()->subDays(4), now()) }}</td>
                                    <td class="text-end">{{ $ligne->calcOffreRestante(now()->subDays(3), now()) }}</td>
                                    <td class="text-end">{{ $ligne->calcOffreRestante(now()->subDays(2), now()) }}</td>
                                    <td class="text-end">{{ $ligne->calcOffreRestante(now()->subDays(1), now()) }}</td>
                                    <td class="text-end">{{ $ligne->calcOffreRestante(now()->subDays(0), now()) }}</td>
                                </tr>
                                <tr>
                                    <td>Prix Moyen</td>
                                    <td class="text-end">{{ eur($ligne->calcAvgPrice(5)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcAvgPrice(4)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcAvgPrice(3)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcAvgPrice(2)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcAvgPrice(1)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcAvgPrice()) }}</td>
                                </tr>
                                <tr>
                                    <td>Billetterie</td>
                                    <td class="text-end">{{ eur($ligne->calcCABilletterie(5)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCABilletterie(4)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCABilletterie(3)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCABilletterie(2)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCABilletterie(1)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCABilletterie(0)) }}</td>
                                </tr>
                                <tr>
                                    <td>Revenues Auxiliaires</td>
                                    <td class="text-end">{{ eur($ligne->calcRentTrajetAux(5)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcRentTrajetAux(4)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcRentTrajetAux(3)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcRentTrajetAux(2)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcRentTrajetAux(1)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcRentTrajetAux(0)) }}</td>
                                </tr>
                                <tr>
                                    <td>Chiffre d'affaires</td>
                                    <td class="text-end">{{ eur($ligne->calcCA(5)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCA(4)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCA(3)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCA(2)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCA(1)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCA(0)) }}</td>
                                </tr>
                                <tr>
                                    <td>Coût</td>
                                    <td class="text-end">{{ eur($ligne->calcCout(5)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCout(4)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCout(3)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCout(2)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCout(1)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcCout(0)) }}</td>
                                </tr>
                                <tr class="text-success fw-bold">
                                    <td>Résultat:</td>
                                    <td class="text-end">{{ eur($ligne->calcResultat(5)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcResultat(4)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcResultat(3)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcResultat(2)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcResultat(1)) }}</td>
                                    <td class="text-end">{{ eur($ligne->calcResultat(0)) }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>Coût des incidents</td>
                                <td class="text-end">{{ eur($ligne->calcCoutIncident(5)) }}</td>
                                <td class="text-end">{{ eur($ligne->calcCoutIncident(4)) }}</td>
                                <td class="text-end">{{ eur($ligne->calcCoutIncident(3)) }}</td>
                                <td class="text-end">{{ eur($ligne->calcCoutIncident(2)) }}</td>
                                <td class="text-end">{{ eur($ligne->calcCoutIncident(1)) }}</td>
                                <td class="text-end">{{ eur($ligne->calcCoutIncident(0)) }}</td>
                            </tr>
                            </tfoot>
                        </table>
                        <table class="table table-striped table-bordered gs-2 gx-2 gy-2">
                            <thead>
                            <tr>
                                <th>{{ Str::upper('Prévisions') }}</th>
                                <th class="text-end">
                                    <div class="text-primary">Aujourd'hui</div>
                                    <span>{{ now()->format("d/m/Y") }}</span>
                                </th>
                                <th class="text-end">
                                    <div class="text-primary">J+1</div>
                                    <span>{{ now()->addDays(1)->format("d/m/Y") }}</span>
                                </th>
                                <th class="text-end">
                                    <div class="text-primary">J+2</div>
                                    <span>{{ now()->addDays(2)->format("d/m/Y") }}</span>
                                </th>
                                <th class="text-end">
                                    <div class="text-primary">J+3</div>
                                    <span>{{ now()->addDays(3)->format("d/m/Y") }}</span>
                                </th>
                                <th class="text-end">
                                    <div class="text-primary">J+4</div>
                                    <span>{{ now()->addDays(4)->format("d/m/Y") }}</span>
                                </th>
                                <th class="text-end">
                                    <div class="text-primary">J+5</div>
                                    <span>{{ now()->addDays(5)->format("d/m/Y") }}</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Chiffre d'affaires</td>
                                <td class="text-end">{{ eur($ligne->calcPrevCA(0)) }}</td>
                                <td class="text-end">{{ eur($ligne->calcPrevCA(1)) }}</td>
                                <td class="text-end">{{ eur($ligne->calcPrevCA(2)) }}</td>
                                <td class="text-end">{{ eur($ligne->calcPrevCA(3)) }}</td>
                                <td class="text-end">{{ eur($ligne->calcPrevCA(4)) }}</td>
                                <td class="text-end">{{ eur($ligne->calcPrevCA(5)) }}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
            <x-base.underline
                title="<img src='{{ asset('/storage/icons/ligne.png') }}' alt='Ligne' class='w-35px'> Résumé financier sur 7 jours"
                size="3"
                size-text="fs-1" />
            <div class="card card-flush shadow-lg mb-10">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="fw-bold fs-4">Détails Financiers</div>
                            <div class="d-flex flex-column mb-5 mt-2">
                                <div class="d-flex flex-column justify-content-between mb-2">
                                    <div class="d-flex flex-row justify-content-between align-items-center mb-2 fw-bolder">
                                        <div class="">Chiffre d'affaire</div>
                                        <div class="fw-bolder">{{ eur($ligne->CAseven()) }}</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between align-items-center mb-2 ms-10 fs-italic">
                                        <div class="">dont CA Billetterie</div>
                                        <div class="fw-semibold">{{ eur($ligne->CAseven('billetterie')) }}</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between align-items-center mb-2 ms-10 fs-italic">
                                        <div class="">dont revenues auxilliaires</div>
                                        <div class="fw-semibold">{{ eur($ligne->CAseven('rent_trajet_aux')) }}</div>
                                    </div>
                                </div>
                                <div class="d-flex flex-column justify-content-between mb-5">
                                    <div class="d-flex flex-row justify-content-between align-items-center mb-2 fw-bolder text-danger">
                                        <div class="">Coût des Trajets</div>
                                        <div class="fw-bolder">{{ eur($ligne->CAseven('electricite') + $ligne->CAseven('gasoil') + $ligne->CAseven('taxe')) }}</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between align-items-center mb-2 ms-10 fs-italic text-danger">
                                        <div class="">dont frais Gasoil</div>
                                        <div class="fw-semibold">{{ eur($ligne->CAseven('gasoil')) }}</div>
                                    </div>
                                    <div class="d-flex flex-row justify-content-between align-items-center mb-2 ms-10 fs-italic text-danger">
                                        <div class="">dont frais electrique</div>
                                        <div class="fw-semibold">{{ eur($ligne->CAseven('electricite')) }}</div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-6">
                            <div class="fw-bold fs-4">Nombre de passagers</div>
                            <div class="d-flex flex-column mb-5 mt-2">
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Première classe</div>
                                    <div class="fw-semibold">{{ $ligne->sumPassengersFirst() }}</div>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <div class="">Seconde Classe</div>
                                    <div class="fw-semibold">{{ $ligne->sumPassengersSecond() }}</div>
                                </div>
                            </div>

                        </div>
                        <div class="separator border-2 border-gray-400 my-5"></div>
                        <div class="col-6">
                            <div class="d-flex flex-column justify-content-between">
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2 fw-bolder">
                                    <div class="">Résultat des Trajets</div>
                                    <div class="fw-bolder">{{ eur($ligne->calcResultatSeven()) }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2 text-danger fw-bolder">
                                    <div class="">Coût des incidents</div>
                                    <div class="fw-semibold">{{ eur($ligne->calcCoutIncident(7)) }}</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <x-base.underline
                title="<img src='{{ asset('/storage/icons/ligne.png') }}' alt='Ligne' class='w-35px'> Engins sur la ligne"
                size="3"
                size-text="fs-1" />
            <div class="card card-flush shadow-lg mb-10">
                <div class="card-body">
                    <div class="d-flex flex-column bg-light-primary p-5 mb-5 shadow-lg rounded-2">
                        <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                            <div class="d-flex flex-row">
                                @if($ligne->user_engine->engine->type_train != 'other')
                                    <img
                                        src="{{ asset('/storage/logos/logo_'.$ligne->user_engine->engine->type_train.'.svg') }}"
                                        alt="" class="w-20px me-3">
                                @else
                                    <img
                                        src="{{ asset('/storage/icons/train.png') }}"
                                        alt="" class="w-20px me-3">
                                @endif
                                <div class="fs-4 text-dark fw-bolder ms-5"><div class="badge badge-secondary">{{ $ligne->user_engine->engine->name }}</div> / {{ $ligne->user_engine->number }}</div>
                            </div>
                            <a href="{{ route('engine.show', $ligne->user_engine->engine->id) }}" class="btn btn-sm btn-flex btn-primary">
                                <img src="{{ asset('/storage/icons/train.png') }}" class="w-30px me-2" alt="">
                                Détail du matériel
                            </a>
                        </div>
                        <div class="d-flex flex-row align-items-center mt-10">
                            <div class="d-flex flex-row align-items-center justify-content-center">
                                @if($ligne->user_engine->engine->type_engine == 'automotrice')
                                    <img src="{{ asset('/storage/engines/'.$ligne->user_engine->engine->type_engine.'/'.$ligne->user_engine->engine->slug.'-0.gif') }}" alt="" class="w-150px me-3" />
                                @else
                                    <img src="{{ asset('/storage/engines/'.$ligne->user_engine->engine->type_engine.'/'.$ligne->user_engine->engine->image) }}" alt="" class="w-150px me-3" />
                                @endif
                            </div>
                            <div class="d-flex flex-row align-items-center">
                                <span>
                                    <span class="">Rayon Action :</span>
                                    <span class="fw-bolder">{{ number_format($ligne->user_engine->max_runtime_engine, 0, '', ' ') }} Km</span>
                                </span>
                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                <span>
                                    <span class="">Places :</span>
                                    <span class="fw-bolder">{{ $ligne->user_engine->engine->nb_passager }}</span>
                                </span>
                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                <span>
                                    <span class="">Usures :</span>
                                    <span class="fw-bolder">{{ $ligne->user_engine->calcUsureEngine() }} %</span>
                                </span>
                                <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                <span>
                                    <span class="">Ancienneté :</span>
                                    <span class="fw-bolder">{{ $ligne->user_engine->calcAncienneteEngine() }}/5</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="planning_ligne" role="tabpanel">
            <ul class="pagination mb-10">
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(6) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-6</span>
                        <small>{{ now()->subDays(6)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(5) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-5</span>
                        <small>{{ now()->subDays(5)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(4) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-4</span>
                        <small>{{ now()->subDays(4)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(3) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-3</span>
                        <small>{{ now()->subDays(3)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(2) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-2</span>
                        <small>{{ now()->subDays(2)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->subDays(1) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J-1</span>
                        <small>{{ now()->subDays(1)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item active">
                    <a href="#" data-date="{{ now() }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-white">Aujourd'hui</span>
                        <small>{{ now()->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(1) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+1</span>
                        <small>{{ now()->addDays(1)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(2) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+2</span>
                        <small>{{ now()->addDays(2)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(3) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+3</span>
                        <small>{{ now()->addDays(3)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(4) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+4</span>
                        <small>{{ now()->addDays(4)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(5) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+5</span>
                        <small>{{ now()->addDays(5)->format("d/m/Y") }}</small>
                    </a>
                </li>
                <li class="page-item ">
                    <a href="#" data-date="{{ now()->addDays(6) }}" class="page-link d-flex flex-column align-items-center">
                        <span class="text-primary">J+6</span>
                        <small>{{ now()->addDays(6)->format("d/m/Y") }}</small>
                    </a>
                </li>
            </ul>
            <div class="card shadow-lg">
                <div class="card-body" id="loadTable">
                    <div class="table-responsive">
                        <div class="table-loading-message">
                            Chargement...
                        </div>
                        <table class="table table-rounded gs-2 gy-2 gx-2 align-middle" id="liste_travel">
                            <thead>
                            <tr>
                                <th class="bg-success text-white fw-semibold">Train</th>
                                <th class="bg-primary text-white fw-semibold text-end">Ligne</th>
                                <th class="bg-primary text-white fw-semibold text-end">Départ / Arrivée</th>
                                <th class="bg-primary text-white fw-semibold text-center">PAX</th>
                                <th class="bg-primary text-white fw-semibold text-end">Chiffre d'affaire</th>
                                <th class="bg-primary text-white fw-semibold text-end">Résultat</th>
                                <th class="bg-primary"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ligne->plannings()->whereBetween('date_depart', [now()->startOfDay(), now()->endOfDay()])->get() as $planning)
                                <tr @if($planning->status == 'initialized') class="table-active text-gray-500" data-bs-toggle="tooltip" data-bs-placement="top" title="Ce trajet n'à pas encore commencée" data-bs-custom-class="tooltip-inverse" @endif>
                                    <td><a href="" class="btn btn-link @if($planning->status == 'initialized') text-gray-300 @endif">{{ $planning->engine->engine->name }} - {{ $planning->engine->number }}</a></td>
                                    <td class="text-end">
                                            <span class="" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ $planning->ligne->ligne->name }}">
                                                {{ Str::upper(Str::limit($planning->ligne->ligne->stationStart->name, 3, '')) }}/{{ Str::upper(Str::limit($planning->ligne->ligne->stationEnd->name, 3, '')) }} <img src="{{ asset('/storage/logos/logo_'.$planning->ligne->ligne->type_ligne.'.svg') }}" alt="" class="w-15px" />
                                            </span>
                                    </td>
                                    <td class="text-end">{{ $planning->date_depart->format("H:i") }} / {{ $planning->date_arrived->format("H:i") }}</td>
                                    <td class="text-center">
                                            <span data-bs-toggle="popover" data-bs-html="true" title="{{ $planning->hub->calcSumPassengers(null, $planning->date_depart, $planning->date_arrived) }} Passagers" data-bs-content="Première classe: <span class='fw-bold'>{{ $planning->hub->calcSumPassengers('first', $planning->date_depart, $planning->date_arrived) }}</span><br>Seconde classe: <span class='fw-bold'>{{ $planning->hub->calcSumPassengers('second', $planning->date_depart, $planning->date_arrived) }}</span>">
                                                {{ $planning->hub->calcSumPassengers('first', $planning->date_depart, $planning->date_arrived) +  $planning->hub->calcSumPassengers('second', $planning->date_depart, $planning->date_arrived) }} pax
                                            </span>
                                    </td>
                                    <td class="text-end">{{ eur($planning->travel->calcCa()) }}</td>
                                    <td class="text-end">{{ eur($planning->travel->calcResultat()) }}</td>
                                    @if($planning->status != 'initialized')
                                        <td>
                                            <a href="{{ route('travel.show', $planning->id) }}" class="btn btn-link"><i class="fa-solid fa-magnifying-glass fs-3"></i></a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="lab_ligne" role="tabpanel">
            <livewire:game-skill-content :skills="$ligne->skills" />
        </div>
        <div class="tab-pane fade" id="sell_ligne" role="tabpanel">
            <div class="row">
                <div class="col-8">
                    <div class="card shadow-lg mb-10">
                        <div class="card-header">
                            <div class="card-title">
                                <x-base.underline
                                    title="<img src='{{ asset('/storage/icons/ligne.png') }}' alt='Ligne' class='w-35px'> Vente de la ligne"
                                    size="3"
                                    size-text="fs-1" />
                            </div>
                        </div>
                        <div class="card-body">
                            <x-base.alert
                                type="solid"
                                color="warning"
                                icon="exclamation-triangle"
                                title="Attention"
                                content="La vente d'une ligne se fait sur la base de son utilisation sur les 7 derniers jours" />

                            <div class="d-flex flex-column bg-light-primary p-5 mb-5 shadow-lg rounded-2">
                                <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                                    <div class="d-flex flex-row">
                                        <div class="badge badge-primary">Ligne</div>
                                        <div class="fs-4 text-dark fw-bolder ms-5">{{ $ligne->ligne->name }}</div>
                                    </div>
                                </div>
                                <div class="d-flex flex-row align-items-center mt-10">
                                    <i class="fa-solid fa-arrow-up text-success fs-2"></i>
                                    <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                    <span>
                                        <span class="">Distance :</span>
                                        <span class="fw-bolder">{{ $ligne->ligne->distance }} Km</span>
                                    </span>
                                    <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                    <span>
                                        <span class="">Offre Restante (J-1) :</span>
                                        <span class="fw-bolder">{{ $ligne->calcOffreRestante() }}</span>
                                    </span>
                                    <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                    <span>
                                        <span class="">Chiffre d'affaires :</span>
                                        <span class="fw-bolder">{{ eur($ligne->calcCA()) }}</span>
                                    </span>
                                    <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                                    <span>
                                        <span class="">Bénéfices :</span>
                                        <span class="fw-bolder">{{ eur($ligne->calcBenefice()) }}</span>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="card shadow-lg">
                        <div class="card-header">
                            <h3 class="card-title">Vente</h3>
                        </div>
                        <div class="card-body">
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-500 p-5 mb-2">
                                    <div class="">Prix Achat</div>
                                    <div class="">{{ eur($ligne->ligne->price) }}</div>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-500 p-5 mb-2">
                                    <div class="">Bénéfice</div>
                                    <div class="">{{ eur($ligne->calcResultatSeven()) }}</div>
                                </div>
                                <div class="separator border-2 border-gray-600 my-5"></div>
                                <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-500 fs-2 fw-bold p-5 mb-2">
                                    <div class="">Total de la vente</div>
                                    <div class="p-2 text-success">+ {{ eur($ligne->simulateSelling()) }}</div>
                                </div>
                                <button class="btn btn-flex btn-lg btn-light-success text-center sell" data-amoount="{{ $ligne->simulateSelling() }}">
                                    <i class="fa-solid fa-euro-sign fs-2 me-2 text-success"></i>
                                    Vendre la ligne
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section("script")
    <script src="{{ asset('/plugins/leaflet/leaflet.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinysort/2.3.6/tinysort.js"></script>
    <script src="{{ asset('/assets/js/map.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        document.querySelectorAll('[name="date"].form-check-input').forEach((element) => {
            element.addEventListener('change', (event) => {
                if (event.target.value == 'today') {
                    document.getElementById('todayTarifBody').classList.remove('d-none');
                    document.getElementById('yestedayTarifBody').classList.add('d-none');
                } else {
                    document.getElementById('todayTarifBody').classList.add('d-none');
                    document.getElementById('yestedayTarifBody').classList.remove('d-none');
                }
            });
        });

        document.querySelector('.sell').addEventListener('click', e => {
            e.preventDefault()

            Swal.fire({
                title: 'Vendre la ligne',
                text: "Êtes-vous sûr de vouloir vendre la ligne ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Vendre',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('api.game.network.ligne.sell', [$ligne->userHub->id, $ligne->id]) }}',
                        method: 'POST',
                        data: {"user_id": {{ auth()->user()->id }}, 'amount': e.target.dataset.amoount},
                        success: () => {
                            Swal.fire({
                                title: 'Vendu !',
                                text: "La ligne a bien été vendu",
                                icon: 'success',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Ok',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    window.location.href = '{{ route('network') }}'
                                }
                            })
                        }
                    })
                }
            })
        })

        $.ajax({
            url: '{{ route('api.game.network.hub.lines', $ligne->userHub->id) }}',
            method: 'GET',
            data: {"user_id": {{ auth()->user()->id }}},
            success: data => {
                let map = initMap([data.lignes[0].hub.longitude, data.lignes[0].hub.latitude], 7)
                map.on('load', () => {
                    formatForHub(map, data.lignes[0].hub)
                    formatForLigne(map, [data.lignes[0]])
                    markerForStationEnd(map, data.lignes[0].arrival.ligne)

                    data.lignes[0].stations.forEach(station => {
                        markerForStation(map, station)
                    })
                    zoomInLine(map, data.lignes[0])
                })
            }
        })
    </script>
@endsection
