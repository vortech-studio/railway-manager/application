Cher Monsieur {{ auth()->user()->name }},<br>
<br>
<p>
    Je suis le responsable du service location de la société {{ $rental_name }} et je tiens à vous
    informer que le contrat de location du matériel roulant pour l'engin dénommé
    <strong>{{ $engine_name }}</strong> a bien été créé, à compter d'aujourd'hui, pour une durée de
    {{ $duration }} {{ Str::plural('semaine', $duration) }}.
</p>
<p>
    Nous avons pris en compte la caution d'un montant de {{ eur($amount_caution) }} que vous avez fournie,
    et nous nous engageons à la restituer dans son intégralité à la fin du contrat,
    sous réserve d'aucun dommage ou manquement aux conditions spécifiées dans le contrat de location.
</p>
<p>
    Pour ce qui est du paiement, nous vous informons que le montant convenu sera prélevé chaque semaine
    jusqu'à la clôture du contrat. Le montant hebdomadaire est de {{ eur($amount_pay) }}.
</p>
<p>Nous vous remercions pour votre confiance en notre société et nous sommes impatients de vous servir.</p>
<p></p>
<p>
    Cordialement,<br>
    Responsable du service location <br>
    {{ $rental_name }}<br>

