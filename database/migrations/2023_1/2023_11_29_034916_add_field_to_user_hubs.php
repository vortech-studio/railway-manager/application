<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToUserHubs extends Migration
{
    public function up(): void
    {
        Schema::table('user_hubs', function (Blueprint $table) {
            $table->boolean('commerce')->default(false);
            $table->integer('nb_commerce')->default(0);
            $table->integer('nb_commerce_limit')->default(0);
            $table->boolean('publicity')->default(false);
            $table->integer('nb_publicity')->default(0);
            $table->integer('nb_publicity_limit')->default(0);
            $table->boolean('parking')->default(false);
            $table->integer('nb_parking')->default(0);
            $table->integer('nb_parking_limit')->default(0);
            $table->integer('limit_ligne')->default(0);
        });
    }

    public function down(): void
    {
        Schema::table('user_hubs', function (Blueprint $table) {
            //
        });
    }
}
