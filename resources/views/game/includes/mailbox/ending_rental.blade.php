Cher Monsieur {{ auth()->user()->name }},<br>
<p>
    Je me permets de vous contacter au sujet de la location que vous avez effectuée avec notre société
    {{ $rental_name }}. Je suis heureux de vous informer que le contrat de location de
    {{ $duration }} {{ Str::plural('semaine', $duration) }} que vous aviez souscrit pour le matériel
    <strong>{{ $name_engine }}</strong> a arrivé à son
    terme avec succès.
</p>
<p>
    Je tiens également à vous informer que le matériel roulant que vous aviez loué a été restitué
    dans un état impeccable dans nos locaux. Nous avons effectué une inspection détaillée de celui-ci et
    nous sommes satisfaits de son bon état de conservation.
</p>
<p>
    De plus, je suis ravi de vous annoncer que votre caution d'un montant de {{ eur($amount_caution) }} a été restituée dans son intégralité. Nos équipes ont procédé à la vérification de l'équipement loué et tous les coûts associés ont été couverts par le montant de la caution, ce qui nous permet de vous rembourser le montant total comme convenu initialement.
</p>
<p>Je vous souhaite une excellente journée.</p>
<p>
    Cordialement,<br>
    Responsable du service location <br>
    {{ $rental_name }}<br>
</p>
