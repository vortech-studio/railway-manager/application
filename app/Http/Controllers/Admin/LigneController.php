<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Core\Gares;
use App\Models\Core\Ligne;
use App\Models\Core\LigneRequirement;
use Illuminate\Http\Request;

class LigneController extends Controller
{
    public function index()
    {
        $lignes = Ligne::all();

        return view('admin.ligne.index', [
            "lignes" => $lignes
        ]);
    }

    public function create()
    {
        return view('admin.ligne.create');
    }

    public function store(Request $request)
    {
        $origin_call = Gares::where('name', 'LIKE', '%'.$request->station_start_id."%");
        $destination_call = Gares::where('name', 'LIKE', '%'.$request->station_end_id."%");

        if($origin_call->exists()) {
            $origin = $origin_call->first();
        } else {
            alert("Erreur", "La gare de départ n'existe pas", "error");
            return redirect()->back()->with('error', "La gare de départ n'existe pas");
        }

        if($destination_call->exists()) {
            $destination = $destination_call->first();
        } else {
            alert("Erreur", "La gare d'arrivée n'existe pas", "error");
            return redirect()->back()->with('error', "La gare d'arrivée n'existe pas");
        }

        if($origin->id == $destination->id) {
            alert("Erreur", "La gare de départ et d'arrivée ne peuvent pas être les mêmes", "error");
            return redirect()->back()->with('error', "La gare de départ et d'arrivée ne peuvent pas être les mêmes");
        }

        $ligne = Ligne::create([
            "nb_station" => $request->nb_station,
            "station_start_id" => $origin->id,
            "station_end_id" => $destination->id,
            "hub_id" => $request->hub_id
        ]);

        if($ligne->stationStart->hub) {
            LigneRequirement::create([
                "ligne_id" => $ligne->id,
                "hub_id" => Gares::where('name', "LIKE", "%".$origin->name."%")->first()->hub->id,
            ]);
        }

        if($ligne->stationEnd->hub) {
            LigneRequirement::create([
                "ligne_id" => $ligne->id,
                "hub_id" => Gares::where('name', "LIKE", "%".$destination->name."%")->first()->hub->id,
            ]);
        }

        alert("Succès", "La ligne a bien été créée", "success");
        return redirect()->route('admin.ligne.index')->with('success', "La ligne a bien été créée");
    }

    public function show($ligne_id)
    {
        $ligne = Ligne::with('hub', 'stations', 'stationStart', 'stationEnd', 'requirements')
            ->findOrFail($ligne_id);

        return view('admin.ligne.show', [
            "ligne" => $ligne
        ]);
    }
}
