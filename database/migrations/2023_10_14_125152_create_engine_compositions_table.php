<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEngineCompositionsTable extends Migration
{
    public function up(): void
    {
        Schema::create('engine_compositions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('type', ["motrice", "voiture", "dual"])->default('motrice');
            $table->string('sub_type')->nullable();
            $table->string('importance')
                ->default(0)
                ->comment("The importance of the engine composition. The higher the number, the more important it is. (0,4)");
            $table->integer('maintenance_check_time')->comment("The time it takes to check the engine composition. (min)");
            $table->integer('maintenance_repar_time')->comment("The time it takes to repair the engine composition. (min)");

            $table->foreignId('engine_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('engine_compositions');
    }
}
