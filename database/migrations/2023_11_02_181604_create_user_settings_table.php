<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSettingsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_settings', function (Blueprint $table) {
            $table->id();
            $table->boolean('auto_ter')->default(0);
            $table->boolean('auto_tgv')->default(0);
            $table->boolean('auto_ic')->default(0);
            $table->boolean('auto_transilien')->default(0);
            $table->integer('auto_ter_max')->default(0);
            $table->integer('auto_tgv_max')->default(0);
            $table->integer('auto_ic_max')->default(0);
            $table->integer('auto_transilien_max')->default(0);

            $table->foreignId('user_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_settings');
    }
}
