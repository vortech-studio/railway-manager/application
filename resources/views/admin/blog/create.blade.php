@extends("admin.template")

@section("css")

@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Création d'un article",
        'action' => true,
        'data' => [
            [
                'url' => back()->getTargetUrl(),
                'title' => "<i class='fa-solid fa-arrow-left me-2'></i> Retour",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <form action="{{ route('admin.blog.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card card-flush shadow-lg">
            <div class="card-body">
                <x-form.input
                    name="title"
                    label="Titre de l'annonce"
                    required="true" />
                <x-form.select
                    name="type_article"
                    label="Type d'annonce"
                    :datas="\App\Models\Core\Blog\Articles::getDataTypeAnnonce()" />

                <x-form.textarea
                    name="content_annonce"
                    label="Description de l'annonce"
                    required="true" />
            </div>
            <div class="card-footer d-flex flex-end">
                <x-form.button />
            </div>
        </div>
    </form>
@endsection

@section("js")
    <script type="text/javascript">

    </script>
@endsection
