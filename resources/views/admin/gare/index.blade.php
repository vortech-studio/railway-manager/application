@extends("admin.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Gestion des Gares",
        'action' => false,
        'data' => [
            /*[
                'url' => route('admin.dashboard'),
                'title' => "Tableau de Bord",
                'class' => "primary"
            ],
            [
                'url' => route('admin.dashboard'),
                'title' => "Tableau de Bord",
                'class' => "primary"
            ]*/
        ]
    ])
@endsection

@section("content")
    <div class="card card-flush shadow-sm border-0">
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <div class="card-title">
                <div class="d-flex align-items-center position-relative my-1">
                    <i class="ki-duotone ki-magnifier fs-3 position-absolute ms-4">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                    <input type="text" data-gare-filter="search" class="form-control form-control-solid w-250px ps-12" placeholder="Rechercher une gare" />
                </div>
            </div>
            <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                <div class="w-150px me-3">
                    <!--begin::Select2-->
                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Hub" data-kt-ecommerce-order-filter="hubs">
                        <option></option>
                        <option value="all">All</option>
                        <option value="1">Hub</option>
                    </select>
                    <!--end::Select2-->
                </div>
                <button class="btn btn-outline btn-outline-primary me-3" data-action="importGare"><i class="fa-solid fa-download me-2"></i> Importer les Gares</button>
                <button class="btn btn-outline btn-outline-primary me-3" data-action="exportGare"><i class="fa-solid fa-upload me-2"></i> Exporter les Gares</button>
                <a href="{{ route('admin.gare.create') }}" class="btn btn-primary"><i class="fa-solid fa-plus me-2"></i> Nouvelle Gare</a>
            </div>
        </div>
        <div class="card-body pt-0">
            <table class="table align-middle table-row-dashed fs-6 gy-5" id="liste_gares">
                <thead>
                    <tr>
                        <th>HUB</th>
                        <th>Code UIC</th>
                        <th>Nom de la Gare</th>
                        <th>Info Superficie</th>
                        <th>Info infrastructure</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($gares as $gare)
                    <tr class="">
                        <td>
                            @if(isset($gare->hub))
                                <i class="fa-solid fa-check text-success fs-2x"></i>
                                <span class="d-none">1</span>
                            @endif
                        </td>
                        <td>{{ $gare->code_uic }}</td>
                        <td>{{ $gare->name }}</td>
                        <td>
                            <div class="d-flex flex-column">
                                <div class="d-flex flex-row justify-content-between align-items-center mb-2">
                                    <strong>Infrastructure</strong>
                                    <span class="badge badge-light-primary">{{ $gare->superficie_infra }} m²</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center">
                                    <strong>Quai</strong>
                                    <span class="badge badge-light-primary">{{ $gare->superficie_quai }} m²</span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="d-flex flex-row justify-content-between align-items-center p-5">
                                @if($gare->commerce)
                                    <div class="d-flex flex-center align-items-center border border-2 border-success p-3">
                                        <i class="fa-solid fa-check text-success fs-2x me-2"></i>
                                        Commerce
                                    </div>
                                @else
                                    <div class="d-flex flex-center align-items-center border border-2 border-danger p-3">
                                        <i class="fa-solid fa-times text-danger fs-2x me-2"></i>
                                        Commerce
                                    </div>
                                @endif
                                @if($gare->pub)
                                    <div class="d-flex flex-center align-items-center border border-2 border-success p-3">
                                        <i class="fa-solid fa-check text-success fs-2x me-2"></i>
                                        Publicité
                                    </div>
                                @else
                                    <div class="d-flex flex-center align-items-center border border-2 border-danger p-3">
                                        <i class="fa-solid fa-times text-danger fs-2x me-2"></i>
                                        Publicité
                                    </div>
                                @endif
                                @if($gare->parking)
                                    <div class="d-flex flex-center align-items-center border border-2 border-success p-3">
                                        <i class="fa-solid fa-check text-success fs-2x me-2"></i>
                                        Parking
                                    </div>
                                @else
                                    <div class="d-flex flex-center align-items-center border border-2 border-danger p-3">
                                        <i class="fa-solid fa-times text-danger fs-2x me-2"></i>
                                        Parking
                                    </div>
                                @endif
                            </div>
                        </td>
                        <td class="align-items-center h-100">
                            <x-base.button
                                class="btn-icon btn-sm btn-outline btn-outline-secondary me-3 btnViewGare"
                                :datas="[['name' => 'id', 'value' => $gare->id]]"
                                text="<i class='fa-solid fa-eye'></i>"
                                tooltip="Voir la fiche"
                                text-indicator="" />
                            <x-base.button
                                class="btn-icon btn-sm btn-outline btn-outline-danger me-3 btnDeleteGare"
                                :datas="[['name' => 'id', 'value' => $gare->id]]"
                                text="<i class='fa-solid fa-trash'></i>"
                                tooltip="Supprimer"
                                text-indicator="" />
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
@endsection

@section("js")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let table = $("#liste_gares").DataTable({
            info: !1,
            order: [[1, 'desc']],
            pageLength: 10,
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
            },
        })

        document.querySelector('[data-gare-filter="search"]').addEventListener('keyup', function (e) {
            table.search(e.target.value).draw();
        });

        document.querySelector('[data-action="importGare"]').addEventListener('click', function (e) {
            e.preventDefault()
            $.ajax({
                url: "{{ route('api.command.import-gare') }}",
                method: "GET",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function (data) {
                    console.log(data)
                    toastr.success("La tache d'import des gares à été lancée")
                }
            })
        })

        document.querySelector('[data-action="exportGare"]').addEventListener('click', function (e) {
            e.preventDefault()
            $.ajax({
                url: "{{ route('api.command.export-gare') }}",
                method: "GET",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function (data) {
                    console.log(data)
                    toastr.success("La tache d'export des gares à été lancée")
                }
            })
        })

        document.querySelectorAll('.btnViewGare').forEach(function (btn) {
            btn.addEventListener('click', function (e) {
                e.preventDefault()
                let id = btn.dataset.id
                window.location.href = '/admin/gares/' + id
            })
        })

        document.querySelectorAll('.btnDeleteGare').forEach(function (btn) {
            btn.addEventListener('click', function (e) {
                e.preventDefault()
                let id = btn.dataset.id
                btn.setAttribute('data-kt-indicator', 'on')
                $.ajax({
                    url: "/api/core/gare/" + id,
                    method: "DELETE",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id
                    },
                    success: function (data) {
                        console.log(data)
                        btn.removeAttribute('data-kt-indicator')
                        toastr.success("La gare à été supprimée")
                        btn.parentNode.parentNode.style.display = "none"
                    }
                })
            })
        });

        const e = document.querySelector('[data-kt-ecommerce-order-filter="hubs"]')
        $(e).on('change', (e => {
            let o = e.target.value
            "all" === o && (o = ""), table.column(0).search(o).draw()
        }))
    </script>
@endsection
