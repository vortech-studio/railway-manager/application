<?php

namespace App\Http\Controllers\Api\Game\Ecran;

use App\Http\Controllers\Controller;
use App\Models\User\UserPlanning;
use App\Services\Polyline;
use Carbon\Carbon;

class TravelController extends Controller
{

    public function travel($planning_id)
    {
        $planning = UserPlanning::find($planning_id);

        if($planning->status == 'travel') {
            if($planning->ligne->ligne->type_ligne == 'ter'):
                ob_start();
                ?>
                <div class="d-flex flex-column bg-white rounded-3 shadow-lg h-600px">
                    <div class="d-flex flex-row justify-content-between align-items-center bg-color-eva h-100px overflow-hidden fs-2x fw-bold text-white px-5">
                        <div class="d-flex">
                            <img src="<?= asset($planning->ligne->ligne->type_ligne_logo) ?>" class="w-70px me-5" alt="">
                            <span class=""><?= \Str::upper($planning->engine->engine->type_train_format) ?> <?= number_format($planning->number_travel, 0, '', ' ') ?></span>
                        </div>
                        <div class="d-flex speedometer me-20">0 Km/h</div>
                    </div>
                    <div class="d-flex flex-row-fluid align-items-center h-100">
                        <div class="d-flex flex-column justify-content-center w-30 bg-gray-400 h-100 py-auto">
                            <div class="d-flex flex-column mb-5 mx-10">
                                <span class="fs-3 text-color-ter">Destination</span>
                                <div class="fs-2x fw-bold text-gray-800"><?= $planning->ligne->ligne->stationEnd->name ?></div>
                            </div>
                            <div class="d-flex flex-column mb-5 mx-10">
                                <span class="fs-3 text-color-ter">Prochain arrets</span>
                                <div class="fs-2x fw-bold text-gray-800"><?= $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->name ?></div>
                            </div>
                            <div class="d-flex flex-column mb-5 mx-10">
                                <span class="fs-3 text-color-ter">Arrivée prévue</span>
                                <div class="fs-3x fw-bold text-gray-800"><?= $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at->format("H:i") ?></div>
                            </div>
                        </div>
                        <div class="d-flex bg-white w-100 py-auto">

                            <div id="kt_carousel_1_carousel" class="carousel carousel-custom slide w-100 h-100" data-bs-ride="carousel" data-bs-interval="5000">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div class="w-25 d-flex flex-column justify-content-center align-items-center mb-3 ms-2">
                                                <div class="w-100px h-100px rounded-circle border border-2 border-color-ter d-flex justify-content-center align-items-center">
                                                    <img src="<?= asset('storage/icons/station.png') ?>" class="w-70px" alt="">
                                                </div>
                                                <span class="border border-2 border-color-ter rounded-5 fs-3 fw-semibold text-center mt-3 p-2">
                                            <?= $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->name ?>
                                        </span>
                                                <div class="fw-bolder fs-2"><?= $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->departure_at->format("H:i") ?></div>
                                            </div>
                                            <div class="h-20px mx-3 w-50 bg-gray-400 bg-opacity-50 rounded mb-10">
                                                <div class="bg-color-ter rounded h-20px progress-bar-striped progress-bar-animated" role="progressbar" style="width: <?= $this->calcPercentTimeInPercent($planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->departure_at, $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at) ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                                    <div class="position-relative">
                                                        <img src="<?= asset('storage/icons/train_point.png'); ?>" alt="" class="w-45px translate-middle" style="position: absolute; top: -25px; left: 100%">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-25 d-flex flex-column justify-content-center align-items-center mb-3 animate__animated animate__flash animate__slower animate__infinite">
                                                <div class="w-100px h-100px rounded-circle border border-2 border-dotted border-color-ter d-flex justify-content-center align-items-center">
                                                    <img src="<?= asset('storage/icons/station_end.png') ?>" class="w-70px" alt="">
                                                </div>
                                                <span class="border border-2 border-dotted border-color-ter rounded-5 fs-3 fw-semibold text-center mt-3 p-2">
                                            <?= $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->name ?>
                                        </span>
                                                <div class="fw-bolder fs-2"><?= $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at->format("H:i") ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div id="map" class="w-100 h-100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex align-items-middle bg-gray-700 h-80px overflow-hidden">
                        <div class="d-flex flex-row justify-content-between align-items-center mx-5">
                            <div>
                                <span id="clock-hours" class="fs-2x text-grey-500 fw-semibold">23</span>
                                <span class="animation-blink">
                                <span class="animation-blink-clock fs-2x text-grey-500 fw-semibold">:</span>
                            </span>
                                <span id="clock-minutes" class="fs-2x text-grey-500 fw-semibold">32</span>
                                <small id="clock-seconds" class="fs-3 ms-1 text-color-ter ">37</small>
                            </div>
                            <img src="<?=asset('/storage/avatar/company/4.png') ?>" class="w-50px position-absolute bottom-0 end-0 me-5" alt="">
                        </div>
                    </div>
                </div>
                <?php
                $content = ob_get_clean();
                $data = [];
                $data['ligne'] = [
                    "type" => $planning->ligne->ligne->type_ligne,
                    "coordinates" => [
                        [$planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->longitude,$planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->latitude],
                        [$planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->longitude,$planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->latitude]
                    ]
                ];
                $data['station_start'] = [
                    "name" => $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->name,
                    "nb_voyageur" => $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->getPrevPassenger(),
                    "longitude" => $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->longitude,
                    "latitude" => $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->latitude
                ];
                $data['station_end'] = [
                    "name" => $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->name,
                    "nb_voyageur" => $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->getPrevPassenger(),
                    "longitude" => $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->longitude,
                    "latitude" => $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->latitude,
                    "next_time" => $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at->diffInMilliseconds($planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->departure_at)
                ];
            endif;
            if($planning->ligne->ligne->type_ligne == 'tgv'):
                ob_start();
                ?>
                <div class="d-flex flex-column bg-color-tgv rounded-3 shadow-lg h-550px">
                    <div class="d-flex align-items-center h-130px bg-white">
                        <div class="d-flex flex-column w-25">
                            <div class="d-flex flex-row justify-content-between align-items-center">
                                <img src="<?= asset('/storage/logos/logo_tgv.svg') ?>" class="ps-5 w-100px" alt="">
                                <div class="d-flex flex-column justify-content-center align-items-center">
                                    <iconify-icon icon="mdi:train-car-passenger-variant" class="text-gray-400" width="70" height="70"></iconify-icon>
                                    <span class="fs-3x fw-bold text-gray-400"><?= rand(1, $planning->engine->engine->technical->nb_wagon) ?></span>
                                </div>
                            </div>
                            <div class="ps-5">
                                <span id="clock-hours" class="fs-3x text-black fw-semibold">23</span>
                                <span class="animation-blink">
                                <span class="animation-blink-clock fs-2x black fw-semibold">:</span>
                            </span>
                                <span id="clock-minutes" class="fs-3x text-black fw-semibold">32</span>
                                <small id="clock-seconds" class="fs-3 ms-1 text-color-tgv ">37</small>
                            </div>
                        </div>
                        <div class="flex-grow-1">
                            <div class="d-flex align-items-center w-100">
                                <div class="separator border border-5 border-dotted border-color-tgv w-25"></div>
                                <div class="d-flex flex-column align-items-start justify-content-center position-relative">
                                    <span class="fs-2 fw-bold text-color-tgv position-absolute w-200px m-5" style="top: 26px;right: -94px;"><?= $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->name ?></span>
                                    <div class="bullet bullet-dot w-40px h-40px bg-white border border-5 border-color-tgv"></div>
                                </div>
                                <div class="h-10px mx-3 w-100 bg-gray-300 bg-opacity-50 rounded w-40 position-relative" style="right: 12px">
                                    <div class="bg-color-tgv rounded h-10px" role="progressbar" style="width: <?= $this->calcPercentTimeInPercent($planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->departure_at, $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at) ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        <div class="position-relative">
                                            <img src="<?= asset('storage/icons/icon_tgv.png'); ?>" alt="" class="w-40px translate-middle" style="position: absolute;left: 104%;top: 5px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex flex-column align-items-start justify-content-center position-relative" style="right: 26px">
                                    <span class="fs-2 fw-bold text-color-tgv position-absolute m-5 text-center w-200px" style="top: 26px;right: -94px;"><?= $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->name ?></span>
                                    <div class="bullet bullet-dot w-40px h-40px bg-white border border-5 border-color-tgv"></div>
                                </div>
                                <div class="separator border border-5 border-dotted border-color-tgv w-25 position-relative" style="right: 25px;"></div>
                            </div>
                        </div>
                    </div>
                    <div id="kt_carousel_1_carousel" class="carousel carousel-custom slide" data-bs-ride="carousel" data-bs-interval="5000">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="d-flex flex-row justify-content-center align-items-center py-auto position-relative h-350px bg-opacity-5 bgi-no-repeat bgi-size-cover bgi-position-x-end " style="background-image: url('<?= asset('/storage/other/wall_wave_1.png') ?>');">
                                    <div class="w-20 d-flex align-items-end justify-content-end position-relative">
                                        <span class="text-white fw-lighter fs-3hx text-end">Vitesse Actuel de votre train</span>
                                    </div>
                                    <div class="w-20 d-flex align-items-baseline justify-content-end position-relative">
                                        <span class="text-white fw-bold fs-7hx speedometer">301</span>
                                        <span class="text-white fs-1 text-end">Km/h</span>
                                    </div>
                                </div>
                            </div>
                            <?php if($planning->incidents()->where('niveau', '>=', 2)->count() != 0): ?>
                                <div class="carousel-item">
                                    <div class="d-flex flex-row justify-content-center align-items-center h-350px">
                                        <div class="w-25">
                                            <iconify-icon icon="mdi:clock-warning" class="text-color-inoui opacity-20" width="230" height="230"></iconify-icon>
                                        </div>
                                        <div class="d-flex flex-column w-50">
                                            <span class="fs-3x fw-bold text-white"><?= $planning->incidents()->where('niveau', '>=', 2)->first()->designation; ?></span>
                                            <span class="fs-3x fw-bold text-gray-300">Nous avons environ <span class="text-yellow-600"><?= $planning->retarded_time; ?> <?= \Str::plural('minute', $planning->retarded_time) ?></span> de retard</span>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="carousel-item">
                                <div class="h-400px">
                                    <table class="table table-striped table-inverse align-middle gx-7">
                                        <tbody class="animation-dynamique-y">
                                        <?php foreach($planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->limit(5)->get() as $station): ?>
                                            <tr class="h-75px">
                                                <td class="w-200px text-center">
                                                    <span class="fw-bold fs-1"><?= $station->arrival_at->format("H:i"); ?></span>
                                                </td>
                                                <td class="w-50px border border-left-5 border-color-tgv border-right-0 border-top-0 border-bottom-0 position-relative">
                                                    <span class="bullet bullet-dot w-25px h-25px bg-white border border-5 border-color-tgv position-absolute" style="top: 25px;left: -15px;"></span>
                                                </td>
                                                <td class="text-white w-full">
                                                    <span class="fw-bolder fs-2x"><?= $station->name; ?></span>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $content = ob_get_clean();
                $data = [];
                $data['next_time'] = $this->calcPercentTimeInPercent($planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->departure_at, $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at);
            endif;
        } elseif($planning->status == 'in_station') {
            if($planning->ligne->ligne->type_ligne == 'ter'):
                ob_start();
                ?>
                <div class="d-flex flex-column bg-white rounded-3 shadow-lg h-600px">
                    <div class="d-flex flex-row justify-content-between align-items-center bg-color-eva h-100px overflow-hidden fs-2x fw-bold text-white px-5">
                        <div class="d-flex">
                            <img src="<?= asset($planning->ligne->ligne->type_ligne_logo) ?>" class="w-70px me-5" alt="">
                            <span class=""><?= \Str::upper($planning->engine->engine->type_train_format) ?> <?= number_format($planning->number_travel, 0, '', ' ') ?></span>
                        </div>
                        <div class="d-flex speedometer me-20">0 Km/h</div>
                    </div>
                    <div class="d-flex flex-row-fluid align-items-center h-100">
                        <div class="d-flex flex-column justify-content-center w-30 bg-gray-400 h-100 py-auto">
                            <div class="d-flex flex-column mb-5 mx-10">
                                <span class="fs-3 text-color-ter">Destination</span>
                                <div class="fs-2x fw-bold text-gray-800"><?= $planning->ligne->ligne->stationEnd->name ?></div>
                            </div>
                            <div class="d-flex flex-column mb-5 mx-10">
                                <span class="fs-3 text-color-ter">Prochain arrets</span>
                                <div class="fs-2x fw-bold text-gray-800"><?= $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->name ?></div>
                            </div>
                            <div class="d-flex flex-column mb-5 mx-10">
                                <span class="fs-3 text-color-ter">Arrivée prévue</span>
                                <div class="fs-3x fw-bold text-gray-800"><?= $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at->format("H:i") ?></div>
                            </div>
                        </div>
                        <div class="d-flex bg-white w-100 py-auto">

                            <div id="kt_carousel_1_carousel" class="carousel carousel-custom slide w-100 h-100" data-bs-ride="carousel" data-bs-interval="5000">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div class="w-25 d-flex flex-column justify-content-center align-items-center mb-3 ms-2">
                                                <div class="w-100px h-100px rounded-circle border border-2 border-color-ter d-flex justify-content-center align-items-center">
                                                    <img src="<?= asset('storage/icons/station.png') ?>" class="w-70px" alt="">
                                                </div>
                                                <span class="border border-2 border-color-ter rounded-5 fs-3 fw-semibold text-center mt-3 p-2">
                                            <?= $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->name ?>
                                        </span>
                                                <div class="fw-bolder fs-2"><?= $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->departure_at->format("H:i") ?></div>
                                            </div>
                                            <div class="h-20px mx-3 w-50 bg-gray-400 bg-opacity-50 rounded mb-10">
                                                <div class="bg-color-ter rounded h-20px progress-bar-striped progress-bar-animated" role="progressbar" style="width: <?= $this->calcPercentTimeInPercent($planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->departure_at, $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at) ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                                    <div class="position-relative">
                                                        <img src="<?= asset('storage/icons/train_point.png'); ?>" alt="" class="w-45px translate-middle" style="position: absolute; top: -25px; left: 100%">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-25 d-flex flex-column justify-content-center align-items-center mb-3 animate__animated animate__flash animate__slower animate__infinite">
                                                <div class="w-100px h-100px rounded-circle border border-2 border-dotted border-color-ter d-flex justify-content-center align-items-center">
                                                    <img src="<?= asset('storage/icons/station_end.png') ?>" class="w-70px" alt="">
                                                </div>
                                                <span class="border border-2 border-dotted border-color-ter rounded-5 fs-3 fw-semibold text-center mt-3 p-2">
                                            <?= $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->name ?>
                                        </span>
                                                <div class="fw-bolder fs-2"><?= $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at->format("H:i") ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div id="map" class="w-100 h-100"></div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="d-flex flex-column justify-content-center align-items-center h-100 w-100">
                                            <div class="fs-2x text-gray-800">Arret en gare de:</div>
                                            <div class="fs-5x fw-bolder text-color-ter"><?= $planning->planning_stations()->where('status', 'arrival')->orWhere('status', 'departure')->orderBy('arrival_at')->first()->name ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex align-items-middle bg-gray-700 h-80px overflow-hidden">
                        <div class="d-flex flex-row justify-content-between align-items-center mx-5">
                            <div>
                                <span id="clock-hours" class="fs-2x text-grey-500 fw-semibold">23</span>
                                <span class="animation-blink">
                                <span class="animation-blink-clock fs-2x text-grey-500 fw-semibold">:</span>
                            </span>
                                <span id="clock-minutes" class="fs-2x text-grey-500 fw-semibold">32</span>
                                <small id="clock-seconds" class="fs-3 ms-1 text-color-ter ">37</small>
                            </div>
                            <img src="<?=asset('/storage/avatar/company/4.png') ?>" class="w-50px position-absolute bottom-0 end-0 me-5" alt="">
                        </div>
                    </div>
                </div>
                <?php
                $content = ob_get_clean();
                $data = [];
                $data['ligne'] = [
                    "type" => $planning->ligne->ligne->type_ligne,
                    "coordinates" => [
                        [$planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->longitude,$planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->latitude],
                        [$planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->longitude,$planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->latitude]
                    ]
                ];
                $data['station_start'] = [
                    "name" => $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->name,
                    "nb_voyageur" => $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->getPrevPassenger(),
                    "longitude" => $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->longitude,
                    "latitude" => $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->latitude
                ];
                $data['station_end'] = [
                    "name" => $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->name,
                    "nb_voyageur" => $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->getPrevPassenger(),
                    "longitude" => $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->longitude,
                    "latitude" => $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->latitude,
                    "next_time" => $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at->diffInMilliseconds($planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->departure_at)
                ];
            endif;
            if($planning->ligne->ligne->type_ligne == 'tgv'):
                ob_start();
                ?>
                <div class="d-flex flex-column bg-color-tgv rounded-3 shadow-lg h-550px">
                    <div class="d-flex align-items-center h-130px bg-white">
                        <div class="d-flex flex-column w-25">
                            <div class="d-flex flex-row justify-content-between align-items-center">
                                <img src="<?= asset('/storage/logos/logo_tgv.svg') ?>" class="ps-5 w-100px" alt="">
                                <div class="d-flex flex-column justify-content-center align-items-center">
                                    <iconify-icon icon="mdi:train-car-passenger-variant" class="text-gray-400" width="70" height="70"></iconify-icon>
                                    <span class="fs-3x fw-bold text-gray-400"><?= rand(1, $planning->engine->engine->technical->nb_wagon) ?></span>
                                </div>
                            </div>
                            <div class="ps-5">
                                <span id="clock-hours" class="fs-3x text-black fw-semibold">23</span>
                                <span class="animation-blink">
                                <span class="animation-blink-clock fs-2x black fw-semibold">:</span>
                            </span>
                                <span id="clock-minutes" class="fs-3x text-black fw-semibold">32</span>
                                <small id="clock-seconds" class="fs-3 ms-1 text-color-tgv ">37</small>
                            </div>
                        </div>
                        <div class="flex-grow-1">
                            <div class="d-flex align-items-center w-100">
                                <div class="separator border border-5 border-dotted border-color-tgv w-25"></div>
                                <div class="d-flex flex-column align-items-start justify-content-center position-relative">
                                    <span class="fs-2 fw-bold text-color-tgv position-absolute w-200px m-5" style="top: 26px;right: -94px;"><?= $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->name ?></span>
                                    <div class="bullet bullet-dot w-40px h-40px bg-white border border-5 border-color-tgv"></div>
                                </div>
                                <div class="h-10px mx-3 w-100 bg-gray-300 bg-opacity-50 rounded w-40 position-relative" style="right: 12px">
                                    <div class="bg-color-tgv rounded h-10px" role="progressbar" style="width: <?= $this->calcPercentTimeInPercent($planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->departure_at, $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at) ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        <div class="position-relative">
                                            <img src="<?= asset('storage/icons/icon_tgv.png'); ?>" alt="" class="w-40px translate-middle" style="position: absolute;left: 104%;top: 5px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex flex-column align-items-start justify-content-center position-relative" style="right: 26px">
                                    <span class="fs-2 fw-bold text-color-tgv position-absolute m-5 text-center w-200px" style="top: 26px;right: -94px;"><?= $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->name ?></span>
                                    <div class="bullet bullet-dot w-40px h-40px bg-white border border-5 border-color-tgv"></div>
                                </div>
                                <div class="separator border border-5 border-dotted border-color-tgv w-25 position-relative" style="right: 25px;"></div>
                            </div>
                        </div>
                    </div>
                    <div id="kt_carousel_1_carousel" class="carousel carousel-custom slide" data-bs-ride="carousel" data-bs-interval="5000">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="d-flex flex-row justify-content-center align-items-center py-auto position-relative h-350px bg-opacity-5 bgi-no-repeat bgi-size-cover bgi-position-x-end " style="background-image: url('<?= asset('/storage/other/wall_wave_1.png') ?>');">
                                    <div class="w-20 d-flex align-items-end justify-content-end position-relative">
                                        <span class="text-white fw-lighter fs-3hx text-end">Vitesse Actuel de votre train</span>
                                    </div>
                                    <div class="w-20 d-flex align-items-baseline justify-content-end position-relative">
                                        <span class="text-white fw-bold fs-7hx speedometer">301</span>
                                        <span class="text-white fs-1 text-end">Km/h</span>
                                    </div>
                                </div>
                            </div>
                            <?php if($planning->incidents()->where('niveau', '>=', 2)->count() != 0): ?>
                                <div class="carousel-item">
                                    <div class="d-flex flex-row justify-content-center align-items-center h-350px">
                                        <div class="w-25">
                                            <iconify-icon icon="mdi:clock-warning" class="text-color-inoui opacity-20" width="230" height="230"></iconify-icon>
                                        </div>
                                        <div class="d-flex flex-column w-50">
                                            <span class="fs-3x fw-bold text-white"><?= $planning->incidents()->where('niveau', '>=', 2)->first()->designation; ?></span>
                                            <span class="fs-3x fw-bold text-gray-300">Nous avons environ <span class="text-yellow-600"><?= $planning->retarded_time; ?> <?= \Str::plural('minute', $planning->retarded_time) ?></span> de retard</span>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="carousel-item">
                                <div class="h-400px d-flex flex-column justify-content-center align-items-center">
                                    <div class="fs-2x text-gray-300">Arret en gare de:</div>
                                    <div class="fs-5x text-color-inoui"><?= $planning->planning_stations()->where('status', 'arrival')->orWhere('status', 'departure')->orderBy('arrival_at')->first()->name ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $content = ob_get_clean();
                $data = [];
                $data['next_time'] = $this->calcPercentTimeInPercent($planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->departure_at, $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->arrival_at);
            endif;
        }

        return response()->json([
            "content" => $content,
            "data" => $data,
        ]);
    }

    public function arrival($planning_id)
    {
        $planning = UserPlanning::find($planning_id);

        if($planning->ligne->ligne->type_ligne == 'ter'):
            ob_start();
            ?>
            <div class="d-flex flex-column bg-white rounded-3 shadow-lg h-600px">
                <div class="d-flex flex-row justify-content-between align-items-center bg-color-eva h-100px overflow-hidden fs-2x fw-bold text-white px-5">
                    <div class="d-flex">
                        <img src="<?= asset($planning->ligne->ligne->type_ligne_logo) ?>" class="w-70px me-5" alt="">
                        <span class=""><?= \Str::upper($planning->engine->engine->type_train_format) ?> <?= number_format($planning->number_travel, 0, '', ' ') ?></span>
                    </div>
                    <div class="d-flex speedometer me-20">0 Km/h</div>
                </div>
                <div class="d-flex flex-row-fluid align-items-center h-100">
                    <div class="d-flex flex-column justify-content-center align-items-center w-70">
                        <div class="fs-3hx text-color-ter fw-bolder"><?= $planning->ligne->ligne->stationEnd->name ?></div>
                        <div class="fs-5x text-gray-400 fw-bold">Terminus</div>
                    </div>
                </div>
                <div class="d-flex align-items-middle bg-gray-700 h-80px overflow-hidden">
                    <div class="d-flex flex-row justify-content-between align-items-center mx-5">
                        <div>
                            <span id="clock-hours" class="fs-2x text-grey-500 fw-semibold">23</span>
                            <span class="animation-blink">
                                <span class="animation-blink-clock fs-2x text-grey-500 fw-semibold">:</span>
                            </span>
                            <span id="clock-minutes" class="fs-2x text-grey-500 fw-semibold">32</span>
                            <small id="clock-seconds" class="fs-3 ms-1 text-color-ter ">37</small>
                        </div>
                        <img src="<?=asset('/storage/avatar/company/4.png') ?>" class="w-50px position-absolute bottom-0 end-0 me-5" alt="">
                    </div>
                </div>
            </div>
            <?php
            $content = ob_get_clean();
            $data = [];
        endif;
        if($planning->ligne->ligne->type_ligne == 'tgv'):
            ob_start();
            ?>
            <div class="d-flex flex-column bg-color-tgv rounded-3 shadow-lg h-550px">
                <div class="d-flex align-items-center h-130px bg-white">
                    <div class="d-flex flex-column w-25">
                        <div class="d-flex flex-row justify-content-between align-items-center">
                            <img src="<?= asset('/storage/logos/logo_tgv.svg') ?>" class="ps-5 w-100px" alt="">
                            <div class="d-flex flex-column justify-content-center align-items-center">
                                <iconify-icon icon="mdi:train-car-passenger-variant" class="text-gray-400" width="70" height="70"></iconify-icon>
                                <span class="fs-3x fw-bold text-gray-400"><?= rand(1, $planning->engine->engine->technical->nb_wagon) ?></span>
                            </div>
                        </div>
                        <div class="ps-5">
                            <span id="clock-hours" class="fs-3x text-black fw-semibold">23</span>
                            <span class="animation-blink">
                                <span class="animation-blink-clock fs-2x black fw-semibold">:</span>
                            </span>
                            <span id="clock-minutes" class="fs-3x text-black fw-semibold">32</span>
                            <small id="clock-seconds" class="fs-3 ms-1 text-color-tgv ">37</small>
                        </div>
                    </div>
                    <div class="flex-grow-1">
                        <div class="d-flex flex-column justify-content-center align-items-center">
                            <div class="fw-bolder text-color-tgv fs-3x"><?= $planning->ligne->ligne->stationEnd->name ?></div>
                            <div class="fw-bolder text-gray-400 fs-2x">Terminus</div>
                            <div class="fw-bolder text-gray-500 fs-3x">Train en correspondance</div>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <table class="table table-striped table-dark gs-5 gs-5 gy-5 p-0 m-0">
                        <thead>
                            <tr class="fs-2x fw-semibold">
                                <th>Départ</th>
                                <th></th>
                                <th>Train</th>
                                <th>Destination</th>
                                <th>Voie</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="fw-bolder fs-1">
                                <td class="w-100px">18:00</td>
                                <td>TGV</td>
                                <td>8 360</td>
                                <td>Marseille Saint-Charles</td>
                                <td>4</td>
                            </tr>
                            <tr class="fw-bolder fs-1">
                                <td class="w-100px">18:00</td>
                                <td>TGV</td>
                                <td>8 360</td>
                                <td>Marseille Saint-Charles</td>
                                <td>4</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
            $content = ob_get_clean();
            $data = [];
        endif;

        return response()->json([
            "content" => $content,
            "data" => $data,
        ]);
    }

    public function getMapUri($planning_id)
    {
        $planning = UserPlanning::find($planning_id);
        $marker_start = $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->longitude.','.$planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->latitude;
        $marker_end = $planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->longitude.','.$planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->latitude;

        $polyline = new Polyline();
        $end = $polyline->encodePoints([
            [$planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->latitude,$planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->longitude],
            [$planning->planning_stations()->where('status', 'init')->orderBy('arrival_at')->first()->ligneStation->gare->latitude,$planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->longitude]
        ]);
        $path = 'path-6('.rawurlencode($end).')';
        $http = "https://api.mapbox.com/styles/v1/mapbox/outdoors-v12/static/pin-s+555555(${marker_start}),pin-s+555555(${marker_end}),${path}/auto/720x480@2x?access_token=".config('services.mapbox.api_key');

        $response = \Http::get($http);

        return response()->json($http);
    }

    private function calcPercentTimeInPercent($departureActualStation, $arrivalNextStation)
    {
        $start = Carbon::createFromTimestamp(strtotime($departureActualStation))->timestamp;
        $end = Carbon::createFromTimestamp(strtotime($arrivalNextStation))->timestamp;
        $timespan = $end - $start;
        $current = now()->timestamp - $start;
        $progress = $current / $timespan;
        $remaining = ($progress) * 100;
        if($remaining >= 100) {
            return 100;
        } else {
            return $remaining;
        }
    }
}
