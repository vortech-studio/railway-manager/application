<?php

namespace App\Listeners;

use App\Models\Core\Achievement;
use App\Models\Core\Mouvement;
use App\Models\User;
use App\Traits\MouvementTrait;
use LevelUp\Experience\Events\AchievementAwarded;

class AchievementAwardedListener
{
    public function __construct()
    {
    }

    public function handle(AchievementAwarded $event): void
    {
        foreach (Achievement::find($event->achievement->id)->rewards as $reward) {
            match ($reward->type) {
                "argent" => $this->addMoney(User::find($event->user->id), $reward->value),
                "tpoint" => $this->addTpoint(User::find($event->user->id), $reward->value),
                "experience" => $this->addExperience(User::find($event->user->id), $reward->value),
            };
        }
    }

    private function addMoney(User $user, $value)
    {
        $mvm = new Mouvement();
        $mvm::adding(
            'revenue',
            "Récompense de succès",
            "divers",
            $value,
            $user->company->id,
        );
    }

    private function addTpoint(User $user, $value)
    {
        $user->tpoint += $value;
        $user->save();
    }

    private function addExperience(User $user, $value)
    {
        $user->addPoints($value);
    }

}
