@extends("game.template")

@section("css")
    <link src="{{ asset('/plugins/leaflet/leaflet.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section("info_bar")
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
        <!--begin::Page title-->
        <div class="page-title d-flex align-items-center me-3">
            <div class="symbol symbol-60 symbol-circle bg-white me-5">
                <img alt="Logo" src="{{ asset('/storage/avatar/company/'.auth()->user()->logo_company) }}"/>
            </div>
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">
                {{ auth()->user()->name_company }}
                <!--begin::Description-->
                <div class="d-flex flex-row">
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/avatar/secretary/'.auth()->user()->avatar_secretary) }}"
                             class="w-25px rounded-2 me-2" alt="Avatar Secretaire" data-bs-toggle="tooltip"
                             data-bs-placement="bottom" title="Secrétaire">
                        <div class="fw-light">{{ auth()->user()->name_secretary }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{ asset('/storage/icons/level-up.png') }}" class="w-25px rounded-2 me-2" alt="Niveau"
                             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Niveau">
                        <div class="fw-light">{{ auth()->user()->getLevel() }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <i class="ki-duotone ki-abstract-8 fs-7 text-success mx-3">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="fw-bolder">
                            Expérience:
                            <div class="fw-normal">{{ auth()->user()->getPoints() }}
                                sur {{ auth()->user()->nextLevelAt() }}</div>
                        </div>
                        <div class="h-8px mx-3 w-100 bg-white bg-opacity-50 rounded">
                            <div class="bg-white rounded h-8px" role="progressbar"
                                 style="width: {{ auth()->user()->nextLevelAt(null, true) }}%;"
                                 aria-valuenow="{{ auth()->user()->nextLevelAt(null, true) }}" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <!--end::Description-->
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Items-->
        <div class="d-flex gap-4 gap-lg-13">
            <div class="d-flex flex-column mb-10">
                <div class="d-flex flex-row justify-content-center align-items-center mb-5">
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/euro.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->argent) }}</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/tpoint.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ intval(auth()->user()->tpoint) }} Tpoint</div>
                    </div>
                    <div class="d-flex flex-row align-items-center me-3">
                        <div class="symbol symbol-30 symbol-circle bg-white me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/research.png') }}"/>
                        </div>
                        <div class="fs-1 text-white fw-bold">{{ eur(auth()->user()->research) }}</div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around align-items-center">
                    <div class="d-flex flex-row bg-light-primary rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/hub.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->hubs()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Hubs</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-warning rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/ligne.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->lignes()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Lignes</div>
                        </div>
                    </div>
                    <div class="d-flex flex-row bg-light-danger rounded-3 px-6 py-3 align-items-center">
                        <div class="symbol symbol-40 me-5">
                            <img alt="Logo" src="{{ asset('/storage/icons/train.png') }}"/>
                        </div>
                        <div class="d-flex flex-column justify-content-end">
                            <div class="fs-1 text-dark fw-bold">{{ auth()->user()->engines()->count() }}</div>
                            <div class="fs-5 text-dark fw-bold">Matériels Roulants</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Items-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row justify-content-between align-items-center p-5 bg-gray-300 mb-10">
        <div class="d-flex flex-row align-items-center">
            <div class="symbol symbol-50 me-3">
                <img src="{{ asset('/storage/icons/network.png') }}" alt="">
            </div>
            <x-base.underline
                title="Réseau Férroviaire"
                size="3"
                size-text="fs-1" />
        </div>
        @include("game.network.partials.menu")
    </div>
    @premium
    <div class="card bg-gray-600 mb-10">
        <div class="card-body h-450">
            <div id="map" style="height: 430px"></div>
        </div>
    </div>
    @endpremium
    @if(auth()->user()->premium)
        <div class="card shadow-lg mb-10">
            <div class="card-body">
                <table class="table table-bordered table-row-border table-row-gray-800 gs-2 gy-2 gx-2 mb-5">
                    <thead>
                        <tr class="fs-8 fw-bold text-center">
                            <th class="bg-warning">Hub Possédé</th>
                            <th class="bg-primary">Chiffre d'affaire</th>
                            <th class="bg-primary">Résultat</th>
                            <th class="bg-primary">Performance</th>
                            <th class="bg-primary"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(auth()->user()->hubs as $hub)
                            <tr class="{{ $hub->active == 0 ? 'bg-gray-500' : '' }}" @if($hub->active == 0) data-bs-toggle="tooltip" title="Livraison en cours"  @endif>
                                <td class="text-center">{{ $hub->hub->gare->name }}</td>
                                <td class="text-center">{{ eur($hub->calcCA()) }}</td>
                                <td class="text-center">{{ eur($hub->calcBenefice()) }}</td>
                                <td class="text-center">{!! $hub->calcRatioPerformance() !!} </td>
                                <td class="text-end">
                                    @if($hub->active)
                                    <a href="{{ route('hub.show', $hub->id) }}" class="" >
                                        <img src="{{ asset('/storage/icons/hub.png') }}" class="w-30px" alt="" data-bs-toggle="tooltip" title="Voir le hub">
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <table class="table table-bordered table-row-border table-row-gray-800 gs-2 gy-2 gx-2 mb-5">
                    <thead>
                    <tr class="fs-8 fw-bold text-center">
                        <th class="bg-primary">Ligne</th>
                        <th class="bg-primary">Distance</th>
                        <th class="bg-primary">Offre/Demande</th>
                        <th class="bg-primary">Chiffre d'affaire</th>
                        <th class="bg-primary">Résultat</th>
                        <th class="bg-primary">Performance</th>
                        <th class="bg-primary">
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(auth()->user()->lignes as $ligne)
                        <tr class="{{ $ligne->active == 0 ? 'bg-gray-500' : '' }}" @if($ligne->active == 0) data-bs-toggle="tooltip" title="Livraison en cours"  @endif>
                            <td class="text-center">{{ $ligne->ligne->name }}</td>
                            <td class="text-center">{{ $ligne->ligne->distance }} Km</td>
                            <td class="text-center">{{ $ligne->tarifs()->where('date_tarif', now()->startOfDay())->first()->offre }} / {{ $ligne->tarifs()->where('date_tarif', now()->startOfDay())->first()->demande }}</td>
                            <td class="text-center">{{ eur($ligne->calcCA()) }}</td>
                            <td class="text-center">{{ eur($ligne->calcBenefice()) }}</td>
                            <td class="text-center">{!! $ligne->calcPerformance() !!} </td>
                            <td class="text-end">
                                @if($ligne->active)
                                    <a href="{{ route('ligne.show', $ligne->id) }}" class="">
                                        <img src="{{ asset('/storage/icons/ligne.png') }}" class="w-30px" alt="" data-bs-toggle="tooltip" title="Voir la ligne">
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <div class="d-flex flex-column w-100">
            @foreach(auth()->user()->hubs as $hub)
            <div class="d-flex flex-column p-5 mb-5 shadow-lg rounded-2 {{ $hub->active ? 'bg-light-warning' : 'bg-gray-500' }}" @if($hub->active == 0) data-bs-toggle="tooltip" title="Livraison en cours"  @endif>
                <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                    <div class="d-flex flex-row">
                        <div class="badge badge-warning">Hub</div>
                        <div class="fs-4 text-dark fw-bolder ms-5">{{ $hub->hub->gare->name }} / {{ Str::ucfirst(Str::lower($hub->hub->gare->region)) }} / {{ $hub->hub->gare->pays }}</div>
                    </div>
                    @if($hub->active)
                        <a href="{{ route('hub.show', $hub->id) }}" class="btn btn-sm btn-flex btn-warning">
                            <img src="{{ asset('/storage/icons/hub.png') }}" class="w-30px" alt="">
                            Détail du hub
                        </a>
                    @endif
                </div>
                <div class="d-flex flex-row align-items-center mt-10">
                    {!! $hub->calcRatioPerformance() !!}
                    <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                    <span>
                        <span class="">Chiffre d'affaire :</span>
                        <span class="fw-bolder">{{ eur($hub->calcCA()) }}</span>
                    </span>
                    <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                    <span>
                        <span class="">Bénéfice :</span>
                        <span class="fw-bolder">{{ eur($hub->calcBenefice()) }}</span>
                    </span>
                </div>
            </div>
            @endforeach
            @foreach(auth()->user()->lignes as $ligne)
                    <div class="d-flex flex-column p-5 mb-5 shadow-lg rounded-2 {{ $ligne->active ? 'bg-light-primary' : 'bg-gray-500' }}" @if($ligne->active == 0) data-bs-toggle="tooltip" title="Livraison en cours"  @endif>
                        <div class="d-flex flex-row justify-content-between align-items-center border-bottom border-gray-600 pb-5">
                            <div class="d-flex flex-row">
                                <div class="badge badge-primary">Ligne</div>
                                <div class="fs-4 text-dark fw-bolder ms-5">{{ $ligne->ligne->name }}</div>
                            </div>
                            @if($ligne->active)
                                <a href="{{ route('ligne.show', $ligne->id) }}" class="btn btn-sm btn-flex btn-primary">
                                    <img src="{{ asset('/storage/icons/ligne.png') }}" class="w-30px me-2" alt="">
                                    Détail de la ligne
                                </a>
                            @endif
                        </div>
                        <div class="d-flex flex-row align-items-center mt-10">
                            {!! $ligne->calcPerformance() !!}
                            <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                            <span>
                                <span class="">Distance :</span>
                                <span class="fw-bolder">{{ $ligne->ligne->distance }} Km</span>
                            </span>
                            <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                            <span>
                                <span class="">Offre Restante (J-1) :</span>
                                <span class="fw-bolder">{{ $ligne->calcOffreRestante(now()->subDays()->startOfDay(), now()->subDays()->endOfDay()) }}</span>
                            </span>
                            <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                            <span>
                                <span class="">Chiffre d'affaires :</span>
                                <span class="fw-bolder">{{ eur($ligne->calcCA()) }}</span>
                            </span>
                            <i class="fa-solid fa-circle text-dark fs-10 mx-5"></i>
                            <span>
                                <span class="">Bénéfices :</span>
                                <span class="fw-bolder">{{ eur($ligne->calcBenefice()) }}</span>
                            </span>
                        </div>
                    </div>
            @endforeach
        </div>
    @endif
@endsection

@section("script")
    <script src="{{ asset('/assets/js/map.js') }}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinysort/2.3.6/tinysort.js"></script>
    <script src="{{ asset('/assets/js/game/network/index.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.ajax({
            url: '/api/core/hub/liste',
            data: {"user_id": {{ auth()->user()->id }}},
            success: data => {
                data.hubs.forEach(hub => {
                    let map = initMap()
                    map.on('load', () => {
                        formatForHub(map, hub)
                        let upLigne = [];
                        hub.lignes.forEach(ligne => {
                            upLigne.push(ligne)
                            markerForStationEnd(map, ligne)
                        })
                        formatForLigne(map, upLigne)
                    })
                })
            }
        })
    </script>
@endsection
