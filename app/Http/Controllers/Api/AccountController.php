<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\Engine;
use App\Models\Core\Hub;
use App\Models\Core\Mouvement;
use App\Models\Core\Vourchers;
use App\Models\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function updatePassword(int $user_id, Request $request)
    {
        $request->validate([
            "password" => "required|string|min:8|confirmed"
        ]);

        if(\Hash::make($request->password) === User::find($user_id)->password) {
            return response()->json([
                "message" => "Le mot de passe est identique à l'ancien"
            ], 400);
        }

        User::find($user_id)->update([
            "password" => \Hash::make($request->password)
        ]);

        return response()->json([
            "message" => "Le mot de passe a été modifié avec succès"
        ], 200);
    }

    public function updateEmail(int $user_id, Request $request)
    {
        $request->validate([
            "email" => "required|email"
        ]);

        if($request->email === User::find($user_id)->email) {
            return response()->json([
                "message" => "L'email est identique à l'ancien"
            ], 400);
        }

        User::find($user_id)->update([
            "email" => $request->email
        ]);

        return response()->json([
            "message" => "L'email a été modifié avec succès"
        ], 200);
    }

    public function exchangeCode(int $user_id, Request $request)
    {
        $user = User::find($user_id);
        $voucher = $user->redeemCode($request->get('code'));

        if($voucher->data['action_type'] == 'gift') {
            $this->giftForUser($user, $voucher);
        }

        return response()->json([
            "message" => "Le code a été échangé avec succès"
        ], 200);
    }

    public function resetAccount(int $user_id, Request $request)
    {
        $user = User::find($user_id);

        $user->name_company = null;
        $user->name_secretary = null;
        $user->avatar_secretary = "default.png";
        $user->logo_company = "default.png";
        $user->desc_company = null;
        $user->argent = 0;
        $user->tpoint = 300;
        $user->research = 0;
        $user->save();



        $user->company()->delete();
        $company = User\UserCompany::automatedCreate($user);
        User\UserCompanySituation::automatedCreate($company);

        Mouvement::adding(
            'revenue',
            'Création de l\'entreprise - Reversion du capital',
            'divers',
            3000000,
            $company->id
        );

        $user->hubs()->delete();

        return response()->json([
            "message" => "Le compte a été réinitialisé avec succès"
        ], 200);

    }

    public function autoPlanning(Request $request, $user_id)
    {
        $user = User::find($user_id);

        $user->automated_planning = $request->get('automated_planning');
        $user->save();

        return response()->json();
    }

    private function giftForUser(array|User|\LaravelIdea\Helper\App\Models\_IH_User_C|null $user, mixed $voucher)
    {
        $modelGiftName = match ($voucher->model_type) {
            \App\Models\Core\Engine::class => "Engine",
            \App\Models\Core\Hub::class => "Hub",
            \App\Models\Core\Ligne::class => "Ligne",
            default => null
        };

        if($modelGiftName == 'Engine') {
            $user->engines()->create([
                "max_runtime_engine" => $voucher->model->calcMaxRuntimeEngine(),
                "user_id" => $user->id,
                "engine_id" => $voucher->model_id,
                "user_hub_id" => $user->hubs()->first()->id,
            ]);
        } else if($modelGiftName == 'Hub') {
            if($user->hubs()->where('hub_id', $voucher->model_id)->exists()) {
                return response()->json([
                    "message" => "Vous avez déjà ce hub"
                ], 400);
            } else {
                $user->hubs()->create([
                    "nb_salarie_iv" => User\UserHub::calcNbSalarieIV($voucher->model),
                    "nb_salarie_com" => 0,
                    "nb_salarie_ir" => User\UserHub::calcNbSalarieIR($voucher->model),
                    "nb_salarie_technicentre" => 0,
                    "date_achat" => now(),
                    "km_ligne" => 0,
                    "user_id" => $user->id,
                    "hub_id" => $voucher->model_id,
                ]);
            }
        }
    }

}
