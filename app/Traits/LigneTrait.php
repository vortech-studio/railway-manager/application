<?php

namespace App\Traits;

use App\Models\Core\Ligne;

trait LigneTrait
{
    public static function calculPrice(Ligne $ligne)
    {
        if($ligne->nb_station == 0) {
            return 1 * $ligne->distance * $ligne->time_min;
        } else {
            return $ligne->nb_station * $ligne->distance * $ligne->time_min;
        }
    }
}
