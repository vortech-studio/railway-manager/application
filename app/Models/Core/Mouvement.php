<?php

namespace App\Models\Core;

use App\Models\User\UserCompany;
use App\Models\User\UserHub;
use App\Models\User\UserLigne;
use App\Traits\MouvementTrait;
use Attribute;
use Illuminate\Database\Eloquent\Model;
use function Deployer\get;

class Mouvement extends Model
{
    use MouvementTrait;
    protected $guarded = [];
    protected $appends = ['created_at', 'type_mvm_icon_template'];

    public function userCompany()
    {
        return $this->belongsTo(UserCompany::class);
    }

    public function userHub()
    {
        return $this->belongsTo(UserHub::class);
    }

    public function userLigne()
    {
        return $this->belongsTo(UserLigne::class);
    }

    protected function createdAt() : \Illuminate\Database\Eloquent\Casts\Attribute
    {
        return \Illuminate\Database\Eloquent\Casts\Attribute::make(
            get: fn($value) => \Carbon\Carbon::parse($value)->format('d/m/Y'),
        );
    }

    public function amount() : \Illuminate\Database\Eloquent\Casts\Attribute
    {
        return \Illuminate\Database\Eloquent\Casts\Attribute::make(
            get: fn($value) => eur($value),
        );
    }

    public function getTypeMvmIconTemplateAttribute()
    {
        return "<div class='symbol symbol-30px'><img src='/storage/icons/mouvement/".$this->type_mvm.".png' alt='' /></div>";
    }
}
