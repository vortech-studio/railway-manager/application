<?php

namespace App\Models\Core\Blog;

use App\Models\User;
use App\Traits\BlogDataTrait;
use Illuminate\Database\Eloquent\Model;
use VanOns\Laraberg\Traits\RendersContent;

class Articles extends Model
{
    use BlogDataTrait, RendersContent;
    protected $guarded = [];
    protected string $contentColumn = 'content';

    protected $appends = ['published', 'type_article', 'type_article_icon'];

    public function author(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getPublishedAttribute()
    {
        if($this->attributes['published'] == 1) {
            return "<span class='badge badge-success'>Publier</span>";
        } else {
            return "<span class='badge badge-danger'>Brouillon</span>";
        }
    }

    public function getTypeArticleAttribute()
    {
        return match ($this->attributes['type_article']) {
            "event" => "Événement",
            "notice" => "Annonce",
            "news" => "A la une",
            default => "Inconnu",
        };
    }

    public function getTypeArticleIconAttribute()
    {
        return match ($this->attributes['type_article']) {
            "event" => "fa-calendar-alt",
            "notice" => "fa-bullhorn",
            "news" => "fa-newspaper",
            default => "fa-question-circle",
        };
    }
}
