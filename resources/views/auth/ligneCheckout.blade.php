<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic
Product Version: 8.1.8
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="{{ config('app.locale') }}">
<!--begin::Head-->
<head><base href="../../../"/>
    {{ seo()->render() }}
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    @vite(["resources/css/app.css"])
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="app-blank bgi-size-cover bgi-attachment-fixed bgi-position-center">
<!--begin::Theme mode setup on page load-->
<script>var defaultThemeMode = "light"; var themeMode; if ( document.documentElement ) { if ( document.documentElement.hasAttribute("data-bs-theme-mode")) { themeMode = document.documentElement.getAttribute("data-bs-theme-mode"); } else { if ( localStorage.getItem("data-bs-theme") !== null ) { themeMode = localStorage.getItem("data-bs-theme"); } else { themeMode = defaultThemeMode; } } if (themeMode === "system") { themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light"; } document.documentElement.setAttribute("data-bs-theme", themeMode); }</script>
<!--end::Theme mode setup on page load-->
<!--begin::Root-->
<div class="d-flex flex-column flex-root" id="kt_app_root">
    <!--begin::Page bg image-->
    <style>body { background-image: url('/storage/other/wall-rm.png'); } [data-bs-theme="dark"] body { background-image: url('/storage/other/wall-rm.png'); }</style>
    <!--end::Page bg image-->
    <!--begin::Authentication - Sign-in -->
    <div class="d-flex flex-column flex-lg-row flex-column-fluid">
        <!--begin::Aside-->
        <div class="d-flex flex-lg-row-fluid w-50">
            <!--begin::Content-->
            <div class="d-flex flex-column flex-center pb-0 pb-lg-10 p-10 w-100 bg-white bg-opacity-75">
                <div class="d-flex flex-row p-5 mb-10 shadow-lg">
                    <div class="symbol symbol-55px symbol-circle me-5">
                        <img src="/assets/media/avatars/300-1.jpg" alt="">
                    </div>
                    <div class="d-flex flex-column">
                        <p>Bravo vous avez acheter votre premier HUB: <strong>{{ $hub->gare->name }}</strong>.</p>
                        <p>A présent, il vous faut une ligne afin d'acquérir votre principal revenue, <strong>le transport de voyageur.</strong></p>
                        <p>Veuillez à présent choisir votre première ligne, ne vous en faite pas, elle est également offerte.</p>
                    </div>
                </div>
            </div>
            <!--end::Content-->
        </div>
        <!--begin::Aside-->
        <!--begin::Body-->
        <div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12 bg-white bg-opacity-50 w-75">
            <!--begin::Wrapper-->
            <div class="bg-body d-flex flex-column flex-center rounded-4 w-md-1000px p-10">
                <!--begin::Content-->
                <div class="d-flex flex-center flex-column align-items-stretch h-lg-100 w-md-800px">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-center flex-column flex-column-fluid pb-15 pb-lg-20">
                        <!--begin::Form-->
                        <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="{{ route('oauth.configStore', ["step" => "3"]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-10">
                                <label for="ligne_id" class="form-label required">Ligne Disponible</label>
                                <select id="ligne_id" class="form-control selectpicker" name="ligne_id" data-live-search="true">
                                    <option value="">-- Choisir une ligne --</option>
                                    @foreach($lignes as $ligne)
                                    <option value="{{ $ligne->id }}" data-content="<i class='fa-solid fa-train me-3'></i> {{ $ligne->stationStart->name }} <-> {{ $ligne->stationEnd->name }} <i class='fa-solid fa-train ms-3'></i>"></option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row mb-10" data-content="loadInfoLigne">
                                <div class="col-8">
                                    <!-- Carte -->
                                    <div id="map" style="height: 450px;"></div>
                                </div>
                                <div class="col-4">
                                    <div class="d-flex flex-column">
                                        <div class="d-flex flex-row">
                                            <div class="fs-2 fw-bolder">Information sur la ligne</div>
                                        </div>
                                        <div class="separator border-2 border-gray-800 py-3"></div>
                                        <div class="d-flex flex-column p-5">
                                            <div class="fw-bold">Intitulé de la ligne</div>
                                            <div class="fs-8" data-ajax-load="intitule">-</div>
                                        </div>
                                        <div class="separator border-1 border-gray-400 py-3"></div>
                                        <div class="d-flex flex-row justify-content-between align-items-center p-5">
                                            <div class="fw-bold">Prix <div class="fs-9 fs-italic">(Hors Subvention)</div></div>
                                            <div data-ajax-load="priceNoSub">-</div>
                                        </div>
                                        <div class="separator border-1 border-gray-400 py-3"></div>
                                        <div class="d-flex flex-row justify-content-between mt-3">
                                            <div class="card card-flush bg-info me-3">
                                                <div class="card-body">
                                                    <div class="d-flex flex-column">
                                                        <div class="fw-bolder fs-2" data-ajax-load="distance">-</div>
                                                        <div class="fw-bold fs-6 text-gray-400">Km de trajet</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card card-flush bg-warning text-inverse me-3">
                                                <div class="card-body">
                                                    <div class="d-flex flex-column">
                                                        <div class="fw-bolder fs-2" data-ajax-load="timeTravel">--:--</div>
                                                        <div class="fw-bold fs-6">Temps de trajet</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="separator border-1 border-gray-400 py-3"></div>
                                        <div class="card card-flush bg-gray-300">
                                            <div class="card-header">
                                                <h3 class="card-title">Arret deservie</h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="d-flex flex-column" data-ajax-load="ligneStations">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-row justify-content-between align-items-center mt-5">
                                            <div class="fs-3">Subvention de l'état</div>
                                            <div class="fs-2 fw-bolder">100 %</div>
                                        </div>
                                        <div class="d-flex flex-row justify-content-between align-items-center">
                                            <div class="fs-3">Total à Payer</div>
                                            <div class="fs-1 fw-bolder text-success border border-3 border-success p-1">0,00 €</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-end">
                                <x-form.button />
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Wrapper-->

                </div>
                <!--end::Content-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Authentication - Sign-in-->
</div>
<!--end::Root-->
<!--begin::Javascript-->
<script>var hostUrl = "assets/";</script>
<!--begin::Global Javascript Bundle(mandatory for all pages)-->
<script src="assets/plugins/global/plugins.bundle.js"></script>
<script src="assets/js/scripts.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta3/dist/js/bootstrap-select.min.js"></script>
<script src="https://unpkg.com/react@17.0.2/umd/react.production.min.js"></script>
<script src="https://unpkg.com/react-dom@17.0.2/umd/react-dom.production.min.js"></script>
@vite(["resources/js/app.js"])
<!--end::Global Javascript Bundle-->
<script type="text/javascript">
    document.querySelector('#ligne_id').addEventListener('change', e => {
        console.log(e.target.value)
        $.ajax({
            url: '/api/core/ligne/'+e.target.value,
            success: data => {
                console.log(data.hub.gare)
                document.querySelector('[data-ajax-load="intitule"]').innerHTML = `<i class="fa-solid fa-train me-3"></i> ${data.ligne.station_start.name} <-> ${data.ligne.station_end.name} <i class="fa-solid fa-train me-3"></i>`
                document.querySelector('[data-ajax-load="priceNoSub"]').innerHTML = new Intl.NumberFormat('fr-FR', {style: 'currency', currency: 'EUR'}).format(data.ligne.price)
                document.querySelector('[data-ajax-load="distance"]').innerHTML = data.ligne.distance
                document.querySelector('[data-ajax-load="timeTravel"]').innerHTML = data.ligne.time_format

                document.querySelector('[data-ajax-load="ligneStations"]').innerHTML = ''

                let followMarker = true;
                let coords = {};
                let map = null;

                let container = L.DomUtil.get('map');
                if(container != null) {
                    container._leaflet_id = null;
                }

                function initMap() {
                    map = L.map('map', {
                        center: [data.hub.gare.latitude, data.hub.gare.longitude],
                        zoom: 8
                    });

                    let osm = L.tileLayer('https://{s}.tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=e100e37f739943e08885b0472fffca1a', {
                        attribution: '&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                        apikey: 'e100e37f739943e08885b0472fffca1a',
                        maxZoom: 22
                    }).addTo(map)

                    L.polyline([data.stations_cos], {color: 'cyan'}).addTo(map)
                }

                initMap()

                Array.from(data.stations).forEach(station => {
                    if(data.ligne.station_start.name === station.gare.name) {
                        document.querySelector('[data-ajax-load="ligneStations"]').innerHTML += `
                    <div class="d-flex align-items-center border border-1 border-primary p-2">
                        <i class="fa-solid fa-train text-primary fs-4 me-3"></i> ${station.gare.name}
                    </div>
                    `
                    } else if(data.ligne.station_end.name === station.gare.name) {
                        document.querySelector('[data-ajax-load="ligneStations"]').innerHTML += `
                    <div class="d-flex align-items-center border border-1 border-danger p-2">
                        <i class="fa-solid fa-train text-danger fs-4 me-3"></i> ${station.gare.name}
                    </div>
                    `
                    } else {
                        document.querySelector('[data-ajax-load="ligneStations"]').innerHTML += `
                    <div class="d-flex align-items-center py-2">
                        <i class="fa-solid fa-train fs-4 me-3"></i> ${station.gare.name}
                    </div>
                    `
                    }

                    let marker = addMarker({
                        lat: station.gare.latitude,
                        lng: station.gare.longitude,
                        title: station.gare.name,
                        draggable: true
                    }, map);
                    marker.bindPopup('')
                    let popup = marker.getPopup()
                    popup.setContent(`
                                <div class="d-flex flex-column p-3 border border-1 border-primary rounded-1 w-430px">
                                    <div class="symbol symbol-75px symbol-2by3">
                                        <img src="/storage/gares/${station.gare.id}.jpg" alt="">
                                    </div>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td class="fw-bold">Gare</td>
                                            <td class="">${station.gare.name}</td>
                                        </tr>
                                    </table>
                                </div>

                                `)

                    marker.openPopup()

                })


            }
        })
    })

    function addMarker(options, map) {
        const icon = L.icon({
            iconUrl: '/storage/icons/train_map.png',
            iconSize: [32,37],
            iconAnchor:   [16, 18], // point of the icon which will correspond to marker's location
            popupAnchor:  [1, -9]
        })
        const marker = L.marker([options.lat, options.lng], { title: options.title, draggable: options.draggable, icon: icon });
        marker.addTo(map);

        marker.on('dragend', function(event) {
            coords = event.target._latlng;
            showNewCoords(event.target._latlng, event.target);
            if (followMarker) {
                map.setView(event.target._latlng);
            }
        });

        return marker;
    }

    function showNewCoords(coords, marker) {
        marker
            .bindPopup(`lat: ${coords.lat} - lng: ${coords.lng}`)
            .openPopup();
    }
</script>
<!--end::Javascript-->
</body>
<!--end::Body-->
</html>
