<?php

namespace App\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Slider extends Component
{
    public $id;
    public $label;
    /**
     * @var false
     */
    public bool $required;

    /**
     * Create a new component instance.
     */
    public function __construct($id, $label, $required = false)
    {
        //
        $this->id = $id;
        $this->label = $label;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.form.slider');
    }
}
