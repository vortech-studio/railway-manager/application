<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalEngineTable extends Migration
{
    public function up(): void
    {
        Schema::create('engine_rental', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('rental_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('engine_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('rental_engine');
    }
}
