<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Core\Vouchers;
use BeyondCode\Vouchers\Models\Voucher;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VourcherController extends Controller
{
    public function index()
    {
        return view('admin.vourcher.index', [
            "vourchers" => Vouchers::all()
        ]);
    }

    public function create()
    {
        return view('admin.vourcher.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $model_type = match ($request->get('model_type')) {
            "Engine" => \App\Models\Core\Engine::class,
            "Hub" => \App\Models\Core\Hub::class,
            "Ligne" => \App\Models\Core\Ligne::class,
            //"Shop" => \App\Models\Core\Shop::class,
            default => null
        };

        $model = $model_type::find($request->get('model_id'));
        $model->createVouchers(
            $request->get('nb_vourcher'),
            [
                "designation" => $request->get('designation'),
                "action_type" => $request->get('action_type'),
                "action_value" => $request->get('action_value'),
            ],
            $request->has('date_expiration') ? Carbon::parse($request->get('date_expiration')) : null,
        );

        return redirect()->route('admin.vourcher.index')
            ->with('success', 'Vourcher created successfully.');
    }
}
