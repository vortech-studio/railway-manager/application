<?php

namespace Database\Seeders;

use App\Jobs\Console\InstallGare;
use App\Models\Core\Achievement;
use App\Models\Core\Engine;
use App\Models\Core\Gares;
use App\Models\Core\Hub;
use App\Models\Core\Rental;
use App\Models\Core\Setting;
use App\Models\Core\TypeIncident;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use LevelUp\Experience\Models\Level;

class UpdateSeeder extends Seeder
{
    public function run(): void
    {
        $rent_1 = Rental::create([
            "name" => "LocaRail+",
            "contract_duration" => 8,
            "type" => json_encode([["ter","tgv","ic","other"]]),
        ]);

        $rent_2 = Rental::create([
            "name" => "RailRent",
            "contract_duration" => 3,
            "type" => json_encode([["ter","tgv"]]),
        ]);

        $all_engines = Engine::all();
        $spec_engines = Engine::where('type_train', 'tgv')
            ->orWhere('type_train', 'ter')
            ->get();

        foreach ($all_engines as $all_engine) {
            $rent_1->engines()->attach($all_engine->id);
        }

        foreach ($spec_engines as $spec_engine) {
            $rent_2->engines()->attach($spec_engine->id);
        }

    }

}
