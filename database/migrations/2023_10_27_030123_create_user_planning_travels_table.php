<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlanningTravelsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_planning_travels', function (Blueprint $table) {
            $table->id();
            $table->decimal('ca_billetterie');
            $table->decimal('ca_other');
            $table->string('frais_electrique');
            $table->string('frais_gasoil');
            $table->string('frais_autre');

            $table->foreignId('user_planning_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_planning_travels');
    }
}
