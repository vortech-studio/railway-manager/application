<div class="d-flex flex-row align-items-center">
    <a href="{{ route('planning.editing') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Planifier les trajets">
        <img src="{{ asset('/storage/icons/journey.png') }}" class="w-50px" alt="">
    </a>
    <a href="{{ route('planning.index') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Planning Général">
        <img src="{{ asset('/storage/icons/planning.png') }}" class="w-50px" alt="">
    </a>
    <a href="{{ route('hub.checkout') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Achat d'un Hub">
        <img src="{{ asset('/storage/icons/hub_checkout.png') }}" class="w-50px" alt="">
    </a>
    <a href="{{ route('ligne.checkout') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Achat d'une ligne">
        <img src="{{ asset('/storage/icons/ligne_checkout.png') }}" class="w-50px" alt="">
    </a>
    <a href="{{ route('network') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Mon Réseau">
        <img src="{{ asset('/storage/icons/network.png') }}" class="w-50px" alt="">
    </a>
    @premium
    <a href="{{ route('vigimat.dashboard') }}" class="btn btn-info btn-circle me-5">VIGIMAT</a>
    @endpremium
</div>
