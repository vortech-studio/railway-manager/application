<?php

namespace App\Jobs\Game;

use App\Models\User\UserDelivery;
use App\Models\User\UserEngine;
use App\Notifications\Game\EngineDeliveryNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class EngineDeliveryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserDelivery $delivery, public UserEngine $engine)
    {
    }

    public function handle(): void
    {
        $this->engine->active = true;
        $this->engine->available = true;
        $this->engine->save();

        $this->engine->user->notify(new EngineDeliveryNotification($this->engine));

        $message = $this->engine->user->messages()->create([
            "from" => "Technicentre de la gare de ".$this->engine->user_hub->hub->gare->name,
            "subject" => "Livraison du matériel roulant ".$this->engine->engine->name,
            "message" => view('game.includes.mailbox.new_engine_planning', [
                "hub_name" => $this->engine->user_hub->hub->gare->name,
                "engine_name" => $this->engine->engine->name,
            ]),
            "important" => true,
            "user_id" => $this->engine->user_id,
        ]);

        $message->actions()->create([
            "action_type" => "other",
            "action_link" => route('planning.editing'),
            "action_text" => "Planifier les trajets",
        ]);

        $message->actions()->create([
            "action_type" => "other",
            "action_link" => route('chat.hub.technicentre.responsable'),
            "action_text" => "Contacter le responsable",
        ]);

        $this->delivery->delete();
    }
}
