<div id="loadContent"></div>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
        let loadContent = document.getElementById('loadContent');
        update()
        setInterval(() => {
            update()
        }, 20000)



        function update() {
            $.ajax({
                url: '/api/game/ecran/travel/'+ {{ $planning->id }},
                success: data => {
                    loadContent.innerHTML = data.content;
                    document.querySelectorAll('.speedometer').forEach(element => {
                        element.innerText = speedometer(100, 200, 270, 270)
                    })

                    let map = initMap([
                        {{ $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->longitude }},
                        {{ $planning->planning_stations()->where('status', 'done')->orderBy('departure_at', 'desc')->first()->ligneStation->gare->latitude }},
                    ], 11)
                    map.on('load', () => {
                        formatForLigne(map, data.data.ligne)
                        map.flyTo(data.data.ligne.coordinates)
                        markerForStation(map, data.data.station_start)
                        markerForStation(map, data.data.station_end)
                        markerForTrain(map, data.data.station_start)
                    })

                    @if($planning->status == 'travel' || $planning->status == 'in_station')
                        @if($planning->date_arrived <= now()->startOfMinute())
                        window.location.reload()
                        @endif
                    @endif
                }
            })

        }
        function speedometer(next_time = 0, min, max, engine_speed) {
            if(next_time >= 0 && next_time <= 20) {
                min = Math.ceil(min)
                max = Math.floor(engine_speed / 100)
                return Math.floor(Math.random() * (max - min + 1)) + min
            } else if(next_time > 20 && next_time <= 60) {
                min = Math.ceil(engine_speed / 100)
                max = Math.floor(engine_speed / 50)
                return Math.floor(Math.random() * (max - min + 1)) + min
            } else {
                min = Math.ceil(engine_speed / 50)
                max = Math.floor(engine_speed)
                return Math.floor(Math.random() * (max - min + 1)) + min
            }
        }
    })
</script>
