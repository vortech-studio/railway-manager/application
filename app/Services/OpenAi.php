<?php

namespace App\Services;
use OpenAI as OpenAiService;
use OpenAI\Exceptions\ErrorException;

class OpenAi
{
    public function handle(string $prompt)
    {
        $api_key = config('services.openai.api_key');
        $client = OpenAiService::client($api_key);

        $title = "Cher PDG,";

        $response = $this->callApi($client, [
            "model" => "gpt-3.5-turbo",
            "messages" => [
                [
                    "role" => "assistant",
                    "content" => $prompt
                ]
            ]
        ]);

        return $response;
    }

    public function dalle(int $nb, string $prompt, string $format = '1024x768')
    {
        $api_key = config('services.openai.api_key');
        $client = OpenAiService::client($api_key);

        $response = $client->images()->create([
            "model" => "dall-e-3",
            "n" => $nb,
            "prompt" => $prompt,
            "size" => $format,
            'response_format' => 'url'
        ]);

        return $response;
    }

    private function callApi($client, $params, $maxRetries = 5)
    {
        $success = false;
        $response = null;
        $retries = 0;

        do {
            try {
                $response = $client->chat()->create($params);
                $success = true;
            }catch (ErrorException $exception) {
                $retries++;
                if ($retries > $maxRetries) {
                    \Log::critical("API call failed after maximum retries.");
                    throw $exception;
                }
                \Log::warning("API call failed. Retrying in 5 second...");
                sleep(5);
            }
        } while (!$success);

        return $response;
    }
}
