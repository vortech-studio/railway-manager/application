@if($engine->user->automated_planning)
    <div class="card">
        <div class="card-header">
            <div class="card-title">Planning Automatique </div>
            <div class="card-toolbar">
                <span class="dateToday fs-3 fw-bolder me-3" data-date="{{ now() }}">{{ now()->format("d/m/Y") }}</span>
            </div>
        </div>
        <div class="card-body">
            <table id="planningStack" class="table table-row-bordered align-middle gs-5 gx-5 gy-5">
                <thead>
                <tr class="bg-color-eva text-white fs-3 text-center">
                    <th class="min-w-50px">Mission</th>
                    <th class="min-w-100px">Départ</th>
                    <th class="min-w-100px">Arrivée</th>
                    <th>EM</th>
                </tr>
                </thead>
                <tbody>
                @foreach($engine->plannings as $planning)
                    <tr>
                        <td class="w-50px text-center">
                            <div class="badge badge-primary">{{ $planning->number_travel }}</div>
                        </td>
                        <td class="w-100px">
                            <div class="d-flex flex-column">
                                <span class="fw-bolder">{{ $planning->ligne->ligne->stationStart->name }}</span>
                                <span>{{ $planning->date_depart->format("d/m/Y à H:i") }}</span>
                            </div>
                        </td>
                        <td class="w-100px">
                            <div class="d-flex flex-column">
                                <span class="fw-bolder">{{ $planning->ligne->ligne->stationEnd->name }}</span>
                                <span>{{ $planning->date_arrived->format("d/m/Y à H:i") }}</span>
                            </div>
                        </td>
                        <td class="scroll-x min-w-300px">
                            @if($planning->engine->engine->type_engine == 'automotrice')
                                @for($i=0; $i < $planning->engine->engine->technical->nb_wagon; $i++)
                                    <img src="/storage/engines/{{ $planning->engine->engine->type_engine }}/{{ \Illuminate\Support\Str::upper(Str::slug($planning->engine->engine->slug)) }}-{{ $i }}.gif" class="w-50px mx-auto" alt="">
                                @endfor
                            @else
                                <img src="/storage/engines/{{ $planning->engine->engine->type_engine }}/{{ $planning->engine->engine->image }}" class="w-135px my-5 mx-20" alt="">
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@else
    <div class="card">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Planification des trajets</div>
                <div class="card-toolbar">
                    <button class="btn btn-primary btn-flush h-100 rounded p-5">Nouvelle mission</button>
                </div>
            </div>
            <div class="card-body" id="manualCalendar">

            </div>
        </div>
    </div>
@endif
