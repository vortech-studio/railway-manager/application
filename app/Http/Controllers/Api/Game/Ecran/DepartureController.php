<?php

namespace App\Http\Controllers\Api\Game\Ecran;

use App\Http\Controllers\Controller;
use App\Models\User\UserHub;
use App\Models\User\UserPlanning;
use Illuminate\Http\Request;

class DepartureController extends Controller
{
    public function list(Request $request)
    {
        $plannings = [];
        $datas = $this->setData($request, $request->has('type'));

        $content_mobile = null;
        $content_desktop = null;

        foreach ($datas as $data) {
            ob_start();
            ?>
            <div class="rounded shadow-lg p-5 mb-2">
                <div class="d-flex flex-column">
                    <div class="d-flex flex-row justify-content-between">
                        <div class="d-flex flex-column w-75">
                            <div class="d-flex flex-row">
                                <?php if($data->status == 'retarded'): ?>
                                    <div class="fs-1 fw-bolder text-primary me-3"><?= $data->date_depart->subMinutes($data->retarded_time)->format("H:i") ?></div>
                                    <div class="badge badge-warning">+ <?= $data->retarded_time; ?> min</div>
                                <?php else: ?>
                                    <div class="fs-1 fw-bolder text-primary me-3"><?= $data->date_depart->format("H:i") ?></div>
                                <?php endif; ?>
                            </div>
                            <div class="fs-3 fw-bolder"><?= $data->ligne->ligne->stationEnd->name ?></div>
                            <div class="fs-6 text-<?= $data->status_format_color_text; ?>"><?= $data->status_format_text; ?></div>
                        </div>
                        <?php if($data->date_depart->diffInMinutes(now()) <= 20): ?>
                        <div class="bg-color-eva text-white d-flex flex-column align-items-center justify-content-center rounded-2 w-75px h-75px py-auto">
                            <div class="fs-6">Voie</div>
                            <div class="fs-2 fw-bold"><?= $data->ligne->quai ?></div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="separator border-3 border-gray-400 my-4"></div>
                    <div class="fs-uppercase fs-3"><?= \Str::upper($data->engine->engine->type_train_format) ?> | <?= $data->number_travel ?></div>
                </div>
            </div>
            <?php
            $content_mobile .= ob_get_clean();
            ob_start();
            ?>
            <div class="d-flex align-items-center bg-white rounded-1 shadow-lg h-100px p-2 mb-5">
                <div class="d-flex flex-row align-items-center me-5">
                    <span class="bullet bullet-vertical h-60px w-10px bg-<?= $data->status_format_color_text ?> me-3"></span>
                    <span class="fs-2 fw-semibold text-<?= $data->status_format_color_text ?>"><?= $data->status_format_text ?></span>
                </div>
                <div class="d-flex flex-column mx-10">
                    <?php if($data->status == 'retarded'): ?>
                        <div class="fs-1 fs-strike strike-danger text-orange-300"><?= $data->date_depart->subMinutes($data->retarded_time)->format("H:i") ?></div>
                        <div class="fs-2x fw-bold text-orange-300"><?= $data->date_depart->format("H:i") ?></div>
                    <?php else: ?>
                        <div class="fs-2x text-orange-300 fw-bold"><?= $data->date_depart->format("H:i") ?></div>
                    <?php endif; ?>
                </div>
                <div class="w-100px">
                    <?php if($data->status == 'retarded'): ?>
                    <span class="badge badge-warning">+ <?= $data->retarded_time; ?> min</span>
                    <?php endif; ?>
                </div>
                <div class="flex-grow-1 mx-10">
                    <div class="d-flex flex-column">
                        <?php if($data->status == 'canceled'): ?>
                        <div class="d-flex flex-row align-items-center">
                            <div class="fs-1 fw-bolder fs-strike strike-danger text-color-eva me-3"><?= $data->ligne->ligne->stationEnd->name; ?></div>
                            <i class="fa-solid fa-exclamation-triangle text-danger fs-1" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-custom-class="tooltip-inverse" title="<?= $data->incidents()->first()->designation; ?>"></i>
                        </div>
                        <?php elseif($data->status == 'retarded'): ?>
                            <div class="d-flex flex-row align-items-center">
                                <div class="fs-1 fw-bolder text-color-eva me-3"><?= $data->ligne->ligne->stationEnd->name; ?></div>
                                <i class="fa-solid fa-info-circle text-warning fs-1" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-custom-class="tooltip-inverse" title="<?= $data->incidents()->first()->designation; ?>"></i>
                            </div>
                        <?php else: ?>
                        <div class="fs-1 fw-bolder text-color-eva"><?= $data->ligne->ligne->stationEnd->name; ?></div>
                        <?php endif; ?>
                        <div class="fs-4 text-uppercase"><?= \Str::upper($data->engine->engine->type_train_format) ?> | <?= $data->number_travel; ?></div>
                    </div>
                </div>
                <?php if($data->status == 'canceled'): ?>
                <div class="d-flex flex-column justify-content-center align-items-center bg-color-eva text-white rounded-3 w-80px h-80px animation-blink animation-blink-1">
                    <img src="<?= asset('/storage/icons/train_cancelled.png') ?>" class="w-50px" alt="">
                </div>
                <?php else: ?>
                    <?php if($data->date_depart->diffInMinutes(now()) <= 20): ?>
                    <div class="bg-color-eva text-white d-flex flex-column align-items-center justify-content-center rounded-2 w-75px h-75px py-auto">
                        <div class="fs-6">Voie</div>
                        <div class="fs-2 fw-bold"><?= $data->ligne->quai ?></div>
                    </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <?php
            $content_desktop .= ob_get_clean();
            $plannings[] = [
                'status' => [
                    'text' => $data->status_format_text,
                    'color' => $data->status_format_color_text
                ],
                'start' => [
                    'time' => $data->date_depart->format('H:i'),
                    'retarded' => $data->status == 'retarded',
                    'canceled' => $data->status == 'canceled',
                    'time_retarded' => $data->status == 'retarded' ? $data->date_depart->addMinutes($data->retarded_time)->format("H:i") : null,
                    'retarded_time' => $data->retarded_time,
                    'time_before_start' => $data->date_depart->diffInMinutes(now())
                ],
                'station' => [
                    'name' => $data->ligne->ligne->stationEnd->name,
                    "voie" => $data->ligne->quai,
                ],
                'engine' => [
                    "type" => \Str::upper($data->engine->engine->type_train_format),
                    "number" => $data->number_travel
                ]
            ];
        }



        return response()->json([
            "plannings" => collect($plannings),
            "content" => [
                "mobile" => $content_mobile,
                "desktop" => $content_desktop
            ]
        ]);
    }

    public function travel(Request $request, $planning_id)
    {
        $planning = UserPlanning::find($planning_id);
        $content_mobile = null;
        $content_desktop = null;
        ob_start();
        ?>
        <div class="d-flex flex-column p-2">
            <div class="d-flex flex-row justify-content-between align-items-center p-5">
                <div class="d-flex flex-row">
                    <img src="<?= asset('/storage/logos/logo_'.$planning->engine->engine->type_train.'.svg') ?>" class="w-60px me-2" alt="">
                    <div class="fs-1 fw-bolder text-color-eva"><?= \Str::upper($planning->engine->engine->type_train_format) ?> <?= $planning->number_travel ?></div>
                </div>
                <span class="badge badge-square bg-<?= $planning->status_format_color_text ?>"><?= $planning->status_format_text ?></span>
            </div>
            <?php if($planning->engine->engine->type_engine == 'automotrice'): ?>
                <div class="d-flex align-items-end bg-gray-400 p-5 scroll scroll-x w-100">
                    <?php for($i=0; $i <= $planning->engine->engine->technical->nb_wagon; $i++): ?>
                    <img src="<?= asset('/storage/engines/automotrice/'.$planning->engine->engine->slug.'-'.$i.'.gif') ?>" class="" alt="" />
                    <?php endfor; ?>
                </div>
            <?php else: ?>
                <div class="d-flex align-items-end bg-gray-400 p-5 scroll scroll-x w-100">
                    <img src="<?= asset('/storage/engines/' . $planning->engine->engine->type_engine . '/'. $planning->engine->engine->image) ?>" class="w-135px my-5 mx-20" alt="">
                </div>
            <?php endif; ?>
            <div class="d-flex flex-column align-items-start p-2">
                <small class="mb-1">Train "<?= $planning->engine->engine->name ?>"
                    <?php if($planning->engine->engine->type_engine == 'automotrice'): ?>
                        - <?= $planning->engine->engine->technical->nb_wagon ?> Voitures - <?= $planning->engine->engine->nb_passager ?> Places
                    <?php endif; ?>
                </small>
                <div class="fs-3">Destination <?= $planning->ligne->ligne->stationEnd->name ?></div>
            </div>
            <?php if($planning->incidents()->count() != 0): ?>
            <a href="#modalIncidents" data-bs-toggle="modal" id="modalIncident" class="btn btn-link d-flex align-items-center p-3 bg-gray-300 rounded-3">
                <div class="symbol symbol-15px symbol-circle me-3">
                    <i class="fa-solid fa-info-circle fs-2x text-danger"></i>
                    <span class="symbol-badge badge badge-sm badge-circle bg-dark start-100 text-white"><?= $planning->incidents()->count() ?></span>
                </div>
                <span>Perturbation en cours sur le trajet...</span>
                <div class="flex-shrink">
                    <i class="fa-solid fa-angle-right text-muted"></i>
                </div>
            </a>
            <?php endif; ?>
            <?php foreach ($planning->planning_stations as $station): ?>
                <?php if($station->ligne_station_id == $planning->ligne->ligne->stationStart->id || $station->ligne_station_id == $planning->ligne->ligne->stationEnd->id): ?>
                    <div class="d-flex align-items-center p-5">
                        <?php if($planning->retarded_time != null): ?>
                            <span class="badge badge-lg badge-primary me-5 fs-strike"><?= $station->departure_at->subMinutes($planning->retarded_time)->format("H:i") ?></span>
                            <span class="badge badge-sm badge-warning me-5">+ <?= $planning->retarded_time ?></span>
                        <?php else: ?>
                            <span class="badge badge-lg badge-primary me-5"><?= $station->departure_at->format("H:i") ?></span>
                        <?php endif; ?>
                        <div class="w-25 text-center">
                            <span class="bullet bullet-dot w-20px h-20px bg-white border border-3 border-primary me-5"></span>
                        </div>
                        <div class="fw-bolder fs-2"><?= $station->name ?></div>
                    </div>
                <?php else: ?>
                    <div class="d-flex align-items-center p-5">
                        <?php if($planning->retarded_time != null): ?>
                            <span class="badge badge-lg badge-primary me-5 fs-strike"><?= $station->departure_at->subMinutes($planning->retarded_time)->format("H:i") ?></span>
                            <span class="badge badge-sm badge-warning me-5">+ <?= $planning->retarded_time ?></span>
                        <?php else: ?>
                            <span class="badge badge-lg badge-primary me-5"><?= $station->departure_at->format("H:i") ?></span>
                        <?php endif; ?>
                        <div class="w-25 text-center">
                            <span class="bullet bullet-dot w-15px h-15px bg-white border border-3 border-primary me-5"></span>
                        </div>
                        <div class="d-flex flex-column">
                            <div class="fw-semibold fs-3"><?= $station->name ?></div>
                            <small class="text-gray-500"><?= $station->arrival_at->diffInMinutes($station->departure_at) ?> min d'arret</small>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <div class="modal bg-body fade" tabindex="-1" id="modalIncidents">
            <div class="modal-dialog modal-fullscreen">
                <div class="modal-content shadow-none">
                    <div class="modal-header">
                        <h5 class="modal-title">Informations du train</h5>

                        <!--begin::Close-->
                        <div class="btn btn-icon btn-sm btn-active-light-danger ms-2" data-bs-dismiss="modal" aria-label="Close">
                            <i class="ki-duotone ki-cross fs-2x"><span class="path1"></span><span class="path2"></span></i>
                        </div>
                        <!--end::Close-->
                    </div>

                    <div class="modal-body">
                        <div class="mb-5">
                            <i class="fa-solid fa-exclamation-triangle fs-1 text-danger me-2"></i> Perturbation en cours
                        </div>
                        <div class="d-flex flex-column">
                            <?php foreach ($planning->incidents as $incident): ?>
                            <div class="d-flex flex-row align-items-center mb-2">
                                <span class="bullet bullet-vertical w-10px h-auto bg-<?= $incident->niveau_color ?> me-5"></span>
                                <span class="fs-3"> <?= $incident->designation; ?></span>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $content_mobile = ob_get_clean();
        ob_start();
        ?>
        <div class="d-flex flex-row align-items-center mb-2 p-5 h-400px overflow-hidden">
            <div class="bg-white rounded-3 w-35 ">
                <div class="bg-gray-400 p-5 rounded-3 rounded-bottom-0">
                    <div class="d-flex flex-wrap align-items-center">
                        <div class="flex-grow-1">
                            <?php if($planning->status == 'retarded'): ?>
                            <div class="d-flex flex-column">
                                <span class="fs-1 fw-bolder text-color-eva fs-strike strike-danger"><?= $planning->date_depart->subMinutes($planning->retarded_time)->format("H:i") ?></span>
                                <span class="fs-2x fw-bolder text-color-eva"><?= $planning->date_depart->format("H:i") ?></span>
                            </div>
                            <?php elseif($planning->status == 'canceled'): ?>
                                <span class="fs-3x fw-bolder text-color-eva fs-strike strike-danger"><?= $planning->date_depart->format("H:i") ?></span>
                            <?php else: ?>
                                <span class="fs-3x fw-bolder text-color-eva"><?= $planning->date_depart->format("H:i") ?></span>
                            <?php endif; ?>

                        </div>
                        <div class="rounded-5 border border-3 bg-gray-600 border-<?= $planning->status_format_color_text ?> text-<?= $planning->status_format_color_text ?>  px-2 py-3 ">
                            <?= $planning->status_format ?>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-column p-7">
                    <div class="fs-4x text-color-eva fw-bolder mb-2"><?= $planning->ligne->ligne->stationEnd->name; ?></div>
                    <div class="bg-cyan-600 p-2 w-50">
                        <span class="text-color-eva fs-2"><?= \Str::upper($planning->engine->engine->type_train_format); ?> <strong><?= number_format($planning->number_travel, 0, '', ' ') ?></strong></span>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column ms-10 animation-dynamique-y">
                <?php foreach ($planning->planning_stations as $station): ?>
                    <?php if($station->ligne_station_id == $planning->ligne->ligne->stationStart->id || $station->ligne_station_id == $planning->ligne->ligne->stationEnd->id): ?>
                    <div class="d-flex align-items-center p-5">
                        <div class="w-25 text-center">
                            <span class="bullet bullet-dot w-45px h-45px bg-white border border-5 border-color-eva-arrival me-20"></span>
                        </div>
                        <div class="fw-bolder text-white fs-2x"><?= $station->name ?></div>
                    </div>
                    <?php else: ?>
                    <div class="d-flex align-items-center p-5">
                        <div class="w-25 text-center">
                            <span class="bullet bullet-dot w-30px h-30px bg-white border border-3 border-color-eva-arrival me-20"></span>
                        </div>
                        <div class="d-flex flex-column">
                            <div class="fw-bold text-white fs-1"><?= $station->name ?></div>
                            <small class="text-gray-300"><?= $station->arrival_at->diffInMinutes($station->departure_at) ?> min d'arret</small>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <?php if($planning->date_depart <= now()->addMinutes(20)): ?>
            <div class="d-flex flex-column rounded-3 w-90px h-90px bg-gray-300 shadow-lg justify-content-center align-items-center">
                <span class="fs-2 text-color-eva">Voie</span>
                <div class="fs-3x fw-bold text-color-eva">5</div>
            </div>
            <?php endif; ?>
        </div>
        <div class="d-flex flex-column align-items-center rounded ">
            <?php if($planning->engine->engine->type_train == 'tgv' || $planning->engine->engine->type_train == 'ic'): ?>
            <div class="d-flex flex-row align-items-center bg-cyan-600 text-white w-100 p-7">
                <div class="w-35">N° Voiture</div>
                <div class="d-flex flex-wrap justify-content-center align-items-center">
                    <?php for ($i=1; $i <= $planning->engine->engine->technical->nb_wagon; $i++): ?>
                    <div class="d-flex w-60px h-40px bg-white rounded text-center justify-content-center align-items-center text-color-eva fs-2 fw-semibold me-3"><?= $i; ?></div>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="d-flex flex-row align-items-center bg-blue-500 text-white w-100 px-7 py-2">
                <div class="w-35">Destination</div>
                <div class="d-flex flex-row justify-content-center align-items-center"><?= $planning->ligne->ligne->stationEnd->name; ?></div>
            </div>
            <?php endif; ?>
            <?php if($planning->status == 'retarded' || $planning->status == 'canceled'): ?>
            <div class="d-flex flex-row align-items-center bg-striped bg-orange-500 text-white w-100 px-7 py-2">
                <div class="w-35">Perturbation</div>
                <div class="d-flex flex-row justify-content-center align-items-center">
                    <?php foreach($planning->incidents as $incident): ?>
                    <span class="fs-6 fs-italic"><?= $incident->designation ?></span>&nbsp; |
                    <?php endforeach; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <?php
        $content_desktop = ob_get_clean();

        return response()->json([
            "content_mobile" => $content_mobile,
            "content_desktop" => $content_desktop
        ]);
    }

    private function setData(Request $request, string $type = null)
    {
        $data = match ($request->get('type')) {
            default => UserPlanning::where('user_id', $request->get('user_id')),
            "hub" => UserHub::find($request->get('user_hub_id'))
                ->plannings(),
        };

        return $data->whereBetween('date_depart', [now()->startOfDay(), now()->endOfDay()])
            ->where('status', 'initialized')
            ->orWhere('status', 'departure')
            ->orWhere('status', 'canceled')
            ->orWhere('status', 'retarded')
            ->orderBy('date_depart')
            ->limit(5)
            ->get();
    }
}
