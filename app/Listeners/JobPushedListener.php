<?php

namespace App\Listeners;

use Laravel\Horizon\Events\JobPushed;

class JobPushedListener
{
    public function __construct()
    {
    }

    public function handle(JobPushed $event): void
    {
        \Log::info('Information JobPushed', [$event]);
    }
}
