<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\Gares;
use App\Models\Core\Hub;
use App\Models\User;
use App\Services\SNCF\LigneVoyageur;
use App\Traits\GareTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HubController extends Controller
{
    use GareTrait;

    public function desactive($hub_id)
    {
        $hub = Hub::findOrFail($hub_id);
        $hub->active = false;
        $hub->save();

        return response()->json([
            "message" => "Hub desactive"
        ]);
    }

    public function active($hub_id)
    {
        $hub = Hub::findOrFail($hub_id);
        $hub->active = true;
        $hub->save();

        return response()->json([
            "message" => "Hub active"
        ]);
    }

    public function destroy($hub_id)
    {
        $hub = Hub::findOrFail($hub_id);
        $hub->delete();

        return response()->json([
            "message" => "Hub supprimer"
        ]);
    }

    public function liste(Request $request)
    {
        if ($request->has('user_id')) {
            $user = User::find($request->get('user_id'));
            $query = $user->hubs()->with('lignes', 'hub')->get();

            $hubs = [];

            foreach ($query as $hub) {
                $q_lignes = $hub->lignes()->with('ligne')->get();
                $lignes = [];

                foreach ($q_lignes as $ligne) {
                    $q_stations = $ligne->ligne->stations()->with('gare')->get();
                    $stations = [];
                    $stations_cos = [];

                    foreach ($q_stations as $station) {
                        $stations[] = [
                            "id" => $station->id,
                            "name" => $station->gare->name,
                            "latitude" => $station->gare->latitude,
                            "longitude" => $station->gare->longitude,
                        ];
                    }

                    foreach ($q_stations as $station) {
                        $stations_cos[] = [
                            $station->gare->latitude,
                            $station->gare->longitude,
                        ];
                    }

                    if($ligne->plannings()->where('date_depart', '>=', Carbon::now())->count() != 0) {
                        $lignes[] = [
                            "id" => $ligne->id,
                            "name" => $ligne->ligne->name,
                            "type" => $ligne->ligne->type_ligne,
                            "stations" => $stations,
                            "coordinates" => $stations_cos,
                            "station_start" => $ligne->ligne->stationStart,
                            "station_end" => $ligne->ligne->stationEnd,
                            "next_departure" => $ligne->plannings()->count() != 0 ? $ligne->plannings()->where('date_depart', '>=', Carbon::now())->first()->date_depart->format("H:i"):"Non Planifier",
                        ];
                    } else {
                        $lignes[] = [
                            "id" => $ligne->id,
                            "name" => $ligne->ligne->name,
                            "type" => $ligne->ligne->type_ligne,
                            "stations" => $stations,
                            "coordinates" => $stations_cos,
                            "station_start" => $ligne->ligne->stationStart,
                            "station_end" => $ligne->ligne->stationEnd,
                            "next_departure" => "Non Planifier",
                        ];
                    }

                }

                $hubs[] = [
                    "id" => $hub->id,
                    "name" => $hub->hub->gare->name,
                    "latitude" => $hub->hub->gare->latitude,
                    "longitude" => $hub->hub->gare->longitude,
                    "lignes" => $lignes,
                    "nb_slot_commerce" => $hub->hub->nb_slot_commerce,
                    "nb_slot_pub" => $hub->hub->nb_slot_pub,
                    "nb_slot_parking" => $hub->hub->nb_slot_parking,
                    "freq_actual" => $hub->hub->calcFreqActual(),
                    "status" => $hub->hub->getStatus()
                ];
            }

            $hubs = collect($hubs);

        } else {
            $hubs = Hub::with('lignes')->get();
        }

        return response()->json([
            "hubs" => $hubs
        ]);
    }

    public function info($hub_id)
    {
        $hub = Hub::findOrFail($hub_id)->load('gare', 'lignes', 'requirements');


        return response()->json([
            "hub" => $hub,
        ]);
    }

    public function infoLigne($hub_id, $ligne_id)
    {
        $hub = Hub::findOrFail($hub_id);
        $lignes = LigneVoyageur::getStopScheduleByRouteId($ligne_id);
        $routes = LigneVoyageur::getRouteScheduleByRouteId($ligne_id);

        $schedules = [];
        foreach ($lignes->stop_schedules as $stop_schedule) {
            $gare = Gares::where('name', 'LIKE', '%' . $stop_schedule->stop_point->name . '%')->first();
            if ($gare) {
                $schedules[] = [
                    "gares_id" => $gare->id,
                    "arret" => $stop_schedule->stop_point->name,
                    "heure" => Carbon::createFromTimestamp(strtotime($stop_schedule->first_datetime->date_time))->format('H:i')
                ];
            } else {
                $gare = Gares::create([
                    "name" => $stop_schedule->stop_point->name,
                    "code_uic" => $stop_schedule->stop_point->id,
                    "type_gare" => "d"
                ]);
                $schedules[] = [
                    "gares_id" => "",
                    "arret" => $stop_schedule->stop_point->name,
                    "heure" => Carbon::createFromTimestamp(strtotime($stop_schedule->first_datetime->date_time))->format('H:i')
                ];
            }

        }

        $ligne = collect([
            "terminus" => $lignes->notes[0]->value,
            "origin" => $routes->route_schedules[0]->display_informations->direction,
            "typeTrain" => $routes->route_schedules[0]->display_informations->commercial_mode,
            "schedules" => collect($schedules)->sortBy('heure')
        ]);
        $route = collect($routes);


        return response()->json([
            "hub" => $hub,
            "ligne" => $ligne,
            "route" => $route
        ]);
    }

    public function export()
    {
        $hubs = Hub::all();

        $file = fopen(public_path('/storage/hubs.json'), 'w');

        fwrite($file, json_encode($hubs));

        fclose($file);

        return response()->json();
    }

    public function import()
    {
        $file = fopen(public_path('/storage/hubs.json'), 'r');
        $hubs = json_decode(fread($file, filesize(public_path('/storage/hubs.json'))), true);
        fclose($file);

        foreach ($hubs as $hub) {
            if (Hub::find($hub['id'])) {
                Hub::find($hub['id'])->update([
                    "price" => $hub['price'],
                    "taxe_hub" => $hub['taxe_hub'],
                    "passenger_first" => $hub['passenger_first'],
                    "passenger_second" => $hub['passenger_second'],
                    "nb_slot_commerce" => $hub['nb_slot_commerce'],
                    "nb_slot_pub" => $hub['nb_slot_pub'],
                    "nb_slot_parking" => $hub['nb_slot_parking'],
                    "active" => $hub['active'],
                    "created_at" => $hub['created_at'],
                    "updated_at" => $hub['updated_at'],
                    "gares_id" => $hub['gares_id'],
                ]);
            } else {
                Hub::create([
                    "id" => $hub['id'],
                    "price" => $hub['price'],
                    "taxe_hub" => $hub['taxe_hub'],
                    "passenger_first" => $hub['passenger_first'],
                    "passenger_second" => $hub['passenger_second'],
                    "nb_slot_commerce" => $hub['nb_slot_commerce'],
                    "nb_slot_pub" => $hub['nb_slot_pub'],
                    "nb_slot_parking" => $hub['nb_slot_parking'],
                    "active" => $hub['active'],
                    "created_at" => $hub['created_at'],
                    "updated_at" => $hub['updated_at'],
                    "gares_id" => $hub['gares_id'],
                ]);
            }
        }
    }

}
