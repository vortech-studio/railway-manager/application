<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\Vourchers;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    public function export()
    {
        $vouchers = Vourchers::all();
        $file = fopen(public_path('/storage/vouchers.json'), 'w');

        fwrite($file, json_encode($vouchers));

        fclose($file);

        return response()->json();
    }

    public function import()
    {
        $file = fopen(public_path('/storage/vouchers.json'), 'r');
        $vouchers = json_decode(fread($file, filesize(public_path('/storage/vouchers.json'))), true);
        fclose($file);

        foreach ($vouchers as $voucher) {
            if (Vourchers::find($voucher['id'])) {
                Vourchers::find($voucher['id'])->update([
                    "code" => $voucher['code'],
                    "model_type" => $voucher['model_type'],
                    "model_id" => $voucher['model_id'],
                    "data" => $voucher['data'],
                    "expires_at" => $voucher['expires_at'],
                    "created_at" => $voucher['created_at'],
                    "updated_at" => $voucher['updated_at'],
                ]);
            } else {
                Vourchers::create([
                    "id" => $voucher['id'],
                    "code" => $voucher['code'],
                    "model_type" => $voucher['model_type'],
                    "model_id" => $voucher['model_id'],
                    "data" => $voucher['data'],
                    "expires_at" => $voucher['expires_at'],
                    "created_at" => $voucher['created_at'],
                    "updated_at" => $voucher['updated_at'],
                ]);
            }
        }

        return response()->json();
    }

    public function listeProduct(Request $request)
    {
        $model = match ($request->get('model_type')) {
            'Engine' => \App\Models\Core\Engine::class,
            'Hub' => \App\Models\Core\Hub::class,
            'Ligne' => \App\Models\Core\Ligne::class,
            default => null,
        };

        $products = $model::all();

        $data = [];

        $data = match ($request->get('model_type')) {
            'Engine' => $this->getProductEngine($data, $products),
            'Hub' => $this->getProductHub($data, $products),
            'Ligne' => $this->getProductLigne($data, $products),
            default => null,
        };

        return response()->json($data);
    }

    private function getProductEngine(array $data, mixed $products)
    {
        foreach ($products as $product) {
            $data[] = [
                "id" => $product->id,
                "name" => $product->name,
            ];
        }

        return $data;
    }

    private function getProductHub(array $data, mixed $products)
    {
        foreach ($products as $product) {
            $data[] = [
                "id" => $product->id,
                "name" => $product->gare->name,
            ];
        }

        return $data;
    }

    private function getProductLigne(array $data, mixed $products)
    {
        foreach ($products as $product) {
            $data[] = [
                "id" => $product->id,
                "name" => $product->name,
            ];
        }

        return $data;
    }
}
