<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlanningStationsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_planning_stations', function (Blueprint $table) {
            $table->id();
            $table->enum('type_infra', ['station', 'kvb', 'niveau', 'signalisation']);
            $table->string('name')->nullable();
            $table->timestamp('departure_at');
            $table->timestamp('arrival_at');
            $table->enum('status', ['init', 'arrival', 'departure', 'done'])->default('init');

            $table->foreignId('user_planning_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();

            $table->foreignId('ligne_station_id')
                ->nullable()
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_planning_stations');
    }
}
