<?php

namespace App\Console\Commands;

use App\Models\Core\Bonus;
use App\Models\User;
use Illuminate\Console\Command;

class GenerateBonusCommand extends Command
{
    protected $signature = 'generate:bonus';

    protected $description = 'Génère la liste des bonus mensuel du jeux';

    public function handle(): void
    {
        $dayOfMonth = now()->daysInMonth;
        foreach (User::all() as $user) {
            foreach ($user->dailys as $daily) {
                $daily->delete();
            }
        }
        Bonus::all()->each(function (Bonus $bonus) {
            $bonus->delete();
        });

        $type = collect([["argent"], ["tpoint"], ["research"], ["simulation"], ["audit_int"], ["audit_ext"]]);

        for ($i = 1; $i <= $dayOfMonth; $i++) {
            $types = $type->shuffle()->first()[0];
            $qte = $this->getQteByType($types);
            Bonus::create([
                "number_day" => $i,
                "type" => $types,
                "designation" => $this->getDesignationByType($types, $qte),
                "qte" => $qte
            ]);
        }
    }

    private function getDesignationByType($type, $qte)
    {
        return match ($type) {
            "argent", "research" => $qte . " €",
            "tpoint" => $qte . " Tpoints",
            "simulation" => $qte . " simulations",
            "audit_ext" => $qte . " audits externes",
            "audit_int" => $qte . " audits internes",
            default => ""
        };
    }

    private function getQteByType($type)
    {
        return match ($type) {
            "argent" => rand(50000,1000000),
            "tpoint" => rand(5,25),
            "research" => rand(10000,100000),
            "simulation", "audit_ext", "audit_int" => rand(1,10),
            default => 0
        };
    }
}
