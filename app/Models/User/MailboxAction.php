<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class MailboxAction extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function mailbox()
    {
        return $this->belongsTo(Mailbox::class);
    }
}
