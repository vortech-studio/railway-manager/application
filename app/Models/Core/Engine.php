<?php

namespace App\Models\Core;

use App\Traits\EngineCompositionTrait;
use App\Traits\EngineDataTrait;
use App\Traits\EngineTrait;
use App\Traits\HasCheckout;
use App\Traits\HasSlug;
use BeyondCode\Vouchers\Traits\HasVouchers;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;

class Engine extends Model
{
    use HasSlug, EngineDataTrait, EngineTrait, HasCheckout, EngineCompositionTrait, HasVouchers;

    public $timestamps = false;

    protected $guarded = [];

    protected $appends = [
        'format_price_checkout',
        'format_price_loan',
        'format_price_maintenance',
        'type_engine_format',
        'type_train_format',
        'type_energy_format',
        'image_url'
    ];

    public function technical()
    {
        return $this->hasOne(EngineTechnical::class);
    }

    public function compositions()
    {
        return $this->hasMany(EngineComposition::class);
    }

    public function rentals()
    {
        return $this->belongsToMany(Rental::class);
    }

    public function getTypeEngineFormatAttribute()
    {
        return match ($this->attributes['type_engine']) {
            "motrice" => "Motrice",
            "remorque" => "Remorque",
            "automotrice" => "Automotrice",
        };
    }

    public function getTypeTrainFormatAttribute()
    {
        return match ($this->attributes['type_train']) {
            "ter" => "Ter",
            "tgv" => "TGV Inoui",
            "ic" => "Intercité",
            "other" => "Autre",
        };
    }

    public function getTypeEnergyFormatAttribute()
    {
        return match ($this->attributes['type_energy']) {
            "diesel" => "Diesel",
            "electrique" => "Electrique",
            "hybride" => "Hybride",
            "vapeur" => "Vapeur",
            "aucun" => "Aucun",
        };
    }

    public function getFormatPriceCheckoutAttribute()
    {
        return eur($this->price_achat);
    }

    public function getFormatPriceLoanAttribute()
    {
        return eur($this->price_location);
    }

    public function getFormatPriceMaintenanceAttribute()
    {
        return eur($this->price_maintenance);
    }

    public function getImageUrlAttribute()
    {
        if ($this->type_engine == 'automotrice') {
            return asset('/storage/engines/automotrice/' . $this->slug . '-0.gif');
        } else {
            return asset('/storage/engines/' . $this->type_engine . '/' . $this->image);
        }
    }

    public function calcMaxRuntimeEngine()
    {
        if ($this->type_engine == 'automotrice') {
            $calc = ($this->technical->poids * $this->velocity * $this->technical->nb_wagon) / ($this->CoefTypeEngine() * $this->CoefTypeEnergy() * $this->CoefEssieux() * $this->CoefTypeMotor()) / 4;
        } else {
            $calc = ($this->technical->poids * $this->velocity) / ($this->CoefTypeEngine() * $this->CoefTypeEnergy() * $this->CoefEssieux() * $this->CoefTypeMotor()) / 4;
        }

        return $calc;
    }

    public function calcVitesseUsure()
    {
        if ($this->type_engine == 'automotrice') {
            $bogie = $this->getCoefTypeEssieux($this->technical->essieux) * $this->technical->nb_wagon;
            $motor = $this->getCoefTypeMotorisation($this->technical->type_motor) * 2;
            $marchandise = $this->getCoefTypeMarchandise($this->technical->type_marchandise) * $this->nb_passager;

            return round(($bogie + $motor + $marchandise * $this->getCoefTypeTrain('automotrice') / 10000) / 10, 2);
        } else {
            $bogie = $this->getCoefTypeEssieux($this->technical->essieux) * 2;
            $motor = $this->getCoefTypeMotorisation($this->technical->type_motor);

            return round($bogie + $motor * $this->getCoefTypeTrain($this->type_engine), 2);
        }
    }

    public static function dispatching(Engine $engine, $type_materiel_roulant): void
    {
        match ($type_materiel_roulant) {
            "motrice" => self::tableEngineMoteur($engine, $engine->type_energy),
            "remorque" => self::tableEngineRemorque($engine, $engine->technical->type_marchandise),
            "automotrice" => self::tableEngineAutomotrice($engine, $engine->type_engine),
        };
    }

    protected function slug(): Attribute
    {
        return Attribute::make(
            set: fn() => \Str::upper(\Str::slug($this->name)),
        );
    }

    private function CoefTypeEngine()
    {
        return match ($this->type_engine) {
            "motrice" => 1,
            "remorque" => 0.5,
            "automotrice" => 1.5,
        };
    }

    private function CoefTypeEnergy()
    {
        return match ($this->type_energy) {
            "diesel" => 1.5,
            "electrique" => 2,
            "hybride" => 2.5,
            "vapeur" => 1,
            "aucun" => 1,
        };
    }

    private function CoefEssieux()
    {
        return match ($this->technical->essieux) {
            "BB" => 1,
            "BOBO" => 1.5,
            "CC" => 2,
            "COCO" => 2.5,
        };
    }

    private function CoefTypeMotor()
    {
        return match ($this->technical->type_motor) {
            default => 1,
            "vapeur" => 2,
            "diesel" => 2.5,
            "electrique 1500V" => 3,
            "electrique 25000V" => 3.5,
            "electrique 1500V/25000V" => 4,
            "hybride" => 4.5,
        };
    }

    /**
     * @param $qte
     * @param $search /subtotal|reduction|subvention|total
     * @return float|int
     */
    public function calcCart($qte, $search = null)
    {
        return match ($search) {
            "subtotal" => $this->subtotal($qte),
            "reduction" => $this->reduction($qte),
            "subvention" => $this->subvention($qte),
            "total" => $this->total($qte)
        };
    }

    private function subtotal($qte)
    {
        return $this->price_achat * $qte;
    }

    private function reduction($qte)
    {
        return $this->subtotal($qte) * ($this->percent_reduction / 100);
    }

    private function subvention($qte)
    {
        if ($this->in_reduction) {
            return $this->subtotal($qte) - $this->reduction($qte) * auth()->user()->company->subvention / 100;
        } else {
            return $this->subtotal($qte) * auth()->user()->company->subvention / 100;
        }
    }

    private function total($qte)
    {
        if ($this->in_reduction) {
            return $this->subtotal($qte) - $this->reduction($qte) - $this->subvention($qte);
        } else {
            return $this->subtotal($qte) - $this->subvention($qte);
        }
    }

    public function getRentalCaution()
    {
        return $this->price_location * 21;
    }

    public function getRentalFrais()
    {
        $duration_maintenance_check = $this->compositions()->sum('maintenance_check_time');
        $diff_hour = now()->addMinutes($duration_maintenance_check)->diffInHours();
        return $duration_maintenance_check * $diff_hour;
    }

    public function getComposition()
    {
        return match($this->type_train) {
            "ter" => [intval($this->nb_passager)],
            "tgv", "other", "ic" => [intval($this->nb_passager * 20 / 100), intval($this->nb_passager * 80 / 100)],
        };
    }


}
