<?php
namespace Deployer;

require 'recipe/laravel.php';

// Config

set('repository', 'https://gitlab.com/vortech-studio/railway-manager/application.git');
set('application', 'railway-manager');
set('php_fpm_version', '8.2');


add('shared_files', ['.env']);
add('shared_dirs', []);
add('writable_dirs', []);

// Hosts

host('beta.railway-manager.ovh')
    ->set('remote_user', 'debian')
    ->set('deploy_path', '/www/wwwroot/beta.railway-manager.ovh')
    ->set('branch', 'master');

host('s1.railway-manager.ovh')
    ->set('remote_user', 'debian')
    ->set('deploy_path', '/www/wwwroot/s1.railway-manager.ovh')
    ->set('branch', 'production');

// Hooks

after('deploy:failed', 'deploy:unlock');
