<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserDailyLogin extends Model
{
    protected $casts = [
        'date' => 'datetime',
    ];
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
