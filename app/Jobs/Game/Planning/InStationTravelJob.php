<?php

namespace App\Jobs\Game\Planning;

use App\Models\User\UserPlanning;
use App\Models\User\UserPlanningStation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InStationTravelJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(public UserPlanningStation $station)
    {
    }

    public function handle(): void
    {
        $this->station->status = 'done';
        $this->station->save();

        $this->station->userPlanning->logs()->create([
            'message' => "Le ".$this->station->userPlanning->engine->engine->type_train_format." N° ".$this->station->userPlanning->number_travel." à atteint la gare de ".$this->station->name,
            "user_planning_id" => $this->station->userPlanning->id,
        ]);
    }
}
