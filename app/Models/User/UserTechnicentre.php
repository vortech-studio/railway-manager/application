<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserTechnicentre extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function hub()
    {
        return $this->belongsTo(UserHub::class, 'user_hub_id');
    }

    public function staffs()
    {
        return $this->hasMany(UserTechnicentreStaf::class);
    }

    public function engines()
    {
        return $this->hasMany(UserTechnicentreEngine::class);
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(UserTechnicentreTask::class);
    }
}
