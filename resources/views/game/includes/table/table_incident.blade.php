@foreach($incidents as $incident)
    <div class="d-flex flex-row h-150px border-bottom border-gray-500 ">
        <div class="d-flex flex-column justify-content-center align-items-center border-gray-500 border-end-dashed p-5">
            <img class="mb-1" src="{{ $incident->planning->engine->engine->image_url }}" alt="">
            <span class="fs-2 text-white">{{ $incident->planning->engine->engine->name }}</span>
            <small class="text-white">{{ $incident->planning->number_travel }}</small>
        </div>
        <div class="d-flex flex-column justify-content-center align-items-center border-gray-500 border-end-dashed w-25 p-5">
            <div class="text-white fs-3">Ligne</div>
            <span class="fs-2 text-white fw-bolder text-center">{{ $incident->planning->ligne->ligne->name }}</span>
        </div>
        <div class="d-flex flex-column justify-content-center align-items-center border-gray-500 border-end-dashed p-5 w-200px">
            <div class="text-white fs-3">{{ $incident->planning->date_depart->format("d/m/Y") }}</div>
            <span class="fs-2 text-white fw-bolder" data-bs-toggle="tooltip" title="Heure de départ">{{ $incident->planning->date_depart->format("H:i") }}</span>
        </div>
        <div class="d-flex flex-row justify-content-between align-items-center p-0">
            <img src="{{ $incident->type->image_origin_url }}" alt="" class="w-200px ps-5 opacity-50 shadow-lg  me-10" data-bs-toggle="tooltip" title="{{ $incident->type->origin_text }}">
            <div class="d-flex flex-column">
                <button  data-incident-id="{{ $incident->id }}" class="btn btn-sm btn-primary btn-flex rounded-4 mb-2 showIncident">
                    <img src="{{ asset('/storage/icons/warning_'.$incident->niveau_indicator.'.png') }}" alt="" class="w-30px me-2 animation-blink">
                    <span>Détail de l'incident</span>
                </button>
                <a href="{{ route('engine.show', 1) }}" class="btn btn-sm btn-primary btn-flex rounded-4">
                    <img src="{{ asset('/storage/icons/train.png') }}" alt="" class="w-30px me-2">
                    <span>Détail de l'engin</span>
                </a>
            </div>
        </div>
    </div>
@endforeach
