<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToUserCompanies extends Migration
{
    public function up(): void
    {
        Schema::table('user_companies', function (Blueprint $table) {
            $table->decimal('rate_research')->default(1.0)->comment("Taux de recherche de l'entreprise");
            $table->float('credit_impot', 16)->default(0)->comment("Crédit d'impôt de l'entreprise");
            $table->float('research_coast_base', 16)->default(0);
        });
    }

    public function down(): void
    {
        Schema::table('user_companies', function (Blueprint $table) {
            $table->dropColumn('rate_research');
            $table->dropColumn('credit_impot');
            $table->dropColumn('research_coast_base');
        });
    }
}
