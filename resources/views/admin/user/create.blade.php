@extends("admin.template")

@section("css")

@endsection

@section("toolbar")
    @include('admin.includes.toolbar', [
        'title' => "Nouveau joueur",
        'action' => true,
        'data' => [
            [
                'url' => back()->getTargetUrl(),
                'title' => "<i class='fa-solid fa-arrow-left me-2'></i> Retour",
                'class' => "primary"
            ]
        ]
    ])
@endsection

@section("content")
    <form action="{{ route('admin.user.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row mb-10">
            <div class="col-md-6 col-sm-12 mb-10">
                <div class="card shadow-lg">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa-solid fa-user fs-5 text-body me-4"></i> Information du joueur
                        </h3>
                    </div>
                    <div class="card-body">
                        <x-form.input
                            name="name"
                            label="Pseudo du joueur"
                            required="true" />

                        <x-form.input
                            name="email"
                            label="Adresse email du joueur"
                            type="email"
                            required="true" />

                        <x-form.switch
                            name="admin"
                            label="Administrateur"
                            value="1"
                            check="" />

                        <x-form.switch
                            name="premium"
                            label="Compte premium"
                            value="1"
                            check="" />

                        <x-form.slider
                            id="level"
                            label="Niveau de départ"
                            min="{{ \LevelUp\Experience\Models\Level::min('level') }}"
                            max="{{ \LevelUp\Experience\Models\Level::max('level') }}" />
                        <x-form.input
                            name="level_id"
                            label="Niveau de départ"
                            type="hidden" />
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 mb-10">
                <div class="card shadow-lg">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa-solid fa-user fs-5 text-body me-4"></i> Avatar du joueur
                        </h3>
                    </div>
                    <div class="card-body">
                        <x-form.image-input
                            name="avatar" />
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 mb-10">
                <div class="card shadow-lg">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa-solid fa-building fs-5 text-body me-4"></i> Information sur l'entreprise
                        </h3>
                    </div>
                    <div class="card-body">
                        <x-form.input
                            name="name_company"
                            label="Nom de l'entreprise"
                            required="true" />
                        <x-form.textarea
                            name="desc_company"
                            label="Description de l'entreprise" />
                        <x-form.input
                            name="argent"
                            label="Argent de départ"
                            type="number"
                            value="3000000"
                            required="true" />
                        <x-form.input
                            name="tpoint"
                            label="T Point de départ"
                            type="number"
                            value="500"
                            required="true" />

                        <x-form.input
                            name="research"
                            label="Allocation à la recherche de départ"
                            type="number"
                            value="0"
                            required="true" />
                        <x-base.underline
                            title="Logo de l'entreprise"
                            size-text="fs-4"
                            size="1" />
                        <x-form.image-input
                            name="logo_company" />
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 mb-10">
                <div class="card shadow-lg">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fa-solid fa-building fs-5 text-body me-4"></i> Information sur le secrétariat
                        </h3>
                    </div>
                    <div class="card-body">
                        <x-form.input
                            name="name_secretary"
                            label="Nom de la secrétaire"
                            required="true" />
                        <x-base.underline
                            title="Avatar de la secrétaire"
                            size-text="fs-4"
                            size="1" />
                        <x-form.image-input
                            name="avatat_secretary" />
                    </div>
                </div>
            </div>
        </div>
        <div class="rounded-3 p-5 bg-white shadow-lg d-flex flex-wrap justify-content-center">
            <x-form.button class="btn btn-block btn-primary"/>
        </div>
    </form>
@endsection

@section("js")

    <script type="text/javascript">
        let slider = document.getElementById('level');

        noUiSlider.create(slider, {
            start: 1,
            connect: true,
            step: 1,
            tooltips: true,
            behaviour: 'tap-drag',
            format: wNumb({
                decimals: 0
            }),
            range: {
                'min': {{ LevelUp\Experience\Models\Level::min('level') }},
                'max': {{ LevelUp\Experience\Models\Level::max('level') }},
            },
            pips: {
                mode: "values",
                values: [{{ LevelUp\Experience\Models\Level::min('level') }}, {{ LevelUp\Experience\Models\Level::max('level') }}],
                density: 4
            }
        });

        slider.noUiSlider.on('update', function (values, handle) {
            document.querySelector('input[name="level"]').value = values[handle];
        });
    </script>
@endsection
