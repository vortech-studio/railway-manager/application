@extends("vigimat.template")

@section("css")
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
@endsection

@section("content")
    <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
        <div class="col-sm-12 col-lg-4">
            <div class="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-xl-100" style="background-color: #146aef;background-image:url('{{ asset('/storage/icons/svg/train.svg') }}');">
                <!--begin::Header-->
                <div class="card-header pt-5 mb-3">
                    <!--begin::Icon-->
                    <div class="d-flex flex-center rounded-circle h-80px w-80px" style="border: 1px dashed rgba(255, 255, 255, 0.4);background-color: #146aef">
                        <i class="fa-solid fa-check text-white fs-2qx lh-0"></i>
                    </div>
                    <!--end::Icon-->
                </div>
                <!--end::Header-->

                <!--begin::Card body-->
                <div class="card-body d-flex align-items-end mb-3">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center">
                        <span class="fs-4hx text-white fw-bold me-6" data-vue="nbActifEngine">4</span>

                        <div class="fw-bold fs-6 text-white">
                            <span class="d-block">Matériel Roulant</span>
                            <span class="">Actif</span>
                        </div>
                    </div>
                    <!--end::Info-->
                </div>
                <!--end::Card body-->
            </div>
        </div>
        <div class="col-sm-12 col-lg-4">
            <div class="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-xl-100" style="background-color: #e93434;background-image:url('{{ asset('/storage/icons/svg/train_error.svg') }}');">
                <!--begin::Header-->
                <div class="card-header pt-5 mb-3">
                    <!--begin::Icon-->
                    <div class="d-flex flex-center rounded-circle h-80px w-80px" style="border: 1px dashed rgba(255, 255, 255, 0.4);background-color: #e93434">
                        <i class="fa-solid fa-times-circle text-white fs-2qx lh-0"></i>
                    </div>
                    <!--end::Icon-->
                </div>
                <!--end::Header-->

                <!--begin::Card body-->
                <div class="card-body d-flex align-items-end mb-3">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center">
                        <span class="fs-4hx text-white fw-bold me-6" data-vue="nbInactifEngine">4</span>

                        <div class="fw-bold fs-6 text-white">
                            <span class="d-block">Matériel Roulant</span>
                            <span class="">Inactif</span>
                        </div>
                    </div>
                    <!--end::Info-->
                </div>
                <!--end::Card body-->
            </div>
        </div>
        <div class="col-sm-12 col-lg-4">
            <div class="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-xl-100" style="background-color: #efb114;background-image:url('{{ asset('/storage/icons/svg/train_warning.svg') }}');">
                <!--begin::Header-->
                <div class="card-header pt-5 mb-3">
                    <!--begin::Icon-->
                    <div class="d-flex flex-center rounded-circle h-80px w-80px" style="border: 1px dashed rgba(255, 255, 255, 0.4);background-color: #efb114">
                        <i class="fa-solid fa-cogs text-white fs-2qx lh-0"></i>
                    </div>
                    <!--end::Icon-->
                </div>
                <!--end::Header-->

                <!--begin::Card body-->
                <div class="card-body d-flex align-items-end mb-3">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center">
                        <span class="fs-4hx text-white fw-bold me-6" data-vue="nbFaultyEngine">4</span>

                        <div class="fw-bold fs-6 text-white">
                            <span class="d-block">Matériel Roulant</span>
                            <span class="">En erreur</span>
                        </div>
                    </div>
                    <!--end::Info-->
                </div>
                <!--end::Card body-->
            </div>
        </div>
    </div>
    <div class="card shadow-lg">
        <div class="card-header">
            <div class="card-title">
                <div class="fs-3">Matériels Roulants</div>
            </div>
            <div class="card-toolbar">
                <div class="me-5">
                    <input type="text" name="search" class="form-control" data-table-filter="search" placeholder="Rechercher un engins...">
                </div>
                <x-base.button
                class="btn btn-sm btn-primary rounded-5"
                text="Audit du parc"
                id="btnAudit" />
            </div>
        </div>
        <div class="card-body">
            <table id="liste_engine" class="table table-striped border gy-5 gs-7">
                <thead>
                    <tr class="fw-semibold fs-4 text-gray-800">
                        <th>Identification</th>
                        <th>Date d'achat</th>
                        <th>Usure</th>
                        <th>Ancienneté</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(auth()->user()->technicentres()->find(Session::get('technicentre')[0]['id'])->engines as $engine)
                        <tr>
                            <td>{{ $engine->engine->number }}</td>
                            <td>{{ $engine->engine->date_achat->format("d/m/Y") }}</td>
                            <td>{{ $engine->engine->calcUsureEngine() }} %</td>
                            <td>{{ $engine->engine->calcAncienneteEngine() }} / 5</td>
                            <td>{!! $engine->engine->status_format !!}</td>
                            <td>
                                <a href="{{ route('vigimat.engine.show', $engine->user_engine_id) }}" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
                                    <i class="fa-solid fa-cogs fs-2"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">

        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="auditEngine">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Audit de votre parc actuel</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <p data-vue="auditDescription"></p>
                    <table class="table table-striped gy-5 gs-7">
                        <thead>
                            <tr class="fs-2 fw-semibold bg-gray-300">
                                <th>Matériel</th>
                                <th>Anciennete</th>
                                <th>Usure</th>
                                <th>Préventive</th>
                                <th>Curative</th>
                                <th>Description</th>
                                <th>Conseil</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="rapportBody"></tbody>
                    </table>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="text/javascript">
        let div = {
            tableListeEngine: document.querySelector('#liste_engine'),
            inputSearch: document.querySelector('[data-table-filter="search"]'),
            btnAudit: document.querySelector('#btnAudit'),
        }

        let modal = {
            modalAudit: document.querySelector('#auditEngine'),
        }

        let block = {

        }

        @if(Session::has('technicentre'))
            updateStatistique()
            let table = $(div.tableListeEngine).DataTable({
                info: !1,
                order: [],
                pageLength: 10,
                language: {
                    url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/fr-FR.json',
                },
            })

            searching(table)
            div.btnAudit.addEventListener('click', () => {
                div.btnAudit.setAttribute('data-kt-indicator', 'on')
                generateAudit()
            })
        @endif

        async function updateStatistique() {
            $.ajax({
                url: '/api/vigimat/charts',
                data: {'type': "stat"},
                success: data => {
                    document.querySelector('[data-vue="nbActifEngine"]').innerHTML = data.nbActifEngine
                    document.querySelector('[data-vue="nbInactifEngine"]').innerHTML = data.nbInactifEngine
                    document.querySelector('[data-vue="nbFaultyEngine"]').innerHTML = data.nbFaultyEngine
                }
            })
        }
        function searching(table) {
            div.inputSearch.addEventListener('keyup', e => {
                table.search(e.target.value).draw();
            })
        }
        function generateAudit() {
            $.ajax({
                url:'/api/vigimat/engines/audit',
                success: data => {
                    console.log(data)
                    let mds = new bootstrap.Modal(modal.modalAudit)
                    div.btnAudit.removeAttribute('data-kt-indicator')
                    mds.show()
                    let rapportBody = document.querySelector('#rapportBody')
                    rapportBody.innerHTML = ''
                    data.rapport.forEach(element => {
                        let row = rapportBody.insertRow()
                        let cellNom = row.insertCell(0)
                        let cellAnciennete = row.insertCell(1)
                        let cellUsure = row.insertCell(2)
                        let cellPreventive = row.insertCell(3)
                        let cellCurative = row.insertCell(4)
                        let cellDescription = row.insertCell(5)
                        let cellConseil = row.insertCell(6)
                        let cellActions = row.insertCell(7);

                        cellNom.innerHTML = element.nom
                        cellAnciennete.innerHTML = element.anciennete+" / 5"
                        cellUsure.innerHTML = element.usure_totale+" %"
                        cellPreventive.innerHTML = element.maintenancePreventive ? "Oui" : "Non"
                        cellCurative.innerHTML = element.maintenanceCurative ? "Oui" : "Non"
                        cellDescription.innerHTML = element.description
                        cellConseil.innerHTML = element.conseils.join('<br>')

                        // Créer les boutons d'action
                        let boutonMaintenancePreventive = document.createElement('button');
                        boutonMaintenancePreventive.innerText = 'Maintenance Préventive';
                        boutonMaintenancePreventive.className = 'btn btn-primary btn-sm';
                        boutonMaintenancePreventive.onclick = function() {
                            declencherMaintenancePreventive(element.id, element.nom);
                        };
                        cellActions.appendChild(boutonMaintenancePreventive);

                        let boutonMaintenanceCurative = document.createElement('button');
                        boutonMaintenanceCurative.innerText = 'Maintenance Curative';
                        boutonMaintenanceCurative.className = 'btn btn-danger btn-sm';
                        boutonMaintenanceCurative.onclick = function() {
                            declencherMaintenanceCurative(element.id, element.nom);
                        };
                        cellActions.appendChild(boutonMaintenanceCurative);

                    })
                }
            })
        }

        // Fonction pour déclencher la maintenance préventive
        function declencherMaintenancePreventive(id, nomMateriel) {
            // Ajouter la logique pour déclencher la maintenance préventive pour le matériel avec le nom spécifié
            $.ajax({
                url: '/api/game/maintenance/maintenance',
                method: 'POST',
                data: {engine_id: id, type: 'prefentif'},
                success: data => {
                    toastr.success('Maintenance préventive déclenchée pour le matériel '+nomMateriel);
                }
            })
        }

        // Fonction pour déclencher la maintenance curative
        function declencherMaintenanceCurative(id, nomMateriel) {
            $.ajax({
                url: '/api/game/maintenance/maintenance',
                method: 'POST',
                data: {engine_id: id, type: 'curatif'},
                success: data => {
                    toastr.success('Maintenance Curative déclenchée pour le matériel '+nomMateriel);
                }
            })
        }
    </script>
@endsection
