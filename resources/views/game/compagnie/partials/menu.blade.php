<div class="d-flex flex-row align-items-center">
    <a href="{{ route('compagnie.customise.index') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Personnalisation">
        <img src="{{ asset('/storage/icons/adjust.png') }}" class="w-50px" alt="">
    </a>
    <a href="{{ route('compagnie.profil.index') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Profil">
        <img src="{{ asset('/storage/icons/identity.png') }}" class="w-50px" alt="">
    </a>
    <a href="{{ route('compagnie.index') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Ma Compagnie">
        <img src="{{ asset('/storage/icons/hq.png') }}" class="w-50px" alt="">
    </a>
    <a href="{{ route('compagnie.bonus.index') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Connexion quotidienne">
        <img src="{{ asset('/storage/icons/bonus.png') }}" class="w-50px" alt="">
    </a>
</div>
