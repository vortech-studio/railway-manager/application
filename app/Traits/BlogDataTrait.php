<?php

namespace App\Traits;

trait BlogDataTrait
{
    public static function getDataTypeAnnonce()
    {
        $type = [];
        $type[] = ["id" => "event", "value" => "Événement"];
        $type[] = ["id" => "notice", "value" => "Annonce"];
        $type[] = ["id" => "news", "value" => "A la une"];
        return $type;
    }
}
