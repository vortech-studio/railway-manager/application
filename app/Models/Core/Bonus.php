<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    public $timestamps = false;
    protected $guarded = [];
}
