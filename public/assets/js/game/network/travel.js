let typeIcon = {
    'hub': {url: '/storage/icons/hub.png', talle: [45,48]},
    'train': {url: '/storage/icons/train_map.png', talle: [32,37]},
    'station': {url: '/storage/icons/train-station.png', talle: [32,37]}
}
let user_hub_id = document.querySelector('[name="user_hub_id"]').value
let planning_id = document.querySelector('[name="planning_id"]').value
let user_id = document.querySelector('[name="user_id"]').value

document.querySelectorAll('[name="method"]').forEach((el) => {
    el.addEventListener('change', (e) => {
        if(e.target.value === 'map') {
            document.querySelector("#mapMethod").classList.remove('d-none')
            document.querySelector("#ecranMethod").classList.add('d-none')
            document.querySelector("#tableMethod").classList.add('d-none')
        } else if(e.target.value === 'ecran') {
            document.querySelector("#mapMethod").classList.add('d-none')
            document.querySelector("#ecranMethod").classList.remove('d-none')
            document.querySelector("#tableMethod").classList.add('d-none')
        } else  {
            document.querySelector("#mapMethod").classList.add('d-none')
            document.querySelector("#ecranMethod").classList.add('d-none')
            document.querySelector("#tableMethod").classList.remove('d-none')
        }
    })
})

function MapInitHub(data, containerDiv) {
    let followMarker = false;
    let coords = {};
    const zoomLevel = 15

    let container = L.DomUtil.get(containerDiv);
    if(container != null) {
        container._leaflet_id = null;
    }
    let map = null;

    const hubsLatLng = [];

    map = L.map(containerDiv).setView([data.gare.latitude, data.gare.longitude], zoomLevel);
    const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    mainLayer.addTo(map);

    const hubOptions = {
        lat: data.gare.latitude,
        lng: data.gare.longitude,
        title: data.gare.name,
        draggable: false,
    };

    let marker = addMarker(hubOptions, map, 'hub');

}
function MapInitLignes(data, containerDiv) {
    let followMarker = false;
    let coords = {};
    const zoomLevel = 7

    let container = L.DomUtil.get(containerDiv);
    if(container != null) {
        container._leaflet_id = null;
    }
    let map = null;

    const hubsLatLng = [];

    map = L.map(containerDiv).setView([data[0].hub.latitude, data[0].hub.longitude], zoomLevel);
    const secondLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    secondLayer.addTo(map);

    const hubOptions = {
        lat: data[0].hub.latitude,
        lng: data[0].hub.longitude,
        title: data[0].hub.name,
        draggable: false,
    };

    let hubMarker = addMarker(hubOptions, map, 'hub');
    hubMarker.on('click', function(event) {
        addPopup(hubMarker, `
                <h3>${data[0].hub.name}</h3>
                <table class="table table-row-bordered table-row-gray-800 border border-gray-800 table-striped gy-2 gx-2 gs-2 mb-2">
                    <thead>
                        <tr>
                            <th>Commerce</th>
                            <th>Publicité</th>
                            <th>Parking</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${data[0].hub.nb_slot_commerce}</td>
                            <td>${data[0].hub.nb_slot_pub}</td>
                            <td>${data[0].hub.nb_slot_parking}</td>
                        </tr>
                    </tbody>
                </table>
                <strong class="mb-1">Fréquence Actuel:</strong> ${data[0].hub.freq_actual} <br>
                <strong class="me-1">Etat de la Gare: </strong> <span class="badge bg-${data[0].hub.state == true ? 'success' : 'danger'}">${data[0].hub.state == true ? 'Ouvert' : 'Fermer'}</span> <br>
            `);
    })

    data.forEach(ligne => {
        const LineOption = {
            lat: ligne.arrival.latitude,
            lng: ligne.arrival.longitude,
            title: ligne.arrival.name,
            draggable: false,
        }

        let ArrivalMarker = addMarker(LineOption, map, 'train')
        ArrivalMarker.on('click', function(event) {
            addPopup(ArrivalMarker, `
                    <h3>${ligne.arrival.name}</h3>
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row justify-content-between align-items-center p-2">
                            <div class="fw-bolder">Départ:</div> ${ligne.hub.name}
                        </div>
                        <div class="separator border-gray-700 my-1"></div>
                        <div class="d-flex flex-row justify-content-between align-items-center p-2">
                            <div class="fw-bolder">Arrivée:</div> ${ligne.arrival.name}
                        </div>
                        <div class="separator border-gray-700 my-1"></div>
                        <div class="d-flex flex-row justify-content-between align-items-center p-2">
                            <div class="fw-bolder"><i class="fa-solid fa-clock me-1"></i> Prochain départ:</div> Non Planifier
                        </div>
                    </div>
                `);
        })

        let typeLine = {
            "ter": {color: 'blue'},
            "tgv": {color: 'red'},
            "ic": {color: 'purple'},
            "othe": {color: 'black'},
        }
        let polyline = L.polyline([ligne.coordinates], {color: typeLine[ligne.type_ligne.name].color}).addTo(map)

        ligne.stations.forEach(station => {
            const StationOption = {
                lat: station.latitude,
                lng: station.longitude,
                title: station.name,
                draggable: false,
            }

            let StationMarker = addMarker(StationOption, map, 'station')
            StationMarker.on('click', function(event) {
                addPopup(StationMarker, `
                    <div class="d-flex flex-row align-items-center p-5">
                        <div class="symbol symbol-20 me-3">
                            <img src="${typeIcon['station'].url}" alt="Station">
                        </div>
                        <div class="fs-3">${station.name}</div>
                    </div>
                `);
            })
        })

        var bounds = L.latLngBounds(ligne.coordinates);
        map.fitBounds(bounds);
    })
    let actualStation;
    let data_engine_speed;
    setInterval(() => {
        $.ajax({
            url: '/api/account/'+user_id+'/planning/travel/'+planning_id,
            success: data => {
                console.log(data)
                if(actualStation !== data.actual_station.name) {
                    let routeLines = data.latlng
                    let actualPositionMarker = addMarker({
                        lat: data.actual_station.lat,
                        lng: data.actual_station.long,
                        title: data.actual_station.name,
                        draggable: false,
                    }, map, 'train')
                    actualStation = data.actual_station.name
                    document.querySelector('.progress-bar-striped').style.width = data.percent_advance + '%'
                    document.querySelector('[data-html="next-station"]').innerHTML = data.next_station.name
                    document.querySelector('[data-html="arrival-station"]').innerHTML = data.next_station.time
                    data_engine_speed = data.engine_speed_max
                }

            }
        })
    }, 1000)

    document.querySelector('[data-html="speed"]').innerHTML = getSpeedEngine(data_engine_speed)+" Km/h"
    setInterval(() => {
        document.querySelector('[data-html="speed"]').innerHTML = getSpeedEngine(data_engine_speed)+" Km/h"
    }, 1000 * Math.floor(Math.random() * 200))

}

function addMarker(options, map, type = 'hub') {
    const icon = L.icon({
        iconUrl: typeIcon[type].url,
        iconSize:    typeIcon[type].talle, // size of the icon
        iconAnchor:   [16, 18], // point of the icon which will correspond to marker's location
        popupAnchor:  [1, -9]
    })
    const marker = L.marker([options.lat, options.lng], { title: options.title, draggable: options.draggable, icon: icon });
    marker.addTo(map);

    marker.on('dragend', function(event) {
        coords = event.target._latlng;
        showNewCoords(event.target._latlng, event.target);
        if (followMarker) {
            map.setView(event.target._latlng);
        }
    });

    return marker;
}

function addPopup(marker, popupContent) {
    marker.bindPopup('', {'className': 'custom'})
    let popup = marker.getPopup()
    popup.setContent(popupContent)
    marker.openPopup()
}


function showNewCoords(coords, marker) {
    marker
        .bindPopup(`lat: ${coords.lat} - lng: ${coords.lng}`)
        .openPopup();
}
