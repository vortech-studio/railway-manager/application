<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlanningLogsTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_planning_logs', function (Blueprint $table) {
            $table->id();
            $table->string('message');
            $table->timestamps();

            $table->foreignId('user_planning_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_planning_logs');
    }
}
