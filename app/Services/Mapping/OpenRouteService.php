<?php

namespace App\Services\Mapping;

class OpenRouteService
{
    protected string $endpoint = 'https://api.openrouteservice.org/';

    public function geocodeSearch(object $gare = null, string $nameGare = null): bool|\Illuminate\Support\Collection
    {
        if (isset($gare)) {
            $query = http_build_query([
                'api_key' => config('services.openrouteservice.key'),
                'text' => 'Gare '.$gare->gare_alias_libelle_noncontraint,
            ]);
        } else {
            $query = http_build_query([
                'api_key' => config('services.openrouteservice.key'),
                'text' => 'Gare '.$nameGare,
            ]);

        }

        $call = \Http::withoutVerifying()
            ->get($this->endpoint.'geocode/search', $query);

        if ($call->successful()) {
            return collect([
                'lat' => $call->object()->features[0]->geometry->coordinates[0],
                'lng' => $call->object()->features[0]->geometry->coordinates[1],
            ]);
        } else {
            return $call->serverError();
        }
    }

    public function geocodeMatchInfo(object $gare = null, string $nameGare = null)
    {
        if (isset($gare)) {
            $query = http_build_query([
                'api_key' => config('services.openrouteservice.key'),
                'text' => 'Gare '.$gare->gare_alias_libelle_noncontraint,
            ]);
        } else {
            $query = http_build_query([
                'api_key' => config('services.openrouteservice.key'),
                'text' => 'Gare '.$nameGare,

            ]);

        }

        $call = \Http::withoutVerifying()
            ->withHeaders([
                'lang' => [
                    'name' => 'French',
                    'via' => 'header'
                ]
            ])
            ->get($this->endpoint.'geocode/search', $query);

        if ($call->successful()) {
            if(isset($call->object()->features[0]->properties->macroregion)) {
                return collect([
                    "region" => \Str::upper($call->object()->features[0]->properties->macroregion),
                    "pays" => \Str::ucfirst($call->object()->features[0]->properties->country),
                ]);
            } else {
                $mapbox = new Mapbox();
                if(isset($gare)) {
                    $s = $mapbox->geocodeMatchInfo($gare->gare_alias_libelle_noncontraint);
                } else {
                    $s = $mapbox->geocodeMatchInfo(null, $nameGare);
                }
                return collect([
                    "region" => $s['region'],
                    "pays" => $s['pays']
                ]);
            }
        } else {
            return $call->serverError();
        }
    }

    public function matrixGetInfo(array $coordinates)
    {
        $query = [
            'locations' => $coordinates,
            'metrics' => ["distance", "duration"],
            'units' => 'km',
        ];

        $call = \Http::withoutVerifying()
            ->withHeaders([
                "Authorization" => config('services.openrouteservice.key'),
                "Accept" => "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8",
                "Content-Type" => "application/json; charset=utf-8",
            ])
            ->post($this->endpoint.'v2/matrix/driving-car', $query);

        if ($call->successful()) {
            return $call->object();
        } else {
            return $call->serverError();
        }
    }
}
