<?php

namespace App\Models\Core;

use AnthonyMartin\GeoLocation\GeoPoint;
use App\Models\User\UserPlanningStation;
use Illuminate\Database\Eloquent\Model;

class LigneStation extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public function gare()
    {
        return $this->belongsTo(Gares::class, 'gares_id');
    }

    public function ligne()
    {
        return $this->belongsTo(Ligne::class);
    }

    public function userPlanningStations()
    {
        return $this->hasMany(UserPlanningStation::class);
    }

    public static function time_stop_station_config(LigneStation $ligneStation)
    {
        return match ($ligneStation->gare->type_gare) {
            "a" => rand(4, 10),
            "b" => rand(1, 3),
            "c" => rand(1, 2),
            "d" => 1
        };
    }

    // Fonction pour calculer le temps en fonction de la distance et de la vitesse
    public function calculerTemps($distance, $vitesse)
    {
        // Formule : Temps = Distance / Vitesse
        $tempsEnSecondes = $distance / $vitesse;
        return intval(($tempsEnSecondes * 60) / 1.8); // Convertir en minutes si nécessaire
    }

// Fonction pour calculer la distance en fonction du temps et de la vitesse
    public function calculerDistance($vitesse, $tempsEnMinutes)
    {
        // Convertir le temps en minutes en secondes
        $tempsEnSecondes = $tempsEnMinutes * 60;

        // Formule : Distance = Vitesse * Temps
        $distanceEnMetres = $vitesse * $tempsEnSecondes;
        return $distanceEnMetres / 1000; // Convertir en kilomètres si nécessaire
    }

// Fonction pour calculer le temps en fonction de la distance et de la vitesse
    public function calculerTempsInconnu($vitesse, $distance)
    {
        // Formule : Temps = Distance / Vitesse
        $tempsEnSecondes = $distance / $vitesse;
        return $tempsEnSecondes / 60; // Convertir en minutes si nécessaire
    }

// Fonction pour calculer la distance en fonction du temps et de la vitesse
    public function calculerDistanceInconnue($vitesse, $tempsEnMinutes)
    {
        // Convertir le temps en minutes en secondes
        $tempsEnSecondes = $tempsEnMinutes * 60;

        // Formule : Distance = Vitesse * Temps
        $distanceEnMetres = $vitesse * $tempsEnSecondes;
        return $distanceEnMetres / 1000; // Convertir en kilomètres si nécessaire
    }

    public function calculerDistanceVincenty($lat1,$lon1,$lat2,$lon2)
    {
        $geoA = new GeoPoint($lat1, $lon1);
        $geoB = new GeoPoint($lat2, $lon2);

        return $geoA->distanceTo($geoB, 'km');
    }
}
