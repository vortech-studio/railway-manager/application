<?php

namespace App\Traits;

use App\Models\Core\CardHolder;
use App\Models\Core\Engine;
use App\Models\Core\Mouvement;
use App\Models\User;
use Illuminate\Http\Request;

trait HasCheckout
{
    public static function checkoutEngine(Request $request)
    {
        $user = auth()->user();
        $engine = Engine::find($request->input('engine_id'));

        $en_us = $user->engines()->create([
            "max_runtime_engine" => $engine->calcMaxRuntimeEngine(),
            "user_id" => $user->id,
            "engine_id" => $engine->id,
            "user_hub_id" => $user->hubs()->orderBy('date_achat', 'desc')->first()->id,
            "number" => User\UserEngine::defineNumberEngine()
        ]);

        // Création de la composition si il s'agit d'une automotrice

        if($engine->type_engine == 'automotrice') {
            $composition = $user->compositions()->create([
                "user_id" => $user->id,
                "user_hub_id" => $user->hubs()->orderBy('date_achat', 'desc')->first()->id,
                "user_ligne_id" => $user->hubs()->orderBy('date_achat', 'desc')->first()->lignes()->orderBy('date_achat', 'desc')->first()->id,
            ]);

            $composition->tracks()->create([
                "user_composition_id" => $composition->id,
                "user_engine_id" => $en_us->id
            ]);
        }

        return $en_us;
    }

    public static function checkoutCardHolder(float $amount, User $user): bool
    {
        if($user->tpoint < $amount) {
            return false;
        } else {
            $user->tpoint -= $amount;
            $user->save();

            return true;
        }
    }

    public static function checkoutTpoint(\Stripe\PaymentIntent $intent, User|\Illuminate\Contracts\Auth\Authenticatable|null $user)
    {
        $tpoint = intval(\Str::remove('tpoint-', $intent->metadata->product));
        $user->tpoint += $tpoint;
        $user->save();
    }

    public static function checkoutHub(float $amount, User $user): bool
    {
        if($user->argent < $amount) {
            return false;
        } else {
            $user->argent -= $amount;
            $user->save();

            return true;
        }
    }

    public static function isOk(float $amount, User $user, string $type_money): bool
    {
        return match($type_money) {
            "argent" => $user->argent >= $amount,
            "tpoint" => $user->tpoint >= $amount,
            "research" => $user->research >= $amount,
        };
    }
}
