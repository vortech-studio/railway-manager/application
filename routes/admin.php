<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::prefix('admin')->middleware(["web","admin"])->group(function () {
    Route::get('/', \App\Http\Controllers\Admin\DashboardController::class)->name('admin.dashboard');

    Route::prefix('gares')->group(function () {
        Route::get('/', [\App\Http\Controllers\Admin\GareController::class, 'index'])->name('admin.gare.index');
        Route::get('create', [\App\Http\Controllers\Admin\GareController::class, 'create'])->name('admin.gare.create');
        Route::post('create', [\App\Http\Controllers\Admin\GareController::class, 'store'])->name('admin.gare.store');
        Route::get('{id}', [\App\Http\Controllers\Admin\GareController::class, 'show'])->name('admin.gare.show');
    });

    Route::prefix('hubs')->group(function () {
        Route::get('/', [\App\Http\Controllers\Admin\HubController::class, 'index'])->name('admin.hub.index');
        Route::get('create', [\App\Http\Controllers\Admin\HubController::class, 'create'])->name('admin.hub.create');
        Route::post('create', [\App\Http\Controllers\Admin\HubController::class, 'store'])->name('admin.hub.store');
        Route::get('{id}/edit', [\App\Http\Controllers\Admin\HubController::class, 'edit'])->name('admin.hub.edit');
        Route::put('{id}/edit', [\App\Http\Controllers\Admin\HubController::class, 'update'])->name('admin.hub.update');
        Route::delete('{id}', [\App\Http\Controllers\Admin\HubController::class, 'delete'])->name('admin.hub.delete');
    });

    Route::prefix('lignes')->group(function () {
        Route::get('/', [\App\Http\Controllers\Admin\LigneController::class, 'index'])->name('admin.ligne.index');
        Route::get('create', [\App\Http\Controllers\Admin\LigneController::class, 'create'])->name('admin.ligne.create');
        Route::post('create', [\App\Http\Controllers\Admin\LigneController::class, 'store'])->name('admin.ligne.store');
        Route::get('{id}', [\App\Http\Controllers\Admin\LigneController::class, 'show'])->name('admin.ligne.show');
        Route::get('{id}/edit', [\App\Http\Controllers\Admin\LigneController::class, 'edit'])->name('admin.ligne.edit');
        Route::put('{id}/edit', [\App\Http\Controllers\Admin\LigneController::class, 'update'])->name('admin.ligne.update');
        Route::delete('{id}', [\App\Http\Controllers\Admin\LigneController::class, 'delete'])->name('admin.ligne.delete');

        Route::prefix('{id}/stations')->group(function () {
            Route::post('/', [\App\Http\Controllers\Admin\LigneStationController::class, 'store'])->name('admin.ligne.station.store');
            Route::post('/requirement', [\App\Http\Controllers\Admin\LigneStationController::class, 'storeRequirement'])->name('admin.ligne.station.storeRequirement');
        });
    });

    Route::prefix('engines')->group(function () {
        Route::get('/', [\App\Http\Controllers\Admin\EngineController::class, 'index'])->name('admin.engine.index');
        Route::get('create', [\App\Http\Controllers\Admin\EngineController::class, 'create'])->name('admin.engine.create');
        Route::post('create', [\App\Http\Controllers\Admin\EngineController::class, 'store'])->name('admin.engine.store');
        Route::get('{id}', [\App\Http\Controllers\Admin\EngineController::class, 'show'])->name('admin.engine.show');
        Route::get('{id}/edit', [\App\Http\Controllers\Admin\EngineController::class, 'edit'])->name('admin.engine.edit');
        Route::put('{id}/edit', [\App\Http\Controllers\Admin\EngineController::class, 'update'])->name('admin.engine.update');
        Route::delete('{id}', [\App\Http\Controllers\Admin\EngineController::class, 'delete'])->name('admin.engine.delete');
    });

    Route::prefix('research')->group(function () {

    });


    Route::prefix('users')->group(function () {
        Route::get('/', [\App\Http\Controllers\Admin\UserController::class, 'index'])->name('admin.user.index');
        Route::get('create', [\App\Http\Controllers\Admin\UserController::class, 'create'])->name('admin.user.create');
        Route::post('create', [\App\Http\Controllers\Admin\UserController::class, 'store'])->name('admin.user.store');
        Route::get('{id}', [\App\Http\Controllers\Admin\UserController::class, 'show'])->name('admin.user.show');
        Route::get('{id}/edit', [\App\Http\Controllers\Admin\UserController::class, 'edit'])->name('admin.user.edit');
        Route::put('{id}/edit', [\App\Http\Controllers\Admin\UserController::class, 'update'])->name('admin.user.update');
        Route::delete('{id}', [\App\Http\Controllers\Admin\UserController::class, 'destroy'])->name('admin.user.destroy');
    });

    Route::prefix('blogs')->group(function () {
        Route::get('/', [\App\Http\Controllers\Admin\BlogController::class, 'index'])->name('admin.blog.index');
        Route::get('create', [\App\Http\Controllers\Admin\BlogController::class, 'create'])->name('admin.blog.create');
        Route::post('create', [\App\Http\Controllers\Admin\BlogController::class, 'store'])->name('admin.blog.store');
        Route::get('{id}', [\App\Http\Controllers\Admin\BlogController::class, 'show'])->name('admin.blog.show');
        Route::get('{id}/edit', [\App\Http\Controllers\Admin\BlogController::class, 'edit'])->name('admin.blog.edit');
        Route::put('{id}/edit', [\App\Http\Controllers\Admin\BlogController::class, 'update'])->name('admin.blog.update');
        Route::delete('{id}', [\App\Http\Controllers\Admin\BlogController::class, 'delete'])->name('admin.blog.delete');
    });

    Route::prefix('badges')->group(function () {
        Route::get('/', [\App\Http\Controllers\Admin\BadgeController::class, 'index'])->name('admin.badge.index');
        Route::prefix('achievements')->group(function () {
            Route::post('/', [\App\Http\Controllers\Admin\BadgeAchievementController::class, 'store'])->name('admin.badge.achievement.store');
            Route::put('{id}', [\App\Http\Controllers\Admin\BadgeAchievementController::class, 'update'])->name('admin.badge.achievement.update');
        });

        Route::prefix('levels')->group(function () {
            Route::post('/', [\App\Http\Controllers\Admin\BadgeLevelController::class, 'store'])->name('admin.badge.level.store');
            Route::put('{id}', [\App\Http\Controllers\Admin\BadgeLevelController::class, 'update'])->name('admin.badge.level.update');
        });

        Route::prefix('rewards')->group(function () {

        });
    });

    Route::prefix('vourcher')->group(function () {
        Route::get('/', [\App\Http\Controllers\Admin\VourcherController::class, 'index'])->name('admin.vourcher.index');
        Route::get('create', [\App\Http\Controllers\Admin\VourcherController::class, 'create'])->name('admin.vourcher.create');
        Route::post('create', [\App\Http\Controllers\Admin\VourcherController::class, 'store'])->name('admin.vourcher.store');
    });

    Route::prefix('configs')->group(function () {
        Route::get('/');
    });
});
