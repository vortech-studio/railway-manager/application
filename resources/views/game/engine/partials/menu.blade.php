<div class="d-flex flex-row align-items-center">
    <a href="{{ route('engine.buy.index') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Achat d'un matériel">
        <img src="{{ asset('/storage/icons/mouvement/achat_materiel.png') }}" class="w-50px" alt="">
    </a>
    <a href="{{ route('engine.maintenance.book') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Carnet d'entretien">
        <img src="{{ asset('/storage/icons/technician.png') }}" class="w-50px" alt="">
    </a>
    <a href="{{ route('engine.index') }}" class="btn btn-flex btn-icon me-5" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Ma flotte">
        <img src="{{ asset('/storage/icons/train.png') }}" class="w-50px" alt="">
    </a>
</div>
