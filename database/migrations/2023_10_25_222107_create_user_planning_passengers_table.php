<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlanningPassengersTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_planning_passengers', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['first', 'second']);
            $table->integer('nb_passengers');

            $table->foreignId('user_planning_id')
               ->constrained()
               ->cascadeOnUpdate()
               ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_planning_passengers');
    }
}
