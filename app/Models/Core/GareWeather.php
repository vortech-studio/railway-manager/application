<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class GareWeather extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'gare_weathers';

    public function gare()
    {
        return $this->belongsTo(Gares::class);
    }
}
