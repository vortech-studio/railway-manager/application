<?php

namespace App\Models\User;

use App\Models\Core\GareEquipement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserTechnicentreTask extends Model
{
    public $timestamps = false;

    protected $casts = [
        'start_at' => 'datetime',
        'end_at' => 'datetime',
    ];
    protected $guarded = [];
    protected $appends = [
        "status_label",
        'other_info',
        'start_at_format',
        'end_at_format',
        'type_text'
    ];

    public function technicentre(): BelongsTo
    {
        return $this->belongsTo(UserTechnicentre::class, 'user_technicentre_id');
    }

    public function staff(): BelongsTo
    {
        return $this->belongsTo(UserTechnicentreStaf::class, 'user_technicentre_staf_id');
    }

    public function engine(): BelongsTo
    {
        return $this->belongsTo(UserEngine::class, 'user_engine_id');
    }

    public function equipement(): BelongsTo
    {
        return $this->belongsTo(GareEquipement::class, 'gare_equipement_id');
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(UserTechnicentreTasking::class);
    }

    public function getStatusLabelAttribute()
    {
        return match ($this->status) {
            "plannified" => '<span class="badge badge-lg badge-info"><i class="fa-solid fa-calendar text-white me-2"></i> Planifié</span>',
            "progressed" => '<span class="badge badge-lg badge-warning animation-blink"><i class="fa-solid fa-cogs text-white me-2"></i> En cours</span>',
            "finished" => '<span class="badge badge-lg badge-success"><i class="fa-solid fa-check-circle text-white me-2"></i> Terminé</span>',
            "canceled" => '<span class="badge badge-lg badge-danger"><i class="fa-solid fa-xmark-circle text-white me-2"></i> Annulé</span>',
        };
    }

    public function getOtherInfoAttribute()
    {
        return [
            "completed_percent" => floatval($this->tasks()->where('status', 'finished')->count() / $this->tasks()->count() * 100)
        ];
    }

    public function getStartAtFormatAttribute()
    {
        return $this->start_at->format("d/m/Y");
    }

    public function getEndAtFormatAttribute()
    {
        return $this->end_at->format("d/m/Y");
    }

    public function getTypeTextAttribute()
    {
        return match ($this->type) {
            "engine_prev" => "Maintenance preventive",
            "engine_cur" => "Maintenance curative",
            "infra" => "Intervention sur Infrastructures"
        };
    }
}
