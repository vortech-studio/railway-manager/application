<table class="table bg-color-eva w-100 rounded-2 text-white gs-7 gy-7 gx-7">
    <thead>
    <tr class="h-120px">
        <th class="w-75" colspan="2">
            <div class="d-flex flex-row align-items-center">
                <img src="{{ asset('/storage/icons/train-station-departure.png') }}" alt="" class="w-75px me-10" />
                <div class="fw-bolder fs-2x">Départ</div>
            </div>
        </th>
        <th class="w-25">
            <div class="d-flex flex-row border border-2 border-white p-5 justify-content-center align-items-end rounded-3">
                <span id="clock-hours" class="fs-1">23</span>
                <span class="animation-blink">
                            <span class="animation-blink-clock fs-1">:</span>
                        </span>
                <span id="clock-minutes" class="fs-1">32</span>
                <small id="clock-seconds" class="fs-3 ms-1 text-orange-600">37</small>
            </div>
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($plannings as $planning)
        <tr class="bg-gray-500">
            <td class="w-100px">
                <div class="d-flex flex-column justify-content-center align-items-center">
                    @if($planning->status == 'retarded')
                        <div class="fw-bold fs-3x text-color-eva fs-strike">{{ $planning->date_depart->format("H:i") }}</div>
                        <div class="fw-bolder fs-2x text-warning">{{ $planning->date_depart->addMinutes($planning->retarded_time)->format("H:i") }}</div>
                    @elseif($planning->status == 'canceled')
                        <div class="fw-bold fs-3x text-danger fs-strike">{{ $planning->date_depart->format("H:i") }}</div>
                    @else
                        <div class="fw-bolder fs-3x text-color-eva">{{ $planning->date_depart->format("H:i") }}</div>
                    @endif

                    <div class="badge badge-lg badge-outline badge-secondary fs-1 fw-bolder">
                        {!! $planning->status_format !!}
                    </div>
                </div>
            </td>
            <td class="bg-white w-50">
                <div class="d-flex flex-row justify-content-between align-items-center">
                    <div class="d-flex flex-column w-100">
                        <span class="fs-3x text-color-eva fw-bold">{{ $planning->ligne->ligne->stationEnd->name }}</span>
                        <div class="d-flex flex-row align-items-center" style="overflow: hidden">
                            <div class="fs-3 text-color-eva-arrival me-3">VIA</div>
                            @foreach($planning->ligne->ligne->stations as $station)
                                @if($station->gare->name == $planning->ligne->ligne->stationStart->name)
                                    @continue
                                @endif
                                <div class="fs-3 text-gray-500">{{ $station->gare->name }}</div>
                                <i class="fa-solid fa-circle text-primary fs-9 mx-3"></i>
                                @if($station->gare->name == $planning->ligne->ligne->stationEnd->name)
                                    @break
                                @endif

                            @endforeach
                        </div>
                    </div>
                    @if($planning->date_depart->diffInMinutes(now()) < 20)
                        <div class="bg-color-eva w-80px h-80px d-flex justify-content-center align-items-center rounded-2 shadow-sm">
                            <span class="text-white fs-2x">{{ $planning->ligne->quai }}</span>
                        </div>
                    @endif
                </div>
            </td>
            <td class="bg-color-eva-arrival text-color-eva">
                <div class="d-flex flex-column justify-content-center align-items-center p-5">
                    <div class="fs-1">{{ Str::upper($planning->engine->engine->type_train) }}</div>
                    <div class="fw-bolder fs-2x">{{ number_format($planning->number_travel, 0, '', ' ') }}</div>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
