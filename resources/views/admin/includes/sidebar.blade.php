<!--begin::Sidebar-->
<div id="kt_app_sidebar" class="app-sidebar flex-column" data-kt-drawer="true" data-kt-drawer-name="app-sidebar" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="225px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle">
    <!--begin::Logo-->
    <div class="app-sidebar-logo px-6" id="kt_app_sidebar_logo">
        <!--begin::Logo image-->
        <a href="{{ route('admin.dashboard') }}">
            <img alt="Logo" src="{{ Storage::disk('public')->url('logos/logo-long-white.png') }}" class="h-25px app-sidebar-logo-default" />
            <img alt="Logo" src="/storage/logos/logo-carre-white.png" class="h-20px app-sidebar-logo-minimize" />
        </a>
        <!--end::Logo image-->
        <!--begin::Sidebar toggle-->
        <div id="kt_app_sidebar_toggle" class="app-sidebar-toggle btn btn-icon btn-shadow btn-sm btn-color-muted btn-active-color-primary body-bg h-30px w-30px position-absolute top-50 start-100 translate-middle rotate" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="app-sidebar-minimize">
            <i class="ki-duotone ki-double-left fs-2 rotate-180">
                <span class="path1"></span>
                <span class="path2"></span>
            </i>
        </div>
        <!--end::Sidebar toggle-->
    </div>
    <!--end::Logo-->
    <!--begin::sidebar menu-->
    <div class="app-sidebar-menu overflow-hidden flex-column-fluid">
        <!--begin::Menu wrapper-->
        <div id="kt_app_sidebar_menu_wrapper" class="app-sidebar-wrapper hover-scroll-overlay-y my-5" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer" data-kt-scroll-wrappers="#kt_app_sidebar_menu" data-kt-scroll-offset="5px" data-kt-scroll-save-state="true">
            <!--begin::Menu-->
            <div class="menu menu-column menu-rounded menu-sub-indention px-3" id="#kt_app_sidebar_menu" data-kt-menu="true" data-kt-menu-expand="false">
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link {{ Route::is(["admin.dashboard"]) ? 'active' : '' }}" href="{{ route('admin.dashboard') }}">
											<span class="menu-icon">
												<i class="ki-duotone ki-home fs-2"></i>
											</span>
                        <span class="menu-title">Tableau de Bord</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <div class="menu-item pt-5">
                    <div class="menu-content">
                        <span class="menu-heading fw-bold text-uppercase fs-7">Jeux</span>
                    </div>
                </div>
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link {{ Route::is(["admin.gare.*"]) ? 'active' : '' }}" href="{{ route('admin.gare.index') }}">
						<span class="menu-icon">
							<i class="ki-duotone ki-home-1 fs-2">
                                <i class="path1"></i>
                                 <i class="path2"></i>
                            </i>
						</span>
                        <span class="menu-title">Gestion des Gare</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link {{ Route::is(["admin.hub.*"]) ? 'active' : '' }}" href="{{ route('admin.hub.index') }}">
						<span class="menu-icon">
							<i class="ki-duotone ki-share fs-2">
                                <i class="path1"></i>
                                <i class="path2"></i>
                                <i class="path3"></i>
                                <i class="path4"></i>
                                <i class="path5"></i>
                                <i class="path6"></i>
                            </i>
						</span>
                        <span class="menu-title">Gestion des Hubs</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link {{ Route::is(["admin.ligne.*"]) ? 'active' : '' }}" href="{{ route('admin.ligne.index') }}">
						<span class="menu-icon">
							<i class="ki-duotone ki-data fs-2">
                                <i class="path1"></i>
                                <i class="path2"></i>
                                <i class="path3"></i>
                                <i class="path4"></i>
                                <i class="path5"></i>
                            </i>
						</span>
                        <span class="menu-title">Gestion des Lignes</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link {{ Route::is(["admin.engine.*"]) ? 'active' : '' }}" href="{{ route('admin.engine.index') }}">
						<span class="menu-icon">
							<i class="ki-duotone ki-bus fs-2">
                                 <i class="path1"></i>
                                 <i class="path2"></i>
                                 <i class="path3"></i>
                                 <i class="path4"></i>
                                 <i class="path5"></i>
                            </i>
						</span>
                        <span class="menu-title">Gestion du matériels roulants</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <div class="menu-item pt-5">
                    <div class="menu-content">
                        <span class="menu-heading fw-bold text-uppercase fs-7">Administration</span>
                    </div>
                </div>
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link {{ Route::is(["admin.user.*"]) ? 'active' : '' }}" href="{{ route('admin.user.index') }}">
						<span class="menu-icon">
							<i class="ki-duotone ki-user fs-2">
                                <i class="path1"></i>
                                 <i class="path2"></i>
                            </i>
						</span>
                        <span class="menu-title">Utilisateurs</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link {{ Route::is(["admin.blog.*"]) ? 'active' : '' }}" href="{{ route('admin.blog.index') }}">
						<span class="menu-icon">
							<i class="ki-duotone ki-notification-on fs-2">
                                <i class="path1"></i>
                                 <i class="path2"></i>
                            </i>
						</span>
                        <span class="menu-title">Annonces</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link {{ Route::is(["admin.badge.*"]) ? 'active' : '' }}" href="{{ route('admin.badge.index') }}">
						<span class="menu-icon">
							<i class="ki-duotone ki-medal-star fs-2">
                                <i class="path1"></i>
                                 <i class="path2"></i>
                            </i>
						</span>
                        <span class="menu-title">Bagdes & Récompenses</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link {{ Route::is(["admin.vourcher.*"]) ? 'active' : '' }}" href="{{ route('admin.vourcher.index') }}">
						<span class="menu-icon">
							<i class="ki-duotone ki-discount fs-2">
                                <i class="path1"></i>
                                 <i class="path2"></i>
                            </i>
						</span>
                        <span class="menu-title">Code d'échange</span>
                    </a>
                    <!--end:Menu link-->
                </div>
                <div class="menu-item">
                    <!--begin:Menu link-->
                    <a class="menu-link {{ Route::is(["admin.dashboard"]) ? 'active' : '' }}" href="{{ route('admin.dashboard') }}">
						<span class="menu-icon">
							<i class="fa-solid fa-cogs fs-2"></i>
						</span>
                        <span class="menu-title">Configurations</span>
                    </a>
                    <!--end:Menu link-->
                </div>
            </div>
            <!--end::Menu-->
        </div>
        <!--end::Menu wrapper-->
    </div>
    <!--end::sidebar menu-->
</div>
<!--end::Sidebar-->
