<?php

namespace App\Http\Controllers\Api\Game\Network;

use App\Http\Controllers\Controller;
use App\Models\User\UserPlanning;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PlanningTravelController extends Controller
{
    public function info(Request $request, $user_id, $planning_id)
    {
        $planning = UserPlanning::find($planning_id);

        if($planning->status != 'arrival') {
            $actual_station_count = $planning->planning_stations()->where('status', 'done')->count();
            $total_station_count = $planning->planning_stations()->count();

            if($actual_station_count != $total_station_count) {
                $next_station = [
                    "name" => $planning->planning_stations()->where('status', 'init')->orderBy('id', 'asc')->first()->name,
                    "lat" => $planning->planning_stations()->where('status', 'init')->orderBy('id', 'asc')->first()->ligneStation->gare->latitude,
                    "long" => $planning->planning_stations()->where('status', 'init')->orderBy('id', 'asc')->first()->ligneStation->gare->longitude,
                    "time" => $planning->planning_stations()->where('status', 'init')->orderBy('id', 'asc')->first()->arrival_at->format('H:i'),
                ];
            } else {
                $next_station = [
                    "name" => null,
                    "lat" => null,
                    "long" => null,
                    "time" => null
                ];
            }

            $latlng = [
                [
                    "lat" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->latitude,
                    "lng" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->longitude,
                ],
                [
                    "lat" => $planning->planning_stations()->where('status', 'init')->orderBy('id', 'asc')->first()->ligneStation->gare->latitude,
                    "lng" => $planning->planning_stations()->where('status', 'init')->orderBy('id', 'asc')->first()->ligneStation->gare->longitude,
                ]
            ];
        } else {
            $actual_station_count = $planning->planning_stations()->where('status', 'done')->count();
            $total_station_count = $planning->planning_stations()->count();
            $next_station = [
                "name" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->name,
                "lat" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->latitude,
                "long" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->longitude,
                "time" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->arrival_at->format('H:i'),
            ];

            $latlng = [
                [
                    "lat" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->latitude,
                    "lng" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->longitude,
                ],
                [
                    "lat" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->latitude,
                    "lng" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->longitude,
                ]
            ];
        }


        $p = [
            "planning" => $planning,
            "actual_station" => [
                "name" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->name,
                "lat" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->latitude,
                "long" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->longitude,
                "time" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->arrival_at->format('H:i'),
            ],
            "next_station" => $next_station,
            "latlng" => $latlng,
            "percent_advance" => round($actual_station_count * 100 / $total_station_count, 2),
            "engine_speed_max" => $planning->engine->engine->velocity,
            "percent_advance_station" => $this->calcPercentTimeInPercent($planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->departure_at, $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->arrival_at)
        ];

        return response()->json($p);
    }

    public function map(Request $request, $planning_id)
    {
        $planning = UserPlanning::find($planning_id);
        $ligne = $planning->ligne->ligne;

        $stations = [];
        $stations_cos = [];

        foreach ($planning->planning_stations as $station) {
            if($station->ligne_station_id != $ligne->stationStart->id && $station->ligne_station_id != $ligne->stationEnd->id) {
                $stations[] = [
                    "name" => $station->name,
                    "latitude" => $station->ligneStation->gare->latitude,
                    "longitude" => $station->ligneStation->gare->longitude,
                    "nb_voyageur" => $station->ligneStation->gare->getPrevPassenger(),
                    "status" => [
                        "name" => $station->status,
                        "format" => $station->status_badge_format
                    ]
                ];

                $stations_cos[] = [
                    $station->ligneStation->gare->longitude,
                    $station->ligneStation->gare->latitude,
                ];
            }

        }
        if($planning->status == 'initialized')
        {

            $actual_station = [
                "name" => $planning->ligne->ligne->stationStart->name,
                "latitude" => $planning->ligne->ligne->stationStart->latitude,
                "longitude" => $planning->ligne->ligne->stationStart->longitude,
                "nb_voyageur" => $planning->ligne->ligne->stationStart->getPrevPassenger(),
                "status" => [
                    "name" => $planning->planning_stations()->first()->status,
                    "format" => $planning->planning_stations()->first()->status_badge_format
                ]
            ];

            foreach ($planning->planning_stations as $k => $station) {
                if($k == 1) {
                    $next_station = [
                        "name" => $station->name,
                        "latitude" => $station->ligneStation->gare->latitude,
                        "longitude" => $station->ligneStation->gare->longitude,
                        "nb_voyageur" => $station->ligneStation->gare->getPrevPassenger(),
                        "status" => [
                            "name" => $station->status,
                            "format" => $station->status_badge_format
                        ],
                        "next_time" => null
                    ];
                }
            }

        } elseif($planning->status == 'travel' || $planning->status == 'in_station') {

            $actual_station = [
                "name" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->name,
                "latitude" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->latitude,
                "longitude" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->longitude,
                "nb_voyageur" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->ligneStation->gare->getPrevPassenger(),
                "status" => [
                    "name" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->status,
                    "format" => $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->status_badge_format
                ],
            ];

            $next_station = [
                "name" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->name,
                "latitude" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->ligneStation->gare->latitude,
                "longitude" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->ligneStation->gare->longitude,
                "nb_voyageur" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->ligneStation->gare->getPrevPassenger(),
                "status" => [
                    "name" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->status,
                    "format" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->status_badge_format
                ],
                "next_time" => $this->calcPercentTimeInPercent(
                    $planning->planning_stations()->where('status', 'done')->orderBy('id', 'desc')->first()->departure_at,
                    $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->arrival_at
                )
            ];
        } else {

            $actual_station = [
                "name" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->name,
                "latitude" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->ligneStation->gare->latitude,
                "longitude" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->ligneStation->gare->longitude,
                "nb_voyageur" => $planning->planning_stations()->where('status', 'init')->orderBy('id', 'desc')->first()->ligneStation->gare->getPrevPassenger(),
                "status" => [
                    "name" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->status,
                    "format" => $planning->planning_stations()->where('status', 'init')->orderBy('id')->first()->status_badge_format
                ],
            ];

            $next_station = [];
        }

        $ligne = [
            "name" => $ligne->name,
            "type" => $ligne->type_ligne,
            "coordinates" => $stations_cos,
            "station_start" => $ligne->stationStart,
            "station_end" => $ligne->stationEnd,
            "time_min" => $ligne->time_min
        ];

        return response()->json([
            "travel" => $planning,
            "ligne" => $ligne,
            "stations" => $stations,
            "stations_cos" => $stations_cos,
            "actual_station" => $actual_station,
            "next_station" => $next_station,
        ]);
    }

    private function calcPercentTimeInPercent($departureActualStation, $arrivalNextStation)
    {
        $start = Carbon::createFromTimestamp(strtotime($departureActualStation))->timestamp;
        $end = Carbon::createFromTimestamp(strtotime($arrivalNextStation))->timestamp;
        $timespan = $end - $start;
        $current = now()->timestamp - $start;
        $progress = $current / $timespan;
        $remaining = (1 - $progress) * 100;
        if($remaining >= 100) {
            return 100;
        } else {
            return $remaining;
        }
    }
}
