<?php

namespace App\Http\Controllers\Game;

use App\Http\Controllers\Controller;
use App\Models\Core\Achievement;
use Illuminate\Http\Request;
use LevelUp\Experience\Events\AchievementAwarded;
use Stripe\Checkout\Session;
use Stripe\Price;
use Stripe\Stripe;

class AccountController extends Controller
{
    public function index()
    {
        seo()->title(config('app.name') . " - Mon Compte");
        return view('game.account.index');
    }

    public function objectif()
    {
        seo()->title(config('app.name') . " - Mon Agenda");
        return view('game.account.objectif');
    }

    public function premium(Request $request)
    {
        Stripe::setApiKey(config('services.stripe.secret_key'));
        seo()->title(config('app.name') . " - Abonnement Premium");
        if ($request->get('state') == 'success') {
            $checkout_session = Session::retrieve($request->get('session-id'));
            $user = auth()->user();
            $user->premium = true;
            $user->stripe_id = $checkout_session->customer->id;
            $user->save();

            $sub = $user->subscription()->create([
                "token" => $request->get('session_id'),
                "user_id" => $user->id,
            ]);

            $user->grantAchievement(Achievement::find(1));
            event(new AchievementAwarded(Achievement::find(1), auth()->user()));

            return redirect()->route('account.premium', ["session_id" => $request->get('session_id')])->with('success', 'Votre abonnement a bien été créé.');
        } else if ($request->get('state') == 'cancel') {
            return redirect()->route('account.premium')->with('error', 'Votre abonnement a été annulé.');
        } else {
            return view('game.account.premium');
        }

    }

    public function subscribePremium(Request $request)
    {
        Stripe::setApiKey(config('services.stripe.secret_key'));

        if ($request->get('action') == 'subscribe') {
            try {
                $prices = Price::all([
                    "lookup_keys" => [$request->get('lookup_key')],
                    "expand" => ["data.product"]
                ]);

                $checkout_session = \Stripe\Checkout\Session::create([
                    'payment_method_types' => ['card'],
                    'line_items' => [
                        [
                            'price' => $prices->data[0]->id,
                            'quantity' => 1,
                        ],
                    ],
                    'mode' => 'subscription',
                    'success_url' => route('account.premium') . "?state=success&session_id={CHECKOUT_SESSION_ID}",
                    'cancel_url' => route('account.premium') . "?state=cancel",
                ]);

                return redirect()->to($checkout_session->url);
            } catch (\Exception $exception) {
                \Log::critical($exception->getMessage());
                return redirect()->back()->with('error', 'Une erreur est survenue lors de la création de votre abonnement.');
            }
        } else {
            try {
                $checkout_session = Session::retrieve($request->get('session-id'));

                $session = \Stripe\BillingPortal\Session::create([
                    'customer' => $checkout_session->customer,
                    'return_url' => route('account.premium'),
                ]);

                return redirect()->to($session->url);
            } catch (\Exception $exception) {
                \Log::critical($exception->getMessage());
                return redirect()->back()->with('error', 'Une erreur est survenue lors de la création de votre abonnement.');
            }
        }
    }

    public function notifications()
    {
        seo()->title(config('app.name') . " - Mes notifications");
        auth()->user()->unreadNotifications->markAsRead();
        return view('game.account.notifications');
    }
}
