<?php

namespace App\Traits;

use App\Models\Core\Gares;
use App\Services\DataGouv\DecoupageCommune;
use App\Services\SNCF\GareVoyageur;
use App\Services\SNCF\Quai;
use Carbon\Carbon;

trait GareTrait
{
    public function formatRegion(object $gare): array|string|null
    {
        if (isset($gare->gare_regionsncf_libelle)) {
            return \Str::remove('REGION ', $gare->gare_regionsncf_libelle);
        } else {
            return preg_replace('GARE [A-Z]{1,2} ', '', $gare->rg_libelle);
        }
    }

    public function calcSuperficie(object $gare = null, $type_gare = null): int
    {
        if ($gare) {
            return match ($gare->segmentdrg_libelle || 'c') {
                default => rand(300, 1200),
                'b' => rand(1200, 4500),
                'a' => rand(4500, 11000)
            };
        } else {
            return match ($type_gare || 'c') {
                default => rand(300, 1200),
                'b' => rand(1200, 4500),
                'a' => rand(4500, 11000)
            };
        }
    }

    public function calcSuperficieQuai(object $gare = null, int $nb_quai = null, string $long_quai = null): float
    {
        if ($gare) {
            $api = new Quai();

            return $api->getLongueurs($gare) * $api->nbQuai($gare);
        } else {

            return $long_quai * $nb_quai;
        }
    }

    public function hasCommerce(object $gare = null, $type_gare = null): bool
    {
        if($gare) {
            if (isset($gare->segmentdrg_libelle)) {
                return $gare->segmentdrg_libelle == 'a' || $gare->segmentdrg_libelle == 'b';
            } else {
                return false;
            }
        } else {
            if (isset($type_gare)) {
                return $type_gare == 'a' || $type_gare == 'b';
            } else {
                return false;
            }
        }
    }

    public function hasPub(object $gare = null, $type_gare = null): bool
    {
        if($gare) {
            if (isset($gare->segmentdrg_libelle)) {
                return $gare->segmentdrg_libelle == 'a' || $gare->segmentdrg_libelle == 'b' || $gare->segmentdrg_libelle == 'c';
            } else {
                return false;
            }
        } else {
            if (isset($type_gare)) {
                return $type_gare == 'a' || $type_gare == 'b' || $type_gare == 'c';
            } else {
                return false;
            }
        }
    }

    public function hasParking(object $gare = null, int $superficie_gare = null, $type_gare = null): bool
    {
        if ($gare) {
            if (isset($gare->segmentdrg_libelle)) {
                return ($gare->segmentdrg_libelle == 'a' || $gare->segmentdrg_libelle == 'b' || $gare->segmentdrg_libelle == 'c') && $superficie_gare > 1500;
            } else {
                return false;
            }
        } else {
            if (isset($type_gare)) {
                return ($type_gare == 'a' || $type_gare == 'b' || $type_gare == 'c') && $superficie_gare > 500;
            } else {
                return false;
            }
        }
    }

    public function calcPrice(Gares $gares): float|int
    {
        return $gares->superficie_infra * $gares->superficie_quai;
    }

    public function calcTaxeHub(mixed $gare = null, Gares $gares = null): float|int
    {
        $price = $this->calcPrice($gares);
        if($gare) {
            $calc = $price / $gares->nb_quai / $this->timeDayWork($gare) / 10;

            if ($calc > 1000) {
                return $price / $gares->nb_quai / $this->timeDayWork($gare) / 100;
            } else {
                return $calc;
            }
        } else {
            $calc = $price / $gares->nb_quai / $this->timeDayWork(null, $gares->type_gare) / 10;

            if($calc > 1000) {
                return $price / $gares->nb_quai / $this->timeDayWork(null, $gares->type_gare) / 100;
            } else {
                return $calc;
            }
        }

    }

    public function calcPassengerFirst(Gares $gares): float|int
    {
        $calc = intval($gares->frequentation) / intval($gares->nb_habitant_city) * 20 / 100;
        if($calc < 1) {
            return $calc * 100;
        } else {
            return $calc;
        }
    }

    public function calcPassengerSecond(mixed $gares): float|int
    {
        $calc = intval($gares->frequentation) / intval($gares->nb_habitant_city) * 80 / 100;

        if($calc < 1) {
            return $calc * 100;
        } else {
            return $calc;
        }
    }

    public function timeDayWork(mixed $gare = null, string $type_gare = null)
    {
        if($gare) {
            $file = \Storage::disk('public')->get('horaires-des-gares1.json');
            $horaire = collect(json_decode($file))->where('nom_normal', $gare->gare_alias_libelle_noncontraint)->first();

            $switch = explode('-', $horaire->horaires_normal ?? '08:00-20:00');
            $timer = Carbon::createFromTimeString($switch[0])->diffInHours($switch[1] < $switch[0] ? Carbon::createFromTimeString($switch[1])->addDay() : Carbon::createFromTimeString($switch[1]));

            return $timer;
        } else {
            return match ($type_gare) {
                'a' => rand(16, 20),
                'b' => rand(12, 16),
                default => rand(8, 12)
            };
        }
    }

    public function calcNbSlotCommerce(Gares $gares)
    {
        if ($gares->commerce) {
            return $gares->superficie_infra + $gares->superficie_quai / 50;
        } else {
            return 0;
        }
    }

    public function calcNbSlotPub(Gares $gares)
    {
        if ($gares->pub) {
            return $gares->superficie_infra + $gares->superficie_quai / 25;
        } else {
            return 0;
        }
    }

    public function calcNbSlotParking(Gares $gares)
    {
        if ($gares->pub) {
            return $gares->superficie_infra + $gares->superficie_quai / 15;
        } else {
            return 0;
        }
    }

}
