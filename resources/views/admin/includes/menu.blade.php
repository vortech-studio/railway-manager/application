<div
    class="
        app-header-menu
        app-header-mobile-drawer
        align-items-stretch
    "
    data-kt-drawer="true"
    data-kt-drawer-name="app-header-menu"
    data-kt-drawer-activate="{default: true, lg: false}"
    data-kt-drawer-overlay="true"
    data-kt-drawer-width="250px"
    data-kt-drawer-direction="end"
    data-kt-drawer-toggle="#kt_app_header_menu_toggle"
    data-kt-swapper="true"
    data-kt-swapper-mode="{default: 'append', lg: 'prepend'}"
    data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}"
>
    <div
        class="menu menu-rounded menu-column menu-lg-row my-5 my-lg-0 align-items-stretch fw-semibold px-2 px-lg-0"
        id="kt_app_header_menu"
        data-kt-menu="true"
    >
        <div  data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start"  class="menu-item here show menu-here-bg me-0 me-lg-2" >
            <a class="menu-link" href="#">
                <span class="menu-icon">
                    <i class="ki-duotone ki-element-11 fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                    </i>
                </span>
                <span class="menu-title">Tableau de Bord</span>
            </a>
        </div>
        <div class="menu-item pt-5">
            <div class="menu-content">
                <span class="menu-heading fw-bold text-uppercase fs-7">Jeux</span>
            </div>
        </div>
        <div class="menu-item">
            <a class="menu-link" href="#">
                <span class="menu-icon">
                    <i class="ki-duotone ki-home-1 fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                </span>
                <span class="menu-title">Gestion des Gares</span>
            </a>
        </div>
        <div class="menu-item">
            <a class="menu-link" href="#">
                <span class="menu-icon">
                    <i class="ki-duotone ki-share fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                </span>
                <span class="menu-title">Gestion des Hubs</span>
            </a>
        </div>
        <div class="menu-item">
            <a class="menu-link" href="#">
                <span class="menu-icon">
                    <i class="ki-duotone ki-data fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                </span>
                <span class="menu-title">Gestion des Lignes</span>
            </a>
        </div>
        <div class="menu-item">
            <a class="menu-link" href="#">
                <span class="menu-icon">
                    <i class="ki-duotone ki-rocket fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                </span>
                <span class="menu-title">Gestion du matériels roulants</span>
            </a>
        </div>
        <div class="menu-item">
            <a class="menu-link" href="#">
                <span class="menu-icon">
                    <i class="ki-duotone ki-rocket fs-2">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                </span>
                <span class="menu-title">Gestion des Infrastructures</span>
            </a>
        </div>

    </div>
</div>
