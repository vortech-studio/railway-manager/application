<?php

namespace App\Observer;

use App\Models\User\UserDelivery;
use App\Notifications\Game\SendMessageNotification;

class UserDeliveryObserver
{
    public function created(UserDelivery $userDelivery): void
    {
        toastr()->info("Une livraison vient de commencer: ".$userDelivery->designation);
        $userDelivery->user->notify(new SendMessageNotification(
            "Nouvelle Livraison",
            "Une livraison vient de commencer: ".$userDelivery->designation,
            "info",
            $this->getIconByTypeDelivery($userDelivery),
            null,
            $userDelivery->type
        ));
    }

    public function updated(UserDelivery $userDelivery): void
    {
    }

    public function deleted(UserDelivery $userDelivery): void
    {
        toastr()->info("Une livraison vient de se terminer: ".$userDelivery->designation);
        $userDelivery->user->notify(new SendMessageNotification(
            "Livraison terminée",
            "Une livraison vient de se terminer: ".$userDelivery->designation,
            "info",
            $this->getIconByTypeDelivery($userDelivery),
            null,
            $userDelivery->type,
        ));
    }

    public function restored(UserDelivery $userDelivery): void
    {
    }

    public function forceDeleted(UserDelivery $userDelivery): void
    {
    }

    private function getIconByTypeDelivery(UserDelivery $userDelivery): string
    {
        return match ($userDelivery->type) {
            "hub" => asset('/storage/icons/hub_checkout.png'),
            "ligne" => asset('/storage/icons/ligne_checkout.png'),
            "engine" => asset('/storage/icons/train_buy.png'),
            "composition" => asset('/storage/icons/train.png'),
            "research" => asset('/storage/icons/research.png')
        };
    }
}
