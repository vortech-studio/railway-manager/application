<?php

namespace App\Http\Controllers\Game;

use App\Charts\EvoCAChart;
use App\Http\Controllers\Controller;
use App\Models\Core\Mouvement;
use App\Models\User;
use LevelUp\Experience\Models\Experience;

class DashboardController extends Controller
{
    public function __invoke()
    {
        seo()->title(config('app.name').' - Tableau de Bord');
        seo()->csrfToken();
        $plannings = auth()->user()->plannings()
            ->where('status', 'initialized')
            ->orWhere('status', 'departure')
            ->orWhere('status', 'retarded')
            ->orWhere('status', 'canceled')
            ->whereBetween('date_depart', [now(), now()->endOfDay()])
            ->orderBy('date_depart')
            ->limit(4)
            ->get();


        return view('game.dashboard', [
            "plannings" => $plannings,
        ]);
    }
}
