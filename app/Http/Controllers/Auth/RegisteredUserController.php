<?php

namespace App\Http\Controllers\Auth;

use Akibatech\FreeMobileSms\FreeMobileSms;
use App\Http\Controllers\Controller;
use App\Models\Core\Mouvement;
use App\Models\User;
use App\Notifications\Admin\SendMessageNotification;
use App\Providers\RouteServiceProvider;
use App\Services\Jira\JiraService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        seo()->title(config('app.name').' - Création de compte');
        seo()->csrfToken();

        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $faker = \Faker\Factory::create("fr_FR");
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'name_conseiller' => $faker->name("Male"),
        ]);

        event(new Registered($user));

        $company = User\UserCompany::automatedCreate($user);
        User\UserCompanySituation::automatedCreate($company);

        Mouvement::adding("revenue", "Création de l'entreprise - Reversion du capital", "subvention", config('global.start_argent'), $company->id);

        $user->update([
            "tpoint" => config('global.start_tpoint'),
            "research" => config('global.start_research'),
            "level_id" => 1
        ]);

        Auth::login($user);

        if(config('app.env') == 'production') {
            $customer_jira = JiraService::createCustomer($user->name, $user->email);
            app('freemobile')->send("Le compte JIRA pour l'utilisateur: {$user->name} a été créé avec succès, veuillez l'activé: ".$customer_jira["_links"]["self"]);
        }

        toast("Votre compte a été créé avec succès.", "success");

        foreach (User::where('admin', true)->get() as $admin) {
            $admin->notify(new SendMessageNotification(
                'Nouveau joueur',
                "Un nouveau joueur vient de s'inscrire sur Railway Simulator.",
                'info',
                "ki-user",
            ));
        }

        return redirect()->route('verification.notice');
    }
}
