<?php

namespace App\Http\Controllers\Game\Network;

use App\Http\Controllers\Controller;
use App\Jobs\Game\HubDeliveryJob;
use App\Models\Core\Hub;
use App\Models\Core\Mouvement;
use App\Models\User\Mailbox;
use App\Models\User\UserDelivery;
use App\Models\User\UserHub;
use App\Traits\HasCheckout;
use Illuminate\Http\Request;

class HubController extends Controller
{
    public function show($id)
    {
        seo()->title(config('app.name') . ' - Hub');
        $hub = auth()->user()->hubs()->with('lignes', 'hub', 'mouvements')->find($id);

        return view('game.network.hub.show', compact('hub'));
    }

    public function checkout()
    {
        seo()->title(config('app.name') . ' - Achat d\'un hub');
        $hubs = Hub::where('active', true)->get();

        return view('game.network.hub.checkout', compact('hubs'));
    }

    public function checkoutConfirm(Request $request)
    {
        $user = auth()->user();
        $hub = Hub::find($request->input('hub_id'));

        if(HasCheckout::isOk($request->input('amount'), $user, 'argent')) {
            Mouvement::adding(
                'charge',
                "Achat d'un hub: ".$hub->gare->name,
                "achat_hub",
                $request->input('amount'),
                $user->company->id,
            );

            if($user->admin) {
                $user_hub = $user->hubs()->create([
                    "nb_salarie_iv" => UserHub::calcNbSalarieIV($hub),
                    "nb_salarie_ir" => UserHub::calcNbSalarieIR($hub),
                    "nb_salarie_com" => 0,
                    "nb_salarie_technicentre" => 0,
                    "date_achat" => now(),
                    "km_ligne" => 0,
                    "user_id" => $user->id,
                    "hub_id" => $hub->id,
                    "active" => true
                ]);
            } else {
                $user_hub = $user->hubs()->create([
                    "nb_salarie_iv" => UserHub::calcNbSalarieIV($hub),
                    "nb_salarie_ir" => UserHub::calcNbSalarieIR($hub),
                    "nb_salarie_com" => 0,
                    "nb_salarie_technicentre" => 0,
                    "date_achat" => now(),
                    "km_ligne" => 0,
                    "user_id" => $user->id,
                    "hub_id" => $hub->id,
                ]);

                $delivery = $user->livraisons()->create([
                    "type" => "hub",
                    "designation" => "HUB: ".$hub->gare->name,
                    "user_id" => $user->id,
                    "user_hub_id" => $user_hub->id,
                ]);

                $delivery->end_at = now()->addMinutes($delivery->getRateFromDelivery());
                $delivery->save();

                $user->messages()->create([
                    "from" => $user->name_secretary,
                    "subject" => "Achat d'un nouveau HUB: ".$hub->gare->name,
                    "message" => view('game.includes.mailbox.checkout_hub', [
                        "hub_name" => $hub->gare->name,
                        "amount" => $request->input('amount'),
                        "delivery_at" => $delivery->end_at->format('d/m/Y H:i'),
                        "user" => $user
                    ]),
                    "important" => true,
                    "user_id" => $user->id,
                ]);

                dispatch(new HubDeliveryJob($delivery, $user_hub))->delay(now()->addMinutes($delivery->getRateFromDelivery()));
            }

            $user->addPoints(150);

            toastr()->success("Votre hub a bien été acheté !");
            return redirect()->route('network');
        } else {
            toastr()->error("Vous n'avez pas assez d'argent pour acheter ce hub !");
            return redirect()->route('hub.checkout');
        }
    }
}
