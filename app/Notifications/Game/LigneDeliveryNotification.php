<?php

namespace App\Notifications\Game;

use App\Models\User\UserLigne;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;

class LigneDeliveryNotification extends Notification
{
    use Queueable;
    public function __construct(public UserLigne $ligne)
    {
    }

    public function via($notifiable): array
    {
        return ['database', 'broadcast', WebPushChannel::class];
    }

    public function toDatabase($notifiable): array
    {
        return [
            'type' => 'success',
            'icon' => 'ki-delivery-time',
            'title' => 'Nouvelle livraison',
            'description' => "La ligne ".$this->ligne->ligne->name." est maintenant active",
            'time' => now(),
            'sector' => 'delivery'
        ];
    }

    public function toArray($notifiable): array
    {
        return [
            'type' => 'success',
            'icon' => 'ki-delivery-time',
            'title' => 'Nouvelle livraison',
            'description' => "La ligne ".$this->ligne->ligne->name." est maintenant active",
            'time' => now(),
            'sector' => 'delivery'
        ];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage([
            'type' => 'success',
            'icon' => 'ki-delivery-time',
            'title' => 'Nouvelle livraison',
            'description' => "La ligne ".$this->ligne->ligne->name." est maintenant active",
            'time' => now(),
            "user_id" => $this->ligne->user_id,
            'sector' => 'delivery'
        ]);
    }

    public function broadcastOn()
    {
        return ['delivery-channel'];
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new \NotificationChannels\WebPush\WebPushMessage)
            ->title('Nouvelle livraison')
            ->icon(asset('/storage/icons/fast-delivery.png'))
            ->body("La ligne ".$this->ligne->ligne->name." est maintenant active")
            ->action('Voir', 'notification_action');
    }
}
