<?php

namespace App\Services\SNCF;

class Quai
{
    public function liste(): string|\Illuminate\Support\Collection
    {
        try {
            $file = \Storage::disk('public')->get('liste-des-quais.json');

            return collect(json_decode($file));
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    protected function search(string $nameGare)
    {
        try {
            return $this->liste()->where('lib_gare', $nameGare);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getLongueur(object $gare = null, string $nameGare = null)
    {
        if(isset($gare)) {
            $quais = $this->search($gare->gare_alias_libelle_noncontraint);
        } else {
            $quais = $this->search($nameGare);
        }
        $long = 0;

        foreach ($quais as $quai) {
            $long = $quai->longueur;
        }

        return $long;
    }

    public function getLongueurs(object $gare)
    {
        $quais = $this->search($gare->gare_alias_libelle_noncontraint);
        $long = 0;

        foreach ($quais as $quai) {
            $long += $quai->longueur;
        }

        return $long;
    }

    public function nbQuai(object $gare = null, string $nameGare = null)
    {
        if(isset($gare)) {
            return $this->search($gare->gare_alias_libelle_noncontraint)->count();
        } else {
            return $this->search($nameGare)->count();
        }
    }
}
