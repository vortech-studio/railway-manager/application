<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Core\Hub;
use App\Models\Core\Ligne;
use App\Models\Core\Mouvement;
use App\Models\User;
use Illuminate\Http\Request;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function info(Request $request, $id)
    {
        return response()->json($request->user());
    }

    public function hubs(Request $request, $id)
    {
        $query = User::find($id)->hubs()->with('hub')->get();
        $hubs = [];

        foreach ($query as $item) {
            $hubs[] = [
                "gare" => $item->hub->gare->name,
                "date_achat" => $item->date_achat->format('d/m/Y'),
                "km_ligne" => $item->km_ligne ?? 0,
                "salaries" => [
                    "iv" => $item->nb_salarie_iv ?? 0,
                    "com" => $item->nb_salarie_com ?? 0,
                    "ir" => $item->nb_salarie_ir ?? 0,
                    "technicentre" => $item->nb_salarie_technicentre ?? 0,
                ],
                "ca_hub" => eur(Mouvement::where('user_hub_id', $item->hub_id)->where('type_account', 'revenue')->sum('amount')),
            ];
        }

        $hubs = collect($hubs);

        try {
            return DataTables::of($hubs)
                ->addIndexColumn()
                ->rawColumns(['ca_hub'])
                ->make(true);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function lignes(Request $request, $id)
    {
        $user = User::find($id);
        $query = $user->lignes()->with('ligne')->get();

        $lignes = [];

        foreach ($query as $ligne) {
            $compo_img = [];
            for($i=0; $i < $ligne->composition->user_engine->engine->technical->nb_wagon; $i++) {
                //$compo_img[] = \Storage::disk('engines')->get($ligne->composition->user_engine->engine->slug.'-'.$i.'.png');
                $compo_img[] = asset('/storage/engines/'.$ligne->composition->user_engine->engine->type_engine.'/'.$ligne->composition->user_engine->engine->slug.'-'.$i.'.gif');
            }

            $lignes[] = [
                "hub" => $ligne->ligne->hub->gare->name,
                "line" => $ligne->ligne->name,
                "date_achat" => $ligne->date_achat->format('d/m/Y'),
                "nb_depart" => $ligne->nb_depart_jour,
                "quai" => $ligne->quai,
                "composition" => $ligne->composition->user_engine->engine->name,
                "images" => $compo_img,
            ];
        }

        $lignes = collect($lignes);

        try {
            return DataTables::of($lignes)
                ->addIndexColumn()
                ->make(true);
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function addHub(Request $request, $user_id)
    {
        $hub = Hub::find($request->get('hub_id'));
        $user = User::find($user_id);

        if(User\UserHub::where('hub_id', $request->get('hub_id'))->count() == 0) {
            $user->hubs()->create([
                "nb_salarie_ir" => User\UserHub::calcNbSalarieIR($hub),
                "nb_salarie_com" => 0,
                "nb_salarie_iv" => User\UserHub::calcNbSalarieIV($hub),
                "nb_salarie_technicentre" => 0,
                "date_achat" => now(),
                "km_ligne" => 0,
                "hub_id" => $request->get('hub_id'),
                "user_id" => $user_id,
            ]);

            return response()->json([
                "status" => "success",
                "message" => "Hub ajouté au joueurs."
            ]);
        } else {
            return response()->json([
                "status" => "error",
                "message" => "Ce hub appartient déjà au joueurs."
            ], 400);
        }
    }

    public function addLigne(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $line = Ligne::find($request->get('ligne_id'));

        if(User\UserLigne::where('ligne_id', $request->get('ligne_id'))->count() == 0) {
            \Schema::disableForeignKeyConstraints();
            $quai = rand(1, $line->hub->gare->nb_quai);
            $ligne = $user->lignes()->create([
                "date_achat" => now(),
                "nb_depart_jour" => 0,
                "quai" => $quai,
                "user_hub_id" => $line->hub->id,
                "ligne_id" => $line->id,
                "user_composition_track_id" => null,
                "user_id" => $user_id,
            ]);

            $ligne->update([
                "nb_depart_jour" => User\UserLigne::calcNbDepartJour($ligne, $line->hub),
            ]);
            \Schema::enableForeignKeyConstraints();

            return response()->json([
                "status" => "success",
                "message" => "Ligne $ligne->name ajouté au joueurs."
            ]);
        } else {
            return response()->json([
                "status" => "error",
                "message" => "Cette ligne appartient déjà au joueurs."
            ], 400);
        }
    }

    public function checkout(Request $request, $user_id)
    {
        Stripe::setApiKey(config('services.stripe.secret_key'));
        try {
            $intent = PaymentIntent::create([
                'amount' => $request->get('amount'),
                'currency' => 'eur',
                'automatic_payment_methods' => ['enabled' => true],
                'metadata' => [
                    "user_id" => $user_id,
                    "product" => $request->get('product')
                ]
            ]);

            return response()->json([
                "success" => true,
                "message" => "Votre achat a bien été effectué !",
                "action" => $request->get('slug'),
                "client_secret" => $intent->client_secret
            ]);
        } catch (ApiErrorException $e) {
            \Log::critical($e->getMessage());
            return response()->json([
                "success" => false,
                "message" => "Une erreur est survenue lors de votre achat !",
                "action" => $request->get('slug')
            ], 500);
        }
    }
}
